# donsakagawarealty.com

Official Repo for Real Estate Mortgage System for R and Sons Properties

.htaccess files are ignored in this project so you need to create each of those on your local environment. 

Create a .htaccess on the root folder of the project with this format

############################################
Options +FollowSymLinks
RewriteEngine On
RewriteBase /

RewriteRule ^([^\.]+)$ $1.php [L]

RewriteCond %{HTTP_HOST} ^www\.(.+) [NC]
RewriteRule ^(.*) http://%1/$1 [R=301,NE,L]
#############################################


Create another .htaccess on the api/csm_v1/public with this format

############################################

RewriteEngine On

# Some hosts may require you to use the `RewriteBase` directive.
# If you need to use the `RewriteBase` directive, it should be the
# absolute physical path to the directory that contains this htaccess file.
#
# RewriteBase /

RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^ index.php [QSA,L]

############################################

Create another .htaccess on the api/csm_v1/ with this format

###########################################

RewriteRule .* - [env=HTTP_AUTHORIZATION:%{HTTP:Authorization}]

###########################################

<!---------------------Connection to the database------------------------>

settings.php is ignored in this project.So you need to create your own configuration to connect to the database. Create a settings.php file on this directory api/csm_v1/src/settings.php with this format.

##########################################
<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        // Database
        'db' => [
            'host' => 'localhost', // To change port, just add it afterwards like localhost:8889
            'dbname' => 'db_name here', // DB name or SQLite path
            'username' => 'DB User here',
            'password' => 'DB Password here'
        ]
    ],
];


#####################################################

