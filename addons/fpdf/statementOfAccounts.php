<?php
require('../fpdf/cellfit.php');



class PDF extends FPDF
{
// Pag
function Header()
{
    // Logo
    // $this->Image('logo.png',10,6,30);
    // Arial bold 15
    // $this->SetFont('Arial','B',15);
    // // Move to the right
    // $this->Cell(80);
    // // Title
    // $this->Cell(30,10,'Mabuhay Hardware Sales Report',4,0,'C');
    // $this->SetFont('Arial','B',12);
    //  $this->Cell(30,10,'Mabuhay Hardware Sales Report',1,0,'C');

    $this->Ln(25);
    $this->Image('logo-invert.jpg',92,0,-2000);
    $this->setFont("Arial",'B',10);
    $this->Cell(201,3,"R and Sons Properties Co.",0,1,"C");
     $this->setFont("Arial",'',8);
    $this->Cell(201,3," 2nd floor S10 at The Paddock, J.Camus St.",0,1,"C");
    $this->Cell(201,3," Corner General Luna St. , Brgy. 4-A Poblacion District,",0,1,"C");
    $this->Cell(201,3,"Davao City, Davao del Sur, Philippines 8000",0,1,"C");
    $this->Cell(201,3,"Tel no. (082) 322-6819",0,1,"C");
     // $this->setFont("Arial","I",10);
    // $this->Cell(260,5,$_GET["cid"],0,1,"C");
    // Line break
    $this->Ln(4);
}

// Page fo$oter
function Footer()
{
    $date = date("m-d-y");
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Times','I',8);
    // Page number
    $this->Cell(0,5,'Page '.$this->PageNo().'/{nb}',0,1,'C');
    $this->Cell(0,5,'R and Sons Properties Co.',0,1,'C');
    $this->Cell(0,-5,'Printed Date:'. $date,0,1,'R');

}
}

// Instanciation of inherited class


$pdf = new FPDF_CellFit();
$pdf->AliasNbPages();
$pdf->AddPage('P','A4');
         $amortzation = 0;
         $month = 0;
         $down =0;
         $dbhost = 'localhost';
         $dbuser = 'dbuser'; //dbuser
         $dbpass = '*/S3hd%/~]m~X<Zf';//*/S3hd%/~]m~X<Zf for testing live
         $dbname = 'webdsr_portal_janjan';//webdsr_portal_janjan for live
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass,$dbname);
         $price = 0;
         if(! $conn ) {
            die('Could not connect: ' . mysqli_error());
         }
         $sql1 = "SELECT CONCAT(cliente.`Fname`,' ',cliente.`Lname`) AS fullName,parent.`propertyName`,plist.`block`, plist.`lot`, property.`date_applied`,
 (plist.`sqm` * property.`sqmPricem2`) as originalPrice, property.`plan_terms`, property.`downpayment`, plist.`monthly_amortization`,
  CONCAT(agent.`Fname`,' ', agent.`Lname`) AS agent, property.`dueDate`,ABS(DATEDIFF(property.`date_applied`, CURDATE()))/30 AS DateDif, property.`fullypaid` FROM tbl_client_properties AS property, tbl_client AS cliente,
  tbl_agent AS agent, tbl_propertylist AS plist, tbl_propertyparent AS parent WHERE  property.`client_id` = cliente.`client_id`
  AND property.`agent_id` = agent.`agent_id` AND property.`property_id`=plist.`property_id` AND plist.`propertyParentID` = parent.`id` AND property.`cp_id`='".$_GET['cid']."' ";
            $resultz = mysqli_query($conn, $sql1);
         if ($resultz->num_rows == 1) {

$rowz = $resultz->fetch_assoc();
$today =($rowz["date_applied"]);
 $month = new DateTime ($rowz["date_applied"]);
 $month2 = new DateTime (date("Y-m-d"));
 $vieww = $month->diff($month2);
 $full = $rowz["fullypaid"];
 $view = (($vieww->format('%y') * 12) + $vieww->format('%m'));
 $down =$rowz["downpayment"];
$price = $rowz["originalPrice"];
$amortzation = ($price-$down)/$rowz["plan_terms"];
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"CLIENT'S NAME: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["fullName"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"ADDRESS: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["propertyName"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"BLOCK NO.: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["block"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"LOT NO.: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["lot"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"DATE APPLIED.: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,date("F  j, Y",strtotime($rowz["date_applied"])),0,1,"L");

$pdf->SetFont('Times','B',10);

$pdf->Cell(50,5,"CONTRACT PRICE: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,number_format(round($rowz["originalPrice"],2),2,'.',','),0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"PLAN TERMS: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["plan_terms"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"DOWNPAYMENT: ",0,0,"L");
$pdf->SetFont('Times','',10);
if ($rowz["downpayment"] > 0) {
 $pdf->Cell(100,5,number_format(round($rowz["downpayment"],2),2,'.',','),0,1,"L");
}
else
{
     $pdf->Cell(100,5,"N/A",0,1,"L");
}
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"MONTHY AMORTIZATION: ",0,0,"L");
$pdf->SetFont('Arial','',10);

$pdf->Cell(100,5,number_format(round($amortzation,2),2,'.',','),0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"DUE DATE: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["dueDate"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(150,5,"Agent: ",0,0,"R");
$pdf->SetFont('Times','',10);
$pdf->Cell(55,5,$rowz["agent"],0,1,"L");
        }
        $pdf->SetFont('Times','B',10);
$pdf->Cell(5,5,"#",1,0,"C");
    $pdf->Cell(15,5,"OR #",1,0,"C");

    $pdf->Cell(40,5,"Date of Payment",1,0,"C");
    $pdf->Cell(30,5,"Bank",1,0,"C");
    $pdf->Cell(30,5,"Cash",1,0,"C");
    $pdf->Cell(71,5,"Description",1,1,"C");
    $pdf->SetFont('Times','',10);

         $sql = "SELECT
                    (IF(tbl_clientpaymenthistory.particulars = '', CONCAT('Month of ',DATE_FORMAT(tbl_clientpaymenthistory.dateAdded,'%M %Y')) , tbl_clientpaymenthistory.particulars)) as `particulars`,
                    DATE_FORMAT(tbl_clientpaymenthistory.paymentMade,'%b %e, %Y') as `dateAdded`,
                    tbl_clientpaymenthistory.debit as `propertyDebit`,
                    tbl_clientpaymenthistory.credit as `propertyCredit`,
                    tbl_clientpaymenthistory.description as `description`,
                    tbl_clientpaymenthistory.remittanceCenter as `remittanceCenter`,
                    tbl_clientpaymenthistory.ornumber as `orNumber`
                FROM
                    tbl_clientpaymenthistory
                WHERE
                    tbl_clientpaymenthistory.active = '1'
                AND
                    tbl_clientpaymenthistory.cp_id = '".$_GET['cid']."'
                    ORDER BY tbl_clientpaymenthistory.paymentMade ";
            $result = mysqli_query($conn, $sql);
            $n=0;
            $bank=0;
            $cash=0;
            $total=0;
            $repeat='';
         if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
                 $repeat = strtotime("+1 Month",strtotime($today));


                $total= $total + $row["propertyCredit"] + $row["propertyDebit"];
                if ($row["particulars"]=="Downpayment") {
                if($n==0)
                {
                   $pdf->Cell(5,5,"",0,0,"C");
                }

                $pdf->Cell(15,5,$row["orNumber"],0,0,"C");
                $pdf->Cell(40,5,$row["dateAdded"],0,0,"C");
                $pdf->Cell(30,5,number_format(($row["propertyDebit"]),2,'.',','),0,0,"C");
                $pdf->Cell(30,5,number_format(($row["propertyCredit"]),2,'.',','),0,0,"C");
                   $pdf->CellFitScale(71,5, "(". $row["remittanceCenter"] . ") " . $row["description"],0,1,"C");
                }
                else
                {
                $n++;
                $pdf->Cell(5,5,$n,0,0,"C");
                $pdf->Cell(15,5,$row["orNumber"],0,0,"C");
                $pdf->Cell(40,5,$row["dateAdded"],0,0,"C");
                $pdf->Cell(30,5,number_format(($row["propertyDebit"]),2,'.',','),0,0,"C");
                $pdf->Cell(30,5,number_format(($row["propertyCredit"]),2,'.',','),0,0,"C");
                 $pdf->CellFitScale(71,5, "(". $row["remittanceCenter"] . ") " . $row["description"],0,1,"C");
                }

                  $today = date('Y-m-d',$repeat);
                }
                //  $pdf->Cell(5,5,$n,0,0,"C");
                // $pdf->Cell(15,5,$row["orNumber"],0,0,"C");
                // if ($row["particulars"]=="Downpayment"||($down==0&&$n==1)) {
                // $pdf->Cell(45,5,"Downpayment",0,0,"C");
                // }
                // else
                // {
                // $pdf->Cell(45,5,date("F, Y",strtotime($today)),0,0,"C");
                // }

                // $pdf->Cell(40,5,$row["dateAdded"],0,0,"C");
                // $pdf->Cell(30,5,number_format(($row["propertyDebit"]),2,'.',','),0,0,"C");
                // $pdf->Cell(30,5,number_format(($row["propertyCredit"]),2,'.',','),0,0,"C");
                // $pdf->Cell(26,5,'',0,1,"C");



         } else {
             $pdf->Cell(196,5,"NO PAYMENTS YET!!",1,1,"C");
         }
           $pdf->SetFont('Times','B',10);
         $pdf->Cell(5,5,"",0,0,"C");
    $pdf->Cell(15,5,"",0,0,"C");
    $pdf->Cell(45,5,"",0,0,"C");
    $pdf->Cell(40,5,"",0,0,"C");
    $pdf->Cell(30,5,"",0,0,"C");
    $pdf->Cell(30,5,"",0,0,"C");
    $pdf->Cell(26,5,number_format(round(($price-$total),2),2,'.',','),1,1,"C");
    $pdf->Cell(5,5,"",0,0,"C");
    $pdf->Cell(15,5,"",0,0,"C");
    $pdf->Cell(85,5,"",0,0,"C");
    $pdf->Cell(60,5,"TOTAL PAID: ".number_format(round($total,2),2,'.',',')." "  ,1,0,"C");
    $pdf->Cell(26,5,"" ,0,1,"C");
    $delay = $view-$n;
  if($delay>0 && $full=="0")
    {
      $pdf->Cell(105,5,"",0,0,"L");
      //$pdf->Cell(105,5,"Delayed Payments: ". ($delay) . " months",0,0,"L");
    }
    else
    {
      $pdf->Cell(105,5,"",0,0,"L");
    }
    $pdf->Cell(60,5,"",0,0,"C");
    $pdf->Cell(26,5,"" ,0,1,"C");

         mysqli_close($conn);

$pdf->Output();
?>
