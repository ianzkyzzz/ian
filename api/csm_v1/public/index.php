<?php

ini_set('date.timezone', 'Asia/Singapore');
ini_set('log_errors',1);
ini_set('error_log','logs');

// date_default_timezone_set('Asia/Manila');

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';

// $_SESSION['ipWhiteList'] = $settings['settings']['ipWhiteList'];

$app = new \Slim\App($settings);
// $app->error(function (Exception $e) use ($app) {
//     $app->response->withStatus(200)->withJson(array(
// 		'success'		=> false,
// 		'has_login' 	=> false,
// 		'message'		=> 'Server error!.'
// 	));
// });



// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes
require __DIR__ . '/../src/routes.php';

// Run app
$app->run();
