<?php
/**
 * Created by Raymund Matol
 * Date: 1/31/16
 */

namespace App\Controllers;

use App\Models\Agent;
use App\Models\Commission;
use App\Models\ClientProperty;
use App\Helpers\Validation;
use App\Models\ClientPaymentHistory;
use App\Controllers\ParentController;
/**
 * Class UserController
 * @package App\Controllers
 */
 
class AgentController extends ParentController{

	protected $agent;
	protected $clientProperty;
	protected $commission;
	protected $validation;
	protected $clientPaymentHistory;

	public function __construct(ClientPaymentHistory $clientPaymentHistory, Agent $agent,Commission $commission,ClientProperty $clientProperty, Validation $validation) {

		// User
		$this->agent = $agent;
		$this->clientProperty = $clientProperty;
		$this->commission = $commission;
		$this->clientPaymentHistory = $clientPaymentHistory;

		// Validation Helper
		$this->validation = $validation;

	}
	// $_SESSION['Name'] == DEBONER21 DULOS 
	/**
     * @param $request
     * @param $response
     * @return $response
     */

	// -------------------------------------------------------------------------
    //  User List
    // -------------------------------------------------------------------------
	public function postThisAgent($request, $response, $args) {
		
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}

		$inputs = [
					'agentFname'					=> $request->getParam('agentFname'),
					'agentLname'					=> $request->getParam('agentLname'),
					'agentMname'					=> $request->getParam('agentMname'),
					'agentAddress'					=> $request->getParam('agentAddress'),
					'agentContactNumber'			=> $request->getParam('agentContactNumber'),
					'agentEmail'					=> $request->getParam('agentEmail'),
					'pos'							=> $request->getParam('pos'),
					'higherUps'						=> $request->getParam('higherUps'),
					'accountNumber'					=> $request->getParam('accountNumber'),
				];
		

		$emptyField = [];
		foreach($inputs as $key => $val) {
			if($key == 'higherUps'){
				$pos = $inputs['pos'];
				if(intval($pos) != 1 ){
					if(!isset($val) || $val == ""){
						$emptyField[] = [
							'message'	=> 'This is required.',
							'key'		=> $key,
						];
					}
				}
			}
			else if($key != 'agentEmail'){
				if($val == ''  || !isset($val)){
					$emptyField[] = $key;
				}
			}
		}

		if(count($emptyField) > 0)
			return $this->returnThis($response , ['success'	=> false,'has_login' => true,
				'message'		=> 'Some feilds are invalid/missing.',
				'feilds'		=> $emptyField
				]);

		$agentFname				= $request->getParam('agentFname');
		$agentLname				= $request->getParam('agentLname');
		$agentMname				= $request->getParam('agentMname');
		$agentAddress			= $request->getParam('agentAddress');
		$agentContactNumber		= $request->getParam('agentContactNumber');
		$agentEmail				= $request->getParam('agentEmail');
		$pos					= $request->getParam('pos');
		$higherUps				= $request->getParam('higherUps');
		$accountNumber			= $request->getParam('accountNumber');


		$salesDirectorID  = 0;
		$salesManagerID   = 0;
		$unitManagerID 	  = 0;
		$managerID 		  = 0;

		if($pos == 2){
			$res = $this->agent->getAgentPos('',$higherUps);
			if($res[0]['agentPosition'] != 1){					// validate upline agent Position
				return $this->returnThis($response , ['success'	=> false,'has_login' => true,
					'message'		=> 'Invalid upline agent'
				]);
			}

			$salesDirectorID 	= $higherUps;
			 
		}
		else if($pos == 3){
			$res = $this->agent->getAgentPos(',salesDirectorID,salesManagerID',$higherUps);
			if($res[0]['agentPosition'] != 2){					// validate upline agent Position
				return $this->returnThis($response , ['success'	=> false,'has_login' => true,
					'message'		=> 'Invalid upline agent'
				]);
			}

			$salesDirectorID 	= $res[0]['salesDirectorID'];
			$salesManagerID  	= $higherUps;
			 
		}
		else if($pos == 4){
			$res = $this->agent->getAgentPos(',salesDirectorID,salesManagerID,unitManagerID',$higherUps);
			if($res[0]['agentPosition'] != 3){					// validate upline agent Position
				return $this->returnThis($response , ['success'	=> false,'has_login' => true,
					'message'		=> 'Invalid upline agent'
				]);
			}

			$salesDirectorID 	= $res[0]['salesDirectorID'];
			$salesManagerID  	= $res[0]['salesManagerID'];
			$unitManagerID 		= $higherUps;
			 
		}
		else if($pos == 5){
			$res = $this->agent->getAgentPos(',salesDirectorID,salesManagerID,unitManagerID',$higherUps);
			if($res[0]['agentPosition'] != 4){					// validate upline agent Position
				return $this->returnThis($response , ['success'	=> false,'has_login' => true,
					'message'		=> 'Invalid upline agent'
				]);
			}

			$salesDirectorID 	= $res[0]['salesDirectorID'];
			$salesManagerID  	= $res[0]['salesManagerID'];
			$unitManagerID 		= $res[0]['unitManagerID'];
			$managerID 			= $higherUps;
		}
		 

		// get agent position
		// print_r([$res,[$salesDirectorID,$salesManagerID,$unitManagerID]]);

		$result = $this->agent->insertThisAgent($agentFname,$agentLname,$agentMname,$agentAddress,$agentContactNumber,$agentEmail,$_SESSION['userID'],$pos,$salesDirectorID,$salesManagerID,$unitManagerID, $managerID,$accountNumber);


		if($result)
			return $this->returnThis($response,[
				'success'	=> true,
				'has_login' => true,
				'message'	=> ''.$agentFname.' '.$agentLname.' successfully added.'
			]);
		else
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> ''.$agentFname.' '.$agentLname.' failed to be added.'
			]);
	}


	public function releaseThisComByProperty($request, $response, $args){
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}

		$cp_idAgentProperty  = $request->getParam('id');
		$releaseDatesdAgentProperty  = $request->getParam('dateRelese');
		$ornumberdAgentProperty  = $request->getParam('Ornumber');	

		$checkOrnumber = $this->agent->isRelnumberused($ornumberdAgentProperty);

		if ($checkOrnumber) {
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Relese number already used!' 
			]);
		}


		if ($cp_idAgentProperty == null || $cp_idAgentProperty == "") {
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> "Undefined client property id!" . $cp_idAgentProperty 
			]);
		}

		//inssert into omrelease
		$releaseData = $this->agent->getPropertyReleaseCom($cp_idAgentProperty);
		if (!$releaseData) {
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> "Unable to get Release Data!" 
			]);
		}

		foreach($releaseData as $key => $val) {
			$getProp = $this->agent->getPropertyDetails($val['agentID'], $val['cp_id']);
			if (!$getProp) {
				return $this->returnThis($response,[
					'success'	=> false,
					'has_login' => true,
					'message'	=> "Uablet to get agent prop details!" 
				]);
			}

			$insertInto = $this->agent->ComRelease($val['agentID'], $val['cp_id'], $getProp[0]['PropertyName'], $getProp[0]['originalPrice'], $getProp[0]['amount'], $ornumberdAgentProperty, $releaseDatesdAgentProperty, $_SESSION['userID']);

			if ($insertInto) {
				return $this->returnThis($response,[
					'success'	=> false,
					'has_login' => true,
					'message'	=> "Unable to insert update!" 
				]);
			}

		}

		// get agentID cp_id propertyname contractprice amount releaseNO releaseDate
	
		// update

		$upateComByProp = $this->agent->updateThisComByProppertyList($cp_idAgentProperty);
		if ($upateComByProp) {
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> "Unable to release agent commission!" 
			]);
		}

		return $this->returnThis($response,[
			'success'	=> true,
			'has_login' => true,
			'message'	=> "Release Successfully!"
		]);


	}

	public function getThisCommissionPerProperty($request, $response, $args){
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}
		
		$propID = $request->getParam('propertyCP_ID');
		$getAgentsWithProperty = $this->agent->getThisAgentWithCommission($propID);

		if (!$getAgentsWithProperty) {
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> "List not available!" 
			]);
		}

		$totalPropertyComissionRelease = 0;
		foreach($getAgentsWithProperty as $key => $val) {
			$totalPropertyComissionRelease += $val['3'];
		}

		return $this->returnThis($response,[
			'success'	=> true,
			'has_login' => true,
			'totalCom'  => $totalPropertyComissionRelease,
			'agentList'	=> $getAgentsWithProperty
		]);

	}

	public function getPropertList($request, $response, $args){
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}

		$getPropertyList = $this->agent->getPropertyList();

		if (!$getPropertyList) {
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Unable to fetch property list!'
			]);
		}

		return $this->returnThis($response,[
			'success'	=> true,
			'has_login' => true,
			'propList'  =>  $getPropertyList,
			'message'	=> 'Success!'
		]);

	}


	public function postUpdateThisAgent($request, $response, $args) {
		
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}

		$agentID				= $request->getParam('id');
		$agentFname				= $request->getParam('agentFname');
		$agentLname				= $request->getParam('agentLname');
		$agentMname				= $request->getParam('agentMname');
		$agentAddress			= $request->getParam('agentAddress');
		$agentContactNumber		= $request->getParam('agentContactNumber');
		$agentEmail				= $request->getParam('agentEmail');
		$pos					= $request->getParam('pos');
		$higherUps				= $request->getParam('higherUps');
		$accountNumber			= $request->getParam('accountNumber');

		$salesDirectorID  = "0";
		$salesManagerID  = "0";
		$unitManagerID = "0";
		$managerID = "0";


		$checkAgnetPos = $this->agent->checkAgentPosition($agentID,$pos);
		if ($checkAgnetPos) {
			$result = $this->agent->updatethisAgentInfoWithOuthPosChange($agentFname,$agentLname,$agentMname,$agentAddress,$agentContactNumber,$agentEmail,$accountNumber, $agentID);
		}else{
			if($pos == 2){
				$res = $this->agent->getAgentPos('',$higherUps);
				if($res[0]['agentPosition'] != 1){					// validate upline agent Position
					return $this->returnThis($response , ['success'	=> false,'has_login' => true,
						'message'		=> 'Invalid agent upline'
					]);
				}

				$salesDirectorID 	= $higherUps;
				$salesManagerID  	= 0;
				$unitManagerID 		= 0;
				$managerID = 0;
			}
			else if($pos == 3){
				$res = $this->agent->getAgentPos(',salesDirectorID,salesManagerID',$higherUps);
				if($res[0]['agentPosition'] != 2){					// validate upline agent Position
					return $this->returnThis($response , ['success'	=> false,'has_login' => true,
						'message'		=> 'Invalid agent upline'
					]);
				}

				$salesDirectorID 	= $res[0]['salesDirectorID'];
				$salesManagerID  	= $higherUps;
				$unitManagerID 		= 0;
				$managerID = 0;
			}
			else if($pos == 4){
				$res = $this->agent->getAgentPos(',salesDirectorID,salesManagerID,unitManagerID',$higherUps);
				if($res[0]['agentPosition'] != 3){					// validate upline agent Position
					return $this->returnThis($response , ['success'	=> false,'has_login' => true,
						'message'		=> 'Invalid agent upline'
					]);
				}

				$salesDirectorID 	= $res[0]['salesDirectorID'];
				$salesManagerID  	= $res[0]['salesManagerID'];
				$unitManagerID 		= $higherUps;
				$managerID 			= 0;
			
			}else if($pos == 5){
				$res = $this->agent->getAgentPos(',salesDirectorID,salesManagerID,unitManagerID, managerID',$higherUps);
				if($res[0]['agentPosition'] != 4){					// validate upline agent Position
					return $this->returnThis($response , ['success'	=> false,'has_login' => true,
						'message'		=> 'Invalid agent upline'
					]);
				}

				$salesDirectorID 	= $res[0]['salesDirectorID'];
				$salesManagerID  	= $res[0]['salesManagerID'];
				$unitManagerID 		= $res[0]['unitManagerID'];
				$managerID 			= $higherUps;
			}

			$result = $this->agent->countThisAgentDownline($agentID, $pos);
			if (count($result) <= 0) {
				$result = $this->agent->updateThisAgentInfo($agentFname,$agentLname,$agentMname,$agentAddress,$agentContactNumber,$agentEmail,$pos,$salesDirectorID,$salesManagerID,$unitManagerID, $managerID,$accountNumber,$agentID);
			}else{
				return $this->returnThis($response,[
					'success'	=> false,
					'has_login' => true,
					'message'	=> 'Unable to change agent position. downline agents will be affected'
				]);
			}
		}

		if($result)
			return $this->returnThis($response,[
				'success'	=> true,
				'has_login' => true,
				'message'	=> $agentFname.' '.$agentLname.' successfully updated UserPropfile'
			]);
		else
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> $agentFname.' '.$agentLname.' failed to be upated.'
			]);
	}
 

	public function getAgentPropertyList($request, $response, $args){
		$agentID 			= $request->getParam('id');

		$getAgentProp  		= $this->agent->agentListProperties($agentID);
		$count_prop 		= $this->agent->propCount($agentID);
		$count_client 		= $this->agent->clientCount($agentID);

		if(count($count_prop) <= 0){
			$count_prop = 0;
		}else{
			$count_prop = $count_prop[0]['prop_no'];
		}
		if(count($count_client) <= 0){
			$count_client = 0;
		}else{
			$count_client = $count_client[0]['client_no'];
		}

		if(count($getAgentProp) <= 0){
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> "Unable to find Agent Properties"
			]);
		}else{
			return $this->returnThis($response,[
				'success'	 => true,
				'has_login'  => true,
				'properties' => $getAgentProp,
				'clientCount'=> $count_client,
				'proptCount' => $count_prop

			]);
		}
		
	}
	
	
	public function updateThisAgent($request, $response, $args) {
		
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}
		

		$emptyField = [];
		foreach($request->getParsedBody() as $key => $val) {
			if($key != 'agentEmail')
				if(empty($val)  || !isset($val)){
					$emptyField[] = $key;
				}
		}

		if(count($emptyField) > 0)
			return $this->returnThis($response , ['success'	=> false,'has_login' => true,
				'message'		=> 'Some feilds are missing.',
				'feilds'		=> $emptyField
				]);

		$id						= $request->getParam('id');
		$agentFname				= $request->getParam('agentFname');
		$agentLname				= $request->getParam('agentLname');
		$agentMname				= $request->getParam('agentMname');
		$agentAddress			= $request->getParam('agentAddress');
		$agentContactNumber		= $request->getParam('agentContactNumber');
		$agentEmail				= $request->getParam('agentEmail');
		$accountNumber			= $request->getParam('accountNumber');


		$result = $this->agent->updateThisAgent($agentFname,$agentLname,$agentMname,$agentAddress,$agentContactNumber,$agentEmail,$_SESSION['userID'],$id, $accountNumber);


		if($result)
			return $this->returnThis($response,[
				'success'	=> true,
				'has_login' => true,
				'message'	=> ''.$agentFname.' '.$agentLname.' successfully updated --.'
			]);
		else
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> ''.$agentFname.' '.$agentLname.' failed to be update.'
			]);
	}


	public function releasetThisAgentCommissionPerPrperty($request, $response, $args) {
		
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}
		
		$agentID	  = $request->getParam('agentID');
		$releaseNO    = $request->getParam('comNo');
		$releaseDate  = $request->getParam('releaseDate');
		$addedByUserID = $_SESSION['userID'];


		$getAgentIReleaseNO = $this->agent->isRelnumberused($releaseNO);
		
		if($getAgentIReleaseNO){
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Release Number Already used.'
			]);
		}
		

		$agent = $this->agent->getAgentInfoForThis($agentID);

		if(count($agent) <= 0){
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'agent' => $agent,
				'agentID' => $agentID,
				'message'	=> 'Agent can not be found.'
			]);
		}


		$agentPropetyWithCommision = $this->commission->getThisAgentAllCommisionsToRelease($agentID);


		if(!$agentPropetyWithCommision){
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Commission not yet available'
			]);
		}

		$dateNow = date('Y-m-d h:m:s');


		// $agentPropetyWithCommision = $this->commission->releaseThisAgentAllCommisionsToRelease($agentID,$dateNow);

		// if(!$agentPropetyWithCommision){
		// 	return $this->returnThis($response,[
		// 		'success'	=> false,
		// 		'has_login' => true,
		// 		'message'	=> 'Failed to update records.'
		// 	]);
		// }

		$agentPropetyWithCommision = $this->commission->getThisAgentAllCommisionsReleased($agentID,$dateNow);

		// return $this->returnThis($response,[
		// 	'success'	=> false,
		// 	'has_login' => true,
		// 	'message'	=> $agentPropetyWithCommision
		// ]);



		$results = [];
		$percentageTotal = 0;
		$grossCom = 0;
		$totalTax = 0;
		

		foreach ($agentPropetyWithCommision as $key => $values) {

			$clientProperty  = $this->clientProperty->getAllPropertyByClientOwnerID($values['cp_id']);

			if(count($clientProperty) <= 0){
				continue;
			}		

			$data=[];

			$contractPrice 			= floatval($clientProperty[0]['contractPrice']);
			$monthlyAmortization 	= floatval($clientProperty[0]['monthlyAmortization']);

			$sqmPricem2 			= floatval($clientProperty[0]['sqmPricem2']);
			$sqm 					= floatval($clientProperty[0]['sqm']);
			$plan_terms 			= intval($clientProperty[0]['plan_terms']);

			$additionalCharges 		= $clientProperty[0]['additionalCharges'];
			$comRelease 			= $clientProperty[0]['commissionReleased'];
			$adCom 					= $clientProperty[0]['adCom'];
			$payment 				= (string) ($values['released'] + 1)  . ' / ' . (int) $comRelease;


			if(!empty($additionalCharges)){
				$additionalCharges = json_decode($clientProperty[0]['additionalCharges']);
				if(count($additionalCharges) != 0){
					$totalChargeValue = 0;
					foreach ($additionalCharges as $key => $value) {
						$totalChargeValue += $value->chargeValue;
					}

					$contractPrice = (($sqmPricem2 * $sqm));
					$monthlyAmortization = ((($sqmPricem2 * $sqm)) / $plan_terms);
				}
			}

			$totalGrossCom = ($contractPrice * floatval(($values['percentagevalue']  / 100) / $comRelease));
			

			$insertInto = $this->commission->insertIntoComRelease($agentID, $values['cp_id'], $values['client_id'],  $values['propertyName'], $contractPrice, ($totalGrossCom - ($totalGrossCom * 0.05)) , $releaseNO, $releaseDate, $addedByUserID, $payment);
			
			if(!$insertInto){
				return $this->returnThis($response,[
					'success'	=> false,
					'has_login' => true,
					'message'	=> 'Failed to update records.'
				]);	
			}	

			if(intval($values['percentagevalue']) < 15){
				$values['percentagevalue'] = '0'.intval($values['percentagevalue']);
			}
			else
				$values['percentagevalue'] = intval($values['percentagevalue']);

			$percentageTotal = $percentageTotal + ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comRelease ) - ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comRelease ) * 0.05));
			$grossCom 		 = $grossCom + ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comRelease ));
			$totalTax 		 = $totalTax + ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comRelease ) * 0.05);
			// $percentageTotal = $percentageTotal + (((($contractPrice * 0.15 * .05) - $adCom )/ $comRelease ) / 0.15 * floatval(('0.'.$values['percentagevalue'])));

			$results[] = [
				// 'clientID' => $values['cp_id'],
				'clienName' 		=> $values['clientName'],
				'propertyName' 		=> $values['propertyName'] .'( '.intval($values['percentagevalue']).'% )',
				'contractPrice'	    => $contractPrice,
				'payment' 			=> $payment,
				'percentageValue'   => number_format( ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comRelease ) - ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comRelease ) * 0.05)), 2),
				'dateRelease'       => $values['dateNow']
			];	
		}

		
	    $getClientID = $this->commission->getAgentCpID($agentID);
			
			foreach ($getClientID as $key => $values) {
				$updteCom = $this->commission->getUpdateCom($values['cp_id'], $values['commissionReleased'], $agentID);
				
				if(!$updteCom){
					return $this->returnThis($response,[
						'success'	=> false,
						'has_login' => true,
						'message'	=> 'Failed to update records.'
					]);
				}


				if($values['fullyPaid'] == 1){
				}else{
					$updatePayents = $this->commission->getUpdatePayments($values['cp_id'], $agentID);
					
					if(!$updatePayents){
						return $this->returnThis($response,[
							'success'	=> false,
							'has_login' => true,
							'message'	=> 'Failed to update records.'
						]);
					
					}	
				}
			}


		$getClientID_2 = $this->commission->getAgentCpID($agentID);

			foreach ($getClientID_2 as $key => $values) {
				if ($values['released'] == $values['commissionReleased']) {
					$updteCom_2 = $this->commission->getUpdateCom_2($values['cp_id'], $values['commissionReleased'], $agentID);
				
					if(!$updteCom_2){
						return $this->returnThis($response,[
							'success'	=> false,
							'has_login' => true,
							'message'	=> 'Failed to update records.'
						]);	
					}		
				}
			}
		
		return $this->returnThis($response,[
				'success'		    => true,
				'has_login' 		=> true,
				'results'			=> $results	,
				'percentageTotal'   => $percentageTotal,
				'gross'				=> $grossCom,
				'wtx'				=> $totalTax
			]);

	}

	public function getThisAgentOptionalPropertyRelease($request, $response, $args)
	{

		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message' => 'You are not login']);
		}
		
		$agentID = $request->getParam('agentID');
		$agent = $this->agent->getAgentInfoForThis($agentID);
		$relNo = $this->agent->getRelNumber();


		if(count($agent) <= 0){
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'agent' 	=> $agent,
				'agentID'   => $agentID,
				'message'	=> 'Agent can not be found.'
			]);
		}


		$agentPropetyWithCommision = $this->commission->getThisAgentAllCommisionsToRelease($agentID);
		if(count($agentPropetyWithCommision) <= 0){
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Commission not yet available'
			]);
		}

		$results = [];
		$optionalRel = [];
		$percentageTotal = 0;
		$grossCom = 0;
		$totalTax = 0;

		foreach ($agentPropetyWithCommision as $key => $values) {
			$clientProperty 	= $this->clientProperty->getAllPropertyByClientOwnerID($values['cp_id']);

			if(count($clientProperty) <= 0){
				continue;
			}		

			$data=[];

			$contractPrice 			= floatval($clientProperty[0]['contractPrice']);
			$monthlyAmortization 	= floatval($clientProperty[0]['monthlyAmortization']);

			$sqmPricem2 			= floatval($clientProperty[0]['sqmPricem2']);
			$sqm 					= floatval($clientProperty[0]['sqm']);
			$plan_terms 			= intval($clientProperty[0]['plan_terms']);

			$additionalCharges 		= $clientProperty[0]['additionalCharges'];

			$comReleased 		    = $clientProperty[0]['commissionReleased'];
			$adCom 		   			= $clientProperty[0]['adCom'];
			$payment 				= (string) ($values['released'] + 1)  . ' / ' . (int) $comReleased;


			if($additionalCharges != 'null'){	

				if(count($clientProperty[0]['additionalCharges']) != 0){
					$additionalCharges = json_decode($clientProperty[0]['additionalCharges']);
					
					$totalChargeValue = 0;
					foreach ($additionalCharges as $key => $value) {
						$totalChargeValue += $value->chargeValue;
					}

					$contractPrice = (($sqmPricem2 * $sqm));
					$monthlyAmortization = ((($sqmPricem2 * $sqm)) / $plan_terms);
				}
			}


			if(intval($values['percentagevalue']) < 15){
				$values['percentagevalue'] = '0'.intval($values['percentagevalue']);
			}
			else{
				$values['percentagevalue'] = intval($values['percentagevalue']);
			}

			$percentageTotal = $percentageTotal + ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comReleased ) - ((($contractPrice * floatval(($values['percentagevalue'] / 100))) / $comReleased ) * 0.05));
			$grossCom 		 = $grossCom + ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comReleased ));
			$totalTax 		 = $totalTax + ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comReleased )* 0.05);
			$amount 		 = ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comReleased ) - ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comReleased ) * 0.05));

			$results[] = [
				0 => "<input type='checkbox' class='icheck ck_properyID' checked data-id='". $values['cp_id'] ."' data-value='". $amount ."'>",
				1 => $values['clientName'],
				2 => $values['propertyName'] .'( '.intval($values['percentagevalue']).'% )',
				3 => $contractPrice,
				4 => $payment,
				5 => number_format($amount, 2),
				6 => $values['mode'],
				7 => (int) $comReleased,
				8 => $values['released']
				// 4 => number_format($getCommission[0]['amount'], 2, '.', ','),
			];
		}

		return $this->returnThis($response,[
			'success'			=> true,
			'has_login'	 		=> true,
			'results'			=> $results,
			'percentageTotal'	=> $percentageTotal,
			'gross'				=> $grossCom,
			'wtx'				=> $totalTax,
			'relnum'			=> (int) $relNo[0]['releaseNO'] + 1
		]);
		
	}

	


	// TABLE FOR AGENT COM RELEASing
	public function getThisAgentCommissionPerPrperty($request, $response, $args){		
		
		if(!$this->isLogin()){
		return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message' => 'You are not login']);
	
	}		
	
	$agentID = $request->getParam('agentID');
	$agent = $this->agent->getAgentInfoForThis($agentID);
	$relNo = $this->agent->getRelNumber();		if(count($agent) <= 0){
		return $this->returnThis($response,[
			'success'	=> false,
			'has_login' => true,
			'agent' 	=> $agent,
			'agentID'   => $agentID,
			'message'	=> 'Agent can not be found.'
		]);
	}		
	$agentPropetyWithCommision = $this->commission->getThisAgentAllCommisionsToRelease($agentID);
	if(count($agentPropetyWithCommision) <= 0){
		return $this->returnThis($response,[
			'success'	=> false,
			'has_login' => true,
			'message'	=> 'Commission not yet available'
		]);
	}		
	$results = [];
	$optionalRel = [];
	$percentageTotal = 0;
	$grossCom = 0;
	
	// $contractPrice = 0.00;
	$totalTax = 0;		
	foreach ($agentPropetyWithCommision as $key => $values) {
		$clientProperty 	= $this->clientProperty->getAllPropertyByClientOwnerID($values['cp_id']);			
		if(count($clientProperty) <= 0){
			continue;
		}					// $data=[];			$contractPrice 			= floatval($clientProperty[0]['contractPrice']);
		$monthlyAmortization 	= floatval($clientProperty[0]['monthlyAmortization']);			
		$sqmPricem2 			= floatval($clientProperty[0]['sqmPricem2']);
		$sqm 					= floatval($clientProperty[0]['sqm']);
		$plan_terms 			= intval($clientProperty[0]['plan_terms']);			
		$additionalCharges 		= $clientProperty[0]['additionalCharges'];			
		$comReleased 		    = $clientProperty[0]['commissionReleased'];
		$adCom 		   			= $clientProperty[0]['adCom'];
		$payment 				= (string) ($values['released'])+1  . ' / ' . (int) $comReleased;
		$contractPrice			= floatval($clientProperty[0]['contractPrice']);
		$comReleased;			
		if($additionalCharges != 'null'){				
			if(count($clientProperty[0]['additionalCharges']) != 0){
				$additionalCharges = json_decode($clientProperty[0]['additionalCharges']);					
				$totalChargeValue = 0;
				foreach ($additionalCharges as $key => $value) {
					$totalChargeValue += $value->chargeValue;
				}					
				// $contractPrice = (($sqmPricem2 * $sqm));
				$monthlyAmortization = ((($sqmPricem2 * $sqm)) / $plan_terms);
			}
		}			
		if(intval($values['percentagevalue']) < 15){
			$values['percentagevalue'] = '0'.intval($values['percentagevalue']);
		}
		else{
			$values['percentagevalue'] = intval($values['percentagevalue']);
		}			
		$percentageTotal = $percentageTotal + ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comReleased ) - ((($contractPrice * floatval(($values['percentagevalue'] / 100))) / $comReleased ) * 0.05));
		$grossCom 		 = $grossCom + ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comReleased ));
		$totalTax 		 = $totalTax + ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comReleased )* 0.05);			
		$testinglang     = $this->commission->testinglang($values['cp_id'])[0];
		$results[] = [
			0 => $values['cp_id'],
			1 => $values['clientName'],
			2 => $values['propertyName'] .'( '.intval($values['percentagevalue']).'% )',
			3 => $contractPrice,
			4 => $payment,
			5 => number_format( ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comReleased ) - ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comReleased ) * 0.05)), 2),
			6 => ($testinglang['remittanceCenter'] == '' ? 'Cash' : $testinglang['remittanceCenter']),
			7 => (int) $comReleased,
			8 => $values['released']
			// 4 => number_format($getCommission[0]['amount'], 2, '.', ','),
		];		}		
		return $this->returnThis($response,[
		'success'			=> true,
		'has_login'	 		=> true,
		'results'			=> $results,
		'percentageTotal'	=> $percentageTotal,
		'gross'				=> $grossCom,
		'wtx'				=> $totalTax,
		'relnum'			=> (int) $relNo[0]['releaseNO'] + 1
		]);
	}


	public function removeThisAgent($request, $response, $args) {
		
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}
		

		$emptyField = [];
		foreach($request->getParsedBody() as $key => $val) {
			if($key != 'agentEmail')
				if(empty($val)  || !isset($val)){
					$emptyField[] = $key;
				}
		}

		if(count($emptyField) > 0)
			return $this->returnThis($response , ['success'	=> false,'has_login' => true,
				'message'		=> 'Some feilds are missing.',
				'feilds'		=> $emptyField
				]);

		$id						= $request->getParam('id');
		


		$result = $this->agent->removeThisAgent($id);

		if($result)
			return $this->returnThis($response,[
				'success'	=> true,
				'has_login' => true,
				'message'	=> 'Successfully remove agent.'
			]);
		else
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Failed to be remove agent.'
			]);
	}


	public function getAgentNames($request, $response, $args){
		$role = $_SESSION["UserRole"];
		$userID = $_SESSION["userID"];

		if($role == "Agent"){
			$agentNames = $this->agent->getSpecificAgent($userID);
		}else{
			$agentNames = $this->agent->getAllAgentsNameList();
		}
		
		$relNo = $this->agent->getRelNumber();
		if ($relNo) {
			 if(count($agentNames) > 0)
				return $this->returnThis($response,[
					'success'	=> true,
					'has_login' => true,
					'message'   => '',
					'agents'	=> $agentNames,
					'number'	=> $relNo[0]['releaseNO'],
					'role'		=> $role
				]);
			else
				return $this->returnThis($response,[
					'success'	=> true,
					'has_login' => true,
					'message'   => 'No results.',
					'agents'	=> $agentNames,
					'number'	=> $relNo[0]['releaseNO'],
					'role'		=> $role
				]);
		}else{
			
			if(count($agentNames) > 0)
				return $this->returnThis($response,[
					'success'	=> true,
					'has_login' => true,
					'message'   => '',
					'agents'	=> $agentNames,
					'number'	=> 0,
					'role'		=> $role
				]);
			else
				return $this->returnThis($response,[
					'success'	=> false,
					'has_login' => true,
					'message'   => 'No results.',
					'agents'	=> $agentNames,
					'number'	=> 0,
					'role'		=> $role
				]);
		}

		
	}


	public function getAllAgents($request, $response, $args) {
		
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}
		
		$role = $_SESSION['UserRole'];
		$userID = $_SESSION['userID'];

		$agents = $this->agent->getAllAgents($role, $userID);

		if(count($agents) > 0)
			return $this->returnThis($response,[
				'success'	=> true,
				'has_login' => true,
				'message'   => '',
				'agents'	=> $agents
			]);
		else
			return $this->returnThis($response,[
				'success'	=> true,
				'has_login' => true,
				'message'   => 'No results.',
				'agents'	=> $agents
			]);
	}




	public function getListOfAgentHigherThanThisPosition($request, $response, $args) {

		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message' => 'You are not login']);
		}
		

		$pos = $request->getParam('position');

		if($pos == "" || !($pos >= 1 && $pos <= 5)){
			return $this->returnThis($response , ['success'	=> false,'has_login' => true,'message'		=> 'Invalid agent position.']);
		}


		$agents = [];
		$higherPositionName = '';

		if($pos == 2){
			$agents = $this->agent->getAllAgentsHigherThanThisPos('agentPosition',1);
			$higherPositionName = 'Sales Director Head';
		}
		else if($pos == 3){
			$agents = $this->agent->getAllAgentsHigherThanThisPos('agentPosition',2);	
			$higherPositionName = 'Sales Director';
		}
		else if($pos == 4){
			$agents = $this->agent->getAllAgentsHigherThanThisPos('agentPosition',3);
			$higherPositionName = 'Unit Manager';
		}
		else if($pos == 5){
			$agents = $this->agent->getAllAgentsHigherThanThisPos('agentPosition',4);
			$higherPositionName = 'Manager';
		}
		 

		if(count($agents) != 0)
			return $this->returnThis($response,[
					'success'				=> true,
					'has_login' 			=> true,
					'message'   			=> '',
					'agents'				=> $agents,
					'higherPositionName' 	=> $higherPositionName
				]);
		else
			return $this->returnThis($response,[
					'success'	=> false,
					'has_login' => true,
					'message'   => 'There has no upline agent position for this.',
				]);
	}
	public function checkAgentUsername($userID)
	{
		$status = "none";
		$checkUser = $this->agent->checkAgentUsername($userID);
		if ($checkUser) {
			$status = $checkUser[0]['username'];
		}	
		return $status;	
	}



	public function getThisAgentInfo($request, $response, $args) {
		
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}

		$id = $request->getParam('id');
		$role = $_SESSION['UserRole'];

		$results = $this->clientProperty->getAllPropertyByAgentID($id);
		$getComRelease = $this->agent->getComRelease($id);

		$agent = $this->agent->getAgentInfoForThis($id);

		if(count($agent) == 1)
			return $this->returnThis($response,[
				'success'	        => true,
				'has_login'         => true,
				'data'	            => $agent,
				'comReleaseTable'	=> $getComRelease,
				'role'				=> $role,
				'username'			=> $this->checkAgentUsername($id)
			]);
		else
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Invalid agent ID.'
			]);
	}


	public function uploadThisImage($request, $response, $args){
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}
		// return $this->returnThis($response,[
		// 	'datas'			=> basename($_FILES['file_uploads']['name'])
		// ]);

		

		// $success = true;
		try {
			$agentID = trim($request->getParam('dataID'));
			if($agentID == ""){
				return $this->returnThis($response,[
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Agent ID is must be provided.'
				]);
			}


			$agent = $this->agent->getAgentInfoForThis($agentID);
			if(count($agent) != 1){
				return $this->returnThis($response,[
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Invalid Agent ID.'
				]);
			}


			// if($target_file != ""){
			$upload_errors = array(
				UPLOAD_ERR_OK => "No Errors",
				UPLOAD_ERR_INI_SIZE => "Larger than upload max_filesize.",
				UPLOAD_ERR_FORM_SIZE => "Larger than FORM_MAX_FILE_SIZE.",
				UPLOAD_ERR_PARTIAL => "Partial Upload.",
				UPLOAD_ERR_NO_FILE => "No File.",
				UPLOAD_ERR_NO_TMP_DIR => "No Temporary Directory.",
				UPLOAD_ERR_CANT_WRITE => "Cant write to disk.",
				UPLOAD_ERR_EXTENSION => "File upload stopped by extension."
			);	
			$tmp_file = $_FILES['file_uploads']['tmp_name'];
			$target_file = basename($_FILES['file_uploads']['name']);// original filename
			$ext = pathinfo($target_file, PATHINFO_EXTENSION);

			$target_file = $agentID.'.'.$ext;

			$upload_dir = 'uploads/agent/'.$agentID;
			$itemurls =  "uploads/agent/".ucwords($target_file);

		
			if(!is_dir($upload_dir)) {
	            mkdir($upload_dir, 0777, true);
	        }

	        if ( !is_writable($upload_dir)) {
			    return $this->returnThis($response,[
						'success'		=> false,
						'has_login' 	=> true,
						'message'		=> 'File directory is not writable.'
					]);
			}


	
			if(!move_uploaded_file($tmp_file, $upload_dir."/".$target_file)){
				return $this->returnThis($response,[
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Failed to upload image.'.$_FILES["file_uploads"]["error"]
				]);
			}


			$datas = $this->agent->updateAgentImagePath($target_file,$agentID);

			if($datas)
				return $this->returnThis($response,[
					'success'		=> true,
					'has_login' 	=> true,
					'message'		=> 'Upload successfull.'
				]);
			else
				return $this->returnThis($response,[
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Failed to save image path to database.'
				]);

		} catch (Exception $e) {
			return $this->returnThis($response,[
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Failed to upload image. '.json_encode($e)
			]);
		}
	}
				 
	public function getComByDate($request, $response, $args){
		$id = $request->getParam('id');
		$dateFrom = $request->getParam('dateFrom');
		$dateTo = $request->getParam('dateTo');

		$agent = $this->agent->getComReleasebyDate($id, $dateFrom, $dateTo);

		if($agent){
			return $this->returnThis($response,[
				'success'	        => true,
				'has_login'         => true,
				'comReleaseTable'	=> $agent
			]);
		}else{
			return $this->returnThis($response,[
				'success'	        => false,
				'has_login'         => true,
				'message'	=> 'Unable to find search Date!'
			]);
		}
	}


	public function getAllByPropertyName($request, $response, $args)
	{
		
		$getAllPropUnderPay = $this->agent->getAllPropertyUnderPayment();

		if (!$getAllPropUnderPay) {
			return $this->returnThis($response,[
				'success'	        => false,
				'has_login'         => true,
				'message'	=> 'Property list unavailable!'
			]);
		}

		return $this->returnThis($response,[
			'success'	        => true,
			'has_login'         => true,
			'properlist'		=> $getAllPropUnderPay
		]);
	}

	public function loadAgentsUnderSelectedProperty($request, $response, $args)
	{
		$anotherID = $request->getParam('parentID');
		 
		$getPropertyUnderParent = $this->agent->SampleDaw($anotherID);
		if (!$getPropertyUnderParent) {
			return $this->returnThis($response,[
				'success'	        => false,
				'has_login'         => true,
				'message'			=> 'Unable to get prop detail!'
			]);	
		}	

		$agentList = [];
		$total = 0;
		
		foreach ($getPropertyUnderParent as $key => $value) {	 	


		 	$getCommission  = $this->agent->getAgentComDetails($value['cp_id'], $value['agentID'], $value['percentagevalue']);
		 	if (!empty($getCommission[0]['agentName'])) {
		 			$agentList[] =  [
				 		0 => $getCommission[0]['agentName'],
				 		1 => $getCommission[0]['propertyName'],
				 		2 => $getCommission[0]['phaseNumber'],
				 		3 => $getCommission[0]['blocl'],
				 		4 => $getCommission[0]['lot'],
				 		5 => $value['percentagevalue'],
				 		6 => $getCommission[0]['agentPos'],
				 		// 7 => $getCommission[0]['amount']
				 		7 => number_format($getCommission[0]['amount'], 2, '.', ',')
				 	];

				 	$total +=  $getCommission[0]['amount'];
		 	}
		 }

		return $this->returnThis($response,[
				'success'	        => true,
				'has_login'         => true,
				'list'				=> $agentList,
				'total'             => $total
 			]);	
	}

	public function ReleaseAllComUnderThisProperty($request, $response, $args)
	{
		
		$relDate 	= $request->getParam('dateRel');
		$relNum  	= $request->getParam('releaseNum');
		$propID  	= $request->getParam('propParentID');

		if (empty($relDate) || empty($relNum) || empty($propID) ) {
			return $this->returnThis($response,[
				'success'	        => false,
				'has_login'         => true,
				'message'			=> 'Some fields are empty!'
			]);	
		}

		$checkOrnumber = $this->agent->isRelnumberused($relNum);
		if ($checkOrnumber) {
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Release number already used!' 
			]);
		}

		$getPropertyUnderParent = $this->agent->SampleDaw($propID);
		if (!$getPropertyUnderParent) {
			return $this->returnThis($response,[
				'success'	        => false,
				'has_login'         => true,
				'message'			=> 'Unable to get prop detail!'
			]);	
		}	

		foreach ($getPropertyUnderParent as $key => $value) {	 	
		 	$getCommission  = $this->agent->getAgentComDetails($value['cp_id'], $value['agentID'], $value['percentagevalue']);
		 	 if (!$getCommission) {
		 	 	return $this->returnThis($response,[
					'success'	        => false,
					'has_login'         => true,
					'message'			=> 'Unable to getComDetails!'
				]);
		 	 }

		 	$getOriginalPrice = $this->agent->getOriginalPropertyPrice($value['cp_id']);
		 	if (!$getOriginalPrice) {
		 		return $this->returnThis($response,[
					'success'	        => false,
					'has_login'         => true,
					'message'			=> 'Unable to getOriginalPrice!'
				]);
		 	}

		 	$propertyName = $getCommission[0]['propertyName'] . ' Ph ' . $getCommission[0]['phaseNumber'] . ' Blk ' . $getCommission[0]['blocl'] . ' Lt ' . $getCommission[0]['lot'];
		 	$insertToCom = $this->agent->ComRelease($value['agentID'], $value['cp_id'], $propertyName, $getOriginalPrice[0]['originalPrice'], $getCommission[0]['amount'], $relNum, $relDate, $_SESSION['userID'], $value['client_id']);
		 	if ($insertToCom) {
				return $this->returnThis($response,[
					'success'	=> false,
					'has_login' => true,
					'message'	=> "Unable to insert update!" 
				]);
			}

			$upateComByProp = $this->agent->updateThisComByProppertyList($value['cp_id']);
			if ($upateComByProp) {
				return $this->returnThis($response,[
					'success'	=> false,
					'has_login' => true,
					'message'	=> "Unable to update upateComByProp!" 
				]);
			}

		// end foreach
		}
		//============

		return $this->returnThis($response,[
			'success'	        => true,
			'has_login'         => true,
			// 'message'			=>  is_null($relDate) . ':' . $relDate . ',' . is_null($relNum) . ':' . $relNum 
			'message'			=>  'Commission Successfully Released!'
		]);	

	}

// comSummary
	public function getComRelSummary($request, $response, $args)
	{
		$agentList = $this->agent->getAgentComRelSum();
		$comSummary= [];
		$Summary =[];
		foreach ($agentList as $key => $value) {
			$getComRelSum = $this->clientPaymentHistory->getClientPropertiesCom3($value['cp_id']);
			// if (!$getComRelSum) {
			// 	return $this->returnThis($response,[
			// 		'success'	        => false,
			// 		'has_login'         => true
			// 	]);	
			// }

			$tatalCom ="";
			foreach ($getComRelSum as $key => $val) {
				if (is_null($value['percentagevalue'])) {
					$Summary[] = [
						'0' => $value['id'],
						'1' => ((($val['TCP'] * ($value['percentagevalue'] / 100))/ $val['CRS']) - ((($val['TCP'] * ($value['percentagevalue'] / 100))/ $val['CRS']) * 0.05))
					];	
				}
			}
		}

		$agentSummary =[];
		$totalSum = 0;
		$getAgentCom = $this->agent->getAgentComRelSum2();
		foreach ($getAgentCom as $key => $val) {
			foreach ($Summary as $key => $val2) {
			 	if ($val2['0'] == $val['id']) {
			 		$totalSum += $val2['1'];
			 	}
			}

			$agentSummary[] = [
				'0' => $val['AccountNumber'],
				'1' => $val['AgentName'],
				'2' => number_format($totalSum,2)
			];

			$totalSum = 0;

		}

		// echo "<pre>";
		// print_r($Summary);

		return $this->returnThis($response,[
			'success'	 => true,
			'has_login'  => true,
			'ComSum' 	 => $agentSummary	
		]);	
	}

	public function getSumComDetails($request, $response, $args)
	{
		
		$getClientProperties = $this->clientPaymentHistory->getClientPropertiesCom();
		$comSummary = [];
		$data = [];
		if ($getClientProperties) {
		 	foreach($getClientProperties as $value){
				$agent_1 = "";
				$agent_2 = "";
				$agent_3 = "";
				$agent_4 = "";
				$sd = "";
				$totalCom ="";  

				$agent1_com ="";
				$agent2_com ="";
				$agent3_com ="";
				$agent4_com ="";
				$agent5_com ="";

				$Agent1 = $this->agent->getAgent1($value['cpID']);
				$Agent2 = $this->agent->getAgent2($value['cpID']);
				$Agent3 = $this->agent->getAgent3($value['cpID']);
				$Agent4 = $this->agent->getAgent4($value['cpID']);
				$SD 	= $this->agent->getSD($value['cpID']);
				
				foreach($Agent1 as $value_1){
 					$agent_1 =  $value_1['agentName']. ' ' . $value_1['percentageVal']. ' ' . number_format($value_1['Agent 1'],2);
 					$agent1_com = $value_1['Com'];
				}

				foreach($Agent2 as $value_2){
 					$agent_2 = $value_2['agentName']. ' ' . $value_2['percentageVal']. ' ' . number_format($value_2['Agent 2'],2);
 					$agent2_com = $value_2['Com'];
				}

				foreach($Agent3 as $value_3){
 					$agent_3 = $value_3['agentName']. ' ' . $value_3['percentageVal']. ' ' . number_format($value_3['Agent 3'],2);
 					$agent3_com 	=$value_3['Com'];
				}

				foreach($Agent4 as $val4){
 					$agent_4 = $val4['agentName']. ' ' . $val4['percentageVal']. ' ' . number_format($val4['Agent 4'],2);
 					$agent5_com = $val4['Com'];
				}


				foreach($SD as $value_4){
 					$sd = $value_4['agentName'] . ' ' . $value_4['percentageVal']. ' ' . number_format($value_4['SD'],2);
 					$agent4_com =$value_4['Com'];
				}

				// $data = [];
				$totalCom = ($agent1_com + $agent2_com + $agent3_com + $agent4_com + $agent5_com);
				$data [] = [
					'0'  => $value['Buyer'],
					'1'  => $value['P'],
					'2'  => $value['Blk'],
					'3'  => $value['Lt'],
					'4'  => $value['TCP'],
					'5'  => $value['addCom'],
					'6'  => $value['CRS'],
					'7'  => $agent_1, 
					'8'  => $agent_2,
					'9'  => $agent_3,
					'10' => $agent_4,
					'11' => $sd,
					'12' => number_format($totalCom,2)
				];
				
			}

		}

		// $thisPpropName = $this->clientPaymentHistory->getPropeprtyList();

		return $this->returnThis($response,[
			'success'	 => true,
			'has_login'  => true,
			'ComDetails' => $data	
		]);
	}


	public function selectThisProperty($request, $response, $args)
	{
		$thisParentID =  $request->getParam('parentID');

		if ($thisParentID == 0 ) {
			$getClientProperties = $this->clientPaymentHistory->getClientPropertiesCom();
		}else{
			$getClientProperties = $this->clientPaymentHistory->getClientPropertiesCom2($thisParentID);
		}
					   
		$comSummary = [];
		$data= [];
		if ($getClientProperties) {
		 	foreach($getClientProperties as $value){
				$agent_1 = "";
				$agent_2 = "";
				$agent_3 = "";
				$agent_4 = "";
				$sd = "";
				$totalCom ="";  

				$agent1_com ="";
				$agent2_com ="";
				$agent3_com ="";
				$agent4_com ="";
				$agent5_com ="";

				$Agent1 = $this->agent->getAgent1($value['cpID']);
				$Agent2 = $this->agent->getAgent2($value['cpID']);
				$Agent3 = $this->agent->getAgent3($value['cpID']);
				$Agent4 = $this->agent->getAgent4($value['cpID']);
				$SD 	= $this->agent->getSD($value['cpID']);
				
				foreach($Agent1 as $value_1){
 					$agent_1 =  $value_1['agentName']. ' ' . $value_1['percentageVal']. ' ' . number_format($value_1['Agent 1'],2);
 					$agent1_com = $value_1['Com'];
				}

				foreach($Agent2 as $value_2){
 					$agent_2 = $value_2['agentName']. ' ' . $value_2['percentageVal']. ' ' . number_format($value_2['Agent 2'],2);
 					$agent2_com = $value_2['Com'];
				}

				foreach($Agent3 as $value_3){
 					$agent_3 = $value_3['agentName']. ' ' . $value_3['percentageVal']. ' ' . number_format($value_3['Agent 3'],2);
 					$agent3_com 	=$value_3['Com'];
				}

				foreach($Agent4 as $value_4){
 					$agent_4 = $value_4['agentName']. ' ' . $value_4['percentageVal']. ' ' . number_format($value_4['Agent 4'],2);
 					$agent5_com  =$value_4['Com'];
				}

				foreach($SD as $value_4){
 					$sd = $value_4['agentName'] . ' ' . $value_4['percentageVal']. ' ' . number_format($value_4['SD'],2);
 					$agent4_com =$value_4['Com'];
				}

				// $data = [];
				$totalCom = ($agent1_com + $agent2_com + $agent3_com + $agent5_com + $agent4_com);
				$data [] = [
					'0'  => $value['Buyer'],
					'1'  => $value['P'],
					'2'  => $value['Blk'],
					'3'  => $value['Lt'],
					'4'  => $value['TCP'],
					'5'  => $value['addCom'],
					'6'  => $value['CRS'],
					'7'  => $agent_1, 
					'8'  => $agent_2,
					'9'  => $agent_3,
					'10' => $agent_4,
					'11' => $sd,
					'12' => number_format($totalCom,2)
				];	
			}
		}

		return $this->returnThis($response,[
			'success'	 => true,
			'has_login'  => true,
			'data' 	     => $data	
		]);

	}	

	public function releaseThisComListOverride($request, $response, $args)
	{
	 	$thisParentID =  $request->getParam('parentID');
	 	$thisNumber =  $request->getParam('relNumber');
	 	$thisDate =  $request->getParam('relDate');


	 // 	$checkOrnumber = $this->agent->isRelnumberused($thisNumber);
		// if ($checkOrnumber) {
		// 	return $this->returnThis($response,[
		// 		'success'	=> false,
		// 		'has_login' => true,
		// 		'message'	=> 'Release number already used!' 
		// 	]);
		// }

		if ($thisParentID  == 0 ) {
			$getClientProperties = $this->clientPaymentHistory->getClientPropertiesCom();
		}else{
			$getClientProperties = $this->clientPaymentHistory->getClientPropertiesCom2($thisParentID);
		}

		$comSummary = [];
		if ($getClientProperties) {
		 	foreach($getClientProperties as $value){

				$propertyName = $value['Property Name'] . ' P ' . $value['P'] .' Blk ' . $value['Blk']. ' Lt ' . $value['Lt'];	 

				$getAgetNameFromComrelease = $this->commission->getAgents($value['cpID']);
				foreach($getAgetNameFromComrelease as $key => $val) {
					$totalGrossCom = (($value['TCP'] * ($val['percentagevalue'] / 100)) / $value['CRS']);
					$insertInto = $this->commission->insertIntoComRelease($val['agentID'], $value['cpID'], $value['client ID'], $propertyName, $value['TCP'], ($totalGrossCom - ($totalGrossCom * 0.05 )), $thisNumber, $thisDate, $_SESSION['userID'], $value['payment']);
				
						if(!$insertInto){
							return $this->returnThis($response,[
								'success'	=> false,
								'has_login' => true,
								'message'	=> 'Failed to update records.'
							]);	
						}	
				}

				// update use client paymenthistory  set releseStat to 1
	 			$updateThisProperty = $this->clientPaymentHistory->updateReleseStat($value['id']);
					if(!$updateThisProperty){
						return $this->returnThis($response,[
							'success'	=> false,
							'has_login' => true,
							'message'	=> 'unable to udpate this clientPaymentHistory'
						]);	
					}	
			
				}

				$data [] = [
					'0'  => $value['cpID'],
				];	
		}

		foreach ($data as $key => $val2) {
			// update tblCommissions
			$thisComId = $this->commission->updateComreleaseNo($val2['0']);
			if(!$thisComId){
				return $this->returnThis($response,[
					'success'	=> false,
					'has_login' => true,
					'message'	=> 'unable to udpate this tbl_Commissions'
				]);	
			}	

		}

		return $this->returnThis($response,[
			'success'	 => true,
			'has_login'  => true,
			'messages' 	 => 'Successfully updated!'	
		]);
	}

	public function releaseThisComList($request, $response, $args)
	{
	 	$thisParentID =  $request->getParam('parentID');
	 	$thisNumber =  $request->getParam('relNumber');
	 	$thisDate =  $request->getParam('relDate');


	 $checkOrnumber = $this->agent->isRelnumberused($thisNumber);
		if ($checkOrnumber) {
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Release number already used!' 
			]);
		}

		if ($thisParentID  == 0 ) {
			$getClientProperties = $this->clientPaymentHistory->getClientPropertiesCom();
		}else{
			$getClientProperties = $this->clientPaymentHistory->getClientPropertiesCom2($thisParentID);
		}

		$comSummary = [];
		if ($getClientProperties) {
		 	foreach($getClientProperties as $value){

				$propertyName = $value['Property Name'] . ' P ' . $value['P'] .' Blk ' . $value['Blk']. ' Lt ' . $value['Lt'];	 

				$getAgetNameFromComrelease = $this->commission->getAgents($value['cpID']);
				foreach($getAgetNameFromComrelease as $key => $val) {
                    $totalGrossCom  = (($value['TCP'] * ($val['percentagevalue'] / 100)) / $value['CRS']);
					$insertInto = $this->commission->insertIntoComRelease($val['agentID'], $value['cpID'], $value['client ID'], $propertyName, $value['TCP'], ($totalGrossCom - ($totalGrossCom * 0.05)), $thisNumber, $thisDate, $_SESSION['userID'], $value['payment']);
						if(!$insertInto){
							return $this->returnThis($response,[
								'success'	=> false,
								'has_login' => true,
								'message'	=> 'Failed to update records.'
							]);	
						}	
				}

				// update use client paymenthistory  set releseStat to 1
	 			$updateThisProperty = $this->clientPaymentHistory->updateReleseStat($value['id']);
					if(!$updateThisProperty){
						return $this->returnThis($response,[
							'success'	=> false,
							'has_login' => true,
							'message'	=> 'unable to udpate this clientPaymentHistory'
						]);	
					}	
			
				}

				$data [] = [
					'0'  => $value['cpID'],
				];	
		}

		foreach ($data as $key => $val2) {
			// update tblCommissions
			$thisComId = $this->commission->updateComreleaseNo($val2['0']);
			if(!$thisComId){
				return $this->returnThis($response,[
					'success'	=> false,
					'has_login' => true,
					'message'	=> 'unable to udpate this tbl_Commissions'
				]);	
			}	

		}

		return $this->returnThis($response,[
			'success'	 => true,
			'has_login'  => true,
			'messages' 	 => 'Successfully updated!'	
		]);
	}

	public function getThisPropertyByDate($request, $response, $args)
	{

		$dateFrom = $request->getParam('dateFrom');
		$dateTo   = $request->getParam('dateTo');
		$parentID = $request->getParam('parentID');

		$comSummary = [];
		$data = [];

		if ($parentID == 0) {
			$getClientProperties = $this->clientPaymentHistory->getClientPropertiesComWithDateOne($dateFrom, $dateTo);
		}else{
			$getClientProperties = $this->clientPaymentHistory->getClientPropertiesComWithDateTwo($dateFrom, $dateTo, $parentID);
		}

		if ($getClientProperties) {
		 	foreach($getClientProperties as $value){
				$agent_1 = "";
				$agent_2 = "";
				$agent_3 = "";
				$agent_4 = "";
				$sd = "";
				$totalCom ="";  

				$agent1_com ="";
				$agent2_com ="";
				$agent3_com ="";
				$agent4_com ="";

				$Agent1 = $this->agent->getAgent1($value['cpID']);
				$Agent2 = $this->agent->getAgent2($value['cpID']);
				$Agent3 = $this->agent->getAgent3($value['cpID']);
				$Agent4 = $this->agent->getAgent4($value['cpID']);
				$SD 	= $this->agent->getSD($value['cpID']);
				
				foreach($Agent1 as $value_1){
 					$agent_1 =  $value_1['agentName']. ' ' . $value_1['percentageVal']. ' ' . number_format($value_1['Agent 1'],2);
 					$agent1_com = $value_1['Com'];
				}

				foreach($Agent2 as $value_2){
 					$agent_2 = $value_2['agentName']. ' ' . $value_2['percentageVal']. ' ' . number_format($value_2['Agent 2'],2);
 					$agent2_com = $value_2['Com'];
				}

				foreach($Agent3 as $value_3){
 					$agent_3 = $value_3['agentName']. ' ' . $value_3['percentageVal']. ' ' . number_format($value_3['Agent 3'],2);
 					$agent3_com 	=$value_3['Com'];
				}

				foreach($Agent4 as $value_4){
 					$agent_4 = $value_4['agentName']. ' ' . $value_4['percentageVal']. ' ' . number_format($value_4['Agent 3'],2);
 					$agent5_com 	=$value_4['Com'];
				}

				foreach($SD as $sdVal){
 					$sd = $sdVal['agentName'] . ' ' . $sdVal['percentageVal']. ' ' . number_format($sdVal['SD'],2);
 					$agent4_com =$sdVal['Com'];
				}

				// $data = [];
				$totalCom = ($agent1_com + $agent2_com + $agent3_com + $agent4_com + $agent5_com);
				$data [] = [
					'0'  => $value['Buyer'],
					'1'  => $value['P'],
					'2'  => $value['Blk'],
					'3'  => $value['Lt'],
					'4'  => $value['TCP'],
					'5'  => $value['addCom'],
					'6'  => $value['CRS'],
					'7'  => $agent_1, 
					'8'  => $agent_2,
					'9'  => $agent_3,
					'10' => $agent_4,
					'11' => $sd,
					'12' => number_format($totalCom,2)
				];
			}
		}

		return $this->returnThis($response,[
			'success'	 => true,
			'has_login'  => true,
			'details' 	 => $data
		]);	
		
	}


	public function getThisSummaryByDate($request, $response, $args)
	{

		$dateFrom = $request->getParam('dateFrom');
		$dateTo   = $request->getParam('dateTo');

		$agentList = $this->agent->getAgentComRelSum();
		$comSummary= [];
		$Summary   = [];
		foreach ($agentList as $key => $value) {
			$getComRelSum = $this->clientPaymentHistory->getClientPropertiesCom3byDate($value['cp_id'], $dateFrom, $dateTo);
		
			$tatalCom ="";
			foreach ($getComRelSum as $key => $val) {
				$grosstotalCom = (($val['TCP'] * ($value['percentagevalue'] / 100)) / $val['CRS']); 
				$Summary[] = [
					'0' => $value['id'],
					'1' => ($grosstotalCom - ($grosstotalCom * 0.05))
				];
			}
		}

		$agentSummary =[];
		$totalSum = 0;
		$getAgentCom = $this->agent->getAgentComRelSum2();
		foreach ($getAgentCom as $key => $val) {
			foreach ($Summary as $key => $val2) {
			 	if ($val2['0'] == $val['id']) {
			 		$totalSum += $val2['1'];
			 	}
			}

			$agentSummary[] = [
				'0' => $val['AccountNumber'],
				'1' => $val['AgentName'],
				'2' => number_format($totalSum,2)
			];

			$totalSum = 0;

		}

		// echo "<pre>";
		// print_r($Summary);

		return $this->returnThis($response,[
			'success'	 => true,
			'has_login'  => true,
			'ComSum' 	 => $agentSummary	
		]);	
	}

	public function printAndSaveThis($request, $response, $args)
	{
		$thisParentID =  $request->getParam('parentID');
	 	$thisNumber   =  $request->getParam('relNumber');
	 	$thisDate     =  $request->getParam('relDate');
	 	$dateFrom     =  $request->getParam('dateFrom');
		$dateTo       =  $request->getParam('dateTo');
	
	 	$checkOrnumber = $this->agent->isRelnumberused($thisNumber);
		if ($checkOrnumber) {
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Release number already used!' 
			]);
		}

		// $thisNumberforRel = $checkOrnumber[0]['releaseNO'];
		// if (empty($thisNumber) || empty($thisDate)) {
		// 	$thisDate =  date("Y/m/d");
		// 	$thisNumber = ($thisNumberforRel + 1);
		// }

		if ($thisParentID  != 0  && empty($dateFrom)) {
			$getClientProperties = $this->clientPaymentHistory->getClientPropertiesCom2($thisParentID);
		
			// return $this->returnThis($response,[
			// 	'success'	=> false,
			// 	'has_login' => true,
			// 	'message'	=> $dateFrom . ' 1st ' . $dateTo . ' ' . $thisParentID
			// ]);

		}else if ($thisParentID  == 0 && empty($dateFrom)) {
		
			$getClientProperties = $this->clientPaymentHistory->getClientPropertiesCom();
		
			// return $this->returnThis($response,[
			// 	'success'	=> false,
			// 	'has_login' => true,
			// 	'message'	=> $dateFrom . ' 2st ' . $dateTo . ' ' . $thisParentID
			// ]);

		}else if ($thisParentID  == 0 ) {
			$getClientProperties = $this->clientPaymentHistory->getClientPropertiesComWithDateOne($dateFrom, $dateTo);
			
			// return $this->returnThis($response,[
			// 	'success'	=> false,
			// 	'has_login' => true,
			// 	'message'	=> $dateFrom . ' 3st ' . $dateTo . ' ' . $thisParentID
			// ]);

		}else{
			// $getClientProperties = $this->clientPaymentHistory->getClientPropertiesCom2($thisParentID);
			$getClientProperties = $this->clientPaymentHistory->getClientPropertiesComWithDateTwo($dateFrom, $dateTo, $thisParentID);
		
			// return $this->returnThis($response,[
			// 	'success'	=> false,
			// 	'has_login' => true,
			// 	'message'	=> $dateFrom . ' 4st ' . $dateTo . ' ' . $thisParentID
			// ]);
		}

		$comSummary = [];
		if ($getClientProperties) {
		 	foreach($getClientProperties as $value){

				$propertyName = $value['Property Name'] . ' P ' . $value['P'] .' Blk ' . $value['Blk']. ' Lt ' . $value['Lt'];	 

				$getAgetNameFromComrelease = $this->commission->getAgents($value['cpID']);
				foreach($getAgetNameFromComrelease as $key => $val) {
					$totalGrossCom = (($value['TCP'] * ($val['percentagevalue'] / 100)) / $value['CRS']);
					$insertInto = $this->commission->insertIntoComRelease($val['agentID'], $value['cpID'], $value['client ID'], $propertyName, $value['TCP'],($totalGrossCom - ($totalGrossCom * 0.05))  , $thisNumber, $thisDate, $_SESSION['userID'], $value['payment']);
				
						if(!$insertInto){
							return $this->returnThis($response,[
								'success'	=> false,
								'has_login' => true,
								'message'	=> 'Failed to update records.'
							]);	
						}	
				}

				// update use client paymenthistory  set releseStat to 1
	 			$updateThisProperty = $this->clientPaymentHistory->updateReleseStat($value['id']);
					if(!$updateThisProperty){
						return $this->returnThis($response,[
							'success'	=> false,
							'has_login' => true,
							'message'	=> 'unable to udpate this clientPaymentHistory'
						]);	
					}	
			
				}

				$data [] = [
					'0'  => $value['cpID'],
				];	
		}

		foreach ($data as $key => $val2) {
			// update tblCommissions
			$thisComId = $this->commission->updateComreleaseNo($val2['0']);
			if(!$thisComId){
				return $this->returnThis($response,[
					'success'	=> false,
					'has_login' => true,
					'message'	=> 'unable to udpate this tbl_Commissions'
				]);	
			}	

		}

		return $this->returnThis($response,[
			'success'	 => true,
			'has_login'  => true,
			'messages' 	 => 'Successfully updated!'	
		]);
	}


	public function release_thisClientSelectedPropertyToRelease($request, $response, $args)
	{
		$agentID = $request->getParam('agentID');
		$comNo = $request->getParam('comNo');
		$releaseDate = $request->getParam('releaseDate');
		$releasePropId = $request->getParam('releasePropId');

		$getAgentIReleaseNO = $this->agent->isRelnumberused($comNo);
		
		if($getAgentIReleaseNO){
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Release Number Already used.'
			]);
		}
		

		$agent = $this->agent->getAgentInfoForThis($agentID);

		if(count($agent) <= 0){
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'agent' => $agent,
				'agentID' => $agentID,
				'message'	=> 'Agent can not be found.'
			]);
		}


		$agentPropetyWithCommision = $this->commission->getThisAgentAllCommisionsToRelease($agentID);
		if(!$agentPropetyWithCommision){
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Commission not yet available'
			]);
		}

		// $agentPropetyWithCommision = $this->commission->getThisAgentAllCommisionsReleased($agentID,$dateNow);  change this to commission release by the selected properties
		
		$agentPropetyWithCommision = $this->commission->getAgentComReleasedOptional($agentID,$releasePropId);

		$results = [];
		$percentageTotal = 0;
		$grossCom = 0;
		$totalTax = 0;
		

		foreach ($agentPropetyWithCommision as $key => $values) {

			$clientProperty  = $this->clientProperty->getAllPropertyByClientOwnerID($values['cp_id']);

			if(count($clientProperty) <= 0){
				continue;
			}		

			$data=[];

			$contractPrice 			= floatval($clientProperty[0]['contractPrice']);
			$monthlyAmortization 	= floatval($clientProperty[0]['monthlyAmortization']);

			$sqmPricem2 			= floatval($clientProperty[0]['sqmPricem2']);
			$sqm 					= floatval($clientProperty[0]['sqm']);
			$plan_terms 			= intval($clientProperty[0]['plan_terms']);

			$additionalCharges 		= $clientProperty[0]['additionalCharges'];
			$comRelease 			= $clientProperty[0]['commissionReleased'];
			$adCom 					= $clientProperty[0]['adCom'];
			$payment 				= (string) ($values['released'] + 1)  . ' / ' . (int) $comRelease;


			if(!empty($additionalCharges)){
				$additionalCharges = json_decode($clientProperty[0]['additionalCharges']);
				if(count($additionalCharges) != 0){
					$totalChargeValue = 0;
					foreach ($additionalCharges as $key => $value) {
						$totalChargeValue += $value->chargeValue;
					}

					$contractPrice = (($sqmPricem2 * $sqm));
					$monthlyAmortization = ((($sqmPricem2 * $sqm)) / $plan_terms);
				}
			}

			$totalGrossCom = ($contractPrice * floatval(($values['percentagevalue']  / 100) / $comRelease));
			

			$insertInto = $this->commission->insertIntoComRelease($agentID, $values['cp_id'], $values['client_id'],  $values['propertyName'], $contractPrice, ($totalGrossCom - ($totalGrossCom * 0.05)) , $comNo, $releaseDate, $_SESSION['userID'], $payment);
			
			if(!$insertInto){
				return $this->returnThis($response,[
					'success'	=> false,
					'has_login' => true,
					'message'	=> 'Failed to update records.'
				]);	
			}	

			if(intval($values['percentagevalue']) < 15){
				$values['percentagevalue'] = '0'.intval($values['percentagevalue']);
			}
			else
				$values['percentagevalue'] = intval($values['percentagevalue']);

			$percentageTotal = $percentageTotal + ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comRelease ) - ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comRelease ) * 0.05));
			$grossCom 		 = $grossCom + ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comRelease ));
			$totalTax 		 = $totalTax + ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comRelease ) * 0.05);
			// $percentageTotal = $percentageTotal + (((($contractPrice * 0.15 * .05) - $adCom )/ $comRelease ) / 0.15 * floatval(('0.'.$values['percentagevalue'])));

			$results[] = [
				// 'clientID' => $values['cp_id'],
				'clienName' 		=> $values['clientName'],
				'propertyName' 		=> $values['propertyName'] .'( '.intval($values['percentagevalue']).'% )',
				'contractPrice'	    => $contractPrice,
				'payment' 			=> $payment,
				'percentageValue'   => number_format( ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comRelease ) - ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comRelease ) * 0.05)), 2),
				'dateRelease'       => $values['dateNow']
			];	

			$updteCom = $this->commission->getUpdateCom($values['cp_id'], $comRelease, $agentID);

	
		}

		return $this->returnThis($response,[
			'success'		    => true,
			'has_login' 		=> true,
			'results'			=> $results	,
			'percentageTotal'   => $percentageTotal,
			'gross'				=> $grossCom,
			'wtx'				=> $totalTax
		]);
	}

	public function get_thisReleasedCommission($request, $response, $args){
		$agentId = $request->getParam('agentID');
		$value   = $request->getParam('value');
		$option  = $request->getParam('option');

        $searchBy ='';

		if($option == 0){
			$searchBy = "releaseNO";
		}else{
			$searchBy = "ReleaseDate";
		}

		$getReleasedCom  = $this->commission->geTReleasedCommission($agentId, $value, $searchBy);
		if(!$getReleasedCom){
			return $this->returnThis($response,[
				'success'		    => false,
				'message'			=> 'Unable to locate this data plese try again!'

			]);
		}

		return $this->returnThis($response,[
			'success'		=> true,
			'data'			=> $getReleasedCom
		]);
	}

	public function addAgentUsernamelogin($request, $response, $args)
	{
		$agentId 	= $request->getParam('agentID');
		$info    	= $request->getParam('info');
		$username   = $request->getParam('username');
		$state 		= $this->checkForAgentInUsers($agentId);
		$message 	= "";

		// return $this->returnThis($response,[
		// 	'success'		=> false,
		// 	'message'		=> $state
		// ]);


		if ($state == "false") {
			$insert =  $this->agent->addNewUserAgent($agentId, $info['fname'], $info['lname'], $info['mname'], $info['addre'], $info['email'], $info['contact'], 'Agent', $username);
			$message= "Agent Username Successfully Added!";
		}else{
			$insert =  $this->agent->updateAgentUserName($agentId, $username);
			$message=  "Agent username successfully updated!";
		}

		if (!$insert) {
			if ($state == 1) {
				return $this->returnThis($response,[
					'success'		=> false,
					'message'		=> 'Sorry.. unable to update agent username.'
				]);
			}else{
				return $this->returnThis($response,[
					'success'		=> false,
					'data'			=> 'Sorry.. unable to add agent username.'
				]);
			}
		}

		return $this->returnThis($response,[
			'success'		=> true,
			'message'		=> $message
		]);

	}

	public function checkForAgentInUsers($agentID)
	{
		$select = $this->agent->checkAgent($agentID);
		if (!$select) {
			return false;
		}

		return $select[0]['user'];
	}

	public function addAgentPasswordlogin($request, $response, $args)
	{
		$agentId 	= $request->getParam('agentID');
		$info    	= $request->getParam('info');
		$pass  		= $request->getParam('pass');
		$message    = "";
		$state 		= $this->checkForAgentInUsers($agentId);

		// return $this->returnThis($response,[
		// 	'success'		=> false,
		// 	'message'		=> $state
		// ]);

		$pass_hash  = $this->getHash($pass);

		if ($state == "false") {
			$insert  = $this->agent->addNewUserAgentPassword($agentId, $info['fname'], $info['lname'], $info['mname'], $info['addre'], $info['email'], $info['contact'], 'Agent', $pass_hash);
			$message = "Agent password successfully added!";
		}else{
			$insert =  $this->agent->updateAgentPassword($agentId, $pass_hash);
			$message = "Agent password successfully updated!";
		}

		if (!$insert) {
			if ($state == 1) {
				return $this->returnThis($response,[
					'success'		=> false,
					'message'		=> 'Sorry.. unable to update agent password.'
				]);
			}else{
				return $this->returnThis($response,[
					'success'		=> false,
					'data'			=> 'Sorry.. unable to add agent password.'
				]);
			}
		}

		return $this->returnThis($response,[
			'success'		=> true,
			'message'		=> $message
		]);
	}

	public function getHash($rand_string){
		
		$options = [
		    'cost' => 11,
		    // 'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM)
		];
		
		// password_verify ( string $password , string $hash )
		return password_hash($rand_string, PASSWORD_BCRYPT, $options);
	}



}