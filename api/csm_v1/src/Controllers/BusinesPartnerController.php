<?php
/**
 * Created by Raymund Matol
 * Date: 1/31/16
 */

namespace App\Controllers;

use App\Helpers\Validation;
use App\Models\BusinessPartner;
use App\Models\Property;
use App\Controllers\ParentController;
/**
 * Class UserController
 * @package App\Controllers
 */
 
class BusinesPartnerController extends ParentController{

	protected $validation; 
	protected $property;
	protected $businessPart;

	public function __construct(BusinessPartner $businessPart, Property $property,  Validation $validation) {
		// model
		$this->businessPart = $businessPart;
		$this->property = $property;	
		// Validation Helper
		$this->validation = $validation;

	}
	// $_SESSION['Name'] == DEBONER21 DULOS 
	/**
     * @param $request
     * @param $response
     * @return $response
     */
	
	public function addNewBusinessPartner($request, $response, $args){
		
		$fname = $request->getParam('fname');
		$lname = $request->getParam('lname');
		$mname = $request->getParam('mname');
		$address = $request->getParam('address');
		$email = $request->getParam('email');
		$contact = $request->getParam('contact');
		$properyList = $request->getParam('prop_list');

		$data = [];
		if (count($properyList) !== 0) {
			foreach ($properyList as $key => $value) {
				array_push($data, $value);
			}
		}

		if ($fname == '' || $lname == '') {
			return $this->returnThis($response,[
				'success'			=> false,
				'has_login' 		=> true,
				'message'			=> 'Please inlcude	 partner name'	
			]);
		}

		$details [] = [
			'fname' => ucwords($fname), 
			'lname' => ucwords($lname),
			'mname' => ucwords($mname),
			'address' => ucwords($address),
			'email' => $email,
			'contact' => $contact,
			'proplist' => json_encode($data)
		];

		if (!$this->newBusinesPartner($details)) {
			return $this->returnThis($response,[
				'success'			=> false,
				'has_login' 		=> true,
				'message'			=> 'Something went wrong unable to add business partner'	
			]);
		}

		return $this->returnThis($response,[
			'success'			=> true,
			'has_login' 		=> true,
			'message'			=> 'Successfully added!'
		]);
	}	

	public function newBusinesPartner($details)
	{
		$insert = $this->businessPart->newBusinessPartner($details);
		if (!$insert) {
			return false;
		}

		return true;
	}


	public function getHash($rand_string){
		
		$options = [
		    'cost' => 11,
		    // 'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM)
		];
		
		// password_verify ( string $password , string $hash )
		return password_hash($rand_string, PASSWORD_BCRYPT, $options);
	}


	public function addbusinessPartnerUser($details)
	{
		$insert = $this->businessPart->insertIntoUsers($details);
		if (!$insert) {
			return false;
		}

		return true;
	}

	public function checkUsername($username)
	{
		$status = true;
		$username = $this->businessPart->checkUsername($username);
		if (!$username) {
			$status = false;
		}

		return $status;

	}


	public function getThisPropertyList($request, $response, $args)
	{
		$prop  = $this->property->getPropertyParents();
		if (!$prop) {
			return $this->returnThis($response,[
				'success'		=> true,
				'has_login' 	=> true,
				'message'	 	=> 'Unable to get proper list!' 
			]);
		}

		return $this->returnThis($response,[
			'success'		=> true,
			'has_login' 	=> true,
			'data'		 	=> $prop
		]);
	}

	public function getBusinessPartnerList($request, $response, $args)
	{
		$select = $this->businessPart->getPartnerList();
		if (!$select) {
			return $this->returnThis($response,[
				'success'		=> true,
				'has_login' 	=> true,
				'message'		=> 'List not available.'
			]);
		}

		return $this->returnThis($response,[
			'success'		=> true,
			'has_login' 	=> true,
			'data'		 	=> $select
		]);	
	}

	public function getRemoveThisPartner($request, $response, $args)
	{
		$ID = $request->getParam('id');
		$remove = $this->businessPart->removePartnerID($ID);
		if (!$remove) {
			return $this->returnThis($response,[
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Something went wrong unable to remove selected list'
			]);	
		}

		return $this->returnThis($response,[
			'success'		=> true,
			'has_login' 	=> true,
			'message'		=> 'Partner successfully remove.'
		]);	
	}

	public function getPartnerProfileDetails($request, $response, $args)
	{
		$partnerID = $request->getParam('id');
		
		$profileDetails = $this->businessPart->getPartnerProfile($partnerID);
		if (!$profileDetails) {
			return $this->returnThis($response,[
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Unable to get profile details'
			]);
		}

		return $this->returnThis($response,[
			'success'		=> true,
			'has_login' 	=> true,
			'data'			=> $profileDetails
		]);
	}

	public function updateBusinespartnerProfile($request, $response, $args)
	{
		$profileDetails = $request->getParam('data');
		$id = $request->getParam('id');

		$update  = $this->businessPart->updateBusinessPartnerProfileDetails($id, $profileDetails);
		if (!$update) {
			return $this->returnThis($response,['success' => false, 'message' => 'Unable to update profile.']);	
		}

		return $this->returnThis($response,[
			'success'		=> true,
			'has_login' 	=> true,
			'message'		=> 'Profile successfully updated!'
		]);

	}

	public function addUpdateUsername($request, $response, $args)
	{
		$id = $request->getParam('id');
		$username = $request->getParam('data');
		$profile  = $request->getParam('profile');

		$checkUsername = $this->businessPart->checkUsername($id, $username);
		if (!$checkUsername) {
		 		return $this->returnThis($response,[
					'success'		=> false,
					'message'		=> 'Something went wrong unable to check partner username.'
				]);	
		}else{
			if ($checkUsername[0]['username'] > 0) {
				return $this->returnThis($response,[
					'success'		=> true,
					'message'		=> 'Update username?',
					'data'			=> 1
				]);				
			}else{
				$checkExistingUsername = $this->businessPart->checkexistingUsername($username);
				if (!$checkExistingUsername) {
					return $this->returnThis($response,[
						'success'		=> false,
						'message'		=> 'Unable to compare to usernames!'
					]);
				}else{

					if ($checkExistingUsername[0]['count'] > 0) {
						return $this->returnThis($response,[
							'success'		=> true,
							'message'		=> 'Username already taken!',
							'data'			=> 0
						]);

					}else{
						$adduserName = $this->businessPart->addUsername($id, $username, $profile);
						if (!$adduserName) {
							return $this->returnThis($response,[
								'success'		=> true,
								'message'		=> 'Unable to add username!',
								'data'			=> 0
							]);	
						}else{
							return $this->returnThis($response,[
								'success'		=> true,
								'message'		=> 'Username successfully added!'
							]);
						}
					}
				}
			}
		}
	}

	public function updateAccouuntUsername($request, $response, $args)
	{
		$id = $request->getParam('id');
		$username = $request->getParam('data');

		$checkExistingUsername = $this->businessPart->checkexistingUsername($username);
		if (!$checkExistingUsername) {
			return $this->returnThis($response,[
				'success'	=> false,
				'message'	=> 'Unable to compare to usernames!',
				'stat'		=> 0
			]);

		}else{
			if ($checkExistingUsername[0]['count'] > 0) {
				return $this->returnThis($response,[
					'success'		=> true,
					'message'		=> 'Username already taken!',
					'stat'			=> 0
				]);
			}else{	
				$updateUsername = $this->businessPart->updateAccountUsername($id, $username);
				if ($updateUsername) {
					return $this->returnThis($response,[
						'success'	=> true,
						'message'	=> 'Unable to update username.',
						'stat'		=> 0
					]);	
				}
			}
		}

		return $this->returnThis($response,[
			'success'	=> true,
			'message'	=> 'Username successfully updated.',
			'stat'		=> 1
		]);		
	}

	public function getPartnerUsername($request, $response, $args)
	{
		$id = $request->getParam('id');

		$username = $this->businessPart->getUsername($id);
		if (!$username) {
			return $this->returnThis($response, [
				'success' => true,
				'message' => 'Unable to get username'
			]);
		}

		return $this->returnThis($response, [
			'success' => true,
			'username' => $username[0]['username']
		]);
	}

	public function getPartnerPassword($request, $response, $args)
	{
		$id 	  = $request->getParam('id');
		$password = $request->getParam('password');

		$checkPass = $this->businessPart->getPassword($id);
		if (!$checkPass) {
			return $this->returnThis($response, [
				'success' => true,
				'message' => 'Please set username first!',
				'title'	  => 'Warning!',
				'label'	  => 'warning'
			]);	
		}

		$newpass_hash = $this->getHash($password);

		$addpass = $this->businessPart->addPassword($id, $newpass_hash);
		if (!$addpass) {
			return $this->returnThis($response, [
				'success' => true,
				'message' => 'Unable to add password',
				'title'	  => 'Warning!',
				'label'	  => 'warning'
			]);
		}

		return $this->returnThis($response, [
			'success' => true,
			'message' => 'Password Added!',
			'title'	  => 'Success!',
			'label'	  => 'success'
		]);

	}

	public function updateBusinessPropertyList($request, $response, $args)
	{
		 $proprtyID = $request->getParam('id');
		 $propList = $request->getParam('properTy');
		 
		 $update = $this->businessPart->updatePropertyById($proprtyID, json_encode($propList));
		 if (!$update) {
		 	return $this->returnThis($response, [
				'success' => true,
				'message' => 'Unable to update business partner property!',
				'title'	  => 'Warning!',
				'label'	  => 'warning'
			]);
		 }

		return $this->returnThis($response, [
			'success' => true,
			'message' => 'Business Property Successfully Updated',
			'title'	  => 'Success!',
			'label'	  => 'success'
		]);

	}


}





	