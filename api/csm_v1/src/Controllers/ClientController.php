<?php
/**
 * Created by Raymund Matol
 * Date: 1/31/16
 */

namespace App\Controllers;

use App\Helpers\Validation;
use App\Models\Client;
use App\Models\Commission;
use App\Models\Agent;
use App\Models\Property;
use App\Models\ClientPaymentHistory;
use App\Models\ClientProperty;
use App\Controllers\ParentController;
/**
 * Class UserController
 * @package App\Controllers
 */

class ClientController extends ParentController{

	protected $validation;
	protected $client;
	protected $commission;
	protected $agent;
	protected $property;
	protected $clientProperty;
	protected $clientPaymentHistory;
	protected $property_originalPrice;
	protected $client_monthlyAmortization;

	public function __construct(Client $client,ClientProperty $clientProperty,ClientPaymentHistory $clientPaymentHistory,Agent $agent,Property $property,Commission $commission, Validation $validation) {
		// model
		$this->client 					= $client;
		$this->commission 				= $commission;
		$this->clientPaymentHistory 	= $clientPaymentHistory;
		$this->clientProperty 			= $clientProperty;
		$this->agent 					= $agent;
		$this->property 				= $property;
		// Validation Helper
		$this->validation 				= $validation;

	}
	// $_SESSION['Name'] == DEBONER21 DULOS
	/**
     * @param $request
     * @param $response
     * @return $response
     */

	public function getAllParentPropertiesAndAgents($request, $response, $args){
		$parentProperties = $this->property->getAllParentProperties();
		$agents = $this->agent->getAllAgentsList();

		return $this->returnThis($response,[
			'success'		=> true,
			'has_login' 	=> true,
			'properties'	=> $parentProperties,
			'agents' 		=> $agents
		]);
	}


	public function getAllClient($request, $response, $args){
		$userID = $_SESSION['userID'];

		if ($_SESSION['UserRole'] == "Agent") {
			$clients = $this->client->getAgentAssociatedClients($userID);
		}else{
			$clients = $this->client->getAllClient();
		}

		return $this->returnThis($response,[
			'success'			=> true,
			'has_login' 		=> true,
			'clients' 			=> $clients
		]);

	}


	public function getAllClientNames($request, $response, $args){

		$clients = $this->client->getAllClientNames();
		$setORnumber = $this->client->getOrNumber();

		return $this->returnThis($response,[
			'success'			=> true,
			'has_login' 		=> true,
			'clients' 			=> $clients,
			'num' 				=> $setORnumber

		]);

	}

	public function forfeitSelectedProperty($request, $response, $args)
	{
		$clientID 			= $request->getParam('id');
		$cp_id 				= $request->getParam('cp_id');

		if($clientID == "" || $cp_id == ""){
			return $this->returnThis($response,[
				'success'					=> false,
				'has_login' 				=> true,
				'message' 					=> 'Some fields from are empty!'
			]);
		}else{

			$updateConRel = $this->clientProperty->forfeitThisSelectedProperty($clientID, $cp_id);
			if(!$updateConRel){
				return $this->returnThis($response,[
					'success'					=> false,
					'has_login' 				=> true,
					'message' 					=> $updateConRel['message']
				]);
			}
							
			// $updateConReltwo = $this->clientProperty->forfeitThisSelectedPropertyTwo($clientID, $cp_id);
			// if(!$updateConReltwo){
			// 	return $this->returnThis($response,[
			// 		'success'					=> false,
			// 		'has_login' 				=> true,
			// 		'message' 					=> "Unable to update propertylist!"
			// 	]);
			// }
			$itextmo = $this->clientProperty->getClientwithNumber($cp_id);
			$apicode="ST-DSRDO875467_BTAET";
			$passwd="8trubl&qfs";
		if(count($itextmo) ==1)
		{
			$propertyName 	=($itextmo[0]['propertyName']);
			$name 			=($itextmo[0]['fullName']);
			$number         =($itextmo[0]['ContactNumber']);
			$lotno          =($itextmo[0]['lot']);
			$blockno        =($itextmo[0]['block']);
			$due            =($itextmo[0]['dueDate']);
			$message = "Hi " . $name . ", This is from R and Sons Properties Co. informing you that your property " . $propertyName . " Block:" . $blockno . " lot:" . $lotno   . " as of today is already forfeited because you failed to settle the extended allowable time of your payment.";
			if (strlen($number)==11) {
				$respondz = $this->itexmo($number,$message,$apicode,$passwd);
			}
				
		// 	$url = 'https://www.itexmo.com/php_api/api.php';
		// $itexmo = array('1' => $number, '2' => $message, '3' => $apicode, 'passwd' => $passwd);
		// $param = array(
		// 	'http' => array(
		// 		'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		// 		'method'  => 'POST',
		// 		'content' => http_build_query($itexmo),
		// 	),
		// );
		// $context  = stream_context_create($param);
		// return file_get_contents($url, false, $context);

		}

			return $this->returnThis($response,[
				'success'					=> true,
				'has_login' 				=> true,
				'message' 					=> $updateConRel['message']
			]);
		}
	}


	public function updatedAdditionalCharges($request, $response, $args)
	{
		$clientID 			= $request->getParam('id');
		$cp_id 				= $request->getParam('cp_id');
		$chargesDetails 	= $request->getParam('chargesDetails');

		if($clientID == "" || $cp_id == "" || $chargesDetails == ""){
			return $this->returnThis($response,[
				'success'					=> false,
				'has_login' 				=> true,
				'message' 					=> 'Some fields from are empty!'
			]);
		}else{

			$updateConRel = $this->clientProperty->updateAdditionalCharges($clientID, $cp_id, $chargesDetails);
			if(!$updateConRel){
				return $this->returnThis($response,[
					'success'					=> false,
					'has_login' 				=> true,
					'message' 					=> "Unable to add/update addtional charges!"
				]);
			}
			return $this->returnThis($response,[
				'success'					=> true,
				'has_login' 				=> true,
				'message' 					=> "Additioanl charges updated!"
			]);
		}
	}

 	public function updateAddCommission($request, $response, $args)
 	{
 		$clientID 			= $request->getParam('id');
		$cp_id 				= $request->getParam('cp_id');
		$addCom 	        = $request->getParam('comSched');

		if($clientID == "" || $cp_id == "" || $addCom == ""){
			return $this->returnThis($response,[
				'success'					=> false,
				'has_login' 				=> true,
				'message' 					=> 'Some fields from are empty!'
			]);
		}else{

			$updateConRel = $this->clientProperty->updateAddCommission($clientID, $cp_id, $addCom);
			
			if(!$updateConRel){
				return $this->returnThis($response,[
					'success'					=> false,
					'has_login' 				=> true,
					'message' 					=> "Unable to update add commission!"
				]);
			}

			return $this->returnThis($response,[
				'success'					=> true,
				'has_login' 				=> true,
				'message' 					=> "Add Commission Updated!"
			]);
		}
 	}

	public function updateComRelShecdule($request, $response, $args)
	{
		$clientID 			= $request->getParam('id');
		$cp_id 				= $request->getParam('cp_id');
		$comReleasedSched 	= $request->getParam('comSched');

		if($clientID == "" || $cp_id == "" || $comReleasedSched == ""){
			return $this->returnThis($response,[
				'success'					=> false,
				'has_login' 				=> true,
				'message' 					=> 'Some fields from are empty!'
			]);
		}else{

			$updateConRel = $this->clientProperty->updateComRelShecdule($clientID, $cp_id, $comReleasedSched);
			if(!$updateConRel){
				return $this->returnThis($response,[
					'success'					=> false,
					'has_login' 				=> true,
					'message' 					=> "Unable to update comission release schedule!"
				]);
			}
			return $this->returnThis($response,[
				'success'					=> true,
				'has_login' 				=> true,
				'message' 					=> "Commission Schedule Updated!"
			]);
		}
	}

	// 1
	public function getAllPropertiesForThisClient($request, $response, $args){
		$clientID 					= $request->getParam('id');
		$clientsProperties 			= $this->clientProperty->getPropertiesForThisclient($clientID);
		$clientsPropertyDetails 	= $this->clientProperty->getPropertyDetails($clientID);

		return $this->returnThis($response,[
			'success'						=> true,
			'has_login' 					=> true,
			'clientsProperties' 			=> $clientsProperties,
			'clientsPropertyDetails' 	    => $clientsPropertyDetails,
		]);

	}


	public function getContractPrice($clientID) {
        return $this->property_originalPrice;
    }

    public function getBalance($clientID) {
        return $this->property_originalPrice;
    }



    // view property payment history
    public function viewPaymentHistoryForThisProperty($request, $response, $args)
    {
    	$propertyID                   = $_GET['id'];
    	$clientid                     = $_GET['clientid'];
		$datas=[];



    	$clientsPropertyPaymentHistory 	= $this->clientPaymentHistory->getPaymentHistoryForThisProperty($propertyID, $clientid);
    	$clientProperty 				= $this->clientProperty->getAllPropertyByClientOwnerIDTwo($propertyID, $clientid);

    	if (!$clientsPropertyPaymentHistory) {
    		 return $this->returnThis($response,[
				'success'									=> false,
				'has_login' 								=> true,
				'message'									=> 'No Payment Record!'
			]);
    	}

    	if(count($clientProperty) <= 0){
			return $this->returnThis($response,[
				'success'									=> false,
				'has_login' 								=> true,
				'message'									=> 'That property does not exist.'
			]);
		}


		$contractPrice 			= floatval($clientProperty[0]['contractPrice']);
		$monthlyAmortization 	= floatval($clientProperty[0]['monthlyAmortization']);

		$sqmPricem2 			= floatval($clientProperty[0]['sqmPricem2']);
		$sqm 					= floatval($clientProperty[0]['sqm']);
		$plan_terms 			= intval($clientProperty[0]['plan_terms']);

		$additionalCharges 		= $clientProperty[0]['additionalCharges'];

		// $paymentStat 		    = $property_originalPrice[0]['fullyPaid'];


		if(!empty($additionalCharges)){
			$additionalCharges = json_decode($clientProperty[0]['additionalCharges']);
			if(count($additionalCharges) != 0){
				$totalChargeValue = 0;
				foreach ($additionalCharges as $key => $value) {
					$totalChargeValue += $value->chargeValue;
				}

				$contractPrice = (($sqmPricem2 * $sqm));
				$monthlyAmortization = ((($sqmPricem2 * $sqm)) / $plan_terms);
			}
		}


    	if(count($clientsPropertyPaymentHistory) > 0){
			foreach ($clientsPropertyPaymentHistory as $key => $value) {

				$contractPrice -= $value['propertyCredit'];
				$contractPrice -= $value['propertyDebit'];

				$datas[] = [
					0 => ($key+1),
					1 => $value['particulars'],
					2 => $value['dateAdded'],
					3 => $value['propertyDebit'],
					4 => $value['propertyCredit'],
					5 => $contractPrice,
					// 6 => $value['propertyAgentComission'],
					// 7 => ($value['dateClaimed'] == '0000-00-00'? '': $value['dateClaimed']),
				];
			}
		}

		return $this->returnThis($response,[
			'success'									=> true,
			'has_login' 								=> true,
			'clientsPropertyPaymentHistory' 			=> $datas
		]);

    }


    // get payment history for client payment form
	public function paymentHistoryForThisProperty($request, $response, $args){
		$paymentopt 					= $request->getParam('paymentopt');
		
		if($paymentopt == 1){

			$propertyID                     = $_GET['id'];
			$cpID 							= $request->getParam('cpID');
	
			if($request->getParam('cpID') == null){
				$cpID = $propertyID;
			}
	
			$clientsPropertyPaymentHistory 	= $this->clientPaymentHistory->getPropertiesPaymentHistory($cpID);
			$clientProperty 				= $this->clientProperty->getAllPropertyByClientOwnerID($propertyID);
			$clientDownpayment 				= $this->client->getThisClientDownPayment($propertyID);
			$property_originalPrice 		= $this->clientProperty->getContactPrice($propertyID);
			$getcontract 					= $this->clientProperty->getContractInfo($propertyID);
			$getTotalPayment				= $this->clientProperty->getTotalPayment($propertyID);
			$getPropertyDetails   			= $this->clientProperty->getThisPropertyDetails($propertyID);
	
			$originalPrice="";
			$totalPay="";
			$monthly="";
			$paymentStat="0";
	
			$datas=[];
	
			$contractPrice 			= floatval($clientProperty[0]['contractPrice']);
			// $monthlyAmortization 	= floatval($clientProperty[0]['monthlyAmortization']);
	
			$sqmPricem2 			= floatval($clientProperty[0]['sqmPricem2']);
			$sqm 					= floatval($clientProperty[0]['sqm']);
			$plan_terms 			= intval($clientProperty[0]['plan_terms']);
			$new_computation		= ((($sqmPricem2 * $sqm) - $clientDownpayment[0]['downpayment']) / $plan_terms);
			$monthlyAmortization 	= floatval($new_computation);
			$additionalCharges 		= $clientProperty[0]['additionalCharges'];
			if ($property_originalPrice) {
				foreach ($property_originalPrice as $key => $val) {
					$originalPrice = ($sqmPricem2 * $sqm) - $val['downpayment'];
					$monthly = $val['monthly'];
					$paymentStat  = $val['fullyPaid'];
				}
			}else{
				foreach ($getcontract as $key => $val) {
					$originalPrice = ($sqmPricem2 * $sqm) - $val['downpayment'];
					$monthly = $val['monthly'];
					$paymentStat  = $val['fullyPaid'];
				}
			}
	
	
			foreach ($getTotalPayment as $key => $val_2) {
				$totalPay  = $val_2['totalPayment'];
			}
	
			 $totalBall 			= $originalPrice - $totalPay;
	 
			 if(count($clientProperty) <= 0){
				return $this->returnThis($response,[
					'success'									=> false,
					'has_login' 								=> true,
					'message'									=> 'That property does not exist.'
				]);
			}
	
			if(!empty($additionalCharges)){
				$additionalCharges = json_decode($clientProperty[0]['additionalCharges']);
				if(count($additionalCharges) != 0){
					$totalChargeValue = 0;
					foreach ($additionalCharges as $key => $value) {
						$totalChargeValue += $value->chargeValue;
					}
	
					// $contractPrice = (($sqmPricem2 * $sqm) + $totalChargeValue );
					$contractPrice = (($sqmPricem2 * $sqm));
					// $monthlyAmortization = ((($sqmPricem2 * $sqm) + $totalChargeValue ) / $plan_terms); g remove ang additional charhes kay wala sila naga ga
					$monthlyAmortization = ((($sqmPricem2 * $sqm) - $clientDownpayment[0]['downpayment']) / $plan_terms);
				}
			}
	
			if(count($clientsPropertyPaymentHistory) > 0){
				foreach ($clientsPropertyPaymentHistory as $key => $value) {
	
					$contractPrice -= $value['propertyCredit'];
					$contractPrice -= $value['propertyDebit'];
	
					$datas[] = [
						0 => ($key+1),
						1 => $value['particulars'],
						2 => $value['dateAdded'],
						3 => $value['propertyDebit'],
						4 => $value['propertyCredit'],
						5 => $contractPrice,
						6 => $value['propertyAgentComission'],
						7 => ($value['dateClaimed'] == '0000-00-00'? '': $value['dateClaimed']),
					];
				}
			}
	
			return $this->returnThis($response,[
				'success'						=> true,
				'has_login' 					=> true,
				'clientsPropertyPaymentHistory' => $datas,
				'balance' 						=> $totalBall,
				'monthlyAmortization'			=> $monthlyAmortization,
				'contract_price'				=> $originalPrice,
				'paid'							=> $paymentStat,
				'clientsPropertyDetails'        => $getPropertyDetails,
				
			]);
		}else{

			$selected 	= $request->getParam('select');
				$propertyID = $request->getParam('multiple');
				$cpID 	= $request->getParam('cpID');
				$multipledata = [];
			if($selected == 2){
				if($propertyID ==  0){
					return $this->returnThis($response,[
						'success'	=> false,
						'message'	=> 'Please select property!'
					]);
				
				}
					foreach($propertyID as $key => $value){
						$clientProperty = $this->clientProperty->getAllPropertyByClientOwnerID($value);
						$clientDownpayment 				= $this->client->getThisClientDownPayment($value);
						$getTotalPayment				= $this->clientProperty->getTotalPayment($value);
						$getPropertyDetails   			= $this->clientProperty->getThisPropertyDetails($value);
						$clientsPropertyPaymentHistory 	= $this->clientPaymentHistory->getPropertiesPaymentHistory($cpID);
						$property_originalPrice 		= $this->clientProperty->getContactPrice($value);
						$getcontract 					= $this->clientProperty->getContractInfo($value);
						$getPropertyDetails   			= $this->clientProperty->getThisPropertyDetails($value);

						$originalPrice="";
						$totalPay="";
						$monthly="";
						$paymentStat="0";
						$datas=[];
						$contractPrice 			= floatval($clientProperty[0]['contractPrice']);
						$monthlyAmortization 	= floatval($clientProperty[0]['monthlyAmortization']);
						$monhtlybill 			= floatval($clientProperty[0]['monthlyAmortization']);
						$sqmPricem2 			= floatval($clientProperty[0]['sqmPricem2']);
						$sqm 					= floatval($clientProperty[0]['sqm']);
						$plan_terms 			= intval($clientProperty[0]['plan_terms']);
						// $titling_fee			= floatval($clientProperty[0]['titling_fee']);
						$additionalCharges 		= $clientProperty[0]['additionalCharges'];
						
						
						if ($property_originalPrice) {
							foreach ($property_originalPrice as $key => $val) {
								$originalPrice = ($sqmPricem2 * $sqm) - $val['downpayment'];
								$contractPrice	= $contractPrice - $val['downpayment'];
								$monthly = $val['monthly'];
								$paymentStat  = $val['fullyPaid'];
							}
						}else{
							foreach ($getcontract as $key => $val) {
								$originalPrice = ($sqmPricem2 * $sqm) - $val['downpayment'];
								$contractPrice	= $contractPrice - $val['downpayment'];
								$monthly = $val['monthly'];
								$paymentStat  = $val['fullyPaid'];
							}
						}
						foreach ($getTotalPayment as $key => $val_2) {
							$totalPay  = $val_2['totalPayment'];
						}
						$totalBalance 	= $contractPrice - $totalPay;
				
						if(count($clientProperty) <= 0){
							return $this->returnThis($response,[
								'success'									=> false,
								'has_login' 								=> true,
								'message'									=> 'That property does not exist.'
							]);
						}
				
						if(!empty($additionalCharges)){
							$additionalCharges = json_decode($clientProperty[0]['additionalCharges']);
							if(count($additionalCharges) != 0){
								$totalChargeValue = 0;
								foreach ($additionalCharges as $key => $value) {
									$totalChargeValue += $value->chargeValue;
								}
				
							
								$contractPrice = (($sqmPricem2 * $sqm));
							
								$monthlyAmortization = ((($sqmPricem2 * $sqm) - $clientDownpayment[0]['downpayment']) / $plan_terms);
							}
						}
						$monthlyAmortization = ((($sqmPricem2 * $sqm) - $clientDownpayment[0]['downpayment']) / $plan_terms);
						if(count($clientsPropertyPaymentHistory) > 0){
							foreach ($clientsPropertyPaymentHistory as $key => $value) {
				
								$contractPrice -= $value['propertyCredit'];
								$contractPrice -= $value['propertyDebit'];
				
								$datas[] = [
									0 => ($key+1),
									1 => $value['particulars'],
									2 => $value['dateAdded'],
									3 => $value['propertyDebit'],
									4 => $value['propertyCredit'],
									5 => $contractPrice,
									6 => $value['propertyAgentComission'],
									7 => ($value['dateClaimed'] == '0000-00-00'? '': $value['dateClaimed']),
								];
							}
						}
						$multipledata [] =[
								'PropertyData' 						=> $clientProperty[0],
								'ClientPropertyPaymentHistory' 		=> $datas,
								'Totalbalance' 						=> $totalBalance,
								'MonthlyAmort' 						=> $monthlyAmortization,
								'ContractPrice' 					=> $originalPrice,
								'PaymentStat' 						=> $paymentStat,
								'ClientPropertyDetails' 			=> $getPropertyDetails
							];
					}
							return $this->returnThis($response,[
								'success'	=> true,
								'has_login'	=> true,
								'result'	=> $multipledata,
								'option'	=> 	$paymentopt,
								'multiple'	=> $propertyID
							]);

			}else{
				//  $propertyID = $request->getParam('multiple');
				// $cpID 							= $request->getParam('cpID');
				if($request->getParam('cpID') == null){
					$cpID = $propertyID;
				}
				if($propertyID ==  0){
					return $this->returnThis($response,[
						'success'	=> false,
						'message'	=> 'Please select property!'
					]);
				}
				foreach ($propertyID as $key => $value){
					$clientsPropertyPaymentHistory 	= $this->clientPaymentHistory->getPropertiesPaymentHistory($cpID);
					$clientProperty 				= $this->clientProperty->getAllPropertyByClientOwnerID($value);
					
					$property_originalPrice 		= $this->clientProperty->getContactPrice($value);
					$getcontract 					= $this->clientProperty->getContractInfo($value);
					$getTotalPayment				= $this->clientProperty->getTotalPayment($value);
					$getPropertyDetails   			= $this->clientProperty->getThisPropertyDetails($value);
					

					$multipledata [] =[
						'PropertyData' 						=> $clientProperty[0],
						// 'ClientPropertyPaymentHistory' 		=> $datas,
						'Totalbalance' 						=> $totalBalance,
						'MonthlyAmort' 						=> $monthlyAmortization,
						'ContractPrice' 					=> $originalPrice,
						'PaymentStat' 						=> $paymentStat,
						'ClientPropertyDetails' 			=> $getPropertyDetails
					];
				
				}
			
				return $this->returnThis($response,[
					'success'		=> true,
					'has_login' 	=> true,
					'result' 		=> $multipledata,
					'option'		=> 	$paymentopt 
				]);	

			}
			
		}

		
	}



	public function getInfoForThisClient($request, $response, $args){

		$id = $request->getParam('id');
		$role = $_SESSION['UserRole'];
		$datas = $this->client->getInfoForThisClient($id);
		$cp = $this->getPropertyForThisClient($id);

		// echo  $id . "<pre>";
		// print_r($cp);

		return $this->returnThis($response,[
			'id'			=> $id,
			'success'		=> true,
			'has_login' 	=> true,
			'datas'			=> $datas,
			'cpList'		=> $cp,
			'role'			=> $role

		]);


	}




	public function uploadThisImage($request, $response, $args){

		// return $this->returnThis($response,[
		// 	'datas'			=> basename($_FILES['file_uploads']['name'])
		// ]);



		// $success = true;
		try {
			$clientID = $request->getParam('dataID');

			$client = $this->client->getInfoForThisClient($clientID);
			if(count($client) != 1){
				return $this->returnThis($response,[
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Invalid client ID.'
				]);
			}


			// if($target_file != ""){
			$upload_errors = array(
				UPLOAD_ERR_OK => "No Errors",
				UPLOAD_ERR_INI_SIZE => "Larger than upload max_filesize.",
				UPLOAD_ERR_FORM_SIZE => "Larger than FORM_MAX_FILE_SIZE.",
				UPLOAD_ERR_PARTIAL => "Partial Upload.",
				UPLOAD_ERR_NO_FILE => "No File.",
				UPLOAD_ERR_NO_TMP_DIR => "No Temporary Directory.",
				UPLOAD_ERR_CANT_WRITE => "Cant write to disk.",
				UPLOAD_ERR_EXTENSION => "File upload stopped by extension."
			);
			$tmp_file = $_FILES['file_uploads']['tmp_name'];
			$target_file = basename($_FILES['file_uploads']['name']);// original filename
			$ext = pathinfo($target_file, PATHINFO_EXTENSION);



			if(empty($clientID)){
				return $this->returnThis($response,[
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Client ID is invalid'
				]);
			}
			else
				$target_file = $clientID.'.'.$ext;

			$upload_dir = 'uploads/client/'.$clientID;
			$itemurls =  "uploads/client/".ucwords($target_file);



			if(!is_dir($upload_dir)) {
	            mkdir($upload_dir, 0777, true);
	        }

	        if ( !is_writable($upload_dir)) {
			    return $this->returnThis($response,[
						'success'		=> false,
						'has_login' 	=> true,
						'message'		=> 'File directory is not writable.'
					]);
			}




			if(!move_uploaded_file($tmp_file, $upload_dir."/".$target_file)){
				return $this->returnThis($response,[
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Failed to upload image.'.$_FILES["file_uploads"]["error"]
				]);
			}


			$datas = $this->client->updateClientImagePath($target_file,$clientID);

			if($datas)
				return $this->returnThis($response,[
					'success'		=> true,
					'has_login' 	=> true,
					'message'		=> 'Upload successfull.'
				]);
			else
				return $this->returnThis($response,[
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Failed to save image path to database.'
				]);

		} catch (Exception $e) {
			return $this->returnThis($response,[
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Failed to upload image. '.json_encode($e)
			]);
		}



	}





	public function removeThisClient($request, $response, $args){

		$id = $request->getParam('propID');
		$type = $request->getParam('propertyType');

		$result = $this->client->removeThisProperty($id);

		if($result)
			return $this->returnThis($response,[
				'success'			=> true,
				'has_login' 		=> true,
				'message'			=> 'Property removed.',
				'id'				=> $id,
				'properties'		=> $this->property->getAllProperty($type)
			]);
		else
			return $this->returnThis($response,[
				'success'			=> false,
				'has_login' 		=> true,
				'message'			=> 'Property failed to removed.'
			]);

	}







	public function updateThisClient($request, $response, $args){

		$emptyField = [];


		$id                 				= $request->getParam('id');
		$update_clientFname                	= $request->getParam('update_clientFname');
		$update_clientLname                	= $request->getParam('update_clientLname');
		$update_clientMname                	= $request->getParam('update_clientMname');
		$update_clientAddress               = $request->getParam('update_clientAddress');
		$update_clientContactNumber         = $request->getParam('update_clientContactNumber');
		$update_clientEmail                	= $request->getParam('update_clientEmail');
		$update_Remarks                 	= $request->getParam('update_Remarks');


 		$inputs = [$id,$update_clientFname,$update_clientLname,$update_clientMname,$update_clientAddress,$update_clientContactNumber,$update_clientEmail,];

 		if(!isset($id) || !isset($update_clientFname) || !isset($update_clientLname) || !isset($update_clientMname) || !isset($update_clientAddress) || !isset($update_clientContactNumber) || !isset($update_clientEmail)){
 			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'result'	=> $inputs,
				'message'	=> 'Some feilds are undefined'
			]);
 		}
 		else if($id == "" || $update_clientFname == "" || $update_clientLname == ""  || $update_clientAddress == "" || $update_clientContactNumber == ""){
 			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'result'	=> $inputs,
				'message'	=> 'Some feilds are empty'
			]);
 		}
 		else if($update_clientEmail != ""){
 			if(!$this->isValidEmail($update_clientEmail)){
 				return $this->returnThis($response,[
					'success'	=> false,
					'has_login' => true,
					'result'	=> $inputs,
					'message'	=> 'Invalid email.'
				]);
 			}
 		}

		$updateClient_fname						= ucwords(strtolower($update_clientFname));
		$updateClient_lname						= ucwords(strtolower($update_clientLname));
		$updateClient_mname						= ucwords(strtolower($update_clientMname));
		$updateClient_address					= ucwords(strtolower($update_clientAddress));
		$update_Remarks							= ucwords(strtolower($update_Remarks));
		$updateClient_contactnumber				= $request->getParam('update_clientContactNumber');
		$updateClient_email						= $request->getParam('update_clientEmail');
		$clientID								= $request->getParam('id');

		$addedByUserID = $_SESSION['userID'];




		$result = $this->client->updateThisClient($updateClient_fname,$updateClient_lname,$updateClient_mname,$updateClient_address,$updateClient_contactnumber,$updateClient_email,$clientID,$update_Remarks);

		if($result){
			return $this->returnThis($response,[
				'success'			=> true,
				'has_login' 		=> true,
				'result'			=> $result,
				'message'			=> 'Client named '.$updateClient_fname .' '.$updateClient_lname.' updated.'
			]);
		}
		else{
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'result'			=> $result,
				'message'	=> 'Client named '.$updateClient_fname .' '.$updateClient_lname.' failed to be updated.'
			]);
		}
	}


	// additional property for client
	public function addThisMoreProperty($request, $response, $args){

        $clientID								= $request->getParam('clientID');
		$chargesDetails							= $request->getParam('chargesDetails');
		$addClientagent							= $request->getParam('addClientagent');
		$propertySelected						= $request->getParam('propertySelected');
		$commissionPercentage					= $request->getParam('commissionPercentage');

		$inputs = [$clientID,$chargesDetails,$addClientagent,$propertySelected,$commissionPercentage];

		if(!isset($clientID)|| !isset($addClientagent) ){
			return $this->returnThis($response , [
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Some feilds are missing.',
				'variables' 	=> $inputs
			]);
		}
		else if( $clientID == ""  ){
			return $this->returnThis($response , [
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Some feilds are empty.',
				'variables' 	=> $inputs
			]);
		}
		else if( !$this->isValidInteger($addClientagent) ){
			return $this->returnThis($response , [
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Some feilds are invalid.',
				'variables' 	=> [$addClientagent]
			]);
		}
		else if( count($this->client->thisClientExistByID($clientID)) <= 0 ){
			return $this->returnThis($response , [
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'This client does not exist'
			]);
		}


		$addedByUserID = $_SESSION['userID'];
		$ornumbers = [];

		foreach ($propertySelected as $key => $property) {
			$ornumber								= trim($property['ornumber']);
			$modeOfpayment							= trim($property['modeOfpayment']);
			$clientremitanceCentername				= strtoupper($property['clientremitanceCentername']);

			$parentID								= trim($property['parentID']);
			$blockNumber							= trim($property['block']);
			$lotNumber								= trim($property['lot']);
			$addClientprop_downpayment				= trim($property['addClientprop_downpayment']);
			$addClientDueDate						= trim($property['addClientDueDate']);
			$addClientprop_dateapplied				= trim($property['addClientprop_dateapplied']);
			$addClientprop_pricePerm2			    = trim($property['addClientprop_pricePerm2']);
			$addClientprop_planterms				= trim($property['addClientprop_planterms']);


			if($addClientprop_downpayment == ''){
				$addClientprop_downpayment = '0';
			}


			if(!isset($parentID)|| !isset($modeOfpayment) || !isset($clientremitanceCentername) || !isset($blockNumber)|| !isset($lotNumber) || !isset($addClientprop_downpayment)
				|| !isset($addClientprop_dateapplied)|| !isset($addClientprop_pricePerm2)|| !isset($addClientprop_planterms)){
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds are missing.',
					'variables' 	=> $property
				]);
			}
			else if( $parentID == "" || $parentID == "0"  ||  $blockNumber == "" || $blockNumber == "0"  ||  $lotNumber == "" || $lotNumber == "0"
				||  $addClientprop_dateapplied == ""   ||  $addClientprop_pricePerm2 == "" || $addClientprop_pricePerm2 == "0"
				||  $addClientprop_planterms == "" || $addClientprop_planterms == "0"  ){
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds are empty.',
					'variables' 	=> $property
				]);
			}
			else if($addClientprop_downpayment != '0'){
				if( !$this->isValidDecimal($addClientprop_downpayment)){
					return $this->returnThis($response , [
						'success'		=> false,
						'has_login' 	=> true,
						'message'		=> 'Some feilds has invalid value. 1',
						'variables' 	=> $property
					]);
				}
				else if ($modeOfpayment == 2 || $modeOfpayment == 3){
					if($clientremitanceCentername == ""){
						return $this->returnThis($response , [
							'success'		=> false,
							'has_login' 	=> true,
							'message'		=> 'Payment source name is required.',
							'variables' 	=> $property
						]);
					}
					else if(in_array($ornumber, $ornumbers)){
						return $this->returnThis($response , [
							'success'		=> false,
							'has_login' 	=> true,
							'message'		=> 'OR number must be unique.',
							'variables' 	=> $property
						]);
					}
					else if(count($this->clientPaymentHistory->isORnumberused($ornumber)) >= 1){
						return $this->returnThis($response , [
							'success'		=> false,
							'has_login' 	=> true,
							'message'		=> 'OR number was used already.',
							'variables' 	=> $property
						]);
					}
					else
						$ornumbers[] = $ornumber;
				}
			}
			else if( !$this->isValidInteger($parentID) ||!$this->isValidInteger($blockNumber) ||!$this->isValidInteger($lotNumber) ||!$this->isValidInteger($addClientprop_planterms) || !$this->isValidDecimal($addClientprop_pricePerm2)  ){
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds has invalid value. 2',
					'variables' 	=> $property
				]);
			}
			else if(!$this->checkThisdate($addClientprop_dateapplied) ){
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds has invalid date value.',
					'variables' 	=> $property
				]);
			}
		}



		$comssionParamerters = ['directCom','SalesDirectorCom','SalesManagerCom','UnitManagerCom'];





		$agent = $this->client->getThisAgent($addClientagent)[0];
		// tbl_agent.agent_id,
	    // tbl_agent.agentPosition,
	    // tbl_agent.salesDirectorID,
	    // tbl_agent.salesManagerID,
	    // tbl_agent.unitManagerID

		$total = 0;

		if($agent['agentPosition']  == 1){
			if(!isset($commissionPercentage['directCom'])){
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds are missing. (1)',
					'variables' 	=> $commissionPercentage
				]);
			}

			$total += intval($commissionPercentage['directCom']);
		}
		else if($agent['agentPosition']  == 2){
			if(!isset($commissionPercentage['directCom']) || !isset($commissionPercentage['SalesDirectorCom'])){
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds are missing. (2)',
					'variables' 	=> $commissionPercentage
				]);
			}

			$total += intval($commissionPercentage['directCom']);
			$total += intval($commissionPercentage['SalesDirectorCom']);
		}
		else if($agent['agentPosition']  == 3){
			if(!isset($commissionPercentage['directCom']) || !isset($commissionPercentage['SalesDirectorCom']) || !isset($commissionPercentage['SalesManagerCom'])){
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds are missing. (3)',
					'variables' 	=> $commissionPercentage
				]);
			}
			$total += intval($commissionPercentage['directCom']);
			$total += intval($commissionPercentage['SalesDirectorCom']);
			$total += intval($commissionPercentage['SalesManagerCom']);
		}else if($agent['agentPosition']  == 4){
			if(!isset($commissionPercentage['directCom']) || !isset($commissionPercentage['SalesDirectorCom']) || !isset($commissionPercentage['SalesManagerCom']) || !isset($commissionPercentage['UnitManagerCom'])){
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds are missing. (4)',
					'variables' 	=> $commissionPercentage
				]);
			}
			$total += intval($commissionPercentage['directCom']);
			$total += intval($commissionPercentage['SalesDirectorCom']);
			$total += intval($commissionPercentage['SalesManagerCom']);
			$total += intval($commissionPercentage['UnitManagerCom']);

		}else if($agent['agentPosition']  == 5){

			if(!isset($commissionPercentage['directCom']) || !isset($commissionPercentage['SalesDirectorCom']) || !isset($commissionPercentage['SalesManagerCom']) || !isset($commissionPercentage['UnitManagerCom'])){
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds are missing. (5)',
					'variables' 	=> $commissionPercentage
				]);
			}
			$total += intval($commissionPercentage['directCom']);
			$total += intval($commissionPercentage['SalesDirectorCom']);
			$total += intval($commissionPercentage['SalesManagerCom']);
			$total += intval($commissionPercentage['UnitManagerCom']);
			$total += intval($commissionPercentage['ManagerCom']);

		}else{
			return $this->returnThis($response , [
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Invalid agent position',
				'variables' 	=> $commissionPercentage
			]);
		}

		if($total != 15){
			return $this->returnThis($response , [
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Percentage commission must 15 in total.',
				'variables' 	=> $commissionPercentage
			]);
		}

		$result = $this->client->insertThisMoreClientProperties(
				$clientID,
				$chargesDetails,
				$propertySelected,
				$addClientagent,
				$commissionPercentage,
				$addedByUserID
			);

		if($result){
			return $this->returnThis($response,[
				'success'	=> true,
				'has_login' => true,
				'id'		=> $this->client->getLastClientInsertedID(),
				'message'	=> 'Client more property has been added.',
				'dateNow'	=> $this->client->dateNow
			]);
		}
		else{
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Client  more property failed to be added.'
			]);
		}


	}






	public function postThisClient($request, $response, $args){

        $addClient_fname						= ucwords(strtolower($request->getParam('addClient_fname')));
		$addClient_lname						= ucwords(strtolower($request->getParam('addClient_lname')));
		$addClient_mname						= ucwords(strtolower($request->getParam('addClient_mname')));
		$addClient_address						= ucwords(strtolower($request->getParam('addClient_address')));

		$addClient_contactnumber				= $request->getParam('addClient_contactnumber');
		$chargesDetails							= $request->getParam('chargesDetails');
		$addClient_email						= $request->getParam('addClient_email');
		$addClientagent							= $request->getParam('addClientagent');

		$spouse 								= $request->getParam('spouse');
		$benif  								= $request->getParam('benif');

		// $commissionReleased 					= $_POST['comission_release'];
		// $addCommission		 					= $_POST['add_commission'];

		if(!isset($addClient_fname)|| !isset($addClient_lname)|| !isset($addClient_address) || !isset($addClient_contactnumber) || !isset($addClientagent)){
			return $this->returnThis($response , [
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Some feilds are missing.',
				'variables' 	=> [$addClient_fname,$addClient_lname,$addClient_address,$addClient_contactnumber,$addClientagent]
			]);
		}
		else if( $addClient_fname == "" || $addClient_lname == "0"  ||  $addClient_address == ""  ||  $addClient_contactnumber == ""  ||  $addClientagent == "" ){
			return $this->returnThis($response , [
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Some feilds are empty.',
				'variables' 	=> [$addClient_fname,$addClient_lname,$addClient_address,$addClient_contactnumber,$addClientagent]
			]);
		}
		else if($addClient_email != ""  ){
			if(!$this->isValidEmail( $addClient_email ))
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds are invalid.',
					'variables' 	=> [$addClient_email]
				]);
		}
		else if( !$this->isValidInteger($addClientagent) ){
			return $this->returnThis($response , [
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Some feilds are invalid.',
				'variables' 	=> [$addClientagent]
			]);
		}
		else if( count($this->client->thisClientExist($addClient_fname,$addClient_lname)) >= 1 ){
			return $this->returnThis($response , [
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'This client already exist'
			]);
		}


		$addedByUserID = $_SESSION['userID'];
		$ornumbers = [];
		$propertySelected								= $request->getParam('propertySelected');
		foreach ($propertySelected as $key => $property) {
			$ornumber								= trim($property['ornumber']);
			$modeOfpayment							= trim($property['modeOfpayment']);
			$clientremitanceCentername				= strtoupper($property['clientremitanceCentername']);

			$parentID								= trim($property['parentID']);
			$blockNumber							= trim($property['block']);
			$lotNumber								= trim($property['lot']);
			$addClientprop_downpayment				= trim($property['addClientprop_downpayment']);
			$addClientprop_dateapplied				= trim($property['addClientprop_dateapplied']);
			$addClientprop_pricePerm2			    = trim($property['addClientprop_pricePerm2']);
			$addClientprop_planterms				= trim($property['addClientprop_planterms']);
			// $commissionReleased						= $property['comission_release']);
			// $addCommission							= $property['add_commission']);

			if($addClientprop_downpayment == ''){
				$addClientprop_downpayment = '0';
			}


			if(!isset($parentID)|| !isset($modeOfpayment) || !isset($clientremitanceCentername) || !isset($blockNumber)|| !isset($lotNumber) || !isset($addClientprop_downpayment)
				|| !isset($addClientprop_dateapplied)|| !isset($addClientprop_pricePerm2)|| !isset($addClientprop_planterms)){
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds are missing.',
					'variables' 	=> $property
				]);
			}
			else if( $parentID == "" || $parentID == "0"  ||  $blockNumber == "" || $blockNumber == "0"  ||  $lotNumber == "" || $lotNumber == "0"
				||  $addClientprop_dateapplied == ""   ||  $addClientprop_pricePerm2 == "" || $addClientprop_pricePerm2 == "0"
				||  $addClientprop_planterms == "" || $addClientprop_planterms == "0"  ){
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds are empty.',
					'variables' 	=> $property
				]);
			}
			else if($addClientprop_downpayment != '0'){
				if( !$this->isValidDecimal($addClientprop_downpayment)){
					return $this->returnThis($response , [
						'success'		=> false,
						'has_login' 	=> true,
						'message'		=> 'Some feilds has invalid value. 1',
						'variables' 	=> $property
					]);
				}
				else if ($modeOfpayment == 2 || $modeOfpayment == 3){
					if($clientremitanceCentername == ""){
						return $this->returnThis($response , [
							'success'		=> false,
							'has_login' 	=> true,
							'message'		=> 'Payment source name is required.',
							'variables' 	=> $property
						]);
					}
					// else if(in_array($ornumber, $ornumbers)){
					// 	return $this->returnThis($response , [
					// 		'success'		=> false,
					// 		'has_login' 	=> true,
					// 		'message'		=> 'OR number must be unique.',
					// 		'variables' 	=> $property
					// 	]);
					// }
					// else if(count($this->clientPaymentHistory->isORnumberused($ornumber)) >= 1){
					// 	return $this->returnThis($response , [
					// 		'success'		=> false,
					// 		'has_login' 	=> true,
					// 		'message'		=> 'OR number was used already.',
					// 		'variables' 	=> $property
					// 	]);
					// }
					else
						$ornumbers[] = $ornumber;
				}
			}
			else if( !$this->isValidInteger($parentID) ||!$this->isValidInteger($blockNumber) ||!$this->isValidInteger($lotNumber) ||!$this->isValidInteger($addClientprop_planterms) || !$this->isValidDecimal($addClientprop_pricePerm2)  ){
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds has invalid value. 2',
					'variables' 	=> $property
				]);
			}
			else if(!$this->checkThisdate($addClientprop_dateapplied) ){
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds has invalid date value.',
					'variables' 	=> $property
				]);
			}
		}



		$comssionParamerters = ['directCom','SalesDirectorCom','SalesManagerCom','UnitManagerCom','ManagerCom'];


		$commissionPercentage = $request->getParam('commissionPercentage');


		$agent = $this->client->getThisAgent($addClientagent)[0];
		// tbl_agent.agent_id,
	    // tbl_agent.agentPosition,
	    // tbl_agent.salesDirectorID,
	    // tbl_agent.salesManagerID,
	    // tbl_agent.unitManagerID

		$total = 0;

		if($agent['agentPosition']  == 1){
			if(!isset($commissionPercentage['directCom'])){
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds are missing. (1)',
					'variables' 	=> $commissionPercentage
				]);
			}

			$total += intval($commissionPercentage['directCom']);
		}
		else if($agent['agentPosition']  == 2){
			if(!isset($commissionPercentage['directCom']) || !isset($commissionPercentage['SalesDirectorCom'])){
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds are missing. (2)',
					'variables' 	=> $commissionPercentage
				]);
			}

			$total += intval($commissionPercentage['directCom']);
			$total += intval($commissionPercentage['SalesDirectorCom']);
		}
		else if($agent['agentPosition']  == 3){
			if(!isset($commissionPercentage['directCom']) || !isset($commissionPercentage['SalesDirectorCom']) || !isset($commissionPercentage['SalesManagerCom'])){
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds are missing. (3)',
					'variables' 	=> $commissionPercentage
				]);
			}
			$total += intval($commissionPercentage['directCom']);
			$total += intval($commissionPercentage['SalesDirectorCom']);
			$total += intval($commissionPercentage['SalesManagerCom']);
		
		}else if($agent['agentPosition']  == 4){

			if(!isset($commissionPercentage['directCom']) || !isset($commissionPercentage['SalesDirectorCom']) || !isset($commissionPercentage['SalesManagerCom']) || !isset($commissionPercentage['UnitManagerCom'])){
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds are missing . (4)',
					'variables' 	=> $commissionPercentage
				]);
			}
			$total += intval($commissionPercentage['directCom']);
			$total += intval($commissionPercentage['SalesDirectorCom']);
			$total += intval($commissionPercentage['SalesManagerCom']);
			$total += intval($commissionPercentage['UnitManagerCom']);


		}else if($agent['agentPosition']  == 5){

			if(!isset($commissionPercentage['directCom']) || !isset($commissionPercentage['SalesDirectorCom']) || !isset($commissionPercentage['SalesManagerCom']) || !isset($commissionPercentage['UnitManagerCom'])){
				return $this->returnThis($response , [
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Some feilds are missing. (5)',
					'variables' 	=> $commissionPercentage
				]);
			}
			$total += intval($commissionPercentage['directCom']);
			$total += intval($commissionPercentage['SalesDirectorCom']);
			$total += intval($commissionPercentage['SalesManagerCom']);
			$total += intval($commissionPercentage['UnitManagerCom']);
			$total += intval($commissionPercentage['ManagerCom']);

		}else{

			return $this->returnThis($response , [
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Invalid agent position',
				'variables' 	=> $commissionPercentage
			]);
		}



		if($total != 15){
			return $this->returnThis($response , [
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Percentage commission must 15 in total.',
				'variables' 	=> $commissionPercentage
			]);
		}


		$result = $this->client->insertThisClientWithProperties(
				$addClient_fname,
				$addClient_lname,
				$addClient_mname,
				$addClient_address,
				$addClient_contactnumber,
				$addClient_email,

				$chargesDetails,
				$propertySelected,
				$addClientagent,
				$commissionPercentage,
				$addedByUserID,
				$spouse,
				$benif
			);

		if($result){
			return $this->returnThis($response,[
				'success'	=> true,
				'has_login' => true,
				'id'		=> $this->client->getLastClientInsertedID(),
				'message'	=> 'Client named '.$addClient_fname .' '.$addClient_lname.' added.',
				'dateNow'	=> $this->client->dateNow
			]);
		}
		else{
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Client named '.$addClient_fname .' '. $addClient_lname.' failed to be added.'
			]);
		}


	}

	// property forfeited
	public function getThisClientPropertyInfoAndHistoryforfeited($request,$response,$args)
	{
		 	$cp_id 						= $request->getParam('id');
		 	$ClientID 					= $request->getParam('ClientID');

			$propertyInfo 				= $this->clientProperty->getPropertyInfoforfeited($cp_id);
			
			// $additionaleCharges         = $this->clientProperty->getAdditionalChargesforfeited($cp_id);

			// echo "<pre>";
			// print_r( json_decode($additionaleCharges[0]['additionalCharges']));

			// if (!$additionaleCharges) {
			//  	return $this->returnThis($response,[
			// 		'success'		=> false,
			// 		'has_login' 	=> true,
			// 		'message'		=> "Uanable to get additional charges!"
			// 	]);
			// }

			$contractPrice = 0;
			$additionalCharges = 0;
			if(count($propertyInfo) == 1){
				$contractPrice 					= $propertyInfo[0]['contractPrice'];
				$additionalCharges 				= $propertyInfo[0]['additionalCharges'];

				$plan_terms 		= $propertyInfo[0]['plan_terms'];
				$sqm				= $propertyInfo[0]['sqm'];
				$sqmPricem2			= $propertyInfo[0]['pricePerm2'];


				if(!empty($additionalCharges)){
					$additionalCharges = json_decode($additionalCharges);
					if(count($additionalCharges) != 0){


						$totalChargeValue = 0;
						foreach ($additionalCharges as $key => $value) {
							$totalChargeValue += floatval($value->chargeValue);
						}

						$propertyInfo[0]['additionalCharges'] = $totalChargeValue;
						$propertyInfo[0]['contractPrice'] = (($sqmPricem2 * $sqm));
						$propertyInfo[0]['monthlyAmortization'] = ((($sqmPricem2 * $sqm)) / $plan_terms);
						$contractPrice  = $propertyInfo[0]['contractPrice'];
					}
				}
				else{
					$propertyInfo[0]['additionalCharges'] = $totalChargeValue;
				}
			}

			// 2
			$result 					= $this->clientPaymentHistory->getPropertiesForThisclientforfietd($cp_id, $ClientID);
			$comReleased 				= $this->clientPaymentHistory->getComReleasedforfieted($cp_id);
			

			$comVal = "";
			if ($comReleased) {
				$comVal = $comReleased[0]['commissionReleased'];
			}

			$propertyPaymentHistory = [];
			$testinglang = $this->commission->testinglang($cp_id);
			foreach ($result as $key => $value) {
				$contractPrice -= ($value['propertyCredit'] + $value['propertyDebit']);
				$commission = ((($value['propertyCredit'] + $value['propertyDebit']) * 0.15 * .05) / $comVal );
				if($value['particulars'] == 'Downpayment'){
					$propertyPaymentHistory[] = [
						0 => ' ',
						1 => $value['orNumber'],
						2 => $value['particulars'],
						3 => $value['dateAdded'],
						4 => $value['propertyDebit'] == '0.00' ? '0.00' : $value['propertyDebit'] ,
						5 => $value['propertyCredit'] == '0.00' ? '0.00' : $value['propertyCredit'] ,
						6 => $contractPrice,
						7 => ($testinglang[0]['remittanceCenter'] == '' ? 'Cash' : $testinglang[0]['remittanceCenter']),
						// 8 => $value['dueDate']
						// 6 => $commission ,
						// 7 => $value['dateClaimed'] == '0000-00-00' ? '' : $value['dateClaimed'],
					];
				}else{
					$propertyPaymentHistory[] = [
						0 => $key+1,
						1 => $value['orNumber'],
						2 => $value['particulars'],
						3 => $value['dateAdded'],
						4 => $value['propertyDebit'] == '0.00' ? '0.00' : $value['propertyDebit'] ,
						5 => $value['propertyCredit'] == '0.00' ? '0.00' : $value['propertyCredit'] ,
						6 => $contractPrice,
						7 => ($testinglang[0]['remittanceCenter'] == '' ? 'Cash' : $testinglang[0]['remittanceCenter']),
						// 8 => $value['dueDate']
						// 6 => $commission ,
						// 7 => $value['dateClaimed'] == '0000-00-00' ? '' : $value['dateClaimed'],
					];
				}
			
			}

			return $this->returnThis($response,[
					'success'					=> true,
					'has_login' 				=> true,
					'result' 				    => $result,
					'propertyPaymentHistory'	=> $propertyPaymentHistory,
					'propertyInfo'				=> $propertyInfo,
					'additionalCharges'			=> $additionalCharges
				]);
	}

	public function getAllclientwithLatepayment($request,$response,$args)
	{
			// 2
			$result = $this->clientProperty->getLatePayments();

			$latepayments = [];
			$count = 1;
			foreach ($result as $key => $value) {
				if($value['diff'] > 0){
					$latepayments[] = [
						0 => $count,
						1 => $value['fName'],
						2 => $value['propertyName'],
						3 => $value['block'],
						4 => $value['lot'],
						5 => $value['diff'],
						// 8 => $value['dueDate']
						// 6 => $commission ,
						// 7 => $value['dateClaimed'] == '0000-00-00' ? '' : $value['dateClaimed'],
					];
					$count ++;
				}
			
			}

			return $this->returnThis($response,[
					'success'					=> true,
					'has_login' 				=> true,
					'result' 				    => $result,
					'latepayments'				=> $latepayments,
				]);
	}
		public function getLateclientswithPropID($request,$response,$args)
	{
		$parentID = $request->getParam('properTyID');
			// 2
			$result = $this->clientProperty->getLatePaymentswithPropID($parentID);

			$latepayments = [];
			$count = 1;
			foreach ($result as $key => $value) {
				if($value['diff'] > 0){
					$latepayments[] = [
						0 => $count,
						1 => $value['fName'],
						2 => $value['propertyName'],
						3 => $value['block'],
						4 => $value['lot'],
						5 => $value['diff'],
						// 8 => $value['dueDate']
						// 6 => $commission ,
						// 7 => $value['dateClaimed'] == '0000-00-00' ? '' : $value['dateClaimed'],
					];
					$count ++;
				}
			
			}

			return $this->returnThis($response,[
					'success'					=> true,
					'has_login' 				=> true,
					'result' 				    => $result,
					'latepayments'				=> $latepayments,
				]);
	}
	public function sendClientwithPropID($request,$response,$args)
	{
	$parentID = $request->getParam('properTyID');
			
			$result = $this->clientProperty->getLatePaymentswithPropID($parentID);
		
			// $latepayments = [];
			$count = 0;
			foreach ($result as $key => $value) {
				// if($value['diff'] > 3 && $value['diff'] < 8){
					if($value['diff'] > 4 && $value['diff'] < 13){
					
					$number = $value['ContactNumber'];
					$message = "Good day Sir/Maam " . $value['fName'] . " a gentle reminder on your property " . $value['propertyName'] . " block: " . $value['block'] . " lot: " . $value['lot'] ." has " . $value['diff'] ." month/s delayed payments. Please pay your remaining balance if you still want to continue your lot installment. Disregard this if already settled. Message from R and Sons Properties Co.";
				if (strlen($value['ContactNumber'])==11) {
					$respondz = $this->sendSMS($number,$message);

					}
		
						// 8 => $value['dueDate']
						// 6 => $commission ,
						// 7 => $value['dateClaimed'] == '0000-00-00' ? '' : $value['dateClaimed'],
					// itexmo($number,$message,$apicode,$passwd);	
					$count ++;
				
				}
				}
		return $this->returnThis($response,[
					'success'					=> true,
					'has_login' 				=> true,
					'result' 				    => $respondz,
					// 'latepayments'				=> $count,
				]);
	}
	public function sendSMS($number,$message){
		$url = 'https://api.m360.com.ph/v3/api/broadcast';

	$ch = curl_init($url);

$data = array("app_key" => "PFEvVnubdtB9brMj","app_secret" => "BoH97PfzCIOG0BT7nM0sZgAu7IazQ8tK","msisdn"=> $number,"content" =>$message,"shortcode_mask"=>"RandSonsPco","rcvd_transid"=>"","is_intl"=>"false");

$postdata = json_encode($data);

$ch = curl_init($url); 
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
$result = curl_exec($ch);
curl_close($ch);

	

		return (1);
}
	// get client property info from client propertes
	public function getThisClientPropertyInfoAndHistory($request,$response,$args){

		$cp_id 						= $request->getParam('id');

		$propertyInfo 				= $this->clientProperty->getPropertyInfo($cp_id);
		$additionaleCharges         = $this->clientProperty->getAdditionalCharges($cp_id);

		if (!$additionaleCharges) {
		 	return $this->returnThis($response,[
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> "Uanable to get additional charges!"
			]);
		}

		$contractPrice = 0;
		$additionalCharges = 0;
		if(count($propertyInfo) == 1){
			$contractPrice 					= $propertyInfo[0]['contractPrice'];
			$additionalCharges 				= $propertyInfo[0]['additionalCharges'];

			$plan_terms 		= $propertyInfo[0]['plan_terms'];
			$dueDate 		    = $propertyInfo[0]['dueDate'];
			$sqm				= $propertyInfo[0]['sqm'];
			$sqmPricem2			= $propertyInfo[0]['pricePerm2'];
			$originDate			= $propertyInfo[0]['dateApplied'];
			$downPayment		= $propertyInfo[0]['downpayment'];

			if(!empty($additionalCharges)){
				$additionalCharges = json_decode($additionalCharges);
				if(count($additionalCharges) != 0){

					$totalChargeValue = 0;
					foreach ($additionalCharges as $key => $value) {
						$totalChargeValue += floatval($value->chargeValue);
					}

					$propertyInfo[0]['additionalCharges'] = $totalChargeValue;
					$propertyInfo[0]['contractPrice'] = (($sqmPricem2 * $sqm));
					$propertyInfo[0]['monthlyAmortization'] = ((($sqmPricem2 * $sqm) - $downPayment ) / $plan_terms);
					$contractPrice  = $propertyInfo[0]['contractPrice'];
				}
			}
			else{
				$propertyInfo[0]['additionalCharges'] = $totalChargeValue;
			}
		}

		// 2
		$result 			= $this->clientPaymentHistory->getPropertiesForThisclient($cp_id);
		$comReleased 		= $this->clientPaymentHistory->getComReleased($cp_id);
		

		$comVal = "";
		if (!is_null($comReleased)) {
			$comVal = $comReleased[0]['commissionReleased'];

			// return $this->returnThis($response,[
			// 	'success'		=> false,
			// 	'has_login' 	=> true,
			// 	'message'		=> "Commission release value is empty, please update it!"
			// ]);
		}



		$propertyPaymentHistory = [];
		$counter				=1;
		$testinglang     = $this->commission->testinglang($cp_id);
		foreach ($result as $key => $value) {
			 $repeat = strtotime("+1 Month",strtotime($originDate));
			$contractPrice -= ($value['propertyCredit'] + $value['propertyDebit']);
			// $commission = ((($value['propertyCredit'] + $value['propertyDebit']) * 0.15 * .05) / $comVal );
			if($value['particulars'] == 'Downpayment'){
				$propertyPaymentHistory[] = [
					0 => '',
					1 => $value['orNumber'],
					2 => 'Downpayment',
					3 => $value['dateAdded'],
					4 => $value['propertyDebit'] == '0.00' ? '0.00' : $value['propertyDebit'] ,
					5 => $value['propertyCredit'] == '0.00' ? '0.00' : $value['propertyCredit'] ,
					6 => $contractPrice,
					7 => $value['remittanceCenter'] =='' ? : 'Cash',
					// 8 => $value['dueDate']
					// 6 => $commission ,
					// 7 => $value['dateClaimed'] == '0000-00-00' ? '' : $value['dateClaimed'],
				];
			}else{
				$propertyPaymentHistory[] = [
					0 => $counter,
					1 => $value['orNumber'],
					2 => date("F, Y",strtotime($originDate)),
					3 => $value['dateAdded'],
					4 => $value['propertyDebit'] == '0.00' ? '0.00' : $value['propertyDebit'] ,
					5 => $value['propertyCredit'] == '0.00' ? '0.00' : $value['propertyCredit'] ,
					6 => $contractPrice,
					7 => $value['remittanceCenter'] =='' ? 'Cash': $value['remittanceCenter'],
					// 8 => $value['dueDate']
					// 6 => $commission ,
					// 7 => $value['dateClaimed'] == '0000-00-00' ? '' : $value['dateClaimed'],
				];
				
				$counter++;
			}
			$originDate = date('Y-m-d',$repeat);
		}

		return $this->returnThis($response,[
				'success'					=> true,
				'has_login' 				=> true,
				'result' 				    => $result,
				'propertyPaymentHistory'	=> $propertyPaymentHistory,
				'propertyInfo'				=> $propertyInfo,
				'additionalCharges'			=> $additionalCharges,
				'test'						=> $testinglang 
			]);
	}

	// 3
	public function getPropertyForThisClient($id){
	 	$propertyStatus = $this->clientProperty->getPropertiesForThisclient($id);
	 	if(count($propertyStatus) <= 0) {
	 		return $this->clientProperty->getPropertiesForThisclientForfiet($id);
	 	}else{
	 		return $this->clientProperty->getPropertiesForThisclient($id);
	 	}

	}

	public function getAgentComforClientProp($request,$response,$args)
	{
		$agentList = [];

		$clientID 					= $request->getParam('clientID');
		$propertyID					= $request->getParam('propertyID');

		$getAgentList = $this->agent->getAgentList($propertyID);
		foreach ($getAgentList as $key => $value) {
			$getAgentName = $this->agent->getAgentName($value['agentid']);

			$agentList[] = [
				'agentID' 	=> $value['agentid'],
				'agentName' => $getAgentName[0]['name'],
				'agentCom'  => $value['percentagevalue'],
				'pos'		=> $getAgentName[0]['pos']
			];
		}

		return $this->returnThis($response,[
				'success'		    => true,
				'has_login' 		=> true,
				'agentList'			=> $agentList
			]);
	}

	public function updateThisAgentComProp($request,$response,$args)
	{
		$propertyID 			    = $request->getParam('propertyID');
		$agentCom 					= $request->getParam('agentCom');
		$agentID 					= $request->getParam('agentID');

		$updateThisAgent = $this->agent->updateThisAgentCom($propertyID, $agentCom, $agentID);
		if (!$updateThisAgent) {
			return $this->returnThis($response,[
				'success'		    => false,
				'has_login' 		=> true,
				'message'			=> 'Unable to update Com'
			]);
		}

		return $this->returnThis($response,[
			'success'		    => true,
			'has_login' 		=> true,
			'message'			=> 'Agent com updated successfully!'
		]);
	}


	public function getThisDownpayment($request,$response,$args)
	{
		$propertyID 			    = $request->getParam('propertyID');
		$clientID 					= $request->getParam('clientID');

		$downpayment = $this->client->getThisClientDownPayment($propertyID);

		return $this->returnThis($response,[
			'success'		    => true,
			'has_login' 		=> true,
			'downpayment'		=> $downpayment[0]['downpayment']
		]);

	}

	public function updateThisDownpayment($request,$response,$args)
	{
		$propertyID 			    = $request->getParam('propertyID');
		$newDownPayment 		    = $request->getParam('newDown');

		$downpayment = $this->client->updateThisClientDownpayment($propertyID, $newDownPayment);
		if (!$downpayment) {
			return $this->returnThis($response,[
				'success'		    => false,
				'has_login' 		=> true,
				'message'			=> 'Unable to update downpayment'
			]);
		}

		return $this->returnThis($response,[
			'success'		    => true,
			'has_login' 		=> true,
			'message'			=> 'Downpayment successfully updated'
		]);
	}

	public function add_ThisnewSpouse($request, $response, $args)
	{
		$clID = $request->getParam('clientID');
		$data = json_encode($request->getParam('spouse'));

		$newSpouse = $this->client->addThisnewSpouse($clID, $data);
		if (!$newSpouse) {
			return $this->returnThis($response,[
				'success'		    => false,
				'message'			=> 'Unable to add new spouse.'
			]);		
		}

		return $this->returnThis($response,[
			'success'		    => true,
			'message'			=> 'Successful!'
		]);	
	}

	public function update_thisSpouse($request, $response, $args)
	{
		$clID   = $request->getParam('clientID');
		$spouse = $request->getParam('spouse');

		$updateList = $this->client->addThisnewSpouse($clID, json_encode($spouse));
		if (!$updateList) {
			return $this->returnThis($response,[
				'success'		    => false,
				'message'			=> 'Unable to continue request'
			]);				
		}

		return $this->returnThis($response,[
			'success'		    => true,
			'message'			=> 'Successful!'
		]);	
	}

	public function add_thisNewBeneficiary($request, $response, $args)
	{
		$clID   = $request->getParam('clientID');
		$benef = $request->getParam('benef');

		$newBenef = $this->client->add_newBeneficiary($clID, json_encode($benef));
		if (!$newBenef) {
			return $this->returnThis($response,[
				'success'		    => false,
				'message'			=> 'Unable to add new beneficiary!'
			]);	
		}

		return $this->returnThis($response,[
			'success'		    => true,
			'message'			=> 'Successful!'
		]);	
	}

	public function update_beneficiary($request, $response, $args)
	{
		$clID   = $request->getParam('clientID');
		$benef = $request->getParam('benef');

		$updateBenif = $this->client->add_newBeneficiary($clID, json_encode($benef));
		if (!$updateBenif) {
			return $this->returnThis($response,[
				'success'		    => false,
				'message'			=> 'Unable to add new beneficiary!'
			]);	
		}

		return $this->returnThis($response,[
			'success'		    => true,
			'message'			=> 'Successful!'
		]);	
	}


	public function getThisProperList($request, $response, $args)
	{

		$userRole = $_SESSION['UserRole'];
		$propertyList = $this->property->getPropertyParents();
		if (!$propertyList) {
			return $this->returnThis($response,[
				'success'	=> false,
				'message'	=> 'Unable to get property list!'
			]);		 	
		 } 

		return $this->returnThis($response,[
			'success'		    => true,
			'data'   			=> $propertyList,
			'role'				=> $userRole
		]);			
	}

	public function get_thisClientsfromSelectedProperty($request, $response, $args)
	{
		$propId  = $request->getParam('properTyID');

		$clientsList = $this->client->get_ThisClients($propId);
		if (!$clientsList) {
			return $this->returnThis($response,[
				'success'   => false,
				'message'   	=> "Unable to  fetch client from this property"
			]);	
		}

		$propList = $this->property->getPropertyDetailList($propId);
		if (!$clientsList) {
			return $this->returnThis($response,[
				'success'   => false,
				'message'   => "Unable to get property details!"
			]);	
		}

		$data = [];
		$data [] = [
			0 => $propList,
			1 => $clientsList,
		];

		return $this->returnThis($response,[
			'success'   => true,
			'data'   	=> $data
		]);
	}

	public function get_thisClientListByBock($request, $response, $args)
	{
		$listId  = $request->getParam('listID');

		$clientList = $this->client->getThisClientListBySelectedBlock($listId);
		if (!$clientList) {
			return $this->returnThis($response,[
				'success'   => true,
				'message'   => 'Unable to get client from this property!'
			]);		
		}

		return $this->returnThis($response,[
			'success'   => true,
			'data'   	=> $clientList
		]);
	}

}
