<?php

namespace App\Controllers;

use App\Helpers\Validation;

use App\Models\Employee;
use App\Models\Department;
use App\Models\Position;
use App\Controllers\ParentController;

  

class EmployeeController extends ParentController{

	protected $validation;
	protected $mPDF;

	private $distance;

	public function __construct(Department $department,Position $position,Employee $employee,Validation $validation, $mpdf) {

		$this->employee = $employee;
		$this->department = $department;
		$this->position = $position;
		// Validation Helper
		$this->validation = $validation;

		$this->mPDF = $mpdf;
	}	




	public function getAllPositionsAndDepartments($request, $response, $args) {
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}
		
		if(!($this->isLoginAsSuperAdmin() )){
			return $this->returnThis($response,['success'	=> false,'has_login' => true,'message'		=> 'You dont have permission to do this transaction.']);
		}
		

		$pos	= $this->position->getAllPositions();
		$dep 	= $this->department->getAllDepartments();

		$positions = [];
		foreach ($pos as $key => $value) {
			$positions[] = [
				'num' 	=> $value['PositionName']
			];
		}

		$data['positions'] = $positions;


		$departments = []; // department
		foreach ($dep as $key => $value) {
			$departments[] = [
				'num' 	=> $value['DepartmentName']
			];
		}

		$data['departments'] = $departments;

		return $this->returnThis($response , [
				'success' 	=> true,'has_login' => true,
				'data' 		=> $data
			]);
	}

	

	public function getAllEmployees($request, $response, $args) {
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}
		
		if(!($this->isLoginAsSuperAdmin())){
			return $this->returnThis($response,['success'	=> false,'has_login' => true,'message'		=> 'You dont have permission to do this transaction.']);
		}
		
		$emp = $this->employee->getAllActiveEmplyee();
		$data = [];
		foreach ($emp as $key => $value) {
			$data['employees'][] = [
				'empID'					=> $value['EmployeeID'],
				'empFname'				=> $value['Fname'] ,
				'empLname'				=> $value['Lname'],
				'empMname'				=> $value['Mname'],
				'position'				=> $value['PositionName'],
				'department'			=> $value['DepartmentName'],
			];
		}

		return $this->returnThis($response , [
				'success' 	=> true,'has_login'=>true,
				'data'		=> $data
			]);
	}


	public function removeThisEmployee($request, $response, $args) {
		$isp = $request->getParam('isp');

		if(!$this->isIpSourceValid($isp)){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'Invalid request.']);
		}

		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}
		
		if(!($this->isLoginAsSuperAdmin())){
			return $this->returnThis($response,['success'	=> false,'has_login' => true,'message'		=> 'You dont have permission to do this transaction.']);
		}
		
		$id = $request->getParam('ID');
		if(empty($id))
			return $this->returnThis($response , [
					'success' 		=> false,'has_login' => true,
					'message'		=> 'Select employee to remove.'
				]);	

		$result = $this->employee->deactivateEmployee($id);
		if($result)
			return $this->returnThis($response , [
					'success' 	=> true,'has_login' => true,
					'message'		=> 'Successfully removed employee.'
				]);	
		else
			return $this->returnThis($response , [
					'success' 	=> false,'has_login' => true,
					'message'		=> 'Failed to removed employee.'
				]);	
	}


	public function postThisEmployee($request, $response, $args) {
		$isp = $request->getParam('isp');

		if(!$this->isIpSourceValid($isp)){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'Invalid request.']);
		}

		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}
		
		if(!($this->isLoginAsSuperAdmin() )){
			return $this->returnThis($response,['success'	=> false,'has_login' => true,'message'		=> 'You dont have permission to do this transaction.']);
		}

		$fname 		= ucwords($request->getParam('Fname'));
		$lname 		= ucwords($request->getParam('Lname'));
		$mname 		= ucwords($request->getParam('Mname'));
		$Posname 	= ucwords($request->getParam('Position'));
		$Depname 	= ucwords($request->getParam('Department'));

		if(empty($fname) || empty($lname) || empty($mname) || empty($Posname) || empty($Depname))
			return $this->returnThis($response , [
					'success'	=> false,'has_login' => true,
					'message'	=> 'All feilds are required.'
				]);



		$res = $this->position->searchForThisPosition($Posname);
		if(count($res) == 0){
			$Posname = $this->position->addThisPositions($Posname);
			if($Posname)
				$Posname = $this->position->getLastID();
			else
				return $this->returnThis($response , [
					'success'	=> false,'has_login' => true,
					'message'	=> 'Failed to add new position'
				]);
		}
		else{
			$Posname =  $res[0]['PositionID'];
		}




		$res = $this->department->searchForThisDepartment($Depname);
		if(count($res) == 0){
			$Depname = $this->department->addThisDepartment($Depname);
			if($Depname)
				$Depname =  $this->department->getLastID();
			else
				return $this->returnThis($response , [
					'success'	=> false,'has_login' => true,
					'message'	=> 'Failed to add new department'
				]);
		}
		else{
			$Depname =  $res[0]['DepartmentID'];
		}



		$res = $this->employee->insertEmployee($fname,$lname, $mname,$Posname,$Depname);
		if($res)
			return $this->returnThis($response , [
				'newID'		=> $this->employee->getLastID(),
				'success'	=> true,'has_login' => true,
				'message'	=> 'New employee has been added.'
			]);
		else{
			return $this->returnThis($response , [
				'success'	=> false,'has_login' => true,
				'message'	=> 'Failed to add employee'
			]);
		}
	}





	public function updateThisEmployee($request, $response, $args) {
		$isp = $request->getParam('isp');

		if(!$this->isIpSourceValid($isp)){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'Invalid request.']);
		}

		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}
		
		if(!($this->isLoginAsSuperAdmin() )){
			return $this->returnThis($response,['success'	=> false,'has_login' => true,'message'		=> 'You dont have permission to do this transaction.']);
		}
		
		$id 		= $request->getParam('ID');
		$fname 		= ucwords($request->getParam('Fname'));
		$lname 		= ucwords($request->getParam('Lname'));
		$mname 		= ucwords($request->getParam('Mname'));
		$Posname 	= ucwords($request->getParam('Position'));
		$Depname 	= ucwords($request->getParam('Department'));

		if(empty($id) || empty($fname) || empty($lname) || empty($mname) || empty($Posname) || empty($Depname))
			return $this->returnThis($response , [
					'success'	=> false,'has_login' => true,
					'message'	=> 'All feilds are required.'
				]);



		$res = $this->position->searchForThisPosition($Posname);
		if(count($res) == 0){
			$Posname = $this->position->addThisPositions($Posname);
			if($Posname)
				$Posname = $this->position->getLastID();
			else
				return $this->returnThis($response , [
					'success'	=> false,'has_login' => true,
					'message'	=> 'Failed to add new position'
				]);
		}
		else{
			$Posname =  $res[0]['PositionID'];
		}
		
		
		
		$res = $this->department->searchForThisDepartment($Depname);
		if(count($res) == 0){
			$Depname = $this->department->addThisDepartment($Depname);
			if($Depname)
				$Depname =  $this->department->getLastID();
			else
				return $this->returnThis($response , [
					'success'	=> false,'has_login' => true,
					'message'	=> 'Failed to add new department'
				]);
		}
		else{
			$Depname =  $res[0]['DepartmentID'];
		}




		$res = $this->employee->updateEmployee($id,$fname,$lname, $mname,$Posname,$Depname);
		if($res)
			return $this->returnThis($response , [
				'success'	=> true,'has_login' => true,
				'message'	=> 'Employee has been updated.'
			]);
		else{
			return $this->returnThis($response , [
				'success'	=> false,'has_login' => true,
				'message'	=> 'Failed to update employee'
			]);
		}


	}



	public function getReport($request, $response, $args) {
		$html = "Hello World";
		$this->mPDF->WriteHTML($html);
		$this->mPDF->Output();
		exit;
	}
}