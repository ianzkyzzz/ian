<?php 
namespace App\Controllers;


class ParentController
{
	
	public function returnThis($response,array $data){
		return $response->withStatus(200)->withJson($data);
	}


	public function isIpSourceValid($ip){
		
		if(isset($_SESSION['UserRole'])){
			if($this->isLoginAsCashier() || $this->isLoginAsAccountant()){
				return in_array($ip, $_SESSION['ipWhiteList']);
			}
			else{
				return true;
			}
		}

		return false;
	}
 


	public function forRedirect(){
		// $app->get('/', function ($req, $res, $args) {
		//   return $res->withStatus(302)->withHeader('Location', 'your-new-uri');
		// });
	}
	
	public function isLoginAsSuperAdmin(){
		if ( $_SESSION['UserRole'] == 'superadmin' ){
			return true;
		}
		else
			return false;
	}
	
	public function isLoginAsEmployeeEncoder(){
		if ( $_SESSION['UserRole'] == 'employee encoder' ){
			return true;
		}
		else
			return false;
	}
	
	public function isLoginAsCashier(){
		if ( $_SESSION['UserRole'] == 'cashier' ){
			return true;
		}
		else
			return false;
	}
	
	public function isLoginAsAdmin(){
		if ( $_SESSION['UserRole'] == 'admin' ){
			return true;
		}
		else
			return false;
	}
	
	public function isLoginAsAccountant(){
		if ( $_SESSION['UserRole'] == 'accountant' ){
			return true;
		}
		else
			return false;
	}
	
	public function isLoginAsBarcodeGenerator(){
		if ( $_SESSION['UserRole'] == 'barcode generator' ){
			return true;
		}
		else
			return false;
	}
	
	public function isLogin(){
		if ( isset($_SESSION['userID']) ){
			return true;
		}
		else
			return false;
	}

//  new code 
	public function isUserAccountLogin(){
		if ( isset($_SESSION['encoder']) ){
			return true;
		}
		else
			return false;
	}




	
	// validation functions
	

	public function isValidPesoAmount($values){
		$validvalue = ['1','2','3','4','5','6','7','8','9','0','.'];
		$value = str_split($values);
		for ($i=0; $i < count($value); $i++) { 
			if(!in_array($value[$i], $validvalue))
				return false;
		}

		return true;
	}




	public function isValidEmail($email){
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		    return false;
		}
		else{
			return true;
		}
	}



	public function isValidDecimal($val){
		return preg_match('/^[1-9][0-9\.]{0,15}$/', $val);
	}


	public function isValidInteger($val){
		return preg_match('/^\d+$/',$val);
	}


	public function checkThisdate($date) {
		$tempDate = explode('-', $date);
		
		if(count($tempDate) != 3)
			return false;
		// checkdate(year,month, day )
		return checkdate($tempDate[1], $tempDate[2], $tempDate[0]);
	}


	public function getAge($bdate) {
		return date_diff(date_create($bdate), date_create('now'))->y; //  Works on PHP 5.3+ only 
	}




}