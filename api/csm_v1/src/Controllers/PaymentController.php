<?php
/**
 * Created by Raymund Matol
 * Date: 1/31/16
 */

namespace App\Controllers;

use App\Helpers\Validation;
use App\Models\Property;
use App\Models\ClientProperty;
use App\Models\ClientPaymentHistory;
use App\Controllers\ParentController;
/**
 * Class UserController
 * @package App\Controllers
 */
 
class PaymentController extends ParentController{

	protected $validation; 
	protected $property;
	protected $clientProperty;
	protected $clientPaymentHistory;

	public function __construct(Property $property, ClientPaymentHistory $clientPaymentHistory,ClientProperty $clientProperty, Validation $validation) {
		// model
		$this->property = $property;
		$this->clientProperty = $clientProperty;
		$this->clientPaymentHistory = $clientPaymentHistory;
		// Validation Helper
		$this->validation = $validation;

	}
	// $_SESSION['Name'] == DEBONER21 DULOS 
	/**
     * @param $request
     * @param $response
     * @return $response
     */
	
	public function getClientList($request, $response, $args){
		
		return $this->returnThis($response,[
			'success'			=> true,
			'has_login' 		=> true,
			'message'			=>'test'
		]);
	}

	//6:40 AM 7/11/2017	
	public function getClientPaymentHistory($request, $response, $args){

		$getPaymentDetails = $this->clientPaymentHistory->getUpdateTrasactionPaymentHistoryertyPaymentDetails($clientID, $propertyID);
	

		if ($getPaymentDetails) {
			return $this->returnThis($response,[
				'success'			=> true,
				'has_login' 		=> true,
				'message'			=> 'No Records Found!'
			]);
		}else{
			return $this->returnThis($response,[
				'success'			=> true,
				'has_login' 		=> true
			]);
		}
		
	}	

	public function getAllTransactionPaymentHistory($request, $response, $args){

		$clientPaymentHistory = $this->clientPaymentHistory->getAllPaymentHistory(); 
		return $this->returnThis($response,[
			'success'		=> true,
			'has_login' 	=> true,
			'clientPaymentHistory'	=> $clientPaymentHistory,
			'role'			=> $_SESSION['UserRole']
		]);
	}	


	public function getUpdateTrasactionPaymentHistory($request, $response, $args){

		$ornumsber = $request->getParam('orNumber');


		$getclientPaymentHistory = $this->clientPaymentHistory->getClientPaymentTransactionHistory($ornumsber); 	
		// $getclientPaymentHistory =  $this->clientPaymentHistory->getAllPaymentHistory(); 
		
		return $this->returnThis($response,[
			'success'		=> true,
			'has_login' 	=> true,
			'clientPaymentTransactionHistory'	=> $getclientPaymentHistory
		]);

	}	

	public function updateClientPaymentTransaction($request, $response, $args){
		$orNumberNew  		 = $_POST['orNumber']; 
		$orNumberOld  		 = $_POST['orNumberOld']; 
		$paymentDate		 = $_POST['paymentDate']; 
		$paymentOption		 = $_POST['paymentOption'];
		$paymentAmount 		 = $_POST['payment'];
		$paymentID 		 	 = $request->getParam('payment_id');
		$notes 			 	 = $request->getParam('notes');

		
		if($paymentOption == "Cash"){
	    	$getclientPaymentHistory = $this->clientPaymentHistory->updateTransactioHistory($orNumberNew, $paymentDate, $orNumberOld, $paymentAmount, $paymentID, $notes); 	
		}else{
	    	$getclientPaymentHistory = $this->clientPaymentHistory->updateTransactioHistoryRemitance($orNumberNew, $paymentDate, $paymentID, $paymentAmount, $paymentOption, $paymentID, $notes); 	
		}

		 if($getclientPaymentHistory){
		 	return $this->returnThis($response,[
				'success'		=> true,
				'has_login' 	=> true,
				'message'		=> "Successfully Updated"
			]);
		 }else{
		 	return $this->returnThis($response,[
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> $paymentDate
			]);
		 }
		
	}

	public function postThisPayment($request, $response, $args){
		$emptyField = [];

		$paymentStatus 		= trim($request->getParam('paymentStatus'));
		$cp_id 				= trim($request->getParam('cp_id'));
	 
		$clientID				= trim($request->getParam('clientID'));
		$addPaymentPropertyList	= trim($request->getParam('addPaymentPropertyList'));
		$modeOfPayment			= trim($request->getParam('modeOfPayment'));
		$amount					= trim($request->getParam('amount'));
		$paymentornumber		= trim($request->getParam('paymentornumber'));
		$paymentDate			= trim($request->getParam('paymentDate'));
		// $paymentmadeDate		= trim($request->getParam('paymentmadeDate'));
		$note		     		= $request->getParam('notes');

		$remitanceCenterName	= trim(strtoupper($request->getParam('remitanceCenterName')));
		$paymentAgentCommision	= trim($request->getParam('paymentAgentCommision'));
 		
 		if(!isset($clientID) || !isset($paymentornumber)  || !isset($addPaymentPropertyList) || !isset($modeOfPayment) || !isset($amount) || !isset($paymentDate) || !isset($remitanceCenterName) || !isset($paymentAgentCommision)){
 			return $this->returnThis($response,[
				'success'			=> false,
				'has_login' 		=> true,
				'message'			=>'Some feilds are missing.'
			]);
 		}
 		else if(!($modeOfPayment == 1 || $modeOfPayment == 2 || $modeOfPayment == 3)){
 			return $this->returnThis($response,[
				'success'			=> false,
				'has_login' 		=> true,
				'message'			=>'Mode of payment is invalid.'
			]);
 		}
 		else if(($modeOfPayment == 2 || $modeOfPayment == 3) && $remitanceCenterName == ""){
 			return $this->returnThis($response,[
				'success'			=> false,
				'has_login' 		=> true,
				'message'			=>'Remitance/Bank name is required.'
			]);
 		}
 		else if(!$this->checkThisdate($paymentDate)){
 			return $this->returnThis($response,[
				'success'			=> false,
				'has_login' 		=> true,
				'message'			=>'Invalid date value.'
			]);
 		}
 		else if($amount == 0 || !$this->isValidDecimal($amount)){
 			return $this->returnThis($response,[
				'success'			=> false,
				'has_login' 		=> true,
				'message'			=>'Invalid amount value.'
			]);
 		}
 		else if($paymentornumber == ""){
 			return $this->returnThis($response,[
				'success'			=> false,
				'has_login' 		=> true,
				'message'			=>'OR number is required.'
			]);
 		}
 	// 	else if(count($this->clientPaymentHistory->isORnumberused($paymentornumber)) >= 1){
		// 	return $this->returnThis($response , [
		// 		'success'		=> false,
		// 		'has_login' 	=> true,
		// 		'message'		=> 'OR number was used already.',
		// 	]);	
		// }

		$updateCom = $this->clientProperty->getUpdateAgentComTable($cp_id);


		// if($updateCom){
		// 	return $this->returnThis($response,[
		// 		'success'			=> false,
		// 		'has_login' 		=> true,
		// 		'message'			=>'unable to update.'
		// 	]);
		// }

		$result = $this->clientProperty->clientPropertyExist($clientID, $cp_id);
		if(count($result ) <= 0){
			return $this->returnThis($response,[
				'success'			=> false,
				'has_login' 		=> true,
				'message'			=>'Invalid property ID.'
			]);
		}

		$credit = 0;
		$debit = 0;

		if(($modeOfPayment == 2 || $modeOfPayment == 3)){
			$debit = $amount;
		}
		else
			$credit = $amount;

		// if ($paymentStatus == 1) {
		// 	$updateClientPoperty = $this->clientPaymentHistory->updateClientPoperty($cp_id, $paymentStatus);
		// }

		$result = $this->clientPaymentHistory->insertThisClientPaymentHistory($cp_id,$clientID,'',$paymentornumber,$remitanceCenterName,$debit,$credit,$_SESSION['userID'],$paymentDate,$note);
																				

		if($result){
					$itextmo = $this->clientProperty->getClientwithNumber($cp_id);
		
		if($itextmo)
		{
			$propertyName 	=($itextmo[0]['propertyName']);
			$name 			=($itextmo[0]['fullName']);
			$number         =($itextmo[0]['ContactNumber']);
			$lotno          =($itextmo[0]['lot']);
			$blockno        =($itextmo[0]['block']);
			$due            =($itextmo[0]['dueDate']);
			$message = "Hi " . $name . " we have just recieve your payment for " . $propertyName . " Block:" . $blockno . " lot:" . $lotno   . ". Your duedate is every  " . $due."th of the month please pay on or before duedate. Thank you and God Bless from R and Sons Properties Co.";
			if (strlen($number)==11) {
				$respomzzz = $this->sendSMS($number,$message);
			}


	
				
			

		}
			return $this->returnThis($response,[
				'success'			=> true,
				'has_login' 		=> true,
				'message'			=>$propertyName,
			]);
			

		}else{
			return $this->returnThis($respondz,[
				'success'			=> false,
				'has_login' 		=> true,
				'message'			=>'Payment failed.'
			]);
		}	
	}	
public function sendSMS($number,$message){
	$url = 'https://api.m360.com.ph/v3/api/broadcast';

	$ch = curl_init($url);

$data = array("app_key" => "PFEvVnubdtB9brMj","app_secret" => "BoH97PfzCIOG0BT7nM0sZgAu7IazQ8tK","msisdn"=> $number,"content" =>$message,"shortcode_mask"=>"RandSonsPco","rcvd_transid"=>"","is_intl"=>"false");

$postdata = json_encode($data);

$ch = curl_init($url); 
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
$result = curl_exec($ch);
curl_close($ch);

	

		return (1);
}

	public function deleteThisPayment($request, $response, $args)
	{
		$PaymentID = $request->getParam('payID');

		if (empty($PaymentID)) {
			return $this->returnThis($response,[
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> "Undefiend paymentId"
			]);
		}

		$deleteThisID =  $this->clientPaymentHistory->removeThisPayment($PaymentID);
		if (!$deleteThisID) {
			return $this->returnThis($response,[
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> "Unable to remove payment"
			]);	
		}

		return $this->returnThis($response,[
				'success'		=> true,
				'has_login' 	=> true,
				'message'		=> "Payment Successfully remove!"
			]);

	}

	public function checkorNumber($request, $response, $args)
	{
		$PaymentID = $request->getParam('ornumber');
		
		$select    = $this->clientPaymentHistory->isORnumberused($PaymentID); 
		if(!$select){
			return $this->returnThis($response , [
				'success'		=> true,
				'has_login' 	=> true,
				'message'		=> 'OR number not used .',
			]);	
		}

		return $this->returnThis($response , [
			'success'		=> true,
			'has_login' 	=> true,
			'message'		=> 'OR number exist.',
		]);	
	}

	public function verify_adminPass($request, $response, $args)
	{
		$pass = $request->getParam('pass');

		$adminPass = $this->clientPaymentHistory->getAdminPass();
		if(password_verify ($pass , $adminPass[0]['Password'])){
			return $this->returnThis($response, ['success' => true]);	
		}else{
			return $this->returnThis($response, ['success' => false]);	
		}

	}


	public function getHash($rand_string){
	
		$options = [
		    'cost' => 11,
		    // 'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM)
		];
		
		// password_verify ( string $password , string $hash )
		return password_hash($rand_string, PASSWORD_BCRYPT, $options);
	}


	public function new_adminPass($request, $response, $args)
	{
		$newpass = $request->getParam('pass');
		$pass = $this->clientPaymentHistory->setNewAdminPass($this->getHash($newpass));

		if (!$pass) {
			return $this->returnThis($response, ['success' => false, 'message' => 'Unable to change thae password']);	
		}else{
			return $this->returnThis($response, ['success' => true, 'message' => 'Successfully change']);	
		}
	}

	public function geThisPaymentDesc($request, $response, $args)
	{
		$id = $request->getParam('id');
		$status = "";
		$dateAdded = "";
		$desc = $this->clientPaymentHistory->getPaymentDescription($id);
		if (!$desc) {
			return $this->returnThis($response, ['success' => true, 'message' => 'Unable to get payment description']);
		}
		if ($desc[0]['paymentDesc'] === "") {
			$status = "";
		}else{
			$status = $desc[0]['paymentDesc'];
			$dateAdded = $desc[0]['dateAdded'];
		}

		return $this->returnThis($response, ['success' => true, 'paymentDes' => $status, 'dateadded' => $dateAdded]);	
	}

	public function geThisCharges($request, $response, $args)
	{
		$clientID  = $request->getParam('clientID');
		$propID    = $request->getParam('propID');
		$charges = [];

		$getPropertyChargesd = $this->clientPaymentHistory->geThisCharges($clientID, $propID);
		// return $this->returnThis($response, ['success' => false, 'message' => $clientID . ' = '. $propID]);	

		if (!$getPropertyChargesd) {
			return $this->returnThis($response, ['success' => false, 'message' => 'Something went wrong unable to get client charges']);		
		}

		if ($getPropertyChargesd[0]['additionalCharges'] == 'null') {
			$charges = [];			
		}else{
			$charges = json_decode($getPropertyChargesd[0]['additionalCharges']);
		}
		
		return $this->returnThis($response, ['success' => true, 'data' => $charges]);	
	}

	public function newCharges($request, $response, $args)
	{
		$clientID  = $request->getParam('clientID');
		$propID    = $request->getParam('propID');
		$charges   = $request->getParam('charges');

		$update = $this->clientPaymentHistory->updateThisCharges($clientID, $propID, json_encode($charges));
		if (!$update) {
			return $this->returnThis($response, ['success' => false, 'message' => 'Unable to add new charges!']);				
		}

		return $this->returnThis($response, ['success' => true, 'message' => 'Charges Successfully added!']);	
	}



}





	