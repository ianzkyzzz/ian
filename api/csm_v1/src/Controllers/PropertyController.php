<?php
/**
 * Created by Raymund Matol
 * Date: 1/31/16
 */

namespace App\Controllers;

use App\Helpers\Validation;
use App\Models\Property;
use App\Controllers\ParentController;
/**
 * Class UserController
 * @package App\Controllers
 */
 
class PropertyController extends ParentController{

	protected $validation; 
	protected $property;

	public function __construct(Property $property, Validation $validation) {
		// model
		$this->property = $property;

		// Validation Helper
		$this->validation = $validation;

	}
	// $_SESSION['Name'] == DEBONER21 DULOS 
	/**
     * @param $request
     * @param $response
     * @return $response
     */

	
	public function getAllProperties($request, $response, $args){
		
		$type = $request->getParam('type');
		$userID = $_SESSION['userID'];

		if ($_SESSION['UserRole'] != 'Admin') {


			// $properties = $this->property->getAllPropertyGues($type); 
			$properties = $this->getAccountPropertyList($userID, $type); 
		}else{
			$properties = $this->property->getAllProperty($type); 
		}



		foreach ($properties as $key => $value) {
			$sqmPricem2 		= $value[4];
			$sqm				= $value[3];
			$plan_terms			= $value[5];


			if(!empty($value[6])){
				$k = $key;
				$additionalCharges = json_decode($value[6]);
				if(count($additionalCharges) != 0){
					

					$totalChargeValue = 0;
					foreach ($additionalCharges as $key => $value) {
						$totalChargeValue += floatval($value->chargeValue);
					}

					
					$properties[$k][6] = (($sqmPricem2 * $sqm));
					$properties[$k][8] = ((($sqmPricem2 * $sqm)) / $plan_terms);
				}
			}
			else{
				$properties[$key][6]  = ($sqmPricem2 * $sqm);
			}
		}
 
		if(count($properties) > 0)
			return $this->returnThis($response,[
				'success'		=> true,
				'has_login' 	=> true,
				'properties'	=> $properties,
				'message'		=> '',
				'type' 			=> $type
			]);
		else
			return $this->returnThis($response,[
				'success'		=> true,
				'has_login' 	=> true,
				'properties'	=> $properties,
				'message'		=> 'No results..',
				'type' 			=> $type
			]);

	}	

	public function getAccountPropertyList($userID, $typeID)
	{
		$proplist 	 = $this->property->getPropertiesGues($userID);
		$getClients = [];

		if ($proplist) {
			if ($proplist[0]['property']) {
				$propList = json_decode($proplist[0]['property']);
				$getClients = $this->property->getAllPropertyGues($propList, $typeID);
			}
		}

		return $getClients;
	}
	

	public function getAllOfThisSelectedPropertiesInfo($request, $response, $args){
		
		$data = $request->getParam('data');


		if(count($data) <= 0){
			return $this->returnThis($response,[
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Please provide property IDs.',
			]);
		}

		$conditions = [];
		$values = [];
		foreach ($data as $key => $value) {

			if(!isset($value['parent']) || !isset($value['block']) || !isset($value['lot']) || !isset($value['phaseNumber'])){
				return $this->returnThis($response,[
					'success'		=> false,
					'has_login' 	=> true,
					'data' 			=> $value,
					'message'		=> 'There are undefined feilds.',
				]);
			}
			
			$conditions[] = '( propertyParentID = :parent'.$key.' AND  block = :block'.$key.' AND  lot = :lot'.$key.'  AND  phaseNumber = :phaseNumber'.$key.' ) ';
			$values['parent'.$key.''] = $value['parent'];
			$values['block'.$key.''] = $value['block'];
			$values['lot'.$key.''] = $value['lot'];
			$values['phaseNumber'.$key.''] = $value['phaseNumber'];
		} 

		$properties = $this->property->getPropertyDetailsForThisSet($conditions,$values); 

		return $this->returnThis($response,[
			'success'		=> true,
			'has_login' 	=> true,
			'properties'	=> $properties,
		]);
 
	}	




	public function getAllPropertyTypes($request, $response, $args){
		
		$propertyTypes = $this->property->getAllPgetAllPropertyTypesroperty(); 

		return $this->returnThis($response,[
				'success'		=> true,
				'has_login' 	=> true,
				'propertyTypes'	=> $propertyTypes,
			]);

	}	





	public function getAllPropertyParents($request, $response, $args){

		$parents = $this->property->getAllPropertyParents(); 
		$propertyTypes = $this->property->getAllPgetAllPropertyTypesroperty(); 
		$blockCount	   = $this->property->getFirstBlockCount();
		$types = [];
		foreach ($propertyTypes as $key => $value) {
			$types[] = '<option value="'.$value['type_id'].'">'.$value['type_name'].'</option>';
		}

		return $this->returnThis($response,[
			'success'		=> true,
			'has_login' 	=> true,
			'parents'		=> $parents,
			'propertyTypes'		=> $types,
			'blockCount'		=> $blockCount
		]);

	}	




	public function getLastBlockForThisPhase($request, $response, $args){
		$parentID = $request->getParam('parentID');
		$phaseNumber = $request->getParam('phaseNumber');
		if(!isset($parentID) || empty($parentID)){
			return $this->returnThis($response,[
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Invalid property ID.',
			]);
		}

		$blockNumber = 0;

		$block = $this->property->getPhaselastBlockNumber($parentID,$phaseNumber); 
		
		// foreach ($block  as $key => $value) {
		// 	$blockNumber = $value;
		// }

		return $this->returnThis($response,[
			'success'		=> true,
			'has_login' 	=> true,
			'blockData'		=> $block,
		]);

	}



	public function getLastBlock($request, $response, $args){
		$parentID = $request->getParam('parentID');
		if(!isset($parentID) || empty($parentID)){
			return $this->returnThis($response,[
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Invalid property ID.',
			]);
		}

		$blockNumber = 0;

		$block = $this->property->getlastBlockNumber($parentID); 
		$phases = $this->property->getAllPhases($parentID); 
		
		// foreach ($block  as $key => $value) {
		// 	$blockNumber = $value;
		// }

		return $this->returnThis($response,[
			'success'		=> true,
			'has_login' 	=> true,
			'blockData'		=> $block,
			'phasesData'		=> $phases
		]);

	}


	public function getPropertyParentsDetailsSpecificBlock($request, $response, $args){
		$parentID = $request->getParam('parentID');
		$blockNo  = $request->getParam('blockNo');

		$parent = $this->property->getPropertyParentsDetails($parentID); 
		
		$blocks = $this->property->getPropertyDetailsByBlock($parentID,$blockNo);
		 

		foreach ($blocks as $key => $value) {
			if(!empty($value['additionalCharges'])){
				$k = $key;
				$additionalCharges = json_decode($value['additionalCharges']);
				$sqmPricem2 		= $value['propertyPricePerSQM'];
				$sqm				= $value['propertySQM'];
				$plan_terms			= $value['propertyPlanTerms'];
				$downpayment		= $value['downPayment'];
				if(count($additionalCharges) != 0){
					$totalChargeValue = 0;
					foreach ($additionalCharges as $key => $value) {
						$totalChargeValue += $value->chargeValue;
					}

					$blocks[$k]['additionalChargesNumber'] = count($additionalCharges);
					$blocks[$k]['propertyOriginalPrice'] = (($sqmPricem2 * $sqm));
					$blocks[$k]['propertyMonthlyAmortization'] = ((($sqmPricem2 * $sqm)- $downpayment) / $plan_terms);
				}
			}
		}

		if(count($parent) <= 0)
			return $this->returnThis($response,[
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Invalid property ID.'
			]);
		else
			return $this->returnThis($response,[
				'success'		=> true,
				'has_login' 	=> true,
				'parent'		=> $parent,
				'blocks' 		=> $blocks
			]);

	}	



	public function getPropertyParentsDetails($request, $response, $args){
		$parentID = $request->getParam('parentID');

		$parent = $this->property->getPropertyParentsDetails($parentID); 
		
		$blocks = $this->property->getPropertyDetailsByBlock($parentID,1);
		 
		$blockCount = $this->property->getBlockCount($parentID);

		foreach ($blocks as $key => $value) {
			if(!empty($value['additionalCharges'])){
				$k = $key;
				$additionalCharges = json_decode($value['additionalCharges']);
				$sqmPricem2 		= $value['propertyPricePerSQM'];
				$sqm				= $value['propertySQM'];
				$plan_terms			= $value['propertyPlanTerms'];

				if(count($additionalCharges) != 0){
					$totalChargeValue = 0;
					foreach ($additionalCharges as $key => $value) {
						$totalChargeValue += $value->chargeValue;
					}

					$blocks[$k]['additionalChargesNumber'] = count($additionalCharges);
					$blocks[$k]['propertyOriginalPrice'] = (($sqmPricem2 * $sqm));
					$blocks[$k]['propertyMonthlyAmortization'] = ((($sqmPricem2 * $sqm)) / $plan_terms);
				}
			}
		}

		if(count($parent) <= 0)
			return $this->returnThis($response,[
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Invalid property ID.'
			]);
		else
			return $this->returnThis($response,[
				'success'		=> true,
				'has_login' 	=> true,
				'parent'		=> $parent,
				'blocks' 		=> $blocks,
				'blockCounter'	=> $blockCount
			]);

	}	


	public function getThisBlock($request, $response, $args){
		$parentID 			= $request->getParam('parentID');
		$block 				= $request->getParam('block');
		$phaseNumber 		= $request->getParam('phaseNumber');
		
		$blocks = $this->property->getThisPropertyBlockDetails($parentID,$block,$phaseNumber); 
		if(count($blocks) <= 0)
			return $this->returnThis($response,[
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Invalid block ID.'
			]);
		else
			return $this->returnThis($response,[
				'success'		=> true,
				'has_login' 	=> true,
				'blocks' 		=> $blocks
			]);
	}	




	public function getAllBlockForThisProperty($request, $response, $args){
		 
		$parentID = $request->getParam('id');

		$blocks = $this->property->getAllBlocksForThisProperty($parentID);

		return $this->returnThis($response,[
			'success'			=> true,
			'has_login' 		=> true,
			'message'			=> '',
			'blocks'			=> $blocks
		]);

	}	



	public function getAllLotsForThisBlock($request, $response, $args){
		  
		$parentID = $request->getParam('id');
		$blockID = $request->getParam('blockID');
		$phaseNumber = $request->getParam('phaseNumber');
		$blockAndLots = $this->property->getAllLotsForThisProperty($parentID,$blockID,$phaseNumber);

		return $this->returnThis($response,[
			'success'				=> true,
			'has_login' 			=> true,
			'message'				=> '',
			'blockAndLots'			=> $blockAndLots
		]);

	}	





	public function removeThisPorperty($request, $response, $args){
		
		$id = $request->getParam('propID');
		$type = $request->getParam('propertyType');

		$result = $this->property->removeThisProperty($id);

		if($result)
			return $this->returnThis($response,[
				'success'			=> true,
				'has_login' 		=> true,
				'message'			=> 'Property removed.',
				'id'				=> $id,
				'properties'		=> $this->property->getAllProperty($type)
			]);
		else
			return $this->returnThis($response,[
				'success'			=> false,
				'has_login' 		=> true,
				'message'			=> 'Property failed to removed.'
			]);

	}	




	public function updateThisPorperty($request, $response, $args){
		$propertyID											= $request->getParam('propID');
        $add_propertyFormName          						= ucwords($request->getParam('update_propertyFormName'));
        $add_propertyFormAddress          					= ucwords($request->getParam('update_propertyFormAddress'));

		$propertyType =  "";
		$emptyField = [];
		foreach($request->getParsedBody() as $key => $val) {
			if(empty($val)  || !isset($val)){
				$emptyField[] = $key;
			}
		}

 
		if(count($emptyField) > 0)
			return $this->returnThis($response , ['success'	=> false,'has_login' => true,
				'message'		=> 'Some feilds are missing.',
				'variables' 	=> $emptyField
				]);

		$result = $this->property->updateThisProperty($propertyID,$add_propertyFormName,$add_propertyFormAddress);

		if($result){
			return $this->returnThis($response,[
				'success'			=> true,
				'has_login' 		=> true,
				'message'			=> 'Property named '.$add_propertyFormName .' updated.'
			]);
		}
		else{
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Property named '.$propertyName .' failed to be updated.'
			]);
		}
	}





	public function postThisProperty($request, $response, $args){
		$add_propertyFormName          						= ucwords($request->getParam('add_propertyFormName'));
        $add_propertyFormAddress          					= ucwords($request->getParam('add_propertyFormAddress'));
        $add_propertyWithPhases          					= ucwords($request->getParam('add_propertyWithPhases'));

		if(!isset($add_propertyFormName) || !isset($add_propertyFormAddress) || !isset($add_propertyWithPhases)){
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Some feilds are missing.'
			]);
		}
		else if($add_propertyFormName == ""  || $add_propertyFormAddress == ""  || $add_propertyWithPhases == "" ){
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Some feilds are empty'
			]);
		}


		$result = $this->property->insertThisProperty($add_propertyFormName,$add_propertyFormAddress,$add_propertyWithPhases,$_SESSION['userID']);

		if($result){
			

			$postPropSubPercentageDefault	= $this->property->postPropSubPercentageDefault($result);
			if(!$postPropSubPercentageDefault){
				return $this->returnThis($response,[
					'success'	=> false,
					'has_login' => true,
					'message'	=> 'Property named '.$add_propertyFormName .' failed to be added.'
				]);
			}
			return $this->returnThis($response,[
				'success'	=> true,
				'has_login' => true,
				'message'	=> 'Property named '.$add_propertyFormName .' added.'
			]);
		}
		else{
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Property named '.$add_propertyFormName .' failed to be added.'
			]);
		}

	}	


	public function addThisBlockAndItsProperties($request, $response, $args){
		$parentID          						= $request->getParam('parentID');
        $blockData          					= $request->getParam('blockData');
        $phaseNumber							= $request->getParam('phaseNumber');
		$newPhase								= $request->getParam('newPhase');


		// return $this->returnThis($response,[
		// 		'success'	=> false,
		// 		'has_login' => true,
		// 		'message'	=> $parentID . ' ' . $newPhase
 	// 		]);

		
		if(!isset($parentID) || !isset($blockData)   )
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'There are undefined values.(1)',
				'variable'	=> $request->getParsedBody()
			]);
		else if($parentID == "" || count($parentID) <= 0)
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'There are empty values.',
				'variable'	=> $request->getParsedBody()
			]);


		$lastBlockNumber = 0;



		$result = $this->property->phasable($parentID); 
		if(count($result) <= 0){								/// check for parent id if exist
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Property Name does not exist.',
				'variable'	=> $request->getParsedBody()
			]);
		}
		else if($result[0]['isPhasable'] == 1){					/// check for parent id if phasable
			
			if(!isset($newPhase) || !isset($phaseNumber)){
				return $this->returnThis($response,[
					'success'	=> false,
					'has_login' => true,
					'message'	=> 'There are undefined values.(2)',
					'variable'	=> $request->getParsedBody()
				]);
			}
			else if($newPhase == 'true'){

			
				$result = $this->property->getPhaseLastNumber($parentID); 
				if(count($result) <= 0){
					$lastBlockNumber = 1;
					$phaseNumber = 1;
					
				}
				else{
					$phaseNumber 		= ($result[0]['phaseNumber'] + 1);
					$lastBlockNumber 	= 1;
				}
			}

			else if($newPhase == 'false'){
				$result = $this->property->phaseNumberExist($parentID,$phaseNumber); 
				if(count($result) <= 0)
					return $this->returnThis($response,[
						'success'	=> false,
						'has_login' => true,
						'message'	=> 'Phase number does not exist.',
						'variable'	=> $request->getParsedBody()
					]);
				else{
					$lastBlockNumber 	= ($result[0]['blockNumber'] + 1);
					$phaseNumber 		= $phaseNumber;
				}
			}
			else{
				return $this->returnThis($response,[
					'success'	=> false,
					'has_login' => true,
					'message'	=> 'Invalid NewPhase value.',
					'variable'	=> $request->getParsedBody()
				]);
			}
		}


		else{
			$phaseNumber = 0;
			$result = $this->property->getlastBlockNumber($parentID); 
			if(count($result) <= 0){
				$lastBlockNumber = 1;
			}
			else{
				$lastBlockNumber = ($result[0]['blockNumber'] + 1);
			}
		}
			


		

		$values = [];
		$parameters = [];

		foreach ($blockData  as $key => $value) {
			$propertyParentID					= $parentID;
			$block								= $lastBlockNumber;
			$phaseNumberFinal					= $phaseNumber;
			$addedBy							= $_SESSION['userID'];

			$lot								= $value['lotnumber'];
			$propertyTypeID						= $value['propertyTypeID'];
			$originalPrice						= $value['contactprice'];
			$monthly_amortization				= $value['monthlyamortization'];
			$sqm								= $value['sqm'];
			$priceperm2							= $value['priceperm2'];
			$planterms							= $value['planterms'];
			$model								= ucwords(strtolower($value['model']));
			$status								= $value['status'];


			if(!isset($lot) || !isset($propertyTypeID)|| !isset($originalPrice)|| !isset($monthly_amortization)
				|| !isset($priceperm2) || !isset($planterms) || !isset($sqm)|| !isset($model)|| !isset($status	)){

				return $this->returnThis($response,['success'	=> false,
					'value' 	=> $value,
					'message'	=> 'Therea are undefined lot value for lot '. $lot
				]);
			}
			else if($lot == "" || $propertyTypeID == "" || $originalPrice == "" || $monthly_amortization == ""
			 || $priceperm2 == '' || $planterms == '' || $sqm == ''|| empty($model) || $status == ""){
				return $this->returnThis($response,['success'	=> false,
					'value' 	=> $value,
					'message'	=> 'Therea are empty values for lot '. $lot
				]);
			}
			else if(!$this->isValidInteger($propertyTypeID) || !$this->isValidInteger($status) ){
				return $this->returnThis($response,['success'	=> false,
					'value' 	=> $value,
					'message'	=> 'Invalid status/Property Type value for lot '. $lot
				]);
			}
			else if(!$this->isValidDecimal($sqm) || !$this->isValidInteger($lot) || !$this->isValidDecimal($priceperm2)  || !$this->isValidInteger($planterms)){
				return $this->returnThis($response,['success'	=> false,
					'value' 	=> $value,
					'message'	=> 'Invalid lot Lot Number/SQM/Price per m<sup>2</sup>/Plan terms value for lot '. $lot
				]);
			}
			else if(!($status == '0' || $status == '2')){
				return $this->returnThis($response,['success'	=> false,
					'value' 	=> $value,
					'message'	=> 'Invalid lot status for lot '. $lot
				]);
			}
			else if(!($propertyTypeID >= '1' &&  $propertyTypeID <= '2')){
				return $this->returnThis($response,['success'	=> false,
					'value' 	=> $value,
					'message'	=> 'Invalid lot property type value for lot  '. $lot
				]);
			}



			if($propertyTypeID == 1){
				$model = "NONE";
			}


			$parameters[] = (
					'('. ':lot'. $key . ','.
					''. ':propertyTypeID'. $key . ','.
					''. ':originalPrice'. $key . ','.
					''. ':monthly_amortization'. $key . ','.
					''. ':sqm'. $key . ','.
					''. ':priceperm2'. $key . ','.
					''. ':planterms'. $key . ','.
					''. ':model'. $key . ','.
					''. ':status'. $key . ','.
					''. ':propertyParentID'. $key . ','.
					''. ':block'. $key . ','.
					''. ':phaseNumber'. $key . ','.
				''. ':addedBy'. $key . ')'
				);


				$values['lot'.$key.'']					= $lot;
				$values['propertyTypeID'.$key.'']		= $propertyTypeID;
				$values['originalPrice'.$key.'']		= $originalPrice;
				$values['monthly_amortization'.$key.'']	= $monthly_amortization;
				$values['sqm'.$key.'']					= $sqm;
				$values['priceperm2'.$key.'']			= $priceperm2;
				$values['planterms'.$key.'']			= $planterms;
				$values['model'.$key.'']				= $model;
				$values['status'.$key.'']				= $status;
				$values['propertyParentID'.$key.'']		= $propertyParentID;
				$values['block'.$key.'']				= $block;
				$values['phaseNumber'.$key.'']			= $phaseNumberFinal;
				$values['addedBy'.$key.'']				= $addedBy;
		}



		


		$result = $this->property->insertThisBlockProperty($parameters,$values);

		if($result){
			return $this->returnThis($response,[
				'success'	=> true,
				'has_login' => true,
				'message'	=> 'Block details successfully added.'
			]);
		}
		else{
			return $this->returnThis($response,['success'	=> false,
				'message'	=> 'Block details failed to be added.'
			]);
		}

	}	

	public function updateThisBlockAndItsProperties($request, $response, $args){
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}

		$parentID          						= trim($request->getParam('parentID'));
		$blocknumber          					= trim($request->getParam('blockid'));
        $blockData          					= $request->getParam('blockData');
        $phasenumber          					= trim($request->getParam('phasenumber'));

        
		if(!isset($parentID) || !isset($blocknumber) || !isset($blockData)  || !isset($phasenumber))
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'There are undefined values.'
			]);
		else if($parentID == "" || $blocknumber == "" || $phasenumber == "")
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'There are empty values.'
			]);
		else if(!$this->isValidInteger($parentID) || !$this->isValidInteger($blocknumber) || !$this->isValidInteger($phasenumber))
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Invalid data.'
			]);


		$blockOriginal  = $this->property->getThisPropertyBlockDetails($parentID,$blocknumber,$phasenumber); 
		if(count($blockOriginal) <= 0){
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Invalid block.'
			]);
		}
		 



		$values = [];
		$parameters = [];
		$toBeUpdated = [];

		foreach ($blockData  as $key => $value) {
			$propertyParentID					= $parentID;
			$block								= $blocknumber;
			$phasenumber 						= $phasenumber;

			$lot								= trim($value['lotnumber']);
			$propertyTypeID						= trim($value['propertyTypeID']);
			$sqm								= trim($value['sqm']);
			$priceperm2							= trim($value['priceperm2']);
			$planterms							= trim($value['planterms']);
			$model								= ucwords(strtolower($value['model']));
			$status								= trim($value['status']);
			$originalPrice						= $value['contactprice'];
			$monthly_amortization				= $value['monthlyamortization'];

			$removeStat				            = $value['removeStat'];

			if($status == 1){
				continue;
			}
			

			if ($removeStat == 1){
				$removeStat = $this->property->removeSelectedLot($propertyParentID, $block, $phasenumber, $lot); 
				if(!$removeStat){
					return $this->returnThis($response,['success'	=> false,
						'value' 	=> $value,
						'message'	=> 'Unable to remove lot '. $lot
					]);
				}
			}else{
				if(!isset($lot) || !isset($propertyTypeID) 	|| !isset($priceperm2) || !isset($planterms) || !isset($sqm)|| !isset($model)|| !isset($status)
					){

					return $this->returnThis($response,['success'	=> false,
						'value' 	=> $value,
						'message'	=> 'Therea are undefined lot value for lot '. $lot
					]);
				}
				else if($lot == "" || $propertyTypeID == "" 
				 || $priceperm2 == '' || $planterms == '' || $sqm == '' || $model == "" || $status == ""){
					return $this->returnThis($response,['success'	=> false,
						'value' 	=> $value,
						'message'	=> 'There are empty values for lot '. $lot
					]);
				}
				else if(!$this->isValidInteger($propertyTypeID) || !$this->isValidInteger($status) ){
					return $this->returnThis($response,['success'	=> false,
						'value' 	=> $value,
						'message'	=> 'Invalid status/Property Type value for lot '. $lot
					]);
				}
				else if(!$this->isValidDecimal($sqm) || !$this->isValidInteger($lot) ||
						!$this->isValidDecimal($priceperm2)  || !$this->isValidInteger($planterms)
						){
					return $this->returnThis($response,['success'	=> false,
						'value' 	=> $value,
						'message'	=> 'Invalid lot Lot Number/SQM/Price per m<sup>2</sup>/Plan terms value for lot 1 '. $lot
					]);
				}
				else if($sqm <= 0 || $lot <= 0 ||
						$priceperm2 <= 0  || $planterms <= 0
						){
					return $this->returnThis($response,['success'	=> false,
						'value' 	=> $value,
						'message'	=> 'Invalid lot Lot Number/SQM/Price per m<sup>2</sup>/Plan terms value for lot 2 '. $lot
					]);
				}
				
				else if(!($status == '0' || $status == '2')){
					return $this->returnThis($response,['success'	=> false,
						'value' 	=> $value,
						'message'	=> 'Invalid lot status for lot '. $lot
					]);
				}
				else if(!($propertyTypeID >= '1' &&  $propertyTypeID <= '2')){
					return $this->returnThis($response,['success'	=> false,
						'value' 	=> $value,
						'message'	=> 'Invalid lot property type value for lot  '. $lot
					]);
				}

				if($propertyTypeID == 1){
					$model = "NONE";
				}
				else if($propertyTypeID == 2){
					if($model == "")
						return $this->returnThis($response,['success'	=> false,
							'value' 	=> $value,
							'message'	=> 'Lot model for this lot '. $lot. ' is empty.'
						]);
				}

				$addedBy = $_SESSION['userID'];


				$propertyOccupied  = $this->property->isLotOccupied($parentID,$block,$lot,$phasenumber); 
				if(count($propertyOccupied) >= 1)
					continue; // this will skip if property is occupied

				$lotOriginal  = $this->property->isLotExist($parentID,$block,$lot,$phasenumber); 

				if(count($lotOriginal) <= 0){
					$parameters[] = (
						'('. ':lot'. $key . ','.
						''. ':propertyTypeID'. $key . ','.
						''. ':originalPrice'. $key . ','.
						''. ':monthly_amortization'. $key . ','.
						''. ':sqm'. $key . ','.
						''. ':priceperm2'. $key . ','.
						''. ':planterms'. $key . ','.
						''. ':model'. $key . ','.
						''. ':status'. $key . ','.
						''. ':propertyParentID'. $key . ','.
						''. ':block'. $key . ','.
						''. ':phaseNumber'. $key . ','.
						''. ':addedBy'. $key . ')'
					);


					$values['lot'.$key.'']					= $lot;
					$values['propertyTypeID'.$key.'']		= $propertyTypeID;
					$values['originalPrice'.$key.'']		= $originalPrice;
					$values['monthly_amortization'.$key.'']	= $monthly_amortization;
					$values['sqm'.$key.'']					= $sqm;
					$values['priceperm2'.$key.'']			= $priceperm2;
					$values['planterms'.$key.'']			= $planterms;
					$values['model'.$key.'']				= $model;
					$values['status'.$key.'']				= $status;
					$values['propertyParentID'.$key.'']		= $propertyParentID;
					$values['block'.$key.'']				= $block;
					$values['phaseNumber'.$key.'']			= $phasenumber;
					$values['addedBy'.$key.'']				= $addedBy;
				}
				else if($lotOriginal[0]['status'] != 1){
					
					$toBeUpdated[] = [$parentID,$block,$lot,
					$sqm,$priceperm2,$planterms,$model,$status,$propertyTypeID,$phasenumber];
				}
			}	
		}

		$result = $this->property->updateAndInsertThisLotPropertyDetails($parameters,$values,$toBeUpdated);

		if($result){
			return $this->returnThis($response,[
				'success'	=> true,
				'has_login' => true,
				'message'	=> 'Block details successfully updated.'
			]);
		}
		else{
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> 'Updating block details failed.'
			]);
		}

	}	

	
	public function getThisClientList($request, $response, $args)
	{
		$select = $this->property->getAgentList();
		if (!$select) {
			return $this->returnThis($response,[
				'success'	=> false,
				'has_login' => true,
				'message'	=> "Unable to get Clients!" 
			]);
		}
		return $this->returnThis($response,[
			'success'	=> true,
			'has_login' => true,
			'val'		=> $select
		]);
	}

	public function setUpdateAssumeTO($request, $response, $args)
	{
		$data = $request->getParam('data');
		$type = $request->getParam('type');
		$cpID = $request->getParam('cpID');

		// return $this->returnThis($response,[
		// 			'success'	=> true,
		// 			'has_login' => true,
		// 			'message'	=> "Property successfully transfer! -". $data . ' - ' . $cpID
		// 		]);

		if ($type == 0 ) {
			$assumeTO = $this->property->assumeToThisClient($data, $cpID);
			if (!$assumeTO) {
				return $this->returnThis($response,[
					'success'	=> false,
					'has_login' => true,
					'message'	=> "Unable to Comple request"
				]);
			}else{
				return $this->returnThis($response,[
					'success'	=> true,
					'has_login' => true,
					'message'	=> "Property successfully transfer!"
				]);
			}
		}else{
			$assumeTO = $this->property->insertNewClient($data, $_SESSION['userID']);
			if ($assumeTO) {
				return $this->returnThis($response,[
					'success'	=> true,
					'has_login' => true,
					'message'	=> "New client successfully added!"
				]);
			}else{
				return $this->returnThis($response,[
					'success'	=> false,
					'has_login' => true,
					'message'	=> "function not yet available"
				]);
			}
		}
	}




}





