<?php
/**
 * 
 * 
 */

namespace App\Controllers;

use App\Helpers\Validation;
use App\Models\ClientPaymentHistory;
use App\Controllers\ParentController;
/**
 * Class UserController
 * @package App\Controllers
 */
 
class ReportController extends ParentController{

	protected $validation; 
	protected $property;
	protected $clientPaymentHistory;

	public function __construct(ClientPaymentHistory $clientPaymentHistory, Validation $validation) {
		// model
		$this->clientPaymentHistory = $clientPaymentHistory;
		// Validation Helper
		$this->validation = $validation;

	}
	// $_SESSION['Name'] == DEBONER21 DULOS 
	/**
     * @param $request
     * @param $response
     * @return $response
     */

	public function searchReleaseReport($request, $response, $args){

		$dateFrom 					= $request->getParam('dateFrom');
		$dateTo 					= $request->getParam('dateTo');
		$searchId 					= $request->getParam('searchID');

		if($searchId == 0){
			$getReleaseComDate = $this->clientPaymentHistory->getReleaseComDate($dateFrom, $dateTo);
			if($getReleaseComDate){
				return $this->returnThis($response,[
					'success'			=> true,
					'has_login' 		=> true,
					'comRelease' 		=> $getReleaseComDate
				]);	
			}else{
				return $this->returnThis($response,[
					'success'			=> false,
					'has_login' 		=> true,
					'message' 			=> "Unable to Search Date!"
				]);
			}	
		}else{
			
			$getReleaseComDate = $this->clientPaymentHistory->getReleaseComDate2($dateFrom, $dateTo, $searchId);
			if($getReleaseComDate){
				return $this->returnThis($response,[
					'success'			=> true,
					'has_login' 		=> true,
					'comRelease' 		=> $getReleaseComDate
				]);	
			}else{
			  	return $this->returnThis($response,[
					'success'			=> false,
					'has_login' 		=> true,
					'message' 			=> "Unable to Search Date!"
				]);
			}	

			// in any case
			// $data = [];
			// $getPropList = $this->clientPaymentHistory->getPropList();
			// foreach ($getPropList as $key => $value) {
			// 	$getpropDetails= $this->clientPaymentHistory->getPropertyDetails($value['cp_id']);
			// 	$getReleseDate = $this->clientPaymentHistory->getReleaseDetail($value['cp_id'], $dateFrom, $dateTo, $searchID);

			// 	$data [] = [
			// 		'0' => ''


			// 	];
			// }
		}
			
	}
	
	public function getAllClientPayments($request, $response, $args){

		$data_ID = $_GET['id'];

	
		$clientPayments = $this->clientPaymentHistory->getClientPaymentHistory();
		$getClientProperties = $this->clientPaymentHistory->getClientProperties();
		$comSummary = [];
		if ($getClientProperties) {
			 	foreach($getClientProperties as $value){
					$agent_1 = "";
					$agent_2 = "";
					$agent_3 = "";
					$agent_4 = "";
					$sd = "";
					$totalCom ="";  

					$agent1_com =0;
					$agent2_com =0;
					$agent3_com =0;
					$agent4_com =0;
					$agent5_com =0;

					$Agent1 = $this->clientPaymentHistory->getAgent1($value['cpID']);
					$Agent2 = $this->clientPaymentHistory->getAgent2($value['cpID']);
					$Agent3 = $this->clientPaymentHistory->getAgent3($value['cpID']);
					$Agent4 = $this->clientPaymentHistory->getAgent4($value['cpID']);
					$SD = $this->clientPaymentHistory->getSD($value['cpID']);
					
					foreach($Agent1 as $value_1){
	 					$agent_1 =$value_1['percentageVal']. ' ' . number_format($value_1['Agent 1'],2);
	 					$agent1_com = $value_1['Com'];
					}

					foreach($Agent2 as $value_2){
	 					$agent_2 = $value_2['percentageVal']. ' ' . number_format($value_2['Agent 2'],2);
	 					$agent2_com = $value_2['Com'];
					}

					foreach($Agent3 as $value_3){
	 					$agent_3 =  $value_3['percentageVal']. ' ' . number_format($value_3['Agent 3'],2);
	 					$agent3_com = $value_3['Com'];
					}


					foreach($Agent4 as $value_3){
	 					$agent_4 =  $value_3['percentageVal']. ' ' . number_format($value_3['Agent 4'],2);
	 					$agent5_com = $value_3['Com'];
					}


					foreach($SD as $value_4){
	 					$sd =  $value_4['percentageVal']. ' ' . number_format($value_4['SD'],2);
	 					$agent4_com = $value_4['Com'];
					}

					
					if ($data_ID == 0 ) {
						$totalCom = $agent1_com + $agent2_com + $agent3_com + $agent4_com + $agent5_com;
						$data [] = [
							'0' => $value['Buyer'],
							'1' => $value['Property Name'],
							'2' => $value['P'],
							'3' => $value['Blk'],
							'4' => $value['Lt'],
							'5' => $value['TCP'],
							'6' => $value['addCom'],
							'7' => $value['CRS'],
							'8' => $agent_1, 
							'9' => $agent_2,
							'10' => $agent_3,
							'11' => $agent_4,
							'12' => $sd,
							'13' => number_format($totalCom,2)
						];

					}else if ($data_ID == 1) {

						if ($sd) {
							 $totalCom = ($agent4_com);
							 $data [] = [
								'0' => $value['Buyer'],
								'1' => $value['Property Name'],
								'2' => $value['P'],
								'3' => $value['Blk'],
								'4' => $value['Lt'],
								'5' => $value['TCP'],
								'6' => $value['addCom'],
								'7' => $value['CRS'],
								'8' => $sd,
								'9' => number_format($totalCom,2)
							];
						}
					
					}else if( $data_ID == 2){
						
						if ($agent_1) {
							$totalCom = ($agent1_com);
							$data [] = [
								'0' => $value['Buyer'],
								'1' => $value['Property Name'],
								'2' => $value['P'],
								'3' => $value['Blk'],
								'4' => $value['Lt'],
								'5' => $value['TCP'],
								'6' => $value['addCom'],
								'7' => $value['CRS'],
								'8' => $agent_1,
								'9' => number_format($totalCom,2)
							];
						}
						
					}else if( $data_ID == 3){
						if ($agent_2) {
							$totalCom = ($agent2_com);
							$data [] = [
								'0' => $value['Buyer'],
								'1' => $value['Property Name'],
								'2' => $value['P'],
								'3' => $value['Blk'],
								'4' => $value['Lt'],
								'5' => $value['TCP'],
								'6' => $value['addCom'],
								'7' => $value['CRS'],
								'8' => $agent_2,
								'9' => number_format($totalCom,2)
							];		 
						}
						
					}else if( $data_ID == 4){
						if ($agent_3) {
							$totalCom = ($agent3_com);
							$data [] = [
								'0' => $value['Buyer'],
								'1' => $value['Property Name'],
								'2' => $value['P'],
								'3' => $value['Blk'],
								'4' => $value['Lt'],
								'5' => $value['TCP'],
								'6' => $value['addCom'],
								'7' => $value['CRS'],
								'8' => $agent_3,
								'9' => number_format($totalCom,2)
							];	 
						}

					}else if( $data_ID == 5){
						if ($agent_4) {
							$totalCom = ($agent5_com);
							$data [] = [
								'0' => $value['Buyer'],
								'1' => $value['Property Name'],
								'2' => $value['P'],
								'3' => $value['Blk'],
								'4' => $value['Lt'],
								'5' => $value['TCP'],
								'6' => $value['addCom'],
								'7' => $value['CRS'],
								'8' => $agent_4,
								'9' => number_format($totalCom,2)
							];	 
						}
					}
			}

		
			$getComRealease = $this->clientPaymentHistory->getclientComReleasev2();

			$totalCom="";
			foreach ($getComRealease as $key => $value) {
				$totalCom += $value['3']; 
			}

			$sum = [];
			$selectSummary = $this->clientPaymentHistory->getSummary();
			foreach ($selectSummary as $key => $value) {
				$getAgentInfo = $this->clientPaymentHistory->agentInfo($value['agent_id']); 
				$sum [] = [
					'0' => $value['accountNumber'],
					'1' => $value['name'],
					'2' => $getAgentInfo[0]['Amount']
				];
			}

			$propList = [];
			$getThis  = $this->clientPaymentHistory->getDistinctProperty();
			foreach ($getThis as $key => $val_getThis) {
				$propList [] = [
					'propID' 	=>  $val_getThis['parentID'],
					'propName'  =>  $val_getThis['Property Name']
				];
			}

			$getComrel = $this->clientPaymentHistory->getCommisisonDetails();
				foreach ($getComrel as $key => $value) {
					$getPropertiesCom = $this->clientPaymentHistory->getPropertisCom($value['cp_id']);
					
					$addcom = "";
					if ($getPropertiesCom[0]['addcom'] == '0') {
						$addcom = 0;
					}else{
						$addcom = $getPropertiesCom[0]['addcom'];
					}

					$comSummary [] = [
								'0'   => $value['clientName'],
								'1'   => $getPropertiesCom[0]['propertyname'],
								'2'   => $getPropertiesCom[0]['ph'],
								'3'   => $getPropertiesCom[0]['blk'],
								'4'   => $getPropertiesCom[0]['lt'],
								'5'   => $getPropertiesCom[0]['tcp'],
								'6'   => $addcom,
								'7'   => $getPropertiesCom[0]['crs'],
								'8'   => $value['payment'],
								'9'   => $value['ReleaseDate'],
								'10'  => $value['agentName'],
								'11'  => $value['precentageValue'],
								'12'  => $value['Amount'],
								'13'  => $value['position'],
							];	 
				}

			if (empty($data)) {
				$data = [];
				return $this->returnThis($response,[
					'success'		=> true,
					'has_login' 	=> true,
					'ClientsPayHis'	=> $data,
					'comRelease'    => $getComRealease, 
					'totalCom'      => $totalCom,
					'summary'		=> $sum,
					'ComSummary'	=> $comSummary,
					'prop'			=> $propList
		 		]);
			}else{
				return $this->returnThis($response,[
					'success'		=> true,
					'has_login' 	=> true,
					'ClientsPayHis'	=> $data,
					'comRelease'    => $getComRealease, 
					'totalCom'      => $totalCom,
					'summary'		=> $sum,
					'ComSummary'	=> $comSummary,	
					'prop'			=> $propList
		 		]);
			}			
		}else{
			return $this->returnThis($response,[
				'success'		=> false,
				'has_login' 	=> true,
				'message'       => 'No Records Found!'
	 		]);
		}
	}

	public function get_propertyByUserAccount($request, $response, $args)
	{

		$userID = $_SESSION['userID'];
		$proplist 	 = $this->clientPaymentHistory->getPropertiesGues($userID);
		if (!$proplist) {
			return $this->returnThis($response,[
				'success'	=> true,
				'has_login' => true,
				'prop'      => []
	 		]);
		}

		$propListAcc = [];
		if ($proplist) {
			if ($proplist[0]['property']) {
				$propList = json_decode($proplist[0]['property']);

				$getThis  = $this->clientPaymentHistory->getDistinctPropertyGuest($propList);
				foreach ($getThis as $key => $val_getThis) {
					$propListAcc [] = [
						'propID' 	=>  $val_getThis['parentID'],
						'propName'  =>  $val_getThis['Property Name']
					];
				}
			}
		}

		return $this->returnThis($response,[
			'success'		=> true,
			'has_login' 	=> true,
			'prop'       	=> $propListAcc
 		]);	
	}

	public function getCommissionSummary($request, $response, $args)
	{
		$date1 = $request->getParam('date1');
		$date2 = $request->getParam('date2');
		$sum = [];
		$selectSummary = $this->clientPaymentHistory->getSummary();
			foreach ($selectSummary as $key => $value) {
				$getAgentInfo = $this->clientPaymentHistory->agentInfoWithDate($value['agent_id'], $date1, $date2); 
				if (!$getAgentInfo) {
					 	return $this->returnThis($response,[
							'success'			=> false,
							'has_login' 		=> true,
							'message' 			=> "Unable to get current date!"
						]);
				}
				if ($getAgentInfo[0]['Amount'] == "") {
					# code...
				}else{
					$sum [] = [
						'0' => $value['accountNumber'],
						'1' => $value['name'],
						'2' => $getAgentInfo[0]['Amount']
					];
				}
				
			}

		return $this->returnThis($response,[
			'success'			=> true,
			'has_login' 		=> true,
			'summary' 			=> $sum
		]);
	}

	public function getSelectedAgentPosition($request, $response, $args)
	{
		$selVal = $request->getParam('selVal');

		$propList = [];
			$getThis  = $this->clientPaymentHistory->getDistinctProperty();
			foreach ($getThis as $key => $val_getThis) {
				$propList [] = [
					'propID' 	=>  $val_getThis['parentID'],
					'propName'  =>  $val_getThis['Property Name']
				];
			}

		$comSummary =[];

		if ($selVal == 0) {
				$getComrel = $this->clientPaymentHistory->getCommisisonDetails();

			}else{
				$getComrel = $this->clientPaymentHistory->getCommisisonDetailsByAgentPos($selVal);
			}	


			foreach ($getComrel as $key => $value) {
				$getPropertiesCom = $this->clientPaymentHistory->getPropertisCom($value['cp_id']);
				// $propParent 	  = $this->clientPaymentHistory->getPropertyParent($value['cp_id']);
				$addcom = "";
				if ($getPropertiesCom[0]['addcom'] == '0') {
					$addcom = 0;
				}else{
					$addcom = $getPropertiesCom[0]['addcom'];
				}

			 
				$comSummary [] = [
					'0'  => $value['clientName'],
					'1'  => $getPropertiesCom[0]['propertyname'],
					'2'  => $getPropertiesCom[0]['ph'],
					'3'  => $getPropertiesCom[0]['blk'],
					'4'  => $getPropertiesCom[0]['lt'],
					'5'  => $getPropertiesCom[0]['tcp'],
					'6'  => $addcom,
					'7'  => $getPropertiesCom[0]['crs'],
					'8'  => $value['payment'],
					'9'  => $value['ReleaseDate'],
					'10' => $value['agentName'],
					'11' => $value['precentageValue'],
					'12' => $value['Amount'],
					'13' => $value['position'],
				];	 
			
	 

			}

			return $this->returnThis($response,[
				'success'			=> true,
				'has_login' 		=> true,
				'prop' 				=> $selVal,
				'ComSummary' 		=> $comSummary
			]);

	}
	public function getThisPropDetails($request, $response, $args)
	{
		$propID = $request->getParam('propID');
		$selVal = $request->getParam('selVal');

		if (is_null($propID)) {
			return $this->returnThis($response,[
				'success'			=> true,
				'has_login' 		=> true,
				'message' 			=> 'Undefiend property ID!'
			]);
		}

		$propList = [];
			$getThis  = $this->clientPaymentHistory->getDistinctProperty();
			foreach ($getThis as $key => $val_getThis) {
				$propList [] = [
					'propID' 	=>  $val_getThis['parentID'],
					'propName'  =>  $val_getThis['Property Name']
				];
			}

		$comSummary =[];
		if ($selVal == 0) {
				$getComrel = $this->clientPaymentHistory->getCommisisonDetails();
			}else{
				$getComrel = $this->clientPaymentHistory->getCommisisonDetailsByAgentPos($selVal);
			}	

			foreach ($getComrel as $key => $value) {
				if ($propID == 0) {
					$getPropertiesCom = $this->clientPaymentHistory->getPropertisCom($value['cp_id']);
				}else{
					$getPropertiesCom = $this->clientPaymentHistory->getPropertisComBySelectedProp($value['cp_id'], $propID);
				}
				// $propParent 	  = $this->clientPaymentHistory->getPropertisComBySelectedProp($value['cp_id'], $propID);
				$addcom = "";
				if ($getPropertiesCom) {

					if ($getPropertiesCom[0]['addcom'] == '0') {
						$addcom = 0;
					}else{
						$addcom = $getPropertiesCom[0]['addcom'];
					}

					$comSummary [] = [
						'0'  => $value['clientName'],
						'1'  => $getPropertiesCom[0]['propertyname'],
						'2'  => $getPropertiesCom[0]['ph'],
						'3'  => $getPropertiesCom[0]['blk'],
						'4'  => $getPropertiesCom[0]['lt'],
						'5'  => $getPropertiesCom[0]['tcp'],
						'6'  => $addcom,
						'7'  => $getPropertiesCom[0]['crs'],
						'8'   => $value['payment'],
						'9'   => $value['ReleaseDate'],
						'10'  => $value['agentName'],
						'11'  => $value['precentageValue'],
						'12' => $value['Amount'],
						'13' => $value['position'],
					];	 

				}
				
			}

		return $this->returnThis($response,[
			'success'			=> true,
			'has_login' 		=> true,
			'prop' 				=> $selVal,
			'ComSummary' 		=> $comSummary
		]);
	}


	public function getThisPropDetailsByDate($request, $response, $args)
	{
		$propID = $request->getParam('propID');
		$selVal = $request->getParam('selVal');
		$date1  = $request->getParam('date1');
		$date2  = $request->getParam('date2');

		if (is_null($date1) || is_null($date2)) {
			return $this->returnThis($response,[
				'success'			=> true,
				'has_login' 		=> true,
				'message' 			=> 'Some fields are empty!'
			]);
		}

		if (is_null($propID)) {
			return $this->returnThis($response,[
				'success'			=> true,
				'has_login' 		=> true,
				'message' 			=> 'Undefiend property ID!'
			]);
		}

		$propList = [];
			$getThis  = $this->clientPaymentHistory->getDistinctProperty();
			foreach ($getThis as $key => $val_getThis) {
				$propList [] = [
					'propID' 	=>  $val_getThis['parentID'],
					'propName'  =>  $val_getThis['Property Name']
				];
			}

		$comSummary =[];
		if ($selVal == 0) {
				$getComrel = $this->clientPaymentHistory->getComDetailsbyDateonly($date1, $date2);
			}else{
				$getComrel = $this->clientPaymentHistory->getComDetaoilsByDate($selVal, $date1, $date2);
			}	

			foreach ($getComrel as $key => $value) {
				if ($propID == 0) {
					$getPropertiesCom = $this->clientPaymentHistory->getPropertisCom($value['cp_id']);
				}else{
					$getPropertiesCom = $this->clientPaymentHistory->getPropertisComBySelectedProp($value['cp_id'], $propID);
				}
				// $propParent 	  = $this->clientPaymentHistory->getPropertisComBySelectedProp($value['cp_id'], $propID);
				$addcom = "";
				if ($getPropertiesCom[0]['addcom'] == '0') {
					$addcom = 0;
				}else{
					$addcom = $getPropertiesCom[0]['addcom'];
				}

				$comSummary [] = [
							'0'  => $value['clientName'],
							'1'  => $getPropertiesCom[0]['propertyname'],
							'2'  => $getPropertiesCom[0]['ph'],
							'3'  => $getPropertiesCom[0]['blk'],
							'4'  => $getPropertiesCom[0]['lt'],
							'5'  => $getPropertiesCom[0]['tcp'],
							'6'  => $addcom,
							'7'  => $getPropertiesCom[0]['crs'],
							'8'  => $value['agentName'],
							'9'  => $value['precentageValue'],
							'10' => $value['Amount'],
							'11' => $value['position'],
						];	 
			}

		return $this->returnThis($response,[
			'success'			=> true,
			'has_login' 		=> true,
			'prop' 				=> $selVal,
			'ComSummary' 		=> $comSummary
		]);
	}

	public function get_thisIncomeStateMent($request, $response, $args)
	{
	
		$geticome = $this->clientPaymentHistory->incomeStatement();
		if (!$geticome) {
			return $this->returnThis($response,[
				'success'			=> false,
				'has_login' 		=> true,
				'message'		    => "Sorry. unable to get report details! "
			]);
		}

		return $this->returnThis($response,[
			'success'			=> true,
			'has_login' 		=> true,
			'data'				=> $geticome
		]);

	}

	public function get_thisPropertyDetailsByDate($request, $response, $args)
	{
		$selectedProp = $request->getParam('selectedroperTy');
		$date = $request->getParam('dateToSearch');
		$type = $request->getParam('paymentType');


		$incomeDetails = $this->clientPaymentHistory->incomeStatementByDate($selectedProp, $date, $type);
		if (!$incomeDetails) {
			return $this->returnThis($response,[
				'success'			=> false,
				'has_login' 		=> true,
				'message'			=> 'No date avaialble on tge given date.'
			]);
		}


		return $this->returnThis($response,[
			'success'			=> true,
			'has_login' 		=> true,
			'data'				=> $incomeDetails
		]);
	}

	public function get_thisPropertiesAdvanceSearh($request, $response, $args)
	{
		$selectedProp = $request->getParam('propID');
		$data 		  = $request->getParam('data');
		$type 		  = $request->getParam('paymentType');
		
		$incomeDetails = $this->clientPaymentHistory->incomeStatementByDateAdvanceSearch($selectedProp, $data, $type);
		if (!$incomeDetails) {
			return $this->returnThis($response,[
				'success'			=> false,
				'has_login' 		=> true,
				'message'			=> 'No date avaialble on tge given date.'
			]);
		}

		return $this->returnThis($response,[
			'success'			=> true,
			'has_login' 		=> true,
			'data'				=> $incomeDetails		
		]);
	}

	public function get_thisAdditionalCharges($request, $response, $args)
	{
		$chargeList 		= [];
		$office   			= 0; 
		$partner1  			= 0; 
		$partner2 			= 0;
		$transactionType 	= $request->getParam('transactionType');
		$charges = $this->clientPaymentHistory->getCharges();
		foreach ($charges as $key => $value) {
			$propertyDev = $this->clientPaymentHistory->getPropertyPercentage($value['id']);
			if ($propertyDev) {
				$office  	= $propertyDev[0]['a'];
				$partner1 	= $propertyDev[0]['b'];
				$partner2 	= $propertyDev[0]['c'];
			}else{
				$office  	= 0;
				$partner1 	= 0;
				$partner2	= 0;
			}

	

			if ($value['charges'] !== "	null")  {
				foreach (json_decode($value['charges'], true) as $key => $val) {

					$thiDaTe = (!isset($val['chargeDate'])) ? 'none' : $val['chargeDate'];

					$payment = (!isset($val['transactionType'])) ? 'none' : $val['transactionType'];
						
					if($transactionType == $payment){
						$list = [
							0 	=> $thiDaTe,
							1 	=> $value['name'],
							2 	=> $value['property'],
							3	=> $val['remittanceName'],
							4 	=> $val['chargeValue'],
							5 	=> ($val['chargeValue'] * ($office / 100)),
							6 	=> $office . ' %' ,
							7 	=> ($val['chargeValue'] * ($partner1 / 100)),
							8 	=> $partner1 .' %',
							9 	=> $val['chargeDescription'],
							10 	=> $value['id'],
							11 	=> ($val['chargeValue'] * ($partner2 / 100)),
							12	=> $partner2 . '%'
						];
						array_push($chargeList, $list);
					}
					
					if($transactionType == 'All'){
						$list = [
							0 	=> $thiDaTe,
							1 	=> $value['name'],
							2 	=> $value['property'],
							3	=> ($payment == 'none' ? 'None' : (($payment == '0') ? $val['remittanceName'] : (($payment == '1') ? $val['remittanceName'].' (Bank)' : ''))),
							4 	=> $val['chargeValue'],
							5 	=> ($val['chargeValue'] * ($office / 100)),
							6 	=> $office . ' %' ,
							7 	=> ($val['chargeValue'] * ($partner1 / 100)),
							8 	=> $partner1 .' %',
							9 	=> $val['chargeDescription'],
							10 	=> $value['id'],
							11 	=> ($val['chargeValue'] * ($partner2 / 100)),
							12	=> $partner2 . '%'
						];
						array_push($chargeList, $list);
					}
						
				

				}
			}
		}

		if (!$charges) {
			return $this->returnThis($response,[
				'success'			=> false,
				'has_login' 		=> true,
				'data'				=> 'List of additional charges not avaialble!'
			]);			
		}

		return $this->returnThis($response,[
			'success'			=> true,
			'has_login' 		=> true,
			'data'				=> $chargeList
		]);	
	}
	

	public function get_additionalCharges($request, $response, $args)
	{
		$data = $request->getParam('data');
		$office   = 0; 
		$partner  = 0; 
		$partner2 = 0;
		$charges = $this->clientPaymentHistory->getChargesAdvanceSearch($data);

		return $this->returnThis($response,[
				'success'			=> false,
				'has_login' 		=> true,
				'data'				=> 'List of additional charges not avaialble!'
			]);		

		foreach ($charges as $key => $value) {
			$propertyDev = $this->clientPaymentHistory->getPropertyPercentage($value['id']);
			if ($propertyDev) {
				$office  = $propertyDev[0]['a'];
				$partner = $propertyDev[0]['b'];
				$partner2 = $propertyDev[0]['c'];
			}else{
				$office  = 0;
				$partner = 0;
				$partner2 = 0;
			}


			if ($value['charges'] !== "	null")  {
				foreach (json_decode($value['charges'], true) as $key => $val) {

					$thiDaTe = (!isset($val['chargeDate'])) ? 'none' : $val['chargeDate'];
				 
					$list = [
						0 => $thiDaTe,
						1 => $value['name'],
						2 => $value['property'],
						3 => $val['chargeValue'],
						4 => ($val['chargeValue'] * ($office / 100)),
						5 => $office . ' %' ,
						6 => ($val['chargeValue'] * ($partner / 100)),
						7 => $partner .' %',
						8 => $val['chargeDescription'],
						9 => $value['id'],
						10 => ($val['chargeValue'] * ($partner2 / 100)),
						11 => $partner2 . '%'
					];
					array_push($chargeList, $list);
				}
			}
		}

		if (!$charges) {
			return $this->returnThis($response,[
				'success'			=> false,
				'has_login' 		=> true,
				'data'				=> 'List of additional charges not avaialble!'
			]);			
		}

		return $this->returnThis($response,[
			'success'			=> true,
			'has_login' 		=> true,
			'data'				=> $chargeList
		]);	
	}

	public function get_userAccount($request, $response, $args)
	{
		return $this->returnThis($response,[
			'success'			=> true,
			'has_login' 		=> true,
			'role'				=> $_SESSION['UserRole']
		]);	
	}

	public function getAllPropertyName($request,$response,$args){
		$getPropertyName = $this->clientPaymentHistory->getPropertyName();
		
		if(!$getPropertyName){
			return $this->returnThis($response,[
				'success'		=>	false,
				'has_login'		=>  true,
				'message'		=> 'Invaid'
			]);
		}

		return $this->returnThis($response,[
			'success'		=> true,
			'has_login'		=> true,
			'result'		=> $getPropertyName
		]);

		
	}

	public function getCashIncome($request,$response,$args){
		$chargeList 			= [];
		$allCharge				= [];
		$overallIncome  		= $this->clientPaymentHistory->getCashIncome();
		$charges	   			= $this->clientPaymentHistory->getChargeAndPropertyName();
		$getPropertyParentAsc 	= $this->clientPaymentHistory->getPropertyParentAsc();
		
		foreach ($charges as $key => $value) {
			if ($value['charges'] !== "	null")  {
				foreach (json_decode($value['charges'], true) as $key => $val) {
					foreach($getPropertyParentAsc as $propName => $v){
						if ($v['propertyName'] === $value['propertyName']){
								
								if (empty($chargeList[$v['propertyName']])){
									$chargeList[trim($value['propertyName'],' ')] = floatval(0+$val['chargeValue']);
								}else{
									$chargeList[trim($value['propertyName'],' ')] = floatval($chargeList[$value['propertyName']]+$val['chargeValue']);
								}
						}
					}
				}
			}

		}
			array_push($allCharge,$chargeList);
			if (!$charges) {
				return $this->returnThis($response,[
					'success'			=> false,
					'has_login' 		=> true,
					'data'				=> 'List of additional charges not avaialble!'
				]);			
			}
			return $this->returnThis($response,[
				'success'			=> true,
				'has_login'			=> true,
				'overallIncome'		=> $overallIncome,
				'property'			=> $getPropertyParentAsc,
				'totalCharges'		=> $allCharge

			]);
	}


}