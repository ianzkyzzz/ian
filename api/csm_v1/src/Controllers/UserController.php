<?php
/**
 * Created by Raymund Matol
 * Date: 1/31/16
 */

namespace App\Controllers;

use App\Models\User;
use App\Helpers\Validation;
use App\Controllers\ParentController;
/**
 * Class UserController
 * @package App\Controllers
 */
 
class UserController extends ParentController{

	protected $user;
	protected $validation;

	public function __construct(User $user, Validation $validation) {

		// User
		$this->user = $user;

		// Validation Helper
		$this->validation = $validation;

	}
	// $_SESSION['Name'] == DEBONER21 DULOS 
	/**
     * @param $request
     * @param $response
     * @return $response
     */

	// -------------------------------------------------------------------------
    //  User List
    // -------------------------------------------------------------------------
	public function getAlluserActive($request, $response, $args) {
		
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}
		

		$result = $this->user->getUserList();
		
		return $this->returnThis($response,[
			'success'	=> true,
			'has_login' => true,
			'users' 	=> $result
		]);
	}

	public function getAllDataDashboard($request, $response, $args){
		// $getYear   = $this->user->getYearList();
		// $getYearSales = $this->user->getTotalSalesForYear();

		$todaysCharges = 0;
		$WeekCharges = 0;
		$MonthCharges = 0;
		
		$pos = $_SESSION['UserRole'];
		$agentID = $_SESSION['userID'];

		$chargesList = [];

		if ($pos == "Agent") {
			$clients 		= $this->user->getAgentAssociatedClients($agentID)[0]['clientCount'];
			$totalCom 		= $this->user->getAgentTotalCom($agentID)[0]['total'];
			$commissionale 	= $this->user->getAgentCommissionable($agentID);
			$amount = 0;

			if(count($commissionale) <= 0){
				 
			}else{
				foreach ($commissionale as $key => $values) {
					$clientProperty 	= $this->user->getAllPropertyByClientOwnerID($values['cp_id']);

					if(count($clientProperty) <= 0){
						continue;
					}		

					$contractPrice 			= floatval($clientProperty[0]['contractPrice']);
					$comReleased 		    = $clientProperty[0]['commissionReleased'];

			
					$amount 		 += ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comReleased ) - ((($contractPrice * floatval(($values['percentagevalue']/ 100))) / $comReleased ) * 0.05));

				}
			}

			$dash [] = [
				'clientCount'  		=> $clients,
				'totalCom'     		=> $totalCom,
				'commissionable'    => number_format($amount, 2),
				'position' 	   		=> $pos
			];
			
		}else{

			$getMonths = $this->user->getMonthList();
			$data = [];
			$x = 0;
			foreach ($getMonths as $value){
				$x++;
				$totalSalesForMonths = $this->user->getTotalSalesForEachMonths($value['month']);
				 
				$data[$x] =[
				 	'month' 		=> $totalSalesForMonths[0]['Month'],
				 	'income' 		=> $totalSalesForMonths[0]['Sales'], 
				 	'expenses' 		=> 31.1
				 	];
					 
			}

			$clients 			= $this->user->getClientsCount()[0]['clientCount'];
			$Agents  			= $this->user->getAgentCount();
			$Sales   			= $this->user->getSumSales();
			$GrossRel   		= $this->user->getGrossRel();
			$property   		= $this->user->propertyReports();
			$subDev     		= $this->user->propertyReportsSubDev();
			$tax  	    		= $this->user->getTax();
			$totalCom   		= $this->user->geTotalCom();
			$releaseCom 		= $this->user->agentCommissionReleases();
			$getDailyCharges 	= $this->user->getDailyCharges();
			$getWeeklyCharges	= $this->user->getWeeklyCharges();
			$getMonthlyCharges  = $this->user->getMonthlyCharges();
			$getCharges 		=array_merge( $getDailyCharges, $getWeeklyCharges,$getMonthlyCharges);

			if ($pos == 'Business Partner') {				

				$clients 	= $this->countClientsUnderBusinessPartnerProperty($agentID);
				$Sales   	= $this->totalSalesUnderBusinessPartnerProperty($agentID);
				$property   = $this->getTotalPropertyReportSales($agentID);
				$subDev     = $this->getTotalpropertyReportsSubDev($agentID);

				// $getCharges = $this->user->getAllCharges();
				$getDailyCharges 	= $this->user->getDailyCharges();
				$getWeeklyCharges	= $this->user->getWeeklyCharges();
				$getMonthlyCharges  = $this->user->getMonthlyCharges();
				$getCharges 		=array_merge( $getDailyCharges, $getWeeklyCharges,$getMonthlyCharges);
			}

			// if ($getCharges) {
			// 	foreach ($getCharges as $key => $value) {
			// 		if ($value['c'] !== "null") {
			// 			foreach (json_decode($value['c']) as $key => $data) {
			// 				array_push($chargesList, $data);
			// 			}
			// 		}
			// 	}	
			// }


			
			$dash [] = [
				'clientCount'   => $clients,
				'AgentCount'    => $Agents[0]['agentCount'],
				'Sales' 	    => $Sales,
				'SalesReport'	=> $data,
				'totalGross'	=> $GrossRel[0]['gross'],
				'prop'			=> $property,
				'propSub'	    => $subDev, 
				'tax'	        => $tax, 
				'com'    	    => $totalCom[0]['com'], 
				'rel'	        => $releaseCom,
				'chargesDetais' => $getCharges,
				'position' 		=> $pos
			];
		}

		return $this->returnThis($response ,[
			'success'		=> true,
			'has_login' 	=> true,
			'data' 			=> $dash
		]);
	}


	public function getAllTotalCharges($userID)
	{
		$proplist 	 = $this->user->getPropertiesGues($userID);
		$getPropList = [];

		if ($proplist) {
			if ($proplist[0]['property']) {
				$proplist = json_decode($proplist[0]['property']);
				$getPropList = $this->user->getAllChargesGuest($proplist);
			}
		}

		return $getPropList;
	}


	public function getTotalpropertyReportsSubDev($userID)
	{
		$proplist 	 = $this->user->getPropertiesGues($userID);
		$getClients = [];

		if ($proplist) {
			if ($proplist[0]['property']) {
				$propList = json_decode($proplist[0]['property']);
				$getClients = $this->user->propertyReportsSubDevGuest($propList);
			}
		}

		return $getClients;
	}


	public function getTotalPropertyReportSales($userID)
	{

		$proplist 	 = $this->user->getPropertiesGues($userID);
		$getClients = [];

		if ($proplist) {
			if ($proplist[0]['property']) {
				$propList = json_decode($proplist[0]['property']);
				$getClients = $this->user->propertyReportsGuest($propList);
			}
		}

		return $getClients;
	}

	public function totalSalesUnderBusinessPartnerProperty($userID)
	{
		$currentSales = 0;
		$weeklySales = 0;
		$monthlySales = 0;

		$proplist 	 = $this->user->getPropertiesGues($userID);

		if ($proplist[0]['property']) {
			foreach (json_decode($proplist[0]['property'], true) as $key => $value) {
				$getClients = $this->user->getSumSalesGuest($value);

				$currentSales += $getClients[0]['Sales'];
				$weeklySales += $getClients[1]['Sales'];
				$monthlySales += $getClients[2]['Sales'];
			}
		}

		$sales = array(
			array( 
		        "Sales" => $currentSales, 
		    ), 
			array( 
		        "Sales" => $weeklySales, 
		    ),
			array( 
		        "Sales" => $monthlySales, 
		    )
		);

		return $sales;

	}

	public function countClientsUnderBusinessPartnerProperty($userID)
	{
		$proplist 	 = $this->user->getPropertiesGues($userID);
		$clientList = [];

		if ($proplist[0]['property']) {
			foreach (json_decode($proplist[0]['property'], true) as $key => $value) {
				// return $value;

				$getClients = $this->user->getClientList($value);
				foreach ($getClients as $k => $clientID) {
					array_push($clientList, $clientID['clientID']);
				}
			}
		}
		
		$clientList = array_unique($clientList);
		return count($clientList);
	}


	public function uploadThisImage($request, $response, $args){
		
		$upload_errors = array(
			UPLOAD_ERR_OK => "No Errors",
			UPLOAD_ERR_INI_SIZE => "Larger than upload max_filesize.",
			UPLOAD_ERR_FORM_SIZE => "Larger than FORM_MAX_FILE_SIZE.",
			UPLOAD_ERR_PARTIAL => "Partial Upload.",
			UPLOAD_ERR_NO_FILE => "No File.",
			UPLOAD_ERR_NO_TMP_DIR => "No Temporary Directory.",
			UPLOAD_ERR_CANT_WRITE => "Cant write to disk.",
			UPLOAD_ERR_EXTENSION => "File upload stopped by extension."
		);	
		$tmp_file = $_FILES['file_uploads']['tmp_name'];
		$target_file = basename($_FILES['file_uploads']['name']);// original filename
		$ext = pathinfo($target_file, PATHINFO_EXTENSION);
		
		$userID = $request->getParam('dataID');
		if(empty($userID)){
			return $this->returnThis($response,[
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Client ID is invalid'
			]);
		}
		else
			$target_file = $userID.'.'.$ext;

		$upload_dir = 'uploads/users/'.$userID;
		$itemurls =  "uploads/users/".ucwords($target_file);

		

		if(!is_dir($upload_dir)) {
            mkdir($upload_dir, 0777, true);
        }

        if ( !is_writable($upload_dir)) {
		    return $this->returnThis($response,[
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'File directory is not writable.'
			]);
		}

		try {
			

			$$userID = $this->user->getThisUser($userID);

			if(count($userID) != 1){
				return $this->returnThis($response,[
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Invalid User ID.'
				]);
			}


			if(!move_uploaded_file($tmp_file, $upload_dir."/".$target_file)){
				return $this->returnThis($response,[
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Failed to upload image.'.$_FILES["file_uploads"]["error"]
				]);
			}


			$datas = $this->user->updateUserImagePath($target_file,$userID);

			if($datas)
				return $this->returnThis($response,[
					'success'		=> true,
					'has_login' 	=> true,
					'userID'		=> $userID,
					'message'		=> 'Upload successfull.'
				]);
			else
				return $this->returnThis($response,[
					'success'		=> false,
					'has_login' 	=> true,
					'message'		=> 'Failed to save image path to database.'
				]);

		} catch (Exception $e) {
			return $this->returnThis($response,[
				'success'		=> false,
				'has_login' 	=> true,
				'message'		=> 'Failed to upload image. '.json_encode($e)
			]);
		}

	}	


	public function getAllInfoForThisUser($request, $response, $args) {
		
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}

		$id = $request->getParam('ID');
		
		if(empty($id))
			return $this->returnThis($response , ['success'	=> false,'has_login' => true,'message'		=> 'User ID to be update is required.']);	
 
		$result = $this->user->getThisUser($id);
		
		return $this->returnThis($response,[
			'success'		=> true,
			'has_login' 	=> true,
			'user' 			=> $result,
			'userRole'		=>$_SESSION['UserRole']
		]);
	}
	
	
	public function changePasswordForThis($request, $response, $args) {
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}
		

		$oldpass 		= $request->getParam('oldPassword');
		$newPass		= $request->getParam('newPassword');
		
		if( empty($oldpass) || empty($newPass)) {
			return $this->returnThis($response,[
				'success' => false,'has_login' => true,
				'message' 	  => "Please Fill-up all the required fields."
			]);
		}
		
		$newpass_hash = $this->getHash($newPass);
		
		$result = $this->user->getAllHashWithUserID($_SESSION['userID']);
		foreach ($result as $key => $value) {
			if(password_verify ( $oldpass , $value['Password'])){ 
				
				$result = $this->user->updatePassword($newpass_hash,$_SESSION['userID']);
				if($result)
					return $this->returnThis($response,array(
						'success'	=> true,'has_login' => true,
						'message'	=> 'Password successfully changed.'
					));
				else
					return $this->returnThis($response,array(
						'success'	=> false,'has_login' => true,
						'message'	=> 'Failed to change password.'
					));
			}
		}

		return $this->returnThis($response,array(
				'success'	=> false,'has_login' => true,
				'message'	=> 'Wrong old password entered.'
			));

	}

	// -------------------------------------------------------------------------
    //  Add User 
    // -------------------------------------------------------------------------
	public function postUser($request, $response, $args) {
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}


		// print_r($request->getQueryParams());  // this for GET request
		// print_r($request->getParsedBody());  // this for POST or PUT request
		$emptyField = [];
		foreach($request->getParsedBody() as $key => $val) {
			if($key != 'Email')
				if(empty($val)  || !isset($val)){
					$emptyField[] = $key;
				}
		}
 
		if(count($emptyField) > 0)
			return $this->returnThis($response , ['success'	=> false,'has_login' => true,
				'message'		=> ''.implode(', ', $emptyField).' are required.']);


		$Fname         = ucwords($request->getParam('Fname'));
	  	$Lname         = ucwords($request->getParam('Lname'));
	  	$Mname         = ucwords($request->getParam('Mname'));
	  	$Address       = ucwords($request->getParam('Address'));
	  	$ContactNumber = $request->getParam('ContactNumber');
	  	$Email         = $request->getParam('Email');
	  	$Position      = ucwords($request->getParam('Position'));
	  	$Username      = $request->getParam('Username');
	  	$Password      = $request->getParam('Password');


	  	$result = $this->user->isUserExist($Username);
	  	if($result)
	  		return $this->returnThis($response , ['success'	=> false,'has_login' => true,
				'message'		=> 'This username is already used.']);


		
		$pass_hash = $this->getHash($Password);

		$result = $this->user->postUser($Fname, $Lname, $Mname, $Address, $ContactNumber,$Email,$Position,$Username,$pass_hash);

		if(!$result) 
			return $this->returnThis($response,[
				'success' => false,'has_login' => true
			]);
		else 
			return $this->returnThis($response,[ 
				'success' => true,'has_login' => true
			]);

	}


	public function getHash($rand_string){
		
		$options = [
		    'cost' => 11,
		    // 'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM)
		];
		
		// password_verify ( string $password , string $hash )
		return password_hash($rand_string, PASSWORD_BCRYPT, $options);
	}

	// -------------------------------------------------------------------------
    //  Update User 
    // -------------------------------------------------------------------------
	public function updateUserInfo($request, $response, $args) {
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}


		// print_r($request->getQueryParams());  // this for GET request
		// print_r($request->getParsedBody());  // this for POST or PUT request

		$emptyField = [];
		foreach($request->getParsedBody() as $key => $val) {
			if( ($key != 'Email') && ($key != 'Position'))
				if(empty($val)  || !isset($val)){
					$emptyField[] = $key;
				}
		}

		if(count($emptyField) == 1){

			return $this->returnThis($response , ['success'	=> false,'has_login' => true,
				'message'		=> ''.implode(', ', $emptyField).' is required.']);

		}else if(count($emptyField) > 1){

			return $this->returnThis($response , ['success'	=> false,'has_login' => true,
				'message'		=> ''.implode(', ', $emptyField).' are required.']);
		}


		$ID            = $request->getParam('ID');
		$Fname         = ucwords($request->getParam('Fname'));
	  	$Lname         = ucwords($request->getParam('Lname'));
	  	$Mname         = ucwords($request->getParam('Mname'));
	  	$Address       = ucwords($request->getParam('Address'));
	  	$ContactNumber = $request->getParam('ContactNumber');
	  	$Email         = $request->getParam('Email');
	  	$Position      = ucwords($request->getParam('Position'));


	  	if ($_SESSION['UserRole'] != 'Admin') {

	  		$result = $this->user->updateUserInfoGues($ID,$Fname, $Lname, $Mname, $Address, $ContactNumber,$Email,$Position);
	  	
	  	}else{

			$result = $this->user->updateUserInfo($ID,$Fname, $Lname, $Mname, $Address, $ContactNumber,$Email,$Position);
	  	}


		if(!$result) {
			
			return $this->returnThis($response,[
				'success' => false,'has_login' => true,
				
			]);
		}
		else {
			if($ID == $_SESSION['userID']){
				$_SESSION['Name'] = ($Fname .' '. $Lname);
			}

			return $this->returnThis($response,[ 
				'success' => true,'has_login' => true,
				'activeUserFullname' 	=>($Fname .' '. $Lname)
			]);
		}
			

	}



	public function updateUserLoginCredential($request, $response, $args) {
		if(!$this->isLogin()){
			return $this->returnThis($response , ['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}


		$emptyField = [];
		foreach($request->getParsedBody() as $key => $val) {
			if(empty($val)  || !isset($val)){
				$emptyField[] = $key;
			}
		}



		if(count($emptyField) > 0)
			return $this->returnThis($response , ['success'	=> false,'has_login' => true,
				'message'		=> ''.implode(', ', $emptyField).' are required.']);


		$ID 		= $request->getParam('ID');
		// $Username 	= $request->getParam('Username');
		$Password 	= $request->getParam('Password');
		
		$pass_hash = $this->getHash($Password);

		$result = $this->user->updateUserLoginCredential($ID,$pass_hash);

		if(!$result) 
			return $this->returnThis($response,[
				'success' => false,'has_login' => true,
				'userFullname' => ($Fname.' '. $Lname)
			]);
		else 
			return $this->returnThis($response,[ 
				'success' => true,'has_login' => true
			]);

	}


	public function saveThisIpAddress($request, $response, $args) {
		$isp = $request->getParam('isp');
		$this->user->saveThisIP($isp);
		
		return $this->returnThis($response,['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
	}




	// -------------------------------------------------------------------------
    //  Soft Remove User 
    // -------------------------------------------------------------------------
	public function removeThisUser($request, $response, $args) {
		if(!$this->isLogin()){
			return $this->returnThis($response,['success'	=> false,'has_login' => false,'message'		=> 'You are not login']);
		}
		
		
		
		$ID 		= $request->getParam('ID');
		$Position 	= $request->getParam('Position');
		
		$res = $this->user->getUserByID($ID);
		if(count($res) > 0){
			if($res[0]['Position'] == 'Admin'){								// if new role is equal to superadmin
				if($Position != $res[0]['Position']){
					if(count($this->user->getUserByRoleActive('Admin')) == 1){ //    if there are only one superadmin then this cant be updated
						return $this->returnThis($response,[
							'success' => false,'has_login' => true,
							'message' => 'Cant update this. This is the only Admin you have.'
						]);
					}
				}
			}
		}
		else{
			return $this->returnThis($response,[
					'success' => false,'has_login' => true,
					'message' => 'Can not find this user.'
				]);
		}

		if( empty($ID) ) {
			return $this->returnThis($response,[
				'success' => false,'has_login' => true,'message'=>'User ID is required.'
			]);
		}

		$result = $this->user->removeUser($ID);

		if($result) 
			return $this->returnThis($response,[
				'success' => true,'has_login' => true,'message'=>'User successfully removed.'
			]);
		else 
			return $this->returnThis($response,[
				'success' => false,'has_login' => true,'message'=>'Failed to removed user.'
			]);
	}

	// -------------------------------------------------------------------------
    //  Generate Random String 
    // -------------------------------------------------------------------------
	private function generateRandomString($length = 5) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	// Display Login Page
	public function loginForm($request,$response,$args) {
	
		if ( isset($_SESSION['userID']) ){
			return $this->returnThis($response,array(
				'success'	=> true,
				'has_login' => true
				
			));
		}
		return $this->returnThis($response,array(
			'success'	=> true,
			'has_login' => false
		));
	}



	
	public function validateUser($request,$response,$args){
		if(!$this->isLogin()){
			return $this->returnThis($response,array(
				'success'	=> true,
				'login' => false
			));
		}
		else{
			return $this->returnThis($response,array(
				'success'	=> true,
				'login' => true
			));
		}
	}

	// Login
	public function accessLogin($request,$response,$args) {
		$username 	= $request->getParam('username');
		$password 	= $request->getParam('password');
		

		$result = $this->user->getAllHashWithUsername($username);
		// if((password_verify ( $password , $value['Password'])){

		// }else{
		// 	$result = $this->checkAgentUser($username);
		// 	if ((password_verify ( $password , $value['Password'])) {
				
		// 	}
		// }
		foreach ($result as $key => $value) {
			if($value['Username'] == $username){
				if(password_verify ( $password , $value['Password'])){
					$_SESSION['userID'] 	= $value['UserID'];
					$_SESSION['imagePath'] 	= $value['img_filename'];
					$_SESSION['UserRole']   = $value['Position'];
					$_SESSION['Name'] 		= $value['Fname']. ' ' . $value['Lname'];

					$position 				= $value['Position'];
 					 
					return $this->returnThis($response,array(
						'success'	=> true,'has_login' => true, 'position' => $position 
					));
				}
			}
		}

		return $this->returnThis($response,array(
				'success'	=> false,'has_login' => false,
				'message'	=> 'Invalid Username/Password!'
			));

	}

	// Logout
	public function logoutThisUser($request,$response,$args){
		session_destroy();
		return $this->returnThis($response,array(
			'success'	=> true
		));
	}


	public function update_changePercentage($request,$response,$args)
	{
		$propID = $request->getParam('propID');
		$val    = $request->getParam('val');
		$dev    = $request->getParam('dev');

		$has_val = $this->user->checkPercentageVal($propID);
	
		if(!$has_val){
			$insertNewVal = $this->user->inserNewPercentage($propID, $val, $dev);
			if(!$insertNewVal){
				return $this->returnThis($response,array(
					'success'	=> false,'has_login' => false,
					'message'	=> 'Unable to update percentage'
				));
			}
			
		}else{

			$totalPercentageValue = $this->user->validatePercentageValue($propID,$dev);
			$percentageSum =($totalPercentageValue[0]['1'] + $totalPercentageValue[0]['2'] + $val);
		
			if ($percentageSum > 100){
				return $this->returnThis($response,array(
					'successs' => false,
					'message'  => 'Percentage is greater than 100%'
				));
			}
			
			$updatePercentage = $this->user->changeThisPercentageVal($propID, $val, $dev);
			if(!$updatePercentage){
				return $this->returnThis($response,array(
					'success'	=> false,'has_login' => false,
					'message'	=> 'Unable to update percentage'
				));
			}
		}

		


		return $this->returnThis($response,array(
				'success'	=> true,
				'message'	=> 'Successfully Updated'
		));
		

	
	}

	public function updateUsernameUser($request,$response,$args)
	{
		$username = $request->getParam('username');
		$userID   = $request->getParam('ID');

		$update = $this->user->updateUsernameUser($userID, $username);
		if (!$update) {
			return $this->returnThis($response,array(
				'success'	=> false,'message' => 'Unable to update username.' 
			));
		}

		return $this->returnThis($response,array(
			'success'	=> true,'message' => 'Username successfully updated. '
		));
	}

	public function checkCurrentPass($request,$response,$args)
	{
		$pass 	  = $request->getParam('pass');
		$userID   = $request->getParam('ID');

		$checkPass = $this->user->checkCurrentPass($userID);
		if (!$checkPass) {
			return $this->returnThis($response,array(
				'success'	=> false,'message' => 'Something went wrong unable to locate user pass.' 
			));
		}

		if (password_verify($pass, $checkPass[0]['pass'])) {
			return $this->returnThis($response,array(
				'success'	=> true 
			));
		}else{
			return $this->returnThis($response,array(
				'success'	=> false
			));
		}
	}

	public function getCheckIfDonSakagawa($request,$response,$args){
			return $this->returnThis($response,[
				'userID'	=> $_SESSION['userID']
			]);
	}

}