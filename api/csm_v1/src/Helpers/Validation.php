<?php
// Define Namespace
namespace App\Helpers;

// Model 
use App\Models\User;


//Class
class Validation {

    protected $user;

    public function __construct( User $user) {
        $this->user = $user;
    }

    
    
    // Validate User's Username Duplication 
    public function isUserExist($username) {
        if($this->user->isUserExist($username)) {
            return true;
        }
        else {
            return false;
        }
    }

}