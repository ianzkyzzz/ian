<?php

namespace App\Models;

use PDO;

class Agent {

    /**
    * @var $db
    */
    private $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }
    

    public function updateThisAgent($agentFname,$agentLname,$agentMname,
        $agentAddress,$agentContactNumber,$agentEmail,$userID,$id, $accountNumber){

        try {
            $sql = "
                UPDATE
                    tbl_agent
                SET 
                    Fname               = :agentFname,
                    Lname               = :agentLname,
                    Mname               = :agentMname,
                    address             = :agentAddress,
                    contactNumber       = :agentContactNumber,
                    email               = :agentEmail,
                    accountNumber       = :accountNumber
                WHERE
                    agent_id            = :id

            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute([
                    'agentFname'            => $agentFname,
                    'agentLname'            => $agentLname,
                    'agentMname'            => $agentMname,
                    'agentAddress'          => $agentAddress,
                    'agentContactNumber'    => $agentContactNumber,
                    'agentEmail'            => $agentEmail,
                    'id'                    => $id,
                    'accountNumber'         => $accountNumber,
                ]);


        } catch (PDOException $e) {
            return $e;
        }

    }

    public function getPropertyList(){
        try {
            $sql="
                SELECT  
                   DISTINCT tbl_clientpaymenthistory.cp_id as 'propId',
                    concat(tbl_propertyparent.propertyName, ' P ', tbl_propertylist.phaseNumber, ' Blk ',       tbl_propertylist.block, ' Lt ', tbl_propertylist.lot) as 'Property'
                FROM
                    tbl_client_properties,
                    tbl_commissions,
                    tbl_propertylist,
                    tbl_propertyparent,
                    tbl_clientpaymenthistory
                where
                    tbl_client_properties.cp_id = tbl_clientpaymenthistory.cp_id
                and
                    tbl_client_properties.cp_id = tbl_commissions.cp_id    
                and 
                    tbl_clientpaymenthistory.cp_id = tbl_clientpaymenthistory.cp_id
                and 
                    tbl_propertylist.propertyParentID = tbl_propertyparent.id
                and 
                    tbl_propertylist.property_id = tbl_client_properties.property_id
                and 
                    tbl_clientpaymenthistory.active = 1 
                and 
                    tbl_commissions.comReleaseNo = 0
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getAllPropertyUnderPayment()
    {
        try {
            $sql="
                SELECT  
                   DISTINCT  
                   tbl_propertyparent.id as 'parentID',
                   tbl_propertyparent.propertyName as 'Property'
                FROM
                    tbl_client_properties,
                    tbl_commissions,
                    tbl_propertylist,
                    tbl_propertyparent,
                    tbl_clientpaymenthistory
                where
                    tbl_client_properties.cp_id = tbl_clientpaymenthistory.cp_id
                and
                    tbl_client_properties.cp_id = tbl_commissions.cp_id    
                and 
                    tbl_clientpaymenthistory.cp_id = tbl_clientpaymenthistory.cp_id
                and 
                    tbl_propertylist.propertyParentID = tbl_propertyparent.id
                and 
                    tbl_propertylist.property_id = tbl_client_properties.property_id
                and 
                    tbl_clientpaymenthistory.active = 1 
                and 
                    tbl_commissions.comReleaseNo = 0

            ";
            
            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getPropertyReleaseCom($propID)
    {
         try {
            $sql = "
                SELECT
                    agentID,
                    cp_id
                from
                    tbl_commissions
                where
                    cp_id = :cp_id
                and 
                    active  = 1
                and 
                    comReleaseNo = 0
            ";
            $statement = $this->db->prepare($sql);
            $statement->execute([
                'cp_id' => $propID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
         } catch (PDOException $e) {
             return $e;
         }
    }

    public function ComRelease($agentID, $cp_id, $PropertyName, $originalPrice, $amount, $ornumberdAgentProperty, $releaseDatesdAgentProperty, $userID, $clientID){
        try {
            $sql="
                INSERT INTO
                    commisionrelease(
                        agentid,
                        cp_id,
                        propertyName,
                        contractPrice,
                        amount,
                        releaseNO,
                        releaseDate, 
                        releaseBy, 
                        client_id
                        )
                VALUES
                    (
                        :agentid,
                        :cp_id,
                        :propame,
                        :originalPice,
                        :amount,
                        :ornumber,
                        :dateRelease,
                        :userId,
                        :clientId
                    )

            ";
            $statement = $this->db->prepare($sql);
            $statement->execute([
                'agentid' => $agentID,
                'cp_id' => $cp_id,
                'propame' => $PropertyName,
                'originalPice' => $originalPrice,
                'amount' => $amount,
                'ornumber' => $ornumberdAgentProperty,
                'dateRelease' => $releaseDatesdAgentProperty,
                'userId' => $userID,
                'clientId' => $clientID
                ]);
        } catch (PDOException $e) {
            return $e;
        }
    }   

    public function getPropertyDetails($agentID, $propID){
        try {
            $sql="
                SELECT concat(tbl_propertyparent.propertyName, ' Ph ', tbl_propertylist.phaseNumber, ' Blk ', tbl_propertylist.block, ' Lt ', tbl_propertylist.lot) as 'PropertyName', tbl_propertylist.originalPrice as 'originalPrice', 
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'amount' 
                from 
                    tbl_propertylist, tbl_propertyparent, tbl_client_properties, tbl_commissions 
                WHERE 
                    tbl_commissions.cp_id = tbl_client_properties.cp_id 
                and 
                    tbl_client_properties.property_id = tbl_propertylist.property_id 
                AND 
                    tbl_propertylist.propertyParentID = tbl_propertyparent.id 
                and 
                    tbl_client_properties.active = 1 
                AND 
                    tbl_commissions.agentID = :id
                and 
                    tbl_client_properties.cp_id =:cp_id
            ";


            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id' => $agentID,
                'cp_id' => $propID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function updateThisComByProppertyList($propID){
        try {
            $sql="
                UPDATE
                    tbl_commissions
                set
                    comReleaseNo = 1
                where
                    cp_id = :cp_id
                and 
                    active = 1
            ";
            $statement = $this->db->prepare($sql);
            $statement ->execute([
                'cp_id' => $propID
                ]);
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getThisAgentWithPropToInsert($propID){
        try {
            $sql="
                SELECT 
                    tbl_commissions.agentid as 'id',
                    tbl_commissions.cp_id as 'cp_id',
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as '3'
                from 
                    tbl_propertylist, 
                    tbl_agent, 
                    tbl_commissions, 
                    tbl_client_properties 
                where 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_agent.agent_id = tbl_commissions.agentID 
                and 
                    tbl_client_properties.cp_id = tbl_commissions.cp_id
                and 
                    tbl_client_properties.cp_id = :id
                and 
                    tbl_commissions.comReleaseNo = 0
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id' => $propID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getThisAgentWithCommission($propID){
        try {
            $sql="
                SELECT 
                    concat(tbl_agent.Fname, ' ', tbl_agent.Mname, ' ', tbl_agent.Lname) as '0',
                    IF(tbl_agent.agentPosition = 1,'Sales Director Head', IF(tbl_agent.agentPosition = 2,'Sales Director', IF(tbl_agent.agentPosition = 3,'Unit Manager', IF(tbl_agent.agentPosition = 4, 'Manager',  'Agent'))) ) as '1',
                    concat(tbl_commissions.percentagevalue,'% ') as '2',
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as '3'
                from 
                    tbl_propertylist, 
                    tbl_agent, 
                    tbl_commissions, 
                    tbl_client_properties 
                where 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_agent.agent_id = tbl_commissions.agentID 
                and 
                    tbl_client_properties.cp_id = tbl_commissions.cp_id
                and 
                    tbl_client_properties.cp_id = :id
                and 
                    tbl_commissions.comReleaseNo = 0
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id' => $propID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getComReleasebyDate($id, $dateFrom, $dateTO){
        try {
            $sql="
                SELECT
                    commisionrelease.releaseNO as '0',
                    commisionrelease.ReleaseDate as '1',
                    commisionrelease.propertyName as '2',
                    commisionrelease.amount as '3',
                    commisionrelease.payment as '4'
                FROM    
                    commisionrelease
                where 
                    commisionrelease.ReleaseDate between :dateFrom and :dateTo
                and
                    commisionrelease.agentID = :id

            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'id'       => $id,
                    'dateFrom' => $dateFrom,
                    'dateTo'   => $dateTO
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getComRelease($id){
        try {

             $sql ="
                SELECT
                    commisionrelease.releaseNO as '0',
                    commisionrelease.ReleaseDate as '1',
                    commisionrelease.propertyName as '2',
                    commisionrelease.amount as '3',
                    commisionrelease.payment as '4'
                FROM    
                    commisionrelease
                where 
                    commisionrelease.agentID = :id
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'id' => $id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public  function agentListProperties($id){
    
        try {
            
            $sql ="
                SELECT
                    tbl_commissions.cp_id as 'client_id',
                    tbl_client_properties.agent_id as 'agent_id',
                    concat(tbl_propertyparent.propertyName, ' P', tbl_propertylist.phaseNumber, ' Blk', tbl_propertylist.block, ' Lt', tbl_propertylist.lot, ' (', tbl_commissions.percentagevalue, '%)') as 'Property',
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Commission',
                    concat( (select count(com.id) from commisionrelease com where com.agentID = tbl_commissions.agentID and com.cp_id = tbl_client_properties.cp_id) , ' / ',  tbl_client_properties.commissionReleased) as 'paymentsFor'
                from
                    tbl_client_properties, tbl_propertylist, tbl_propertyparent, tbl_commissions
                WHERE
                    tbl_client_properties.cp_id = tbl_commissions.cp_id
                and 
                    tbl_client_properties.property_id = tbl_propertylist.property_id
                and 
                    tbl_propertylist.propertyParentID = tbl_propertyparent.id
                and 
                    (select count(com.id) from commisionrelease com where com.agentID = tbl_commissions.agentID and com.cp_id = tbl_client_properties.cp_id ) < tbl_client_properties.commissionReleased
                and
                    tbl_commissions.agentID = :id
                and
                    tbl_client_properties.active = 1

            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'id' => $id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function propCount($agentID){
        try {
            $sql="
                SELECT
                    COUNT(tbl_commissions.cp_id) as 'prop_no'
                FROM    
                    tbl_commissions,
                    tbl_client_properties

                WHERE 

                    (select count(com.id) from commisionrelease com where com.agentID = tbl_commissions.agentID and com.cp_id = tbl_client_properties.cp_id) < tbl_client_properties.commissionReleased
                and 
                    tbl_commissions.cp_id  = tbl_client_properties.cp_id
                and
                    tbl_commissions.agentID = :id
                and 
                    tbl_client_properties.active = 1
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'id' => $agentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }


    public function clientCount($agentID){
        try {
            $sql="

                SELECT
                    count(DISTINCT(tbl_client_properties.client_id)) as 'client_no'
                from 
                    tbl_client_properties, tbl_commissions
                WHERE   
                    tbl_client_properties.cp_id = tbl_commissions.cp_id
                and
                    (select count(com.id) from commisionrelease com where com.agentID = tbl_commissions.agentID and com.cp_id = tbl_client_properties.cp_id) < tbl_client_properties.commissionReleased
                AND 
                    tbl_commissions.agentID = :id
                and 
                    tbl_client_properties.active = 1
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'id' => $agentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function removeThisAgent($id){

        try {
            $sql = "
                UPDATE
                    tbl_agent
                SET 
                    active               = 0
                WHERE
                    agent_id            = :id

            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute([
                    'id'                    => $id
                ]);


        } catch (PDOException $e) {
            return $e;
        }

    }





    public function insertThisAgent($agentFname,$agentLname,$agentMname,$agentAddress,$agentContactNumber,$agentEmail,$userID,$agentPosition,$salesDirectorID,$salesManagerID,$unitManagerID,$managerID, $accountNumber){

        try {
            $sql = "
                INSERT INTO
                    tbl_agent(
                        Fname,
                        Lname,
                        Mname,
                        address,
                        contactNumber,
                        email,
                        addedBy,
                        agentPosition,
                        salesDirectorID,
                        salesManagerID,
                        unitManagerID,
                        managerID,
                        accountNumber
                    )
                VALUES(
                    :agentFname,
                    :agentLname,
                    :agentMname,
                    :agentAddress,
                    :agentContactNumber,
                    :agentEmail,
                    :userID,
                    :agentPosition,
                    :salesDirectorID,
                    :salesManagerID,
                    :unitManagerID,
                    :managerID,
                    :accountNumber
                )
            ";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
                    'agentFname'            => $agentFname,
                    'agentLname'            => $agentLname,
                    'agentMname'            => $agentMname,
                    'agentAddress'          => $agentAddress,
                    'agentContactNumber'    => $agentContactNumber,
                    'agentEmail'            => $agentEmail,
                    'userID'                => $userID,
                    'agentPosition'         => $agentPosition,
                    'salesDirectorID'       => $salesDirectorID,
                    'salesManagerID'        => $salesManagerID,
                    'unitManagerID'         => $unitManagerID,
                    'managerID'             => $managerID,
                    'accountNumber'         => $accountNumber
                ]);


        } catch (PDOException $e) {
            return $e;
        }

    }

    public function getRelNumber(){
        try {
            $sql = "SELECT FLOOR(RAND() * 99999) AS releaseNO FROM commisionrelease WHERE 'releaseNO' NOT IN (SELECT releaseNO FROM commisionrelease) LIMIT 1 ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function isRelnumberused($ornumber){
        try {
            $sql = "
                SELECT 
                    releaseNO
                FROM 
                    commisionrelease 
                WHERE 
                    releaseNO = :ornumber
            ";
 
              $statement = $this->db->prepare($sql);

              $statement->execute(['ornumber' => $ornumber]);
              $statement->setFetchMode(PDO::FETCH_ASSOC);
     
              return $statement->fetchAll();

            } catch (PDOException $e) {
                return $e;
            }
    }

    public function updateAgentImagePath($fileName,$agentID){

        try {
            $sql = "
                UPDATE
                    tbl_agent
                SET 
                    imageName         = :image_file
                WHERE
                    agent_id            = :agentID

            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute([
                    'image_file'            => $fileName,
                    'agentID'             => $agentID
                ]);


        } catch (PDOException $e) {
            return $e;
        }

    }

    public function getAgentInfoForThis($id){
        try {
            $sql = "
                SELECT 
                    tbl_agent.agent_id as agentID,
                    tbl_agent.Fname as update_agentFname,
                    tbl_agent.Mname as  update_agentMname,
                    tbl_agent.Lname as update_agentLname,
                    tbl_agent.address as update_agentAddress,
                    tbl_agent.email as update_agentEmail,
                    tbl_agent.agentPosition as agentPositionList,
                    (IF(tbl_agent.agentPosition = 1,'Sales Director Head',IF(tbl_agent.agentPosition = 2,'Sales Director',IF(tbl_agent.agentPosition = 3,'Unit Manager', IF(tbl_agent.agentPosition = 4,'Manager','Agent'))))) as agentPositionName,
                    CONCAT(agentDirectorManager.Fname,' ',agentDirectorManager.Lname) as salesDirectorAgentName,
                    CONCAT(agentSalesManager.Fname,' ',agentSalesManager.Lname) as salesManagerAgentName,
                    CONCAT(agentUnitManager.Fname,' ',agentUnitManager.Lname) as unitManagerAgentName,
                    CONCAT(agentManager.Fname,' ',agentManager.Lname) as managerName,
                    tbl_agent.contactNumber as update_agentContactNumber,
                    tbl_agent.accountNumber as 'update_agentAccountNumber',
                    tbl_agent.imageName as 'update_agentImageName'
                FROM
                    tbl_agent
                LEFT JOIN tbl_agent as agentDirectorManager on agentDirectorManager.agent_id = tbl_agent.salesDirectorID
                LEFT JOIN tbl_agent as agentSalesManager on agentSalesManager.agent_id = tbl_agent.salesManagerID
                LEFT JOIN tbl_agent as agentUnitManager on agentUnitManager.agent_id = tbl_agent.unitManagerID
                LEFT JOIN tbl_agent as agentManager on agentManager.agent_id = tbl_agent.managerID
                WHERE
                    tbl_agent.active = 1
                AND    
                    tbl_agent.agent_id = :id
            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'id' => $id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getAllAgentsList(){
        try {
            $sql = "
                SELECT 
                    tbl_agent.agent_id as agentID,
                    CONCAT(tbl_agent.Fname,' ',tbl_agent.Mname,' ',tbl_agent.Lname) as agentName,
                    tbl_agent.address as agentAddress,
                    tbl_agent.email as agentEmail,
                    tbl_agent.contactNumber as update_agentContactNumber
                FROM
                    tbl_agent
                WHERE
                    tbl_agent.active = 1
            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }







    public function getAllAgentsHigherThanThisPos($param,$id){
        try {
            $sql = "
                SELECT 
                    tbl_agent.agent_id as agentID,
                    CONCAT(tbl_agent.Fname,' ',tbl_agent.Mname,' ',tbl_agent.Lname) as agentName
                FROM
                    tbl_agent
                WHERE
                    tbl_agent.active = 1
                AND
                    ".$param." = :".$param."

            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute([
                $param  => $id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }




    public function getAgentUplineAgentID($typeOfUplinePositionAttribute,$agentid){
        try {
            $sql = "
                SELECT 
                    ".$typeOfUplinePositionAttribute."
                FROM
                    tbl_agent
                WHERE
                    tbl_agent.active = 1
                AND
                    agent_id = :agentid

            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute([
                'agentid'  => $agentid
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function updateThisAgentInfo($agentFname,$agentLname,$agentMname,$agentAddress,$agentContactNumber,$agentEmail,$pos,$salesDirectorID,$salesManagerID,$unitManagerID, $managerID ,$accountNumber, $agentID){
         try {
            $sql = "
                UPDATE
                    tbl_agent
                SET 
                    Fname               = :agentFname,
                    Lname               = :agentLname,
                    Mname               = :agentMname,
                    address             = :agentAddress,
                    contactNumber       = :agentContactNumber,
                    email               = :agentEmail,
                    agentPosition       = :agentPosition,
                    salesDirectorID     = :salesDirectorID,
                    salesManagerID      = :salesManagerID,
                    unitManagerID       = :unitManagerID,
                    managerID           = :managerID,
                    accountNumber       = :accountNumber
                WHERE
                    agent_id            = :id

            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute([
                    'agentPosition'         => $pos,
                    'salesDirectorID'       => $salesDirectorID,
                    'salesManagerID'        => $salesManagerID,
                    'unitManagerID'         => $unitManagerID,
                    'managerID'             => $managerID,
                    'agentFname'            => $agentFname,
                    'agentLname'            => $agentLname,
                    'agentMname'            => $agentMname,
                    'agentAddress'          => $agentAddress,
                    'agentContactNumber'    => $agentContactNumber,
                    'agentEmail'            => $agentEmail,
                    'id'                    => $agentID,
                    'accountNumber'         => $accountNumber
                ]);


        } catch (PDOException $e) {
            return $e;
        }
    }




    public function getAgentPos($otherAttributeToRetrive,$id){
        try {
            $sql = "
                SELECT 
                    agentPosition
                    ".$otherAttributeToRetrive."
                FROM
                    tbl_agent
                WHERE
                    tbl_agent.active = 1
                AND
                    agent_id = :id

            ";
            $statement = $this->db->prepare($sql);

            $statement->execute([
                'id'  => $id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAllAgentsNameList(){
        try {
            $sql="
            SELECT DISTINCT 
            a.agent_id AS 'agentID',
            CONCAT(a.Fname , ' ', a.Mname, ' ', a.Lname) AS 'agentName'
        FROM
            tbl_agent a
            INNER JOIN `tbl_commissions` b
             ON a.`agent_id`= b.agentID
        WHERE 
            a.active = 1
            AND b.`active` = 1 
            AND b.comReleaseNo = 0
            ORDER BY a.`agent_id` DESC 
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    } 


    public function getAllAgentPropertyID($agentID){
        try {
            $sql="
                SELECT
                    tbl_commissions.cp_id
                from 
                    tbl_commissions
                WHERE   
                    tbl_commissions.active = 1
                and 
                    tbl_commissions.agentID = :agentID

            ";
            $statement = $this->db->prepare($sql);
            $statement->execute([
                'agentID' => $agentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getAllAgents($role, $userID){
        try {

            if ($role == 'Admin') {
                $sql = "
                    SELECT 
                        CONCAT(tbl_agent.Fname,' ',tbl_agent.Mname,' ',tbl_agent.Lname) as '0',
                        tbl_agent.address as '1',
                        (IF(tbl_agent.agentPosition = 1,'Sales Director Head',IF(tbl_agent.agentPosition = 2,'Sales Director',IF(tbl_agent.agentPosition = 3,'Unit Manager', IF(tbl_agent.agentPosition = 4,'Manager', 'Agent'))))) as '2',
                        tbl_agent.email as '3',
                        tbl_agent.contactNumber as '4',
                        CONCAT('<button type=\"button\" class=\"btn btn-outline green btn_updateThisAgent\" data-id=\"',tbl_agent.agent_id,'\" ><i class=\" fa fa-search\" ></i> </button><button type=\"button\" class=\"btn btn-outline red btn_removeThisAgent\" data-id=\"',tbl_agent.agent_id,'\" ><i class=\"icon-close\" ></i></button>') as '5' 
                    FROM
                        tbl_agent
                    WHERE
                        tbl_agent.active = 1
                ";   
            }else if ($role == 'Agent'){
                 $sql = "
                    SELECT 
                        CONCAT(tbl_agent.Fname,' ',tbl_agent.Mname,' ',tbl_agent.Lname) as '0',
                        tbl_agent.address as '1',
                        (IF(tbl_agent.agentPosition = 1,'Sales Director Head',IF(tbl_agent.agentPosition = 2,'Sales Director',IF(tbl_agent.agentPosition = 3,'Unit Manager', IF(tbl_agent.agentPosition = 4,'Manager', 'Agent'))))) as '2',
                        tbl_agent.email as '3',
                        tbl_agent.contactNumber as '4',
                        CONCAT('<button type=\"button\" class=\"btn btn-outline green btn_updateThisAgent\" data-id=\"',tbl_agent.agent_id,'\" ><i class=\" fa fa-search\" ></i> </button>') as '5' 
                    FROM
                        tbl_agent
                    WHERE
                        tbl_agent.active = 1
                    and 
                        agent_id = ". $userID  .";
                ";   
            }else{
                $sql = "
                    SELECT 
                        CONCAT(tbl_agent.Fname,' ',tbl_agent.Mname,' ',tbl_agent.Lname) as '0',
                        tbl_agent.address as '1',
                        (IF(tbl_agent.agentPosition = 1,'Sales Director Head',IF(tbl_agent.agentPosition = 2,'Sales Director',IF(tbl_agent.agentPosition = 3,'Unit Manager', IF(tbl_agent.agentPosition = 4,'Manager', 'Agent'))))) as '2',
                        tbl_agent.email as '3',
                        tbl_agent.contactNumber as '4',
                        CONCAT('<button type=\"button\" class=\"btn btn-outline green btn_updateThisAgent\" data-id=\"',tbl_agent.agent_id,'\" ><i class=\"fa fa-search\"  ></i>  </button>') as '5' 
                    FROM
                        tbl_agent
                    WHERE
                        tbl_agent.active = 1
                ";
            }
 
            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAgentInfoUnderThisPropery($parentID)
    {
        try {
            $sql="
                SELECT


            ";
            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'id' => $parentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }

     public function checkAgentPosition($id,$pos)
    {
        try {
            $sql="

                SELECT
                    agentPosition
                from 
                    tbl_agent
                where
                    agentPosition =:posID
                AND
                    active = 1
                and
                    agent_id = :agentId

            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'posID' => $pos,
                'agentId' => $id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll(); 

        } catch (PDOException $e) {
            return $e;
        }
    }

     // update agentposition without changes in agent position
    public function updatethisAgentInfoWithOuthPosChange($agentFname,$agentLname,$agentMname,$agentAddress,$agentContactNumber,$agentEmail,$accountNumber, $agentID)
    {
            try {
                $sql="  
                    UPDATE
                        tbl_agent
                    SET 
                        Fname               = :agentFname,
                        Lname               = :agentLname,
                        Mname               = :agentMname,
                        address             = :agentAddress,
                        contactNumber       = :agentContactNumber,
                        email               = :agentEmail,
                        accountNumber       = :accountNumber
                    WHERE
                        agent_id            = :id

                ";
                $statement = $this->db->prepare($sql);
                return $statement->execute([
                    'agentFname'            => $agentFname,
                    'agentLname'            => $agentLname,
                    'agentMname'            => $agentMname,
                    'agentAddress'          => $agentAddress,
                    'agentContactNumber'    => $agentContactNumber,
                    'agentEmail'            => $agentEmail,
                    'id'                    => $agentID,
                    'accountNumber'         => $accountNumber
                ]);

            } catch (PDOException $e) {
                return $e;
            }
    }

    public function getListProperties($parentID)
    {
        try {

            $sql="
               SELECT
                    tbl_commissions.cp_id, tbl_Commissions.agentID, tbl_commissions.percentagevalue
                FROM
                    tbl_commissions, tbl_client_properties, tbl_propertylist
                where
                    tbl_commissions.cp_id  = tbl_client_properties.cp_id
                AND
                    tbl_client_properties.property_id = tbl_propertylist.property_id
                and 
                    tbl_propertylist.propertyParentID = 1
                and 
                    tbl_commissions.comReleaseNo = 0
            ";

            $statement = $this->db->prepare($sql);
           $statement->execute([
                'agentID' => $agentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll(); 

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAgentComDetails($cp_id, $agentId, $percentageValue)
    {
        try {
            $sql="
                SELECT
                    concat(tbl_agent.fname, ' ', tbl_agent.mname, ' ', tbl_agent.lname) as 'agentName',
                    tbl_propertyparent.propertyName as 'propertyName',
                    tbl_propertylist.phaseNumber as 'phaseNumber',
                    tbl_propertylist.block as 'blocl',
                    tbl_propertylist.lot as 'lot',
                    IF(tbl_agent.agentPosition = 1,'Sales Director Head', IF(tbl_agent.agentPosition = 2,'Sales Director', IF(tbl_agent.agentPosition = 3,'Unit Manager', IF(tbl_agent.agentPosition = 4,'Manager', 'Agent') )) ) as 'agentPos',
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'amount'
                from
                    tbl_agent, tbl_propertylist, tbl_propertyparent, tbl_commissions, tbl_client_properties
                WHERE
                    tbl_commissions.agentID = tbl_agent.agent_id
                AND
                    tbl_commissions.cp_id = tbl_client_properties.cp_id
                AND
                    tbl_client_properties.property_id = tbl_propertylist.property_id
                and
                    tbl_propertylist.propertyParentID = tbl_propertyparent.id 
                and 
                    tbl_commissions.cp_id = :cp_id
                and 
                    tbl_commissions.agentID = :agentId
            ";
            $statement = $this->db->prepare($sql);
            $statement->execute([
                'cp_id'             => $cp_id,
                'agentId'           => $agentId
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll(); 
        } catch (Exception $e) {
            
        }
    }

    public function SampleDaw($parentID)
    {
       try {
            $sql="
                SELECT
                    tbl_commissions.cp_id, tbl_commissions.agentID, tbl_commissions.percentagevalue, tbl_client_properties.client_id
                FROM
                    tbl_commissions, tbl_client_properties, tbl_propertylist
                where
                    tbl_commissions.cp_id  = tbl_client_properties.cp_id
                AND
                    tbl_client_properties.property_id = tbl_propertylist.property_id
                and 
                    tbl_propertylist.propertyParentID = :agentID
                and 
                    tbl_commissions.comReleaseNo = '0'
            ";

             $statement = $this->db->prepare($sql);
             $statement->execute([
                'agentID' => $parentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll(); 
            
       } catch (PDOException $e) {
           return $e;
       }
    }

    public function getOriginalPropertyPrice($clientPropertyID)
    {
        try {
            $sql="
                SELECT
                    tbl_propertylist.originalPrice
                from
                    tbl_client_properties, tbl_propertylist
                WHERE
                    tbl_propertylist.property_id = tbl_client_properties.property_id
                and 
                    tbl_client_properties.cp_id = :cp_id
            ";

            $statement =$this->db->prepare($sql);
            $statement->execute([
                    'cp_id' => $clientPropertyID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAgentComRelSum2()
    {
        try {
            $sql="
                SELECT
                    DISTINCT
                    tbl_agent.agent_id as 'id',
                    tbl_agent.accountNumber as 'AccountNumber',
                    concat(tbl_agent.fname, ' ', tbl_agent.mname, ' ', tbl_agent.lname) as 'AgentName'
                from
                    tbl_agent
                inner join 
                    tbl_commissions
                on
                    tbl_agent.agent_id = tbl_commissions.agentID
                where 
                     (select count(com.id) from commisionrelease com where com.agentID = tbl_agent.agent_id and com.cp_id = tbl_commissions.cp_id) < (select count(id) from tbl_clientpaymenthistory where cp_id = tbl_commissions.cp_id)
            ";

            $statement =$this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getAgentComRelSum()
    {
        try {
            $sql="
                SELECT
                    DISTINCT
                    tbl_agent.agent_id as 'id',
                    tbl_agent.accountNumber as 'AccountNumber',
                    concat(tbl_agent.fname, ' ', tbl_agent.mname, ' ', tbl_agent.lname) as 'AgentName',
                    tbl_commissions.cp_id,
                    tbl_commissions.percentagevalue
                from
                    tbl_agent
                inner join 
                    tbl_commissions
                on
                    tbl_agent.agent_id = tbl_commissions.agentID
                where 
                    (select count(com.id) from commisionrelease com where com.agentID = tbl_agent.agent_id and com.cp_id = tbl_commissions.cp_id) < (select count(id) from tbl_clientpaymenthistory where cp_id = tbl_commissions.cp_id)
            ";

            $statement =$this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getComlistFromAgent($AgentId)
    {
        try {
            $sql="
                SELECT  
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'amount' 
                from 
                    tbl_propertylist, tbl_propertyparent, tbl_client_properties, tbl_commissions 
                WHERE 
                    tbl_commissions.cp_id = tbl_client_properties.cp_id 
                and 
                    tbl_client_properties.property_id = tbl_propertylist.property_id 
                AND 
                    tbl_propertylist.propertyParentID = tbl_propertyparent.id 
                and 
                    tbl_client_properties.active = 1 
                AND 
                    tbl_commissions.agentID = :id
           ";

            $statement =$this->db->prepare($sql);
            $statement->execute([
                    'id' => $AgentId
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAgent1($cpid){
        try {
            $sql="
               SELECT
                    concat(tbl_agent.fname, ' ', tbl_agent.lname) as 'agentName',
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Agent 1',
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Com',
                    tbl_client_properties.agent_id, 
                    tbl_agent.agentPosition,
                    concat(tbl_commissions.percentagevalue,'% ') as 'percentageVal' 
                from 
                    tbl_propertylist, 
                    tbl_agent, 
                    tbl_commissions, 
                    tbl_client_properties 
                where 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_agent.agent_id = tbl_commissions.agentID 
                and 
                    tbl_agent.agentPosition = 2 
                and 
                    tbl_client_properties.cp_id = tbl_commissions.cp_id
                and 
                    tbl_client_properties.cp_id = :id
                and 
                    tbl_commissions.comReleaseNo = 0
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id'  => $cpid
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }

    }

    public function getAgent2($cpid){
        try {
            $sql="
                SELECT
                    concat(tbl_agent.fname, ' ', tbl_agent.lname) as 'agentName', 
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Agent 2',
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Com',
                    tbl_client_properties.agent_id, 
                    tbl_agent.agentPosition,
                    concat(tbl_commissions.percentagevalue,'% ') as 'percentageVal' 
                from 
                    tbl_propertylist, 
                    tbl_agent, 
                    tbl_commissions, 
                    tbl_client_properties 
                where 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_agent.agent_id = tbl_commissions.agentID 
                and 
                    tbl_agent.agentPosition = 3 
                and 
                    tbl_client_properties.cp_id = tbl_commissions.cp_id
                and 
                    tbl_client_properties.cp_id = :id
                and 
                    tbl_commissions.comReleaseNo = 0
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id'  => $cpid
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAgent3($cpid){
        try {
            $sql="
                SELECT
                    concat(tbl_agent.fname, ' ', tbl_agent.lname) as 'agentName', 
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Agent 3',
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Com',
                    tbl_client_properties.agent_id, 
                    tbl_agent.agentPosition,
                    concat(tbl_commissions.percentagevalue,'% ') as 'percentageVal'
                from 
                    tbl_propertylist, 
                    tbl_agent, 
                    tbl_commissions, 
                    tbl_client_properties 
                where 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_agent.agent_id = tbl_commissions.agentID 
                and 
                    tbl_agent.agentPosition = 4 
                and 
                    tbl_client_properties.cp_id = tbl_commissions.cp_id
                and 
                    tbl_client_properties.cp_id = :id
                and 
                    tbl_commissions.comReleaseNo = 0

            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id'  => $cpid
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }

    }

    public function getAgent4($cpid){
        try {
            $sql="
                SELECT
                    concat(tbl_agent.fname, ' ', tbl_agent.lname) as 'agentName', 
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Agent 4',
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Com',
                    tbl_client_properties.agent_id, 
                    tbl_agent.agentPosition,
                    concat(tbl_commissions.percentagevalue,'% ') as 'percentageVal'
                from 
                    tbl_propertylist, 
                    tbl_agent, 
                    tbl_commissions, 
                    tbl_client_properties 
                where 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_agent.agent_id = tbl_commissions.agentID 
                and 
                    tbl_agent.agentPosition = 5 
                and 
                    tbl_client_properties.cp_id = tbl_commissions.cp_id
                and 
                    tbl_client_properties.cp_id = :id
                and 
                    tbl_commissions.comReleaseNo = 0

            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id'  => $cpid
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }

    }

    public function getSD($cpid){
        try {
            $sql="
                 SELECT 
                    concat(tbl_agent.fname, ' ', tbl_agent.lname) as 'agentName',
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'SD',
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Com',
                    tbl_client_properties.agent_id, 
                    tbl_agent.agentPosition,
                    concat(tbl_commissions.percentagevalue,'% ') as 'percentageVal'

                from 
                    tbl_propertylist, 
                    tbl_agent, 
                    tbl_commissions, 
                    tbl_client_properties 
                where 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_agent.agent_id = tbl_commissions.agentID 
                and 
                    tbl_agent.agentPosition = 1 
                and 
                    tbl_client_properties.cp_id = tbl_commissions.cp_id
                and 
                    tbl_client_properties.cp_id = :id
                and 
                    tbl_commissions.comReleaseNo = 0
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id'  => $cpid
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }

    }

    public function countThisAgentDownline($agentId, $possition)
    {
        try {
            if ($possition == 1) {
                $sql = 'SELECT * from tbl_agent where salesDirectorID = '.$agentId.'  and agent_id != '.$agentId;
            }else if ($possition == 2) {
                $sql = 'SELECT * from tbl_agent where salesManagerID = '.$agentId.'  and agent_id != '.$agentId;
            }else{
                $sql = 'SELECT * from tbl_agent where unitManagerID = '.$agentId.'  and agent_id != '.$agentId;
            }
        
            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getAgentList($propertyID)
    {
        try {
            $sql="
                SELECT
                    agentid, 
                    percentagevalue
                from    
                    tbl_commissions
                where
                    cp_id = :cp_id
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'cp_id' => $propertyID
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAgentName($agentID)
    {
       try {
           $sql="
                SELECT
                    concat(fname , ' ', lname) as name,
                    IF(agentPosition = 1, 'SDH', IF(agentPosition = 2, 'SD', IF(agentPosition = 3, 'UM', IF(agentPosition = 4, 'M', 'A')))) as 'pos'
                FROM
                    tbl_agent
                WHERE
                    agent_id =:agentID
           ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'agentID' => $agentID
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
       } catch (PDOException $e) {
           return $e;
       }
    }

    public function updateThisAgentCom($propertyID, $agentCom, $agentID)
    {
        try {
            $sql="
                UPDATE
                    tbl_commissions
                SET
                    percentagevalue = :com
                WHERE
                    agentid =:agentID
                and
                    cp_id = :cp_id
           ";

            $statement = $this->db->prepare($sql);
            return  $statement->execute([
                'cp_id'     => $propertyID,
                'com'       => $agentCom,
                'agentID'   => $agentID
            ]);
        } catch (PDOException $e) {
            return $e;
        }
    }

    // public function getClientPaymenthistory($value='')
    // {
    //     try {   
    //         sql="

    //             select  
    //                 id, 
    //                 cp_id,
    //             from
    //                 tbl_clientpaymenthistory
    //             where
    //                 releseStat = 0
    //             and 
    //                 active = 1 
    //         ";

    //        } catch (PDOException $e) {
    //            return $e;
    //        }   
    // }

    public function checkAgent($agentID)
    {
        try {

            $sql = "select if(count(UserID)=0, 'false', 'true') as 'user' from tbl_users where refID = :agentID";
            $statement = $this->db->prepare($sql);
            $statement->execute([
                'agentID'  => $agentID,
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function addNewUserAgent($agentId, $fname, $lname, $mname, $addre, $email, $contact, $pos, $username)
    {
        try {

            $sql ="INSERT into 
                        tbl_users(Fname, Lname, Mname, Address, Email, ContactNumber, img_filename, Position, Username, Password, refID)
                            value(:fname, :lname, :mname, :addres, :email, :contact, 'no_image.jpg', :pos, :username, 'none', :agentID)";
            
            $statement = $this->db->prepare($sql);
            return $statement->execute([
                'fname'     => $fname,
                'lname'     => $lname,
                'mname'     => $mname,
                'addres'    => $addre,
                'email'     => $email,
                'contact'   => $contact,
                'pos'       => $pos,
                'username'  => $username,
                'agentID'   => $agentId
            ]);
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function updateAgentUserName($userID, $username)
    {
        try {

            $sql="update tbl_users set Username =:username where refID =:agentID";
            $statement = $this->db->prepare($sql);
            return $statement->execute([
                'agentID'   => $userID,
                'username'  => $username
            ]);
            
        } catch (PDOException $e) {
            return $e;
        }
    } 

    public function updateAgentPassword($userID, $pass)
    {
        try {

            $sql="update tbl_users set Password =:password where refID =:agentID";
            $statement = $this->db->prepare($sql);
            return $statement->execute([
                'agentID'     => $userID,
                'password'    => $pass
            ]);
            
        } catch (PDOException $e) {
            return $e;
        }
    }

     public function addNewUserAgentPassword($agentId, $fname, $lname, $mname, $addre, $email, $contact, $pos, $password)
    {
        try {

            $sql ="INSERT into 
                        tbl_users(Fname, Lname, Mname, Address, Email, ContactNumber, img_filename, Position, Username, Password, refID)
                            value(:fname, :lname, :mname, :addres, :email, :contact, 'no_image.jpg', :pos, 'none', :password, :agentID)";
            
            $statement = $this->db->prepare($sql);
            return $statement->execute([
                'fname'     => $fname,
                'lname'     => $lname,
                'mname'     => $mname,
                'addres'    => $addre,
                'email'     => $email,
                'contact'   => $contact,
                'pos'       => $pos,
                'password'  => $password,
                'agentID'   => $agentId
            ]);
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getSpecificAgent($userID)
    {
        try {
             
            $sql="
                SELECT 
                    tbl_agent.agent_id as 'agentID',
                    concat(tbl_agent.Fname , ' ', tbl_agent.Mname, ' ', tbl_agent.Lname) as 'agentName'
                FROM
                    tbl_agent
                where 
                    active = 1
                and
                    agent_id = (select refID from tbl_users where userID = :userID)
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'userID' => $userID
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

            
        } catch (PDOException $e) {
            return $e;
        }
    }


    public function checkAgentUsername($userID)
    {
        try {

            $sql = "select username from tbl_users where refID = :userID";
            $statement = $this->db->prepare($sql);
            $statement->execute([
                'userID' => $userID
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

}