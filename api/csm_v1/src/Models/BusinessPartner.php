<?php

namespace App\Models;

use PDO;

class BusinessPartner {

    /**
    * @var $db
    */
    private $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }
    

    public function newBusinessPartner($d){

        try {
            $sql = "
				insert into tbl_businesspartner(fname, lname, mname, address, contact, email, property) 
				values(:fname, :lname, :mname, :address, :contact, :email, :property)";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
            	'fname'    => $d[0]['fname'],
            	'lname'    => $d[0]['lname'],
            	'mname'    => $d[0]['mname'],
            	'address'  => $d[0]['address'],
            	'email'    => $d[0]['email'],
            	'contact'  => $d[0]['contact'],
            	'property' => $d[0]['proplist']
            ]);

        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getPartnerList()
    {
    	try {

    		$sql = "select concat(fname, ' ', mname,  ' ', lname ) as '0', address as '1', email as '2', contact as '3', 
					CONCAT('<button type=\"button\" class=\"btn btn-outline green btn_partnerDetails\" data-id=\"',tbl_businesspartner.id,'\" ><i class=\" fa fa-search\" ></i> </button> <button type=\"button\" class=\"btn btn-outline red btn_removePartner\" data-id=\"',tbl_businesspartner.id,'\" ><i class=\"icon-close\" ></i></button>') as '4' from tbl_businesspartner where active = 1";

    		$statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
    	} catch (PDOException $e) {
    		return $e;
    	}
    }

    public function removePartnerID($ID)
    {
    	try {

    		$sql = "update tbl_businesspartner set active = 0 where id = :id";

    		$statement = $this->db->prepare($sql);
            return  $statement->execute(['id' => $ID]);
    		
    	} catch (PDOException $e) {
    		return $e;
    	}
    }

    public function getPartnerProfile($partnerID)
    {
        try {

            $sql = "select concat(fname, ' ', lname) as 'name', fname, lname, mname, address, contact, email, property from tbl_businesspartner where id = :id";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id' => $partnerID 
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function updateBusinessPartnerProfileDetails($id, $details)
    {
        try {

            $sql = "update tbl_businesspartner 
                        set 
                            fname =:fname, 
                            lname = :lname, 
                            mname =:mname, 
                            address = :address, 
                            email =:email, 
                            contact =:contact 
                    where 
                        id =:id";

            $statement = $this->db->prepare($sql);
            return  $statement->execute([
                    'id'        => $id,
                    'fname'     => $details['fname'],
                    'lname'     => $details['lname'],
                    'mname'     => $details['mname'],
                    'address'   => $details['address'],
                    'contact'   => $details['contact'],
                    'email'     => $details['email']
                ]);
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function addUsername($id, $username, $profile)
    {
        try {
            
            $sql = "insert into tbl_users(Fname, Lname, Mname, Address, ContactNumber, Email, username, password, position, img_filename, refID) 
                        values(:fname, :lname, :mname, :address, :contact, :email, :username, 'none', 'Business Partner', 'no_image.jpg', :id)";

            $statement = $this->db->prepare($sql);
            return  $statement->execute([
                    'id'        => $id,
                    'fname'     => $profile[0]['fname'],
                    'lname'     => $profile[0]['lname'],
                    'mname'     => $profile[0]['mname'],
                    'address'   => $profile[0]['address'],
                    'contact'   => $profile[0]['contact'],
                    'email'     => $profile[0]['email'],
                    'username'  => $username
                ]);

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function checkUsername($id, $username)
    {
        try {

            $sql = "select count(userID) as 'username' from tbl_users where refID =:id and position ='Business Partner'";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id' => $id 
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function updateAccountUsername($id, $username)
    {
        try {
            // $sql = "";

            // $statement = $this->db->prepare($sql);
            // $statement->execute([
            //     'id' => $id 
            // ]);
            // s$statement->setFetchMode(PDO::FETCH_ASSOC);
            // return $statement->fetchAll();

            return false;

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function checkexistingUsername($username)
    {
        try {

            $sql = "select count(userID) as 'count' from tbl_users where username =:username";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'username' => $username 
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getUsername($id)
    {
        try {

            $sql="select username from tbl_users where refID = :id and active = 1";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id' => $id
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getPassword($id)
    {
        try {

            $sql= "select password from tbl_users where refID= :id";

            $statement =  $this->db->prepare($sql);
            $statement->execute([
                'id' => $id
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function addPassword($id, $password)
    {
        try {

            $sql = "update tbl_users set password = :password where refID = :id";

            $statement = $this->db->prepare($sql);
            return  $statement->execute([
                'id' => $id,
                'password' => $password
            ]);
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function updatePropertyById($id, $propList)
    {
        try {

            $sql = "update tbl_businesspartner set property =:property where id=:id";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
                'property' => $propList,
                'id' => $id
            ]);
            
        } catch (PDOException $e) {
            return $e;
        }
    }

}