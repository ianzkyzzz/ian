<?php

namespace App\Models;

use PDO;

class Client {

    /**
    * @var $db
    */
    private $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }



    public function updateClientImagePath($fileName,$clientID){

        try {
            $sql = "
                UPDATE
                    tbl_client
                SET 
                    image_file               = :image_file
                WHERE
                    client_id            = :client_id

            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute([
                    'image_file'            => $fileName,
                    'client_id'             => $clientID
                ]);


        } catch (PDOException $e) {
            return $e;
        }

    }

    
    public function updateThisClient($Fname,$Lname,$Mname,$Address,$ContactNumber,$Email,$clientID,$Remarks){

        try {
            $sql = "
                UPDATE
                    tbl_client
                SET 
                    Fname               = :Fname,
                    Lname               = :Lname,
                    Mname               = :Mname,
                    Address             = :Address,
                    ContactNumber       = :ContactNumber,
                    Email               = :Email,
                    Remarks             = :Remarks
                WHERE
                    client_id            = :client_id
            ";
            
            $statement = $this->db->prepare($sql);

            return $statement->execute([
                    'Fname'            => $Fname,
                    'Lname'            => $Lname,
                    'Mname'            => $Mname,
                    'Address'          => $Address,
                    'ContactNumber'    => $ContactNumber,
                    'Email'            => $Email,
                    'client_id'        => $clientID,
                    'Remarks'          => $Remarks
                ]);


        } catch (PDOException $e) {
            return $e;
        }

    }

    public function insertThisClientWithProperties(
                $addClient_fname,
                $addClient_lname,
                $addClient_mname,
                $addClient_address,
                $addClient_contactnumber,
                $addClient_email,
                
                $chargesDetails,
                $propertySelected,
                $addClientagent,
                $commissionPercentage,
                $addedByUserID, 
                $spouse,
                $benif
            ){

        $clientID = 0;

        try {
            $this->db->setAttribute(PDO::ATTR_AUTOCOMMIT,false);
            $this->db->beginTransaction();

            //                                       insert new client entry
            $sql = "
                INSERT INTO
                    tbl_client(
                        Fname,
                        Lname,
                        Mname,
                        Address,
                        ContactNumber,
                        Email,
                        addedBy,
                        spouse,
                        benificiary
                    )
                VALUES(
                    :addClient_fname,
                    :addClient_lname,
                    :addClient_mname,
                    :addClient_address,
                    :addClient_contactnumber,
                    :addClient_email,
                    :addedByUserID,
                    :spouse,
                    :benificiary
                )
            ";

            $statement = $this->db->prepare($sql);
            $result =  $statement->execute([
                    'addClient_fname'               => $addClient_fname,
                    'addClient_lname'               => $addClient_lname,
                    'addClient_mname'               => $addClient_mname,
                    'addClient_address'             => $addClient_address,
                    'addClient_contactnumber'       => $addClient_contactnumber,
                    'addClient_email'               => $addClient_email,
                    'addedByUserID'                 => $addedByUserID,
                    'spouse'                        => json_encode($spouse),
                    'benificiary'                   => json_encode($benif)
                ]);


            if(!$result){
                $this->message = 'Failed to add message.';
                $this->db->rollBack();
                return false;
            }
            
            $clientID = $this->db->lastInsertId();

            $values = [];
            $param = [];
            $valuesForDownpayment = [];
            $paramForDownpayment = [];
            foreach ($propertySelected as $key => $property) {
                $ornumber                               = trim($property['ornumber']);
                $modeOfpayment                          = trim($property['modeOfpayment']);
                $clientremitanceCentername              = strtoupper($property['clientremitanceCentername']);

                $parentID                               = trim($property['parentID']);
                $phaseNumber                            = trim($property['phaseNumber']);
                $blockNumber                            = trim($property['block']);
                $lotNumber                              = trim($property['lot']);
                $addClientprop_planterms                = trim($property['addClientprop_planterms']);
                $addClientprop_pricePerm2               = trim($property['addClientprop_pricePerm2']);
                $addClientprop_downpayment              = trim($property['addClientprop_downpayment']);
                $addClientDueDate                       = trim($property['addClientDueDate']);
                $addClientprop_dateapplied              = trim($property['addClientprop_dateapplied']);
                $commissionReleased                     = trim($property['comission_release']);
                $addCommission                          = trim($property['add_commission']);

                $property_id = $this->thisPropertyExist($parentID,$blockNumber,$lotNumber,$phaseNumber);
                if(count($property_id) != 1){
                    $this->message = 'Invalid ParentID/blockNumber/lotNumber.';
                    $this->db->rollBack();
                    return false;
                }

                $this->dateNow = $property_id[0]['dateNow'];

                //                            insert new client property entry
                $param = '(
                    :addClientprop_property,
                    :clientID,
                    :chargesDetails,
                    :addClientprop_planterms,
                    :addClientprop_pricePerm2,
                    :addClientprop_downpayment,
                    :addClientDueDate,
                    :addClientprop_dateapplied,
                    :addClientagent,
                    :percentageCommissionJson,
                    :addedByUserID,
                    :commissionReleased,
                    :adCom
                )';

                
                $values = [
                    'addClientprop_property'               => $property_id[0]['property_id'],                           
                    'clientID'                             => $clientID,   
                    'chargesDetails'                       => json_encode($chargesDetails),                       
                    'addClientprop_planterms'              => $addClientprop_planterms,
                    'addClientprop_pricePerm2'             => $addClientprop_pricePerm2,
                    'addClientprop_downpayment'            => $addClientprop_downpayment,
                    'addClientDueDate'                     => $addClientDueDate,
                    'addClientprop_dateapplied'            => $addClientprop_dateapplied,
                    'addClientagent'                       => $addClientagent,  
                    'percentageCommissionJson'             => json_encode($commissionPercentage),               
                    'addedByUserID'                        => $addedByUserID,  
                    'commissionReleased'                   => $commissionReleased,
                    'adCom'                                => $addCommission
                ];

                $sql = "
                    INSERT INTO
                        tbl_client_properties(
                            property_id,
                            client_id,
                            additionalCharges,
                            plan_terms,
                            sqmPricem2,
                            downpayment,
                            duedate,
                            date_applied,
                            agent_id,
                            percentageCommissionJson,
                            addedBy,
                            commissionReleased,
                            adCom
                        )
                    VALUES
                        ".$param."
                ";

                $statement = $this->db->prepare($sql);

                $result =  $statement->execute($values);

                if(!$result){
                    $this->message = 'Failed to add clients property.';
                    $this->db->rollBack();
                    return false;
                }

                $this->lastInsertId = $this->db->lastInsertId();
                //                                      inserting new agent commision entries

                $agent  = $this->getThisAgent($addClientagent);

                $valuesToBeAdded = [];
                if($agent[0]['agentPosition'] == 1){
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$addClientagent.','.$commissionPercentage['directCom'].')';
                }
                else if($agent[0]['agentPosition'] == 2){
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$addClientagent.','.$commissionPercentage['directCom'].')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['salesDirectorID'].','.$commissionPercentage['SalesDirectorCom'].')';
                }
                else if($agent[0]['agentPosition'] == 3){
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$addClientagent.','.$commissionPercentage['directCom'].')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['salesDirectorID'].','.$commissionPercentage['SalesDirectorCom'].')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['salesManagerID'].','.$commissionPercentage['SalesManagerCom'].')';
                }
                else if($agent[0]['agentPosition'] == 4){
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$addClientagent.','.$commissionPercentage['directCom'].')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['salesDirectorID'].','. $commissionPercentage['SalesDirectorCom'].')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['salesManagerID'] .','. $commissionPercentage['SalesManagerCom'] .')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['unitManagerID']  .','. $commissionPercentage['UnitManagerCom']  .')';
                }
                else if($agent[0]['agentPosition'] == 5){
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$addClientagent.','.$commissionPercentage['directCom'].')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['salesDirectorID'].','. $commissionPercentage['SalesDirectorCom'].')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['salesManagerID'] .','. $commissionPercentage['SalesManagerCom'] .')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['unitManagerID']  .','. $commissionPercentage['UnitManagerCom']  .')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['managerID']      .','. $commissionPercentage['ManagerCom'].')';
                }


                $sql = "
                    INSERT INTO
                        tbl_commissions(
                            cp_id,
                            agentID,
                            percentagevalue
                        )
                    VALUES
                        ".implode(',', $valuesToBeAdded) . "
                ";

                $statement = $this->db->prepare($sql);
                $result =  $statement->execute();
                if(!$result){
                    $this->message = 'Failed to add commisions.';
                    $this->db->rollBack();
                    return false;
                }

                if($addClientprop_downpayment != '0'){

                    $debit = 0;
                    $credit = 0;

                    $clientremitanceCenterID = 0;
                    

                    if($modeOfpayment == 2 || $modeOfpayment == 3){
                        $debit = $addClientprop_downpayment;
                        $clientremitanceCentername = strtoupper(trim($clientremitanceCentername));

                        $clientremitanceCenterID = $this->getRemitanceID($clientremitanceCentername);
                    }
                    else{
                        $clientremitanceCentername  ="";
                        $credit = $addClientprop_downpayment;
                    }

                    $param = '(
                        :cp_id'.$key.',
                        :clientID'.$key.',
                        :ornumber'.$key.',
                        :particulars'.$key.',
                        :clientremitanceCentername'.$key.',
                        :debit'.$key.',
                        :credit'.$key.',
                        :agentComission'.$key.',
                        :addedBy'.$key.'
                    )';


                    $values = [
                        'cp_id'.$key.''                         => $this->lastInsertId,
                        'clientID'.$key.''                      => $clientID,
                        'particulars'.$key.''                   => 'Downpayment',
                        'ornumber'.$key.''                      => $ornumber,
                        'clientremitanceCentername'.$key.''     => $clientremitanceCenterID,
                        'debit'.$key.''                         => $debit,
                        'credit'.$key.''                        => $credit,
                        'agentComission'.$key.''                => 0,
                        'addedBy'.$key.''                       => $addedByUserID,
                    ];


                    $sql = "
                        INSERT INTO
                            tbl_clientpaymenthistory(
                                cp_id,
                                client_id,
                                ornumber,
                                particulars,
                                remittanceCenter,
                                debit,
                                credit,
                                agentComission,
                                addedBy
                            )
                        VALUES
                            ".$param ."
                    ";

                    $statement = $this->db->prepare($sql);

                    $result =  $statement->execute($values);

                    if(!$result){
                        $this->message = 'Failed to add first payment history.';
                        $this->db->rollBack();
                        return false;
                    }
                }
                

                $sql = "
                    UPDATE
                        tbl_propertylist
                    SET 
                        status              = 1,
                        DefaultPlanTerms    = :DefaultPlanTerms,
                        pricePerm2          = :pricePerm2 
                    WHERE
                        property_id            = :propertyID
                ";


                $statement = $this->db->prepare($sql);

                $result =  $statement->execute([
                        'propertyID'            => $property_id[0]['property_id'],
                        'DefaultPlanTerms'      => $addClientprop_planterms,
                        'pricePerm2'            => $addClientprop_pricePerm2,
                    ]);

                if(!$result){
                    $this->message = 'Failed to update property lot status.';
                    $this->db->rollBack();
                    return false;
                }
            }

            $this->lastInsertId =  $clientID;


            return $this->db->commit();
        } catch (PDOException $e) {
            $this->message = 'Something is wrong(1)';
            $this->lastInsertId = 0;
            $this->db->rollBack();
            return false;
        } catch (Exception $e) {
            $this->message = 'Something is wrong(2)';
            $this->lastInsertId = 0;
            $this->db->rollBack();
            return false;
        }

    }




   public function getThisAgent($agentID){
        try {
            $sql = "
                SELECT 
                    tbl_agent.agent_id,
                    tbl_agent.agentPosition,
                    tbl_agent.salesDirectorID,
                    tbl_agent.salesManagerID,
                    tbl_agent.unitManagerID,
                    tbl_agent.managerID
                FROM
                    tbl_agent
                WHERE
                    tbl_agent.agent_id = :agentID
            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'agentID'      => $agentID,
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            $result = $statement->fetchAll();
            
            return $result;

        } catch (PDOException $e) {
            return $e;
        }
    }





    protected $lastInsertId = 0; 


    public function getLastClientInsertedID(){
        return $this->lastInsertId;
    }   





    private function getRemitanceID($clientremitanceCentername){
        try {
            $sql = "
                SELECT 
                    tbl_remitancecenter.remittanceID
                FROM
                    tbl_remitancecenter
                WHERE
                    tbl_remitancecenter.name = :clientremitanceCentername
            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'clientremitanceCentername'      => $clientremitanceCentername,
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            $result = $statement->fetchAll();
            if(count($result) > 0){
                return $result[0]['remittanceID']; 
            }
            else{
                $sql = "
                    INSERT INTO  
                        tbl_remitancecenter(name)
                    VALUES(:clientremitanceCentername)
                ";
     
                $statement = $this->db->prepare($sql);

                $result = $statement->execute([
                        'clientremitanceCentername'      => $clientremitanceCentername,
                    ]);

                if(!$result){
                    return 0;
                }
                else{
                    return $this->db->lastInsertId();   
                }

            }

        } catch (PDOException $e) {
            return $e;
        }
    }




    private function thisPropertyExist($parentID,$blockNumber,$lotNumber,$phaseNumber){
        try {
            $sql = "
                SELECT 
                    date_format(now(),'%M %e, %Y') as dateNow,
                    tbl_propertylist.property_id
                FROM
                    tbl_propertylist
                WHERE
                    tbl_propertylist.active = 1
                AND 
                    tbl_propertylist.status = 0   
                AND
                    tbl_propertylist.propertyParentID = :parentID
                AND    
                    tbl_propertylist.block = :blockNumber
                AND    
                    tbl_propertylist.lot = :lotNumber
                AND    
                    tbl_propertylist.phaseNumber = :phaseNumber
            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'parentID'      => $parentID,
                    'blockNumber'   => $blockNumber,
                    'phaseNumber'   => $phaseNumber,
                    'lotNumber'     => $lotNumber
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }




    public $message = "";
    public $dateNow = "";
    // additinal properties for client
    public function insertThisMoreClientProperties(
                $clientID,
                $chargesDetails,
                $propertySelected,
                $addClientagent,
                $commissionPercentage,
                $addedByUserID 
            ){


        try {
            $this->db->setAttribute(PDO::ATTR_AUTOCOMMIT,false);
            $this->db->beginTransaction();


            //                                       insert new client entry
            
            
            $values = [];
            $param = [];
            $valuesForDownpayment = [];
            $paramForDownpayment = [];
            foreach ($propertySelected as $key => $property) {
                $ornumber                               = trim($property['ornumber']);
                $modeOfpayment                          = trim($property['modeOfpayment']);
                $clientremitanceCentername              = strtoupper($property['clientremitanceCentername']);

                $parentID                               = trim($property['parentID']);
                $phaseNumber                            = trim($property['phaseNumber']);
                $blockNumber                            = trim($property['block']);
                $lotNumber                              = trim($property['lot']);
                $addClientprop_planterms                = trim($property['addClientprop_planterms']);
                $addClientprop_pricePerm2               = trim($property['addClientprop_pricePerm2']);
                $addClientprop_downpayment              = trim($property['addClientprop_downpayment']);
                $addClientDueDate                       = trim($property['addClientDueDate']);
                $addClientprop_dateapplied              = trim($property['addClientprop_dateapplied']);
                $commissionReleased                     = trim($property['comission_release']);
                $addCommission                          = trim($property['add_commission']);

                $property_id = $this->thisPropertyExist($parentID,$blockNumber,$lotNumber,$phaseNumber);
                if(count($property_id) != 1){
                    $this->message = 'Invalid ParentID/blockNumber/lotNumber.';
                    $this->db->rollBack();
                    return false;
                }

                $this->dateNow = $property_id[0]['dateNow'];

                //                            insert new client property entry
                $param = '(
                    :addClientprop_property,
                    :clientID,
                    :chargesDetails,
                    :addClientprop_planterms,
                    :addClientprop_pricePerm2,
                    :addClientprop_downpayment,
                    :addClientprop_dateapplied,
                    :addClientDueDate,
                    :addClientagent,
                    :percentageCommissionJson,
                    :addedByUserID, 
                    :commissionReleased,
                    :adCom
                )';

                
                $values = [
                    'addClientprop_property'               => $property_id[0]['property_id'],                           
                    'clientID'                             => $clientID,   
                    'chargesDetails'                       => json_encode($chargesDetails),                       
                    'addClientprop_planterms'              => $addClientprop_planterms,
                    'addClientprop_pricePerm2'             => $addClientprop_pricePerm2,
                    'addClientprop_downpayment'            => $addClientprop_downpayment,
                    'addClientprop_dateapplied'            => $addClientprop_dateapplied,
                    'addClientDueDate'                     => $addClientDueDate,
                    'addClientagent'                       => $addClientagent,  
                    'percentageCommissionJson'             => json_encode($commissionPercentage),               
                    'addedByUserID'                        => $addedByUserID,   
                    'commissionReleased'                   => $commissionReleased,
                    'adCom'                                => $addCommission
                ];



                $sql = "
                    INSERT INTO
                        tbl_client_properties(
                            property_id,
                            client_id,
                            additionalCharges,
                            plan_terms,
                            sqmPricem2,
                            downpayment,
                            date_applied,
                            dueDate,
                            agent_id,
                            percentageCommissionJson,
                            addedBy, 
                            commissionReleased,
                            adCom
                        )
                    VALUES
                        ".$param."
                ";

                $statement = $this->db->prepare($sql);

                $result =  $statement->execute($values);

                if(!$result){
                    $this->message = 'Failed to add clients property.';
                    $this->db->rollBack();
                    return false;
                }

                $this->lastInsertId = $this->db->lastInsertId();

                // inserting new agent commision entries

                $agent  = $this->getThisAgent($addClientagent);
                // tbl_agent.agent_id,
                // tbl_agent.agentPosition,
                // tbl_agent.salesDirectorID,
                // tbl_agent.salesManagerID,
                // tbl_agent.unitManagerID

                $valuesToBeAdded = [];
                if($agent[0]['agentPosition'] == 1){
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$addClientagent.','.$commissionPercentage['directCom'].')';
                }
                else if($agent[0]['agentPosition'] == 2){
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$addClientagent.','.$commissionPercentage['directCom'].')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['salesDirectorID'].','.$commissionPercentage['SalesDirectorCom'].')';
                }
                else if($agent[0]['agentPosition'] == 3){
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$addClientagent.','.$commissionPercentage['directCom'].')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['salesDirectorID'].','.$commissionPercentage['SalesDirectorCom'].')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['salesManagerID'].','.$commissionPercentage['SalesManagerCom'].')';
                }
                else if($agent[0]['agentPosition'] == 4){
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$addClientagent.','.$commissionPercentage['directCom'].')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['salesDirectorID']. ','.$commissionPercentage['SalesDirectorCom'] .')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['salesManagerID'].  ','.$commissionPercentage['SalesManagerCom']  .')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['unitManagerID'].   ','.$commissionPercentage['UnitManagerCom']   .')';
                }
                else if($agent[0]['agentPosition'] == 5){
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$addClientagent.','.$commissionPercentage['directCom'].')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['salesDirectorID'].','. $commissionPercentage['SalesDirectorCom'].')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['salesManagerID'] .','. $commissionPercentage['SalesManagerCom'] .')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['unitManagerID']  .','. $commissionPercentage['UnitManagerCom']  .')';
                    $valuesToBeAdded[] = '('.$this->lastInsertId.','.$agent[0]['managerID']      .','. $commissionPercentage['ManagerCom'].')';
                }

                

                $sql = "
                    INSERT INTO
                        tbl_commissions(
                            cp_id,
                            agentID,
                            percentagevalue
                        )
                    VALUES
                        ". implode(',', $valuesToBeAdded) . "
                ";

                $statement = $this->db->prepare($sql);
                $result =  $statement->execute();

                if(!$result){
                    $this->message = 'Failed to add commisions';
                    $this->db->rollBack();
                    return false;
                }


                if($addClientprop_downpayment != '0'){

                    $debit = 0;
                    $credit = 0;

                    $clientremitanceCenterID = 0;
                    

                    if($modeOfpayment == 2 || $modeOfpayment == 3){
                        $debit = $addClientprop_downpayment;
                        $clientremitanceCentername = strtoupper(trim($clientremitanceCentername));

                        $clientremitanceCenterID = $this->getRemitanceID($clientremitanceCentername);
                    }
                    else{
                        $clientremitanceCentername  ="";
                        $credit = $addClientprop_downpayment;
                    }

                    $param = '(
                        :cp_id'.$key.',
                        :clientID'.$key.',
                        :ornumber'.$key.',
                        :particulars'.$key.',
                        :clientremitanceCentername'.$key.',
                        :debit'.$key.',
                        :credit'.$key.',
                        :agentComission'.$key.',
                        :dateClaimed'.$key.',
                        :addedBy'.$key.'
                    )';


                    $values = [
                        'cp_id'.$key.''                         => $this->lastInsertId,
                        'clientID'.$key.''                      => $clientID,
                        'particulars'.$key.''                   => 'Downpayment',
                        'ornumber'.$key.''                      => $ornumber,
                        'clientremitanceCentername'.$key.''     => $clientremitanceCenterID,
                        'debit'.$key.''                         => $debit,
                        'credit'.$key.''                        => $credit,
                        'agentComission'.$key.''                => 0,
                        'dateClaimed'.$key.''                   => null,
                        'addedBy'.$key.''                       => $addedByUserID,
                    ];


                    $sql = "
                        INSERT INTO
                            tbl_clientpaymenthistory(
                                cp_id,
                                client_id,
                                ornumber,
                                particulars,
                                remittanceCenter,
                                debit,
                                credit,
                                agentComission,
                                dateClaimed,
                                addedBy
                            )
                        VALUES
                            ".$param ."
                    ";

                    $statement = $this->db->prepare($sql);

                    $result =  $statement->execute($values);

                    if(!$result){
                        $this->message = 'Failed to add first payment history.';
                        $this->db->rollBack();
                        return false;
                    }
                }
                

                $sql = "
                    UPDATE
                        tbl_propertylist
                    SET 
                        status              = 1,
                        DefaultPlanTerms    = :DefaultPlanTerms,
                        pricePerm2          = :pricePerm2 
                    WHERE
                        property_id            = :propertyID
                ";


                $statement = $this->db->prepare($sql);

                $result =  $statement->execute([
                        'propertyID'            => $property_id[0]['property_id'],
                        'DefaultPlanTerms'      => $addClientprop_planterms,
                        'pricePerm2'            => $addClientprop_pricePerm2,
                    ]);

                if(!$result){
                    $this->message = 'Failed to update property lot status.';
                    $this->db->rollBack();
                    return false;
                }
            }

            $this->lastInsertId =  $clientID;


            return $this->db->commit();
        } catch (PDOException $e) {
            $this->message = 'Something is wrong(1)';
            $this->lastInsertId = 0;
            $this->db->rollBack();
            return false;
        } catch (Exception $e) {
            $this->message = 'Something is wrong(2)';
            $this->lastInsertId = 0;
            $this->db->rollBack();
            return false;
        }

    }



    public function thisClientExistByID($clientID){
        try {
            $sql = "
                SELECT 
                    *
                FROM
                    tbl_client
                WHERE
                    tbl_client.active = 1
                AND    
                    tbl_client.client_id = :client_id
            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'client_id' => $clientID,
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }



    public function thisClientExist($addClient_fname,$addClient_lname){
        try {
            $sql = "
                SELECT 
                    *
                FROM
                    tbl_client
                WHERE
                    tbl_client.active = 1
                AND    
                    tbl_client.Fname = :Fname
                AND    
                    tbl_client.Lname = :Lname
            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'Fname' => $addClient_fname,
                    'Lname' => $addClient_lname
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }



    public function getInfoForThisClient($id){
        try {
            $sql = "
                SELECT 
                    tbl_client.client_id as clientID,
                    tbl_client.Fname as update_clientFname,
                    tbl_client.Mname as update_clientMname,
                    tbl_client.Lname as update_clientLname,
                    tbl_client.Address as update_clientAddress,
                    tbl_client.Email as update_clientEmail,
                    tbl_client.Remarks as update_Remarks,
                    tbl_client.image_file as imagePath,
                    tbl_client.ContactNumber as update_clientContactNumber,
                    tbl_client.spouse as spouseList,
                    tbl_client.benificiary as benifList
                FROM
                    tbl_client
                WHERE
                    tbl_client.active = 1
                AND    
                    tbl_client.client_id = :id
            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'id' => $id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }



    public function 
    Properties($clientID){
        try {
            $sql = "
                SELECT 
                    cp_id as 'clientPropertyID',
                    CONCAT(tbl_propertyparent.propertyName,'( ',IF(tbl_propertylist.propertyTypeID = 1,'',tbl_typeofproperty.type_name),' - ','') as 'clientID'
                FROM
                    tbl_client_properties
                INNER JOIN
                    tbl_propertylist on tbl_propertylist.property_id = tbl_client_properties.property_id
                INNER JOIN
                    tbl_propertyparent on tbl_propertyparent.id = tbl_propertylist.propertyParentID
                INNER JOIN
                    tbl_typeofproperty on tbl_typeofproperty.type_id = tbl_propertylist.propertyTypeID
                WHERE
                    tbl_client_properties.active = 1
                AND
                   tbl_client_properties.client_id =  :clientID
            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'clientID' => $clientID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }




    public function getAllClientNames(){
        try {
            $sql = "
                SELECT 
                   DISTINCT tbl_client_properties.client_id as 'clientID',
                    CONCAT(tbl_client.Fname,' ',tbl_client.Mname,' ',tbl_client.Lname) as 'clientName'
                FROM
                    tbl_client,
                    tbl_client_properties
                WHERE
                    tbl_client.active = 1
                and 
                    tbl_client_properties.active = 1
                and 
                    tbl_client_properties.fullyPaid = 0
                and     
                    tbl_client_properties.client_id = tbl_client.client_id
                    ORDER BY tbl_client.Lname ASC
            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

     public function getOrNumber(){
        try {
            $sql = " SELECT ornumber FROM tbl_clientpaymenthistory where active = 1 ORDER BY ornumber DESC LIMIT 1 ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }



    public function getAllClient(){
        try {
            $sql = "
                SELECT 
                    tbl_client.client_id as '0',
                    CONCAT(tbl_client.Fname,' ',tbl_client.Mname,' ',tbl_client.Lname) as '1',
                    tbl_client.address as '2',
                    tbl_client.email as '3',
                    tbl_client.contactNumber as '4',
                    CONCAT('<button type=\"button\" title=\"',tbl_client.Remarks,'\"  class=\"btn btn-outline green btn_viewThisClient\" data-id=\"',tbl_client.client_id,'\" ><i class=\"icon-pencil\"  ></i> View Profile</button>') as '5'
                FROM
                    tbl_client
                WHERE
                    tbl_client.active = 1
                ORDER BY tbl_client.client_id asc
            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getThisClientDownPayment($propValue)
    {
         try {
            $sql = "
                SELECT 
                    downpayment
                FROM
                    tbl_client_properties
                WHERE
                    active = 1
                and 
                    cp_id =:propID
            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute([
                'propID' => $propValue
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function updateThisClientDownpayment($propValue, $downpayment)
    {
        
        try {
            $sql = "
                UPDATE
                    tbl_client_properties
                SET 
                    downpayment      = :newdown
                WHERE
                    cp_id            = :propID
            ";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
                    'newdown'            => $downpayment,
                    'propID'             => $propValue
                ]);


        } catch (PDOException $e) {
            return $e;
        }
    }

    public function addThisnewSpouse($clID, $data)
    {
      try {
           $sql = "
                UPDATE
                    tbl_client
                SET 
                    spouse = :data
                WHERE
                    client_id = :clientID
            ";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
                'clientID'      => $clID,
                'data'          => $data
            ]);

      } catch (PDOException $e) {
         return $e;
      }
    }

    public function add_newBeneficiary($clID, $data)
    {
        try {
            
            $sql = "
                UPDATE
                    tbl_client
                SET 
                    benificiary = :data
                WHERE
                    client_id = :clientID
            ";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
                'clientID'      => $clID,
                'data'          => $data
            ]);

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function get_ThisClients($propId)
    {
        try {

            $sql = "
                SELECT 
                    tbl_client.client_id as '0',
                    CONCAT(tbl_client.Fname,' ',tbl_client.Mname,' ',tbl_client.Lname) as '1',
                    tbl_client.address as '2',
                    tbl_client.email as '3',
                    tbl_client.contactNumber as '4',
                    CONCAT('<button type=\"button\" class=\"btn btn-outline green btn_viewThisClient\" data-id=\"',tbl_client.client_id,'\" ><i class=\"icon-pencil\"  ></i> View Profile</button>') as '5'
                FROM
                    tbl_client
                WHERE
                    tbl_client.active = 1
                and 
                    client_id = any(select cp_id from tbl_client_properties where property_id = any(select property_id from tbl_propertylist where propertyParentID = " . $propId . "))
                ORDER BY tbl_client.client_id asc
            ";
 
            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getThisClientListBySelectedBlock($listId)
    {
        try {

            $sql = "
                SELECT 
                    tbl_client.client_id as '0',
                    CONCAT(tbl_client.Fname,' ',tbl_client.Mname,' ',tbl_client.Lname) as '1',
                    tbl_client.address as '2',
                    tbl_client.email as '3',
                    tbl_client.contactNumber as '4',
                    CONCAT('<button type=\"button\" class=\"btn btn-outline green btn_viewThisClient\" data-id=\"',tbl_client.client_id,'\" ><i class=\"icon-pencil\"  ></i> View Profile</button>') as '5'
                FROM
                    tbl_client
                WHERE
                    tbl_client.active = 1
                and 
                    client_id = any(select cp_id from tbl_client_properties where property_id = ". $listId .")
                ORDER BY tbl_client.client_id asc
            ";
 
            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAgentAssociatedClients($userID)
    {
        try {
            $sql ="select 
                        b.cp_id as '0',
                        CONCAT(c.Fname,' ',c.Mname,' ',c.Lname) as '1',
                        c.address as '2',
                        c.email as '3',
                        c.contactNumber as '4',
                        CONCAT('<button type=\"button\" class=\"btn btn-outline green btn_viewThisClient\" data-id=\"',c.client_id,'\" ><i class=\"icon-pencil\"  ></i> View Profile</button>') as '5'
                   from 
                        tbl_commissions a left join tbl_client_properties b on a.cp_id = b.cp_id inner join tbl_client c on b.client_id = c.client_id where agentID = any(select refID from tbl_users where UserID =". $userID .")";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();            
            
        } catch (PDOException $e) {
            return $e;
        }
    }


}