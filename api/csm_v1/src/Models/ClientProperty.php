<?php

namespace App\Models;

use PDO;

class ClientProperty {

    /**
    * @var $db
    */
    private $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }







    public function insertThisClientProperty($agentFname,$agentLname,$agentMname,$agentAddress,$agentContactNumber,$agentEmail,$userID){

        try {
            $sql = "
                INSERT INTO
                    tbl_agent(
                        Fname,
                        Lname,
                        Mname,
                        address,
                        contactNumber,
                        email,
                        addedBy
                    )
                VALUES(
                    :agentFname,
                    :agentLname,
                    :agentMname,
                    :agentAddress,
                    :agentContactNumber,
                    :agentEmail,
                    :userID
                )
            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute([
                    'agentFname'            => $agentFname,
                    'agentLname'            => $agentLname,
                    'agentMname'            => $agentMname,
                    'agentAddress'          => $agentAddress,
                    'agentContactNumber'    => $agentContactNumber,
                    'agentEmail'            => $agentEmail,
                    'userID'                => $userID
                ]);


        } catch (PDOException $e) {
            return $e;
        }

    }




    public function getAllPropertyByAgentID($agentID){
        try {
            $sql = "
                SELECT
                    (tbl_client_properties.sqmPricem2 * tbl_propertylist.sqm) as contractPrice,
                    ((tbl_client_properties.sqmPricem2 * tbl_propertylist.sqm) / tbl_client_properties.plan_terms) as monthlyAmortization,
                    tbl_client_properties.additionalCharges,
                    tbl_client_properties.sqmPricem2,
                    tbl_propertylist.sqm,
                    tbl_client_properties.plan_terms
                FROM
                    tbl_client_properties
                INNER JOIN
                    tbl_propertylist on  tbl_propertylist.property_id = tbl_client_properties.property_id
                WHERE
                    tbl_client_properties.active = 1
                AND
                    tbl_client_properties.cp_id = :agentID

            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'agentID'          => $agentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);


            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }

    // public function  getUpdateAgentComTable($cp_id){
    //     try {

    //         $sql = "
    //             UPDATE
    //                 tbl_commissions
    //             SET
    //                 comReleaseNo  = 0
    //             WHERE
    //                 cp_id         = (SELECT tbl_client_properties.cp_id from tbl_client_properties where tbl_client_properties.property_id = :cp_id)

    //         ";

    //         $statement = $this->db->prepare($sql);
    //         return $statement->execute([
    //             'cp_id'  => $cp_id
    //         ]);


    //     } catch (PDOException $e) {
    //         return $e;
    //     }
    // }
    public function getLatePayments()
    {
        //
        try
        {
            $sql = "
                   SELECT CONCAT(clients.`Fname`, ' ', clients.`Lname`) AS fName,cp.`date_applied`,ROUND((ABS(DATEDIFF(CURRENT_TIMESTAMP,cp.`date_applied`))/30)-COUNT(history.`id`)) AS diff, propName.`propertyName`, plist.`block`, plist.`lot`, COUNT(history.`id`) AS counter
                FROM tbl_client AS clients, tbl_clientpaymenthistory AS history, tbl_client_properties AS cp, tbl_propertylist AS plist, tbl_propertyparent AS propName
                WHERE clients.`client_id` = history.`client_id` AND history.`cp_id`=cp.`cp_id` AND cp.`property_id`=plist.`property_id` 
                AND plist.`propertyParentID`=propName.`id` AND cp.`active`='1' AND history.`active`='1' AND cp.`fullyPaid`!='1'
                GROUP BY history.`cp_id` ORDER BY ROUND((ABS(DATEDIFF(CURRENT_TIMESTAMP,cp.`date_applied`))/30)-COUNT(history.`id`)) DESC ;
            ";
                $statement = $this->db->prepare($sql);
              $statement->execute();
              $statement->setFetchMode(PDO::FETCH_ASSOC);
          return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
          
        }
    
}
 public function getLatePaymentswithPropID($parentID)
    {
        //
        try
        {
            $sql = "
            SELECT CONCAT(clients.`Fname`, ' ', clients.`Lname`) AS fName,cp.`date_applied`,ROUND((ABS(DATEDIFF(CURRENT_TIMESTAMP,cp.`date_applied`))/30)-COUNT(history.`id`)) AS diff, propName.`propertyName`, plist.`block`, plist.`lot`, COUNT(history.`id`) AS counter, clients.`ContactNumber`
                FROM tbl_client AS clients, tbl_clientpaymenthistory AS history, tbl_client_properties AS cp, tbl_propertylist AS plist, tbl_propertyparent AS propName
                WHERE clients.`client_id` = history.`client_id` AND history.`cp_id`=cp.`cp_id` AND cp.`property_id`=plist.`property_id` 
                AND plist.`propertyParentID`=propName.`id` AND cp.`active`='1' AND history.`active`='1'  AND cp.`fullyPaid`!='1' AND propName.`id`=".$parentID."
                GROUP BY history.`cp_id` ORDER BY ROUND((ABS(DATEDIFF(CURRENT_TIMESTAMP,cp.`date_applied`))/30)-COUNT(history.`id`)) DESC ;
            ";
                $statement = $this->db->prepare($sql);
              $statement->execute();
              $statement->setFetchMode(PDO::FETCH_ASSOC);
          return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
          
        }
    
}
    public function  getUpdateAgentComTable($cp_id){
        try {

            $sql = "
                UPDATE
                    tbl_commissions
                SET
                    comReleaseNo  = 0
                WHERE
                    cp_id  = :cp_id
            ";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
                'cp_id'  => $cp_id
            ]);

        } catch (PDOException $e) {
            return $e;
        }
    }

    // payment history for propertylist
     public function getAllPropertyByClientOwnerIDTwo($cpID, $clientid){
        try {
            $sql = "
                SELECT
                    (tbl_client_properties.sqmPricem2 * tbl_propertylist.sqm) as contractPrice,
                    ((tbl_client_properties.sqmPricem2 * tbl_propertylist.sqm) / tbl_client_properties.plan_terms) as monthlyAmortization,
                    tbl_client_properties.additionalCharges,
                    tbl_client_properties.sqmPricem2,
                    tbl_propertylist.sqm,
                    tbl_client_properties.plan_terms,
                    tbl_client_properties.commissionReleased,
                    tbl_client_properties.adCom
                FROM
                    tbl_client_properties
                INNER JOIN
                    tbl_propertylist on  tbl_propertylist.property_id = tbl_client_properties.property_id
                WHERE
                    tbl_client_properties.active = 1
                AND
                    tbl_client_properties.property_id = :cpID
                and
                    tbl_client_properties.client_id = :clientid

            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'cpID'       => $cpID,
                    'clientid'   => $clientid
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);


            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }


    // get realese com
    public function getAllPropertyByClientOwnerID($cpID){
        try {
            $sql = "
                SELECT
                    (tbl_client_properties.sqmPricem2 * tbl_propertylist.sqm) as contractPrice,
                    ((tbl_client_properties.sqmPricem2 * tbl_propertylist.sqm) / tbl_client_properties.plan_terms) as monthlyAmortization,
                    tbl_client_properties.additionalCharges,
                    tbl_client_properties.sqmPricem2,
                    tbl_propertylist.sqm,
                    tbl_client_properties.plan_terms,
                    tbl_client_properties.commissionReleased,
                    tbl_client_properties.adCom
                FROM
                    tbl_client_properties
                INNER JOIN
                    tbl_propertylist on  tbl_propertylist.property_id = tbl_client_properties.property_id
                WHERE
                    tbl_client_properties.active = 1
                AND
                    tbl_client_properties.cp_id = :cpID

            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'cpID'   => $cpID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getPropertiesForThisclient($id){
        try {
            $sql = "
                SELECT
                    tbl_client_properties.cp_id as clientPropertyID,
                    CONCAT(tbl_propertyparent.propertyName,IF(tbl_propertylist.phaseNumber = 0,' ',CONCAT(' Phase ',tbl_propertylist.phaseNumber)),' Block ',tbl_propertylist.block,' Lot ',tbl_propertylist.lot,'  ',IF(tbl_propertylist.propertyTypeID = 2,CONCAT('(',tbl_propertylist.model,')'),''),' - ',tbl_typeofproperty.type_name) as propertyName,
                        tbl_client_properties.active as propStat
                FROM
                    tbl_client_properties
                INNER JOIN
                    tbl_propertylist ON tbl_propertylist.property_id = tbl_client_properties.property_id
                INNER JOIN
                    tbl_propertyparent ON tbl_propertyparent.id = tbl_propertylist.propertyParentID
                INNER JOIN
                    tbl_typeofproperty ON tbl_typeofproperty.type_id = tbl_propertylist.propertyTypeID
                WHERE
                    tbl_client_properties.active = 1
                AND
                    tbl_client_properties.client_id = :id
                ORDER BY
                    propertyName ASC
            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'id' => $id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    // if property is forfieted
    public function getPropertiesForThisclientForfiet($id){
        try {
            $sql = "
                SELECT
                    tbl_client_properties.cp_id as clientPropertyID,
                    CONCAT(tbl_propertyparent.propertyName,IF(tbl_propertylist.phaseNumber = 0,' ',CONCAT(' Phase ',tbl_propertylist.phaseNumber)),' Block ',tbl_propertylist.block,' Lot ',tbl_propertylist.lot,'  ',IF(tbl_propertylist.propertyTypeID = 2,CONCAT('(',tbl_propertylist.model,')'),''),' - ',tbl_typeofproperty.type_name) as propertyName,
                        tbl_client_properties.active as propStat

                FROM
                    tbl_client_properties
                INNER JOIN
                    tbl_propertylist ON tbl_propertylist.property_id = tbl_client_properties.property_id
                INNER JOIN
                    tbl_propertyparent ON tbl_propertyparent.id = tbl_propertylist.propertyParentID
                INNER JOIN
                    tbl_typeofproperty ON tbl_typeofproperty.type_id = tbl_propertylist.propertyTypeID
                WHERE
                    tbl_client_properties.active = 0
                AND
                    tbl_client_properties.client_id = :id
                ORDER BY
                    propertyName ASC
            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'id' => $id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getContractInfo($clientID){
        try {
            $sql ="
                SELECT DISTINCT
                    round(((tbl_propertylist.pricePerm2  * tbl_propertylist.sqm) / tbl_client_properties.plan_terms), 2) as 'monthly',
                    ((tbl_propertylist.pricePerm2  * tbl_propertylist.sqm)) as 'contractPrice',
                    tbl_propertylist.originalPrice,
                    tbl_propertylist.DefaultPlanTerms,
                    tbl_client_properties.fullyPaid,
                    tbl_client_properties.downpayment
                from
                    tbl_propertylist,
                    tbl_client_properties
                WHERE
                    tbl_propertylist.property_id = tbl_client_properties.property_id
                and
                    tbl_client_properties.cp_id = :id
            ";
            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'id' => $clientID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getContactPrice($clientID){
            try {
                $sql = "
                        SELECT DISTINCT
                            (((tbl_propertylist.pricePerm2  * tbl_propertylist.sqm)  - (tbl_clientpaymenthistory.credit + tbl_clientpaymenthistory.debit)) / tbl_client_properties.plan_terms) as 'monthly',
                            ((tbl_propertylist.pricePerm2  * tbl_propertylist.sqm) - (tbl_clientpaymenthistory.credit + tbl_clientpaymenthistory.debit)) as 'contractPrice',
                            tbl_propertylist.originalPrice,
                            tbl_clientpaymenthistory.credit,
                            tbl_propertylist.DefaultPlanTerms,
                            tbl_client_properties.fullyPaid,
                            tbl_client_properties.downpayment
                        from
                            tbl_clientpaymenthistory,
                            tbl_propertylist,
                            tbl_client_properties
                        WHERE
                            tbl_clientpaymenthistory.particulars = 'Downpayment'
                        and
                            tbl_propertylist.property_id = tbl_client_properties.property_id
                        and
                            tbl_client_properties.cp_id = '$clientID'
                        and
                            tbl_clientpaymenthistory.active = 1
                    ";

            $statement = $this->db->prepare($sql);

            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();

        } catch (Exception $e) {
            return $e;
        }
    }




public function getTotalPayment($propertyID){
        try {
                $sql = "
                        SELECT
                            sum((tbl_clientpaymenthistory.debit + tbl_clientpaymenthistory.credit))
                        as
                            totalPayment
                        from
                            tbl_clientpaymenthistory
                        where
                            tbl_clientpaymenthistory.particulars != 'Downpayment'
                        and
                            tbl_clientpaymenthistory.cp_id = '$propertyID'
                        and
                            tbl_clientpaymenthistory.active = 1

                    ";

            $statement = $this->db->prepare($sql);

            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();

        } catch (Exception $e) {
            return $e;
        }
}

    public function getPropertyDetails($id){
        try {
            $sql = "
                SELECT
                    tbl_client_properties.client_id as client_id,
                    tbl_client_properties.property_id as prop_id,
                    tbl_propertyparent.address as prop_address,
                    tbl_propertylist.phaseNumber as prop_phaseNo,
                    tbl_propertylist.block as prop_blkNo,
                    tbl_propertylist.lot as prop_ltNo
                FROM
                    tbl_client_properties,
                    tbl_propertyparent,
                    tbl_propertylist
                where
                    tbl_propertylist.property_id = tbl_client_properties.property_id
                and
                    tbl_propertylist.propertyParentID = tbl_propertyparent.id
                and
                    tbl_client_properties.client_id = :id";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'id' => $id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }






    // forfiet selected property
    public function forfeitThisSelectedProperty($clientID, $cp_id)
    {
        try {

            $this->db->setAttribute(PDO::ATTR_AUTOCOMMIT,false);
            $this->db->beginTransaction();

            $sql = "UPDATE tbl_client_properties SET active  =  0 WHERE cp_id     = :cp_id and
                    client_id = :clientID and active = 1";

            $statement = $this->db->prepare($sql);
            $updateClientProp = $statement->execute([
                'clientID'  => $clientID,
                'cp_id'     => $cp_id
            ]);
            $this->forfeitThisSelectedPropertyTwo($cp_id);
            if(!$updateClientProp){
                $this->db->rollBack();
                return array(
                    "status" => false,
                    "message" => 'Unable to update client property!'
                );
            }
            

            $sql = "UPDATE tbl_clientpaymenthistory SET active = 0 WHERE cp_id = :cp_id AND
            client_id = :clientID AND active = 1";
            
            $statement = $this->db->prepare($sql);
            $updateClientPaymt = $statement->execute([
                'clientID'  => $clientID,
                'cp_id'     => $cp_id
            ]);

            if(!$updateClientPaymt){
                $this->db->rollBack();
                return array(
                    "status" => false,
                    "message"  => 'Unable to update client payment history'
                );
            }

            $this->db->commit();
            return array(
                "status" => true,
                "message" => 'Property Forfeited!'
            );



        } catch (PDOException $e) {
            return $e;
        }
    }

    public function forfeitThisSelectedPropertyTwo($cp_id)
    {
        try {

            $sql = "
                UPDATE
                     tbl_propertylist
                SET
                     status  =  0
                WHERE
                    property_id  = (SELECT property_id from tbl_client_properties where cp_id = :cp_id)
                and
                    active = 1";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
            'cp_id'=>$cp_id
            ]);

        } catch (PDOException $e) {
            return $e;
        }
    }




    //add additioanl charges in client profile
    public function updateAdditionalCharges($clientID, $cp_id, $chargesDetails)
     {
        try {

             $sql = "
                UPDATE
                    tbl_client_properties
                SET
                    additionalCharges  = :addCharges
                WHERE
                    cp_id     = :cp_id
                and
                    client_id = :clientID
                and
                    active = 1
            ";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
                'clientID'  => $clientID,
                'cp_id'     => $cp_id,
                'addCharges'    => json_encode($chargesDetails)
            ]);
        } catch (PDOException $e) {
            return $e;
        }
     }

    //forfeited property for additional charges
      public function getAdditionalChargesforfeited($cp_id)
    {
        try {
            $sql="
                SELECT
                    additionalCharges
                FROM
                    tbl_client_properties
                where
                    cp_id = :id
                and
                    active = 0
                ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'id' => $cp_id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    // get addtioanl charges for client profile
    public function getAdditionalCharges($cp_id)
    {
        try {
            $sql="
                SELECT
                    additionalCharges
                FROM
                    tbl_client_properties
                where
                    cp_id = :id
                and
                    active = 1
                ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'id' => $cp_id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function updateAddCommission($clientID, $cp_id, $addCom)
    {
        try {
             $sql = "
                UPDATE
                    tbl_client_properties
                SET
                    adCom  = :comRel
                WHERE
                    cp_id     = :cp_id
                and
                    client_id = :clientID
                and
                    active = 1
            ";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
                'clientID'  => $clientID,
                'cp_id'     => $cp_id,
                'comRel'    => $addCom
            ]);
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function updateComRelShecdule($clientID, $cp_id, $comReleasedSched)
    {
        try {
             $sql = "
                UPDATE
                    tbl_client_properties
                SET
                    commissionReleased  = :comRel
                WHERE
                    cp_id     = :cp_id
                and
                    client_id = :clientID
                and
                    active = 1
            ";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
                'clientID'  => $clientID,
                'cp_id'     => $cp_id,
                'comRel'    => $comReleasedSched
            ]);
        } catch (PDOException $e) {
            return $e;
        }
    }

    // forfeited froperty info
     public function getPropertyInfoforfeited($id){
        try {
            $sql = "
                SELECT
                    tbl_client_properties.additionalCharges,
                    tbl_propertyparent.address as propertyAddress,
                    CONCAT(IF(tbl_propertylist.phaseNumber = 0,' ',CONCAT('Phase ',tbl_propertylist.phaseNumber)),' Block ',tbl_propertylist.block,' Lot ',tbl_propertylist.lot) AS blockAndLot,
                    CONCAT(tbl_typeofproperty.type_name ,'(',tbl_propertylist.sqm,' sqm<sup>2</sup> )')  as propertyType,
                    tbl_propertylist.model  as propertyModel,
                    tbl_client_properties.plan_terms  ,
                    tbl_client_properties.dueDate  ,
                    tbl_propertylist.sqm  ,
                    tbl_propertylist.pricePerm2  ,
                    DATE_FORMAT(tbl_client_properties.date_applied,'%b %e, %Y') as dateApplied,
                    (tbl_propertylist.sqm * tbl_client_properties.sqmPricem2) as contractPrice,
                    (IF(tbl_client_properties.plan_terms > 1 , CONCAT(tbl_client_properties.plan_terms,' Months') , CONCAT(tbl_client_properties.plan_terms,' Month'))) as planTerms,
                    tbl_client_properties.downpayment as downpayment,
                    ((tbl_propertylist.sqm * tbl_client_properties.sqmPricem2) / tbl_client_properties.plan_terms) as monthlyAmortization,
                    CONCAT(tbl_agent.Fname ,' ',tbl_agent.Lname) AS agentFullname,
                    tbl_client_properties.commissionReleased as ComRelease,
                    tbl_client_properties.fullyPaid,
                    tbl_client_properties.adCom
                FROM
                    tbl_client_properties
                INNER JOIN
                    tbl_propertylist ON tbl_propertylist.property_id = tbl_client_properties.property_id
                INNER JOIN
                    tbl_propertyparent ON tbl_propertyparent.id = tbl_propertylist.propertyParentID
                INNER JOIN
                    tbl_typeofproperty ON tbl_typeofproperty.type_id = tbl_propertylist.propertyTypeID
                INNER JOIN
                    tbl_agent ON tbl_agent.agent_id = tbl_client_properties.agent_id
                WHERE
                    tbl_client_properties.active = 0
                AND
                    tbl_client_properties.cp_id = :id
            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'id' => $id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    // request from client property info
    public function getPropertyInfo($id){
        try {
            $sql = "
                SELECT
                    tbl_client_properties.additionalCharges,
                    tbl_propertyparent.address as propertyAddress,
                    CONCAT(IF(tbl_propertylist.phaseNumber = 0,' ',CONCAT('Phase ',tbl_propertylist.phaseNumber)),' Block ',tbl_propertylist.block,' Lot ',tbl_propertylist.lot) AS blockAndLot,
                    CONCAT(tbl_typeofproperty.type_name ,'(',tbl_propertylist.sqm,' sqm<sup>2</sup> )')  as propertyType,
                    tbl_propertylist.model  as propertyModel,
                    tbl_client_properties.plan_terms  ,
                    tbl_client_properties.dueDate  ,
                    tbl_propertylist.sqm  ,
                    tbl_propertylist.pricePerm2  ,
                    DATE_FORMAT(tbl_client_properties.date_applied,'%b %e, %Y') as dateApplied,
                    (tbl_propertylist.sqm * tbl_client_properties.sqmPricem2) as contractPrice,
                    (IF(tbl_client_properties.plan_terms > 1 , CONCAT(tbl_client_properties.plan_terms,' Months') , CONCAT(tbl_client_properties.plan_terms,' Month'))) as planTerms,
                    tbl_client_properties.downpayment as downpayment,
                    (((tbl_propertylist.sqm * tbl_client_properties.sqmPricem2)- tbl_client_properties.downpayment) / tbl_client_properties.plan_terms) as monthlyAmortization,
                    CONCAT(tbl_agent.Fname ,' ',tbl_agent.Lname) AS agentFullname,
                    tbl_client_properties.commissionReleased as ComRelease,
                    tbl_client_properties.fullyPaid,
                    tbl_client_properties.adCom
                FROM
                    tbl_client_properties
                INNER JOIN
                    tbl_propertylist ON tbl_propertylist.property_id = tbl_client_properties.property_id
                INNER JOIN
                    tbl_propertyparent ON tbl_propertyparent.id = tbl_propertylist.propertyParentID
                INNER JOIN
                    tbl_typeofproperty ON tbl_typeofproperty.type_id = tbl_propertylist.propertyTypeID
                INNER JOIN
                    tbl_agent ON tbl_agent.agent_id = tbl_client_properties.agent_id
                WHERE
                    tbl_client_properties.active = 1
                AND
                    tbl_client_properties.cp_id = :id
            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'id' => $id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    // public function clientPropertyExist($clientID, $propertyId){
    //     try {
    //         $sql = "
    //             SELECT
    //                 cp_id
    //             FROM
    //                 tbl_client_properties
    //             WHERE
    //                 tbl_client_properties.active = 1
    //             AND
    //                 tbl_client_properties.property_id = :propertyId
    //             AND
    //                 tbl_client_properties.client_id = :clientID
    //         ";

    //         $statement = $this->db->prepare($sql);

    //         $statement->execute([
    //                 'propertyId'    => $propertyId,
    //                 'clientID'      => $clientID
    //             ]);
    //         $statement->setFetchMode(PDO::FETCH_ASSOC);

    //         return $statement->fetchAll();

    //     } catch (PDOException $e) {
    //         return $e;
    //     }
    // }


    public function clientPropertyExist($clientID, $propertyId){
        try {
            $sql = "
                SELECT
                    cp_id
                FROM
                    tbl_client_properties
                WHERE
                    tbl_client_properties.active = 1
                AND
                    tbl_client_properties.cp_id = :propertyId
                AND
                    tbl_client_properties.client_id = :clientID
            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'propertyId'    => $propertyId,
                    'clientID'      => $clientID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getClientwithNumber($cp_id){
         try {
            $sql = "
                SELECT CONCAT(cliente.`Fname`,' ',cliente.`Lname`) AS fullName,parent.`propertyName`,`cliente`.ContactNumber, plist.`block`, plist.`lot`,
  property.`dueDate`FROM tbl_client_properties AS property, tbl_client AS cliente, 
  tbl_agent AS agent, tbl_propertylist AS plist, tbl_propertyparent AS parent WHERE  property.`client_id` = cliente.`client_id` 
  AND property.`agent_id` = agent.`agent_id` AND property.`property_id`=plist.`property_id` AND plist.`propertyParentID` = parent.`id` AND property.`cp_id`= :cp_ids
            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'cp_ids'      => $cp_id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }
    public function getThisPropertyDetails($propertyID)
    {

      try {
          $sql = "
              SELECT
                  tbl_client_properties.client_id as client_id,
                  tbl_client_properties.property_id as prop_id,
                  tbl_propertyparent.address as prop_address,
                  tbl_propertylist.phaseNumber as prop_phaseNo,
                  tbl_propertylist.block as prop_blkNo,
                  tbl_propertylist.lot as prop_ltNo
              FROM
                  tbl_client_properties,
                  tbl_propertyparent,
                  tbl_propertylist
              where
                  tbl_propertylist.property_id = tbl_client_properties.property_id
              and
                  tbl_propertylist.propertyParentID = tbl_propertyparent.id
              and
                  tbl_client_properties.cp_id = :id
          ";

          $statement = $this->db->prepare($sql);
          $statement->execute([
                  'id' => $propertyID
              ]);
          $statement->setFetchMode(PDO::FETCH_ASSOC);
          return $statement->fetchAll();

      } catch (PDOException $e) {
          return $e;
      }

    }

    

}
