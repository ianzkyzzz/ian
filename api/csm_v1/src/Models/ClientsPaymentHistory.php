<?php

namespace App\Models;

use PDO;

class ClientPaymentHistory {

    /**
    * @var $db
    */
    private $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }

    // ge gamit sa get payment table sa client prev code (SELECT tbl_client_properties.cp_id from tbl_client_properties where tbl_client_properties.property_id =:id)
    public function getPropertiesForThisclient($cpID){
        try {
            $sql = "
               SELECT 
                    (IF(tbl_clientpaymenthistory.particulars = '', CONCAT('Month of ',DATE_FORMAT(tbl_clientpaymenthistory.dateAdded,'%M %Y')) , tbl_clientpaymenthistory.particulars)) as 'particulars',
                    DATE_FORMAT(tbl_clientpaymenthistory.paymentMade,'%b %e, %Y') as dateAdded,
                    tbl_clientpaymenthistory.debit as propertyDebit,
                    tbl_clientpaymenthistory.credit as propertyCredit,
                    tbl_clientpaymenthistory.agentComission as propertyAgentComission,
                    tbl_clientpaymenthistory.dateClaimed as dateClaimed,  
                    tbl_clientpaymenthistory.remittanceCenter as remittanceCenter,
                    tbl_clientpaymenthistory.ornumber as orNumber

                FROM
                    tbl_clientpaymenthistory
                WHERE
                    tbl_clientpaymenthistory.active = 1
                AND    
                    tbl_clientpaymenthistory.cp_id = :id
                    ORDER BY paymentMade ASC
            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'id' => $cpID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getPropertiesForThisclientforfietd($cpID, $ClientID){
        try {
            $sql = "
               SELECT 
                    (IF(tbl_clientpaymenthistory.particulars = '', CONCAT('Month of ',DATE_FORMAT(tbl_clientpaymenthistory.dateAdded,'%M %Y')) , tbl_clientpaymenthistory.particulars)) as 'particulars',
                    DATE_FORMAT(tbl_clientpaymenthistory.paymentMade,'%b %e, %Y') as dateAdded,
                    tbl_clientpaymenthistory.debit as propertyDebit,
                    tbl_clientpaymenthistory.credit as propertyCredit,
                    tbl_clientpaymenthistory.agentComission as propertyAgentComission,
                    tbl_clientpaymenthistory.dateClaimed as dateClaimed,
                    tbl_clientpaymenthistory.ornumber as orNumber

                FROM
                    tbl_clientpaymenthistory
                WHERE
                    tbl_clientpaymenthistory.active = 1
                AND    
                    tbl_clientpaymenthistory.cp_id = :id
                and 
                    tbl_clientpaymenthistory.client_id = :clientID
                    ORDER BY id ASC
            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'id'       => $cpID, 
                    'clientID' => $ClientID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getComReleasedforfieted($cp_id){
        try {

            $sql = "
                SELECT 
                    tbl_client_properties.commissionReleased 
                from 
                    tbl_client_properties 
                where 
                    tbl_client_properties.cp_id =:id
                and 
                    tbl_client_properties.active = 0
            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'id' => $cp_id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getComReleased($cp_id){
        try {

            $sql = "
                SELECT 
                    tbl_client_properties.commissionReleased 
                from 
                    tbl_client_properties 
                where 
                    tbl_client_properties.cp_id =:id
                and 
                    tbl_client_properties.active = 1
            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'id' => $cp_id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    // public function updateClientPoperty($cp_id, $paymentStatus){
    //     try {
    //         $sql="
    //             UPDATE 
    //                 tbl_client_properties 
    //             set 
    //                 fullyPaid = :paymentStatus
    //             where 
    //                 cp_id = :cp_id
    //         ";

    //         $statement = $this->db->prepare($sql);
    //         return $statement->execute([
    //                 'cp_id'    => $cp_id,
    //                 'paymentStatus'    => $paymentStatus
    //             ]);

            
    //     } catch (PDOException $e) {
    //         return $e;
    //     }
    // }

    public function updateClientPoperty($cp_id, $paymentStatus){
        try {
            $sql="
                UPDATE 
                    tbl_client_properties 
                set 
                    fullyPaid = :paymentStatus
                where 
                    cp_id = :cp_id
            ";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
                    'cp_id'    => $cp_id,
                    'paymentStatus'    => $paymentStatus
                ]);

            
        } catch (PDOException $e) {
            return $e;
        }
    }

    // view payment history for this property.. propertylist
    public function getPaymentHistoryForThisProperty($cpID, $clientid){
        try {
            $sql = "
                SELECT 
                    ((tbl_client_properties2.pricePerm2 * tbl_client_properties2.sqm) / tbl_client_properties2.DefaultPlanTerms) as 'monthlyAmortization',
                    tbl_clientpaymenthistory.particulars as 'particulars',
                    DATE_FORMAT(tbl_clientpaymenthistory.dateAdded,'%b %e, %Y') as dateAdded,
                    tbl_client_properties2.property_id,
                    tbl_client_properties2.pricePerm2,
                    tbl_client_properties2.sqm,
                    tbl_client_properties2.DefaultPlanTerms,
                    tbl_clientpaymenthistory.debit as propertyDebit,
                    tbl_clientpaymenthistory.credit as propertyCredit,
                    (tbl_client_properties2.pricePerm2 * tbl_client_properties2.sqm)  as propertyContractPrice,
                    tbl_clientpaymenthistory.agentComission as propertyAgentComission,
                    tbl_clientpaymenthistory.dateClaimed as dateClaimed,
                    tbl_client_properties2.desc as 'desc'
                FROM
                    tbl_clientpaymenthistory
                INNER JOIN
                    (SELECT
                        tbl_propertylist.property_id,
                        tbl_propertylist.pricePerm2,
                        tbl_propertylist.sqm,
                        tbl_propertylist.DefaultPlanTerms,
                        tbl_client_properties.cp_id,
                        tbl_client_properties.desc
                    FROM
                        tbl_client_properties
                    INNER JOIN 
                        tbl_propertylist ON tbl_propertylist.property_id = tbl_client_properties.property_id
                    ) as tbl_client_properties2  on tbl_client_properties2.cp_id = tbl_clientpaymenthistory.cp_id
                WHERE
                    tbl_clientpaymenthistory.active = 1
                AND    
                    tbl_client_properties2.property_id = :id
                and 
                    tbl_clientpaymenthistory.client_id = :clientid
            ";
  
            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id' => $cpID,
                'clientid' => $clientid

                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getPropertiesPaymentHistory($cpID){
        try {
            $sql = "

                SELECT 
                    ((tbl_client_properties2.pricePerm2 * tbl_client_properties2.sqm) / tbl_client_properties2.DefaultPlanTerms) as 'monthlyAmortization',
                    tbl_clientpaymenthistory.particulars as 'particulars',
                    DATE_FORMAT(tbl_clientpaymenthistory.dateAdded,'%b %e, %Y') as dateAdded,
                    tbl_client_properties2.property_id,
                    tbl_client_properties2.pricePerm2,
                    tbl_client_properties2.sqm,
                    tbl_client_properties2.DefaultPlanTerms,
                    tbl_clientpaymenthistory.debit as propertyDebit,
                    tbl_clientpaymenthistory.credit as propertyCredit,
                    (tbl_client_properties2.pricePerm2 * tbl_client_properties2.sqm)  as propertyContractPrice,
                    tbl_clientpaymenthistory.agentComission as propertyAgentComission,
                    tbl_clientpaymenthistory.dateClaimed as dateClaimed
                FROM
                    tbl_clientpaymenthistory
                INNER JOIN
                    (SELECT
                        tbl_propertylist.property_id,
                        tbl_propertylist.pricePerm2,
                        tbl_propertylist.sqm,
                        tbl_propertylist.DefaultPlanTerms,
                        tbl_client_properties.cp_id
                    FROM
                        tbl_client_properties
                    INNER JOIN 
                        tbl_propertylist ON tbl_propertylist.property_id = tbl_client_properties.property_id
                    ) as tbl_client_properties2  on tbl_client_properties2.cp_id = tbl_clientpaymenthistory.cp_id
                WHERE
                    tbl_clientpaymenthistory.active = 1
                AND    
                    tbl_clientpaymenthistory.cp_id = :id
            ";
  
            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id' => $cpID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }

    // // incase of imergency allowSeparatePhase
    // public function getPropertyDetails($cp_id)
    // {
    //     try {
    //         $sql="
    //             select
    //                 tbl_propertyparent.propertyName as 'propName',
    //                 tbl_propertylist.phaseNumber as 'ph',
    //                 tbl_propertylist.block as 'blk',
    //                 tbl_propertylist.lot as 'lt'
    //             from 
    //                 tbl_propertyparent, tbl_propertylist, tbl_client_properties
    //             where
    //                 tbl_client_properties.property_id = tbl_propertylist.property_id
    //             and 
    //                 tbl_propertylist.propertyParentID = tbl_propertyparent.id
    //             and 
    //                 tbl_client_properties.cp_id = :cpID
    //         ";

    //         $statement = $this->db->prepare($sql);
    //         $statement->execute([
    //             'cpID' => $cp_id
    //             ]);
    //         $statement->setFetchMode(PDO::FETCH_ASSOC);
    //         return $statement->fetchAll();

    //     } catch (PDOException $e) {
    //         return $e;
    //     }
    // }

    // public function getPropList()
    // {
    //      try {
    //         $sql="
    //             select
    //                 *
    //             from 
    //                 commisionrelease
    //         ";

    //         $statement = $this->db->prepare($sql);
    //         $statement->execute();
    //         $statement->setFetchMode(PDO::FETCH_ASSOC);
    //         return $statement->fetchAll();
    // }

    // public function getReleaseDetail($cp_id, $dateFrom, $dateTo, $searchID)
    // {
    //    try {
    //         $sql="
    //             SELECT
    //                  concat(tbl_client.fname, ' ', tbl_client.mname, ' ', tbl_client.lname) as '0',
    //                  commisionrelease.propertyname as '1',
    //                  concat(tbl_agent.fname, ' ', tbl_agent.lname) as '2',
    //                  tbl_commissions.percentagevalue as '3',
    //                  commisionrelease.amount as '4',
    //                  commisionrelease.ReleaseDate as '5'
    //             FROM    
    //                  commisionrelease
    //             inner join
    //                  tbl_client on tbl_client.client_id = commisionrelease.client_id
    //             inner join
    //                  tbl_agent on tbl_agent.agent_id = commisionrelease.agentID
    //             inner join
    //                  tbl_commissions on tbl_commissions.agentID = commisionrelease.agentID
    //             WHERE
    //                  tbl_commissions.cp_id = commisionrelease.cp_id
    //             and
    //                 commisionrelease.ReleaseDate BETWEEN :dateFrom and :dateTo
    //             and
    //                 commisionrelease.agent_id = commisionrelease.agentID 
    //             and 
    //                 commisionrelease.cp_id = :cp_id
    //         ";
 
    //         $statement = $this->db->prepare($sql);
    //         $statement->execute([
    //             'dateFrom' => $dateFrom,
    //             'dateTo'   => $dateTo,
    //             'searchID' => $searchID,
    //             'cp_id'    => $cp_id
    //             ]);
    //         $statement->setFetchMode(PDO::FETCH_ASSOC);
    //         return $statement->fetchAll();

    //     } catch (PDOException $e) {
    //         return $e;
    //     }
    // }

// ===============================================================================
    public function getReleaseComDate2($dateFrom, $dateTo, $searchID)
    {
        try {
            $sql="
                SELECT
                     concat(tbl_client.fname, ' ', tbl_client.mname, ' ', tbl_client.lname) as '0',
                     commisionrelease.propertyname as '1',
                     concat(tbl_agent.fname, ' ', tbl_agent.lname) as '2',
                     tbl_commissions.percentagevalue as '3',
                     commisionrelease.amount as '4',
                     -- commisionrelease.ReleaseDate as '5'
                     IF(tbl_agent.agentPosition = 1,'Sales Director Head', IF(tbl_agent.agentPosition = 2,'Sales Director', IF(tbl_agent.agentPosition = 3,'Unit Manager', IF(tbl_agent.agentPosition = 4, 'Manager', 'Agent'))) ) as '5'
                 FROM    
                     commisionrelease
                 inner join
                     tbl_client on tbl_client.client_id = commisionrelease.client_id
                 inner join
                     tbl_agent on tbl_agent.agent_id = commisionrelease.agentID
                 inner join
                     tbl_commissions on tbl_commissions.agentID = commisionrelease.agentID
                 WHERE
                     tbl_commissions.cp_id = commisionrelease.cp_id
                 and
                    commisionrelease.ReleaseDate BETWEEN :dateFrom and :dateTo
                 and
                    tbl_agent.agentPosition = :searchID
            ";
 
            $statement = $this->db->prepare($sql);
            $statement->execute([
                'dateFrom' => $dateFrom,
                'dateTo' => $dateTo,
                'searchID' => $searchID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getReleaseComDate($dateFrom, $dateTo){
        try {
            $sql="

                SELECT
                    concat(tbl_client.fname, ' ', tbl_client.mname, ' ', tbl_client.lname) as '0',
                    commisionrelease.propertyname as '1',
                    concat(tbl_agent.fname, ' ', tbl_agent.lname) as '2',
                    tbl_commissions.percentagevalue as '3',
                    commisionrelease.amount as '4',
                    -- commisionrelease.ReleaseDate as '5'
                    IF(tbl_agent.agentPosition = 1,'Sales Director Head', IF(tbl_agent.agentPosition = 2,'Sales Director', IF(tbl_agent.agentPosition = 3,'Unit Manager',  IF(tbl_agent.agentPosition = 4,'Manager', 'Agent'))) ) as '5'
                FROM    
                    commisionrelease
                inner join
                    tbl_client on tbl_client.client_id = commisionrelease.client_id
                inner join
                    tbl_agent on tbl_agent.agent_id = commisionrelease.agentID
                inner join
                    tbl_commissions on tbl_commissions.agentID = commisionrelease.agentID
                WHERE
                    tbl_commissions.cp_id = commisionrelease.cp_id
                and
                   commisionrelease.ReleaseDate BETWEEN :dateFrom and :dateTo
                and
                    tbl_agent.agent_id = commisionrelease.agentID 
            ";
 
            $statement = $this->db->prepare($sql);
            $statement->execute([
                'dateFrom' => $dateFrom,
                'dateTo' => $dateTo
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    } 
        // public function getReleaseComDate($dateFrom, $dateTo){
    //     try {
    //         $sql="
    //             SELECT  
    //                 concat(tbl_agent.Fname, ' ', tbl_agent.Mname, ' ', tbl_agent.Lname) as '0',
    //                 commisionrelease.propertyName as '1',
    //                 commisionrelease.contractPrice as '2',
    //                 commisionrelease.amount as '3',
    //                 commisionrelease.ReleaseDate as '4'
    //             FROM    
    //                 tbl_agent,
    //                 commisionrelease
    //             WHERE
    //                 commisionrelease.ReleaseDate BETWEEN :dateFrom and :dateTo
    //             and
    //                 tbl_agent.agent_id = commisionrelease.agentID 
    //         ";
 
    //         $statement = $this->db->prepare($sql);
    //         $statement->execute([
    //             'dateFrom' => $dateFrom,
    //             'dateTo' => $dateTo
    //             ]);
    //         $statement->setFetchMode(PDO::FETCH_ASSOC);
    //         return $statement->fetchAll();

    //     } catch (PDOException $e) {
    //         return $e;
    //     }
    // }

    public function getPropertisCom($cp_id)
    {
         try {
             $sql="
                select 
                    tbl_propertyparent.propertyName as 'propertyname',
                    tbl_propertylist.phaseNumber as 'ph',
                    tbl_propertylist.block as 'blk',
                    tbl_propertylist.lot as 'lt',
                    tbl_propertylist.originalPrice as 'tcp',
                    tbl_client_properties.commissionReleased as 'crs',
                    tbl_client_properties.adcom as 'addcom'
                from
                    tbl_propertyparent,
                    tbl_propertylist,
                    tbl_client_properties
                where 
                    tbl_client_properties.property_id = tbl_propertylist.property_id
                and 
                    tbl_propertylist.propertyParentID = tbl_propertyparent.id
                and 
                    tbl_client_properties.cp_id = :cpID
                
            ";
 
          $statement = $this->db->prepare($sql);
          $statement->execute([
                'cpID' => $cp_id
            ]);
          $statement->setFetchMode(PDO::FETCH_ASSOC);
          return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getPropertisComBySelectedProp($cp_id, $propID)
    {
       
        try {
             $sql="
                select 
                    tbl_propertyparent.propertyName as 'propertyname',
                    tbl_propertylist.phaseNumber as 'ph',
                    tbl_propertylist.block as 'blk',
                    tbl_propertylist.lot as 'lt',
                    tbl_propertylist.originalPrice as 'tcp',
                    tbl_client_properties.commissionReleased as 'crs',
                    tbl_client_properties.adcom as 'addcom'
                from
                    tbl_propertyparent,
                    tbl_propertylist,
                    tbl_client_properties
                where 
                    tbl_client_properties.property_id = tbl_propertylist.property_id
                and 
                    tbl_propertylist.propertyParentID = tbl_propertyparent.id
                and 
                    tbl_client_properties.cp_id = :cpID
                and 
                    tbl_propertyparent.id = :propID
                
            ";
 
          $statement = $this->db->prepare($sql);
          $statement->execute([
                'cpID'      => $cp_id,
                'propID'    => $propID
            ]);
          $statement->setFetchMode(PDO::FETCH_ASSOC);
          return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getclientComReleasev2()
    {
        try {
             $sql="
                SELECT
                    concat(tbl_client.fname, ' ', tbl_client.mname, ' ', tbl_client.Lname) as '0',
                    commisionrelease.propertyname as '1',
                    concat(tbl_agent.fname, ' ', tbl_agent.lname) as '2',
                    tbl_commissions.percentagevalue as '3',
                    commisionrelease.amount as '4',
                    -- commisionrelease.ReleaseDate as '5'
                    IF(tbl_agent.agentPosition = 1,'Sales Director Head', IF(tbl_agent.agentPosition = 2,'Sales Director', IF(tbl_agent.agentPosition = 3,'Unit Manager', IF(tbl_agent.agentPosition = 4,'Manager','Agent'))) ) as '5'
                FROM    
                    commisionrelease
                inner join
                    tbl_client on tbl_client.client_id = commisionrelease.client_id
                inner join
                    tbl_agent on tbl_agent.agent_id = commisionrelease.agentID
                inner join
                    tbl_commissions on tbl_commissions.agentID = commisionrelease.agentID
                WHERE
                    tbl_commissions.cp_id = commisionrelease.cp_id
                ORDER BY commisionrelease.id DESC
            ";
 
          $statement = $this->db->prepare($sql);
          $statement->execute();
          $statement->setFetchMode(PDO::FETCH_ASSOC);
          return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getCommisisonDetails()
    {
         try {
             $sql="
                SELECT
                    concat(tbl_client.fname, ' ', tbl_client.mname, ' ', tbl_client.Lname) as 'clientName',
                    commisionrelease.propertyname as '1',
                    commisionrelease.cp_id as 'cp_id', 
                    commisionrelease.agentID as 'agentId',
                    concat(tbl_agent.fname, ' ', tbl_agent.lname) as 'agentName',
                    tbl_commissions.percentagevalue as 'precentageValue',
                    commisionrelease.payment as 'payment',
                    commisionrelease.amount as 'Amount',
                    commisionrelease.ReleaseDate as 'ReleaseDate',
                    commisionrelease.payment as 'payment',
                    IF(tbl_agent.agentPosition = 1,'Sales Director Head', IF(tbl_agent.agentPosition = 2,'Sales Director', IF(tbl_agent.agentPosition = 3,'Unit Manager',IF(tbl_agent.agentPosition = 4,'Manager','Agent'))) ) as 'position'
                FROM    
                    commisionrelease
                inner join
                    tbl_client on tbl_client.client_id = commisionrelease.client_id
                inner join
                    tbl_agent on tbl_agent.agent_id = commisionrelease.agentID
                inner join
                    tbl_commissions on tbl_commissions.agentID = commisionrelease.agentID
                WHERE
                    tbl_commissions.cp_id = commisionrelease.cp_id
                ORDER BY commisionrelease.id DESC
            ";
 
          $statement = $this->db->prepare($sql);
          $statement->execute();
          $statement->setFetchMode(PDO::FETCH_ASSOC);
          return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getCommisisonDetailsByAgentPos($pos)
    {
      
        try {
             $sql="
                SELECT
                    concat(tbl_client.fname, ' ', tbl_client.mname, ' ', tbl_client.lname) as 'clientName',
                    commisionrelease.propertyname as '1',
                    commisionrelease.cp_id as 'cp_id', 
                    commisionrelease.agentID as 'agentId',
                    concat(tbl_agent.fname, ' ', tbl_agent.lname) as 'agentName',
                    tbl_commissions.percentagevalue as 'precentageValue',
                    commisionrelease.amount as 'Amount',
                    commisionrelease.payment as 'payment',
                    commisionrelease.ReleaseDate as 'ReleaseDate',
                    IF(tbl_agent.agentPosition = 1,'Sales Director Head', IF(tbl_agent.agentPosition = 2,'Sales Director', IF(tbl_agent.agentPosition = 3,'Unit Manager', IF(tbl_agent.agentPosition = 4,'Manager','Agent'))) ) as 'position'
                FROM    
                    commisionrelease
                inner join
                    tbl_client on tbl_client.client_id = commisionrelease.client_id
                inner join
                    tbl_agent on tbl_agent.agent_id = commisionrelease.agentID
                inner join
                    tbl_commissions on tbl_commissions.agentID = commisionrelease.agentID
                WHERE
                    tbl_commissions.cp_id = commisionrelease.cp_id
                and 
                    tbl_agent.agentPosition = :pos
                ORDER BY commisionrelease.id DESC
            ";
 
          $statement = $this->db->prepare($sql);
          $statement->execute([
                'pos' => $pos
          ]);
          $statement->setFetchMode(PDO::FETCH_ASSOC);
          return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getComDetaoilsByDate($pos, $date1, $date2)
    {
        try {
             $sql="
                SELECT
                    concat(tbl_client.fname, ' ', tbl_client.mname, ' ', tbl_client.lname) as 'clientName',
                    commisionrelease.propertyname as '1',
                    commisionrelease.cp_id as 'cp_id', 
                    commisionrelease.agentID as 'agentId',
                    concat(tbl_agent.fname, ' ', tbl_agent.lname) as 'agentName',
                    tbl_commissions.percentagevalue as 'precentageValue',
                    commisionrelease.amount as 'Amount',
                    IF(tbl_agent.agentPosition = 1,'Sales Director Head', IF(tbl_agent.agentPosition = 2,'Sales Director', IF(tbl_agent.agentPosition = 3,'Unit Manager', IF(tbl_agent.agentPosition = 4, 'Manager','Agent'))) ) as 'position'
                FROM    
                    commisionrelease
                inner join
                    tbl_client on tbl_client.client_id = commisionrelease.client_id
                inner join
                    tbl_agent on tbl_agent.agent_id = commisionrelease.agentID
                inner join
                    tbl_commissions on tbl_commissions.agentID = commisionrelease.agentID
                WHERE
                    tbl_commissions.cp_id = commisionrelease.cp_id
                and 
                    tbl_agent.agentPosition = :pos
                and 
                    commisionrelease.ReleaseDate BETWEEN :date1 AND :date2
                ORDER BY commisionrelease.id DESC
            ";
 
          $statement = $this->db->prepare($sql);
          $statement->execute([
                'pos'   => $pos,
                'date1' => $date1,
                'date2' => $date2
          ]);
          $statement->setFetchMode(PDO::FETCH_ASSOC);
          return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }   
    }


    public function getComDetailsbyDateonly($date1, $date2)
    {
        
        try {
             $sql="
                SELECT
                    concat(tbl_client.fname, ' ', tbl_client.mname, ' ', tbl_client.lname) as 'clientName',
                    commisionrelease.propertyname as '1',
                    commisionrelease.cp_id as 'cp_id', 
                    commisionrelease.agentID as 'agentId',
                    concat(tbl_agent.fname, ' ', tbl_agent.lname) as 'agentName',
                    tbl_commissions.percentagevalue as 'precentageValue',
                    commisionrelease.amount as 'Amount',
                    IF(tbl_agent.agentPosition = 1,'Sales Director Head', IF(tbl_agent.agentPosition = 2,'Sales Director', IF(tbl_agent.agentPosition = 3,'Unit Manager', IF(tbl_agent.agentPosition = 4,'Manager','Agent'))) ) as 'position'
                FROM    
                    commisionrelease
                inner join
                    tbl_client on tbl_client.client_id = commisionrelease.client_id
                inner join
                    tbl_agent on tbl_agent.agent_id = commisionrelease.agentID
                inner join
                    tbl_commissions on tbl_commissions.agentID = commisionrelease.agentID
                WHERE
                    tbl_commissions.cp_id = commisionrelease.cp_id
                and 
                    commisionrelease.ReleaseDate BETWEEN :date1 AND :date2
                ORDER BY commisionrelease.id DESC
            ";
 
          $statement = $this->db->prepare($sql);
          $statement->execute([
                'date1' => $date1,
                'date2' => $date2
          ]);
          $statement->setFetchMode(PDO::FETCH_ASSOC);
          return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }   
    }

    public function getclientComRelease(){
        try {

            $sql="
                SELECT
                    concat(tbl_agent.Fname, ' ', tbl_agent.Mname, ' ', tbl_agent.Lname) as '0',
                    commisionrelease.propertyName as '1',
                    commisionrelease.contractPrice as '2',
                    commisionrelease.amount as '3',
                    commisionrelease.ReleaseDate as '4'
                FROM    
                    tbl_agent,
                    commisionrelease
                WHERE
                    tbl_agent.agent_id = commisionrelease.agentID
                ORDER BY commisionrelease.id DESC
            ";
 
          $statement = $this->db->prepare($sql);
          $statement->execute();
          $statement->setFetchMode(PDO::FETCH_ASSOC);
          return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }


    public function isORnumberused($ornumber){
        try {
            $sql = "
                SELECT 
                    ornumber
                FROM 
                    tbl_clientpaymenthistory 
                WHERE 
                    tbl_clientpaymenthistory.ornumber = :ornumber
                AND 
                    active = 1
            ";
 
              $statement = $this->db->prepare($sql);

              $statement->execute(['ornumber' => $ornumber]);
              $statement->setFetchMode(PDO::FETCH_ASSOC);
     
              return $statement->fetchAll();

            } catch (PDOException $e) {
                return $e;
            }
    }

    // get propertylistToComRelease
    public function commisionreleaseDetails()
    {
        try {
            $sql ="
                SELECT
                    * 
                from 
                    commisionrelease
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }


    // get listed distinct properties
    public function getDistinctProperty()
    {
        try {
            $sql="
                SELECT
                    distinct
                    tbl_propertyparent.id as 'parentID',
                    tbl_propertyparent.propertyName as 'Property Name'
                FROM
                    tbl_propertyparent, tbl_propertylist, tbl_client_properties
                where 
                    tbl_propertyparent.id = tbl_propertylist.propertyParentID 
                and 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_client_properties.active = 1
                and
                    tbl_client_properties.fullyPaid = 0
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getPropertiesGues($userID)
    {
        try {

            $sql = "select property from tbl_businesspartner where id = any (select refID from tbl_users where UserID =:id)";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id' => $userID
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    // get listed distinct properties
    public function getDistinctPropertyGuest($list)
    {
        try {
            $sql="
                SELECT
                    distinct
                    tbl_propertyparent.id as 'parentID',
                    tbl_propertyparent.propertyName as 'Property Name'
                FROM
                    tbl_propertyparent, tbl_propertylist, tbl_client_properties
                where 
                    tbl_propertyparent.id = tbl_propertylist.propertyParentID 
                and 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_client_properties.active = 1
                and 
                    tbl_propertylist.propertyParentID in (". implode(',', $list) .")
                and
                    tbl_client_properties.fullyPaid = 0
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    // get commissionlist accourding to commission release cp_id
    public function getPropertyParent($cp_id)
    {
        try {
            $sql="
                SELECT
                    tbl_client_properties.property_id as 'Propert ID',
                    tbl_client.client_id as 'client ID',
                    tbl_client_properties.cp_id as 'cpID',
                    concat(tbl_client.Fname,' ', tbl_client.Mname,' ', tbl_client.Lname) as 'Buyer',
                    tbl_propertyparent.propertyName as 'Property Name', 
                    tbl_propertylist.phaseNumber as 'P', 
                    tbl_propertylist.block as 'Blk', 
                    tbl_propertylist.lot as 'Lt',
                    tbl_propertylist.originalPrice as 'TCP',
                    tbl_client_properties.adCom as 'addCom',
                    tbl_client_properties.commissionReleased as 'CRS',
                    tbl_propertyparent.id as 'parentID'
                from 
                    tbl_client_properties,
                    tbl_propertyparent, 
                    tbl_propertylist ,
                    tbl_client
                where 
                    tbl_propertyparent.id = tbl_propertylist.propertyParentID 
                and 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_client.client_id = tbl_client_properties.client_id
                and 
                    tbl_client_properties.active = 1
                and
                    tbl_client_properties.cp_id = :cpid
            ";

        } catch (PDOException $e) {
            
            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'cpid' => $cp_id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        }
    }

    //used in report template
    public function getClientProperties(){
        try {
            $sql="
                SELECT
                    tbl_client_properties.property_id as 'Propert ID',
                    tbl_client.client_id as 'client ID',
                    tbl_client_properties.cp_id as 'cpID',
                    concat(tbl_client.Fname,' ', tbl_client.Mname,' ', tbl_client.Lname) as 'Buyer',
                    tbl_propertyparent.propertyName as 'Property Name', 
                    tbl_propertylist.phaseNumber as 'P', 
                    tbl_propertylist.block as 'Blk', 
                    tbl_propertylist.lot as 'Lt',
                    tbl_propertylist.originalPrice as 'TCP',
                    tbl_client_properties.adCom as 'addCom',
                    tbl_client_properties.commissionReleased as 'CRS',
                    tbl_propertyparent.id as 'parentID'
                from 
                    tbl_client_properties,
                    tbl_propertyparent, 
                    tbl_propertylist ,
                    tbl_client
                where 
                    tbl_propertyparent.id = tbl_propertylist.propertyParentID 
                and 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_client.client_id = tbl_client_properties.client_id
                and 
                    tbl_client_properties.active = 1
                and
                    tbl_client_properties.fullyPaid = 0
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }

    }

    public function getClientPropertiesCom3byDate($cp_id, $dateFrom, $dateTo)
    {
        try {
            $sql="
                SELECT
                    tbl_clientpaymenthistory.id,
                    (tbl_clientpaymenthistory.debit + tbl_clientpaymenthistory.credit) as 'Amount',
                    tbl_clientpaymenthistory.dateadded as 'PaymentDate',
                    tbl_client_properties.property_id as 'Propert ID',
                    tbl_client.client_id as 'client ID',
                    tbl_client_properties.cp_id as 'cpID',
                    concat(tbl_client.Fname,' ', tbl_client.Mname,' ', tbl_client.Lname) as 'Buyer',
                    tbl_propertyparent.propertyName as 'Property Name', 
                    tbl_propertylist.phaseNumber as 'P', 
                    tbl_propertylist.block as 'Blk', 
                    tbl_propertylist.lot as 'Lt',
                    tbl_propertylist.originalPrice as 'TCP',
                    tbl_client_properties.adCom as 'addCom',
                    tbl_client_properties.commissionReleased as 'CRS',
                    tbl_propertyparent.id as 'parentID',
                    (tbl_clientpaymenthistory.debit + tbl_clientpaymenthistory.credit) as 'payment'
                from 
                    tbl_client_properties,
                    tbl_propertyparent, 
                    tbl_propertylist,
                    tbl_client,
                    tbl_clientpaymenthistory
                where 
                    tbl_propertyparent.id = tbl_propertylist.propertyParentID 
                and 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_client.client_id = tbl_client_properties.client_id
                and 
                    tbl_clientpaymenthistory.cp_id = tbl_client_properties.cp_id
                and
                    tbl_clientpaymenthistory.releseStat = 0
                and
                    tbl_client_properties.active = 1
                and
                    tbl_client_properties.fullyPaid = 0
                and 
                    tbl_clientpaymenthistory.particulars = ''
                and
                    tbl_clientpaymenthistory.dateAdded BETWEEN :date1 AND :date2
                and
                    tbl_clientpaymenthistory.cp_id = :parentID
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'parentID' => $cp_id, 
                    'date1'     => $dateFrom,
                    'date2'      => $dateTo
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }  
    } 

    public function getClientPropertiesCom3($cp_id)
    {
         try {
            $sql="
                SELECT
                    tbl_clientpaymenthistory.id,
                    (tbl_clientpaymenthistory.debit + tbl_clientpaymenthistory.credit) as 'Amount',
                    tbl_clientpaymenthistory.dateadded as 'PaymentDate',
                    tbl_client_properties.property_id as 'Propert ID',
                    tbl_client.client_id as 'client ID',
                    tbl_client_properties.cp_id as 'cpID',
                    concat(tbl_client.Fname,' ', tbl_client.Mname,' ', tbl_client.Lname) as 'Buyer',
                    tbl_propertyparent.propertyName as 'Property Name', 
                    tbl_propertylist.phaseNumber as 'P', 
                    tbl_propertylist.block as 'Blk', 
                    tbl_propertylist.lot as 'Lt',
                    tbl_propertylist.originalPrice as 'TCP',
                    tbl_client_properties.adCom as 'addCom',
                    tbl_client_properties.commissionReleased as 'CRS',
                    tbl_propertyparent.id as 'parentID',
                    (tbl_clientpaymenthistory.debit + tbl_clientpaymenthistory.credit) as 'payment'
                from 
                    tbl_client_properties,
                    tbl_propertyparent, 
                    tbl_propertylist,
                    tbl_client,
                    tbl_clientpaymenthistory
                where 
                    tbl_propertyparent.id = tbl_propertylist.propertyParentID 
                and 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_client.client_id = tbl_client_properties.client_id
                and 
                    tbl_clientpaymenthistory.cp_id = tbl_client_properties.cp_id
                and
                    tbl_clientpaymenthistory.releseStat = 0
                and
                    tbl_client_properties.active = 1
                and
                    tbl_client_properties.fullyPaid = 0
                and
                    tbl_clientpaymenthistory.cp_id = :parentID
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'parentID' => $cp_id
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }  
    }

      public function getClientPropertiesComWithDateOne($dateFrom, $dateTo)
    {
        try {
            $sql="
                SELECT
                    tbl_clientpaymenthistory.id,
                    (tbl_clientpaymenthistory.debit + tbl_clientpaymenthistory.credit) as 'Amount',
                    tbl_clientpaymenthistory.dateadded as 'PaymentDate',
                    tbl_client_properties.property_id as 'Propert ID',
                    tbl_client.client_id as 'client ID',
                    tbl_client_properties.cp_id as 'cpID',
                    concat(tbl_client.Fname,' ', tbl_client.Mname,' ', tbl_client.Lname) as 'Buyer',
                    tbl_propertyparent.propertyName as 'Property Name', 
                    tbl_propertylist.phaseNumber as 'P', 
                    tbl_propertylist.block as 'Blk', 
                    tbl_propertylist.lot as 'Lt',
                    tbl_propertylist.originalPrice as 'TCP',
                    tbl_client_properties.adCom as 'addCom',
                    tbl_client_properties.commissionReleased as 'CRS',
                    tbl_propertyparent.id as 'parentID',
                    (tbl_clientpaymenthistory.debit + tbl_clientpaymenthistory.credit) as 'payment'
                from 
                    tbl_client_properties,
                    tbl_propertyparent, 
                    tbl_propertylist,
                    tbl_client,
                    tbl_clientpaymenthistory
                where 
                    tbl_propertyparent.id = tbl_propertylist.propertyParentID 
                and 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_client.client_id = tbl_client_properties.client_id
                and 
                    tbl_clientpaymenthistory.cp_id = tbl_client_properties.cp_id
                and
                    tbl_clientpaymenthistory.releseStat = 0
                and
                    tbl_client_properties.active = 1
                and
                    tbl_client_properties.fullyPaid = 0
                and 
                    tbl_clientpaymenthistory.particulars = ''
                and
                    tbl_clientpaymenthistory.dateAdded BETWEEN :date1 AND :date2

            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'date1' => $dateFrom,
                    'date2' => $dateTo
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }        
    }


    public function getClientPropertiesComWithDateTwo($dateFrom, $dateTo, $parentID)
    {
        try {
            $sql="
                SELECT
                    tbl_clientpaymenthistory.id,
                    (tbl_clientpaymenthistory.debit + tbl_clientpaymenthistory.credit) as 'Amount',
                    tbl_clientpaymenthistory.dateadded as 'PaymentDate',
                    tbl_client_properties.property_id as 'Propert ID',
                    tbl_client.client_id as 'client ID',
                    tbl_client_properties.cp_id as 'cpID',
                    concat(tbl_client.Fname,' ', tbl_client.Mname,' ', tbl_client.Lname) as 'Buyer',
                    tbl_propertyparent.propertyName as 'Property Name', 
                    tbl_propertylist.phaseNumber as 'P', 
                    tbl_propertylist.block as 'Blk', 
                    tbl_propertylist.lot as 'Lt',
                    tbl_propertylist.originalPrice as 'TCP',
                    tbl_client_properties.adCom as 'addCom',
                    tbl_client_properties.commissionReleased as 'CRS',
                    tbl_propertyparent.id as 'parentID',
                    (tbl_clientpaymenthistory.debit + tbl_clientpaymenthistory.credit) as 'payment'
                from 
                    tbl_client_properties,
                    tbl_propertyparent, 
                    tbl_propertylist,
                    tbl_client,
                    tbl_clientpaymenthistory
                where 
                    tbl_propertyparent.id = tbl_propertylist.propertyParentID 
                and 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_client.client_id = tbl_client_properties.client_id
                and 
                    tbl_clientpaymenthistory.cp_id = tbl_client_properties.cp_id
                and
                    tbl_clientpaymenthistory.releseStat = 0
                and
                    tbl_client_properties.active = 1
                and
                    tbl_client_properties.fullyPaid = 0
                and 
                    tbl_clientpaymenthistory.particulars = ''
                and
                    tbl_clientpaymenthistory.dateAdded BETWEEN :date1 AND :date2
                and
                    tbl_propertyparent.id = :parentID

            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'parentID' => $parentID,
                    'date1' => $dateFrom,
                    'date2' => $dateTo
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }        
    }

    public function getClientPropertiesCom2($parentID)
    {
        try {
            $sql="
                SELECT
                    tbl_clientpaymenthistory.id,
                    (tbl_clientpaymenthistory.debit + tbl_clientpaymenthistory.credit) as 'Amount',
                    tbl_clientpaymenthistory.dateadded as 'PaymentDate',
                    tbl_client_properties.property_id as 'Propert ID',
                    tbl_client.client_id as 'client ID',
                    tbl_client_properties.cp_id as 'cpID',
                    concat(tbl_client.Fname,' ', tbl_client.Mname,' ', tbl_client.Lname) as 'Buyer',
                    tbl_propertyparent.propertyName as 'Property Name', 
                    tbl_propertylist.phaseNumber as 'P', 
                    tbl_propertylist.block as 'Blk', 
                    tbl_propertylist.lot as 'Lt',
                    tbl_propertylist.originalPrice as 'TCP',
                    tbl_client_properties.adCom as 'addCom',
                    tbl_client_properties.commissionReleased as 'CRS',
                    tbl_propertyparent.id as 'parentID',
                    (tbl_clientpaymenthistory.debit + tbl_clientpaymenthistory.credit) as 'payment'
                from 
                    tbl_client_properties,
                    tbl_propertyparent, 
                    tbl_propertylist,
                    tbl_client,
                    tbl_clientpaymenthistory
                where 
                    tbl_propertyparent.id = tbl_propertylist.propertyParentID 
                and 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_client.client_id = tbl_client_properties.client_id
                and 
                    tbl_clientpaymenthistory.cp_id = tbl_client_properties.cp_id
                and
                    tbl_clientpaymenthistory.releseStat = 0
                and
                    tbl_client_properties.active = 1
                and
                    tbl_client_properties.fullyPaid = 0
                and 
                    tbl_clientpaymenthistory.particulars = ''
                and
                    tbl_propertyparent.id = :parentID

            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'parentID' => $parentID
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }        
    }

    //used in table releasecommission 
    public function getClientPropertiesCom()
    {
        try {
            $sql="
                SELECT
                    tbl_clientpaymenthistory.id,
                    (tbl_clientpaymenthistory.debit + tbl_clientpaymenthistory.credit) as 'Amount',
                    tbl_clientpaymenthistory.dateadded as 'PaymentDate',
                    tbl_client_properties.property_id as 'Propert ID',
                    tbl_client.client_id as 'client ID',
                    tbl_client_properties.cp_id as 'cpID',
                    concat(tbl_client.Fname,' ', tbl_client.Mname,' ', tbl_client.Lname) as 'Buyer',
                    tbl_propertyparent.propertyName as 'Property Name', 
                    tbl_propertylist.phaseNumber as 'P', 
                    tbl_propertylist.block as 'Blk', 
                    tbl_propertylist.lot as 'Lt',
                    tbl_propertylist.originalPrice as 'TCP',
                    tbl_client_properties.adCom as 'addCom',
                    tbl_client_properties.commissionReleased as 'CRS',
                    tbl_propertyparent.id as 'parentID',
                    (tbl_clientpaymenthistory.debit + tbl_clientpaymenthistory.credit) as 'payment'
                from 
                    tbl_client_properties,
                    tbl_propertyparent, 
                    tbl_propertylist,
                    tbl_client,
                    tbl_clientpaymenthistory
                where 
                    tbl_propertyparent.id = tbl_propertylist.propertyParentID 
                and 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_client.client_id = tbl_client_properties.client_id
                and 
                    tbl_clientpaymenthistory.cp_id = tbl_client_properties.cp_id
                and
                    tbl_clientpaymenthistory.releseStat = 0
                and
                    tbl_client_properties.active = 1
                and
                    tbl_client_properties.fullyPaid = 0
                and 
                    tbl_clientpaymenthistory.particulars = ''
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }        
    }

    public function getAgent1($cpid){
        try {
            $sql="
               SELECT
                    concat(tbl_agent.fname, ' ', tbl_agent.lname) as 'agentName',
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Agent 1',
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Com',
                    tbl_client_properties.agent_id, 
                    tbl_agent.agentPosition,
                    concat(tbl_commissions.percentagevalue,'% ') as 'percentageVal' 
                from 
                    tbl_propertylist, 
                    tbl_agent, 
                    tbl_commissions, 
                    tbl_client_properties 
                where 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_agent.agent_id = tbl_commissions.agentID 
                and 
                    tbl_agent.agentPosition = 2 
                and 
                    tbl_client_properties.cp_id = tbl_commissions.cp_id
                and 
                    tbl_client_properties.cp_id = :id
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id'  => $cpid
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }

    }

    public function getAgent2($cpid){
        try {
            $sql="
                SELECT
                    concat(tbl_agent.fname, ' ', tbl_agent.lname) as 'agentName', 
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Agent 2',
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Com',
                    tbl_client_properties.agent_id, 
                    tbl_agent.agentPosition,
                    concat(tbl_commissions.percentagevalue,'% ') as 'percentageVal' 
                from 
                    tbl_propertylist, 
                    tbl_agent, 
                    tbl_commissions, 
                    tbl_client_properties 
                where 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_agent.agent_id = tbl_commissions.agentID 
                and 
                    tbl_agent.agentPosition = 3 
                and 
                    tbl_client_properties.cp_id = tbl_commissions.cp_id
                and 
                    tbl_client_properties.cp_id = :id
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id'  => $cpid
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAgent3($cpid){
        try {
            $sql="
                SELECT
                    concat(tbl_agent.fname, ' ', tbl_agent.lname) as 'agentName', 
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Agent 3',
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Com',
                    tbl_client_properties.agent_id, 
                    tbl_agent.agentPosition,
                    concat(tbl_commissions.percentagevalue,'% ') as 'percentageVal'
                from 
                    tbl_propertylist, 
                    tbl_agent, 
                    tbl_commissions, 
                    tbl_client_properties 
                where 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_agent.agent_id = tbl_commissions.agentID 
                and 
                    tbl_agent.agentPosition = 4 
                and 
                    tbl_client_properties.cp_id = tbl_commissions.cp_id
                and 
                    tbl_client_properties.cp_id = :id

            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id'  => $cpid
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }

    }

     public function getAgent4($cpid){
        try {
            $sql="
                SELECT
                    concat(tbl_agent.fname, ' ', tbl_agent.lname) as 'agentName', 
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Agent 4',
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Com',
                    tbl_client_properties.agent_id, 
                    tbl_agent.agentPosition,
                    concat(tbl_commissions.percentagevalue,'% ') as 'percentageVal'
                from 
                    tbl_propertylist, 
                    tbl_agent, 
                    tbl_commissions, 
                    tbl_client_properties 
                where 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_agent.agent_id = tbl_commissions.agentID 
                and 
                    tbl_agent.agentPosition = 4 
                and 
                    tbl_client_properties.cp_id = tbl_commissions.cp_id
                and 
                    tbl_client_properties.cp_id = :id

            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id'  => $cpid
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }

    }

    public function getSD($cpid){
        try {
            $sql="
                 SELECT 
                    concat(tbl_agent.fname, ' ', tbl_agent.lname) as 'agentName',
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'SD',
                    (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) - (((tbl_propertylist.originalPrice * (tbl_commissions.percentagevalue / 100)) / tbl_client_properties.commissionReleased) * 0.05)) as 'Com',
                    tbl_client_properties.agent_id, 
                    tbl_agent.agentPosition,
                    concat(tbl_commissions.percentagevalue,'% ') as 'percentageVal'

                from 
                    tbl_propertylist, 
                    tbl_agent, 
                    tbl_commissions, 
                    tbl_client_properties 
                where 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_agent.agent_id = tbl_commissions.agentID 
                and 
                    tbl_agent.agentPosition = 1 
                and 
                    tbl_client_properties.cp_id = tbl_commissions.cp_id
                and 
                    tbl_client_properties.cp_id = :id
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id'  => $cpid
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }

    }


    public function getClientPaymentHistory(){
        try {
            $sql = "
                SELECT 
                    distinct tbl_clientpaymenthistory.id as '0',
                    concat(tbl_client.fname, ' ', tbl_client.mname, ' ', tbl_client.fname) as '1',
                    tbl_propertyparent.propertyName as '2',
                    tbl_clientpaymenthistory.particulars as '3',
                    tbl_clientpaymenthistory.debit as '4',
                    tbl_clientpaymenthistory.credit as '5',
                    tbl_clientpaymenthistory.dateadded as '6' 
                FROM 
                    tbl_clientpaymenthistory,
                    tbl_client_properties,
                    tbl_client, 
                    tbl_propertyparent 
                WHERE 
                    tbl_clientpaymenthistory.cp_id = tbl_client.client_id
                and 
                    tbl_clientpaymenthistory.active = 1
            ";
 
              $statement = $this->db->prepare($sql);

              $statement->execute();
              $statement->setFetchMode(PDO::FETCH_ASSOC);
     
              return $statement->fetchAll();

            } catch (PDOException $e) {
                return $e;
            }
    }



    public function getSummary(){
        try {
            
            $sql ="
                SELECT
                    DISTINCT tbl_agent.agent_id,
                    tbl_agent.accountNumber,
                    concat(tbl_agent.Fname, ' ', tbl_agent.Mname, ' ', tbl_agent.Lname) as 'name'
                FROM
                    tbl_agent,
                    commisionrelease
                WHERE
                    tbl_agent.agent_id = commisionrelease.agentID
            ";
              $statement = $this->db->prepare($sql);
              $statement->execute();
              $statement->setFetchMode(PDO::FETCH_ASSOC);
              return $statement->fetchAll();
        } catch (Exception $e) {
            
        }
    }

    public function agentInfo($agentId){
        try {
            $sql="
                SELECT
                    SUM(commisionrelease.amount) as 'Amount'
                FROM
                    commisionrelease
                WHERE
                    commisionrelease.agentID = :id 
            ";
              $statement = $this->db->prepare($sql);
              $statement->execute([
                    'id' => $agentId
                ]);
              $statement->setFetchMode(PDO::FETCH_ASSOC);
              return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function agentInfoWithDate($agentID, $date1, $date2){
        try {
            $sql="
                SELECT
                    SUM(commisionrelease.amount) as 'Amount'
                FROM
                    commisionrelease
                WHERE
                    commisionrelease.agentID = :id
                and 
                    commisionrelease.ReleaseDate BETWEEN :date1 AND :date2;
            ";
              $statement = $this->db->prepare($sql);
              $statement->execute([
                    'id' => $agentID,
                    'date1' => $date1,
                    'date2' => $date2,
                ]);
              $statement->setFetchMode(PDO::FETCH_ASSOC);
              return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAllPaymentHistory(){
        try {
            $sql = "
                SELECT 
                    tbl_clientpaymenthistory.id as '0',
                    tbl_clientpaymenthistory.ornumber as '1',
                    CONCAT(tbl_client.Fname,' ',tbl_client.Lname) as '2',
                    tbl_client_properties2.lotarea as '3',
                    DATE_FORMAT(tbl_clientpaymenthistory.dateAdded,'%M %d, %Y') as '4',
                    -- date(tbl_clientpaymenthistory.dateAdded) as '4',
                    (tbl_clientpaymenthistory.debit + tbl_clientpaymenthistory.credit) as '5',
                    IF(tbl_clientpaymenthistory.debit <> 0, tbl_clientpaymenthistory.remittanceCenter, IF(tbl_clientpaymenthistory.credit <> 0,'Cash', IF(tbl_clientpaymenthistory.debit <> 0 AND tbl_clientpaymenthistory.credit <> 0, CONCAT('Cash and Debit  (',tbl_clientpaymenthistory.remittanceCenter,')'),'NONE' )) )  as '6',

                    CONCAT('<button type=\"button\" class=\"btn btn-outline green btn_updatePayment\" data-id=\"',tbl_clientpaymenthistory.id,'\" ><i class=\"icon-pencil\"  ></i></button>

                        <button type=\"button\" class=\"btn btn-outline red btn_DeletePayment\" data-id=\"',tbl_clientpaymenthistory.id,'\" ><i class=\"fa fa-trash\"  ></i> </button>

                        <button type=\"button\" class=\"btn btn-outline blue btn_printThisPayment\" data-address=\"',tbl_client.Address,'\" data-id=\"',tbl_clientpaymenthistory.id,'\" ><i class=\"fa fa-print\"  ></i> </button> ') as '7'

                FROM
                    tbl_clientpaymenthistory
                INNER JOIN
                    (SELECT
                        CONCAT(tbl_propertylist2.propertyName,' ',IF(tbl_propertylist2.phaseNumber = 0,'',CONCAT('Phase ',tbl_propertylist2.phaseNumber)),' Block ',tbl_propertylist2.block,' Lot ',tbl_propertylist2.lot ) as lotarea,
                        tbl_client_properties.sqmPricem2,
                        tbl_propertylist2.sqm,
                        tbl_client_properties.plan_terms,
                        tbl_client_properties.cp_id
                    FROM
                        tbl_client_properties
                    INNER JOIN 
                        (
                            select
                                tbl_propertylist.property_id, 
                                tbl_propertylist.phaseNumber,
                                tbl_propertylist.block,
                                tbl_propertylist.lot,
                                tbl_propertylist.sqm,
                                tbl_propertyparent.propertyName
                            FROM
                                tbl_propertylist
                            INNER JOIN
                                tbl_propertyparent on tbl_propertyparent.id =  tbl_propertylist.propertyParentID
                        ) as tbl_propertylist2 ON tbl_propertylist2.property_id = tbl_client_properties.property_id
                    ) as tbl_client_properties2  on tbl_client_properties2.cp_id = tbl_clientpaymenthistory.cp_id
                INNER JOIN 
                    tbl_client ON tbl_client.client_id = tbl_clientpaymenthistory.client_id
                WHERE
                    tbl_clientpaymenthistory.active = 1

            ";
 
            $statement = $this->db->prepare($sql);

            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getClientPaymentTransactionHistory($orNumber){
        
        try {
            $sql = "
                SELECT 
                    tbl_client_properties.property_id, 
                    tbl_propertyparent.propertyName, 
                    tbl_propertylist.phaseNumber, 
                    tbl_propertylist.block, 
                    tbl_propertylist.lot 
                from 
                    tbl_client_properties, 
                    tbl_propertyparent, 
                    tbl_propertylist 
                where 
                    tbl_propertyparent.id = tbl_propertylist.propertyParentID 
                and 
                    tbl_propertylist.property_id = tbl_client_properties.property_id 
                and 
                    tbl_client_properties.client_id = (select tbl_clientpaymenthistory.client_id 
                    from tbl_clientpaymenthistory where tbl_clientpaymenthistory.id = ' $orNumber ' and tbl_clientpaymenthistory.active = 1)

            ";
 
            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function updateTransactioHistory($orNumberNew, $paymentDate, $orNumberOld, $paymentAmount, $paymentID, $notes){
        try {
            $sql="
                UPDATE 
                    tbl_clientpaymenthistory 
                set 
                    ornumber         = :orNumberNew, 
                    dateadded        = :paymentDate,
                    credit           = :paymentAmount, 
                    debit            = 0,
                    remittanceCenter = '',
                    description      = :paymentDesc
                where 
                    id = :paymentID
                and 
                    active = 1
            ";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
                    'orNumberNew'    => $orNumberNew,
                    'paymentID'      => $paymentID,
                    'paymentAmount'  => $paymentAmount,
                    'paymentDate'    => $paymentDate,
                    'paymentDesc'    => $notes
                ]);

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function updateTransactioHistoryRemitance($orNumberNew, $paymentDate, $orNumberOld, $paymentAmount, $paymentOption, $notes){
        try {
            $sql="
                UPDATE 
                    tbl_clientpaymenthistory 
                set 
                    ornumber = :orNumberNew, dateadded = :paymentDate,
                    debit    = :paymentAmount, credit = 0,
                    remittanceCenter = :paymentOption,
                    description      = :paymentDesc
                where 
                    id = :orNumberOld
                and 
                    active = 1

            ";

            $statement = $this->db->prepare($sql);
            $statement = $this->db->prepare($sql);
            return $statement->execute([
                    'orNumberNew'    => $orNumberNew,
                    'orNumberOld'    => $orNumberOld,
                    'paymentAmount'  => $paymentAmount,
                    'paymentOption'  => $paymentOption,
                    'paymentDate'    => $paymentDate, 
                    'paymentDesc'    => $notes

                ]);

        } catch (PDOException $e) {
            return $e;
        }
    }

    // public function insertThisClientPaymentHistory($paymentornumber,$clientID,$cp_id,$particulars,$remittanceCenter,$debit,$credit,$agentComission,$addedBy,$dateClaimed){

    //     try {
    //         $sql = "
    //             INSERT INTO
    //                 tbl_clientpaymenthistory(
    //                     cp_id,
    //                     client_id,
    //                     particulars,
    //                     ornumber,
    //                     remittanceCenter,
    //                     debit,
    //                     credit,
    //                     agentComission,
    //                     dateAdded,
    //                     addedBy
    //                 )
    //             VALUES(
    //                 :cp_id,
    //                 :client_id,
    //                 :particulars,
    //                 :ornumber,
    //                 :remittanceCenter,
    //                 :debit,
    //                 :credit,
    //                 :agentComission,
    //                 :dateClaimed,
    //                 :addedBy
    //             )
    //         ";

    //         $statement = $this->db->prepare($sql);

    //         return $statement->execute([
    //                 'cp_id'             => $cp_id,
    //                 'client_id'         => $clientID,
    //                 'particulars'       => $particulars,
    //                 'ornumber'          => $paymentornumber,
    //                 'remittanceCenter'  => $remittanceCenter,
    //                 'debit'             => $debit,
    //                 'credit'            => $credit,
    //                 'agentComission'    => $agentComission,
    //                 'addedBy'           => $addedBy,
    //                 'dateClaimed'       => $dateClaimed
    //             ]);


    //     } catch (PDOException $e) {
    //         return $e;
    //     }

    // }

    public function insertThisClientPaymentHistory($cp_id,$clientID,$particulars,$paymentornumber,$remittanceCenter,$debit,$credit,$addedBy,$dateClaimed,$desc){
                                                      
        try {
            $sql = "
                INSERT INTO
                    tbl_clientpaymenthistory(
                        cp_id,
                        client_id,
                        particulars,
                        ornumber,
                        remittanceCenter,
                        debit,
                        credit,
                        dateAdded,
                        addedBy,
                        description
                    )
                VALUES(
                    :cp_id,
                    :client_id,
                    :particulars,
                    :ornumber,
                    :remittanceCenter,
                    :debit,
                    :credit,
                    :dateClaimed,
                    :addedBy,
                    :description
                )
            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute([
                    'cp_id'             => $cp_id,
                    'client_id'         => $clientID,
                    'particulars'       => $particulars,
                    'ornumber'          => $paymentornumber,
                    'remittanceCenter'  => $remittanceCenter,
                    'debit'             => $debit,
                    'credit'            => $credit,
                    'addedBy'           => $addedBy,
                    'dateClaimed'       => $dateClaimed,
                    'description'       => $desc
                ]);


        } catch (PDOException $e) {
            return $e;
        }

    }

    public function removeThisPayment($payID)
    {
        try {
            $sql = "
                Update 
                    tbl_clientpaymenthistory
                set
                    active = 0
                WHERE
                    id = :payID  
            ";
 
            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'payID' => $payID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }


    public function updateReleseStat($paymentID) {
        try {
            
            $sql="
                update 
                    tbl_clientpaymenthistory
                set
                    releseStat = 1
                where
                    id = :paymentID 
            ";

        $statement = $this->db->prepare($sql);
        return  $statement->execute([
            'paymentID' => $paymentID
        ]);
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAdminPass()
    {
        try {

            $sql ="select Password from tbl_users where position = 'AdminPass'";
            
            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
 
            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function setNewAdminPass($newPass)
    {
        try {

            $sql ="update tbl_users set Password =:pass where position ='AdminPass'";
            
            $statement = $this->db->prepare($sql);
            return  $statement->execute([
                'pass' => $newPass
            ]);
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function incomeStatement()
    {
        try {

    // Old Query
    //         $sql="
    //         SELECT 
    //             a.ornumber AS '0',
    //             DATE(a.dateadded) AS '1',
    //             (SELECT CONCAT(fname, ' ', lname) FROM tbl_client WHERE client_id = a.client_id) AS '2', 
    //             (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1 ) AS '3',
    //             propD.TCP AS '4',
    //             propD.sqm AS '5',
    //             propD.term AS '6',
    //             propD.blk AS '7',
    //             propD.lts  AS '8',
    //             propD.downpayment AS '9',
    //             IF(a.debit = 0.00, 'Cash', a.remittanceCenter)  AS '10',
    //             (a.debit + a.credit) AS '11',
    //             IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ((propD.tcp * .1) / propD.rel ) - ( ((propD.tcp * .1) / propD.rel ) * 0.05 )  ,2), 0.00) AS '12',
    //             IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ((propD.tcp * .02) / propD.rel ) - ( ((propD.tcp * .02) / propD.rel ) * 0.05 )  ,2), 0.00) AS '13',
    //             IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ((propD.tcp * .03) / propD.rel ) - ( ((propD.tcp * .03) / propD.rel ) * 0.05 )  ,2), 0.00) AS '14',
    //             IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND(( (((propD.TCP / propD.rel) * .1) * .05) + (((propD.TCP / propD.rel) * .02) * .05) + (((propD.TCP / propD.rel) * .03) * .05) ), 2), 0.00) AS '15',
    //             CONCAT(IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED),ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perA / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perA / 100)), 2)) , ' (', propD.perA, '%)') AS '16',
    //             CONCAT(IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perB / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perB / 100)), 2)) , ' (', propD.perB, '%)') AS '17',
    //             CONCAT(IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perC / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perC / 100)), 2)) , ' (', propD.perC, '%)') AS '18',
    //             IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED),  ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perA / 100)) +  ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perB / 100)) + ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perC / 100)), 2), ROUND( (((a.debit + a.credit) * (propD.perA / 100)) + ((a.debit + a.credit)* (propD.perB / 100)) + ((a.debit + a.credit) * (propD.perC / 100))), 2)) AS '19',
    //             a.description  AS '20',
    //             IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED),ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perA / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perA / 100)), 2)) AS '21',
    //             IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perB / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perB / 100)), 2)) AS '22',
    //             IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perC / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perC / 100)), 2)) AS '23'
    //         FROM 
    //         tbl_clientpaymenthistory a 
    //     INNER JOIN 
    //         (SELECT 
    //             (a.pricePerm2 * a.sqm) AS 'TCP',
    //             a.sqm   AS 'sqm',
    //             a.block AS 'blk',
    //             a.lot   AS 'lts',
    //             ((a.pricePerm2 * a.sqm) / a.DefaultPlanTerms) AS 'm',
    //             b.cp_id,
    //             b.commissionReleased AS 'rel',
    // ROUND ((((a.pricePerm2 * a.sqm) - b.downpayment) / a.DefaultPlanTerms),2) AS 'downpayment',
    //             a.DefaultPlanTerms AS 'term',
    //             c.PercentageA AS 'perA',
    //             c.PercentageB AS 'perB',
    //             c.PercentageC AS  'perC'
    //         FROM 
    //            tbl_client_properties b
    //         LEFT JOIN tbl_propertylist a ON a.property_id = b.property_id
    //         LEFT JOIN tbl_propsubpercetage c ON a.propertyParentID = c.propId        
    //         ) AS propD ON propD.cp_id = a.cp_id
    //     WHERE 
    //         active = 1
    //     AND 
    //         debit = 0.00
    //     ";


    //Optimized Query
            $sql="
            SELECT 
            a.ornumber AS '0',
            DATE(a.dateadded) AS '1',
            CONCAT(e.fname,' ',e.lname) AS '2',
            CASE a.client_id 
              WHEN @cid THEN 
                  CASE a.cp_id 
                      WHEN @cpid THEN @curRow := @curRow + 1 
                      ELSE @curRow := 1 
                  END 
              ELSE @curRow := 1 
            END AS '3',
            propD.TCP AS '4',
            propD.sqm AS '5',
            propD.term AS '6',
            propD.blk AS '7',
            propD.lts  AS '8',
            propD.downpayment AS '9',
            IF(a.debit = 0.00, 'Cash', a.remittanceCenter)  AS '10',
            (a.debit + a.credit) AS '11',
            IF(@curRow <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ((propD.tcp * .1) / propD.rel ) - ( ((propD.tcp * .1) / propD.rel ) * 0.05 )  ,2), 0.00) AS '12',
            IF(@curRow <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ((propD.tcp * .02) / propD.rel ) - ( ((propD.tcp * .02) / propD.rel ) * 0.05 )  ,2), 0.00)  AS '13',
            IF(@curRow <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ((propD.tcp * .03) / propD.rel ) - ( ((propD.tcp * .03) / propD.rel ) * 0.05 )  ,2), 0.00) AS '14',
            IF(@curRow <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND(( (((propD.TCP / propD.rel) * .1) * .05) + (((propD.TCP / propD.rel) * .02) * .05) + (((propD.TCP / propD.rel) * .03) * .05) ), 2), 0.00) AS '15',
            CONCAT(IF(@curRow <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perA / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perA / 100)), 2)) , ' (', propD.perA, '%)') AS '16',
            CONCAT(IF(@curRow <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED),  ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perB / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perB / 100)), 2)) , ' (', propD.perB, '%)') AS '17',
            CONCAT(IF(@curRow <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perC / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perC / 100)), 2)) , ' (', propD.perC, '%)') AS '18',
            IF(@curRow <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED),  ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perA / 100)) +  ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perB / 100)) + ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perC / 100)), 2), ROUND( (((a.debit + a.credit) * (propD.perA / 100)) + ((a.debit + a.credit)* (propD.perB / 100)) + ((a.debit + a.credit) * (propD.perC / 100))), 2)) AS '19',
            a.description  AS '20',
            IF(@curRow <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perA / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perA / 100)), 2)) AS '21',
            IF(@curRow <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED),ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perB / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perB / 100)), 2)) AS '22',
            IF(@curRow <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perC / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perC / 100)), 2)) AS '23',
            @cid := a.client_id AS client_id,
            @cid := a.client_id AS client_id,
            @cpid := a.cp_id AS cpid 
          FROM
            tbl_clientpaymenthistory a 
             INNER JOIN 
                      (SELECT 
                          (a.pricePerm2 * a.sqm) AS 'TCP',
                          a.sqm   AS 'sqm',
                          a.block AS 'blk',
                          a.lot   AS 'lts',
                          ((a.pricePerm2 * a.sqm) / a.DefaultPlanTerms) AS 'm',
                          b.cp_id,
                          b.commissionReleased AS 'rel',
                  ROUND ((((a.pricePerm2 * a.sqm) - b.downpayment) / a.DefaultPlanTerms),2) AS 'downpayment',
                          a.DefaultPlanTerms AS 'term',
                          c.PercentageA AS 'perA',
                          c.PercentageB AS 'perB',
                          c.PercentageC AS  'perC'
                      FROM 
                         tbl_client_properties b
                      LEFT JOIN tbl_propertylist a ON a.property_id = b.property_id
                      LEFT JOIN tbl_propsubpercetage c ON a.propertyParentID = c.propId        
                      ) AS propD ON propD.cp_id = a.cp_id
                 INNER JOIN tbl_client e ON e.client_id = a.client_id
                
          WHERE a.active = 1 
            AND a.debit = 0.00
            AND YEAR(a.dateAdded) = YEAR(NOW())
           ORDER BY a.client_id,a.cp_id ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    //naa
    public function incomeStatementByDate($prop, $date, $type)
    {

        try {

            $searchby = "";
            $sql= "";

            $payment ='a.debit = 0.00';                
            if ($type == 1) {
                $payment = 'a.credit = 0.00';
            }

            if ($date == 0){
                $searchby = "a.active = 1 and ". $payment . '';
            }else if ($date == 1){
                $searchby = 'a.active = 1 and date(a.dateadded) = date(now()) and '. $payment . '';
            }else if($date == 2){
                $searchby = 'a.active = 1 and week(a.dateAdded) = week(date(now())) and ' . $payment . '';
            }else{
                $searchby = 'a.active = 1 and Month(a.dateadded) = Month(date(now())) and '. $payment . '';
            }

            if ($prop == 0) {

                 $sql="
                 SELECT 
                a.ornumber AS '0',
                DATE(a.dateadded) AS '1',
                (SELECT CONCAT(fname, ' ', lname) FROM tbl_client WHERE client_id = a.client_id) AS '2', 
                (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1 ) AS '3',
                propD.TCP AS '4',
                propD.sqm AS '5',
                propD.term AS '6',
                propD.blk AS '7',
                propD.lts  AS '8',
                propD.downpayment AS '9',
                IF(a.debit = 0.00, 'Cash', a.remittanceCenter)  AS '10',
                (a.debit + a.credit) AS '11',
                IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ((propD.tcp * .1) / propD.rel ) - ( ((propD.tcp * .1) / propD.rel ) * 0.05 )  ,2), 0.00) AS '12',
                IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ((propD.tcp * .02) / propD.rel ) - ( ((propD.tcp * .02) / propD.rel ) * 0.05 )  ,2), 0.00) AS '13',
                IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ((propD.tcp * .03) / propD.rel ) - ( ((propD.tcp * .03) / propD.rel ) * 0.05 )  ,2), 0.00) AS '14',
                IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND(( (((propD.TCP / propD.rel) * .1) * .05) + (((propD.TCP / propD.rel) * .02) * .05) + (((propD.TCP / propD.rel) * .03) * .05) ), 2), 0.00) AS '15',
                CONCAT(IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED),ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perA / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perA / 100)), 2)) , ' (', propD.perA, '%)') AS '16',
                CONCAT(IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perB / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perB / 100)), 2)) , ' (', propD.perB, '%)') AS '17',
                CONCAT(IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perC / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perC / 100)), 2)) , ' (', propD.perC, '%)') AS '18',
                IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED),  ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perA / 100)) +  ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perB / 100)) + ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perC / 100)), 2), ROUND( (((a.debit + a.credit) * (propD.perA / 100)) + ((a.debit + a.credit)* (propD.perB / 100)) + ((a.debit + a.credit) * (propD.perC / 100))), 2)) AS '19',
                a.description  AS '20',
                IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED),ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perA / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perA / 100)), 2)) AS '21',
                IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perB / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perB / 100)), 2)) AS '22',
                IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perC / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perC / 100)), 2)) AS '23'
            FROM  
                 tbl_clientpaymenthistory a 
             INNER JOIN 
                 (SELECT 
                     (a.pricePerm2 * a.sqm) AS 'TCP',
                     a.sqm   AS 'sqm',
                     a.block AS 'blk',
                     a.lot   AS 'lts',
                     ((a.pricePerm2 * a.sqm) / a.DefaultPlanTerms) AS 'm',
                     b.cp_id,
                     b.commissionReleased AS 'rel',
         ROUND ((((a.pricePerm2 * a.sqm) - b.downpayment) / a.DefaultPlanTerms),2) AS 'downpayment',
                     a.DefaultPlanTerms AS 'term',
                     c.PercentageA AS 'perA',
                     c.PercentageB AS 'perB',
                     c.PercentageC AS  'perC'
                 FROM 
                    tbl_client_properties b
                 LEFT JOIN tbl_propertylist a ON a.property_id = b.property_id
                 LEFT JOIN tbl_propsubpercetage c ON a.propertyParentID = c.propId        
                 ) AS propD ON propD.cp_id = a.cp_id
             WHERE 
                        ". $searchby ."";
            }else{

                $sql="
                SELECT 
                a.ornumber AS '0',
                DATE(a.dateadded) AS '1',
                (SELECT CONCAT(fname, ' ', lname) FROM tbl_client WHERE client_id = a.client_id) AS '2', 
                (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1 ) AS '3',
                propD.TCP AS '4',
                propD.sqm AS '5',
                propD.term AS '6',
                propD.blk AS '7',
                propD.lts  AS '8',
                propD.downpayment AS '9',
                IF(a.debit = 0.00, 'Cash', a.remittanceCenter)  AS '10',
                (a.debit + a.credit) AS '11',
                IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ((propD.tcp * .1) / propD.rel ) - ( ((propD.tcp * .1) / propD.rel ) * 0.05 )  ,2), 0.00) AS '12',
                IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ((propD.tcp * .02) / propD.rel ) - ( ((propD.tcp * .02) / propD.rel ) * 0.05 )  ,2), 0.00) AS '13',
                IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ((propD.tcp * .03) / propD.rel ) - ( ((propD.tcp * .03) / propD.rel ) * 0.05 )  ,2), 0.00) AS '14',
                IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND(( (((propD.TCP / propD.rel) * .1) * .05) + (((propD.TCP / propD.rel) * .02) * .05) + (((propD.TCP / propD.rel) * .03) * .05) ), 2), 0.00) AS '15',
                CONCAT(IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED),ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perA / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perA / 100)), 2)) , ' (', propD.perA, '%)') AS '16',
                CONCAT(IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perB / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perB / 100)), 2)) , ' (', propD.perB, '%)') AS '17',
                CONCAT(IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perC / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perC / 100)), 2)) , ' (', propD.perC, '%)') AS '18',
                IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED),  ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perA / 100)) +  ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perB / 100)) + ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perC / 100)), 2), ROUND( (((a.debit + a.credit) * (propD.perA / 100)) + ((a.debit + a.credit)* (propD.perB / 100)) + ((a.debit + a.credit) * (propD.perC / 100))), 2)) AS '19',
                a.description  AS '20',
                IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED),ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perA / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perA / 100)), 2)) AS '21',
                IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perB / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perB / 100)), 2)) AS '22',
                IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perC / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perC / 100)), 2)) AS '23'
            FROM 
            tbl_clientpaymenthistory a 
        INNER JOIN 
            (SELECT 
                (a.pricePerm2 * a.sqm) AS 'TCP',
                a.sqm   AS 'sqm',
                a.block AS 'blk',
                a.lot   AS 'lts',
                ((a.pricePerm2 * a.sqm) / a.DefaultPlanTerms) AS 'm',
                b.cp_id,
                b.commissionReleased AS 'rel',
    ROUND ((((a.pricePerm2 * a.sqm) - b.downpayment) / a.DefaultPlanTerms),2) AS 'downpayment',
                a.DefaultPlanTerms AS 'term',
                c.PercentageA AS 'perA',
                c.PercentageB AS 'perB',
                c.PercentageC AS  'perC'
                FROM 
                   tbl_client_properties b
                LEFT JOIN tbl_propertylist a ON a.property_id = b.property_id
                LEFT JOIN tbl_propsubpercetage c ON a.propertyParentID = c.propId        
                ) AS propD ON propD.cp_id = a.cp_id
            WHERE 
                        ". $searchby ." 
                    and 
                         a.cp_id = any (select cp_id from tbl_client_properties where property_id = any(select property_id from tbl_propertylist where propertyParentID  = ". $prop ."))";
            }  

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }

    }


    public function incomeStatementByDateAdvanceSearch($prop, $data, $type)
    {
        try {

            $searchby = "";
            $sql= "";

            $payment ='a.debit = 0.00';                
            if ($type == 1) {
                $payment = 'a.credit = 0.00';
            }

            switch ($data[0]['option']) {
                case 0:
                    $searchby = 'year(a.dateadded) = '. $data[0]['data']['year'] .' and a.active = 1 and ' . $payment . '';
                    break;
                case 1:
                    $searchby = 'month(a.dateadded) = '. $data[0]['data']['month'] .' and year(a.dateadded) = '. $data[0]['data']['year'] .' and a.active = 1 and ' .$payment . '';
                    break;
                default:
                    $searchby = 'date(a.dateadded) BETWEEN "'. $data[0]['data']['from'] .'" and "'. $data[0]['data']['to'] .'" and a.active = 1 and ' .$payment . '';
                     break;
            }   

            if ($prop == 0) {

                 $sql="
                 SELECT 
                 a.ornumber AS '0',
                 DATE(a.dateadded) AS '1',
                 (SELECT CONCAT(fname, ' ', lname) FROM tbl_client WHERE client_id = a.client_id) AS '2', 
                 (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1 ) AS '3',
                 propD.TCP AS '4',
                 propD.sqm AS '5',
                 propD.term AS '6',
                 propD.blk AS '7',
                 propD.lts  AS '8',
                 propD.downpayment AS '9',
                 IF(a.debit = 0.00, 'Cash', a.remittanceCenter)  AS '10',
                 (a.debit + a.credit) AS '11',
                 IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ((propD.tcp * .1) / propD.rel ) - ( ((propD.tcp * .1) / propD.rel ) * 0.05 )  ,2), 0.00) AS '12',
                 IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ((propD.tcp * .02) / propD.rel ) - ( ((propD.tcp * .02) / propD.rel ) * 0.05 )  ,2), 0.00) AS '13',
                 IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ((propD.tcp * .03) / propD.rel ) - ( ((propD.tcp * .03) / propD.rel ) * 0.05 )  ,2), 0.00) AS '14',
                 IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND(( (((propD.TCP / propD.rel) * .1) * .05) + (((propD.TCP / propD.rel) * .02) * .05) + (((propD.TCP / propD.rel) * .03) * .05) ), 2), 0.00) AS '15',
                 CONCAT(IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED),ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perA / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perA / 100)), 2)) , ' (', propD.perA, '%)') AS '16',
                 CONCAT(IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perB / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perB / 100)), 2)) , ' (', propD.perB, '%)') AS '17',
                 CONCAT(IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perC / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perC / 100)), 2)) , ' (', propD.perC, '%)') AS '18',
                 IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED),  ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perA / 100)) +  ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perB / 100)) + ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perC / 100)), 2), ROUND( (((a.debit + a.credit) * (propD.perA / 100)) + ((a.debit + a.credit)* (propD.perB / 100)) + ((a.debit + a.credit) * (propD.perC / 100))), 2)) AS '19',
                 a.description  AS '20',
                 IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED),ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perA / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perA / 100)), 2)) AS '21',
                 IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perB / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perB / 100)), 2)) AS '22',
                 IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perC / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perC / 100)), 2)) AS '23'
             FROM 
            tbl_clientpaymenthistory a 
        INNER JOIN 
            (SELECT 
                (a.pricePerm2 * a.sqm) AS 'TCP',
                a.sqm   AS 'sqm',
                a.block AS 'blk',
                a.lot   AS 'lts',
                ((a.pricePerm2 * a.sqm) / a.DefaultPlanTerms) AS 'm',
                b.cp_id,
                b.commissionReleased AS 'rel',
    ROUND ((((a.pricePerm2 * a.sqm) - b.downpayment) / a.DefaultPlanTerms),2) AS 'downpayment',
                a.DefaultPlanTerms AS 'term',
                c.PercentageA AS 'perA',
                c.PercentageB AS 'perB',
                c.PercentageC AS  'perC'
                 FROM 
                    tbl_client_properties b
                 LEFT JOIN tbl_propertylist a ON a.property_id = b.property_id
                 LEFT JOIN tbl_propsubpercetage c ON a.propertyParentID = c.propId        
                 ) AS propD ON propD.cp_id = a.cp_id
             WHERE 
                        ". $searchby ."";
            }else{

                $sql="
                SELECT 
                a.ornumber AS '0',
                DATE(a.dateadded) AS '1',
                (SELECT CONCAT(fname, ' ', lname) FROM tbl_client WHERE client_id = a.client_id) AS '2', 
                (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1 ) AS '3',
                propD.TCP AS '4',
                propD.sqm AS '5',
                propD.term AS '6',
                propD.blk AS '7',
                propD.lts  AS '8',
                propD.downpayment AS '9',
                IF(a.debit = 0.00, 'Cash', a.remittanceCenter)  AS '10',
                (a.debit + a.credit) AS '11',
                IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ((propD.tcp * .1) / propD.rel ) - ( ((propD.tcp * .1) / propD.rel ) * 0.05 )  ,2), 0.00) AS '12',
                IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ((propD.tcp * .02) / propD.rel ) - ( ((propD.tcp * .02) / propD.rel ) * 0.05 )  ,2), 0.00) AS '13',
                IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ((propD.tcp * .03) / propD.rel ) - ( ((propD.tcp * .03) / propD.rel ) * 0.05 )  ,2), 0.00) AS '14',
                IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND(( (((propD.TCP / propD.rel) * .1) * .05) + (((propD.TCP / propD.rel) * .02) * .05) + (((propD.TCP / propD.rel) * .03) * .05) ), 2), 0.00) AS '15',
                CONCAT(IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED),ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perA / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perA / 100)), 2)) , ' (', propD.perA, '%)') AS '16',
                CONCAT(IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perB / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perB / 100)), 2)) , ' (', propD.perB, '%)') AS '17',
                CONCAT(IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perC / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perC / 100)), 2)) , ' (', propD.perC, '%)') AS '18',
                IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED),  ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perA / 100)) +  ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perB / 100)) + ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perC / 100)), 2), ROUND( (((a.debit + a.credit) * (propD.perA / 100)) + ((a.debit + a.credit)* (propD.perB / 100)) + ((a.debit + a.credit) * (propD.perC / 100))), 2)) AS '19',
                a.description  AS '20',
                IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED),ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perA / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perA / 100)), 2)) AS '21',
                IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perB / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perB / 100)), 2)) AS '22',
                IF((SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id AND tbl_clientpaymenthistory.`active` = 1) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), ROUND ( ( (( (a.debit + a.credit) ) - (((propD.TCP * .15) / propD.rel )  )) *(propD.perC / 100)),2), ROUND( ((a.debit + a.credit) * (propD.perC / 100)), 2)) AS '23'
            FROM 
                tbl_clientpaymenthistory a 
            INNER JOIN 
                (SELECT 
                    (a.pricePerm2 * a.sqm) AS 'TCP',
                    a.sqm   AS 'sqm',
                    a.block AS 'blk',
                    a.lot   AS 'lts',
                    ((a.pricePerm2 * a.sqm) / a.DefaultPlanTerms) AS 'm',
                    b.cp_id,
                    b.commissionReleased AS 'rel',
        ROUND ((((a.pricePerm2 * a.sqm) - b.downpayment) / a.DefaultPlanTerms),2) AS 'downpayment',
                    a.DefaultPlanTerms AS 'term',
                    c.PercentageA AS 'perA',
                    c.PercentageB AS 'perB',
                    c.PercentageC AS  'perC'
                FROM 
                   tbl_client_properties b
                LEFT JOIN tbl_propertylist a ON a.property_id = b.property_id
                LEFT JOIN tbl_propsubpercetage c ON a.propertyParentID = c.propId        
                ) AS propD ON propD.cp_id = a.cp_id
            WHERE 
                        ". $searchby ." 
                    and 
                         a.cp_id = any (select cp_id from tbl_client_properties where property_id = any(select property_id from tbl_propertylist where propertyParentID  = ". $prop ."))";
            }  

        
            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getPaymentDescription($id)
    {
        try {

            $sql="select description as 'paymentDesc', date(dateAdded) as 'dateAdded' from tbl_clientpaymenthistory where id =:id";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id' => $id
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getCharges()
    {

        try {
                        
                            $sql="select p.additionalCharges as 'charges', concat(c.fname, ' ', c.lname) as name, property.propName as 'property', property.parentID as 'id' from tbl_client_properties p left join tbl_client c  on c.client_id = p.client_id  left join (select concat(parent.propertyName, ' ', if(list.phaseNumber = 0, '',  concat('Ph ', list.phaseNumber)) , ' Blk', list.block, ' Lt', list.lot) as 'propName', list.property_id as 'property_id', list.propertyParentID  as 'parentID' from tbl_propertylist list left join tbl_propertyparent parent on list.propertyParentID = parent.id) as property on p.property_id = property.property_id where p.active = 1 and p.additionalCharges != 'null'";
                        

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getChargeAndPropertyName(){
        try{
            $sql= "SELECT 
            p.additionalCharges AS 'charges',
            b.propertyName
          FROM
            tbl_client_properties p 
            INNER JOIN tbl_propertylist a 
            ON a.`property_id` = p.`property_id`
            INNER JOIN tbl_propertyparent b
            ON a.`propertyParentID` = b.`id`	
          WHERE p.active = 1 
            AND p.additionalCharges != 'null'";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        } catch (PDOException $e) {
        return $e;
        }
    }

    public function geThisCharges($clientId, $propId)
    {
        try {

            $sql = "select additionalCharges from tbl_client_properties where cp_id = :clientID and client_id =:propID and active = 1";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'clientID' => $clientId,
                'propID' => $propId 
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function updateThisCharges($clientId, $propId, $charges)
    {
        try {

            $sql = "update tbl_client_properties set additionalCharges =:charges where cp_id = :clientID and client_id =:propID and active = 1 ";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
                'clientID'  => $clientId,
                'propID'    => $propId,
                'charges'   => $charges
            ]);

        } catch (PDOException $e) {
            return  $e;
        }
    }

    public function getChargesAdvanceSearch($data)
    {
        switch ($data[0]['option']) {
            case 0:
                $searchby = 'year(a.dateadded) = '. $data[0]['data']['year'] .' and active = 1 and ' . $payment . '';
                break;
            case 1:
                $searchby = 'month(a.dateadded) = '. $data[0]['data']['month'] .' and year(a.dateadded) = '. $data[0]['data']['year'] .' and active = 1 and ' .$payment . '';
                break;
            default:
                $searchby = 'date(a.dateadded) BETWEEN "'. $data[0]['data']['from'] .'" and "'. $data[0]['data']['to'] .'" and active = 1 and ' .$payment . '';
                 break;
        }

        try {

            $sql = "select 
                        p.additionalCharges as 'charges', 
                        concat(c.fname, ' ', c.lname) as name, 
                        property.propName as 'property',
                        property.parentID as 'id'
                    from 
                        tbl_client_properties p 
                            left join tbl_client c  
                                on c.client_id = p.client_id  
                            left join 
                                (select concat(parent.propertyName, ' ', if(list.phaseNumber = 0, '',  concat('Ph ', list.phaseNumber)) , ' Blk', list.block, ' Lt', list.lot) as 'propName', 
                                list.property_id as 'property_id',
                                list.propertyParentID as 'parentID',
                                from tbl_propertylist 
                                list left join tbl_propertyparent parent on list.propertyParentID = parent.id) 
                            as 
                                property on p.property_id = property.property_id 
                            where 
                                p.active = 1 
                            and 
                                p.additionalCharges != 'null'";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'clientID' => $clientId,
                'propID' => $propId 
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getPropertyPercentage($propID)
    {
        try {
            
            $sql = "
            SELECT 
                PercentageA AS 'a',
                PercentageB AS 'b',
                PercentageC AS 'c' 
            FROM
                tbl_propsubpercetage 
            WHERE propId = :propID 
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'propID' => $propID 
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getCashIncome(){
        try{
            $sql= "
            SELECT 
            SUM(ROUND(a.credit,2)) AS '0',
            SUM(ROUND(a.debit,2)) AS '1',
            SUM(ROUND ( ((propD.tcp * .02) / propD.rel ) - ( ((propD.tcp * .02) / propD.rel ) * 0.05 )  ,2)) AS '2',
            SUM(ROUND ( ((propD.tcp * .03) / propD.rel ) - ( ((propD.tcp * .03) / propD.rel ) * 0.05 )  ,2)) AS '3'
            FROM
            tbl_clientpaymenthistory a 
            INNER JOIN tbl_client_properties b 
            ON a.`cp_id` = b.cp_id 
            INNER JOIN tbl_propertylist c
            ON b.property_id = c.property_id
            INNER JOIN tbl_propertyparent d
            ON c.propertyParentID = d.id
            INNER JOIN 
                (SELECT 
                    (a.pricePerm2 * a.sqm) AS 'TCP',
                    a.sqm   AS 'sqm',
                    a.block AS 'blk',
                    a.lot   AS 'lts',
                    ((a.pricePerm2 * a.sqm) / a.DefaultPlanTerms) AS 'm',
                    b.cp_id,
                    b.commissionReleased AS 'rel',
            ROUND ((((a.pricePerm2 * a.sqm) - b.downpayment) / a.DefaultPlanTerms),2) AS 'downpayment',
                    a.DefaultPlanTerms AS 'term',
                    c.PercentageA AS 'perA',
                    c.PercentageB AS 'perB',
                    c.PercentageC AS  'perC'
                FROM 
                    tbl_client_properties b
                LEFT JOIN tbl_propertylist a ON a.property_id = b.property_id
                LEFT JOIN tbl_propsubpercetage c ON a.propertyParentID = c.propId        
                ) AS propD ON propD.cp_id = a.cp_id
          WHERE a.active = 1 
            GROUP BY d.id
            "; 
            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        }catch(PDOException $e){
            return $e;
        }
    }

        

    public function getPropertyParentAsc(){
        try{
            $sql = "SELECT propertyName FROM tbl_propertyparent ORDER BY id";
            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        }catch(PDOException $e){
            return $e;
        }
    }
    
    

    public function getTaxCash(){
        try{
            $sql = "
            SELECT 

    IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1 ) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), SUM(ROUND(( (((propD.TCP / propD.rel) * .1) * .05) + (((propD.TCP / propD.rel) * .02) * .05) + (((propD.TCP / propD.rel) * .03) * .05) ), 2)), 0.00) AS 'Tax Cash'
    FROM 
    tbl_clientpaymenthistory a 
    INNER JOIN tbl_client_properties b 
    ON a.`cp_id` = b.cp_id 
    INNER JOIN tbl_propertylist c
    ON b.property_id = c.property_id
    INNER JOIN tbl_propertyparent d
    ON c.propertyParentID = d.id
INNER JOIN 
    (SELECT 
        (a.pricePerm2 * a.sqm) AS 'TCP',
        a.sqm   AS 'sqm',
        a.block AS 'blk',
        a.lot   AS 'lts',
        ((a.pricePerm2 * a.sqm) / a.DefaultPlanTerms) AS 'm',
        b.cp_id,
        b.commissionReleased AS 'rel',
ROUND ((((a.pricePerm2 * a.sqm) - b.downpayment) / a.DefaultPlanTerms),2) AS 'downpayment',
        a.DefaultPlanTerms AS 'term',
        c.PercentageA AS 'perA',
        c.PercentageB AS 'perB',
        c.PercentageC AS  'perC'
    FROM 
       tbl_client_properties b
    LEFT JOIN tbl_propertylist a ON a.property_id = b.property_id
    LEFT JOIN tbl_propsubpercetage c ON a.propertyParentID = c.propId        
    ) AS propD ON propD.cp_id = a.cp_id
WHERE 
    a.active = 1 
    AND 
    a.debit = 0.00
      GROUP BY d.id
            ";
            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        }catch(PDOException $e){
            return $e;
        }
    }

    

    public function getTaxBank(){
        try{
            $sql = "
                    SELECT 

            IF( (SELECT COUNT(client_id) AS 'num' FROM tbl_clientpaymenthistory WHERE client_id = a.client_id AND id <= a.id AND cp_id = a.cp_id  AND tbl_clientpaymenthistory.`active` = 1 ) <= CAST(SUBSTRING_INDEX(propD.rel, '-', -1) AS UNSIGNED), SUM(ROUND(( (((propD.TCP / propD.rel) * .1) * .05) + (((propD.TCP / propD.rel) * .02) * .05) + (((propD.TCP / propD.rel) * .03) * .05) ), 2)), 0.00) AS 'Tax Cash'
            FROM 
            tbl_clientpaymenthistory a 
            INNER JOIN tbl_client_properties b 
            ON a.`cp_id` = b.cp_id 
            INNER JOIN tbl_propertylist c
            ON b.property_id = c.property_id
            INNER JOIN tbl_propertyparent d
            ON c.propertyParentID = d.id
        INNER JOIN 
            (SELECT 
                (a.pricePerm2 * a.sqm) AS 'TCP',
                a.sqm   AS 'sqm',
                a.block AS 'blk',
                a.lot   AS 'lts',
                ((a.pricePerm2 * a.sqm) / a.DefaultPlanTerms) AS 'm',
                b.cp_id,
                b.commissionReleased AS 'rel',
        ROUND ((((a.pricePerm2 * a.sqm) - b.downpayment) / a.DefaultPlanTerms),2) AS 'downpayment',
                a.DefaultPlanTerms AS 'term',
                c.PercentageA AS 'perA',
                c.PercentageB AS 'perB',
                c.PercentageC AS  'perC'
            FROM 
            tbl_client_properties b
            LEFT JOIN tbl_propertylist a ON a.property_id = b.property_id
            LEFT JOIN tbl_propsubpercetage c ON a.propertyParentID = c.propId        
            ) AS propD ON propD.cp_id = a.cp_id
        WHERE 
            a.active = 1 
            AND 
            a.credit = 0.00

            GROUP BY d.id
            ";
            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        }catch(PDOException $e){
            return $e;
        }
    }

    public function getPropertyName(){
        try{
            $sql="
            SELECT 
                id,
                propertyName 
            FROM
                tbl_propertyparent";
                $statement = $this->db->prepare($sql);
                $statement->execute();
                $statement->setFetchMode(PDO::FETCH_ASSOC);
                return $statement->fetchAll();
        }catch(PDOException $e){
                return $e;
        }
    }
}