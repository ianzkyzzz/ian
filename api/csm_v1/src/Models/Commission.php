<?php

namespace App\Models;

use PDO;

class Commission {

    /**
    * @var $db
    */
    private $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }

    
    // releasing of com for agent
    public function getThisAgentAllCommisionsToRelease($agentID){
        try {
            $sql = "
            SELECT
            DISTINCT clientProperty.cp_id,
              tbl_commissions.agentID,
              tbl_commissions.percentagevalue,
              clientProperty.clientName,
              clientProperty.propertyName,
              (select count(com.id) from commisionrelease com where com.agentID = tbl_commissions.agentID and com.cp_id = tbl_client_properties.cp_id) as 'released'
          FROM
              tbl_commissions
          INNER JOIN
              (
                  SELECT
                      tbl_client_properties.cp_id,
                      concat(tbl_client.Fname,' ',tbl_client.Lname) as clientName,
                      tbl_propertylist2.propertyName
                  FROM 
                      tbl_client_properties
                  INNER JOIN 
                      tbl_client ON tbl_client.client_id =  tbl_client_properties.client_id
                  INNER JOIN 
                      (
                      SELECT
                          CONCAT(tbl_propertyparent.propertyName,IF(tbl_propertylist.phaseNumber = 0,'',CONCAT(' Phase ',tbl_propertylist.phaseNumber)),' Block ',tbl_propertylist.block,' Lot ',tbl_propertylist.lot) as propertyName,
                          tbl_propertylist.property_id
                      FROM
                          tbl_propertylist
                      INNER JOIN
                          tbl_propertyparent on tbl_propertyparent.id = tbl_propertylist.propertyParentID
                      ) as tbl_propertylist2 ON tbl_propertylist2.property_id =  tbl_client_properties.property_id
              ) as clientProperty on clientProperty.cp_id = tbl_commissions.cp_id
          INNER JOIN
              tbl_client_properties on tbl_client_properties.cp_id = tbl_commissions.cp_id 
          INNER JOIN
              tbl_clientpaymenthistory ON tbl_clientpaymenthistory.cp_id = tbl_client_properties.cp_id 
          WHERE
              tbl_commissions.active = 1
          AND
              tbl_client_properties.cp_id = tbl_clientpaymenthistory.cp_id
          AND
              tbl_clientpaymenthistory.active = 1
          AND
              (select count(com.id) from commisionrelease com where com.agentID = tbl_commissions.agentID and com.cp_id = tbl_client_properties.cp_id) < tbl_client_properties.commissionReleased
          AND
              (select count(com.id) from commisionrelease com where com.agentID = tbl_commissions.agentID and com.cp_id = tbl_client_properties.cp_id) < if(tbl_client_properties.fullyPaid = 1, tbl_client_properties.commissionReleased, (select count(id) from tbl_clientpaymenthistory where cp_id = tbl_commissions.cp_id and active = 1))                AND
              tbl_commissions.agentID = :agentID
            ";    
            $statement = $this->db->prepare($sql);            
            $statement->execute([
                    'agentID'  => $agentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);            
            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function testinglang($cp_id){
        try{
            $sql="SELECT 
            * 
          FROM
            tbl_clientpaymenthistory 
          WHERE cp_id = :cp_id
            AND active = 1 
          ORDER BY id DESC 
          LIMIT 1 ";
          $statement = $this->db->prepare($sql);
          $statement->execute(['cp_id' => $cp_id]);
          $statement->setFetchMode(PDO::FETCH_ASSOC);
          return $statement->fetchAll();
        }catch(PDOException $e){
            return $e;
        }
    }

    public function countRelease($cp_id,$agentID){
        try{
            $sql = "SELECT 
                        COUNT(*)  as 'released'
                    FROM 
                        commisionrelease 
                    WHERE 
                        cp_id = :cp_id 
                    AND 
                        agentID = :agentID
            ";
            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'cp_id'     => $cp_id,
                    'agentID'   => $agentID
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();
        }catch(PDOException $e){
            return $e;
        }
    }



    public function getThisAgentAllCommisions($agentID){
        try {
            $sql = "
                SELECT
                    clientProperty.cp_id,
                    tbl_commissions.agentID,
                    tbl_commissions.percentagevalue,
                    clientProperty.clientName,
                    clientProperty.propertyName
                FROM
                    tbl_commissions
                INNER JOIN
                    (
                        SELECT
                            tbl_client_properties.cp_id,
                            concat(tbl_client.Fname,' ',tbl_client.Lname) as clientName,
                            tbl_propertylist2.propertyName
                        FROM 
                            tbl_client_properties
                        INNER JOIN 
                            tbl_client ON tbl_client.client_id =  tbl_client_properties.client_id
                        INNER JOIN 
                            (
                            SELECT
                                CONCAT(tbl_propertyparent.propertyName,IF(tbl_propertylist.phaseNumber = 0,'',CONCAT(' Phase ',tbl_propertylist.phaseNumber)),' Block ',tbl_propertylist.block,' Lot ',tbl_propertylist.lot) as propertyName,
                                tbl_propertylist.property_id
                            FROM
                                tbl_propertylist
                            INNER JOIN
                                tbl_propertyparent on tbl_propertyparent.id = tbl_propertylist.propertyParentID
                            ) as tbl_propertylist2 ON tbl_propertylist2.property_id =  tbl_client_properties.property_id
                    ) as clientProperty on clientProperty.cp_id = tbl_commissions.cp_id  
                WHERE
                    active = 1
                AND
                    released = 0
                AND
                    tbl_commissions.agentID = :agentID
            ";

            $statement = $this->db->prepare($sql);


            $statement->execute([
                    'agentID'  => $agentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }


    // attribute paymnet remove from the list beacuse theres currenttly no use  of it.
    public function  insertIntoComRelease($agentID, $cpID, $clientID, $propName, $contract, $amount, $releaseNO, $releaseDate, $releaseBy, $payment){
        try {

             $sql = "
                INSERT INTO
                    commisionrelease(
                        agentID,
                        cp_id,
                        client_id,
                        propertyName,
                        contractPrice,
                        amount,
                        releaseNO,
                        ReleaseDate,
                        releaseBy,
                        payment
                    )

                VALUES(
                    :agentID,
                    :cp_id,
                    :clientID,
                    :propertyName,
                    :contractPrice,
                    :amount,
                    :releaseNO,
                    :ReleaseDate,
                    :releaseBy,
                    :payment
                )
            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute([
                    'agentID'              => $agentID,
                    'cp_id'                => $cpID,
                    'clientID'             => $clientID,
                    'propertyName'         => $propName,
                    'contractPrice'        => $contract,
                    'amount'               => $amount,
                    'releaseNO'            => $releaseNO,
                    'ReleaseDate'          => $releaseDate,
                    'releaseBy'            => $releaseBy,
                    'payment'              => $payment
                ]);

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAgentComReleasedOptional($agentID,$proplist)
    {
        try {
            
            $sql = "
                SELECT
                   DISTINCT clientProperty.cp_id,
                        tbl_commissions.agentID,
                        tbl_commissions.percentagevalue,
                        clientProperty.clientName,
                        clientProperty.propertyName,
                        clientProperty.client_id,
                        (select count(com.id) from commisionrelease com where com.agentID = tbl_commissions.agentID and com.cp_id = tbl_client_properties.cp_id) as 'released',
                        date_format(tbl_commissions.dateReleased,'%M %d, %Y %r') as dateNow
                    FROM
                        tbl_commissions
                    INNER JOIN
                        (
                            SELECT
                                tbl_client_properties.cp_id,
                                concat(tbl_client.Fname,' ',tbl_client.Lname) as clientName,
                                tbl_propertylist2.propertyName,
                                tbl_client_properties.client_id
                            FROM 
                                tbl_client_properties
                            INNER JOIN 
                                tbl_client ON tbl_client.client_id =  tbl_client_properties.client_id
                            INNER JOIN 
                                (
                                SELECT
                                    CONCAT(tbl_propertyparent.propertyName,IF(tbl_propertylist.phaseNumber = 0,'',CONCAT(' Phase ',tbl_propertylist.phaseNumber)),' Block ',tbl_propertylist.block,' Lot ',tbl_propertylist.lot) as propertyName,
                                    tbl_propertylist.property_id
                                FROM
                                    tbl_propertylist
                                INNER JOIN
                                    tbl_propertyparent on tbl_propertyparent.id = tbl_propertylist.propertyParentID
                                ) as tbl_propertylist2 ON tbl_propertylist2.property_id =  tbl_client_properties.property_id
                        ) as clientProperty on clientProperty.cp_id = tbl_commissions.cp_id
                    inner JOIN
                        tbl_client_properties on tbl_client_properties.cp_id = tbl_commissions.cp_id
                    INNER JOIN
                        tbl_clientpaymenthistory on tbl_client_properties.cp_id = tbl_clientpaymenthistory.cp_id
                    WHERE
                        tbl_commissions.active = 1
                    AND
                        tbl_clientpaymenthistory.active = 1
                    AND
                        (select count(com.id) from commisionrelease com where com.agentID = tbl_commissions.agentID and com.cp_id = tbl_client_properties.cp_id) < tbl_client_properties.commissionReleased
                    AND
                        --  tbl_commissions.released < if(tbl_client_properties.fullyPaid = 1, tbl_client_properties.commissionReleased, (select count(id) from tbl_clientpaymenthistory where cp_id = tbl_commissions.cp_id))

                        (select count(com.id) from commisionrelease com where com.agentID = tbl_commissions.agentID and com.cp_id = tbl_client_properties.cp_id) < if(tbl_client_properties.fullyPaid = 1, tbl_client_properties.commissionReleased, (select count(id) from tbl_clientpaymenthistory where cp_id = tbl_commissions.cp_id and active = 1))
                    AND
                        tbl_commissions.agentID = :agentID
                    AND
                        tbl_commissions.cp_id  IN (".implode(',', $proplist).")";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'agentID'  => $agentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();




        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getThisAgentAllCommisionsReleased($agentID,$dateNow){
        try {
            $sql = "
                SELECT
                   DISTINCT clientProperty.cp_id,
                        tbl_commissions.agentID,
                        tbl_commissions.percentagevalue,
                        clientProperty.clientName,
                        clientProperty.propertyName,
                        clientProperty.client_id,
                        (select count(com.id) from commisionrelease com where com.agentID = tbl_commissions.agentID and com.cp_id = tbl_client_properties.cp_id) as 'released',
                        date_format(tbl_commissions.dateReleased,'%M %d, %Y %r') as dateNow
                    FROM
                        tbl_commissions
                    INNER JOIN
                        (
                            SELECT
                                tbl_client_properties.cp_id,
                                concat(tbl_client.Fname,' ',tbl_client.Lname) as clientName,
                                tbl_propertylist2.propertyName,
                                tbl_client_properties.client_id
                            FROM 
                                tbl_client_properties
                            INNER JOIN 
                                tbl_client ON tbl_client.client_id =  tbl_client_properties.client_id
                            INNER JOIN 
                                (
                                SELECT
                                    CONCAT(tbl_propertyparent.propertyName,IF(tbl_propertylist.phaseNumber = 0,'',CONCAT(' Phase ',tbl_propertylist.phaseNumber)),' Block ',tbl_propertylist.block,' Lot ',tbl_propertylist.lot) as propertyName,
                                    tbl_propertylist.property_id
                                FROM
                                    tbl_propertylist
                                INNER JOIN
                                    tbl_propertyparent on tbl_propertyparent.id = tbl_propertylist.propertyParentID
                                ) as tbl_propertylist2 ON tbl_propertylist2.property_id =  tbl_client_properties.property_id
                        ) as clientProperty on clientProperty.cp_id = tbl_commissions.cp_id
                    inner JOIN
                        tbl_client_properties on tbl_client_properties.cp_id = tbl_commissions.cp_id
                    INNER JOIN
                        tbl_clientpaymenthistory on tbl_client_properties.cp_id = tbl_clientpaymenthistory.cp_id
                    WHERE
                        tbl_commissions.active = 1
                    AND
                        tbl_clientpaymenthistory.active = 1
                    AND
                        (select count(com.id) from commisionrelease com where com.agentID = tbl_commissions.agentID and com.cp_id = tbl_client_properties.cp_id) < tbl_client_properties.commissionReleased
                    AND
                        (select count(com.id) from commisionrelease com where com.agentID = tbl_commissions.agentID and com.cp_id = tbl_client_properties.cp_id) < if(tbl_client_properties.fullyPaid = 1, tbl_client_properties.commissionReleased, (select count(id) from tbl_clientpaymenthistory where cp_id = tbl_commissions.cp_id and active = 1))
                        
                    AND
                        tbl_commissions.agentID = :agentID
            ";

            $statement = $this->db->prepare($sql);


            $statement->execute([
                    'agentID'  => $agentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAgentCpID($agentID){
        try {
            $sql="
               SELECT
                 DISTINCT  tbl_commissions.cp_id,
                    tbl_client_properties.commissionReleased,
                    (select count(com.id) from commisionrelease com where com.agentID = tbl_commissions.agentID and com.cp_id = tbl_client_properties.cp_id) as 'released',
                    tbl_client_properties.fullyPaid
                    
                FROM
                    tbl_commissions,
                    tbl_client_properties     
                WHERE   
                    tbl_client_properties.cp_id = tbl_commissions.cp_id
                AND
                    (select count(com.id) from commisionrelease com where com.agentID = tbl_commissions.agentID and com.cp_id = tbl_client_properties.cp_id) < tbl_client_properties.commissionReleased
                AND 
                    -- tbl_commissions.released < if(tbl_client_properties.fullyPaid = 1, tbl_client_properties.commissionReleased, (select count(id) from tbl_clientpaymenthistory where cp_id = tbl_commissions.cp_id and active = 1))

                    (select count(com.id) from commisionrelease com where com.agentID = tbl_commissions.agentID and com.cp_id = tbl_client_properties.cp_id) < if(tbl_client_properties.fullyPaid = 1, tbl_client_properties.commissionReleased, (select count(id) from tbl_clientpaymenthistory where cp_id = tbl_commissions.cp_id and active = 1))
                    
                AND
                    tbl_commissions.active = 1
                AND
                    tbl_commissions.agentID = :agentID
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'agentID'  => $agentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getUpdateCom_2($cpID, $comRel, $agentID){
        
        try {

            $sql="

                UPDATE
                    tbl_commissions
                SET
                    tbl_commissions.active = 0
                WHERE
                    tbl_commissions.released < :comRel
                AND
                    tbl_commissions.cp_id = :cp_id
                AND
                    tbl_commissions.agentID = :agentID

            ";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
                    'cp_id'  => $cpID,
                    'comRel'  => $comRel,
                    'agentID'  => $agentID
                ]);

        } catch (PDOException $e) {
            return $e;
        }

    }


    public function getUpdatePayments($cpID, $agentID){
        try {
            
            $sql="
                UPDATE
                    tbl_commissions
                SET
                    tbl_commissions.comReleaseNo = 1
                WHERE
                    tbl_commissions.cp_id = :cpID
                and 
                    tbl_commissions.agentID = :agentID

            ";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
                    'cpID'  => $cpID, 
                    'agentID'  => $agentID
                ]);

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getUpdateCom($cpID, $comRel, $agentID){
        
        try {
            $sql="

                UPDATE
                    tbl_commissions
                SET
                    tbl_commissions.released = (released + 1)
                WHERE
                    tbl_commissions.active = 1
                AND
                    tbl_commissions.released < :comRel
                AND
                    tbl_commissions.cp_id = :cp_id
                AND
                    tbl_commissions.agentID = :agentID

            ";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
                    'comRel'   => $comRel,
                    'cp_id'    => $cpID,
                    'agentID'  => $agentID
                ]);

        } catch (PDOException $e) {
            return $e;
        }

    }


    public function getAgents($cpID) {
        try {
             
            $sql="
                select * 
                from 
                    tbl_commissions
                where 
                    comReleaseNo = 0
                and 
                    cp_id= :cpID
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'cpID'  => $cpID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }


    public function updateComreleaseNo($cp_id) {
        try {
            
            $sql="
                update
                    tbl_commissions
                set 
                    comReleaseNo = 1,
                    released  = (released + 1)
                where
                    cp_id =:cpID 
                and 
                    comReleaseNo = 0
            ";

        $statement = $this->db->prepare($sql);
        return  $statement->execute([
            'cpID' => $cp_id
        ]);
        } catch (PDOException $e) {
            return $e;
        }
    }
    

    // public function releaseThisAgentAllCommisionsToRelease($agentID,$dateNow){
    //     try {
    //         $sql = "
    //             UPDATE
    //                 tbl_commissions
    //             SET
    //             	released = 1,
    //             	dateReleased = '".$dateNow."'
    //             WHERE
    //                 active = 1
    //             AND
    //             	released = 0
    //             AND
    //                 tbl_commissions.agentID = :agentID
    //         ";

    //         $statement = $this->db->prepare($sql);
            

    //         return $statement->execute([
    //                 'agentID'  => $agentID
    //             ]);
    //     } catch (PDOException $e) {
    //         return $e;
    //     }
    // }

    public function geTReleasedCommission($agentId, $value, $searchBy){
        try{
            $sql = "select concat(cl.fname, ' ', cl.lname) as '0',  
                        concat(com.propertyName, ' (', (select percentagevalue from tbl_commissions where cp_id = prop.cp_id  and agentID = com.agentID), ')') as '1', 
                        com.contractPrice as '2', 
                        com.payment as '3', 
                        com.amount as '4', 
                        round((com.contractPrice / prop.commissionReleased) * ((select percentagevalue from tbl_commissions where cp_id = com.cp_id  and agentID = com.agentID) / 100)) as '5', 
                        com.ReleaseDate as '6' 
                    from 
                        commisionrelease com, tbl_client cl, tbl_client_properties prop where cl.client_id = com.client_id and prop.client_id = cl.client_id and prop.cp_id = com.cp_id and com.agentID =:ID and com.". $searchBy . " =:val";
            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'ID'   => $agentId,
                    'val'  => $value
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        }catch (PDOException $e){
            return $e;
        }
    }

}