<?php

namespace App\Models;

use PDO;

class Property {

    /**
    * @var $db
    */
    private $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }



    public function insertThisProperty($add_propertyFormName,$add_propertyFormAddress,$add_propertyWithPhases,$addedBy){
        try {
            $sql = "
                INSERT INTO 
                    tbl_propertyparent(
                        propertyName,
                        address,
                        isPhasable,
                        addedBy
                    )
                VALUES(
                        :add_propertyFormName,
                        :add_propertyFormAddress,
                        :add_propertyWithPhases,
                        :addedBy
                )

            ";

            $statement = $this->db->prepare($sql);
                    try{
                        $this->db->beginTransaction();
                         $statement->execute([
                            'add_propertyFormName'       => $add_propertyFormName,
                            'add_propertyFormAddress'    => $add_propertyFormAddress,
                            'add_propertyWithPhases'     => $add_propertyWithPhases,
                            'addedBy'  => $addedBy
                        ]);
                        $lastId  = $this->db->lastInsertId();
                        $this->db->commit();
                        return $lastId;
                    }catch(PDOException $e){
                        return $e;
                    }
   


        } catch (PDOException $e) {
            return $e;
        }
    }

    public function postPropSubPercentageDefault($propID){
        try{
            $sql="
            INSERT INTO 
            tbl_propsubpercetage
            (
                propId,
                PercentageA,
                PercentageB,
                PercentageC
            )
            VALUES
            (
                :propID,
                0,
                0,
                0
            )
            ";
            $statement = $this->db->prepare($sql);
            return $statement->execute([
                'propID'    => $propID
            ]);
        }catch(PDOException $e){
            return $e;
        }
    }

    public function removeSelectedLot($propertyParentID, $block, $phasenumber, $lot){
        try {
              $sql = "
                UPDATE
                    tbl_propertylist
                SET
                    active        = 0
                WHERE
                    propertyParentID  = :porpertyID
                and
                    block             = :block
                and
                    lot               = :lot
                and
                    phaseNumber       = :phaseNo
            ";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
                    'porpertyID'     => $propertyParentID,
                    'block'          => $block,
                    'phaseNo'        => $phasenumber,
                    'lot'            => $lot
                ]);

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function insertThisBlockProperty(array $parameters,array $values){
        try {
            $this->db->setAttribute(PDO::ATTR_AUTOCOMMIT,false);
            $this->db->beginTransaction();

            $sql = "
                INSERT INTO 
                    tbl_propertylist(
                        lot,
                        propertyTypeID,
                        originalPrice,
                        monthly_amortization,
                        sqm,
                        pricePerm2,
                        DefaultPlanTerms,
                        model,
                        status,
                        propertyParentID,
                        block,
                        phaseNumber,
                        addedBy
                    )
                VALUES
                        ".implode(',', $parameters)."
                
            ";

            $statement = $this->db->prepare($sql);
            $result = $statement->execute($values);

            if(!$result){
                $this->db->rollBack();
                return false;
            }

            return $this->db->commit();
        } catch (PDOException $e) {
            $this->db->rollBack();
            return false;
        } catch (Exception $e) {
            $this->db->rollBack();
            return false;
        }
    }




    public function getlastBlockNumber($parentID){
        try {
            $sql = "
               SELECT
                    phaseNumber,
                    block as blockNumber
                FROM
                    tbl_propertylist
                WHERE
                    propertyParentID  = :parentID
                AND
                    active = 1
                and  
                    phaseNumber = (SELECT phaseNumber from tbl_propertylist where propertyParentID = ". $parentID ." order by phaseNumber DESC LIMIT 1 )
                ORDER BY  
                    block desc
                LIMIT 1
            ";

            $statement = $this->db->prepare($sql);

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'parentID'  => $parentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);


            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }



    public function getPhaseLastNumber($parentID){
        try {
            $sql = "
                SELECT
                    phaseNumber,
                    block as blockNumber
                FROM
                    tbl_propertylist
                WHERE
                    propertyParentID                     = :parentID
                AND
                    active = 1
                and
                    phaseNumber = (SELECT phaseNumber from tbl_propertylist where propertyParentID = ". $parentID ." ORDER by phaseNumber DESC limit 1)
                 ORDER BY  
                    block DESC
                LIMIT 1
            ";

            $statement = $this->db->prepare($sql);


            $statement->execute([
                    'parentID'  => $parentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }




    public function phaseNumberExist($parentID,$phaseNumber){
        try {
            $sql = "
                SELECT
                    phaseNumber,
                    block as blockNumber
                FROM
                    tbl_propertylist
                WHERE
                    propertyParentID                     = :parentID
                AND
                    phaseNumber                          = :phaseNumber 
                AND
                    active = 1
                ORDER BY 
                    phaseNumber, block DESC
                LIMIT 1
            ";

            $statement = $this->db->prepare($sql);


            $statement->execute([
                    'parentID'  => $parentID,
                    'phaseNumber' => $phaseNumber
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getPhaselastBlockNumber($parentID,$phaseNumber){
        try {
            $sql = "
                SELECT
                    phaseNumber,
                    block as blockNumber
                FROM
                    tbl_propertylist
                WHERE
                    propertyParentID                     = :parentID
                AND
                    phaseNumber                          = :phaseNumber 
                AND
                    active = 1
                ORDER BY  block DESC
                LIMIT 1
            ";

            $statement = $this->db->prepare($sql);


            $statement->execute([
                    'parentID'  => $parentID,
                    'phaseNumber' => $phaseNumber
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }




    public function phasable($parentID){
        try {
            $sql = "
                SELECT
                    isPhasable
                FROM
                    tbl_propertyparent
                WHERE
                    id                     = :parentID
                AND
                    active = 1
            ";

            $statement = $this->db->prepare($sql);


            $statement->execute([
                    'parentID'  => $parentID,
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }






    public function getAllPhases($parentID){
        try {
            $sql = "
                SELECT
                    phaseNumber,
                    CONCAT('Phase ',phaseNumber) as phaseNumber2 
                FROM
                    tbl_propertylist
                WHERE
                    propertyParentID                  = :parentID
                AND
                    active = 1
                GROUP BY
                    phaseNumber
                ORDER BY  phaseNumber ASC
            ";

            $statement = $this->db->prepare($sql);

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'parentID'  => $parentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);


            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }






    public function getAllPgetAllPropertyTypesroperty(){
        try {
            $sql = "
                SELECT
                    tbl_typeofproperty.type_id,
                    tbl_typeofproperty.type_name
                FROM
                    tbl_typeofproperty
                WHERE
                    active = 1
            ";

            $statement = $this->db->prepare($sql);

            $statement = $this->db->prepare($sql);

            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);


            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }







    public function updateThisProperty($propertyID,$add_propertyFormName,$add_propertyFormAddress){
        try {
            $sql = "
                UPDATE
                    tbl_propertyparent
                SET
                    propertyName        = :add_propertyFormName,
                    address             = :add_propertyFormAddress
                WHERE
                    id                  = :propertyID
            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute([
                    'add_propertyFormName'          => $add_propertyFormName,
                    'add_propertyFormAddress'       => $add_propertyFormAddress,
                    'propertyID'                    => $propertyID
                ]);


        } catch (PDOException $e) {
            return $e;
        }
    }


    public function removeThisProperty($propertyID){
        try {
            $sql = "
                UPDATE
                    tbl_property
                SET
                    active    = 0
                WHERE
                    property_id        = :propertyID
            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute([    
                    'propertyID'            => $propertyID
                ]);


        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getAllParentProperties(){
        try {
            $sql = "
                SELECT 
                    tbl_propertyparent.id as propertyID,
                    tbl_propertyparent.propertyName AS propertyName,
                    tbl_propertyparent.address AS propertyAddress
                FROM 
                    tbl_propertyparent
                WHERE 
                    tbl_propertyparent.active = 1

            ";

            $statement = $this->db->prepare($sql);

            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);


            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }




    public function getAllPropertyParents(){
        try {
            $sql = "
                SELECT 
                    id as propertyID,
                    propertyName AS propertyName,
                    address AS properyAddress
                FROM 
                    tbl_propertyparent
                WHERE 
                    tbl_propertyparent.active = 1

            ";

            $statement = $this->db->prepare($sql);

            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);


            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }




    public function getPropertyParentsDetails($parentID){
        try {
            $sql = "
                SELECT 
                    id as propertyID,
                    propertyName AS propertyName,
                    address AS properyAddress,
                    isPhasable AS phasable
                FROM 
                    tbl_propertyparent
                WHERE 
                    tbl_propertyparent.active = 1
                AND
                    tbl_propertyparent.id = :parentID


            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'parentID' => $parentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);


            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }



    public function  isLotOccupied($parentID,$block,$lot,$phasenumber){
        

        try {
            $sql = "
                SELECT 
                    tbl_propertylist.property_id AS ID,
                    tbl_propertylist.status AS status
                FROM 
                    tbl_propertylist
                WHERE
                    tbl_propertylist.propertyParentID = :parentID
                AND
                    tbl_propertylist.block = :block
                AND
                    tbl_propertylist.lot = :lot
                AND
                    tbl_propertylist.phaseNumber = :phasenumber
                AND
                    tbl_propertylist.status = 1
                AND
                    tbl_propertylist.active = 1
            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'parentID'          => $parentID,
                    'lot'               => $lot,
                    'phasenumber'       => $phasenumber,
                    'block'             => $block
                ]);
            
            $statement->setFetchMode(PDO::FETCH_ASSOC);


            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }






    public function  isLotExist($parentID,$block,$lot,$phasenumber){
        

        try {
            $sql = "
                SELECT 
                    tbl_propertylist.property_id AS ID,
                    tbl_propertylist.status AS status
                FROM 
                    tbl_propertylist
                WHERE
                    tbl_propertylist.propertyParentID = :parentID
                AND
                    tbl_propertylist.block = :block
                AND
                    tbl_propertylist.lot = :lot
                AND
                    tbl_propertylist.phaseNumber = :phasenumber
                AND
                    tbl_propertylist.active = 1
            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'parentID'          => $parentID,
                    'lot'               => $lot,
                    'phasenumber'       => $phasenumber,
                    'block'             => $block
                ]);
            
            $statement->setFetchMode(PDO::FETCH_ASSOC);


            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }



    public function  getThisPropertyBlockDetails($parentID,$block,$phaseNumber){
        

        try {
            $sql = "
                SELECT 
                    tbl_propertylist.property_id AS ID,
                    tbl_propertylist.propertyParentID AS parentID,
                    tbl_propertylist.propertyTypeID AS propertyTypeID,
                    tbl_typeofproperty.type_name AS propertyTypeName,
                    tbl_propertylist.block AS block,
                    tbl_propertylist.lot AS lot,
                    (tbl_propertylist.sqm * tbl_propertylist.pricePerm2) AS propertyOriginalPrice,
                    ((tbl_propertylist.sqm * tbl_propertylist.pricePerm2) / tbl_propertylist.DefaultPlanTerms) AS propertyMonthlyAmortization,
                    tbl_propertylist.sqm AS propertySQM,
                    tbl_propertylist.pricePerm2 AS propertyPricePerSQM,
                    tbl_propertylist.DefaultPlanTerms AS propertyPlanTerms,
                    tbl_propertylist.model AS propertyModel,
                    tbl_propertylist.status AS propertyStatus
                FROM 
                    tbl_propertylist
                INNER JOIN
                    tbl_typeofproperty on tbl_typeofproperty.type_id = tbl_propertylist.propertyTypeID
                WHERE
                    tbl_propertylist.propertyParentID   = :parentID
                AND
                    tbl_propertylist.block              = :block
                AND
                    tbl_propertylist.phaseNumber        = :phaseNumber
                AND
                    tbl_propertylist.active = 1
                ORDER BY
                    block, lot ASC
            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'parentID'          => $parentID,
                    'block'             => $block,
                    'phaseNumber'       => $phaseNumber,
                ]);
            
            $statement->setFetchMode(PDO::FETCH_ASSOC);


            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }



    // public function updateAndInsertThisLotPropertyDetails($parameters,$values,$toBeUpdated){

    //     try {
    //         $this->db->setAttribute(PDO::ATTR_AUTOCOMMIT,false);
    //         $this->db->beginTransaction();

    //         if(count($parameters) > 0){
    //             $sql = "
    //                 INSERT INTO 
    //                     tbl_propertylist(
    //                         lot,
    //                         propertyTypeID,
    //                         originalPrice,
    //                         monthly_amortization,
    //                         sqm,
    //                         pricePerm2,
    //                         DefaultPlanTerms,
    //                         model,
    //                         status,
    //                         propertyParentID,
    //                         phaseNumber,
    //                         block,
    //                         addedBy
    //                     )
    //                 VALUES
    //                         ".implode(',', $parameters)."
                    
    //             ";




    //             $statement = $this->db->prepare($sql);

    //             $result = $statement->execute($values);

    //             if(!$result){
    //                 $this->db->rollBack();
    //                 return false;
    //             }
    //         }

    //         $newUpdateValues = [];
    //         $sql = "";
    //         foreach ($toBeUpdated as $key => $lotDetails) {
    //             $newUpdateValues = [];
    //             $sql = "  
    //                 UPDATE 
    //                     `tbl_propertylist` 
    //                 SET
    //                     propertyTypeID         = :propertyTypeID,
    //                     originalPrice          = :originalPrice,
    //                     monthly_amortization   = :monthly_amortization,
    //                     sqm                    = :sqm,
    //                     pricePerm2             = :pricePerm2,
    //                     DefaultPlanTerms       = :DefaultPlanTerms,
    //                     model                  = :model,
    //                     status                 = :status
    //                 WHERE
    //                     propertyParentID = :parentID
    //                 AND
    //                     block = :block
    //                 AND
    //                     lot = :lot
    //                 AND
    //                     phaseNumber = :phaseNumber
    //                 AND
    //                     active = 1
    //             ";

    //             $newUpdateValues['parentID']                  = $lotDetails[0]; // parent Id
    //             $newUpdateValues['block']                     = $lotDetails[1]; // block  number
    //             $newUpdateValues['lot']                       = $lotDetails[2]; // lot number
    //             $newUpdateValues['originalPrice']             = $lotDetails[3];
    //             $newUpdateValues['monthly_amortization']      = $lotDetails[4];
    //             $newUpdateValues['sqm']                       = $lotDetails[5];
    //             $newUpdateValues['pricePerm2']                = $lotDetails[6];
    //             $newUpdateValues['DefaultPlanTerms']          = $lotDetails[7];
    //             $newUpdateValues['model' ]                    = $lotDetails[8];
    //             $newUpdateValues['status']                    = $lotDetails[9];
    //             $newUpdateValues['propertyTypeID' ]           = $lotDetails[10];
    //             $newUpdateValues['phaseNumber' ]              = $lotDetails[11]; // phaseNumber

    //             $statement = $this->db->prepare($sql);

    //             $result = $statement->execute($newUpdateValues);

    //             unset($newUpdateValues);

    //             if(!$result){
    //                 $this->db->rollBack();
    //                 return false;
    //             }
    //         }

            
            
    //         return $this->db->commit();

    //     } catch (PDOException $e) {
    //         $this->db->rollBack();
    //         return false;
    //     } catch (Exception $e) {
    //         $this->db->rollBack();
    //         return false;
    //     }
       

    // }


    public function updateAndInsertThisLotPropertyDetails($parameters,$values,$toBeUpdated){

        try {
            $this->db->setAttribute(PDO::ATTR_AUTOCOMMIT,false);
            $this->db->beginTransaction();

            if(count($parameters) > 0){
                $sql = "
                    INSERT INTO 
                        tbl_propertylist(
                            lot,
                            propertyTypeID,
                            originalPrice,
                            monthly_amortization,
                            sqm,
                            pricePerm2,
                            DefaultPlanTerms,
                            model,
                            status,
                            propertyParentID,
                            block,
                            phaseNumber,
                            addedBy
                        )
                    VALUES
                            ".implode(',', $parameters)."
                    
                ";


                $statement = $this->db->prepare($sql);

                $result = $statement->execute($values);

                if(!$result){
                    $this->db->rollBack();
                    return false;
                }
            }

            $newUpdateValues = [];
            $sql = "";
            foreach ($toBeUpdated as $key => $lotDetails) {
                $newUpdateValues = [];
                $sql = "  
                    UPDATE 
                        `tbl_propertylist` 
                    SET
                        propertyTypeID         = :propertyTypeID,
                        sqm                    = :sqm,
                        pricePerm2             = :pricePerm2,
                        DefaultPlanTerms       = :DefaultPlanTerms,
                        model                  = :model,
                        status                 = :status
                        
                    WHERE
                        propertyParentID = :parentID
                    AND
                        block = :block
                    AND
                        lot = :lot
                    AND
                        phaseNumber = :phaseNumber
                    AND
                        active = 1
                ";

                $newUpdateValues['parentID']                  = $lotDetails[0]; // parent Id
                $newUpdateValues['block']                     = $lotDetails[1]; // block  number
                $newUpdateValues['lot']                       = $lotDetails[2]; // lot number
                $newUpdateValues['sqm']                       = $lotDetails[3];
                $newUpdateValues['pricePerm2']                = $lotDetails[4];
                $newUpdateValues['DefaultPlanTerms']          = $lotDetails[5];
                $newUpdateValues['model' ]                    = $lotDetails[6];
                $newUpdateValues['status']                    = $lotDetails[7];
                $newUpdateValues['propertyTypeID' ]           = $lotDetails[8];
                $newUpdateValues['phaseNumber' ]              = $lotDetails[9]; // phaseNumber

                $statement = $this->db->prepare($sql);

                $result = $statement->execute($newUpdateValues);

                unset($newUpdateValues);

                if(!$result){
                    $this->db->rollBack();
                    return false;
                }
            }

            
            
            return $this->db->commit();

        } catch (PDOException $e) {
            $this->db->rollBack();
            return false;
        } catch (Exception $e) {
            $this->db->rollBack();
            return false;
        }
       

    }



    public $SQLs = "";

    public function getPropertyDetailsForThisSet($conditions,$values){

        try {
            $sql = "
                SELECT 
                    now() AS dateNow,
                    tbl_propertylist.property_id AS ID,
                    tbl_propertylist.propertyParentID AS parentID,
                    tbl_propertyparent.propertyName as propertyName,
                    tbl_propertyparent.address as propertyAddress,
                    tbl_propertylist.propertyTypeID AS propertyTypeID,
                    tbl_typeofproperty.type_name AS propertyTypeName,
                    tbl_propertylist.phaseNumber,
                    tbl_propertylist.block AS block,
                    tbl_propertylist.lot AS lot,
                    tbl_propertylist.originalPrice AS propertyOriginalPrice,
                    tbl_propertylist.monthly_amortization AS propertyMonthlyAmortization,
                    tbl_propertylist.sqm AS propertySQM,
                    tbl_propertylist.pricePerm2 AS propertyPricePerSQM,
                    tbl_propertylist.DefaultPlanTerms AS propertyPlanTerms,
                    tbl_propertylist.model AS propertyModel,
                    tbl_propertylist.status AS propertyStatus
                FROM 
                    tbl_propertylist
                INNER JOIN
                    tbl_typeofproperty on tbl_typeofproperty.type_id = tbl_propertylist.propertyTypeID
                INNER JOIN
                    tbl_propertyparent on tbl_propertyparent.id = tbl_propertylist.propertyParentID
                WHERE
                    ".implode(' or ', $conditions)."
                AND
                    tbl_propertylist.active = 1
                AND
                    tbl_propertylist.status = 0
                ORDER BY
                    propertyName,block,lot ASC

            ";

            $this->SQLs = $sql;
            $statement = $this->db->prepare($sql);
            $statement->execute($values);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getBlockCount($parentID){
        try{
            $sql="
            SELECT DISTINCT 
            propertyParentID,
            block 
            FROM
            tbl_propertylist 
            INNER JOIN tbl_propertyparent 
                ON tbl_propertylist.`propertyParentID` = tbl_propertyparent.id 
            WHERE tbl_propertylist.`propertyParentID` = :parentID 
                ";
            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'parentID'  => $parentID,
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        }catch(PDOException $e){
                return $e;
        }
    }

    public function getFirstBlockCount(){
        try{
            $sql="
            SELECT DISTINCT 
            block 
            FROM
            tbl_propertylist 
            INNER JOIN tbl_propertyparent 
                ON tbl_propertylist.`propertyParentID` = tbl_propertyparent.id 
            WHERE tbl_propertylist.`propertyParentID` = 1 
                ";
            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        }catch(PDOException $e){
                return $e;
        }
    }

    public function getPropertyDetailsByBlock($parentID,$blockNo){
        try{
            $sql= "
            SELECT 
            tbl_propertylist.property_id AS ID,
            tbl_propertylist.propertyParentID AS parentID,
            tbl_propertylist.propertyTypeID AS propertyTypeID,
            tbl_typeofproperty.type_name AS propertyTypeName,
            tbl_propertylist.phaseNumber AS phaseNumber,
            tbl_propertylist.block AS block,
            tbl_propertylist.lot AS lot,
            (tbl_propertylist.sqm * tbl_propertylist.pricePerm2) AS propertyOriginalPrice,
            (((tbl_propertylist.sqm * tbl_propertylist.pricePerm2) - clientOwner.downPayment )/ tbl_propertylist.DefaultPlanTerms) AS propertyMonthlyAmortization,
            tbl_propertylist.sqm AS propertySQM,
            tbl_propertylist.pricePerm2 AS propertyPricePerSQM,
            tbl_propertylist.DefaultPlanTerms AS propertyPlanTerms,
            tbl_propertylist.model AS propertyModel,
            clientOwner.clientName,
            clientOwner.additionalCharges,      
            tbl_propertylist.status AS propertyStatus,
            clientOwner.downPayment
        FROM 
            tbl_propertylist
        INNER JOIN
            tbl_typeofproperty ON tbl_typeofproperty.type_id = tbl_propertylist.propertyTypeID
        LEFT JOIN
            (
            SELECT 
                tbl_client_properties.property_id,
                tbl_client_properties.additionalCharges,
                CONCAT(tbl_client.Fname,' ',tbl_client.Lname) AS clientName,
                tbl_client_properties.`downpayment` AS downPayment
            FROM 
                tbl_client_properties 
            INNER JOIN 
                tbl_client ON tbl_client.client_id = tbl_client_properties.client_id
            WHERE 
                tbl_client_properties.active = 1
            ) clientOwner ON clientOwner.property_id = tbl_propertylist.property_id
        WHERE
            tbl_propertylist.propertyParentID = :parentID
            AND 
            tbl_propertylist.block = :blockNo
        AND
            tbl_propertylist.active = 1
        ORDER BY
            phaseNumber, block, lot ASC
            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'parentID'  => $parentID,
                    'blockNo'   => $blockNo
                ]);
            
            $statement->setFetchMode(PDO::FETCH_ASSOC);


            return $statement->fetchAll();
        }catch(PDOException $e){
                return $e;
        }
    }


    public function getPropertyDetails($parentID){
       

        try {
            $sql = "
                SELECT 
                    tbl_propertylist.property_id AS ID,
                    tbl_propertylist.propertyParentID AS parentID,
                    tbl_propertylist.propertyTypeID AS propertyTypeID,
                    tbl_typeofproperty.type_name AS propertyTypeName,
                    tbl_propertylist.phaseNumber AS phaseNumber,
                    tbl_propertylist.block AS block,
                    tbl_propertylist.lot AS lot,
                    (tbl_propertylist.sqm * tbl_propertylist.pricePerm2) AS propertyOriginalPrice,
                    ((tbl_propertylist.sqm * tbl_propertylist.pricePerm2) / tbl_propertylist.DefaultPlanTerms) AS propertyMonthlyAmortization,
                    tbl_propertylist.sqm AS propertySQM,
                    tbl_propertylist.pricePerm2 AS propertyPricePerSQM,
                    tbl_propertylist.DefaultPlanTerms AS propertyPlanTerms,
                    tbl_propertylist.model AS propertyModel,
                    clientOwner.clientName,
                    clientOwner.additionalCharges,      
                    tbl_propertylist.status AS propertyStatus
                FROM 
                    tbl_propertylist
                INNER JOIN
                    tbl_typeofproperty on tbl_typeofproperty.type_id = tbl_propertylist.propertyTypeID
                LEFT JOIN
                    (
                    SELECT 
                        tbl_client_properties.property_id,
                        tbl_client_properties.additionalCharges,
                        CONCAT(tbl_client.Fname,' ',tbl_client.Lname) AS clientName
                    FROM 
                        tbl_client_properties 
                    INNER JOIN 
                        tbl_client on tbl_client.client_id = tbl_client_properties.client_id
                    where 
                        tbl_client_properties.active = 1
                    ) clientOwner on clientOwner.property_id = tbl_propertylist.property_id
                WHERE
                    tbl_propertylist.propertyParentID = :parentID
                AND
                    tbl_propertylist.active = 1
                ORDER BY
                    phaseNumber, block, lot ASC
            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'parentID'  => $parentID,
                ]);
            
            $statement->setFetchMode(PDO::FETCH_ASSOC);


            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAllProperty($typeID){
        try {
            $sql = "
                SELECT 
                    tbl_propertyparent.propertyName AS '0',
                    tbl_propertyparent.address AS '1',
                    CONCAT(IF(tbl_propertylist.phaseNumber = 0,'', CONCAT('Phase ',tbl_propertylist.phaseNumber)),' Block <strong>',tbl_propertylist.block,'</strong> Lot <strong>',tbl_propertylist.lot,'</strong>') AS '2',
                    tbl_propertylist.sqm AS '3',
                    tbl_propertylist.pricePerm2 AS '4',
                    tbl_propertylist.DefaultPlanTerms AS '5',
                    clientOwner.additionalCharges AS '6',
                    tbl_propertylist.model AS '7',
                    ((tbl_propertylist.sqm * tbl_propertylist.pricePerm2) / tbl_propertylist.DefaultPlanTerms ) AS '8',
                    tbl_typeofproperty.type_name AS '9',
                    clientOwner.clientName as '10',
                    CONCAT('<span class=\"label label-sm ',IF(tbl_propertylist.status = 0,'label-info',IF(tbl_propertylist.status = 1 , 'label-danger' , 'label-warning')),'\"> ',IF(tbl_propertylist.status = 0,'AVAILABLE',IF(tbl_propertylist.status = 1 , 'Occupied' , 'Disabled')),' </span>',IF(tbl_propertylist.status = 1,CONCAT('<button class=\"btn btn-outline green btnViewClientProperyPaymentHistory btn-xs\"  clientID=\"',clientOwner.client_id,'\"  data-clientpropertyID=\"',tbl_propertylist.property_id,'\" style=\"margin-left: 3px\" >View History</button><button class=\"btn btn-outline green btn_AssumeTo btn-xs\"  clientID=\"',clientOwner.client_id,'\"  data-clientpropertyID=\"',clientOwner.cp_id,'\" style=\"margin-left: 3px\" > Assume to</button>'),'')) AS '11'
                FROM 
                    tbl_propertylist
                INNER JOIN 
                    tbl_typeofproperty on  tbl_typeofproperty.type_id = tbl_propertylist.propertyTypeID
                INNER JOIN 
                    tbl_propertyparent on  tbl_propertyparent.id = tbl_propertylist.propertyParentID  
                LEFT JOIN
                    (
                    SELECT 
                        tbl_client_properties.cp_id,
                        tbl_client_properties.property_id,
                        tbl_client_properties.client_id,
                        tbl_client_properties.additionalCharges,
                        CONCAT(tbl_client.Fname,' ',tbl_client.Lname) AS clientName
                    FROM 
                        tbl_client_properties 
                    INNER JOIN 
                        tbl_client on tbl_client.client_id = tbl_client_properties.client_id 
                    where  tbl_client_properties.active = 1
                    ) clientOwner on clientOwner.property_id = tbl_propertylist.property_id  
                WHERE 
                    tbl_propertylist.active = 1
                AND 
                    tbl_propertylist.propertyTypeID = :typeID

            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'typeID'          => $typeID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);


            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }

      public function getAllPropertyGues($list, $typeID){
        try {
            $sql = "
                SELECT 
                    tbl_propertyparent.propertyName AS '0',
                    tbl_propertyparent.address AS '1',
                    CONCAT(IF(tbl_propertylist.phaseNumber = 0,'', CONCAT('Phase ',tbl_propertylist.phaseNumber)),' Block <strong>',tbl_propertylist.block,'</strong> Lot <strong>',tbl_propertylist.lot,'</strong>') AS '2',
                    tbl_propertylist.sqm AS '3',
                    tbl_propertylist.pricePerm2 AS '4',
                    tbl_propertylist.DefaultPlanTerms AS '5',
                    clientOwner.additionalCharges AS '6',
                    tbl_propertylist.model AS '7',
                    ((tbl_propertylist.sqm * tbl_propertylist.pricePerm2) / tbl_propertylist.DefaultPlanTerms ) AS '8',
                    tbl_typeofproperty.type_name AS '9',
                    clientOwner.clientName as '10',
                    CONCAT('<span class=\"label label-sm ',IF(tbl_propertylist.status = 0,'label-info',IF(tbl_propertylist.status = 1 , 'label-danger' , 'label-warning')),'\"> ',IF(tbl_propertylist.status = 0,'AVAILABLE',IF(tbl_propertylist.status = 1 , 'Occupied' , 'Disabled')),' </span>',IF(tbl_propertylist.status = 1,CONCAT('<button class=\"btn btn-outline green btnViewClientProperyPaymentHistory btn-xs\"  clientID=\"',clientOwner.client_id,'\"  data-clientpropertyID=\"',tbl_propertylist.property_id,'\" style=\"margin-left: 3px\" >View History</button>'),'')) AS '11'
                FROM 
                    tbl_propertylist
                INNER JOIN 
                    tbl_typeofproperty on  tbl_typeofproperty.type_id = tbl_propertylist.propertyTypeID
                INNER JOIN 
                    tbl_propertyparent on  tbl_propertyparent.id = tbl_propertylist.propertyParentID  
                LEFT JOIN
                    (
                    SELECT 
                        tbl_client_properties.cp_id,
                        tbl_client_properties.property_id,
                        tbl_client_properties.client_id,
                        tbl_client_properties.additionalCharges,
                        CONCAT(tbl_client.Fname,' ',tbl_client.Lname) AS clientName
                    FROM 
                        tbl_client_properties 
                    INNER JOIN 
                        tbl_client on tbl_client.client_id = tbl_client_properties.client_id 
                    where  tbl_client_properties.active = 1
                    ) clientOwner on clientOwner.property_id = tbl_propertylist.property_id  
                WHERE 
                    tbl_propertylist.active = 1
                AND 
                    tbl_propertylist.propertyTypeID = :typeID
                and
                    tbl_propertylist.propertyParentID in (" . implode(',', $list) . ")";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'typeID'          => $typeID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);


            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getAllBlocksForThisProperty($parentID){
        try {

            $sql = "
                SELECT
                    tbl_propertylist.phaseNumber as phaseNumber, 
                    tbl_propertylist.propertyParentID as parentID,
                    tbl_propertylist.block as blockNumber,
                    concat(IF(tbl_propertylist.phaseNumber = 0,'',CONCAT('PHASE ',tbl_propertylist.phaseNumber)),' Block ',tbl_propertylist.block) as blockNumberName
                FROM 
                    tbl_propertylist
                WHERE 
                    tbl_propertylist.active = 1
                AND 
                    tbl_propertylist.propertyParentID = :parentID
                GROUP BY
                    tbl_propertylist.phaseNumber,tbl_propertylist.block,
                    tbl_propertylist.propertyParentID
                ORDER BY
                    tbl_propertylist.phaseNumber,tbl_propertylist.block

            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'parentID'          =>$parentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);


            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }



    public function getAllLotsForThisProperty($parentID,$blockNumber,$phaseNumber){
        try {
            $sql = "
                SELECT 
                    tbl_propertylist.propertyParentID as parentID,
                    tbl_propertylist.phaseNumber as phaseNumber,
                    tbl_propertylist.block as blockNumber,
                    tbl_propertylist.lot as lotNumber,
                    tbl_propertylist.model as propertyModel,
                    tbl_propertylist.propertyTypeID as propertyTypeID,
                    tbl_typeofproperty.type_name as propertyTypeName,
                    tbl_propertylist.status as lotStatus,
                    concat('Block ',tbl_propertylist.block) as blockNumberName,
                    concat('Lot ',tbl_propertylist.lot) as LotNumberName
                FROM 
                    tbl_propertylist
                INNER JOIN
                    tbl_typeofproperty ON tbl_typeofproperty.type_id = tbl_propertylist.propertyTypeID
                WHERE 
                    tbl_propertylist.active = 1
                AND 
                    tbl_propertylist.propertyParentID= :parentID
                AND 
                    tbl_propertylist.block= :blockNumber
                AND 
                    tbl_propertylist.phaseNumber= :phaseNumber

            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'parentID'                          =>$parentID,
                    'phaseNumber'                       =>$phaseNumber,
                    'blockNumber'                       =>$blockNumber
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);


            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAgentList()
    {
        try {
            $sql="
                select client_id as 'id', concat(fname, ' ', lname) as 'name' from tbl_client where active = 1
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function insertNewClient($data, $addedBy)
    {
        try { 
           $sql = "
                INSERT INTO
                    tbl_client(
                        Fname,
                        Lname,
                        Mname,
                        Address,
                        ContactNumber,
                        Email,
                        addedBy
                    )
                VALUES(
                    :addClient_fname,
                    :addClient_lname,
                    :addClient_mname,
                    :addClient_address,
                    :addClient_contactnumber,
                    :addClient_email,
                    :addedByUserID
                )
            ";

            $statement = $this->db->prepare($sql);
            return $result =  $statement->execute([
                    'addClient_fname'               => $data[0],
                    'addClient_lname'               => $data[1],
                    'addClient_mname'               => $data[2],
                    'addClient_address'             => $data[3],
                    'addClient_contactnumber'       => $data[4],
                    'addClient_email'               => $data[5],
                    'addedByUserID'                 => $addedBy
                ]);

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function assumeToThisClient($data, $cpID)
    {
        try {
            $this->db->setAttribute(PDO::ATTR_AUTOCOMMIT,false);
            $this->db->beginTransaction();
            $sql = "
                update 
                    tbl_client_properties, tbl_clientpaymenthistory
                    set
                        tbl_client_properties.client_id =". $data .",
                        tbl_clientpaymenthistory.client_id =". $data ."
                where
                    tbl_client_properties.cp_id = tbl_clientpaymenthistory.cp_id
                and
                    tbl_clientpaymenthistory.cp_id =:cpID
            ";

            $statement = $this->db->prepare($sql);
            $result =  $statement->execute([
                    'cpID'        => $cpID
                ]);
            return $this->db->commit();
        } catch (Exception $e) {
           $this->db->rollBack();
           return $e;
        }        
    }

   
    public function getPropertyParents(){
        try {
            $sql = "
                select 
                    id,
                    propertyName as 'text'
                FROM 
                    tbl_propertyparent
                WHERE 
                    tbl_propertyparent.active = 1";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getPropertyDetailList($propId)
    {
        try {
            $sql= "select 
                    property_id as 'id',
                    if(phaseNumber = 0, concat('Block', ' ', block, ' Lot', ' ', lot), concat('Ph ', phaseNumber,  ' Block', ' ', block, ' Lot', ' ', lot)) as 'text'
                FROM 
                    tbl_propertylist
                WHERE 
                    active = 1
                and 
                    propertyParentID =:propId ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'propId' => $propId
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getPropertiesGues($userID)
    {
        try {

            $sql = "select property from tbl_businesspartner where id = any (select refID from tbl_users where UserID =:id)";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id' => $userID
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }




}



