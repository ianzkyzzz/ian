<?php

namespace App\Models;

use PDO;

class User {

    /**
    * @var $db
    */
    private $db;

    public function __construct(PDO $db) {
        $this->db = $db;
    }



    public function saveThisIP($isp) {
        try {
            $sql = "
                INSERT INTO
                    tbl_ips(ips)
                VALUES
                    (:isp);
            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute([
                    'isp'       => $isp
                ]);


        } catch (PDOException $e) {
            return $e;
        }
    }


    public function isUserExist($username) {
        try {
            $sql = "
                SELECT
                    *
                FROM
                    tbl_users
                WHERE
                    Username = :username
            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'username'      => $username
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            $results = $statement->fetchAll();

            if(count($results) >= 1){
                return true;
            } else {
                return false;
            }

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getMonthList(){
        try {
            $sql="
                SELECT
                    DISTINCT(MONTH(dateAdded)) as 'month'
                from
                     tbl_clientpaymenthistory
                WHERE
                    active = 1
                ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getTotalSalesForEachMonths($value){
        try {
            $sql="
                SELECT
                    sum(debit + credit) as 'Sales',
                        concat(DAY(dateAdded),'/',MONTH(dateAdded),'/', YEAR(dateAdded))
                    as 'Month'
                from
                    tbl_clientpaymenthistory
                where
                    MONTH(dateAdded) = :month
                and
                    active = 1
                group by
                    Month

            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'month' => $value
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        } catch (PDOException $e) {
            return $e;
        }
    }


    public function propertyReports()
    {
        try {

            $sql = "select propertyName as 'prop', (select sum(debit + credit) from tbl_clientpaymenthistory where cp_id = any (select cp_id from tbl_client_properties where property_id = any(select property_id from tbl_propertylist where propertyParentID = tbl_propertyparent.id))) as 'sum' from tbl_propertyparent WHERE active = 1";
            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }
    
    public function propertyReportsGuest($list)
    {
        try {

            $sql = "select propertyName as 'prop', (select sum(debit + credit) from tbl_clientpaymenthistory where cp_id = any (select cp_id from tbl_client_properties where property_id = any(select property_id from tbl_propertylist where propertyParentID = tbl_propertyparent.id))) as 'sum' from tbl_propertyparent where id in (" . implode(',', $list) . ")";
            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function propertyReportsSubDev()
    {
        try {
            
            $sql = "
                SELECT 
                    propertyName AS 'prop',
                    id AS 'propID',
                    (SELECT 
                        SUM(debit + credit) 
                    FROM
                        tbl_clientpaymenthistory 
                    WHERE cp_id = ANY 
                        (SELECT 
                        cp_id 
                        FROM
                        tbl_client_properties 
                        WHERE property_id = ANY 
                        (SELECT 
                            property_id 
                        FROM
                            tbl_propertylist 
                        WHERE propertyParentID = tbl_propertyparent.id))) AS 'sum',
                    (SELECT 
                        PercentageA 
                    FROM
                        tbl_propsubpercetage 
                    WHERE propId = tbl_propertyparent.id) AS 'A',
                    (SELECT 
                        PercentageB 
                    FROM
                        tbl_propsubpercetage 
                    WHERE propId = tbl_propertyparent.id) AS 'B'
                    ,
                    (SELECT 
                        PercentageC
                    FROM
                        tbl_propsubpercetage 
                    WHERE propId = tbl_propertyparent.id) AS 'C'
                    
                    FROM
                    tbl_propertyparent 
                    WHERE active = 1
            ";
            
            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function propertyReportsSubDevGuest($list)
    {
        try {
            
            $sql = "select propertyName as 'prop', id as 'propID', (select sum(debit + credit) from tbl_clientpaymenthistory where cp_id = any (select cp_id from tbl_client_properties where property_id = any(select property_id from tbl_propertylist where propertyParentID = tbl_propertyparent.id))) as 'sum', (select PercentageA from tbl_propsubpercetage where propId = tbl_propertyparent.id) as 'A',  (select PercentageB from tbl_propsubpercetage where propId = tbl_propertyparent.id) as 'B' from tbl_propertyparent where id in (" . implode(',', $list) . ")";
            
            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getClientsCount(){
        try {
            $sql="
                SELECT
                    count(client_id) as 'clientCount'
                from
                    tbl_client
                where
                    active =1
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();
        } catch (PDOException $e) {

        }
    }


    public function getClientsCountGuest($userID){
        try {
            $sql="
               select distinct(client_id) as 'id' from tbl()
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();
        } catch (PDOException $e) {

        }
    }

    public function getPropertiesGues($userID)
    {
        try {

            $sql = "select property from tbl_businesspartner where id = any (select refID from tbl_users where UserID =:id)";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id' => $userID
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getClientList($propID)
    {
        try {

            $sql = "select distinct(client_id) as 'clientID' from tbl_client_properties a left join tbl_propertylist b on a.property_id = b.property_id where b.propertyParentID =:id and a.active = 1";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id' => $propID
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }


    public function getAgentCount(){
       try {
            $sql="
                SELECT
                    count(agent_id) as 'agentCount'
                from
                    tbl_agent
                where 
                    active = 1
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();
        } catch (PDOException $e) {

        }
    }


    public function getSumSales(){
        try {
            $sql="
                SELECT
                        if(sum(debit + credit) is null, 0.00, sum(debit + credit))  as 'Sales'
                    from
                        tbl_clientpaymenthistory
                    where
                        date(dateAdded) = DATE(now())
                    and
                        active = 1

                union all

                SELECT
                        if(sum(debit + credit) is null, 0.00, sum(debit + credit)) as 'Sales'
                    from
                        tbl_clientpaymenthistory
                    where
                        week(dateAdded) = week(date(now()))
                    and
                        active = 1
               union all

               SELECT
                        if(sum(debit + credit) is null, 0.00, sum(debit + credit))
                    from
                        tbl_clientpaymenthistory
                    where
                        month(dateAdded) = month(DATE(now()))
                    and
                        active = 1 
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();
        } catch (PDOException $e) {

        }
    }


    public function getSumSalesGuest($propID){
        try {
            $sql="
                SELECT
                        if(sum(debit + credit) is null, 0.00, sum(debit + credit))  as 'Sales'
                  from
                        tbl_clientpaymenthistory 
                    where
                        cp_id = any (select distinct(c.cp_id) from tbl_client_properties c left join tbl_propertylist b on c.property_id = b.property_id where c.active = 1 and b.propertyParentID = " . $propID . ")
                    and
                        date(dateAdded) = DATE(now())
                    and
                        active = 1

                union all

                SELECT
                        if(sum(debit + credit) is null, 0.00, sum(debit + credit)) as 'Sales'
                    from
                        tbl_clientpaymenthistory
                    where
                        cp_id = any (select distinct(c.cp_id) from tbl_client_properties c left join tbl_propertylist b on c.property_id = b.property_id where c.active = 1 and b.propertyParentID = " . $propID . ")
                    and
                        week(dateAdded) = week(date(now()))
                    and
                        active = 1
               union all

               SELECT
                        if(sum(debit + credit) is null, 0.00, sum(debit + credit))
                    from
                        tbl_clientpaymenthistory
                    where
                         cp_id = any (select distinct(c.cp_id) from tbl_client_properties c left join tbl_propertylist b on c.property_id = b.property_id where c.active = 1 and b.propertyParentID = " . $propID . ")
                    and
                        month(dateAdded) = month(DATE(now()))
                    and
                        active = 1 
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();
        } catch (PDOException $e) {

        }
    }






    public function getUserByID($id){
        try {
            $sql = "
                SELECT
                    tbl_users.Position,
                    tbl_users.UserID
                FROM
                    tbl_users
                WHERE
                    tbl_users.Active = 1
                AND
                   tbl_users.UserID = :id
            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'id'        => $id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }




    public function getUserByRoleActive($role){
        try {
            $sql = "
                SELECT
                        tbl_users.Position,
                FROM
                    tbl_users
                WHERE
                    tbl_users.Active = 1
                AND
                   tbl_reports_users.Role = :role
            ";


            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'role'      => $role
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    // -------------------------------------------------------------------------
    //  User List
    // -------------------------------------------------------------------------
    public function getUserList() {
        try {
            $sql = "
                SELECT
                    CONCAT(tbl_users.Fname,' ',tbl_users.Mname,' ',tbl_users.Lname) as '0',
                    tbl_users.Position as '1',
                    tbl_users.Email as '2',
                    tbl_users.Username as '3',
                    CONCAT('<button type=\"button\" class=\"btn btn-outline green btn_updateThisEmployee\" data-id=\"',tbl_users.UserID,'\" ><i class=\"icon-pencil\"  ></i> View</button><button type=\"button\" class=\"btn btn-outline red btn_removeThisEmployee\" data-id=\"',tbl_users.UserID,'\" data-position=\"',tbl_users.Position,'\"><i class=\"icon-close\" ></i> Remove</button>') as '4'
                FROM
                    tbl_users
                WHERE
                    tbl_users.Active = 1
            ";

            $statement = $this->db->prepare($sql);

            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }



    public function updateUserImagePath($fileName,$UserID){

        try {
            $sql = "
                UPDATE
                    tbl_users
                SET
                    img_filename                  = :image_filename
                WHERE
                    UserID                          = :UserID

            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute([
                    'image_filename'            => $fileName,
                    'UserID'                    => $UserID
                ]);


        } catch (PDOException $e) {
            return $e;
        }

    }



    public function getThisUser($id) {
        try {
            $sql = "
                SELECT
                    UserID          as      'ID' ,
                    Fname           as      'user_fname',
                    Lname           as      'user_lname' ,
                    Mname           as      'user_mname',
                    Address         as      'user_address',
                    Email           as      'user_email' ,
                    ContactNumber   as      'user_contactNumber',
                    img_filename    as      'user_imageFilename',
                    Position        as      'user_position',
                    Username        as      'user_username'
                FROM
                    tbl_users
                WHERE
                    tbl_users.Active = 1
                AND
                    tbl_users.UserID = :id
            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'id' => $id
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }




    // -------------------------------------------------------------------------
    //  Add User
    // -------------------------------------------------------------------------
    public function postUser($Fname, $Lname, $Mname, $Address, $ContactNumber,$Email,$Position,$Username,$pass_hash) {

        try {
            $sql = "
                INSERT INTO
                    tbl_users
                    (
                        Fname,
                        Lname,
                        Mname,
                        Address,
                        ContactNumber,
                        Email,
                        Position,
                        Username,
                        Password
                    )
                VALUES
                    (
                        :Fname,
                        :Lname,
                        :Mname,
                        :Address,
                        :ContactNumber,
                        :Email,
                        :Position,
                        :Username,
                        :Password
                    )
            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute([
                    'Fname'         => $Fname,
                    'Lname'         => $Lname,
                    'Mname'         => $Mname,
                    'Address'       => $Address,
                    'ContactNumber' => $ContactNumber,
                    'Email'         => $Email,
                    'Position'      => $Position,
                    'Username'      => $Username,
                    'Password'      => $pass_hash
                ]);

        } catch (PDOException $e) {
            return $e;
        }
    }

    // -------------------------------------------------------------------------
    //  Update User
    // -------------------------------------------------------------------------
    public function updateUserInfo($ID,$Fname, $Lname, $Mname, $Address, $ContactNumber,$Email,$Position) {

        try {
            $sql = "
                UPDATE
                    tbl_users
                SET
                    Fname           = :Fname,
                    Lname           = :Lname,
                    Mname           = :Mname,
                    Address         = :Address,
                    ContactNumber   = :ContactNumber,
                    Email           = :Email,
                    Position        = :Position
                WHERE
                    UserID          = :ID
            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute([
                    'ID'            => $ID,
                    'Fname'         => $Fname,
                    'Lname'         => $Lname,
                    'Mname'         => $Mname,
                    'Address'       => $Address,
                    'ContactNumber' => $ContactNumber,
                    'Email'         => $Email,
                    'Position'      => $Position
                ]);

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function updateUserInfoGues($ID,$Fname, $Lname, $Mname, $Address, $ContactNumber,$Email,$Position) {

        try {
            $sql = "
                UPDATE
                    tbl_users
                SET
                    Fname           = :Fname,
                    Lname           = :Lname,
                    Mname           = :Mname,
                    Address         = :Address,
                    ContactNumber   = :ContactNumber,
                    Email           = :Email
                WHERE
                    UserID          = :ID
            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute([
                    'ID'            => $ID,
                    'Fname'         => $Fname,
                    'Lname'         => $Lname,
                    'Mname'         => $Mname,
                    'Address'       => $Address,
                    'ContactNumber' => $ContactNumber,
                    'Email'         => $Email
                ]);

        } catch (PDOException $e) {
            return $e;
        }
    }




    public function updateUserLoginCredential($ID,$pass_hash) {

        try {
            $sql = "
                UPDATE
                    tbl_users
                SET
                    -- Username        = :Username,
                    Password        = :Password
                WHERE
                    UserID          = :ID
            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute([
                    'ID'            => $ID,
                    // 'Username'      => $Username,
                    'Password'      => $pass_hash
                ]);

        } catch (PDOException $e) {
            return $e;
        }
    }

    // -------------------------------------------------------------------------
    //  Soft Remove User
    // -------------------------------------------------------------------------
    public function removeUser($userId) {
        try {
            $sql = "
                UPDATE
                    tbl_users
                SET
                    Active = 0
                WHERE
                    UserID = :userId
            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute([
                    'userId'    => $userId
                ]);

        } catch (PDOException $e) {
            return $e;
        }
    }


    public function updatePassword($newPasswordHashed, $userId) {
        try {
            $sql = "
                UPDATE
                    tbl_reports_users
                SET
                    Password    = :newPasswordHashed
                WHERE
                    UserID      = :userId
            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute([
                    'newPasswordHashed' => $newPasswordHashed,
                    'userId'            => $userId
                ]);

        } catch (PDOException $e) {
            return $e;
        }
    }


    public function accessLogin($username,$password){
        try {
            $sql = "
                SELECT
                    userID,
                    Role,
                    Department
                FROM
                    tbl_reports_users
                WHERE
                    Username            = :username AND
                    Password            = :password AND
                    tbl_reports_users.active    = 1
            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'password' => $password,
                    'username' => $username
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            $results = $statement->fetchAll();

            if(count($results) === 1){
                return $results[0];
            } else {
                return false;
            }
        } catch(PDOException $e) {
            return $e;
        }
    }


    public function getAllHashWithUsername($username){
        try {
            $sql = "
                SELECT
                    *
                FROM
                    tbl_users
                WHERE
                    tbl_users.Username            = :username
                AND
                    tbl_users.Active              = 1
            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'username' => $username
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();


        } catch(PDOException $e) {
            return $e;
        }
    }

     public function getAllHashWithUserID($userId){
        try {
            $sql = "
                SELECT
                    *
                FROM
                    tbl_reports_users
                INNER JOIN
                    tbl_employee on tbl_reports_users.EmployeeID = tbl_employee.EmployeeID
                INNER JOIN
                    tbl_departments on tbl_employee.DepartmentID = tbl_departments.DepartmentID
                INNER JOIN
                    tbl_positions on tbl_employee.PositionID = tbl_positions.PositionID
                WHERE
                    tbl_reports_users.UserID            = :userId
                AND
                    tbl_reports_users.active    = 1
            ";

            $statement = $this->db->prepare($sql);

            $statement->execute([
                    'userId'    => $userId
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();


        } catch(PDOException $e) {
            return $e;
        }
    }

    public function getGrossRel()
    {
        try {

            $sql = "select if(sum(amount) is null, 0.00, sum(amount)) as 'gross' from commisionrelease where date(ReleaseDate) = date(now())";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function checkPercentageVal($propID)
    {
        try {
            $sql = "select id from tbl_propsubpercetage where propId =:id";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'id' => $propID
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }

    public function inserNewPercentage($propID, $val, $dev)
    {
        try {
            $sql = "insert into tbl_propsubpercetage(propID, PercentageA, PercentageB)
                    values(:id, :val, 0)";

            if($dev == '2'){
                $sql = "insert into tbl_propsubpercetage(propID, PercentageA, PercentageB)
                values(:id, 0, :val) ";
            }


            $statement = $this->db->prepare($sql);
            return $statement->execute([
                'id'  => $propID,
                'val' => $val
            ]);
        
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function changeThisPercentageVal($propID, $val, $dev)
    {
        try {

            $sql = "UPDATE tbl_propsubpercetage SET PercentageA = :val WHERE propId = :id";
            if($dev == '2'){
            $sql = "UPDATE tbl_propsubpercetage  SET PercentageB = :val WHERE  propId = :id";
            }
            if ($dev == '3'){
            $sql = "UPDATE tbl_propsubpercetage SET PercentageC = :val  WHERE propId = :id";
            }
            $statement = $this->db->prepare($sql);
            return  $statement->execute([
                'id'  => $propID,
                'val' => $val
            ]);

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function validatePercentageValue($propID,$dev){
        try{
            $sql="";
            if ($dev == 1){
                $sql="SELECT percentageB AS '1',percentageC AS '2' FROM tbl_propsubpercetage WHERE propID = :id";
            }else if ($dev == 2){
                $sql="SELECT percentageA AS '1' , percentageC AS '2' FROM tbl_propsubpercetage WHERE propID = :id";
            }else if ($dev == 3){
                $sql="SELECT percentageA AS '1', percentageB AS '2' FROM tbl_propsubpercetage WHERE propID = :id";
            }
            $statement = $this->db->prepare($sql);
            //wew
            $statement->execute([
                'id' => $propID
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
        }catch(PDOException $e){
            return $e;
        }
    }

    public function getTax()
    {
        try {

            $sql = "SELECT 0 AS 'tax'
            UNION ALL 
          SELECT IF ( SUM(((contractPrice / b.commissionReleased) * ( c.percentagevalue /  100 )) * .05) IS NULL, 0.00, (SUM(((contractPrice / b.commissionReleased) * ( c.percentagevalue /  100 )) * .05))) AS 'tax' FROM commisionrelease a, tbl_client_properties b ,tbl_commissions c WHERE a.cp_id = b.cp_id AND c.cp_id = b.cp_id AND a.agentID = c.agentID AND DATE(a.releasedate) = DATE(NOW())
            UNION ALL 
          SELECT IF (SUM(((contractPrice / b.commissionReleased) * ( c.percentagevalue /  100 )) * .05) IS NULL, 0.00, (SUM(((contractPrice / b.commissionReleased) * ( c.percentagevalue /  100 )) * .05))) AS 'tax' FROM commisionrelease a, tbl_client_properties b ,tbl_commissions c WHERE a.cp_id = b.cp_id AND c.cp_id = b.cp_id AND a.agentID = c.agentID AND WEEK(a.releasedate) = WEEK(DATE(NOW())) 
            UNION ALL
          SELECT IF (SUM(((contractPrice / b.commissionReleased) * ( c.percentagevalue /  100 )) * .05) IS NULL, 0.00, (SUM(((contractPrice / b.commissionReleased) * ( c.percentagevalue /  100 )) * .05)))  AS 'tax' FROM commisionrelease a, tbl_client_properties b ,tbl_commissions c WHERE a.cp_id = b.cp_id AND c.cp_id = b.cp_id AND a.agentID = c.agentID AND MONTH(a.releasedate) = MONTH(DATE(NOW()))";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function geTotalCom()
    {
        try {

            $sql = "select sum(amount) as 'com' from commisionrelease";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }   


    public function agentCommissionReleases()
    {
        try {

            // $sql = "select sum(amount) as 'val', ReleaseDate as 'date' from commisionrelease order by ReleaseDate asc group by ReleaseDate";
            $sql = "SELECT 
            SUM(amount) AS 'val',
            ReleaseDate AS 'date' 
          FROM
            commisionrelease 
          WHERE ReleaseDate = DATE(CURDATE())
          GROUP BY ReleaseDate ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            
        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getDailyCharges()
    {
        try {

            $sql = "
            SELECT 
                IF(SUM(CAST(json_extract (json_extract (additionalCharges, '$[0]'),'$.chargeValue') AS DECIMAL(11,2))) IS NULL,0,
                SUM(CAST(json_extract (json_extract (additionalCharges, '$[0]'),'$.chargeValue') AS DECIMAL(11,2))) ) AS 'c' ,
                CAST(JSON_UNQUOTE(json_extract(json_extract (additionalCharges, '$[0]'),'$.chargeDate')) AS DATE) AS 'd' 
            FROM 
                tbl_client_properties	
            WHERE
                DATE(JSON_UNQUOTE(json_extract(json_extract (additionalCharges, '$[0]'),'$.chargeDate')) ) != 'Null'
             AND
             DATE(JSON_UNQUOTE(json_extract(json_extract (additionalCharges, '$[0]'),'$.chargeDate')) ) = CURDATE()
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getWeeklyCharges(){
        try {

            $sql = "
            SELECT 
                IF(SUM(CAST(json_extract (json_extract (additionalCharges, '$[0]'),'$.chargeValue') AS DECIMAL(11,2))) IS NULL, 0 ,SUM(CAST(json_extract (json_extract (additionalCharges, '$[0]'),'$.chargeValue') AS DECIMAL(11,2)))) AS 'c' ,
                    CAST(JSON_UNQUOTE(json_extract(json_extract (additionalCharges, '$[0]'),'$.chargeDate')) AS DATE) AS 'd' 
            FROM 
                tbl_client_properties	
                WHERE
                DATE(JSON_UNQUOTE(json_extract(json_extract (additionalCharges, '$[0]'),'$.chargeDate')) ) != 'Null'
                AND
                WEEK(JSON_UNQUOTE(json_extract(json_extract (additionalCharges, '$[0]'),'$.chargeDate')) ) = WEEK(DATE(NOW()))
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getMonthlyCharges(){
        try {

            $sql = "
            SELECT 
                IF(SUM(CAST(json_extract (json_extract (additionalCharges, '$[0]'),'$.chargeValue') AS DECIMAL(11,2))) IS NULL,0,SUM(CAST(json_extract (json_extract (additionalCharges, '$[0]'),'$.chargeValue') AS DECIMAL(11,2)))) AS 'c' ,
                    CAST(JSON_UNQUOTE(json_extract(json_extract (additionalCharges, '$[0]'),'$.chargeDate')) AS DATE) AS 'd' 
            FROM 
                tbl_client_properties	
                WHERE
                DATE(JSON_UNQUOTE(json_extract(json_extract (additionalCharges, '$[0]'),'$.chargeDate')) ) != 'Null'
                AND
                MONTH(JSON_UNQUOTE(json_extract(json_extract (additionalCharges, '$[0]'),'$.chargeDate')) ) = MONTH(DATE(NOW()))
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAllChargesGuest($list)
    {
        try {

            $sql = "select additionalCharges as 'c' from tbl_client_properties where property_id = (select property_id from tbl_propertylist where propertyParentID in (". implode(',', $list) .")) and active = 1";

            $statement = $this->db->prepare($sql);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function updateUsernameUser($userID, $username)
    {
        try {

            $sql = "update tbl_users set username =:username where UserID = :userID";

            $statement = $this->db->prepare($sql);
            return $statement->execute([
                'userID' => $userID,
                'username' => $username
            ]);
            
        } catch (PDOException $e) {
            return $e;
        }
    }


    public function checkCurrentPass($userID)
    {
        try {
            
            $sql = "select Password as 'pass' from tbl_users where UserID = :userID";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                'userID' => $userID
            ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAgentAssociatedClients($userID)
    {
        try {
            
            $sql = "select count(distinct(b.client_id)) as 'clientCount' from tbl_commissions a left join tbl_client_properties b on a.cp_id = b.cp_id where agentID = (select refID from tbl_users where UserID = :agentID)";
            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'agentID'  => $userID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAgentTotalCom($agentID)
    {
        try {
            $sql = "select sum(amount) as 'total' from commisionrelease where agentID = :agentID";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'agentID'  => $agentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();

        } catch (PDOException $e) {
            return $e;
        }
    }

     public function getAllPropertyByClientOwnerID($cpID){
        try {
            $sql = "
                SELECT
                    (tbl_client_properties.sqmPricem2 * tbl_propertylist.sqm) as contractPrice,
                    ((tbl_client_properties.sqmPricem2 * tbl_propertylist.sqm) / tbl_client_properties.plan_terms) as monthlyAmortization,
                    tbl_client_properties.additionalCharges,
                    tbl_client_properties.sqmPricem2,
                    tbl_propertylist.sqm,
                    tbl_client_properties.plan_terms,
                    tbl_client_properties.commissionReleased,
                    tbl_client_properties.adCom
                FROM
                    tbl_client_properties
                INNER JOIN
                    tbl_propertylist on  tbl_propertylist.property_id = tbl_client_properties.property_id
                WHERE
                    tbl_client_properties.active = 1
                AND
                    tbl_client_properties.cp_id = :cpID

            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'cpID'   => $cpID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();


        } catch (PDOException $e) {
            return $e;
        }
    }

    public function getAgentCommissionable($agentID)
    {
        try {
            
            $sql = "
                SELECT
                   DISTINCT clientProperty.cp_id,
                        tbl_commissions.agentID,
                        tbl_commissions.percentagevalue,
                        clientProperty.clientName,
                        clientProperty.propertyName,
                        clientProperty.client_id,
                        (select count(com.id) from commisionrelease com where com.agentID = tbl_commissions.agentID and com.cp_id = tbl_client_properties.cp_id) as 'released',
                        date_format(tbl_commissions.dateReleased,'%M %d, %Y %r') as dateNow
                    FROM
                        tbl_commissions
                    INNER JOIN
                        (
                            SELECT
                                tbl_client_properties.cp_id,
                                concat(tbl_client.Fname,' ',tbl_client.Lname) as clientName,
                                tbl_propertylist2.propertyName,
                                tbl_client_properties.client_id
                            FROM 
                                tbl_client_properties
                            INNER JOIN 
                                tbl_client ON tbl_client.client_id =  tbl_client_properties.client_id
                            INNER JOIN 
                                (
                                SELECT
                                    CONCAT(tbl_propertyparent.propertyName,IF(tbl_propertylist.phaseNumber = 0,'',CONCAT(' Phase ',tbl_propertylist.phaseNumber)),' Block ',tbl_propertylist.block,' Lot ',tbl_propertylist.lot) as propertyName,
                                    tbl_propertylist.property_id
                                FROM
                                    tbl_propertylist
                                INNER JOIN
                                    tbl_propertyparent on tbl_propertyparent.id = tbl_propertylist.propertyParentID
                                ) as tbl_propertylist2 ON tbl_propertylist2.property_id =  tbl_client_properties.property_id
                        ) as clientProperty on clientProperty.cp_id = tbl_commissions.cp_id
                    inner JOIN
                        tbl_client_properties on tbl_client_properties.cp_id = tbl_commissions.cp_id
                    INNER JOIN
                        tbl_clientpaymenthistory on tbl_client_properties.cp_id = tbl_clientpaymenthistory.cp_id
                    WHERE
                        tbl_commissions.active = 1
                    AND
                        tbl_clientpaymenthistory.active = 1
                    AND
                        tbl_commissions.released < tbl_client_properties.commissionReleased
                    AND
                        (select count(com.id) from commisionrelease com where com.agentID = tbl_commissions.agentID and com.cp_id = tbl_client_properties.cp_id) < if(tbl_client_properties.fullyPaid = 1, tbl_client_properties.commissionReleased, (select count(id) from tbl_clientpaymenthistory where cp_id = tbl_commissions.cp_id and active = 1))
                        
                    AND
                        tbl_commissions.agentID  = (select refID from tbl_users where UserID =:agentID) 
            ";

            $statement = $this->db->prepare($sql);
            $statement->execute([
                    'agentID'  => $agentID
                ]);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            return $statement->fetchAll();
            

        } catch (PDOException $e) {
            return $e;
        }
    }


}
