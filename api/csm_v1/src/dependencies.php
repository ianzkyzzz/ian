<?php
// DIC configuration

$container = $app->getContainer();

$container['mpdf'] = function ($c) {
    include('../vendor/mpdf60/mpdf.php');
    $mpdf = new mPDF('c');
    return $mpdf;
};

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};



$container['jwt'] = function ($container) {
    return new stdClass();
};

// -----------------------------------------------------------------------------
// Database connection
// -----------------------------------------------------------------------------

$container['db'] = function ($c) {
    try {
        $settings = $c->get('settings')['db'];
        $pdo = new PDO("mysql:host=" . $settings['host'] . ";dbname=" . $settings['dbname'],
            $settings['username'], $settings['password'],[PDO::ATTR_EMULATE_PREPARES => false]);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        echo "Database connection error!";
        return null;
    }
    return $pdo;
};


// -----------------------------------------------------------------------------
// Controllers
// -----------------------------------------------------------------------------

$container['UserController'] = function($c) {
    return new \App\Controllers\UserController($c->get('User'), $c->get('Validation'));
};

$container['PropertyController'] = function($c) {
    return new \App\Controllers\PropertyController( $c->get('Property'), $c->get('Validation'));
};

$container['AgentController'] = function($c) {
    return new \App\Controllers\AgentController($c->get('ClientPaymentHistory'), $c->get('Agent'),$c->get('Commission'), $c->get('ClientProperty'), $c->get('Validation'));
};

$container['ClientController'] = function($c) {
    return new \App\Controllers\ClientController( $c->get('Client'), $c->get('ClientProperty'), $c->get('ClientPaymentHistory'),$c->get('Agent'),$c->get('Property'),$c->get('Commission'), $c->get('Validation'));
};


$container['PaymentController'] = function($c) {
    return new \App\Controllers\PaymentController( $c->get('Property'),$c->get('ClientPaymentHistory'),$c->get('ClientProperty'), $c->get('Validation'));
};


$container['ReportController'] = function($c) {
    return new \App\Controllers\ReportController($c->get('ClientPaymentHistory'), $c->get('Validation'));
};

$container['BusinesPartnerController'] = function($c) {
    return new \App\Controllers\BusinesPartnerController($c->get('BusinessPartner'),  $c->get('Property'),$c->get('Validation'));
};


// -----------------------------------------------------------------------------
// Model factories
// -----------------------------------------------------------------------------


$container['Agent'] = function ($container) {
    return new App\Models\Agent($container->get('db'));
};

$container['User'] = function ($container) {
    return new App\Models\User($container->get('db'));
};

$container['Property'] = function ($container) {
    return new App\Models\Property($container->get('db'));
};

$container['Client'] = function ($container) {
    return new App\Models\Client($container->get('db'));
};

$container['ClientProperty'] = function ($container) {
    return new App\Models\ClientProperty($container->get('db'));
};

$container['ClientPaymentHistory'] = function ($container) {
    return new App\Models\ClientPaymentHistory($container->get('db'));
};

$container['Commission'] = function ($container) {
    return new App\Models\Commission($container->get('db'));
};

$container['BusinessPartner'] = function ($container) {
    return new App\Models\BusinessPartner($container->get('db'));
};



// -----------------------------------------------------------------------------
// Helpers
// -----------------------------------------------------------------------------

$container['Validation'] = function ($container) {
    return new App\Helpers\Validation($container->get('User'));
};
