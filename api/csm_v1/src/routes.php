<?php

// -----------------------------------------------------------------------------
// Routes
// -----------------------------------------------------------------------------

$app->group('/', function(){
	$this->map(['GET'],'login','UserController:loginForm');
	$this->map(['POST'],'login','UserController:accessLogin');
	$this->map(['GET'],'logout','UserController:logoutThisUser');
	$this->map(['GET'],'isLogin','UserController:validateUser');
});
  
$app->group('/dashboard', function() use ($app){
	
	$this->group('/user', function() use ($app){
		$this->map(['POST'],'/uploadThisImage','UserController:uploadThisImage');
		$this->map(['POST'],'/removeUser','UserController:removeThisUser');
		$this->map(['POST'],'/addUser','UserController:postUser');
		$this->map(['POST'],'/updateUserInfo','UserController:updateUserInfo');
		$this->map(['POST'],'/updateUserLoginCredential','UserController:updateUserLoginCredential');
		$this->map(['GET'],'/getAllUser','UserController:getAlluserActive');
		$this->map(['GET'],'/getAllInfoForThisUser','UserController:getAllInfoForThisUser');
		$this->map(['GET'],'/getAllDataDashboard','UserController:getAllDataDashboard');
		$this->map(['GET'],'/checkThisCurrentPassword','UserController:checkCurrentPass');

		$this->map(['PUT'],'/UpdatePErcentage','UserController:update_changePercentage');
		$this->map(['PUT'],'/update_thisUserUsername','UserController:updateUsernameUser');
		
		$this->map(['GET'],'/checkIfDSR','UserController:getCheckIfDonSakagawa');
	});	 

	$this->group('/agent', function() use ($app){
		$this->map(['POST'],'/addAgent','AgentController:postThisAgent');
		$this->map(['POST'],'/updateAgent','AgentController:postUpdateThisAgent');
		$this->map(['POST'],'/removeAgent','AgentController:removeThisAgent');
		$this->map(['GET'],'/getAll','AgentController:getAllAgents');
		$this->map(['GET'],'/getThisAgent','AgentController:getThisAgentInfo');
		$this->map(['GET'],'/getThisPropertyInfoAndHistory','AgentController:getThisAgentInfo');
		$this->map(['GET'],'/getListOfAgentHigherThanThisPosition','AgentController:getListOfAgentHigherThanThisPosition');
		$this->map(['GET'],'/getThisAgentCommissionPerPrperty','AgentController:getThisAgentCommissionPerPrperty');
		$this->map(['GET'],'/geThisAgentoptionalRel','AgentController:getThisAgentOptionalPropertyRelease');
		$this->map(['POST'],'/releasetThisAgentCommissionPerPrperty','AgentController:releasetThisAgentCommissionPerPrperty');
		$this->map(['POST'],'/realeaseThisOptionalComRelease','AgentController:release_thisClientSelectedPropertyToRelease');
		$this->map(['GET'],'/getAgentPropertyList','AgentController:getAgentPropertyList');
		$this->map(['GET'],'/getAgentNames','AgentController:getAgentNames');
		$this->map(['GET'],'/getComByDate','AgentController:getComByDate');
		$this->map(['GET'],'/getPropertList','AgentController:getPropertList');
		$this->map(['GET'],'/getThisCommissionPerProperty','AgentController:getThisCommissionPerProperty');
		$this->map(['GET'],'/getThisReleasedCom','AgentController:get_thisReleasedCommission');

		$this->map(['POST'],'/releaseThisComByProperty','AgentController:releaseThisComByProperty');

		$this->map(['POST'],'/uploadThisImage','AgentController:uploadThisImage');

		$this->map(['GET'],'/getAllByPropertyName','AgentController:getAllByPropertyName');
		
		$this->map(['GET'],'/loadAgentsUnderSelectedProperty','AgentController:loadAgentsUnderSelectedProperty');

		$this->map(['POST'],'/releaseAllComUnderThisProp','AgentController:ReleaseAllComUnderThisProperty');
		
		$this->map(['GET'],'/getComRelSummary','AgentController:getComRelSummary');
		
		$this->map(['GET'],'/getSumComDetails','AgentController:getSumComDetails');
		
		$this->map(['GET'],'/selectThisProperty','AgentController:selectThisProperty');
		
		$this->map(['GET'],'/releaseThisComList','AgentController:releaseThisComList');
		
		$this->map(['GET'],'/releaseThisComListOverride','AgentController:releaseThisComListOverride');

		$this->map(['GET'],'/getThisPropertyByDate','AgentController:getThisPropertyByDate');
		
		$this->map(['GET'],'/getThisSummaryByDate','AgentController:getThisSummaryByDate');
		
		$this->map(['GET'],'/printAndSaveThis','AgentController:printAndSaveThis');
		
		$this->map(['PUT'],'/addAgentUsernameLogin','AgentController:addAgentUsernamelogin');
		$this->map(['PUT'],'/addAgentPasswordLogin','AgentController:addAgentPasswordlogin');
	});	
 

	$this->group('/property', function() use ($app){
		$this->map(['POST'],'/addThisPorperty','PropertyController:postThisProperty');
		$this->map(['POST'],'/updateThis','PropertyController:updateThisPorperty');
		$this->map(['POST'],'/removeThis','PropertyController:removeThisPorperty');
		$this->map(['POST'],'/assumeTO','PropertyController:setUpdateAssumeTO');

		$this->map(['GET'],'/getAll','PropertyController:getAllProperties'); 
		$this->map(['GET'],'/getAllPropertyParents','PropertyController:getAllPropertyParents'); 
		$this->map(['GET'],'/getPropertyParentsDetails','PropertyController:getPropertyParentsDetails'); 
		$this->map(['GET'],'/getPropertyParentDetailsSpecificBlock','PropertyController:getPropertyParentsDetailsSpecificBlock');
		$this->map(['GET'],'/getThisBlock','PropertyController:getThisBlock'); 

		$this->map(['GET'],'/getlastBlock','PropertyController:getLastBlock'); 
		$this->map(['GET'],'/getLastBlockForThisPhase','PropertyController:getLastBlockForThisPhase'); 
		
		$this->map(['GET'],'/getInfo','PropertyController:getAllBlockAndLotsForThisProperty');
		$this->map(['GET'],'/getAllBlockForThisParent','PropertyController:getAllBlockForThisProperty');
		$this->map(['GET'],'/getAllLotsForThisParentBlock','PropertyController:getAllLotsForThisBlock');
		$this->map(['GET'],'/getCleintList','PropertyController:getThisClientList');

		$this->map(['POST'],'/addThisBlock','PropertyController:addThisBlockAndItsProperties');
		$this->map(['POST'],'/updateThisBlock','PropertyController:updateThisBlockAndItsProperties');
		$this->map(['GET'],'/getAllOfThisSelectedPropertiesInfo','PropertyController:getAllOfThisSelectedPropertiesInfo');

	});	
 


	$this->group('/client', function() use ($app){
		$this->map(['POST'],'/uploadThisImage','ClientController:uploadThisImage');
		$this->map(['POST'],'/addThis','ClientController:postThisClient');
		$this->map(['POST'],'/addThisMoreProperty','ClientController:addThisMoreProperty');
		$this->map(['POST'],'/updateThis','ClientController:updateThisClient');
		$this->map(['POST'],'/updateComRelShecdule','ClientController:updateComRelShecdule');
		$this->map(['POST'],'/updateAddCommission','ClientController:updateAddCommission');
		$this->map(['POST'],'/updatedAdditionalCharges','ClientController:updatedAdditionalCharges');
		$this->map(['POST'],'/forfeitSelectedProperty','ClientController:forfeitSelectedProperty');
		$this->map(['GET'],'/getAllclientwithLatepayment','ClientController:getAllclientwithLatepayment');
		
		$this->map(['GET'],'/getAllParentProAndAgents','ClientController:getAllParentPropertiesAndAgents');
		// $this->map(['POST'],'/updateThis','ClientController:updateThisClient');
		// $this->map(['POST'],'/removeThis','ClientController:removeThisClient');
		$this->map(['GET'],'/getAll','ClientController:getAllClient');
		$this->map(['GET'],'/getAllNames','ClientController:getAllClientNames');
		$this->map(['GET'],'/getAllProperties','ClientController:getAllPropertiesForThisClient');
		$this->map(['GET'],'/getInfo','ClientController:getInfoForThisClient');
		$this->map(['GET'],'/paymentHistoryForThisProperty','ClientController:paymentHistoryForThisProperty');
		$this->map(['GET'],'/viewPaymentHistoryForThisProperty','ClientController:viewPaymentHistoryForThisProperty');
		$this->map(['GET'],'/getThisClientPropertyInfoAndHistory','ClientController:getThisClientPropertyInfoAndHistory');		
		$this->map(['GET'],'/getThisClientPropertyInfoAndHistoryforfeited','ClientController:getThisClientPropertyInfoAndHistoryforfeited');
		$this->map(['GET'],'/getAllProperParentList','ClientController:getThisProperList');
		$this->map(['GET'],'/getClietsfromselectedProperTy','ClientController:get_thisClientsfromSelectedProperty');
		$this->map(['GET'],'/getclientLatepaymentswithID','ClientController:getLateclientswithPropID');
		$this->map(['GET'],'/getSMSnotification','ClientController:sendClientwithPropID');
		$this->map(['GET'],'/getThisClientListfromBockSearch','ClientController:get_thisClientListByBock');
		

		$this->map(['GET'],'/getAgentComforClientProp','ClientController:getAgentComforClientProp');
		$this->map(['GET'],'/getThisDownpayment','ClientController:getThisDownpayment');
		$this->map(['POST'],'/udpateAgentCom','ClientController:updateThisAgentComProp');
		$this->map(['POST'],'/updateThisDownpayment','ClientController:updateThisDownpayment');

		$this->map(['PUT'],'/newSpouse','ClientController:add_ThisnewSpouse');
		$this->map(['PUT'],'/updateSpouse','ClientController:update_thisSpouse');

		$this->map(['PUT'],'/newBeneficiary','ClientController:add_thisNewBeneficiary');
		$this->map(['PUT'],'/updateBeneficiary','ClientController:update_beneficiary');
	});	


	$this->group('/payment', function() use ($app){
		$this->map(['POST'],'/postThisPayment','PaymentController:postThisPayment');	
		$this->map(['POST'],'/updateClientPaymentTransaction','PaymentController:updateClientPaymentTransaction');	
		
		$this->map(['GET'],'/transactionPaymentHistory','PaymentController:getAllTransactionPaymentHistory');
		$this->map(['GET'],'/updateTransactionPaymentHistoryTable','PaymentController:getUpdateTrasactionPaymentHistory');
		$this->map(['GET'],'/getClientPaymentHistoryDetails','PaymentController:getClientPaymentHistory');
		$this->map(['GET'],'/deleteThisPayment','PaymentController:deleteThisPayment');
		$this->map(['GET'],'/checkorNumber','PaymentController:checkorNumber');
		$this->map(['GET'],'/checkAdminPass','PaymentController:verify_adminPass');
		$this->map(['GET'],'/getPaymentDescription','PaymentController:geThisPaymentDesc');
		$this->map(['GET'],'/getCharges','PaymentController:geThisCharges');

		$this->map(['PUT'],'/newAdminPass','PaymentController:new_adminPass');
		$this->map(['PUT'],'/addnewCharges','PaymentController:newCharges');
	});	


	$this->group('/report', function() use ($app){
		$this->map(['GET'],'/getClientPayment','ReportController:getAllClientPayments');		
		$this->map(['GET'],'/searchReleaseReport','ReportController:searchReleaseReport');		
		$this->map(['GET'],'/getCommissionSummary','ReportController:getCommissionSummary');		
		$this->map(['GET'],'/getSelectedAgentPosition','ReportController:getSelectedAgentPosition');		
		$this->map(['GET'],'/getThisPropDetails','ReportController:getThisPropDetails');		
		$this->map(['GET'],'/getThisPropDetailsByDate','ReportController:getThisPropDetailsByDate');		
		$this->map(['GET'],'/getIncomStatement','ReportController:get_thisIncomeStateMent');		
		$this->map(['GET'],'/getPropertyDetailsByDate','ReportController:get_thisPropertyDetailsByDate');		
		$this->map(['GET'],'/getPropertiesAdvanceSearh','ReportController:get_thisPropertiesAdvanceSearh');		
		$this->map(['GET'],'/getAdditionalChargest','ReportController:get_thisAdditionalCharges');		
		$this->map(['GET'],'/getAdditionalChargestAdvanceSearch','ReportController:get_additionalCharges');		

		$this->map(['GET'],'/checkUserAccount','ReportController:get_userAccount');		
		$this->map(['GET'],'/getPropertyByUserAccount','ReportController:get_propertyByUserAccount');
		
		$this->map(['GET'],'/getCashIncome','ReportController:getCashIncome');

		$this->map(['GET'],'/getAllPropertyName','ReportController:getAllPropertyName');
	});

	$this->group('/partner', function() use ($app){
		$this->map(['POST'],'/add','BusinesPartnerController:addNewBusinessPartner');	

		$this->map(['GET'],'/getproperty','BusinesPartnerController:getThisPropertyList');			
		$this->map(['GET'],'/getParterList','BusinesPartnerController:getBusinessPartnerList');			
		$this->map(['GET'],'/removePartner','BusinesPartnerController:getRemoveThisPartner');			
		$this->map(['GET'],'/getProfile','BusinesPartnerController:getPartnerProfileDetails');
		$this->map(['GET'],'/getUsername','BusinesPartnerController:getPartnerUsername');
		$this->map(['GET'],'/getPassword','BusinesPartnerController:getPartnerPassword');

		$this->map(['PUT'],'/updatePartnerProfile','BusinesPartnerController:updateBusinespartnerProfile');			
		$this->map(['PUT'],'/addUpdateAccountUsername','BusinesPartnerController:addUpdateUsername');			
		$this->map(['PUT'],'/updateAccountUsername','BusinesPartnerController:updateAccouuntUsername');			
		$this->map(['PUT'],'/updateProperty','BusinesPartnerController:updateBusinessPropertyList');			
	});

});

