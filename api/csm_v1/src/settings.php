<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        // Database
        'db' => [
            'host' => 'localhost', // To change port, just add it afterwards like localhost:8889
            'dbname' => 'webdsr_portal_janjan', //webdsr_portal_janjan
            'username' => 'dbuser', //dbuser
            'password' => '*/S3hd%/~]m~X<Zf' //*/S3hd%/~]m~X<Zf

// 			'host' => 'techstationinc-rds-seoul.cyxjx1jbgrnv.ap-northeast-2.rds.amazonaws.com', // To change port, just add it afterwards like localhost:8889
//             'dbname' => 'webdsk_master_mortgage', // DB name or SQLite path
//             'username' => 'admin',//webdsk_stage_use
//             'password' => '2n.QL8(TeB}qGk?4'
        ]
    ],
];
