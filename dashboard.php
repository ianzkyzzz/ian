<?php 
    session_start();
    ini_set('date.timezone', 'Asia/Singapore');
?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
 
<head>
    <meta charset="utf-8" />
    
    <!-- Document Title 
    ============================================= -->
    <title>RNS</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <!-- <meta content="Preview page of Metronic Admin Theme #1 for blank page layout" name="description" /> -->
    <meta content="" name="author" />

    <?php 
        include 'include/dashboard-css.php';
    ?>
     
    <!-- <link rel="stylesheet" type="text/css" href="css/dashboardStyle.css"> -->
    <link rel="stylesheet" type="text/css" href="css/custom.css">
    
    <!-- <link rel="shortcut icon" href="images/logoicon.png" />  -->
    <link rel="shortcut icon" href="images/logo-invert.jpg" /> 
</head>

<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-md">
    
    <div class="page-wrapper">
        <div class="hmsLoading  ">
            <img src="images/logo-invert.jpg" style="width: 30%">
        </div>
        <div class="hmsLoading2 hidden ">
            <!-- <img src="images/loading.gif"> -->
            <i  class="fa  fa-spinner fa-spin dark"></i> &nbsp; &nbsp;
            <label style="font-size: 50%" class="loadingMessage"></label>   
            
        </div>

    <?php include 'include/header.php'; ?>
    
    <div class="page-container"> 
        
        <?php include 'include/sidebar.php'; ?>
  
        <div class="page-content-wrapper">
            
            <div class="dashboardPage dashboardContainer"></div>

            <div class="dashboardPage addUserOrEmployeeContainer"></div>

            <div class="dashboardPage updateUserOrEmployeeContainer"></div>

            <div class="dashboardPage listOfUserOrEmployeeContainer"></div>

            <div class="dashboardPage addPropertyContainer"></div>

            <div class="dashboardPage updatePropertyContainer"></div>

            <div class="dashboardPage propertyListContainer"></div>

            <div class="dashboardPage addClientContainer"></div>

            <div class="dashboardPage clientListContainer"></div>

            <div class="dashboardPage clientProfileContainer"></div>

            <div class="dashboardPage addAgentContainer"></div>

            <div class="dashboardPage agentListContainer"></div>

            <div class="dashboardPage updateAgentContainer"></div> 

            <div class="dashboardPage addPaymentContainer"></div>
            
            <div class="dashboardPage addReporstContainer"></div>

            <div class="dashboardPage commissionReleasingContainer"></div>

            <div class="dashboardPage addMoreAdditionalPropertyContainer"></div>
        </div>
        
        <?php include 'include/footer.php'; ?>
  
    </div>
   

    <?php include 'include/dashboard-js.php'; ?>
        
    <!-- <script type="text/javascript" src="js/underscore.js"></script> -->
    <script type="text/javascript" src="js/app/gf.js"></script>
    <script type="text/javascript" src="js/app/dashboard.js"></script>
        
    <script type="text/javascript" src="js/app/property.js"></script>
    <script type="text/javascript" src="js/app/client.js"></script>
    <script type="text/javascript" src="js/app/agent.js"></script>
    <script type="text/javascript" src="js/app/payment.js"></script>
    <script type="text/javascript" src="js/app/report.js"></script>
    <script type="text/javascript" src="js/app/businessPartner.js"></script>
    

    <?php 
        if ($_SESSION['UserRole'] == 'Encoder') {
    ?>           
            <script type="text/javascript" src="js/app/user-profile.js"></script>
            <script type="text/javascript" src="js/app/user.js"></script> 
      
    <?php   
        }else{
    ?>
            <script type="text/javascript" src="js/app/user.js"></script> 
             <script type="text/javascript">
                 $('.showHide').removeClass('hidden');
             </script>
    <?php  
        }
    ?>



    <!-- <script >
        $(document).ready(function(){
            // $('#demo-chosen-select').chosen();
            $('#demo-cs-multiselect').chosen({width:'100%'});
        });
    </script> -->
    
    
</body>
</html>
