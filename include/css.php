<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="Tagum Doctors Hospital Inc." />

<!-- Stylesheets -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="addons/metronic/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="addons/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="addons/metronic/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> 
<link href="addons/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->



<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="addons/metronic/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="addons/metronic/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="addons/metronic/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="addons/metronic/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="addons/metronic/pages/css/login.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" /> </head>
<!-- END HEAD -->

<link rel="stylesheet" type="text/css" href="css/custom.css">