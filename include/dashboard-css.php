<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="Tagum Doctors Hospital Inc." />


<!-- BEGIN PAGE FIRST SCRIPTS -->
<script src="addons/metronic/global/plugins/pace/pace.min.js" type="text/javascript"></script>
<!-- END PAGE FIRST SCRIPTS -->
<!-- BEGIN PAGE TOP STYLES -->
<link href="addons/metronic/global/plugins/pace/themes/pace-theme-flash.css" rel="stylesheet" type="text/css" /> 
<!-- END PAGE TOP STYLES -->
<!-- BEGIN GLOBAL MANDATORY STYLES -->

<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="addons/metronic/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="addons/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="addons/metronic/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="addons/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />


<link href="addons/metronic/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
<link href="addons/metronic/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css" rel="stylesheet" type="text/css" />

<link href="addons/metronic/global/plugins/chosen/chosen.min.css" rel="stylesheet">


<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->

<link href="addons/metronic/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
<link href="addons/metronic/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="addons/metronic/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="addons/metronic/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
<link href="addons/metronic/global/plugins/bootstrap-sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->

 
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="addons/metronic/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="addons/metronic/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->

<!-- BEGIN PAGE LEVEL STYLES -->
<link href="addons/metronic/pages/css/search.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="addons/metronic/layouts/layout2/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="addons/metronic/layouts/layout2/css/themes/blue.min.css" rel="stylesheet" type="text/css" id="style_color" />
<!-- END PAGE LEVEL PLUGINS -->


<link href="addons/metronic/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="addons/metronic/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="addons/metronic/global/plugins/icheck/skins/all.css" rel="stylesheet" type="text/css" />

<link href="css/style2.css" rel="stylesheet" type="text/css"/>
<link href="addons/metronic/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!-- <link href="addons/metronic/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css" />


<link href="addons/metronic/global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css" />


<link href="addons/metronic/global/css/components.min.css" rel="stylesheet" type="text/css" />
<link href="addons/metronic/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
 -->


<!-- BEGIN THEME LAYOUT STYLES -->

<!-- <link href="addons/metronic/layouts/layout2/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="addons/metronic/layouts/layout2/css/themes/blue.min.css" rel="stylesheet" type="text/css" id="style_color" /> -->
<!-- <link href="addons/metronic/layouts/layout2/css/custom.min.css" rel="stylesheet" type="text/css" /> -->
<!-- END THEME LAYOUT STYLES -->


<!-- <link href="addons/metronic/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="addons/metronic/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" /> -->


<!-- new code -->
<!-- <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
 -->

