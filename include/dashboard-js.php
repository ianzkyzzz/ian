
 <!-- BEGIN CORE PLUGINS -->
<script src="addons/metronic/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
 
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="addons/metronic/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/counterup/jquery.counterup.js" type="text/javascript"></script>

<script src="addons/metronic/global/plugins/morris/morris.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
<!-- <script src="addons/metronic/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script> -->
<script src="addons/metronic/global/scripts/datatable.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="addons/metronic/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>
<script src="addons/metronic/pages/scripts/table2-datatables2-buttons2.min.js" type="text/javascript"></script>
<script src="addons/metronic/pages/scripts/ui-sweetalert.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>

<script src="addons/metronic/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>

<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="addons/metronic/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<script src="addons/metronic/pages/scripts/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/jquery.input-ip-address-control-1.0.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script src="addons/metronic/global/plugins/jquery-repeater/jquery.repeater.js" type="text/javascript"></script>

<script src="addons/metronic/global/plugins/jquery-notific8/jquery.notific8.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>

<!-- <script src="addons/metronic/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
 -->
<script src="addons/metronic/global/plugins/chosen/chosen.jquery.min.js"></script>
<script src="addons/metronic/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js" type="text/javascript"></script>

<script src="addons/metronic/pages/scripts/table-datatables-responsive.js" type="text/javascript"></script>
<script src="addons/metronic/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>

<script src="addons/metronic/pages/scripts/portlet-ajax.min.js" type="text/javascript"></script>
<script src="addons/metronic/pages/scripts/bootbox.min.js" type="text/javascript"></script>
<script src="addons/metronic/pages/scripts/form-input-mask.js" type="text/javascript"></script>

<script src="addons/metronic/pages/scripts/ui-blockui.min.js" type="text/javascript"></script>
<!-- BEGIN THEME LAYOUT SCRIPTS -->

<script src="addons/metronic/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<!-- <script src="addons/metronic/layouts/layout/scripts/demo.min.js" type="text/javascript"></script> -->
<script src="addons/metronic/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="addons/metronic/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->       

<script src="addons/metronic/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>

<script src="addons/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>

<!-- <script src="addons/metronic/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
<script src="addons/metronic/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
 -->
<script src="addons/metronic/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>


<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="addons/metronic/global/scripts/app.min.js" type="text/javascript"></script>

<!-- <script src="addons/metronic/pages/scripts/components-bootstrap-tagsinput.min.js" type="text/javascript"></script> -->
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!-- <script src="addons/metronic/pages/scripts/ui-general.min.js" type="text/javascript"></script> -->
<script src="addons/metronic/pages/scripts/components-select2.js" type="text/javascript"></script>
<script src="addons/metronic/pages/scripts/form-icheck.min.js" type="text/javascript"></script>

<script src="js/app/accounting.min.js" type="text/javascript"></script>

<!-- begin date picker -->
<script src="addons/metronic/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- end datepicker -->

<script src="addons/metronic/pages/scripts/form-icheck.min.js" type="text/javascript"></script>

<!-- <script  src="//cdn.datatables.net/plug-ins/1.10.15/api/sum().js" type="text/javascript"></script> -->

<script src="addons/metronic/pages/scripts/dashboard.min.js" type="text/javascript"></script>


<!-- Resources -->
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/none.js"></script>
<!-- Chart code -->


<!-- additional new code -->
<!-- <script src="assets/metronic/global/plugins/morris/morris.min.js" type="text/javascript"></script> -->
<!-- 
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
 -->

