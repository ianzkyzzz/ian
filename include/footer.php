<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> <?php echo (date("Y") == '2017' ? '2017' : '2017 - '.date("Y")); ?> &copy; <a href="#" style="text-decoration: none;color:#337ab7">R AND SONS PROPERTIES Co.</a>.</div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER --> 