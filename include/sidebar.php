<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- END SIDEBAR -->
    
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start active open">
                <a href="#Dashboard" class="nav-link nav-toggle openDashboardContainer">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li>
            <?php if ($_SESSION['UserRole'] != 'Business Partner'): ?>
                
                <li class="nav-item  ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-users"></i>
                        <span class="title">Clients</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <?php if ($_SESSION['UserRole'] != 'Agent'): ?>
                            <li class="nav-item  ">
                                <a href="#AddClient" class="nav-link  openAddClientForm">
                                    <span class="title">Add Client</span>
                                </a>
                            </li>
                        <?php endif ?>
                        <li class="nav-item  ">
                            <a href="#ViewAllClient" class="nav-link openViewAllClientContainer">
                                <span class="title">View All Client</span>
                            </a>
                        </li>
                    </ul>
                </li>

            <?php endif ?>

            <?php if ($_SESSION['UserRole'] == 'Admin'): ?>
                
                <li class="nav-item  ">
                    <a href="javascript:;" class="nav-link nav-toggle businessPartnerComponent">
                        <i class="fa fa-users"></i>
                        <span class="title">Business Partners</span>
                        <span class="arrow"></span>
                    </a>
                </li> 
                
            <?php endif ?>
       
            <?php if ($_SESSION['UserRole'] != 'Agent'): ?>
                <li class="nav-item  ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-pointer"></i>
                        <span class="title">Property</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="#PorpertyList" class="nav-link openPorpertyListContainer">
                                <span class="title">Property List</span>
                            </a>
                        </li>
                        <?php if ($_SESSION['UserRole'] != 'Business Partner'): ?>
                            <li class="nav-item  ">
                                <a href="#AddPorperty" class="nav-link openAddPorpertyContainer">
                                    <span class="title">Properties</span>
                                </a>
                            </li>
                        <?php endif ?>
                    </ul>
                </li>

                <li class="nav-item showHide">
                    <a href="javascript:;" class="nav-link nav-toggle  openReportPage">
                        <i class="icon-bar-chart"></i>
                        <span class="title">Reports</span>
                        <span class="arrow"></span>
                    </a>
                </li>

            <?php endif ?>
            
            <?php if ($_SESSION['UserRole'] != 'Business Partner'): ?>
                
                <li class="nav-item  ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-user"></i>
                        <span class="title">Agent</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item  ">
                            <a href="#AgentList" class="nav-link openAgentListPage">
                                <span class="title">Agent List</span>
                            </a>
                        </li>
                        
                        <?php if ($_SESSION['UserRole'] == 'Admin'): ?>
                            <li class="nav-item  ">
                                <a href="#AddAgent" class="nav-link openAddAgentPage">
                                    <span class="title">Add Agent</span>
                                </a>
                            </li>
                        <?php endif ?>

                        <li class="nav-item">
                            <a href="#ReleaseCommission" class="nav-link openCommissionReleasingPage">
                                <span class="title">Release Commission</span>
                            </a>
                        </li>
                    </ul>
                </li>
               
            <?php endif ?>

            <?php 
                if ($_SESSION['UserRole'] == "Admin") {
                    echo '<li class="nav-item position">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="icon-user"></i>
                            <span class="title">User/Admin</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item  ">
                                <a href="#UsersList" class="nav-link openUserListPage">
                                    <span class="title">Users List</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="#AddUser" class="nav-link openAddUserPage">
                                    <span class="title">Add User</span>
                                </a>
                            </li>
                        </ul>
                    </li> ';
                }
            ?>
    
            <?php if ( ($_SESSION['UserRole'] != 'Agent') && ($_SESSION['UserRole'] != 'Business Partner') ){ ?>
                <li class="nav-item  ">
                    <a href="javascript:;" class="nav-link nav-toggle openPaymentPage">
                        <i class="fa fa-paypal"></i>
                        <span class="title"> Payments </span>
                        <!-- <span class="arrow"></span> -->
                    </a>
                </li>     
            <?php } ?>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->