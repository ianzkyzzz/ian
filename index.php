<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>
    <meta charset="utf-8" />
    
    <!-- Document Title
    ============================================= -->
    <title> RNS </title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for blank page layout" name="description" />
    <meta content="" name="author" />

    <?php
        include 'include/css.php';
    ?>
     

    <link rel="shortcut icon" href="images/logo-invert.jpg" /> 
</head>
 
<body class="login" style="background-color: #fff">
        <!-- BEGIN LOGO -->
        <?php include 'include/js.php';?>
        <script src="js/app/gf.js" type="text/javascript"></script>
        <script src="js/app/login.min.js" type="text/javascript"></script>
</body>
</html>