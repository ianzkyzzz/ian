var Agent = function() {
    var templates = [],
        baseTemplateUrl = 'template/agent/';
    var gl_agentID = '';
    var gl_agentName = "";
    var gl_data = [];

    function getTemplate(baseTemplateUrl, templateName, callback) {
        if (!templates[templateName]) {
            $.get(baseTemplateUrl + templateName, function(resp) {
                compiled = _.template(resp);
                templates[templateName] = compiled;
                if (_.isFunction(callback)) {
                    callback(compiled);
                }
            }, 'html');
        } else {
            callback(templates[templateName]);
        }
    }

    var commisionReleasingTableListOfProperties;

    function tablecommisionReleasingTableListOfProperties() {
        var table = $('.commisionReleasingTableListOfProperties');
        commisionReleasingTableListOfProperties = table.dataTable({
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No employees added yet",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            // bootstrap_full_number
            "pagingType": "bootstrap_extended",
            "order": [
                [0, 'asc']
            ],

            // "columnDefs": [
            //     {
            //         "targets": [ 0 ],
            //         "visible": false,
            //         "searchable": true
            //     }

            // ],

            // "order": [[ 0, "asc" ]], 

            "lengthMenu": [
                [5, 10, 15, 20, 50, 100],
                [5, 10, 15, 20, 50, 100] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            destroy: true
        });

        var table = $('.commisionReleasingTableListOfProperties').DataTable();
        table.on('order.dt search.dt', function() {
            table.column(5, { search: 'applied', order: 'applied' }).nodes().each(function(cell, i) {
                
                cell.innerHTML = accounting.format(cell.innerHTML, 2);
            });
        }).draw();

    }

    var agentListTable;

    function tableAgentListTable() {
        var table = $('.agentListTable');
        agentListTable = table.dataTable({
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No employees added yet",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            // bootstrap_full_number
            "pagingType": "bootstrap_extended",
            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 50, 100],
                [5, 10, 15, 20, 50, 100] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            destroy: true
        });

    }


    return {
        init: init
    };

    // Init
    function init() {
        addEventHandlers();
        loadInitialization();

    }

    function loadInitialization() {


    }


    function addEventHandlers() {
        $(document).on('click', '.openAddAgentPage', openAddAgentPage);
        $(document).on('click', '.btnSumbitAgent', postThisAgent);
        $(document).on('click', '.openAgentListPage ,.returnToAgentList', openAgentListPage);
        $(document).on('click', '.btn_updateThisAgent', openAgentProfile);
        $(document).on('click', '.btnReturnToAgentList', returnToListOfAgents);
        $(document).on('click', '.btnUpdateAgent', updateThisAgent);
        $(document).on('click', '.btn_removeThisAgent', btn_removeThisAgent);
        $(document).on('change', '.agentPositionList', agentPositionListIsChange);

        $(document).on('submit', '#agentPrifilePicture', uploadThisImage);


        $(document).on('click', '.openCommissionReleasingPage', openCommissionReleasingPage);
        $(document).on('change', '.commissionRelaseAgentList', commissionRelaseAgentListIsChange);
        $(document).on('click', '.btnreleaseThisCommision', btnreleaseThisCommisionIsClick);

        $(document).on('click', '.agentProfilePage .tab_active_3', tab_active_3IsClicked);
        $(document).on('click', '.btn-searchComDate', func_searchComDate);
        $(document).on('click', '.btnRelaseThis', releaseThispaymentList);

        $(document).on('click', '.btnReleaseThisTotalAgentCommission', relComByProp);

        $(document).on('click', '.btn_searchThisDate', getThisDate);

        $(document).on('click', '#btn_setUsername_agent', addAgentLoginUserName);

        $(document).on('click', '.btn_comSummary', getThisSummary);

        $(document).on('click', '#btn_setPassword_agent', addAgentLoginUsersPassword);

        $(document).on('click', '.btn_printDetails', printThis);

        $(document).on('click', '.btnReleaseThisTotalAgentCommissionunderProperty', relAllUnderThisProp);

        $(document).on('click', '#tab_optionalRel', loadDataForOptionalRel);

        $(document).on('click', '#btn_releaseOptionalCom', releaseOptionalCom);

        $(document).on('click', '#btn_searhThisCom', getThisReleadCommissionList);

        $(document).on('click', '#btn_printRelCom ', printCom);

        $(document).on('change', '.commissionPopertyList', propertyReleasChange);

        $(document).on('change', '.commissionPopertyAllList', commissionAllPropertyListChange);

        $(document).on('change', '.getCommissionList', updateThisCom);

        $(document).on('change', '.getCommissionList ', getThisPropertylist);

        $(document).on('change', '#slt_opAgentlisty ', getCommissionListFromThisClient);

        $(document).on('click','#bckAgentList',openAgentListPage);

        
        $(document).on('change', '#slt_searComType ', function(params) {
            if(this.value == 1){
                $('#txt_searchWithSelecteOption').val('');
                $("#txt_searchWithSelecteOption").datepicker({
                    format: "yyyy-mm-dd"
                });
            }else{
                $('#txt_searchWithSelecteOption').val('');
                $("#txt_searchWithSelecteOption").data('datepicker').remove();
            }    
        });


        $(document).on('change', '.ck_properyParent ', function(argument) {
            const checkID = $(this);
            if (checkID.prop('checked')) {
                $('.ck_properyID').prop('checked', true);
            } else {
                $('.ck_properyID').prop('checked', false);
            }
        });

        $(document).on('change', '.ck_properyID ', function(argument) {
            calculateTotalRelComFromOptionalRelease();
        });

        $(document).on('keyup', '.releaseNO', function() {
            if ($('.releaseNO').val() != null) {
                $('.releaseNO').css("border-bottom", "1px solid #2ab4c0");
            } else {
                $('.releaseNO').css("border-bottom", "1px solid #c2cad8");
            }
        });

        $(document).on('click', '.releaseDate', function() {
            if ($('.releaseDate').val() != null) {
                $('.releaseDate').css("border-bottom", "1px solid #2ab4c0");
            } else {
                $('.releaseDate').css("border-bottom", "1px solid #c2cad8");
            }
        });
    }



    function uploadThisImage(event) {
        // swal('Warning','not ready','warning');
        // return false;
        showLoading();
        $.ajax({
            url: apiUrl + "/dashboard/agent/uploadThisImage",
            type: 'POST',
            data: new FormData(this),
            async: true,
            cache: false,
            contentType: false,
            // enctype: 'multipart/form-data',
            processData: false,
            success: function(response) {
                hideLoading();
                if (response.success) {
                    setTimeout(function() { swal('Success!', response.message, 'success'); }, 0);
                } else {
                    setTimeout(function() { swal('Warning!', response.message, 'warning'); }, 0);
                }

            },
            error: function(err) {
                hideLoading();
                setTimeout(function() { swal("Error!", err.statusText, "error"); }, 0);
            }
        });

        event.stopPropagation();
        event.preventDefault();
    }

    function func_searchComDate() {
        var id = $('.imageAgentID').val();
        var datefrom = $('.dateFrom').val();
        var dateto = $('.dateTo').val();

        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/getComByDate",
            data: { id: id, dateFrom: datefrom, dateTo: dateto },
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {
                    var table;

                    table = $('#comRelTable').DataTable();
                    table.clear().draw();
                    table.rows.add(response.comReleaseTable).draw();
                    hideLoading();

                } else {
                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }

            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }

    function tab_active_3IsClicked() {
        $('.updateAgentForm input').focus();
    }


    function agentPositionListIsChange() {
        var pos = $(this);

        if (pos.val() == 1) {
            $('.agentListContainer').addClass('hidden');
        } else {
            showLoading();
            var agentList = $('.agentList');
            $.ajax({
                type: 'GET',
                url: apiUrl + "/dashboard/agent/getListOfAgentHigherThanThisPosition",
                data: { position: pos.val() },
                cache: false,
                async: true,
                success: function(response) {
                    if (response.success) {
                        pos.parent().removeClass('has-error');
                        pos.parent().find('span').html('');

                        $('.agentListContainer').removeClass('hidden');
                        agentList.empty();
                        $.each(response.agents, function(i, n) {
                            agentList.append('<option value="' + n.agentID + '">' + n.agentName + '</option>');
                        });

                        $('.agentListContainer').find('label').html(response.higherPositionName);
                        hideLoading();
                    } else {
                        hideLoading();
                        pos.prev().attr('selected', '');
                        agentList.empty();
                        $('.agentListContainer').addClass('hidden');
                        swal("Warning!", response.message, "warning");
                    }
                },
                error: function(err) {
                    hideLoading();
                    agentList.empty();
                    $('.agentListContainer').addClass('hidden');
                    swal("Error!", err.statusText, "error");
                }
            });
        }
    }


    function returnToListOfAgents() {
        $('.openAgentListPage').click();
    }




    function validateAgentFormInputs() {
        var hasError = false;

        var pos = $('.agentPositionList');

        $.each($('.addAgentForm input'), function(i, n) {
            var input = $(this);

            if (input.val().trim() == "") {
                if (!input.hasClass('agentEmail')) {
                    input.parent().addClass('has-error');
                    input.parent().find('span').html('This is required..');
                    hasError = true;
                }
            } else if (input.hasClass('agentEmail')) {
                if (!validateEmail(input.val().trim())) {
                    input.parent().addClass('has-error');
                    input.parent().find('span').html('This email is invalid..');
                    hasError = true;
                } else {
                    input.parent().removeClass('has-error');
                    input.parent().find('span').html('');
                }
            } else {
                input.parent().removeClass('has-error');
                input.parent().find('span').html('');
            }
        });

        var position = parseInt(pos.val());
        var higherUps = $('.agentList').val();


        if (!(position >= 1 && position <= 5)) {
            pos.parent().addClass('has-error');
            pos.parent().find('span').html('Invalid position ID');
            hasError = true;
        } else if (position >= 2 && position <= 5) {
            if ($('.agentListContainer').hasClass("hidden") || $('.agentList option').length == 0) {
                pos.parent().addClass('has-error');
                pos.parent().find('span').html('There has no upline position selected. ');
                hasError = true;
            } else {
                pos.parent().removeClass('has-error');
                pos.parent().find('span').html('');
            }
        } else {
            pos.parent().removeClass('has-error');
            pos.parent().find('span').html('');
        }


        return hasError;
    }



    function validateAgentUpdateFormInputs() {
        var hasError = false;

        $.each($('.updateAgentForm input'), function(i, n) {
            var input = $(this);

            if (input.val().trim() == "") {
                if (!input.hasClass('update_agentEmail')) {
                    input.parent().addClass('has-error');
                    input.parent().find('span').html('This is required..');
                    hasError = true;
                }
            } else if (input.hasClass('update_agentEmail')) {
                if (!validateEmail(input.val().trim())) {
                    input.parent().addClass('has-error');
                    input.parent().find('span').html('This email is invalid..');
                    hasError = true;
                } else {
                    input.parent().removeClass('has-error');
                    input.parent().find('span').html('');
                }
            } else {
                input.parent().removeClass('has-error');
                input.parent().find('span').html('');
            }
        });

        return hasError;
    }


    function clearAgentFormInputs() {
        $('.addAgentForm input').val('').focus();
    }


    function postThisAgent() {
        if (validateAgentFormInputs()) {
            return false;
        }

        confirmWithSwal(function() {
                showLoading();
                var Fname = $('.agentFname').val().trim();
                var Lname = $('.agentLname').val().trim();
                var Mname = $('.agentMname').val().trim();
                var Address = $('.agentAddress').val().trim();
                var ContactNumber = $('.agentContactNumber').val().trim();
                var Email = $('.agentEmail').val().trim();
                var pos = $('.agentPositionList').val();
                var higherUps = $('.agentList').val();
                var accountNumber = $('.agentAccountNumber').val();


                var data = {
                    'agentFname': Fname,
                    'agentLname': Lname,
                    'agentMname': Mname,
                    'agentAddress': Address,
                    'agentContactNumber': ContactNumber,
                    'pos': pos,
                    'higherUps': higherUps,
                    'agentEmail': Email,
                    'accountNumber': accountNumber
                };



                $.ajax({
                    type: 'POST',
                    url: apiUrl + "/dashboard/agent/addAgent",
                    data: data,
                    cache: false,
                    async: true,
                    success: function(response) {
                        if (response.success) {
                            clearAgentFormInputs();

                            hideLoading();
                            swal("Success!", response.message, "success");

                            window.clearTimeout(loadInBackground);
                        } else {
                            hideLoading();
                            swal("Warning!", response.message, "warning");
                            window.clearTimeout(loadInBackground);
                        }
                    },
                    error: function(err) {
                        hideLoading();
                        swal("Error!", err.statusText, "error");
                        window.clearTimeout(loadInBackground);
                    }
                });
            },
            'Are you sure to submit this agent?',
            'warning');

    }





    function btn_removeThisAgent() {
        var id = $(this).attr('data-id');
        confirmWithSwal(function() {
                showLoading();
                $.ajax({
                    type: 'POST',
                    url: apiUrl + "/dashboard/agent/removeAgent",
                    data: { id: id },
                    cache: false,
                    async: true,
                    success: function(response) {
                        if (response.success) {

                            // var table = $('.agentListTable').DataTable();
                            // table.clear().draw();
                            // table.rows.add(response.agents).draw();

                            hideLoading();
                            swal("Success!", response.message, "success");
                            openAgentListPage();
                            // window.clearTimeout(loadInBackground);
                        } else {
                            hideLoading();
                            swal("Warning!", response.message, "warning");
                            window.clearTimeout(loadInBackground);
                        }
                    },
                    error: function(err) {
                        hideLoading();
                        swal("Error!", err.statusText, "error");
                        window.clearTimeout(loadInBackground);
                    }
                });
            },
            'Are you sure to remove this agent?',
            'warning');
    }


    function updateThisAgent() {

        if (validateAgentUpdateFormInputs()) {
            return false;
        }

        var id = $(this).attr('data-id');
        confirmWithSwal(function() {
                showLoading();
                var Fname = $('.update_agentFname').val().trim();
                var Lname = $('.update_agentLname').val().trim();
                var Mname = $('.update_agentMname').val().trim();
                var Address = $('.update_agentAddress').val().trim();
                var ContactNumber = $('.update_agentContactNumber').val().trim();
                var Email = $('.update_agentEmail').val().trim();
                var accountNumber = $('.update_agentAccountNumber').val().trim();
                var pos = $('.agentPositionList').val();
                var higherUps = $('.agentList').val();

                var data = {
                    'id': id,
                    'agentFname': Fname,
                    'agentLname': Lname,
                    'agentMname': Mname,
                    'agentAddress': Address,
                    'agentContactNumber': ContactNumber,
                    'agentEmail': Email,
                    'pos': pos,
                    'higherUps': higherUps,
                    'accountNumber': accountNumber
                };

                $.ajax({
                    type: 'POST',
                    url: apiUrl + "/dashboard/agent/updateAgent",
                    data: data,
                    cache: false,
                    async: true,
                    success: function(response) {
                        if (response.success) {
                            hideLoading();
                            swal("Success!", response.message, "success");
                            window.clearTimeout(loadInBackground);
                            openUpdateAgentForm(id);
                        } else {
                            hideLoading();
                            swal("Warning!", response.message, "warning");
                            window.clearTimeout(loadInBackground);
                        }
                    },
                    error: function(err) {
                        hideLoading();
                        swal("Error!", err.statusText, "error");
                        window.clearTimeout(loadInBackground);
                    }
                });
            },
            'Are you sure to submit this updates?',
            'warning');

    }

    function openAgentProfile() {
        var id = $(this).attr('data-id');
        gl_agentID = id;
        openUpdateAgentForm(id);
    }

    function openUpdateAgentForm(id) {
        var navParent = $(this);
        showLoading();

        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/getThisAgent",
            data: { id: id },
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {
                    $(".dashboardPage").addClass('hidden');

                    getTemplate(baseTemplateUrl, 'agentProfile.html', function(render) {
                        var renderedhtml = render({ agent: "" });

                        $(".dashboardPage").empty();
                        $(".updateAgentContainer").html(renderedhtml);
                        $('.updateAgentContainer').removeClass('hidden');
                        $('#tbl_releasedCom').dataTable();


                        $('.dateFrom .dateTo').datepicker({
                            format: 'yyyy-mm-dd'
                        });

                        var agentID = 0;
                        var imageName = "no_image.jpg";

                        gl_agentName = response.data[0]['update_agentFname'] + ' ' + response.data[0]['update_agentLname']; 
                        gl_data = {
                            'fname': response.data[0]['update_agentFname'],
                            'lname': response.data[0]['update_agentLname'],
                            'mname': response.data[0]['update_agentMname'],
                            'addre': response.data[0]['update_agentAddress'],
                            'email': response.data[0]['update_agentEmail'],
                            'contact': response.data[0]['update_agentContactNumber']
                        };

                        if (response.role == "Agent") {
                            $('#tab_updateInfo').empty();
                            $('#tab_loginSettings').empty();
                        }

                        document.getElementById("txt_username_agent").value = response.username;

                        $.each(response.data[0], function(i, n) {
                            if (i == 'agentID') {
                                $(('.' + i)).attr('data-id', n)
                                agentID = n;
                            } else if (i == 'update_agentImageName') {
                                imageName = n;
                            } else
                                $(('.' + i)).val(n);
                        });

                        $('.imageAgentID').val(agentID);

                        if (imageName == 'no_image.jpg') {
                            $('#agentPrifilePicture img').attr('src', 'images/no_image.jpg');
                        } else if (imageName == '') {
                            $('#agentPrifilePicture img').attr('src', 'images/no_image.jpg');
                        } else {
                            $('#agentPrifilePicture img').attr('src', 'api/csm_v1/public/uploads/agent/' + agentID + '/' + imageName);
                        }

                        $.each(response.data, function(i, n) {
                            $('.agentProfileTab .agentFullName').html(n['update_agentFname'] + ' ' + n['update_agentLname']);
                            $('.agentProfileTab .agentFullAddress').html(n['update_agentAddress']);
                            $('.agentProfileTab .agentFullEmail').html(n['update_agentEmail']);
                            $('.agentProfileTab .agentFullContactNumber').html(n['update_agentContactNumber']);
                            $('.agentProfileTab .agentFullPosition').html(n['agentPositionName']);
                            $('.agentProfileTab .agentAccountNumber').html(n['update_agentAccountNumber']);
                        });

                        getAgentListProperties(id);

                        $('.updateAgentForm input').focus();
                        $('.date-picker').datepicker();

                        comReleaseTable(response.comReleaseTable);

                        hideLoading();
                    });

                } else {
                    hideLoading();
                    // swal("Warning!", response.message, "warning");
                }
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }

    function customAgentPrintHeader(argument) {
        return `<style>
                  .cms-container{
                    padding: 5px; 
                    margin-bottom: 25px;
                  }

                  .cms-text-center{
                      text-align: center;
                  }

                  .cms_title{
                    float: left
                  }

                  .cms_font{
                    font-size: 12pt;
                    font-weight: bold;
                  }

              </style>

              <div class="cms-container cms-text-center">
                <img src="images/logo-invert.jpg" class="cms-logo">
              </div>
              <div class="cms-container">
                <h2 class="cms-text-center" style="margin-bottom: 5px; margin-top: 0px">>R and Sons Properties Co.</h2>
             <p class="cms-text-center" style="margin:0px;">Address: 2nd floor S10 at The Paddock, J.Camus St.</p>
                      <p class="cms-text-center" style="margin:0px;">Corner General Luna St. , Brgy. 4-A Poblacion District,</p>
                      <p class="cms-text-center" style="margin:0px;">Davao City, Davao del Sur, Philippines 8000</p>
                      <p class="cms-text-center" style="margin:0px;">Telephone Number (082) 235-1146</p>
              </div>

              <div class="cms_title">
                  <h1 class="cms_font">Name: ` + $('.agentFullName').html(); + ` </h1>
              </div>
            `;
    }

    function comReleaseTable(data) {
        var table = $('#comRelTable').DataTable({
            buttons: [

                {
                    extend: 'print',
                    className: 'btn btn-outline red',
                    title: '',
                    customize: function(win) {
                        $(win.document.body)
                            .css('font-size', '10pt')
                            .prepend(customAgentPrintHeader());


                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }

                },
                { extend: 'excel', className: 'btn btn-outline blue' },
                { extend: 'pdf', className: 'btn btn-outline green' }

            ],

            "footerCallback": function(row, data, start, end, display) {
                var api = this.api(),
                    data;

                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                // Total over all pages
                total = api
                    .column(3)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total over this page
                pageTotal = api
                    .column(3, { page: 'current' })
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(3).footer()).html(
                    '₱' + pageTotal.toFixed(2) + ' ( ₱' + total.toFixed(2) + ' total)'
                );
            }
        });

        table.buttons().container()
            .appendTo($('.col-sm-6:eq(0)', table.table().container()));

        table.clear().draw();
        table.rows.add(data).draw();
    }

    function getAgentListProperties(id) {
        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/getAgentPropertyList",
            data: { id: id },
            cache: false,
            async: true,
            success: function(response) {

                if (response.success) {
                    $('.imageClientID').val(id);

                    $('.totalClients').html(response.clientCount);
                    $('.totalClients').attr('data-value', response.clientCount)

                    $('.totalProp').html(response.proptCount);
                    $('.totalProp').attr('data-value', response.proptCount);
                    var totalCom = 0;
                    $.each(response.properties, function(i, n) {
                        totalCom = (totalCom + parseInt(n['Commission']));

                        $('.totalCom').html('₱ ' + addCommas(totalCom));
                        $('.totalCom').attr('data-value', totalCom)

                        $('.agentProp').append(
                            '<li>' +
                            '<span class="sale-info margin-10px"> ' + n['Property'] + '  (' + n['paymentsFor'] + ')' +
                            '<i class="fa fa-img-up"></i>' +
                            '</span>' +
                            '<span class="sale-num margin-5px margin-10px"> ₱ ' + accounting.format(n['Commission'], 2) + ' </span>' +
                            '</li>'
                        );
                    });

                    hideLoading();

                } else {

                    hideLoading();
                    // swal("Warning!", response.message, "warning");
                }
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }


    function openAgentListPage() {
        showLoading();
        var navParent = $(this);

        getTemplate(baseTemplateUrl, 'agentList.html', function(render) {
            var renderedhtml = render({ agents: "" });

            $('.page-sidebar-menu').find('.nav-item').removeClass('active open');

            if (navParent.parent().hasClass('nav-item')) {
                navParent.parent().addClass('active open');
            }

            if (navParent.parent().parent().parent().hasClass('nav-item')) {
                navParent.parent().parent().parent().addClass('active open');
            }
            $(".dashboardPage").empty();


            $(".agentListContainer").html(renderedhtml);
            $('.agentListContainer').removeClass('hidden');

            tableAgentListTable();
            getAllAgents();
        });

    }

    function styleCss(argument) {
        $('.cssList').empty();

        $('.cssList').append(
            `.content-header{
                margin-bottom: 5px: 
            }

            p{
                margin: 0px,0px;
            }   

            .cms-container{
                padding: 5px; 
                /*claer: both;*/
            }

            .cms-col-md-12{
              margin: 0 auto;
             /* margin: 5px;*/
              max-width: 100%;
              /*background-color: #ccc;*/
              border-radius: 3px;
              padding: 15px; 
            }

            .cms-form-group{
                padding:5px;
                width: 100%;
            }

            .cms-bold{
                font-weight: bold;
            }

            .cms-toUppecase{
                text-transform: uppercase;
            }

            .cms-row{
                width: 100%;
            }

            .cms-red{
                color: red;
            }

            .cms-pdng-5{
                padding-left: 5px;
            }

            table.cms-tables {
                border-collapse: collapse;
                width: 100%;
            }

            table.cms-tables tbody  tr th, table.cms-tables tbody  tr td{
                padding: 8px;
                text-align: left;
                border-bottom: 1px solid #ddd;
            }

             table.cms-tables tbody  tr th, table.cms-tables tbody  tr td {
                border: 1px solid #ddd;
                text-align: left;
            }

           table.cms-tables tbody tr:hover{background-color:#f5f5f5}

           table.cms-tables tbody tr th {
                background-color: #4CAF50;
                color: white;
            }

            .cms-text-right{
                text-align: right;
            }

            .cms-text-normal{
                font-weight: normal;
            }

            .cms-text-center{
                text-align: center;
            }

            .cms-pdng-right{
                padding-right: 5px;
            }

            .cms-num{
                width: 5%
            }

            .cms-logo{
               width: 42%;
               height: 87px;
            }

            .cms-margin-top {
                margin-top: 32px;
            }

            .cms-col-md-12 {
                margin: 0 auto;
                max-width: 100%;
                border-radius: 3px;
                padding: 7px;
            }

            .reldate{
                text-align: right;
                float: right;
                width: 49%;
            }

            span.releaseDate {
                font-size: small;
                font-weight: 400;
            }

            .cms-col-md-12.tlb_header {
                display: inline;
            }`
        );
    }



    function btnreleaseThisCommisionIsClick() {
        var releaseData = [];
        $('.commisionReleasingTableListOfProperties tr').each(function() {

            if (!this.rowIndex) return; // skip first row
            releaseData = {
                'cpID': this.cells[0].innerHTML,
                'clientName': this.cells[1].innerHTML,
                'Property': this.cells[2].innerHTML,
                'ContractPrice': this.cells[3].innerHTML,
                'payment': this.cells[4].innerHTML,
                'Amount': this.cells[5].innerHTML
            }
        });

        var amountToRelease = $('.btnreleaseThisCommision').attr('data-amountToRelease');
        if (amountToRelease <= 0) {
            swal("Warning!", 'There has nothing to release for now.', "warning");
            return false
        } else if ($('.releaseNO').val().length <= 0 || $('.releaseDate').val().length <= 0) {
            $('.releaseDate').css("border-bottom", "1px solid #f30e0e");
            swal("Warning!", 'Please include release date!.', "warning");
            return false
        }

        confirmWithSwal(function() {
            showLoading();
            var agentID = $('.commissionRelaseAgentList').val().trim();
            var comNO = $('.releaseNO').val();
            var releaseDate = $('.releaseDate').val();
            $.ajax({
                type: 'POST',
                url: apiUrl + "/dashboard/agent/releasetThisAgentCommissionPerPrperty",
                data: { agentID: agentID, comNo: comNO, releaseDate: releaseDate },
                cache: false,
                async: true,
                success: function(response) {
                    // console.log(response)

                    $('.releaseReport  table').empty();
                    if (response.success) {

                        styleCss();

                        $.each(response.results, function(i, n) {
                            $('.releaseReport table').append(
                                '<tr>' +
                                '<td>' + n['clienName'] + '</td>' +
                                '<td>' + n['propertyName'] + '</td>' +
                                '<td>' + accounting.format(n['contractPrice'], 2) + '</td>' +
                                '<td>' + n['payment'] + '</td>' +
                                '<td>' + accounting.format(n['percentageValue'], 2) + '</td>' +
                                // '<td>'+n['dateRelease']+'</td>'+
                                '</tr>'
                            );

                            $('.releaseReport2 table').append(
                                '<tr>' +
                                '<td>' + n['clienName'] + '</td>' +
                                '<td>' + n['propertyName'] + '</td>' +
                                '<td>' + accounting.format(n['contractPrice'], 2) + '</td>' +
                                '<td>' + n['payment'] + '</td>' +
                                '<td>' + accounting.format(n['percentageValue'], 2) + '</td>' +
                                // '<td>'+n['dateRelease']+'</td>'+
                                '</tr>'
                            );

                            // var dateVal = (n['dateRelease']).split(' ').slice(0, 3).join(' ');
                        });


                        $('.releaseReport table').prepend(` <tr>
                                <th>Buyer's Name</th>
                                <th>Property</th>
                                <th>TCP</th>
                                <th>Payment</th>
                                <th>Amount</th>
                              </tr>`);


                        $('.releaseDate').html(toWeirdDate(releaseDate));
                        $('.relSpan').html(comNO);

                        const agentName = $(".commissionRelaseAgentList option:selected").text();
                        $('.lbl_agentName').html(agentName);
                        const releasedNum = $('.releaseCount').val();

                        $('.totalCommissionClaimed').html(accounting.format(response.percentageTotal, 2));
                        $('.totalGrosCom').html(accounting.format(response.gross, 2));
                        $('.txt_wtx').html(accounting.format(response.wtx, 2));

                        var content = document.getElementById('releaseReport').innerHTML;
                        var content2 = document.getElementById('releaseReport2').innerHTML;

                        // 
                        bootbox.dialog({
                            message: content,
                            title: "Agent commision released Report",
                            // onEscape: function() {  },
                            // backdrop: true,
                            buttons: {
                                success: {
                                    label: "Print",
                                    className: "green btn-outline",
                                    callback: function() {
                                        var html = "<html>";
                                        // html+="<head><style>@page {size: auto;margin: 0cm; padding: 0; width: 100%; height: 100%; background-color: white;}@media print {#officialReceipt { page-break-after: always; width: 100%; margin: 0; padding: 0; background-color: white;} body , body > *{width: 100%; height:100%; margin: 0; padding: 0; background-color: white;}}</style></head>";
                                        html += content2;
                                        html += "</html>";
                                        var printWin = window.open();
                                        printWin.document.write(html);
                                        printWin.document.close();
                                        printWin.focus();
                                        printWin.print();
                                        printWin.close();
                                    }
                                }
                            },
                        }).init(function() {
                            hideLoading();
                        });

                        commissionRelaseAgentListIsChange();

                    } else {

                        if (response.message === "Release Number Already used.") {
                            document.getElementById('txt_OrNumber').focus();
                        }

                        // const agentName = $('.commissionRelaseAgentList selected').text();

                        setTimeout(function() { swal("Warning!", response.message, "warning"); }, 0);

                    }

                    hideLoading();
                },
                error: function(err) {
                    hideLoading();
                    setTimeout(function() { swal("Error!", err.statusText, "error"); }, 0);
                }
            });

        }, 'Are you sure to release this?', 'warning');
    }


    function commissionRelaseAgentListIsChange() {
        var agentID = $('.commissionRelaseAgentList').val().trim();

        if (agentID == "") {
            swal('Warning', 'Please select agent first.', 'warning');
            return false;
        }

        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/getThisAgentCommissionPerPrperty",
            data: { agentID: agentID },
            cache: false,
            async: true,
            success: function(response) {
                var table = $('.commisionReleasingTableListOfProperties').DataTable();
                table.clear().draw();
                if (response.success) {
                    table.rows.add(response.results).draw();
                    // $.each(response.results,(i,n)=>{
                    //     table.append(`
                    //         <tr>
                    //         <td></td>
                    //         <td></td>
                    //         <td></td>
                    //         <td></td>
                    //         <td></td>
                    //         <td></td>
                    //         <td>${n[8]}</td>
                    //         <td></td>
                    //         </tr>
                    //     `);
                    // });
                    $('.releaseNO').val(response.relnum);
                    $('.releaseNORroperty').val(response.relnum);

                    $('.agentTotalCommision').html(accounting.format(response.percentageTotal, 2));
                    $('.btnreleaseThisCommision').attr('data-amountToRelease', response.percentageTotal);
                    // $('.releaseCount').val((response.results[0][5] + 1) + " / " + response.results[0][6]);
                    
                    hideLoading();

                } else {

                    $('.agentTotalCommision').html(accounting.format(0, 2));
                    $('.btnreleaseThisCommision').attr('data-amountToRelease', 0);
                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });


    }


    function relComByProp() {
        var commissionPropertyID = $('.commissionPopertyList').val();
        var dateRelese = $('.releaseDateByproperty').val();
        var Ornumber = $('.releaseNORroperty').val();

        // alert(propCP_ID);
        showLoading();
        $.ajax({
            type: 'POST',
            url: apiUrl + "/dashboard/agent/releaseThisComByProperty",
            data: { id: commissionPropertyID, dateRelese: dateRelese, Ornumber: Ornumber },
            cache: false,
            async: true,
            success: function(response) {
                hideLoading();
                if (response.success) {
                    // alert('reload');
                    swal("Success!", response.message, "success");

                    commissionRelaseAgentListIsChange()
                    getPropertyList();
                } else {
                    swal("Warning!", response.message, "warning");
                }
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }


    function getAllAgents() {
        showLoading();

        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/getAll",
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {

                    if ($('.agentListTable').length == 1) {
                        var table = $('.agentListTable').DataTable();
                        table.clear().draw();
                        table.rows.add(response.agents).draw();
                    }


                    var list = $('.commissionRelaseAgentList');

                    if (list.length == 1) {
                        list.empty();
                        $.each(response.agents, function(i, n) {
                            var e = $("<div>" + n[5] + "</div>")
                            e = e.find('.btn_updateThisAgent').attr('data-id');
                            list.append(
                                '<option value="' + e + '">' + n[0] + '</option>'
                            );
                        });

                        commissionRelaseAgentListIsChange();
                    }


                    hideLoading();
                } else {
                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }




    function openCommissionReleasingPage() {
        showLoading();

        var navParent = $(this);


        getTemplate(baseTemplateUrl, 'releaseingOfCommission.html', function(render) {
            var renderedhtml = render({ Agents: "" });
            $('.page-sidebar-menu').find('.nav-item').removeClass('active open');

            if (navParent.parent().hasClass('nav-item')) {
                navParent.parent().addClass('active open');
            }

            if (navParent.parent().parent().parent().hasClass('nav-item')) {
                navParent.parent().parent().parent().addClass('active open');
            }
            $(".dashboardPage").empty();

            $(".commissionReleasingContainer").html(renderedhtml);
            $('.commissionReleasingContainer').removeClass('hidden');

            var Comdetails = $('.ComReleaseDetails').DataTable({
                buttons: [{
                        extend: 'print',
                        className: 'btn dark btn-outline btn_printDetails',
                        message: 'Commission Details!'
                    },
                    {
                        extend: 'pdf',
                        className: 'btn green btn-outline'
                    },
                    {
                        extend: 'excel',
                        className: 'btn purple btn-outline ',
                        extension: '.xls'
                    },

                ],
            });

            Comdetails.buttons().container()
                .appendTo($('.col-sm-6:eq(0)', Comdetails.table().container()));

            var ComSummary = $('.CommisisonReleaseSummary').DataTable({
                buttons: [{
                        extend: 'print',
                        className: 'btn dark btn-outline btn_printSummary',
                        message: 'Commission Summary!'
                    },
                    {
                        extend: 'pdf',
                        className: 'btn green btn-outline'
                    },
                    {
                        extend: 'excel',
                        className: 'btn purple btn-outline ',
                        extension: '.xls'
                    },

                ],
            });

            ComSummary.buttons().container()
                .appendTo($('.col-sm-6:eq(0)', ComSummary.table().container()));


            tablecommisionReleasingTableListOfProperties();

            // getAllAgents();
            getAgentList();
            // getPropertyList();
            // getAllPropertyList();
            // getComRelSummary();
            // getSumDetails();

            // alert($('.commissionRelaseAgentList').val() )
            // commissionRelaseAgentListIsChange();

            $('.select2').select2();
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd'
            });


            hideLoading();
        });
    }

    function getAgentList() {
        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/getAgentNames",
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {

                    $('.releaseNO').val((parseInt(response.number) + 1));
                    $('.releaseNORroperty').val(parseInt(response.number) + 1);
                    $('.releaseAllNORroperty').val(parseInt(response.number) + 1);
                    $('.commissionRelaseAgentList').empty();

                    const data = [];

                    if (response.role == "Agent") {
                        $('.form-to-remove').empty();
                    }else{
                        $('.form-to-remove').css("visibility", "true !important")
                    }

                    $.each(response.agents, function(i, n) {
                        $('.commissionRelaseAgentList').append(
                            '<option value="' + n['agentID'] + '">' + n['agentName'] + '</option>'
                        );

                        const agentDetails = {
                            id: n['agentID'],
                            text: n['agentName']
                        }
                        data.push(agentDetails);
                    });

                    $('#slt_opAgentlisty').select2({
                        data: data
                    });

                    commissionRelaseAgentListIsChange();

                } else {
                    swal("Warning!", response.message, "warning");
                }
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }

    function getPropertyList() {
        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/getPropertList",
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {
                    // $('.releaseNO').val((parseInt(response.number)+1));
                    $('.commissionPopertyList').empty();
                    $.each(response.propList, function(i, n) {
                        $('.commissionPopertyList').append(
                            '<option value="' + n['propId'] + '">' + n['Property'] + '</option>'
                        );
                    });

                    propertyReleasChange();

                } else {
                    swal("Warning!", response.message, "warning");
                }
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }

    function propertyReleasChange() {
        var propertyCP_ID = $('.commissionPopertyList').val();

        if (propertyCP_ID == "") {
            swal('Warning', 'Please select agent first.', 'warning');
            return false;
        }
        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/getThisCommissionPerProperty",
            data: { propertyCP_ID: propertyCP_ID },
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {

                    var table2 = $('.commssionReleasingTableListAgent').DataTable();
                    table2.clear().draw();
                    table2.rows.add(response.agentList).draw();

                    $('.propertyTotalCommission').html(accounting.format(response.totalCom, 2));
                    // $('.btnreleaseThisCommision').attr('data-amountToRelease',response.percentageTotal);

                    hideLoading();

                } else {
                    $('.agentTotalCommision').html(accounting.format(0, 2));
                    $('.btnreleaseThisCommision').attr('data-amountToRelease', 0);
                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });



    }

    function openAddAgentPage() {
        showLoading();

        var navParent = $(this);


        getTemplate(baseTemplateUrl, 'addAgent.html', function(render) {
            var renderedhtml = render({ Agents: "" });
            $('.page-sidebar-menu').find('.nav-item').removeClass('active open');

            if (navParent.parent().hasClass('nav-item')) {
                navParent.parent().addClass('active open');
            }

            if (navParent.parent().parent().parent().hasClass('nav-item')) {
                navParent.parent().parent().parent().addClass('active open');
            }

            $(".dashboardPage").empty();
            $(".addAgentContainer").html(renderedhtml);
            $('.addAgentContainer').removeClass('hidden');

            hideLoading();
        });

    }

    function getAllPropertyList() {
        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/getAllByPropertyName",
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {

                    $('.commissionPopertyAllList').empty();
                    $.each(response.properlist, function(i, n) {
                        $('.commissionPopertyAllList').append(
                            '<option value="' + n['parentID'] + '">' + n['Property'] + '</option>'
                        );

                        $('.getCommissionList').append(
                            '<option value="' + n['parentID'] + '">' + n['Property'] + '</option>'
                        );
                    });

                    commissionAllPropertyListChange();
                    hideLoading();
                } else {

                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });



    }


    // load agents under selected property
    function commissionAllPropertyListChange() {
        var selectedProp = $('.commissionPopertyAllList').val();
        // alert(selectedProp);
        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/loadAgentsUnderSelectedProperty",
            data: { parentID: selectedProp },
            cache: false,
            async: true,
            success: function(response) {

                if (response.success) {
                    var table = $('.commssionReleasingTableListAgentUnderProperty').DataTable();
                    table.clear().draw();
                    table.rows.add(response.list).draw();
                    hideLoading();

                    $('.propertyAllTotalCommission').html(accounting.format(response.total, 2));

                } else {

                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }

    function relAllUnderThisProp() {
        var dateRelease = $('.ReleaseByProperty').val();
        var releaseNumber = $('.releaseAllNORroperty').val();
        var propertyParentID = $('.commissionPopertyAllList').val();

        showLoading();
        $.ajax({
            type: 'POST',
            url: apiUrl + "/dashboard/agent/releaseAllComUnderThisProp",
            data: { dateRel: dateRelease, releaseNum: releaseNumber, propParentID: propertyParentID },
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {
                    hideLoading();
                    swal("Success!", response.message, "success");
                } else {

                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
        openCommissionReleasingPage();
    }

    function getComRelSummary(argument) {
        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/getComRelSummary",
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {

                    table = $('.CommisisonReleaseSummary').DataTable();
                    table.clear().draw();
                    table.rows.add(response.ComSum).draw();

                    hideLoading();
                } else {

                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }

    function getSumDetails(argument) {
        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/getSumComDetails",
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {

                    table = $('.ComReleaseDetails').DataTable();
                    table.clear().draw();
                    table.rows.add(response.ComDetails).draw();

                    hideLoading();
                } else {

                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }

    function updateThisCom(argument) {
        var comVAl = $(this).val();
        // alert(comVAl);
    }

    function getThisPropertylist(argument) {
        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/selectThisProperty",
            data: { parentID: $(this).val() },
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {

                    table = $('.ComReleaseDetails').DataTable();
                    table.clear().draw();
                    table.rows.add(response.data).draw();

                    // swal("Success!", response.message, "success");    
                    hideLoading();
                } else {
                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }

    function releaseThispaymentList() {
        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/releaseThisComList",
            data: { parentID: $('.getCommissionList').val(), relNumber: $('.ThisOrnumber').val(), relDate: $('.releaseThidate').val() },
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {

                    swal("Success!", response.message, "success");
                    openCommissionReleasingPage();
                    hideLoading();

                } else {

                    if (response.message == "Release number already used!") {
                        bootbox.confirm({
                            message: "<span style='font-size:large;'>Release number already been used, override?</span>",
                            buttons: {
                                confirm: {
                                    label: 'Yes',
                                    className: 'btn-success'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-danger'
                                }
                            },
                            callback: function(result) {
                                if (result) {
                                    showLoading();
                                    $.ajax({
                                        type: 'GET',
                                        url: apiUrl + "/dashboard/agent/releaseThisComListOverride",
                                        data: { parentID: $('.getCommissionList').val(), relNumber: $('.ThisOrnumber').val(), relDate: $('.releaseThidate').val() },
                                        cache: false,
                                        async: true,
                                        success: function(response) {
                                            if (response.success) {
                                                swal("Success!", response.message, "success");
                                                openCommissionReleasingPage();
                                                hideLoading();
                                            } else {
                                                hideLoading();
                                                swal("Warning!", response.message, "warning");
                                            }
                                        },
                                        error: function(err) {
                                            hideLoading();
                                            swal("Error!", err.statusText, "error");
                                        }
                                    });
                                }
                            }
                        });

                    }
                    hideLoading();
                    // swal("Warning!", response.message, "warning");
                }
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }


    function getThisDate() {
        var datefrom = $('.dateFromthis').val();
        var dateto = $('.dateTothis').val();
        var parentID = $('.getCommissionList').val();

        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/getThisPropertyByDate",
            data: { dateFrom: datefrom, dateTo: dateto, parentID: parentID },
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {

                    table = $('.ComReleaseDetails').DataTable();
                    table.clear().draw();
                    table.rows.add(response.details).draw();

                    hideLoading();
                } else {
                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }

    function getThisSummary() {

        var datefrom = $('.dateFromComSum').val();
        var dateto = $('.dateToComCum').val();

        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/getThisSummaryByDate",
            data: { dateFrom: datefrom, dateTo: dateto },
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {

                    table = $('.CommisisonReleaseSummary').DataTable();
                    table.clear().draw();
                    table.rows.add(response.ComSum).draw();
                    hideLoading();

                } else {
                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });

    }

    function printThis() {

        var datefrom = $('.dateFromthis').val();
        var dateto = $('.dateTothis').val();
        var parentID = $('.getCommissionList').val();

        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/printAndSaveThis",
            data: { dateFrom: datefrom, dateTo: dateto, parentID: parentID, relNumber: $('.ThisOrnumber').val(), relDate: $('.releaseThidate').val() },
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {

                    openCommissionReleasingPage();
                    hideLoading();
                } else {
                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }


    function loadDataForOptionalRel(argument) {
        const agentId = $('#slt_opAgentlisty').val();
        if (agentId == "") {
            swal('Warning', 'Please select agent first.', 'warning');
            return false;
        }

        loadClientComDetails(agentId);


    }

    function loadClientComDetails(agentId) {
        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/geThisAgentoptionalRel",
            data: { agentID: agentId },
            cache: false,
            async: true,
            success: function(response) {

                var table2 = $('.commissionReloptional').DataTable();
                table2.clear().draw();

                if (response.success) {
                    table2.rows.add(response.results).draw();

                    $('.releaseNO').val(response.relnum);
                    $('.releaseNORroperty').val(response.relnum);

                    $('.agentTotalCommision').html(accounting.format(response.percentageTotal, 2));
                    $('.btnreleaseThisCommision').attr('data-amountToRelease', response.percentageTotal);
                    // $('.releaseCount').val((response.results[0][5] + 1) + " / " + response.results[0][6]);

                } else {

                    $('.agentTotalCommision').html(accounting.format(0, 2));
                    $('.btnreleaseThisCommision').attr('data-amountToRelease', 0);
                    swal("Warning!", response.message, "warning");
                }
                hideLoading();
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }

        });
    }

    function getCommissionListFromThisClient(argument) {
        const agentId = $(this).val();
        if (agentId == "") {
            swal('Warning', 'Please select agent first.', 'warning');
            return false;
        }

        loadClientComDetails(agentId);
    }

    function calculateTotalRelComFromOptionalRelease(argument) {
        let totalCom = 0;
        let selectedCom = $('.ck_properyID');

        $.each(selectedCom, function(i, n) {
            if ($(this).prop('checked')) {
                totalCom += parseFloat($(this).attr('data-value'));
                // console.log($(this).prop('checked'));
            }
        })

        $('.agentTotalCommision').html(accounting.format(totalCom, 2));
    }

    function releaseOptionalCom() {
        let releaseData = [];
        let relPropertyID = [];

        $('.commissionReloptional tr').each(function() {
            // console.log($(this).find('input').prop('checked'));
            if (!this.rowIndex) return; // skip first rowsw
            if ($(this).find('input').prop('checked')) {
                releaseData = {
                    'clientName': this.cells[1].innerHTML,
                    'Property': this.cells[2].innerHTML,
                    'ContractPrice': this.cells[3].innerHTML,
                    'payment': this.cells[4].innerHTML,
                    'Amount': this.cells[5].innerHTML
                };

                relPropertyID.push($(this).find('input').attr('data-id'));
            }
        });

        // return false;
        // console.log(releaseData);
        // // relcom
        // alert(JSON.stringify(releaseData));

        var amountToRelease = $('.agentTotalCommision').html();

        if (amountToRelease <= 0) {

            swal("Warning!", 'There has nothing to release for now.', "warning");
            return false

        } else if ($('.releaseNO').val().length <= 0 || $('.op_releaseDate').val().length <= 0) {

            $('.op_releaseDate').css("border-bottom", "1px solid #f30e0e");

            swal("Warning!", 'Please include release date!.', "warning");

            return false

        }

        confirmWithSwal(function() {
            showLoading();

            const agentID = $('#slt_opAgentlisty').val().trim();
            const agentName = $('#slt_opAgentlisty selected').text();
            const comNO = $('#txt_remcomnumOp').val();
            const releaseDate = $('.op_releaseDate').val();

            $.ajax({
                type: 'POST',
                url: apiUrl + "/dashboard/agent/realeaseThisOptionalComRelease",
                data: { agentID: agentID, comNo: comNO, releaseDate: releaseDate, releasePropId: relPropertyID },
                cache: false,
                async: true,
                success: function(response) {

                    $('.releaseReport  table').empty();
                    if (response.success) {

                        styleCss();

                        $.each(response.results, function(i, n) {
                            $('.releaseReport table').append(
                                '<tr>' +
                                '<td>' + n['clienName'] + '</td>' +
                                '<td>' + n['propertyName'] + '</td>' +
                                '<td>' + accounting.format(n['contractPrice'], 2) + '</td>' +
                                '<td>' + n['payment'] + '</td>' +
                                '<td>' + accounting.format(n['percentageValue'], 2) + '</td>' +
                                // '<td>'+n['dateRelease']+'</td>'+
                                '</tr>'
                            );
                            $('.releaseReport2 table').append(
                                '<tr>' +
                                '<td>' + n['clienName'] + '</td>' +
                                '<td>' + n['propertyName'] + '</td>' +
                                '<td>' + accounting.format(n['contractPrice'], 2) + '</td>' +
                                '<td>' + n['payment'] + '</td>' +
                                '<td>' + accounting.format(n['percentageValue'], 2) + '</td>' +
                                // '<td>'+n['dateRelease']+'</td>'+
                                '</tr>'
                            );

                            // var dateVal = (n['dateRelease']).split(' ').slice(0, 3).join(' ');
                        });

                        $('.releaseReport table').prepend(` <tr>
                            <th>Buyer's Name</th>
                            <th>Property</th>
                            <th>TCP</th>
                            <th>Payment</th>
                            <th>Amount</th>
                          </tr>`);

                        $('.releaseDate').html(toWeirdDate(releaseDate));
                        $('.relSpan').html(comNO);

                        const agentName = $("#slt_opAgentlisty option:selected").text();
                        $('.lbl_agentName').html(agentName);
                        const releasedNum = $('.releaseCount').val();

                        $('.totalCommissionClaimed').html(accounting.format(response.percentageTotal, 2));
                        $('.totalGrosCom').html(accounting.format(response.gross, 2));
                        $('.txt_wtx').html(accounting.format(response.wtx, 2));

                        const content = document.getElementById('releaseReport').innerHTML;
                        const content2 = document.getElementById('releaseReport2').innerHTML;

                        // 
                        bootbox.dialog({
                            message: content,
                            title: "Agent commision released Report",
                            // onEscape: function() {  },
                            // backdrop: true,
                            buttons: {
                                success: {
                                    label: "Print",
                                    className: "green btn-outline",
                                    callback: function() {
                                        var html = "<html>";
                                        // html+="<head><style>@page {size: auto;margin: 0cm; padding: 0; width: 100%; height: 100%; background-color: white;}@media print {#officialReceipt { page-break-after: always; width: 100%; margin: 0; padding: 0; background-color: white;} body , body > *{width: 100%; height:100%; margin: 0; padding: 0; background-color: white;}}</style></head>";
                                        html += content2;
                                        html += "</html>";
                                        var printWin = window.open();
                                        printWin.document.write(html);
                                        printWin.document.close();
                                        printWin.focus();
                                        printWin.print();
                                        printWin.close();
                                    }
                                }
                            },
                        }).init(function() {
                            hideLoading();
                        });

                        loadClientComDetails(agentID);

                    } else {

                        if (response.message === "Release Number Already used.") {
                            document.getElementById('txt_OrNumber').focus();
                        }

                        setTimeout(function() { swal("Warning!", response.message, "warning"); }, 0);

                    }

                    hideLoading();
                },
                error: function(err) {
                    hideLoading();
                    setTimeout(function() { swal("Error!", err.statusText, "error"); }, 0);
                }
            });

        }, 'Are you sure to release this?', 'warning');
    }

    function getThisReleadCommissionList() {
        const selection = document.getElementById('slt_searComType').value;
        const val = document.getElementById('txt_searchWithSelecteOption').value;
        let totalCom = ''

        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "/dashboard/agent/getThisReleasedCom",
            data: { agentID: gl_agentID, value: val, option: selection},
            cache: false,
            async: true,
            success: function(response) {
                printForm();

                var table = $('#tbl_releasedCom').DataTable();
                const tableValue = response.data;

                table.clear().draw();
                if (response.success) {
                    table.rows.add(tableValue).draw();
                    totalCom = getSum(tableValue);

                    $('#totalAmount').html(totalCom);
                    let txt_wtx = 0;
                    let totalGrosCom = 0;
                    let totalCommissionClaimed = 0;
                    let dateRel = 0;
                    $('#printReleasedComForm table').empty();

                    $.each(tableValue, function (i,n) {
                        $('#printReleasedComForm table').append(
                            '<tr>' +
                            '<td>' + n[0] + '</td>' +
                            '<td>' + n[1] + '</td>' +
                            '<td>' + accounting.format(n[2], 2) + '</td>' +
                            '<td>' + n[3] + '</td>' +
                            '<td>' + accounting.format(n[4], 2) + '</td>' +
                            '</tr>'
                        );

                        totalCommissionClaimed =  totalCommissionClaimed + Number(n[4]); 
                        totalGrosCom =  totalGrosCom + Number(n[5]); 
                        dateRel = n[6];
                    })

                    txt_wtx =  totalGrosCom - totalCommissionClaimed;

                    $('.lbl_agentName').html(gl_agentName);
                    $('.txt_wtx').html(txt_wtx.toFixed(2));
                    $('.totalGrosCom').html(totalGrosCom.toFixed(2));
                    $('.totalCommissionClaimed').html(totalCommissionClaimed);
                    $('.releaseDate').html(toWeirdDate(dateRel));


                } else {
                    swal("Warning!", response.message, "warning");
                }
                hideLoading();
            },
            error: function(err) {
                swal("Error!", err.statusText, "error");
                hideLoading();
            }

        });
    }

    function getSum(data) {
        let total = 0;
        data.map(obj =>{ 
           const val = Number(obj[4]);
           total = total + val;
        });
        return total.toFixed(2);
    }

    function printForm(argument) {
        $('#printReleasedComForm').empty();

        const printForm =`
            <style type="text/css">
                .content-header{
                    margin-bottom: 5px: 
                }

                p{
                    margin: 0px,0px;
                }   

                .cms-container{
                    padding: 5px; 
                }

                .cms-col-md-12{
                  margin: 0 auto;
                  max-width: 100%;
                  border-radius: 3px;
                  padding: 15px; 
                }

                .cms-form-group{
                    padding:5px;
                    width: 100%;
                }

                .cms-bold{
                    font-weight: bold;
                }

                .cms-toUppecase{
                    text-transform: uppercase;
                }

                .cms-row{
                    width: 100%;
                }

                .cms-red{
                    color: red;
                }

                .cms-pdng-5{
                    padding-left: 5px;
                }

                table.cms-tables-receipt {
                    border-collapse: collapse;
                    width: 100%;
                    font-size: 80% !important;
                    color: black;
                }

                table.cms-tables-receipt tbody  tr th, table.cms-tables-receipt tbody  tr td{
                    padding: 8px;
                    text-align: left;
                    border-bottom: 1px solid #ddd;
                }

                 table.cms-tables-receipt tbody  tr th, table.cms-tables-receipt tbody  tr td {
                    border: 1px solid #ddd;
                    text-align: left;
                }

               table.cms-tables-receipt tbody tr:hover{background-color:#f5f5f5}

               table.cms-tables-receipt tbody tr th {
                    background-color: #4CAF50;
                    color: white;
                }

                .cms-text-right{
                    text-align: right;
                }

                .cms-text-normal{
                    font-weight: normal;
                }

                .cms-text-center{
                    text-align: center;
                }

                .cms-pdng-right{
                    padding-right: 5px;
                }

                .cms-num{
                    width: 5%
                }

                .cms-logo{
                   width: 42%;
                   height: 87px;
                }

                .cms-margin-top {
                    margin-top: 32px;
                }

                .cms-col-md-12 {
                    margin: 0 auto;
                    max-width: 100%;
                    border-radius: 3px;
                    padding: 7px;
                }

                span.releaseDate {
                    font-size: small;
                    font-weight: 400;
                }

                span.cms-pdng-5.cms-text-normal.cms-red.cms-bold.lbl_agentName {
                    color: #292929;
                }

                .reldate{
                    text-align: right;
                    float: right;
                    width: 49%;
                }

                .flex-container {
                  padding: 0;
                  margin: 0;
                  list-style: none;
                  
                  display: -webkit-box;
                  display: -moz-box;
                  display: -ms-flexbox;
                  display: -webkit-flex;
                  display: flex;
                  
                  -webkit-flex-flow: row wrap;
                  justify-content: space-around;
                }

                .flex-item {
                  width: 48%;
                  height: 150px;
                  font-size: 80% !important;
                }

            </style>


            <ul class="flex-container">
                <li class="flex-item">

                    <div class="cms-col-md-12">
                        <label for="" style="float: right"> RNS copy</label>
                    </div>

                    <div class="cms-container cms-text-center">
                      <img src="../images/logo-invert.jpg" class="cms-logo">
                    </div>

                    <div class="cms-container">
                      <h2 class="cms-text-center headerTitle" style="margin-bottom: 5px; margin-top: 0px;">R and Sons Properties Co.</h2>
                       <p class="cms-text-center" style="margin:0px;">Address: 2nd floor S10 at The Paddock, J.Camus St.</p>
                      <p class="cms-text-center" style="margin:0px;">Corner General Luna St. , Brgy. 4-A Poblacion District,</p>
                      <p class="cms-text-center" style="margin:0px;">Davao City, Davao del Sur, Philippines 8000</p>
                      <p class="cms-text-center" style="margin:0px;">Telephone Number (082) 235-1146</p>
                    </div>

                    <div class="cms-container cms-margin-top">
                        <div class="cms-col-md-12">
                            <div class="cms-form-group">
                                <label class="cms-bold cms-toUppecase"> Name :<span class="cms-pdng-5 cms-text-normal cms-red cms-bold lbl_agentName"> Dohn Doe Monroe </span> </label> 
                            </div>
                        </div>
                    </div>

                    <div class="cms-container">
                        <div style="white-space:nowrap; padding-left: 10px; padding-right: 10px;">
                            <!-- <label class="cms-bold cms-toUppecase">Total Commission <span class="cms-pdng-5 cms-text-normal cms-red cms-bold totalCommissionClaimed">: P120,000,000.00</span> </label>  -->
                            <label class="cms-bold cms-toUppecase" style="float: right;"> Date: <span class="releaseDate"> Jan, 20 2018 </span></label>
                        </div>
                    </div>

                    <div class="cms-container">
                        <div class="cms-col-md-12">
                            <table class="cms-tables-receipt">
                              <tr>
                                <th>Buyer's Name</th>
                                <th>Property</th>
                                <th>TCP</th>
                                <th>Payment</th>
                                <th>Amount</th>
                              </tr>
                            </table>
                        </div>

                        <div class="cms-col-md-12" style="text-align: left">
                            <label class="NoLeb">NO: <span class="relSpan"></span></label>
                            <label class="cms-bold" style ="padding-left: 10px; float: right"> WTX: <span class="cms-pdng-5 cms-text-normal txt_wtx"></span> </label>
                        </div>

                        <div class="cms-col-md-12" style="margin-top: -14px">
                            <label class="NoLeb"> Recieved By: <span class="lbl_agentName" style="text-decoration: underline"></span></label>
                            <label class="cms-bold" style="float: right">Gross Com: <span class="cms-pdng-5 cms-text-normal cms-red cms-bold totalGrosCom"> </span> </label> 
                        </div>

                        <div class="col-md-12" style="margin-top: 14px">
                            <span style="padding-left: 60px; color: gray; margin-top: -13px; display: block;"> Signature over Printed Name </span>
                            <label class="cms-bold" style="float: right; padding-right: 7px; margin-top: -21px;">Net Com :<span class="cms-pdng-5 cms-text-normal cms-red cms-bold totalCommissionClaimed">:</span> </label> 
                        </div>
                    
                    </div>
                </li>

                <li class="flex-item">
                    <div class="cms-col-md-12">
                        <label for="" style="float: right"> Agent's Copy </label>
                    </div>

                    <div class="cms-container cms-text-center">
                      <img src="images/logo-invert.jpg" class="cms-logo">
                    </div>

                    <div class="cms-container">
                            <h2 class="cms-text-center headerTitle" style="margin-bottom: 5px; margin-top: 0px;">R and Sons Properties Co.</h2>
                       <p class="cms-text-center" style="margin:0px;">Address: 2nd floor S10 at The Paddock, J.Camus St.</p>
                      <p class="cms-text-center" style="margin:0px;">Corner General Luna St. , Brgy. 4-A Poblacion District,</p>
                      <p class="cms-text-center" style="margin:0px;">Davao City, Davao del Sur, Philippines 8000</p>
                      <p class="cms-text-center" style="margin:0px;">Telephone Number (082) 235-1146</p>
                    </div>

                     <div class="cms-container cms-margin-top">
                        <div class="cms-col-md-12">
                            <div class="cms-form-group">
                                <label class="cms-bold cms-toUppecase"> Name :<span class="cms-pdng-5 cms-text-normal cms-red cms-bold lbl_agentName"> Dohn Doe Monroe </span> </label> 
                            </div>
                        </div>
                    </div>

                    <div class="cms-container">
                        <div style="white-space:nowrap; padding-left: 10px; padding-right: 10px;">
                           <!--  <label class="cms-bold cms-toUppecase">Total Commission <span class="cms-pdng-5 cms-text-normal cms-red cms-bold totalCommissionClaimed">:</span> </label> --> 
                            <label class="cms-bold cms-toUppecase" style="float: right;"> Date: <span class="releaseDate"> Jan, 20 2018 </span></label>
                        </div>
                    </div>

                    <div class="cms-container">
                        <div class="cms-col-md-12">
                            <table class="cms-tables-receipt">
                              <tr>
                                <th>Buyer's Name</th>
                                <th>Property</th>
                                <th>TCP</th>
                                <th>Payment</th>
                                <th>Amount</th>
                              </tr>
                            </table>
                        </div>

                        <div class="cms-col-md-12" style="text-align: left">
                            <label class="NoLeb">NO: <span class="relSpan"></span></label>
                            <label class="cms-bold" style ="padding-left: 10px; float: right"> WTX: <span class="cms-pdng-5 cms-text-normal txt_wtx"></span></label>
                        </div>

                        <div class="cms-col-md-12" style="margin-top: -14px">
                            <label class="NoLeb"> Recieved By: <span class="lbl_agentName" style="text-decoration: underline"></span></label>
                            <label class="cms-bold" style="float: right">Gross Com: <span class="cms-pdng-5 cms-text-normal cms-red cms-bold totalGrosCom"> </span> </label> 
                        </div>

                        <div class="col-md-12" style="margin-top: 14px">
                            <span style="padding-left: 60px; color: gray; margin-top: -13px; display: block;"> Signature over Printed Name </span>
                            <label class="cms-bold" style="float: right; padding-right: 7px; margin-top: -21px;">Net Com :<span class="cms-pdng-5 cms-text-normal cms-red cms-bold totalCommissionClaimed">:</span> </label> 
                        </div>

                    </div>
                </li>
            </ul>`;


            $('#printReleasedComForm').append(printForm);
    }


    function printCom(argument) {
        const content2 = document.getElementById('printReleasedComForm').innerHTML;

        var html = "<html>";
        html += content2;
        html += "</html>";
        var printWin = window.open();
        printWin.document.write(html);
        printWin.document.close();
        printWin.focus();
        printWin.print();
        printWin.close();
    }

    function addAgentLoginUserName(argument) {
        const username = document.getElementById("txt_username_agent").value;

        $.ajax({
            type: 'PUT',
            url: apiUrl + "/dashboard/agent/addAgentUsernameLogin",
            data: {agentID: gl_agentID, info: gl_data, username: username},
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {
                    swal("Success!", response.message, "success");
                } else {
                    swal("Warning!", response.message, "warning");
                }
                    hideLoading();
            },
            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }

    function addAgentLoginUsersPassword(argument) {
        const pass = document.getElementById('txt_newPass_agent').value;
        const reTyepass = document.getElementById('txt_reTypeNew_agent').value;
        
        if (pass !== reTyepass) {
            document.getElementById('txt_reTypeNew_agent').value = "";
            $('#txt_reTypeNew_agent').addClass('label-error');
        }else{
            $('#txt_reTypeNew_agent').removeClass('label-error');

            $.ajax({
                type: 'PUT',
                url: apiUrl + "/dashboard/agent/addAgentPasswordLogin",
                data: {agentID: gl_agentID, info: gl_data, pass: pass},
                cache: false,
                async: true,
                success: function(response) {
                    if (response.success) {
                        swal("Success!", response.message, "success");
                    } else {
                        swal("Warning!", response.message, "warning");
                    }
                        hideLoading();
                },
                error: function(err) {
                    hideLoading();
                    swal("Error!", err.statusText, "error");
                }
            });
        }




    }


}();

jQuery(document).ready(function() {
    Agent.init();
});