var object = [];
var data = [];
var request = 'POST';
var route = '';
var BUSINESS_PROFILEDETAILS = [];
var BUSINESS_DATAID = 0;
var PROPERTY_LIST = [];
var templates = [],
        baseTemplateUrl = 'template/BusParner/';

var BusinessPartner = function() {
    return {
        init: init
    };

    // Init
    function init() {
        addEventHandlers();
    }


    function addEventHandlers(argument) {
    	 $(document).on('click', '.businessPartnerComponent', BusinespPartnerComponent);
         $(document).on('click', '#btb_addbusinessPartner', addBusinessPartner);

         $(document).on('click', '.btn_removePartner', removeBusinessPartner);

         $(document).on('click', '.btn_partnerDetails', viewPartnerProfile);
         $(document).on('click', '#btn_partnerUpdateDetails', updateBusinesPartnerProfile);
         $(document).on('click', '#btn_submit_partUsername', submitUsername);
         $(document).on('click', '#btn_set_passwort_partner', submitPassword);
         $(document).on('click', '#login_settings', getUsername);
         $(document).on('click', '#btn_updateProp', updateProperty);
    	 

         $(document).on('click', '#back_to_list', function (argument) {
             BusinespPartnerComponent();
         });


    	 $(document).on('click', '#btn_cancel', function (argument) {
    	 	bootbox.hideAll();
    	 });

         $(document).on('submit', '#form_businessPartner', submitnewBusinessPartner);


    	 $(document)
            .on('input', '#form_updateBusinessPartnerProfile', function (argument) {
                $('#btn_partnerUpdateDetails').removeClass('hidden');   
            });
    }

    function BusinespPartnerComponent(argument) {
    	const navParent = $(this);
    	template(navParent);	
        getPartnerlist();
    }


    function addBusinessPartner(e) {
    	bootbox.confirm({
		    title: " <strong> <i class='fa fa-user-plus'></i> New BusnissPartner </strong>",
		    message: businessAddForm(),
		    backdrop: false,
		    buttons: {
		        cancel: {
		            label: '<i class="fa fa-times"></i> Cancel',
		            className: 'hidden'
		        },
		        confirm: {
		            label: '<i class="fa fa-check"></i> Confirm',
		            className: 'hidden'
		        }
		    },
		    callback: function (result) {}
		});

        setTimeout(function() {
            getPropertyList();
        }, 1000);
    }

    function submitnewBusinessPartner(event) {
    	event.preventDefault();

        const fname = document.getElementById('txt_fname_bs'); 
        const lname = document.getElementById('txt_lname_bs');
        const {message, success} = object;


        functionURLRequestForm(new FormData(this), 'POST', 'add');
        if (success) {
            swal('Success', message, 'success')
            bootbox.hideAll();
            setTimeout(function() {
                getPartnerlist();
            }, 1000);
        }


    }

    function getPropertyList(argument) {
      functionURLRequest({}, 'GET', 'getproperty');
      
      $('#proplist_bs').empty();
      const {data} = object;
      const listProp = JSON.parse(PROPERTY_LIST);

      $.each(data, function (i,n) {
        let form = `
            <label class="mt-checkbox">
                <input type="checkbox" id="inlineCheckbox`+i+`" name="prop_list[]" value="`+ n.id +`"> `+n.text+`
                <span></span>
            </label>`;

        let propStat = listProp.find(k => parseInt(k) == n.id );
        
        if (typeof(propStat) != 'undefined') {
            form = `
            <label class="mt-checkbox">
                <input type="checkbox" id="inlineCheckbox`+i+`" checked name="prop_list[]" value="`+ n.id +`"> `+n.text+`
                <span></span>
            </label>`;
        }

        $('#proplist_bs').append(form);

      });
    }

    function getPartnerlist(argument) {
        showLoading();
        setTimeout(function() {
            let table;
            $("#tbl_businessPartners").dataTable().fnDestroy()

            functionURLRequest({}, 'GET', 'getParterList');
            const {data} = object;
            table = $('#tbl_businessPartners').DataTable({
                responsive: true
            });
            table.clear().draw();
            table.rows.add(data).draw();

        }, 100);

        hideLoading();
    }

    function removeBusinessPartner(argument) {
        const {success, message } = object;
        const data = { 
            id: $(this).attr('data-id')
        }

        swal({
          title: "Are you sure?",
          text: "You won't be able to recover this.",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes, delete it!",
          closeOnConfirm: false
        },
        function(){
            functionURLRequest(data, 'GET', 'removePartner');
            if (success) {
                swal('Success', message, 'success');
                setTimeout(function() {
                    getPartnerlist();
                }, 1000);
            }
        });
    }


    function viewPartnerProfile(argument) {
        const dataID  = $(this).attr('data-id');
        BUSINESS_DATAID = dataID;   
        $('#form_businessPartner_div, #form_Options').empty();
        functionURLRequest({id: dataID}, 'GET', 'getProfile');

        const {data} = object;

        const profileBreadCrumnbs = `
            <li>
                <i class="icon-home"></i>
                <a href="#Dashboard" class="toHome">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#javascript" id="back_to_list"> <span>Business Partner</span> </a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span> Profile </span>
            </li>
        `;   
        const breadCrumbs  = $('#partnerBreadCrumbs');
        const profile  = $('#form_businessPartner_div');
        breadCrumbs.empty();
        profile.empty();
        breadCrumbs.append(profileBreadCrumnbs);
        profile.append(businessPartnerPropfile(data));
        BUSINESS_PROFILEDETAILS = data;
    }


    function updateBusinesPartnerProfile(argument) {

        let pfDetails = [];

        pfDetails = {
            fname: document.getElementById('txt_partnerFname').value, 
            lname: document.getElementById('txt_partnerLastname').value, 
            mname: document.getElementById('txt_partnerMiddleName').value, 
            address: document.getElementById('txt_partnerAddress').value, 
            email: document.getElementById('txt_partnerEmail').value ,
            contact: document.getElementById('txt_partnerContact').value 
        };

        swal({
          title: "Update Profile?",
          text: "",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Update",
          closeOnConfirm: false
        },
        function(){
            functionURLRequest({id: BUSINESS_DATAID, data: pfDetails}, 'PUT', 'updatePartnerProfile');
            swal('Success!', object.message, 'success');
        });
    }

    function submitUsername(argument) {
        const username = $('#txt_partnerUsername').val();

        if (username !== '') {
            functionURLRequest({id: BUSINESS_DATAID, data: username, profile: BUSINESS_PROFILEDETAILS}, 'PUT', 'addUpdateAccountUsername');
            if (object.data === 1) {
                swal({
                  title: "Update Username?",
                  text: '',
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Update",
                  closeOnConfirm: false
                },
                function(){
                    functionURLRequest({id: BUSINESS_DATAID, data: username}, 'PUT', 'updateAccountUsername');
                    if (object.stat === 0) {
                        swal('Warning!', object.message, 'warning');    
                    }else{
                        swal('Success!', object.message, 'success');    
                    }
                });
            }else if (object.data === 0) {
                swal('Warning!', object.message, 'warning');    
            }else{
                swal('Success!', object.message, 'success');    
            }

        }else{
            $('#txt_partnerUsername').focus();
        }
    }

    function submitPassword(argument) {
        const password = $('#txt_pass').val();
        const retyped = $('#txt_newPass').val();

        if (password == '' || retyped == '') {
            
            if(retyped == ''){
                $('#txt_newPass').focus();
            }

            if (password == '') {
                $('#txt_pass').focus();
            }

            swal('Warning', 'Please fill up empty fields!', 'warning');

        }else{
            
            if (password == retyped) {
                
                functionURLRequest({id: BUSINESS_DATAID, password: password}, 'GET', 'getPassword');    
                
                const {title, message, label} = object;

                if (message == 'Please set username first!') {
                    $('#txt_partnerUsername').focus();
                }

                swal(title, message, label);
                
            }else{

                $('#txt_newPass').val('');
                $('#txt_newPass').focus();

                swal('Warning', 'Password do not match!', 'warning');
            }

            
        }
    }

    function getUsername(argument) {

        functionURLRequest({id: BUSINESS_DATAID}, 'GET', 'getUsername');
        const {username} = object;
        $('#txt_partnerUsername').val(username);

    }

    function propertyListOption(argument) {
        const content = `
             <div class="form-group">
                <label>Property List</label>
                <div class="mt-checkbox-inline" id="proplist_bs">
                    <div class="spiner">
                        <i class="fa fa-spinner"></i>
                    </div>
                </div>
            </div>
        `;  
        return content;
    }

    function updateProperty(argument) {
        const propID = BUSINESS_DATAID;

        bootbox.confirm({
            title: "<strong> Update Property  </strong>",
            message: propertyListOption(),
            size: 'small', 
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                const properyClassname = $('.mt-checkbox')
                const newPropList = [];
                $.each(properyClassname, function (i,n) {
                    const propID = $(this).find('input');

                    if (propID.prop('checked')) {
                        newPropList.push(propID.val());
                    }
                });

                if (result) {
                    functionURLRequest({id: BUSINESS_DATAID, properTy: newPropList}, 'PUT', 'updateProperty');
                    bootbox.hideAll();
                    const {title, message, label} = object;
                    swal(title, message, label);

                    setTimeout(function() {
                        getProfile();
                    }, 100);
                }
            }
        });

        setTimeout(function() {
            getPropertyList();
        }, 10);
    }

    function getProfile(argument) {
        functionURLRequest({id: BUSINESS_DATAID}, 'GET', 'getProfile');
        const {data} = object;
        const profile  = $('#form_businessPartner_div');
              profile.empty();
              profile.append(businessPartnerPropfile(data));
    }

// endclass

}();

function functionURLRequest(setData, setRequest, setRoute) {
    request = setRequest;
    route   = setRoute;
    data    = setData;

    ajaxResquest();   
}


function functionURLRequestForm(setData, setRequest, setRoute) {
    request = setRequest;
    route   = setRoute;
    ajaxResquestForm(setData);   
}

function businessAddForm() {
	return `<form role="form" id="form_businessPartner">
                <div class="form-body">                     
                    <div class="form-group">
                        <label>Full name :</label>
                        <div class="input-icon input-icon-sm">
                            <div class="form-inline">
                            	<input type="text" required class="form-control input-sm fname" placeholder="Firstname" id="txt_fname_bs" name="fname"> 
                            	<input type="text" required class="form-control input-sm" placeholder="Lastname" id="txt_lname_bs" name="lname"> 
                            	<input type="text" required class="form-control input-sm" placeholder="Middlename" id="txt_mname_bs" name="mname"> 
                            </div>
                        </div>
                    </div>
                   
					<div class="form-group">
                        <label>Contact Info :</label>
                        <div class="input-icon input-icon-sm">
                            <div class="form-inline">
                            	<input type="text" class="form-control input-sm" placeholder="Contact #" id="txt_contact_bs" name="contact"> 
                            	<input type="text" class="form-control input-sm" placeholder="Email" id="txt_email_bs" name="email"> 
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Address :</label>
                        <div class="input-icon-sm">
                           <input type="text" required class="form-control input-sm" placeholder="Address" id="txt_address_bs" name="address"> 
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Property List</label>
                        <div class="mt-checkbox-inline" id="proplist_bs">
                            <div class="spiner">
                                <i class="fa fa-spinner"></i>
                            </div>
                        </div>
                    </div>
                </div>
            
                <div class="form-actions text-right" style="margin-bottom: -25px; margin-top: 14px;">
                    <button type="button" class="btn default" id="btn_cancel">Cancel</button>
                    <button type="submit" class="btn blue" >Submit</button>
                </div>

            </form>
        </div>
    </div>`;
}

function businessPartnerPropfile(data) {

    const {address, contact, email, name, property, fname, lname, mname} = data[0];
    const listCount = JSON.parse(property).length;
    PROPERTY_LIST   = property;

    return `<link href="addons/metronic/pages/css/profile-2.min.css" rel="stylesheet" type="text/css" />
                <style>
                    .form-inline{
                        display: inline-flex
                    }
                        
                    .field-inline{
                        display: inline;
                        margin: 3px;
                    }

                    .user-form-agent{
                        height: 165px;
                    }

                    .padding-left-16{
                        padding-left: 16px;
                    }

                    .tab-active > a {
                        color: #169ef4 !important;
                        text-decoration: none !important;
                        background: #ecf5fb !important;
                        border-left: solid 2px #169ef4 !important;
                        font-weight: bold !important;
                    }

                    .label-error{
                        border-bottom: 2px solid #ff0018 !important;
                    }

                </style>

                <!-- BEGIN CONTENT -->
                <div class="profile">
                    <div class="tabbable-line tabbable-full-width">
                        <div class="row">
                            <div class="col-md-3">
                                <form id="agentPrifilePicture" method="" action="">
                                    <ul class="list-unstyled profile-nav">
                                        <li>
                                            <img src="images/no_image.jpg" class="img-responsive pic-bordered" alt="" id="btnfile">
                                            <input type="hidden" class="imageAgentID" name="dataID" value="" />
                                            <input type="hidden" name="MAX_FILE_SIZE" value="10000000000000" />
                                            <input name="file_uploads" accept=".jpg, .png, .jpeg, .gif" type="file" id="file_uploads" style="display: none;">
                                            </img>
                                            <a href="javascript:;" class="profile-edit hidden" style="text-decoration: none"> Click image to select file</a>
                                        </li>

                                        <li>
                                            <button type="submit" class="btn btn-outline blue hidden img-update updateAgentImageProfile" style="width: 100%"><span  class="icon-cloud-upload"> </span> Upload</button>
                                        </li>

                                        <li class="tab-active">
                                            <a href="#tab_1_1" data-toggle="tab" class="active-tab"> Overview </a>
                                        </li>

                                        <li id="tab_updateInfo">
                                            <a href="#tab_1_3" data-toggle="tab" class="active-tab"> Update Information </a>
                                        </li>

                                        <li id="tab_loginSettings">
                                            <a href="#partnerLoginSettings" data-toggle="tab" class="active-tab" id="login_settings"> Login Settings </a>
                                        </li>

                                    </ul>
                                </form>
                            </div>
                            <div class="col-md-9">
                                <div class="tab-content">
                                    <!--start tab_1_1-->
                                    <div class="tab-pane active agentProfileTab" id="tab_1_1">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-md-12 ">
                                                        <!-- <h1 class="font-green sbold uppercase">${name}</h1> -->
                                                        <div class="portlet sale-summary">
                                                            <div class="portlet-title" style="padding: 0px 10px">
                                                                <div class="caption font-green sbold uppercase margin-10px" id="partner_fullname"> ${name} </div>
                                                            </div>
                                                            <div class="portlet-body">
                                                                <ul class="list-unstyled" style="padding: 0px 10px;">
                                                                    <li>
                                                                        <span class="sale-info margin-10px ">Address :
                                                                        <i class="fa fa-img-up"></i>
                                                                    </span>
                                                                        <span class="sale-num margin-10px" id="partner_address"> ${address} </span>
                                                                    </li>
                                                                    <li>
                                                                        <span class="sale-info margin-10px"> Email :
                                                                        <i class="fa fa-img-down"></i>
                                                                    </span>
                                                                        <span class="sale-num margin-10px" id="partner_email"> ${email} </span>
                                                                    </li>
                                                                    <li>
                                                                        <span class="sale-info margin-10px">Contact No : </span>
                                                                        <span class="sale-num margin-10px" id="partner_contactNumber"> ${contact} </span>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="con-lg-offset-8 col-md-offset-8 col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                        <div class="dashboard-stat2 ">
                                                            <div class="display">
                                                                <div class="number">
                                                                    <h3 class="font-green-sharp">
                                                                        <span data-counter="counterup" class="totalProp" data-value="${listCount}">${listCount}</span>
                                                                    </h3>
                                                                    <small>Properties</small>
                                                                </div>
                                                                <div class="icon">
                                                                    <i class="icon-pie-chart"></i>
                                                                </div>
                                                            </div>
                                                            <div class="card-footer">
                                                                <a id="btn_updateProp"> <span class="fa fa-pencil-square-o"></span></a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                    //     <div class="dashboard-stat2 ">
                                                    //         <div class="display">
                                                    //             <div class="number">
                                                    //                 <h3 class="font-blue-sharp">
                                                    //                     <span data-counter="counterup" class="totalCom" data-value="0">₱0.00</span>
                                                    //                 </h3>
                                                    //                 <small> Total Part </small>
                                                    //             </div>
                                                    //             <div class="icon">
                                                    //                 <i class="icon-basket"></i>
                                                    //             </div>
                                                    //         </div>
                                                    //     </div>
                                                    </div> -->

                                                    <!--end col-md-4-->
                                                </div>
                                                <!--end row-->
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <!--   start tab_1_3 -->
                                    <div class="tab-pane" id="tab_1_3">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="portlet box green ">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <strong> <i class="fa fa-id-card"></i> Update Info </strong> </div>
                                                        <div class="tools">
                                                            <!-- <a href="" class="collapse"> </a>
                                                        <a href="" class="reload"> </a> -->
                                                        </div>
                                                    </div>
                                                    <div class="portlet-body form">
                                                        <div class="form updateAgentForm" role="form" id="form_updateBusinessPartnerProfile">
                                                            <div class="form-body">

                                                                <div class="row">
                                                                    <div class="form-group">
                                                                        <div class="col-md-3">
                                                                            <div class="form-group form-md-line-input">
                                                                                <input type="text" class="form-control " id="txt_partnerFname" value="${fname}">
                                                                                <label for="txt_partnerFname">First Name</label>
                                                                                <span class="help-block"></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="form-group form-md-line-input">
                                                                                <input type="text" class="form-control" id="txt_partnerLastname" value="${lname}">
                                                                                <label for="txt_partnerLastname">Last Name</label>
                                                                                <span class="help-block"></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="form-group form-md-line-input">
                                                                                <input type="text" class="form-control " id="txt_partnerMiddleName" value="${mname}">
                                                                                <label for="txt_partnerMiddleName">Middle Name</label>
                                                                                <span class="help-block"></span>
                                                                            </div>
                                                                        </div>
                                                                         
                                                                    </div>
                                                                </div>
                                                                 
                                                                <div class="row">
                                                                    <div class="form-group">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group form-md-line-input">
                                                                                <input type="text" class="form-control" id="txt_partnerAddress" value="${address}">
                                                                                <label for="txt_partnerAddress">Address: </label>
                                                                                <span class="help-block"></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="form-group form-md-line-input">
                                                                                <input type="text" class="form-control" id="txt_partnerEmail" value="${email}">
                                                                                <label for="txt_partnerEmail">Email: </label>
                                                                                <span class="help-block"></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="form-group form-md-line-input">
                                                                                <input type="text" class="form-control" id="txt_partnerContact" value="${contact}">
                                                                                <label for="txt_partnerContact">Contact No:</label>
                                                                                <span class="help-block"></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="form-actions text-right">
                                                                <button type="button" data-id="" class="btn btn-outline green hidden" id="btn_partnerUpdateDetails"><i class="fa fa-paper-plane" aria-hidden="true"></i> Update </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane" id="partnerLoginSettings">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="portlet box green ">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <strong><i class="fa fa-lock"></i> Login Settings </strong> </div>
                                                        <div class="tools">
                                                            <!-- <a href="" class="collapse"> </a>
                                                        <a href="" class="reload"> </a> -->
                                                        </div>
                                                    </div>
                                                    <div class="portlet-body form">
                                                        <div class="form updateAgentForm" role="form">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-body">
                                                                        <div class="row">
                                                                            <h4> <strong class="padding-left-16"> Username </strong> </h4>
                                                                            <div class="form-group user-form-agent">
                                                                                <div class="col-md-offset-1 col-md-10">
                                                                                    <div class="form-group form-md-line-input ">
                                                                                        <input type="text" class="form-control" id="txt_partnerUsername">
                                                                                        <label for="txt_partnerUsername">Username</label>
                                                                                        <span class="help-block"></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions text-right">
                                                                        <button type="button" data-id="" class="btn btn-outline green" id="btn_submit_partUsername"><i class="fa fa-paper-plane" aria-hidden="true"></i> Submit  </button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-body">
                                                                        <div class="row">
                                                                            <h4> <strong> Password </strong> </h4>
                                                                            <div class="form-group">
                                                                                <div class="col-md-offset-1 col-md-10">
                                                                                    <div class="form-group form-md-line-input">
                                                                                        <input type="password" class="form-control" id="txt_pass">
                                                                                        <label for="txt_pass">Password </label>
                                                                                        <span class="help-block"></span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-offset-1 col-md-10">
                                                                                    <div class="form-group form-md-line-input">
                                                                                        <input type="password" class="form-control" id="txt_newPass">
                                                                                        <label for="txt_newPass">Re-type password</label>
                                                                                        <span class="help-block"></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions text-right">
                                                                        <button type="button" data-id="" class="btn btn-outline blue" id="btn_set_passwort_partner"><i class="fa fa-key" aria-hidden="true"></i> Submit </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
                 
                <script>
                    $(document).ready(function() {

                        $("#btnfile").click(function() {
                            $("#file_uploads").click();
                        });

                        function readURL(input) {
                            if (input.files && input.files[0]) {
                                var reader = new FileReader();

                                reader.onload = function(e) {
                                    $('#btnfile').attr('src', e.target.result);
                                }

                                reader.readAsDataURL(input.files[0]);
                            }

                            var x = document.getElementById("file_uploads").value;

                            var path = x;
                            var pos = path.lastIndexOf(path.charAt(path.indexOf(":") + 1));
                            filename = path.substring(pos + 1);
                            document.getElementById("imgName").value = filename;
                        }

                        $("#file_uploads").change(function() {
                            readURL(this);
                        });

                        $(".active-tab").click(function() {
                            $('.active-tab').parent().removeClass('tab-active');
                            $(this).parent().addClass('tab-active');

                            if(this.text.trim() == "Update Information"){
                                $('.updateAgentImageProfile').removeClass('hidden');
                                $('.profile-edit').removeClass('hidden');
                            }else{
                                $('.updateAgentImageProfile').addClass('hidden');
                                $('.profile-edit').addClass('hidden');
                            }
                        });
                    });
                </script>`;
}

function getTemplate(baseTemplateUrl, templateName, callback) {
    if (!templates[templateName]) {
        $.get(baseTemplateUrl + templateName, function(resp) {
            compiled = _.template(resp);
            templates[templateName] = compiled;
            if (_.isFunction(callback)) {
                callback(compiled);
            }
        }, 'html');
    } else {
        callback(templates[templateName]);
    }
}


function template(navParent) {
	// setTimeout(function() {
		getTemplate(baseTemplateUrl, 'businessPartner.html', function(render) {
	        $( ".dashboardPage" ).empty();
	        var renderedhtml = render({data: ""});
	        $(".clientListContainer").html(renderedhtml);
	        $('.clientListContainer').removeClass('hidden');

	        $('.page-sidebar-menu').find('.nav-item').removeClass('active open');

	        if(navParent.parent().hasClass('nav-item')){
	            navParent.parent().addClass('active open');
	        }

	        if(navParent.parent().parent().parent().hasClass('nav-item')){
	            navParent.parent().parent().parent().addClass('active open');
	        }

	     });
	// }, 100);
}

function ajaxResquest(){
    $.ajax({
        type : request,
          url: apiUrl + "/dashboard/partner/"+ route,
        data : data,
        cache: false,
        async: false,
        success: function(response) {
            if(response.success){
              returnResponse(response);
            }
            else{
              console.log('woo')
              swal("Warning!", response.message, "warning");
            }
        },
          error: function(err){
             swal("Error!", err.statusText, "error");
          }
    });     
}

function ajaxResquestForm(formdata){
 	$.ajax({
	    type : request,
	      url: apiUrl + "/dashboard/partner/"+ route,
	    data : formdata,
	    cache: false,
	    async: true,
        processData: false,
        contentType: false,
	    success: function(response) {
	        if(response.success){
	           returnResponse(response);
	        }
	        else{
	          swal("Warning!", response.message, "warning");
	        }
	    },
	      error: function(err){
	         swal("Error!", err.statusText, "error");
	      }
	});  
}




function returnResponse(response){
    return object = response;
}

jQuery(document).ready(function() {    
  BusinessPartner.init();
});

