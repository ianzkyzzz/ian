var Client = function () {
    var templates = [],
            baseTemplateUrl = 'template/client/';
    var selectedProperties = [];
    var set_clientID ="";
    var set_spouseList = [];
    var set_beneficiaries = [];
    var set_btnVal = 1;


    function getTemplate(baseTemplateUrl, templateName, callback) {
        if (!templates[templateName]) {
            $.get(baseTemplateUrl + templateName, function(resp) {
                compiled = _.template(resp);
                templates[templateName] = compiled;
                if (_.isFunction(callback)) {
                    callback(compiled);
                }
            }, 'html');
        } else {
            callback(templates[templateName]);
        }
    }

 

    var clientPropertyPaymentHistoryTable;
    function tableclientPropertyPaymentHistoryTable(){
        var table = $('.clientPropertyPaymentHistoryTable');
        clientPropertyPaymentHistoryTable =  table.dataTable({
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No Payment History Yet",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            // bootstrap_full_number
            "pagingType": "bootstrap_extended", 
            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, 50,100],
                [5, 10, 15, 20, 50,100] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            destroy : true
        });


        var table = $('.clientPropertyPaymentHistoryTable').DataTable();
        table.on( 'order.dt search.dt', function () {
            table.column(4, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML =  accounting.format(cell.innerHTML,2);
            } );
            table.column(5, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = accounting.format(cell.innerHTML,2);
            } );
            table.column(6, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = accounting.format(cell.innerHTML,2);
            } );
            // table.column(6, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            //     cell.innerHTML = accounting.format(cell.innerHTML,2);
            // } );
        } ).draw();

    }

 
    var clientListTable;
    function tableclientListTable(){
        var table = $('.clientListTable');
        clientListTable =  table.dataTable({
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No employees added yet",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            // bootstrap_full_number
            "pagingType": "bootstrap_extended", 
            "order": [
                [0, 'desc']
            ],
            
            "lengthMenu": [
                [25, 35, 40, 70,100, 200],
                [25, 35, 40, 70,100, 200] // change per page values here
            ],
            // set the initial value
            "pageLength": 100,
            destroy : true
        });

    }


    function clienUpdateFormValidation(){
        var form1 = $('.updateClientForm2');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);


        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            messages: {
                payment: {
                    maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                    minlength: jQuery.validator.format("At least {0} items must be selected")
                },
                'checkboxes1[]': {
                    required: 'Please check some options',
                    minlength: jQuery.validator.format("At least {0} items must be selected"),
                },
                'checkboxes2[]': {
                    required: 'Please check some options',
                    minlength: jQuery.validator.format("At least {0} items must be selected"),
                }
            },
            rules: {
                update_clientFname: {
                    required: true 
                },
                update_clientLname: {
                    required: true
                },
                update_clientAddress: {
                    required: true
                },
                update_clientContactNumber: {
                    required: true
                },
                update_clientEmail: {
                    email: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            errorPlacement: function(error, element) {
                if (element.is(':checkbox')) {
                    error.insertAfter(element.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline"));
                } else if (element.is(':radio')) {
                    error.insertAfter(element.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function(form) {
                success1.show();
                error1.hide();

                confirmWithSwal(function(){
                    btnUpdateClientClicked($('.btnUpdateClient').attr('data-id'));
                  },
                  'Are you sure to submit this update?',
                  'warning');

                return false;
            }
        });

    }


    function clientWizardValidator() {
      if (!jQuery().bootstrapWizard) {
          return;
      }

      function format(state) {
          if (!state.id) return state.text; // optgroup
          return "<img class='flag' src='../../assets/global/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
      }

      $("#country_list").select2({
          placeholder: "Select",
          allowClear: true,
          formatResult: format,
          width: 'auto', 
          formatSelection: format,
          escapeMarkup: function (m) {
              return m;
          }
      });

      var form = $('#submit_form');
      var error = $('.alert-danger', form);
      var success = $('.alert-success', form);

      form.validate({
          doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
          errorElement: 'span', //default input error message container
          errorClass: 'help-block help-block-error', // default input error message class
          focusInvalid: false, // do not focus the last invalid input
          rules: {
              addClient_fname: {
                maxlength: 20,
                required: true
              },
              addClient_lname: {
                maxlength: 20,
                required: true
              },
              addClient_email: {
                email: true
              },
              addClient_contactnumber: {
                  required: true
              },
              addClient_address: {
                  required: true
              },
              addClientprop_property: {
                  required: true
              },
              //payment
              addClientprop_dateapplied: {
                  required: true,
                  date : true
              },
              
              salesDirectorAgentCommission: {
                  number: true
              },
              salesManagerAgentCommission: {
                  number: true
              },
              managerAgentCommission: {
                  number: true
              },
              unitManagerAgentCommission: {
                  number: true
              },
              addClientprop_pricePerm2: {
                  number: true,
                  required: true
              },
              addClientprop_planterms: {
                  digits: true,
                  required: true
              },
              addClientagent: {
                  required: true
              },
              commissonReleasedSched: {
                  required: true
              }
          },
          

          messages: { // custom messages for radio buttons and checkboxes
              'payment[]': {
                  required: "Please select at least one option",
                  minlength: jQuery.validator.format("Please select at least one option")
              }
          },

          errorPlacement: function (error, element) { // render error placement for each input type
              if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                  error.insertAfter("#form_gender_error");
              } else if (element.attr("name") == "payment[]") { // for uniform checkboxes, insert the after the given container
                  error.insertAfter("#form_payment_error");
              } else {
                  error.insertAfter(element); // for other inputs, just perform default behavior
              }
          },

          invalidHandler: function (event, validator) { //display error alert on form submit   
              success.hide();
              error.show();
              App.scrollTo(error, -200);
          },

          highlight: function (element) { // hightlight error inputs
              $(element)
                  .closest('.form-group').removeClass('has-success').addClass('has-error'); // set error class to the control group
          },

          unhighlight: function (element) { // revert the change done by hightlight
              $(element)
                  .closest('.form-group').removeClass('has-error'); // set error class to the control group
          },

          success: function (label) {
              if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radio buttons, no need to show OK icon
                  label
                      .closest('.form-group').removeClass('has-error').addClass('has-success');
                  label.remove(); // remove error label here
              } else { // display success icon for other inputs
                  label
                      .addClass('valid') // mark the current input as valid and display OK icon
                  .closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
              }
          },

          submitHandler: function (form) {
              success.show();
              error.hide();

              form[0].submit();
              //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
          }

      });

      var displayConfirm = function() {
          $('#tab3 .form-control-static', form).each(function(){
              var input = $('[name="'+$(this).attr("data-display")+'"]', form);
              if (input.is(":radio")) {
                  input = $('[name="'+$(this).attr("data-display")+'"]:checked', form);
              }
              if (input.is(":text") || input.is("textarea")) {
                  $(this).html(input.val());
              } else if (input.is("select")) {
                  $(this).html(input.find('option:selected').text());
              } else if (input.is(":radio") && input.is(":checked")) {
                  $(this).html(input.attr("data-title"));
              } else if ($(this).attr("data-display") == 'payment[]') {
                  var payment = [];
                  $('[name="payment[]"]:checked', form).each(function(){ 
                      payment.push($(this).attr('data-title'));
                  });
                  $(this).html(payment.join("<br>"));
              }
          });
      }

      var handleTitle = function(tab, navigation, index) {
          var total = navigation.find('li').length;
          var current = index + 1;
          // set wizard title
          $('.step-title', $('#form_wizard_1')).text('Step ' + (index + 1) + ' of ' + total);
          // set done steps
          jQuery('li', $('#form_wizard_1')).removeClass("done");
          var li_list = navigation.find('li');
          for (var i = 0; i < index; i++) {
              jQuery(li_list[i]).addClass("done");
          }

          if (current == 1) {
              $('#form_wizard_1').find('.button-previous').hide();
          } else {
              $('#form_wizard_1').find('.button-previous').show();
          }

          if (current >= total) {
              $('#form_wizard_1').find('.button-next').hide();
              $('#form_wizard_1').find('.button-submit').show();
              displayConfirm();
          } else {
              $('#form_wizard_1').find('.button-next').show();
              $('#form_wizard_1').find('.button-submit').hide();
          }
          App.scrollTo($('.page-title'));
      }



      // default form wizard
      $('#form_wizard_1').bootstrapWizard({
          'nextSelector': '.button-next',
          'previousSelector': '.button-previous',
          onTabClick: function (tab, navigation, index, clickedIndex) {
              // return false;
              success.hide();
              error.hide();
              return false;
              
          },
          onNext: function (tab, navigation, index) {
              success.hide();
              error.hide();
              if(index == 2){
                if(validateAllInputFeildsForPropertiesHasError() || form.valid() == false){   // if has error
                  return false;
                }
                else{
                  $('.getClientName').html($('.addClient_fname').val() + ' ' +  $('.addClient_mname').val() + ' ' + $('.addClient_lname').val());
                  chargeValueChange(); 
                }
              }
              else if(index == 3){
                if(chargeValueChange() || form.valid() == false){   // if has error
                  swal('Warning!','There missing/invalid values.','warning');
                  return false;
                }else{
                  $('.addClient_summary_cleintName').val($('.addClient_fname').val() + ' ' + $('.addClient_Mname').val() + ' ' + $('.addClient_lname').val());
                }
              }
              else if(index == 4){
                // alert( $('.directAgentCommission').val() + ', ' + $('.unitManagerAgentCommission').val() + ', ' + $('.salesManagerAgentCommission').val() + ', ' + $('.salesDirectorAgentCommission').val());
                
                if(form.valid() == false){   // if has error
                  swal('Warning!','There missing/invalid values.','warning');
                  return false;
                }
                else if(calculatePercentageIsEqualTo10Validation(true)){
                  return false;
                }
               
                initalizeAddClientSummary();

              }else{
                if (form.valid() == false) {
                    return false;
                } 
              }


              handleTitle(tab, navigation, index);
          },
          onPrevious: function (tab, navigation, index) {
              success.hide();
              error.hide();

              handleTitle(tab, navigation, index);
          },
          onTabShow: function (tab, navigation, index) {
              var total = navigation.find('li').length;
              var current = index + 1;
              var $percent = (current / total) * 100;
              $('#form_wizard_1').find('.progress-bar').css({
                  width: $percent + '%'
              });
          }
      });

      $('#form_wizard_1').find('.button-previous').hide();
      $('#form_wizard_1 .button-submit').click(function () {
          success.hide();
          error.hide();
          
          if (validateAllInputFeildsForPropertiesHasError() || chargeValueChange() || calculatePercentageIsEqualTo10Validation(true) || form.valid() == false){
            return false;
          }
          else{


            initalizeAddClientSummary();
            confirmWithSwal(function(){postThisClient()},'Are you sure to submit this application?','warning');
          }
      }).hide();

      //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
      $('.addClientprop_property , .addClientagent', form).change(function () {
          form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
      });
    }


    return {
        init: init
    };

    // Init
    function init() {
        addEventHandlers();
        loadInitialization();
    }

    function loadInitialization(){
      

    }





    function addEventHandlers(){
        $(document).on('click', '.openAddClientForm', openAddClientContainer);
        $(document).on('click', '.openViewAllClientContainer ,.returnToClientList', openClientListContainer);
        $(document).on('change', '.addClientagent', clientagentListValueChange);
        $(document).on('click', '.btn_viewThisClient , .btnReturnToClientProfile ,.addMoreClientPropertyClientName', openClientProfileOfthis);

        $(document).on('click','.clientSummaryPrintThis',printContent);

        $(document).on('click', '.updateClient-infoTab', updateProfileTabIsClicked);
        $(document).on('click', '.thisClientPropertiesTab', thisClientPropertiesTabIsClicked);
        $(document).on('click', '.btn_viewLots', getViewLotDetails);

        $(document).on('change', '.clientPropertiesList ', thisClientPropertiesTabIsClicked);
       
        $(document).on('submit','#clientProfilePictureForm',uploadThisImage);
        // $(document).on('change','#clientProfilePictureForm input[type=file]', prepareUpload);

        $(document).on('click', '.btnfile', updateProfilePicChange);
        $(document).on('click', '.tab_active_1', ifoverView);
        $(document).on('click', '.tab_active_2', ifupdateInfo);
        $(document).on('click', '.tab_active_3', ifchangePass);
        $(document).on('change', '#file_uploads', updateProfilePicChange);

        $(document).on('click', '#btn-print', printPayments);

        $(document).on('click', '.cms-btn-view', viewPropertyList);
        $(document).on('click', '.clientFormblocksListElement', clientFormblocksListChange);
        $(document).on('click', '.clientFormLotListCheckBoxLi', clientFormLotListCheckBoxLiIsClicked);
        

        $(document).on('change', '.addClientprop_property', addClientprop_propertyIsChange);
        
        $(document).on('click', '.btnCancelThisSelectedClientProperty', btnCancelThisSelectedClientPropertyIsClicked);
        

        $(document).on('keyup', '.directAgentCommission, .salesDirectorAgentCommission ,.salesManagerAgentCommission ,.unitManagerAgentCommission', calculatePercentageIsEqualTo10Validation);
        $(document).on('keyup', '.addClientprop_pricePerm2  ,.addClientprop_planterms', generateContactPriceAndMonthlyAmortization ); 



        $(document).on('keyup', '.addClientprop_downpayment', addClientprop_downpaymentFocusOut);
        $(document).on('change', '.clientmodeOfPayment', clientmodeOfPaymentChange);
        
        // new code 
        $(document).on('focusout', '.chargeValue ,.chargeDescription', chargeValueChange);
        $(document).on('click', '.cms-addo-charges', additionalCharges);
        $(document).on('click', '.cms-redo-charges', function(){
          $(this).parent().parent().parent().remove();
          chargeValueChange();
        });


        $(document).on('click', '.btnAddMoreProperty', btnAddMorePropertySiClicked);
        $(document).on('click', '.addmoreProp_tab2', addmoreProp_tab2IsClicked);
        $(document).on('click', '.addmoreProp_tab3', addmoreProp_tab3IsClicked);
        $(document).on('click', '.addmoreProp_tab4', addmoreProp_tab4IsClicked);

        $(document).on('click', '.btnSumbitThisAdditionalProperty', btnSumbitThisAdditionalPropertyIsClicked);

        $(document).on('change', '.commissonReleasedSched', showReleaseSched);

        $(document).on('click', '.btnComRelSched', updateComRelShecdule);

        $(document).on('click', '.btnAddCom', updayAddCom);

        $(document).on('click', '.btnaddCharges', updateAddCharges);
        
        $(document).on('click', '.cms-addo-charges', countClick);
        $(document).on('click', '.cms-redo-charges', countClickback);

        $(document).on('click', '.btnForfietThisProperty', forfietPropertyStatus);
        

        $(document).on('click', '.btnChangeAgentCom', loadAgentCom);
        $(document).on('click', '.btnUpdateAgentCom', updateAgentCom);
        $(document).on('click', '.btnchangeDownpayment', getDownpayment);
        $(document).on('click', '.btnUpdateDownpayment', updateDownpayment);
        $(document).on('click', '.btnChangeProperty', changeProperty);
        $(document).on('click', '#btn_printPropInfo', printPropInfo);

        // spouse
        $(document).on('click', '#btn_addSpouse', addSpouse);
        $(document).on('click', '#spouse_removeThisSpouse', RemoveSpouse);
        $(document).on('click', '#btn_newSpouse', AddNewSpouse);
        $(document).on('click', '.spouce_file', update_thisSpouse);
        $(document).on('click', '.rbtn_sp_update',function (argument) {
            set_btnVal = $(this).attr('data-id'); 
        });

        // beneficiary
        $(document).on('click', '#btn_addBenef', AddBeneficiary);
        $(document).on('click', '#btn_RemoveBenef', RemoveBeneficiary);
        $(document).on('click', '#btn_newBenif', new_beneficiary);
        $(document).on('click', '.benif_a', update_benef);

        $(document).on('change', '#slt_propertyParentList', get_ClientOfTheSelectedProperty);
        $(document).on('change', '#slt_propertyList', get_ClientByFromTheSelectedBl);
        $(document).on('click', '#loadDatePicker', function () {
            $('.chargeDate').datepicker({
              format: "yyyy-mm-dd"
            });
        });

        $(document).on('click','#backtoList',openClientListContainer);


    }

    function RemoveSpouse(argument) {
      $(this).parent().parent().remove();
    }

    function forfietPropertyStatus(){
      confirmWithSwal(function(){forfeitProperty()},'Are you sure you want to forfeit this property?','warning');
    }


    function forfeitProperty(){
       var clientId = $('.imageClientID').val();
       var cp_id    = $('.clientPropertiesList').val();

        $.ajax({
            type: 'POST',
            url: apiUrl+"/dashboard/client/forfeitSelectedProperty",
            data : {id : clientId, cp_id: cp_id},
            cache: false,
            async: true,
            success: function(response) { 
                console.log(response);
                if(response.success){
                  
                   $('.propertyStatus').html('Property Forfeited');
                   $('.btnComRelSched').attr("Disabled","btnComRelSched");
                   $('.btnComRelSched').removeClass('btnComRelSched');
                   $('.btnAddCom').attr("Disabled","btnAddCom");
                   $('.btnAddCom').removeClass('btnAddCom');
                   $('.btnaddCharges').attr("Disabled","btnaddCharges");
                   $('.btnaddCharges').removeClass('btnaddCharges');
                   $('.btnForfietThisProperty').attr("Disabled","btnForfietThisProperty");
                   $('.btnForfietThisProperty').removeClass('btnForfietThisProperty');

                  swal("Success!", response.message, "success");
                  thisClientPropertiesTabIsClicked();
                }
                else{
                  hideLoading();
                  swal("Warning!", response.message, "warning");
                }
            },
            error: function(err){
              hideLoading();
              swal("Error!", err.statusText, "error");
            }
        });

    }


    var countClickCharges = 0;
    function countClick() {
       countClickCharges = 1;
       // alert(countClickCharges);
    }

    function countClickback() {
      if(countClickCharges == 0){
      }else{
        countClickCharges -=1 ;
      }
       // alert(countClickCharges);
    }


    function updateAddCharges() {

       const countCharge = $('.additionalChargesDetails').length;
       if (countCharge === 0) {
         // return false;
         alert('Add additional charges before you update!');
       }else{

        swal({
          title: "Are you sure?",
          text: "You want to udate this charges!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes!",
          cancelButtonText: "No, cancel plx!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm) {
          if (isConfirm) {
            upadateAddRemoveCharges();   
          } else {
            swal.close();
          }
        });

       }
    }

    function upadateAddRemoveCharges() {
      chargeValueChange();

      var clientId = $('.imageClientID').val();
      var cp_id    = $('.clientPropertiesList').val();

      $.ajax({
          type: 'POST',
          url: apiUrl+"/dashboard/client/updatedAdditionalCharges",
          data : {id : clientId, cp_id: cp_id, chargesDetails, chargesDetails},
          cache: false,
          async: true,
          success: function(response) { 
              // console.log(response);
              if(response.success){

                swal("Success!", response.message, "success");
                thisClientPropertiesTabIsClicked();
              }
              else{
                hideLoading();
                swal("Warning!", response.message, "warning");
              }
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
       });

    }

    function updayAddCom(){

      if ($('.updateAddCom').val() == "") {
         alert('Warning! add com field is empty.');
      }else{
        updateAddCommission();
      }

    }


   function updateAddCommission(){
    var comSched = $('.updateAddCom').val();
    var clientId = $('.imageClientID').val();
    var cp_id    = $('.clientPropertiesList').val();

    $.ajax({
        type: 'POST',
        url: apiUrl+"/dashboard/client/updateAddCommission",
        data : {comSched: comSched, id : clientId, cp_id: cp_id},
        cache: false,
        async: true,
        success: function(response) { 
            console.log(response);
            if(response.success){
                
              swal("Success!", response.message, "success");
              thisClientPropertiesTabIsClicked();
            }
            else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }
        },
        error: function(err){
          hideLoading();
          swal("Error!", err.statusText, "error");
        }
    });

   } 

    function updateComRelShecdule(){
      if($('.updateComRelSched').val() == ""){
         alert('Empty ComRelease Schedule');
      }else{
        updateComSched();
      }
    }

   function updateComSched(){
    var comSched = $('.updateComRelSched').val();
    var clientId = $('.imageClientID').val();
    var cp_id    = $('.clientPropertiesList').val();

    $.ajax({
          type: 'POST',
          url: apiUrl+"/dashboard/client/updateComRelShecdule",
          data : {comSched: comSched, id : clientId, cp_id: cp_id},
          cache: false,
          async: true,
          success: function(response) { 
              console.log(response);
              if(response.success){
                  
                swal("Success!", response.message, "success");
                thisClientPropertiesTabIsClicked();
              }
              else{
                hideLoading();
                swal("Warning!", response.message, "warning");
              }
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });

   }


    function showReleaseSched(){
      //alert($(this).val());
      if($(this).val() == 0){
      $(this).parent().parent().parent().find('.schedInput').removeClass('hidden');
      }else{
        $(this).parent().parent().parent().find('.schedInput').addClass('hidden');
      }
    }


    function btnSumbitThisAdditionalPropertyIsClicked(){
      var clientID = $(this).attr('data-id');

      var propertySelected = [];
      $.each($('.selectecPropertiesContainer .selectedProperty'),function(){
        var parentContainer = $(this);
        var parentID = parentContainer.attr('data-parentID');
        var block = parentContainer.attr('data-block');
        var lot = parentContainer.attr('data-lot');
        var phaseNumber = parentContainer.attr('data-phaseNumber');

        var ornumber                             = parentContainer.find('.addClientprop_ornumber').val().trim();
        var modeOfpayment                        = parentContainer.find('.clientmodeOfPayment').val();
        var clientremitanceCentername            = parentContainer.find('.clientremitanceCentername').val();

        var addClientprop_pricePerm2             = parentContainer.find('.addClientprop_pricePerm2').val();
        var addClientprop_property               = parentContainer.find('.addClientprop_property').val();
        var addClientprop_downpayment            = accounting.unformat(parentContainer.find('.addClientprop_downpayment').val().trim());
        var addClientprop_dateapplied            = parentContainer.find('.addClientprop_dateapplied').val().trim();
        var addClientDueDate                     = parentContainer.find('.addClientDueDate').val();
        var addClientprop_planterms              = accounting.unformat(parentContainer.find('.addClientprop_planterms').val().trim());

          var commissionRelease                    
        if(parentContainer.find('.commissonReleasedSched').val().trim() == 0){
              commissionRelease     = parentContainer.find('.commissonReleasedSchedInput').val().trim();
        }else{
              commissionRelease     = parentContainer.find('.commissonReleasedSched').val().trim();
        }
        var addCom                  = parentContainer.find('.addComission').val().trim();
        if (addCom === '') {
          addCom = 0;
        }
 
        propertySelected.push({
          parentID                            : parentID,
          ornumber                            : ornumber,
          phaseNumber                         : phaseNumber,
          block                               : block,
          lot                                 : lot,
          modeOfpayment                       : modeOfpayment,
          clientremitanceCentername           : clientremitanceCentername,
          addClientprop_property              : addClientprop_property,
          addClientprop_pricePerm2            : addClientprop_pricePerm2,
          addClientprop_downpayment           : addClientprop_downpayment,
          addClientprop_dateapplied           : addClientprop_dateapplied,
          addClientDueDate                    : addClientDueDate,
          addClientprop_planterms             : addClientprop_planterms,
          comission_release                   : commissionRelease,
          add_commission                      : addCom

        });

      });


      var directCom         = $('.directAgentCommission').val();
      var UnitManagerCom    = $('.unitManagerAgentCommission').val();
      var SalesManagerCom   = $('.salesManagerAgentCommission').val();
      var SalesDirectorCom  = $('.salesDirectorAgentCommission').val();
      var managerCom        = $('.managerAgentCommission').val();

      

      var addClientagent                    = $('.addClientagent').val().trim();

      var data = {
          clientID                            : clientID,

          propertySelected                    :propertySelected,
          chargesDetails                      :chargesDetails,
          addClientagent                      : addClientagent,
          commissionPercentage                :{
                                        directCom         : directCom,
                                        UnitManagerCom    : UnitManagerCom,
                                        SalesManagerCom   : SalesManagerCom,
                                        SalesDirectorCom  : SalesDirectorCom,
                                        ManagerCom        : managerCom  
                                    }  
        };
        
        showLoading();
        $.ajax({
          type: 'POST',
          url: apiUrl+"/dashboard/client/addThisMoreProperty",
          data : data,
          cache: false,
          async: true,
          success: function(response) { 
              console.log(response);

              if(response.success){

                $('.addClient_summary_date').html(response.dateNow);
                var content = document.getElementById('printReceipt').innerHTML;

                bootbox.dialog({
                  message: content,
                  title: "Property Summary",
                  // onEscape: function() {  },
                  // backdrop: true,
                  buttons: {
                    success: {
                      label: "Print",
                      className: "green btn-outline",
                      callback: function() {
                        var html="<html>";
                        // html+="<head><style>@page {size: auto;margin: 0cm; padding: 0; width: 100%; height: 100%; background-color: white;}@media print {#officialReceipt { page-break-after: always; width: 100%; margin: 0; padding: 0; background-color: white;} body , body > *{width: 100%; height:100%; margin: 0; padding: 0; background-color: white;}}</style></head>";
                        html+= content;
                        html+="</html>";
                        var printWin = window.open();
                        printWin.document.write(html);
                        printWin.document.close();
                        printWin.focus();
                        printWin.print();
                        printWin.close();
                      }
                    }
                  }
                });

                openProfile(clientID);
                window.clearTimeout(loadInBackground);
              }
              else{
                hideLoading();
                swal("Warning!", response.message, "warning");
                $.each(response.variables,function(i,n){
                  $('.'+n.key).parent().addClass('has-error');
                  $('.'+n.key).parent().find('.help-block').html(n.errorMessage)
                });
                window.clearTimeout(loadInBackground);
              }
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
            window.clearTimeout(loadInBackground);
          }
      });
    }



    function addmoreProp_tab2IsClicked(){

      if(validateAllInputFeildsForPropertiesHasError()){
        $('.addmoreProp_tab1').click();
      }
      else
        chargeValueChange()


    }


    function addmoreProp_tab3IsClicked(){

      if(validateAllInputFeildsForPropertiesHasError()){
        $('.addmoreProp_tab1').click();
      }
      else if(chargeValueChange()){   // if has error
        swal('Warning!','There missing/invalid values.','warning');
        $('.addmoreProp_tab2').click();
      }


    }


    function addmoreProp_tab4IsClicked(){

      if(validateAllInputFeildsForPropertiesHasError()){
        $('.addmoreProp_tab1').click();
      }
      else if(chargeValueChange()){   // if has error
        swal('Warning!','There missing/invalid values.','warning');
        $('.addmoreProp_tab2').click();
      }
      else if($('.addClientagent').val().trim() == ""){
        swal('Warning!','Select agent first.','warning');
        $('.addmoreProp_tab3').click();
      }
      else if(calculatePercentageIsEqualTo10Validation(true)){
        // swal('Warning!','There missing/invalid values.','warning');
        $('.addmoreProp_tab3').click();
      }

      initalizeAddClientSummary();

    }






    function btnAddMorePropertySiClicked(){
      showLoading();
      var clientID = $(this).attr('data-clientID');
      var clientFullName = $(this).attr('data-clientFullName');
      
      getTemplate(baseTemplateUrl, 'addMoreProperty.html', function(render) {
          $( ".dashboardPage" ).empty();
          var renderedhtml = render({data: ""});
          $(".addMoreAdditionalPropertyContainer").html(renderedhtml);
          $('.addMoreAdditionalPropertyContainer').removeClass('hidden');

          getAllPropertiesAndAgents();

          $('.btnReturnToClientProfile ,.btnSumbitThisAdditionalProperty ,.addMoreClientPropertyClientName').attr('data-id',clientID);
          
          $('.addMoreClientPropertyClientName').html(clientFullName); 
      });
    }


    function addClientprop_downpaymentFocusOut(){
      var value = $(this).val().trim();
      if(value != ""){
        if( value != '-1' && validateNumberAndDot(value)){
          $(this).parent().parent().parent().find('.clientmodeOfPaymentContainer ,.clientornumberContainer').removeClass('hidden');
            getOrNumber();
            var TCP=$('.addClientprop_originalprice').val();
            var planTerms=$('.addClientprop_planterms').val();
            var calculate=((parseFloat(TCP.replace(',','')) - parseFloat(value)) / parseFloat(planTerms));
            $('.addClientprop_monthlyamortization').val(accounting.format(calculate,2));
        }
        else{
          $(this).parent().parent().parent().find('.clientmodeOfPaymentContainer ,.clientornumberContainer').addClass('hidden');
        }
      }
      else{
        $(this).parent().parent().parent().find('.clientmodeOfPaymentContainer ,.clientornumberContainer').addClass('hidden');
      }


    }



    function clientmodeOfPaymentChange(){
      var value = $(this).val();
      var text  = $(this).find('option:selected').text()
      if(value == 2 || value == 3){
        $(this).parent().parent().parent().find('.clientremitanceCenterContainer').removeClass('hidden');
        $(this).parent().parent().parent().find('.clientremitanceCenterContainer').find('label').html(text + " name");
      }
      else{
        $(this).parent().parent().parent().find('.clientremitanceCenterContainer').addClass('hidden');
      }

      $(this).parent().parent().parent().find('.clientornumberContainer').removeClass('hidden');
    }


    var totalCharges = 0;
    var chargesDetails = [];
    var chargeDate = '';
    function chargeValueChange(){

      totalCharges = 0;
      chargesDetails = [];
      var hasError = false;
      
      $.each($('.additionalChargesDetails'),function(i,n){
        
        var chargeValue = $(this).find('.chargeValue');
        var description = $(this).find('.chargeDescription');
        var chargeDate  = $(this).find('.chargeDate');

        if(chargeValue.val().trim() != "" && typeof chargeValue.val().trim() != 'undefined' && validateNumberAndDot(chargeValue.val().trim())){
          
          totalCharges += parseFloat(chargeValue.val().trim());

          chargeValue.parent().removeClass('has-error');
          chargesDetails.push({
            index : i,
            chargeValue : chargeValue.val(),
            chargeDescription : description.val().toUpperCase(),
            chargeDate : chargeDate.val()
          });
        }
        else{
          hasError = true;
          chargeValue.parent().addClass('has-error');
        }

        if(description.val().trim() == ""){
          hasError = true;
          description.parent().addClass('has-error');
        }
        else{
          description.parent().removeClass('has-error');
        }
      });


      $('.totalCharges').html(accounting.format(totalCharges,2));
      $('.totalCharges').attr('data-totalCharges',totalCharges);

      updateTotalChargesPerProperty(totalCharges);

      return hasError;
    }


    function updateTotalChargesPerProperty(totalCharges){
      var propertyName = $('.addClientprop_property option:selected').text();
      $('.listOfPropertySelectedForChargesOutputContainer').empty();
      $.each($('.selectedProperty'),function(){
        var phaseNumber = $(this).attr('data-phasenumber');
        var parentID = $(this).attr('data-parentid');
        var block = $(this).attr('data-block');
        var lot = $(this).attr('data-lot');
        var downpayment = $(this).find('.addClientprop_downpayment');


        if(downpayment.val().trim() != ""){
          downpayment = parseFloat(downpayment.val().trim());        
        }
        else
          downpayment = 0;

        var sqmValue = parseFloat($(this).attr('data-sqm'));
        var pricePerm2 = parseFloat($(this).find('.addClientprop_pricePerm2').val());
        var planterms = parseInt($(this).find('.addClientprop_planterms').val());

        var newPrice = (((sqmValue * pricePerm2) - downpayment) + totalCharges);
        var newMonthly = (newPrice / planterms);


        if(phaseNumber != 0){
          phaseNumber = " Phase "+ phaseNumber+" ";
        }
        else
          phaseNumber = " ";


        $('.listOfPropertySelectedForChargesOutputContainer').append(
            getlistOfPropertySelectedForChargesOutputDetailsForm(
              (propertyName + " "+phaseNumber + " Block "+block+" Lot "+lot), 
              newPrice, 
              newMonthly)
            );
      });
    }




    function getlistOfPropertySelectedForChargesOutputDetailsForm(propertyName, newPrice, newMonthly){
      return(
        '<div class="col-sm-12 listOfPropertySelectedForChargesOutput">  '+
            '<div class="row">'+
                '<div class="col-sm-12">'+
                    '<h4 class="propertyName">'+propertyName+'</h4>'+
                '</div>'+
            '</div>'+
            '<div class="row">'+
                '<div class="col-sm-8">'+
                    '<div class="form-group form-md-line-input ">'+
                        '<input type="text" readonly="" value="'+accounting.format(newPrice,2)+'" readonly data-propertyID="0" class="form-control propertyPrice" placeholder="" name="propertyPrice">'+
                        '<label class="control-label"   for="form_control_1">Property price:</label>'+
                        '<div class="form-control-focus"> </div>'+
                        '<span class="help-block"></span>'+
                    '</div> '+
                '</div>'+
                '<div class="col-sm-4">'+
                    '<div class="form-group form-md-line-input ">'+
                        '<input type="text" readonly="" readonly  value="'+accounting.format(newMonthly,2)+'"   data-propertyID="0" class="form-control propertyPriceMonthly" placeholder="" name="propertyPriceMonthly">'+
                        '<label class="control-label"    for="form_control_1">Property monthly amortization:</label>'+
                        '<div class="form-control-focus"> </div>'+
                        '<span class="help-block"></span>'+
                    '</div> '+
                '</div>'+
            '</div>'+
        '</div>'
        );
    }



    function sqmOrPriceOrPlantesmHasError(sqm,price,terms){
      var hasError = false;
      var input = [price,terms];
      $.each(input,function(i,n){
        var e = $(this);
        
        if(e.val() == ""){
          hasError = true;
          $(e.parent().addClass('has-error'));
        }
        else if(e.hasClass('addClientprop_pricePerm2') ){
          if( !validateNumberAndDot(e.val())){
            hasError = true;
            e.parent().addClass('has-error');
          }
          else{
            e.parent().removeClass('has-error');
          }
        }
        else if(e.hasClass('addClientprop_planterms')){
          if( !validateInteger(e.val()) ){
            hasError = true;
            e.parent().addClass('has-error');
          }
          
        }
        else{
          e.parent().removeClass('has-error');
        }
      });

      if(sqm == ""){
        swal('Warning','SQM value is empty','warning');
        hasError = true;
      }
      if( !validateNumberAndDot(sqm)){
        swal('Warning','SQM value is invalid','warning');
        hasError = true;
      }

      return hasError;
    }


    function generateContactPriceAndMonthlyAmortization(){
      var sqm = $(this).parent().parent().parent().parent().parent().attr('data-sqm');
      var price = $(this).parent().parent().parent().find('.addClientprop_pricePerm2');
      var terms = $(this).parent().parent().parent().find('.addClientprop_planterms');


      if(sqmOrPriceOrPlantesmHasError(sqm,price,terms)){
        return false;
      }
      
      var total = (parseFloat(sqm) * parseFloat(price.val()));
      if ($('.addClientprop_downpayment').val().trim() === ''){
        $(this).parent().parent().parent().parent().parent().find('.addClientprop_monthlyamortization').val(accounting.format((total / parseInt(terms.val())),2));
      }else{
        var downpayment=$('.addClientprop_downpayment').val();
        var monthlyAmortization= ((parseFloat(total) - parseFloat(downpayment)) / parseFloat(terms.val()));
        $(this).parent().parent().parent().parent().parent().find('.addClientprop_monthlyamortization').val(accounting.format(monthlyAmortization,2));
      }
      $(this).parent().parent().parent().parent().parent().find('.addClientprop_originalprice').val(accounting.format(total,2));
      
    }


    function clientFormLotListCheckBoxLiIsClicked(){
      $(this).find('input').click();
    }


    function btnCancelThisSelectedClientPropertyIsClicked(evt){
      var number = $('.btnCancelThisSelectedClientProperty').length;
      var parent = $(this).parent().parent().parent().parent().parent();

      if(number == 1)
        addClientprop_propertyIsChange();
      else{
        $.each($('.slectedPorpertyList li'),function(){
          if($(this).attr('data-parent') == parent.attr('data-parentid') 
            &&  $(this).attr('data-block')  == parent.attr('data-block')
              && $(this).attr('data-lot')  == parent.attr('data-lot')){
            $(this).remove();
          }
        });        
        // parent.fadeOut().delay(1000).remove();
        parent.slideUp( 1000, function() {
           parent.remove();
        });
      }

      evt.preventDefault();
    }


    function addClientprop_propertyIsChange(){
      $( ".selectedProperty" ).slideUp( 1000, function() {
        $('.selectecPropertiesContainer').html("");
      });
      $('.slectedPorpertyList').html("");
    }



    function calculatePercentageIsEqualTo10Validation(isSubmit){

      var json = {
          directAgentCommission               : $('.directAgentCommission').val(),
          salesDirectorAgentCommission        : $('.salesDirectorAgentCommission').val(),
          salesManagerAgentCommission         : $('.salesManagerAgentCommission').val(),
          unitManagerAgentCommission          : $('.unitManagerAgentCommission').val(),
          managerAgentCommission              : $('.managerAgentCommission').val()
        };

      var hasError = false;
      var total = 0;
      $.each(json,function(i,n){
        var hrEror = false;
        if(n == undefined){
          // 
        }
        else if(!validateNumberAndDot(n)){
          hrEror = true;
          hasError= true;
          // 
        }
        else if(n == ""){
          // 
          hrEror = true;
          hasError= true;
        }
        else if(parseFloat(n) <= -1){
          // 
          hrEror = true;
          hasError= true;
        }
        else{
          // 
          total += parseFloat(n);
        }


        if(hrEror){
          $('.'+i).parent().addClass('has-error');
        }
        else{
          $('.'+i).parent().removeClass('has-error');
        }
      });

      if(total != 15){
        if(isSubmit == true){
          swal('Warning!','Commission percentage must be 15 in total.','warning');
          
          $('.directAgentCommission, .salesDirectorAgentCommission ,.salesManagerAgentCommission ,.unitManagerAgentCommission ,.managerAgentCommission').parent().addClass('has-error');
        }
        else
          $('.directAgentCommission, .salesDirectorAgentCommission ,.salesManagerAgentCommission ,.unitManagerAgentCommission ,.managerAgentCommission').parent().removeClass('has-error');
        hasError= true;
      }
        
      return hasError;
    }




    function validateAllInputFeildsForPropertiesHasError(){
      var hasError = false;
      var message = '';

      $.each($('.selectedProperty'),function(){
        var parent = $(this);
        var addClientprop_pricePerm2 = parent.find('.addClientprop_pricePerm2');
        var addClientprop_downpayment = parent.find('.addClientprop_downpayment');
        var addClientprop_dateapplied = parent.find('.addClientprop_dateapplied');
        var addClientprop_planterms = parent.find('.addClientprop_planterms');

        var inputs = [
          addClientprop_pricePerm2,
          addClientprop_downpayment,
          addClientprop_dateapplied,
          addClientprop_planterms
        ];

        $.each(inputs,function(i,n){
          if(!n.hasClass('addClientprop_downpayment')){
            if(n.val() == ""){
              n.parent().addClass('has-error');
              n.parent().find('.help-block').html('This is required.');
              hasError = true;
            }
            else{
              n.parent().find('.help-block').html('');
              n.parent().removeClass('has-error');
            } 
          }
        });

        if(hasError)
          swal('Warning!','There are empty feilds','warning');

        if(!hasError){
          $.each(inputs,function(i,n){
            if( n.hasClass('addClientprop_pricePerm2')){
              if(n.val() == "" || !validateNumberAndDot(n.val())){
                n.parent().find('.help-block').html('Please provide valid ammount.');
                n.parent().addClass('has-error');
                hasError = true;
              }
              else{
                n.parent().find('.help-block').html('');
                n.parent().removeClass('has-error');
              }
            }
            if(n.hasClass('addClientprop_downpayment')){
              var parnt = n.parent().parent().parent();
              if(n.val() != ""){
                if(!validateNumberAndDot(n.val())){
                  n.parent().find('.help-block').html('Please provide valid ammount.');
                  n.parent().addClass('has-error');
                  hasError = true;
                }
                else if(parnt.find('.clientmodeOfPayment').val() == 2 || parnt.find('.clientmodeOfPayment').val() == 3 ){

                  if(parnt.find('.clientremitanceCentername').val().trim() == ""){
                    parnt.find('.clientremitanceCentername').parent().find('.help-block').html('Please provide valid ammount.');
                    parnt.find('.clientremitanceCentername').parent().addClass('has-error');
                    hasError = true;
                  }
                  else{
                    parnt.find('.clientremitanceCentername ,.addClientprop_ornumber').parent().find('.help-block').html('');
                    parnt.find('.clientremitanceCentername ,.addClientprop_ornumber').parent().removeClass('has-error');
                    n.parent().find('.help-block').html('');
                    n.parent().removeClass('has-error'); 
                  }
                }
                else{
                  
                  n.parent().find('.help-block').html('');
                  n.parent().removeClass('has-error'); 
                  parnt.find('.clientremitanceCentername ,.addClientprop_ornumber').parent().find('.help-block').html('');
                  parnt.find('.clientremitanceCentername ,.addClientprop_ornumber').parent().removeClass('has-error');
                }
              }
            }
            else if(n.hasClass('addClientprop_planterms')){
              if(!validateInteger(n.val())){
                n.parent().find('.help-block').html('Please provide valid terms.');
                n.parent().addClass('has-error');
                hasError = true;
              }
              if(parseInt(n.val()) <= 0){
                n.parent().find('.help-block').html('Please provide valid terms.');
                n.parent().addClass('has-error');
                hasError = true;
              }
              else{
                n.parent().find('.help-block').html('');
                n.parent().removeClass('has-error');
              }
            }



            if(hasError)
              message = 'There are invalid values.3 ';


            if(n.hasClass('addClientprop_dateapplied')){
              if(!ValidateDate(n.val())){
                n.parent().find('.help-block').html('Please provide date value.');
                n.parent().addClass('has-error');
                hasError = true;
              }
              else{
                n.parent().find('.help-block').html('');
                n.parent().removeClass('has-error');
              }

              if(hasError)
                message = 'There are invalid values.2 ';
            }


          });
        }

      });
  

      if(orNumberDuplicates()){
        message = 'OR number might be empty or not unique.';
        hasError = true; 
      }

      if($('.selectedProperty').length == 0){
        message = 'There are invalid values.1 ';
        hasError = true;
      }

      if($('.selectecPropertiesContainer').html() == ""){
        message = 'Please select property for this client first.';
        hasError = true;
      }

      if(message != "")
        swal('Warning',message,'warning');

      return hasError;
    }



    function orNumberDuplicates(){
      var hasDuplicate = false;
      var array = [];
      $.each($('.addClientprop_downpayment'),function(i,n){
        var val = $(this).val().trim();
        if(val != ""){
          if(validateNumberAndDot(val)){
            var parent = $(this).parent().parent().parent();
            var ornumber = parent.find('.addClientprop_ornumber').val().trim();
            if(ornumber == ""){
              hasDuplicate = false;
            }
            else if($.inArray( ('{'+ornumber+'}') , array ) != -1){
              hasDuplicate = false;
            }
            else{
              array.push(('{'+ornumber+'}'));
            }
          }
        }
      });

      return hasDuplicate;
    }


    function removeThisSelectedBlockAndParent(Sparent,Sblock,phaseNumber){

      $.each($('.slectedPorpertyList li'),function(i,n){
        var parent = $(this).attr('data-parent');
        var block = $(this).attr('data-block');
        var lot = $(this).attr('data-lot');
        var phase = $(this).attr('data-phasenumber');
        if(parent == Sparent && block == Sblock && phase == phaseNumber){
          $(this).remove();
        }
      }); 
    }

     function  ifoverView(){
        $(".tab_active_1").css({"color": "#169ef4", "text-decoration": "none", "background": "#ecf5fb", "border-left": "solid 2px #169ef4"});
        $(".tab_active_2").css({"color": "#557386", "text-decoration": "none", "background": "#f0f6fa", "border-left": "solid 2px #c4d5df"});
        $(".tab_active_3").css({"color": "#557386", "text-decoration": "none", "background": "#f0f6fa", "border-left": "solid 2px #c4d5df"});
        $(".img-update").addClass("hidden");
        $(".profile-edit").addClass("hidden");
        $(".updateImg").removeClass("btnfile");
    }
    
    function  ifupdateInfo(){
        $(".tab_active_2").css({"color": "#169ef4", "text-decoration": "none", "background": "#ecf5fb", "border-left": "solid 2px #169ef4"});
        $(".tab_active_1").css({"color": "#557386", "text-decoration": "none", "background": "#f0f6fa", "border-left": "solid 2px #c4d5df"});
        $(".tab_active_3").css({"color": "#557386", "text-decoration": "none", "background": "#f0f6fa", "border-left": "solid 2px #c4d5df"});
        $(".img-update").addClass("hidden");
        $(".profile-edit").addClass("hidden");
        $(".updateImg").removeClass("btnfile");
    }


      function  ifchangePass(){
        $(".tab_active_3").css({"color": "#169ef4", "text-decoration": "none", "background": "#ecf5fb", "border-left": "solid 2px #169ef4"});
        $(".tab_active_1").css({"color": "#557386", "text-decoration": "none", "background": "#f0f6fa", "border-left": "solid 2px #c4d5df"});
        $(".tab_active_2").css({"color": "#557386", "text-decoration": "none", "background": "#f0f6fa", "border-left": "solid 2px #c4d5df"});
        $(".img-update").removeClass("hidden");
        $(".profile-edit").removeClass("hidden");
        $(".updateImg").addClass("btnfile");
    }



    var files;
    function prepareUpload(event)
    {
      $('.clientProfilePictureForm').ajaxForm({
        beforeSubmit: function() {
         
       },
       success: function(data) {
          
          
         // var $out = $('#results');
         // $out.html('Your results:');
         // $out.append('<div><pre>'+ data +'</pre></div>');
        }
      });
    }


    function clientFormblocksListChange(){
      var parent = $(this);
      $('.clientFormblocksListElement').css('color','white');

      var parentProperty = $('.addClientprop_property').val();
      var blockNumber = $(this).attr('data-blockNumber');
      var phaseNumber = $(this).attr('data-phasenumber');
      if(parentProperty == undefined || !validateInteger(parentProperty)){
        swal('Warning!','Please select property name.','warning');
        return false;
      }
      else if(blockNumber == undefined || !validateInteger(blockNumber)){
        swal('Warning!','Please select block number.','warning');
        return false;
      }

      showLoading();
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/property/getAllLotsForThisParentBlock",
          data : {id : parentProperty,blockID : blockNumber,phaseNumber:phaseNumber},
          cache: false,
          async: true,
          success: function(response) { 
            
            if(response.success){
                var clientFormlotsList = $('.clientFormLotsList');
                clientFormlotsList.empty();
                $.each(response.blockAndLots,function(i,n){
                  var newBootboxElementj = "";
                  if(n.lotStatus == 0){
                    newBootboxElementj = $(
                      '<li class="mt-list-item clientFormLotListCheckBoxLi">'+
                          '<div class="list-icon-container done">'+
                              '<i class="icon-check"></i>'+
                          '</div>'+
                          '<div class="list-datetime"> <input type="checkbox" data-phasenumber="'+n.phaseNumber+'" data-lotNumber="'+n.lotNumber+'" data-blockNumber="'+blockNumber+'" class="clientFormLotListCheckBox" name=""> </div>'+
                          '<div class="list-item-content" >'+
                              '<h3 class="uppercase">'+
                                  '<a href="javascript:;">'+n.LotNumberName+'</a>'+
                              '</h3>'+
                          '</div>'+
                          '<p class="ist-item-content text-center">  ('+n.propertyTypeName+(n.propertyTypeID == 2? (', '+n.propertyModel):'')+') </p>'+
                      '</li>'
                    ).hide();                  
                  }else if(n.lotStatus == 1){
                   
                    newBootboxElementj = $(
                      '<li class="mt-list-item " style="color:gray; background-color: #ece7e7">'+
                          '<div class="list-icon-container ">'+
                              '<i class="icon-close"></i>'+
                          '</div>'+
                          '<div class="list-datetime">  </div>'+
                          '<div class="list-item-content" >'+
                              '<h3 class="uppercase">'+
                                  '<label  >'+n.LotNumberName+'</label>'+
                              '</h3>'+
                          '</div>'+
                          '<p class="ist-item-content text-center"> ('+n.propertyTypeName+(n.propertyTypeID == 2? (', '+n.propertyModel):'')+') - ( Occupied ) </p>'+
                      '</li>'
                    ).hide();  

                  }else if(n.lotStatus == 2){

                    newBootboxElementj = $(
                      '<li class="mt-list-item " style="color:gray; background-color: #ece7e7">'+
                          '<div class="list-icon-container ">'+
                              '<i class="icon-close"></i>'+
                          '</div>'+
                          '<div class="list-datetime">  </div>'+
                          '<div class="list-item-content" >'+
                              '<h3 class="uppercase">'+
                                  '<label  style="color:gray">'+n.LotNumberName+' </label>'+
                              '</h3>'+
                          '</div>'+
                          '<div class=" ist-item-content text-center" >'+
                            '<p> ('+n.propertyTypeName+(n.propertyTypeID == 2? (', '+n.propertyModel):'')+') - ( Disabled ) </p>'+
                          '</div>'+
                      '</li>'
                    ).hide();  

                  }

                  clientFormlotsList.append(newBootboxElementj);
                  newBootboxElementj.delay(100*i).slideDown(300);
                });

                initSelectedProperties();
                hideLoading();  
            }
            else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }

            hideLoading();
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });

    }

    
    function viewPropertyList(){
      var parentProperty = $('.addClientprop_property').val();
      if(parentProperty == undefined || !validateInteger(parentProperty)){
        swal('Warning!','Please select property name.','warning');
        return false;
      }

      showLoading();
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/property/getAllBlockForThisParent",
          data : {id : parentProperty},
          cache: false,
          async: true,
          success: function(response) { 
            
            if(response.success){

              bootbox.dialog({

                message: getModalForBlocksAndLotsList(),
                title: "Property blocks and lots",
                onEscape: function() {  },
                backdrop: true,
                buttons: {
                  success: {
                    label: "Initialize",
                    className: "green btn-outline",
                    callback: function() {
                      var selectedProp = [];

                      getClientFormLotsListNew();

                      $.each($('.slectedPorpertyList li'),function(i,n){
                        var parent = $(this).attr('data-parent');
                        var block = $(this).attr('data-block');
                        var lot = $(this).attr('data-lot');
                        var phaseNumber = $(this).attr('data-phasenumber');
                        selectedProp.push({
                          parent  : parent,
                          block   : block,
                          lot     : lot,
                          phaseNumber: phaseNumber
                        });
                      }); 


                      if(selectedProp.length != 0)
                        updateSelectecPropertiesContainer(selectedProp);
                    }
                  },
                }
              }).init(function(){

                var clientFormblocksList = $('.clientFormblocksList');
                clientFormblocksList.empty();
                $.each(response.blocks,function(i,n){
                  var newBootboxElementj = $(
                    '<li class="mt-list-item clientFormblocksListElement " data-phaseNumber="'+n.phaseNumber+'" data-blocknumber="'+n.blockNumber+'">'+
                        '<div class="list-todo-icon bg-white">'+
                            '<i class="fa fa-map"></i>'+
                        '</div>'+
                        '<div class="list-todo-item dark">'+
                            '<a class="list-toggle-container" data-toggle="collapse" data-parent="#accordion1" href="#task-2" aria-expanded="false">'+
                                '<div class="list-toggle done uppercase">'+
                                    '<div class="list-toggle-title bold">'+n.blockNumberName+'</div>'+
                                    // '<div class="badge badge-default pull-right bold "><span style="color: red;" class="sbold">0</span></div>'+
                                '</div>'+
                            '</a>'+
                        '</div>'+
                    '</li>'
                  ).hide();  

                  clientFormblocksList.append(newBootboxElementj);
                  newBootboxElementj.slideDown(500);
                });

                
                

                // initSelectedProperties();

                $('.select2').select2(); 
                hideLoading();  
              });
            }
            else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }

            hideLoading();
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }







    function getClientFormLotsListNew(){
      var parentPropertyVal= $('.addClientprop_property').val();
      var clientFormblocksListVal = $('.clientFormLotListCheckBoxLi input')[0];

      if(clientFormblocksListVal != undefined){
        block = clientFormblocksListVal.getAttribute("data-blockNumber");
        phaseNumber = clientFormblocksListVal.getAttribute("data-phasenumber");
        removeThisSelectedBlockAndParent(parentPropertyVal,block,phaseNumber);


        $.each($('.clientFormLotListCheckBoxLi input:checked'),function(){
          var lot = $(this).attr('data-lotnumber'); 

          $('.slectedPorpertyList').append(
            '<li data-phasenumber="'+phaseNumber+'" data-parent="'+parentPropertyVal+'" data-block="'+block+'" data-lot="'+lot+'">Phase '+phaseNumber+' Block '+block+' Lot '+lot+'</li>'
          );
        });
      }
    }







    function updateSelectecPropertiesContainer(selectedProp){
      showLoading();
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/property/getAllOfThisSelectedPropertiesInfo",
          data : {data : selectedProp},
          cache: false,
          async: true,
          success: function(response) { 
            
            if(response.success){
              var selectedPropertyContainer = $('.selectecPropertiesContainer');
              selectedPropertyContainer.empty();

              $.each(response.properties,function(i,n){
                var newElement = $(getSelectedPropertyForm(
                      n.parentID,
                      n.phaseNumber,
                      n.block,
                      n.lot,
                      n.propertyName,
                      n.propertyAddress,
                      n.dateNow,
                      // propertyMonthlyAmortization = (n.propertyOriginalPrice / n.propertyPlanTerms),
                      n.propertyMonthlyAmortization,
                      (n.phaseNumber == 0? "": ("Phase "+n.phaseNumber))+' Block '+n.block+' Lot '+n.lot,
                      n.propertySQM,
                      n.propertyOriginalPrice,
                      n.propertyModel,
                      n.propertyPricePerSQM,
                      n.propertyPlanTerms,
                      n.propertyTypeID)).hide();
                selectedPropertyContainer.append(
                    newElement
                  );
                newElement.slideDown(1500);
              });

              $('select').select2();
              $('.date-picker').datepicker({
                  format: "yyyy-mm-dd",
                  todayHighlight: true
              });
              $('.selectecPropertiesContainer input').focus();
              hideLoading();
            }
            else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }

            hideLoading();
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }



    function initSelectedProperties(){
      //  this will initialize values if was selected previously
      var parentPropertyVal= $('.addClientprop_property').val();
      var clientFormblocksListVal = $('.clientFormblocksList').val();


      var checkThisInput = function(block,lot){
        $.each($('.clientFormLotListCheckBoxLi input'),function(){
            var lot2 = $(this).attr('data-lotnumber');
            var block2 = $(this).attr('data-blocknumber');    

            if(block2 == block && lot2 == lot ){
              $(this).parent().parent().click();
            }
        });
      };

      $.each($('.slectedPorpertyList li'),function(i,n){
        var parent = $(this).attr('data-parent');
        var block = $(this).attr('data-block');
        var lot = $(this).attr('data-lot');

        checkThisInput(block,lot);

      }); 

    }





    function getSelectedPropertyForm(parentID,phaseNumber,block,lot,propertyName,address,dateApplied,monthlyAmortization,
      blockAndLotNumber,sqm,contractPrice,model,pricePerm2,plantTerms,propertyTypeID){


      return ('<div class="row selectedProperty" data-propertyTypeID="'+propertyTypeID+'" data-model="'+model+'" data-phaseNumber="'+phaseNumber+'" data-sqm="'+sqm+'" data-parentID="'+parentID+'" data-block="'+block+'" data-lot="'+lot+'">'+
        '<div class="col-md-12"><hr  style="margin-top: 10px;">'+
          '<div class="form-group">'+
                '<div class="col-xs-8">'+
                  '<h2><strong>'+propertyName+' '+blockAndLotNumber+'</strong></h2>'+
                '</div>'+
          '</div>'+
        '</div>'+
        '<div class="col-md-7">'+
            '<div class="form-group">'+
                '<div class="col-sm-5">'+
                    '<div class="form-group form-md-line-input ">'+
                        '<input type="text" readonly="" value="'+propertyName+'" class="form-control addClientprop_propertyName" placeholder="" name="addClientprop_propertyName">'+
                        '<label class="control-label" for="form_control_1">Property Name'+
                        '</label>'+
                        '<div class="form-control-focus"> </div>'+
                        '<span class="help-block"></span>'+
                    '</div>'+
                 '</div>'+
                 '<div class="col-sm-7">'+
                    '<div class="form-group form-md-line-input ">'+
                        '<input type="text" readonly="" value="'+address+'" class="form-control addClientprop_address" placeholder="" name="addClientprop_address">'+
                        '<label class="control-label" for="form_control_1">Address'+
                        '</label>'+
                        '<div class="form-control-focus"> </div>'+
                        '<span class="help-block"></span>'+
                    '</div>'+
                 '</div>'+
            '</div>'+
            '<div class="form-group">'+
                 '<div class="col-sm-4"> '+
                    '<div class="form-group form-md-line-input ">'+
                        '<input type="text" value="'+dateApplied+'" class="form-control addClientprop_dateapplied date-picker" placeholder="" data-date-format="yyyy-mm-dd" value="2017-12-21" name="addClientprop_dateapplied">'+
                        '<label class="control-label" for="form_control_1">Date Applied'+
                            '<span class="required">*</span>'+
                        '</label>'+
                        '<div class="form-control-focus"> </div>'+
                        '<span class="help-block"></span>'+
                    '</div>'+
                 '</div>'+
                '<div class="col-sm-4">'+
                    '<div class="form-group form-md-line-input ">'+
                        '<input type="text" readonly="" value="'+blockAndLotNumber+'" class="form-control addClientprop_lotarea" placeholder="" name="addClientprop_lotarea">'+
                        '<label class="control-label" for="form_control_1">Lot No. /Area'+
                        '</label>'+
                        '<div class="form-control-focus"> </div>'+
                        '<span class="help-block"></span>'+
                    '</div>  '+
                '</div>'+

                '<div class="col-sm-4">  '+
                    '<div class="form-group form-md-line-input ">'+
                        '<input type="text" readonly="" value="'+sqm+'" class="form-control addClientprop_sqm" placeholder="" name="addClientprop_sqm">'+
                        '<label class="control-label" for="form_control_1">SQ.M'+
                        '</label>'+
                        '<div class="form-control-focus"> </div>'+
                        '<span class="help-block"></span>'+
                    '</div>  '+
                '</div>'+
                '<div class="col-sm-4"> '+
                    '<div class="form-group form-md-line-input ">'+
                        '<input type="text" readonly=""  value="'+model+'"  class="form-control addClientprop_model" placeholder="" name="addClientprop_model">'+
                        '<label class="control-label" for="form_control_1">Lot Model'+
                        '</label>'+
                        '<div class="form-control-focus"> </div>'+
                        '<span class="help-block"></span>'+
                    '</div> '+
                '</div>'+
                '<div class="col-sm-4"> '+
                    '<div class="form-group form-md-line-input ">'+
                        // '<input type="number" class="form-control commissonReleasedSched" placeholder="" name="commissonReleasedSched">'+
                        '<select class="form-control commissonReleasedSched select2" name="commissonReleasedSched" style="width: 100%;">'+   
                          '<option value="12">12</option> '+   
                          '<option value="15">15</option> '+   
                          '<option value="18">18</option> '+   
                          '<option value="0">Others</option> '+   
                        '</select>'+   
                        '<label class="control-label" for="form_control_1">Com Release Schedule'+
                        '</label>'+
                        '<div class="form-control-focus"> </div>'+
                        '<span class="help-block"></span>'+
                    '</div> '+
                '</div>'+
                 '<div class="col-sm-4 hidden schedInput"> '+
                    '<div class="form-group form-md-line-input ">'+
                        '<input type="number" class="form-control commissonReleasedSchedInput" placeholder="" name="commissonReleasedSchedInput">'+
                        '<label class="control-label" for="form_control_1">Release Sced:'+
                        '</label>'+
                        '<div class="form-control-focus"> </div>'+
                        '<span class="help-block"></span>'+
                    '</div> '+
                '</div>'+
                '<div class="col-sm-4"> '+
                    '<div class="form-group form-md-line-input ">'+
                        '<input type="number" class="form-control addComission" placeholder="" name="commissonReleasedSched">'+
                        '<label class="control-label" for="form_control_1">Add Commission'+
                        '</label>'+
                        '<div class="form-control-focus"> </div>'+
                        '<span class="help-block"></span>'+
                    '</div> '+
                '</div>'+
                '<div class="col-sm-4">  '+
                    '<div class="form-group form-md-line-input ">'+
                        '<input type="Number"  class="form-control addClientprop_downpayment "  placeholder="" name="addClientprop_downpayment">'+
                        '<label class="control-label" for="form_control_1">Down Payment'+
                        '</label>'+
                        '<div class="form-control-focus"> </div>'+
                        '<span class="help-block"></span>'+
                    '</div>  '+
                                 '<div class="col-sm-4">  '+
                    '<div class="form-group form-md-line-input ">'+
                        '<input type="Number"  class="form-control addClientDueDate "  placeholder="" name="addClientDueDate">'+
                        '<label class="control-label" for="form_control_1">Due Date'+
                        '</label>'+
                        '<div class="form-control-focus"> </div>'+
                        '<span class="help-block"></span>'+
                    '</div>  '+
                '</div>  '+
                '</div>  '+
                '<div class="col-sm-4 clientornumberContainer hidden">  '+
                    '<div class="form-group form-md-line-input ">'+
                        '<input type="text"  class="form-control addClientprop_ornumber"  placeholder="" name="addClientprop_ornumber">'+
                        '<label class="control-label" for="form_control_1">OR Number'+
                          '<span class="required">*</span>'+
                        '</label>'+
                        '<div class="form-control-focus"> </div>'+
                        '<span class="help-block"></span>'+
                    '</div>  '+
                '</div>  '+
                '<div class="col-sm-4 clientmodeOfPaymentContainer hidden ">  '+
                    '<div class="form-group form-md-line-input">'+
                        '<select class="form-control clientmodeOfPayment select2" name="clientmodeOfPayment" style="width: 100%;">'+
                            '<option value="1">Cash</option>'+
                            '<option value="2">Bank</option>'+
                            '<option value="3">Remittance Center</option>'+
                        '</select>'+
                        '<label for="form_control_1">Mode of Payment</label>'+
                        '<span class="help-block"></span>'+
                    '</div>'+
                    '<div class="form-group form-md-line-input hidden clientremitanceCenterContainer ">'+
                        '<input type="text" class="form-control clientremitanceCentername"  id="form_control_1" name="clientremitanceCentername">'+
                        '<label for="form_control_1">Remittance Center<span class="required">*</span></label>'+
                    '</div>'+
                '</div>  '+
            '</div>'+
        '</div>'+
        '<div class="col-md-5">'+
            '<div class="form-group">'+
                '<div class="col-sm-6">'+
                    '<div class="form-group form-md-line-input ">'+
                        '<input type="text"   value="'+pricePerm2+'" class="form-control addClientprop_pricePerm2" placeholder="" name="addClientprop_pricePerm2" >'+
                        '<label class="control-label" for="form_control_1">Price per m<sup>2</sup>'+
                            '<span class="required">*</span>'+
                        '</label>'+
                        '<div class="form-control-focus"> </div>'+
                        '<span class="help-block"></span>'+
                    '</div> '+
                '</div>'+
                 '<div class="col-sm-6">'+
                    '<div class="form-group form-md-line-input ">'+
                        '<input type="text"  value="'+plantTerms+'" class="form-control addClientprop_planterms" placeholder="" name="addClientprop_planterms" >'+
                        '<label class="control-label" for="form_control_1">Plan Terms'+
                            '<span class="required">*</span>'+
                        '</label>'+
                        '<div class="form-control-focus"> </div>'+
                        '<span class="help-block"></span>'+
                    '</div> '+
                '</div>'+
            '</div>'+
            '<div class="form-group">'+
                '<div class="col-sm-6"> '+
                    '<div class="form-group form-md-line-input ">'+
                        '<input type="text" readonly value="'+contractPrice+'" class="form-control addClientprop_originalprice" placeholder="" name="addClientprop_originalprice">'+
                        '<label class="control-label" for="form_control_1">Contract Price'+
                            '<span class="required">*</span>'+
                        '</label>'+
                        '<div class="form-control-focus"> </div>'+
                        '<span class="help-block"></span>'+
                    '</div>  '+
                '</div>'+
                '<div class="col-sm-6">'+
                    '<div class="form-group form-md-line-input ">'+
                        '<input type="text" readonly value="'+monthlyAmortization+'" class="form-control addClientprop_monthlyamortization" placeholder="" name="addClientprop_monthlyamortization">'+
                        '<label class="control-label" for="form_control_1">Monthly Amortization'+
                            '<span class="required">*</span>'+
                        '</label>'+
                        '<div class="form-control-focus"> </div>'+
                        '<span class="help-block"></span>'+
                    '</div>  '+
                '</div>'+
            '</div>'+
            '<div class="form-group">'+
                '<div class="col-xs-12"> '+
                    '<div class="col-xs-12 ">'+
                      '<button style="margin-top: 20px;" class="btn btn-outline pull-right red btnCancelThisSelectedClientProperty"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+
      '</div>');



    }



    function getModalForBlocksAndLotsList_backup(){
      


      return (
        phaseList+
        '<div class="row">'+
          '<div class="col-sm-4 ">'+
              '<div class="input-group select2-bootstrap-prepend">'+
                  '<span class="input-group-btn">'+
                      '<button class="btn btn-default" type="button" data-select2-open="multi-prepend"> Block </button>'+
                  '</span>'+
                  '<select class="form-control select2 clientFormblocksList" style="width:100%">'+
                  '</select>'+
              '</div>'+
          '</div>'+
         
           '<div class="col-sm-8">'+
                '<div class="input-group select2-bootstrap-prepend">'+
                  '<span class="input-group-btn">'+
                      '<button class="btn btn-default" type="button" data-select2-open="multi-prepend"> Lot </button>'+
                  '</span>'+
                  '<select id="multi-prepend" class="form-group select2 clientFormLotsList" multiple style="width:100%">'+
                  '</select>'+
              '</div>'+
           '</div>'+
        '</div>'
        );
    }


    function getModalForBlocksAndLotsList(){



      var newBootboxElementj = (
        '<div class="row"><style>'+
            '.mt-element-list .list-todo.mt-list-container ul>.mt-list-item>.list-todo-item {'+
              'margin-left: 15px;'+
              'display: inline-block;'+
              'vertical-align: top;'+
              'width: 83%;'+
              'position: relative;'+
            '}'+
            '.clientFormLotListCheckBoxLi , .clientFormblocksList > li{'+
              'transform: background-color 2s;'+
            '}'+
            '.clientFormLotListCheckBoxLi:hover , .clientFormblocksList > li:hover{'+
              'background-color: #ebf7ff;'+
            '}'+
          '</style>'+
                '<div class="col-md-6">'+
                    '<div class="portlet-body">'+
                        '<div class="mt-element-list">'+
                            '<div class="mt-list-head list-todo red">'+
                                '<div class="list-head-title-container">'+
                                    '<h3 class="list-title">Block List</h3>'+
                                '</div>'+
                                ''+
                            '</div>'+
                            '<div class="mt-list-container list-todo" id="accordion1" role="tablist" aria-multiselectable="true" style="overflow-y: scroll; height: 250px;">'+
                                '<div class="list-todo-line"></div>'+
                                '<ul class="clientFormblocksList">'
        );

    


      newBootboxElementj += (
                              '</ul>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="col-md-6">'+
                '<div class="portlet-body">'+
                    '<div class="mt-element-list">'+
                        '<div class="mt-list-head list-simple font-white bg-green-sharp">'+
                            '<div class="list-head-title-container">'+
                                '<h3 class="list-title">Lot List</h3>'+
                            '</div>'+
                        '</div>'+
                        '<div class="mt-list-container list-simple" style="overflow-y: scroll; height: 250px;">'+
                            '<ul class="clientFormLotsList">'
        );




      newBootboxElementj += (
                            '</ul>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'
        );

      return newBootboxElementj;
    }






    function uploadThisImage(event){
      showLoading();

      $.ajax({
        url: apiUrl+"/dashboard/client/uploadThisImage",
        type: 'POST',
        data: new FormData(this),
        async: true,
        cache: false,
        contentType: false,
        // enctype: 'multipart/form-data',
        processData: false,
        success: function (response) {          
          hideLoading();
          if(response.success){
            setTimeout(function(){swal('Success!',response.message,'success');},0);
          }
          else{
            setTimeout(function(){swal('Warning!',response.message,'warning');},0);
          }

        },
        error: function(err){
          hideLoading();
          setTimeout(function(){swal("Error!", err.statusText, "error");},0);
        }
      });

      event.stopPropagation();
      event.preventDefault(); 
    }

    function updateProfilePicChange(){
      $("#file_uploads").click(); 

        var input = this;
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.btnfile').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
        
        var x = document.getElementById("file_uploads").value;
                
        var path = x;
        var pos =path.lastIndexOf( path.charAt( path.indexOf(":")+1) );
        filename = path.substring( pos+1);
    }

    function printPayments(){
      var cid = '';
      var cid = $('.clientPropertiesList ').val();
      // window.location.href = '/addons/fpdf/tutorial/tuto5.php?cid='+cid+'?';
      window.open('/addons/fpdf/statementOfAccount.php?cid='+cid, '_blank');
      // var html="<html>";
      // html+= document.getElementById('reporPropertyPaymentHistory').innerHTML;
      // html+="</html>";

      // var printWin = window.open();
      // printWin.document.write(html);
      // printWin.document.close();
      // printWin.focus();
      // printWin.print();
      // printWin.close();
    }

    function updateProfileTabIsClicked(){
      clienUpdateFormValidation();
      $('.updateClientForm2 input').focus();
    }


    function thisClientPropertiesTabIsClicked(){
      var id = $('.clientPropertiesList').val();
      if(id == "" || id == undefined)
        return false;

      var ClientID = $('.imageClientID').val();

      if ($('.propertyStatus').html() == "Property Forfeited") {
          loadThisPropertyDetailsAndPaymentHistoryforfeited(id,ClientID);
      }else{
          loadThisPropertyDetailsAndPaymentHistory(id);
      }

      loadAgentCom();
    }

    function loadThisPropertyDetailsAndPaymentHistoryforfeited(id, ClientID){
      showLoading();
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/client/getThisClientPropertyInfoAndHistoryforfeited",
          data : {id : id, ClientID: ClientID},
          cache: false,
          async: true,
          success: function(response) { 
            if(response.success){

              if (response.propertyInfo[0]['fullyPaid'] == 1) {
                $('.paidDaw').removeClass('hidden');
              }else{
                $('.paidDaw').addClass('hidden');
              }

              $.each(response.propertyInfo[0],function(i,n){
                if(('.prof_'+i) == '.prof_contractPrice' || ('.prof_'+i) == '.prof_monthlyAmortization'  || ('.prof_'+i) == '.prof_additionalCharges'  || ('.prof_'+i) == '.prof_pricePerm2'  || ('.prof_'+i) == '.prof_downpayment'){
                  n = "&#x20B1; "+ accounting.format(n,2);
                }else if(i == "ComRelease"){
                  $('.updateComRelSched').val(''+ n +'');
                }else if(i == "adCom"){
                  $('.updateAddCom').val(''+ n +'');
                }
                $('.prof_'+i).html(n);
              });

              $('.addtional-charges').empty();
              $.each(response.additionalCharges, function(i,n){
                 $('.addtional-charges').append(
                  '<div class="row additionalChargesDetails">'+
                      '<div  class="form-group" style="padding-left: 15px">'+
                          '<div class="col-md-3">'+
                              '<div class="form-group form-md-line-input">'+
                                  '<input type="text" class="form-control chargeValue" name="chargeValue" value="'+ n['chargeValue'] +'">'+
                                  '<label for="form_control_1">Value</label>'+
                                  '<span class="help-block"></span>'+
                              '</div>'+
                          '</div>'+
                          '<div class="col-md-3">'+
                              '<div class="form-group form-md-line-input">'+
                                  '<input type="text" class="form-control chargeDate" name="chargeDate" value="'+ n['chargeDate'] +'">'+
                                  '<label for="form_control_1">Date Added</label>'+
                                  '<span class="help-block"></span>'+
                              '</div>'+
                          '</div>'+
                          '<div class="col-md-7">'+
                              '<div class="form-group form-md-line-input">'+
                                  '<input type="text" class="form-control chargeDescription" name="chargeDescription" value="'+ n['chargeDescription'] +'">'+
                                  '<label for="form_control_1">Description</label>'+
                                  '<span class="help-block"></span>'+
                              '</div>'+
                          '</div>'+
                           '<div class="col-md-2 text-left">'+
                              '<a href="javascript:;" class="btn btn-outline red cms-redo-charges"><span class="icon-close"></span></a>'+
                          '</div>'+
                      '</div>'+
                  '</div>'
                );
              });

              $('.chargeValue').datepicker({
                format: 'yyyy-mm-dd'
              })


              var table = $('.clientPropertyPaymentHistoryTable').DataTable();
              table.clear().draw();
              table.rows.add(response.propertyPaymentHistory).draw();
              $('.listOfPaymentHistory').empty();
              $.each(response.propertyPaymentHistory,function(i,n){


                $('.listOfPaymentHistory').append(
                  '<tr>'+
                    '<td>'+n[0]+'</td>'+
                    '<td>'+n[1]+'</td>'+
                    '<td>'+n[2]+'</td>'+
                    '<td>'+n[3]+'</td>'+
                    '<td>'+(n[4] == '0.00' ? '': ("&#x20B1; "+accounting.format(n[4],2)))+'</td>'+
                    '<td>'+(n[5] == '0.00' ? '': ("&#x20B1; "+accounting.format(n[5],2)))+'</td>'+
                    '<td>'+(n[6] == '0.00' ? '': ("&#x20B1; "+accounting.format(n[6],2)))+'</td>'+
                    '<td>'+n[7]+'</td>'+
                    // '<td>'+("&#x20B1; "+accounting.format(n[6],2))+'</td>'+
                  '</tr>'
                  );
              });

              $('.listOfPaymentHistory').prepend(` <tr>
                                <th class="cms-num">#</th>
                                <th>OR #</th>
                                <th>PAYMENT FOR MONTH</th>
                                <th>DATE OF PAYMENT</th>
                                <th>BANK/REMITTANCE</th>
                                <th>CASH</th>
                                <th>BALANCE</th>
                                <th>MODE OF PAYMENT</th>
                            </tr>`);
            }
            else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }

            $('.clientPropertiesList').select2({
              placeholder: "Select a project",
              allowClear: true
            }); 
            hideLoading();
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }

    function getSum(total, num) {
       return total + num;
    }


    function loadThisPropertyDetailsAndPaymentHistory(id){
      showLoading();
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/client/getThisClientPropertyInfoAndHistory",
          data : {id : id},
          cache: false,
          async: true,
          success: function(response) { 
            if(response.success){

              if (response.propertyInfo[0]['fullyPaid'] == 1) {
                $('.paidDaw').removeClass('hidden');
              }else{
                $('.paidDaw').addClass('hidden');
              }

              $.each(response.propertyInfo[0],function(i,n){
                if(('.prof_'+i) == '.prof_contractPrice' || ('.prof_'+i) == '.prof_monthlyAmortization' || ('.prof_'+i) == '.dueDate' || ('.prof_'+i) == '.prof_additionalCharges'  || ('.prof_'+i) == '.prof_pricePerm2'  || ('.prof_'+i) == '.prof_downpayment'){
                  n = "&#x20B1; "+ accounting.format(n,2);
                }else if(i == "ComRelease"){
                  $('.updateComRelSched').val(''+ n +'');
                }else if(i == "adCom"){
                  $('.updateAddCom').val(''+ n +'');
                }
                $('.prof_'+i).html(n);
              });

              $('.addtional-charges').empty();

              let chargeTotal = 0;
              $.each(response.additionalCharges, function(i,n){
                  chargeTotal += (parseFloat(n['chargeValue']));
               
                 $('.addtional-charges').append(
                  '<div class="row additionalChargesDetails">'+
                      '<div  class="form-group" style="padding-left: 15px">'+
                          '<div class="col-md-3">'+
                              '<div class="form-group form-md-line-input">'+
                                  '<input type="text" class="form-control chargeValue" name="chargeValue" value="'+ n['chargeValue'] +'">'+
                                  '<label for="form_control_1">Value</label>'+
                                  '<span class="help-block"></span>'+
                              '</div>'+
                          '</div>'+
                          '<div class="col-md-3">'+
                              '<div class="form-group form-md-line-input">'+
                                  '<input type="text" class="form-control chargeDate" name="chargeDate" value="'+ n['chargeDate'] +'">'+
                                  '<label for="form_control_1">Date </label>'+
                                  '<span class="help-block"></span>'+
                              '</div>'+
                          '</div>'+
                          '<div class="col-md-7">'+
                              '<div class="form-group form-md-line-input">'+
                                  '<input type="text" class="form-control chargeDescription" name="chargeDescription" value="'+ n['chargeDescription'] +'">'+
                                  '<label for="form_control_1">Description</label>'+
                                  '<span class="help-block"></span>'+
                              '</div>'+
                          '</div>'+
                           '<div class="col-md-2 text-left">'+
                              '<a href="javascript:;" class="btn btn-outline red cms-redo-charges"><span class="icon-close"></span></a>'+
                          '</div>'+
                      '</div>'+
                  '</div>'
                );
              });

              $('.totalCharges').html(chargeTotal);
              var table = $('.clientPropertyPaymentHistoryTable').DataTable();
              table.clear().draw();
              table.rows.add(response.propertyPaymentHistory).draw();
              $('.listOfPaymentHistory').empty();
              $.each(response.propertyPaymentHistory,function(i,n){

                $('.listOfPaymentHistory').append(
                  '<tr>'+
                    '<td>'+n[0]+'</td>'+
                    '<td>'+n[1]+'</td>'+
                    '<td>'+n[2]+'</td>'+
                    '<td>'+n[3]+'</td>'+
                    '<td>'+(n[4] == '0.00' ? '': ("&#x20B1; "+accounting.format(n[4],2)))+'</td>'+
                    '<td>'+(n[5] == '0.00' ? '': ("&#x20B1; "+accounting.format(n[5],2)))+'</td>'+
                    '<td>'+(n[6] == '0.00' ? '': ("&#x20B1; "+accounting.format(n[6],2)))+'</td>'+
                    '<td>'+n[7]+'</td>'+
                    // '<td>'+("&#x20B1; "+accounting.format(n[6],2))+'</td>'+
                  '</tr>'
                  );
              });

              $('.listOfPaymentHistory').prepend(` <tr>
                                <th class="cms-num">#</th>
                                <th>OR #</th>
                                <th>PAYMEMT FOR MONTH</th>
                                <th>DATE OF PAYMENT</th>
                                <th>BANK/REMITTANCE</th>
                                <th>CASH</th>
                                <th>BALANCE</th>
                                <th>MODE OF PAYMENT</th>
                            </tr>`);

            }
            else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }

            $('.clientPropertiesList').select2({
              placeholder: "Select a project",
              allowClear: true
            }); 
            hideLoading();
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }


    function btnUpdateClientClicked(id){
      showLoading();
      $.ajax({
          type: 'POST',
          url: apiUrl+"/dashboard/client/updateThis",
          data : {
            id : id,
            update_clientFname : $('.update_clientFname').val(),
            update_clientLname : $('.update_clientLname').val(),
            update_clientMname : $('.update_clientMname').val(),
            update_clientAddress : $('.txt_clientAddress').val(),
            update_clientContactNumber : $('.txt_number').val(),
            update_Remarks : $('.txt_remarks').val(),
            update_clientEmail : $('.txt_emai').val()
          },
          cache: false,
          async: true,
          success: function(response) { 
            hideLoading();

            if(response.success){
              openProfile(id);
              setTimeout(function(){swal("Success!", response.message, "success");},0);
              window.clearTimeout(loadInBackground);
            }
            else{
              setTimeout(function(){swal("Warning!", response.message, "warning");},0);
              window.clearTimeout(loadInBackground);
            }

            
          },
          error: function(err){
            hideLoading();
            setTimeout(function(){swal("Error!", err.statusText, "error");},0);
            window.clearTimeout(loadInBackground);
          }
      });
    }

    function clientagentListValueChange()
    {
      showLoading();
      var id = $(this).val();

      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/agent/getThisAgent",
          data : {id : id},
          cache: false,
          async: true,
          success: function(response) { 
            
            hideLoading();
            if(response.success){
              $('.addClient_agent_name').val(response.data[0].update_agentFname+ " "+ response.data[0].update_agentLname ).focus();
              $('.addClient_agent_contactnumber').val(response.data[0].update_agentContactNumber).focus();
              $('.addClient_agent_address').val(response.data[0].update_agentAddress).focus();
              $('.addClient_agent_email').val(response.data[0].update_agentEmail).focus();
              $('.addClient_agent_position').val(response.data[0].agentPositionName).focus();
              var pos = response.data[0].agentPositionList;

              $('.addClientagent').attr('data-agentPosition',pos);

              $('.uplineClientAgentContainers').remove();

              if(pos == 2){
                $('.clientAgentPercentageCalculatorContainer').append(getPercentageCalculationForm(
                  (response.data[0].salesDirectorAgentName+ " (SDH) %"),'salesDirectorAgentCommission'));
                  $('.directAgentCommission').val(12);
                  $('.salesDirectorAgentCommission').val(3);
              
              }else if(pos == 3){
              
                $('.clientAgentPercentageCalculatorContainer').append(getPercentageCalculationForm(
                  (response.data[0].salesManagerAgentName+ " (SD) %"),'salesManagerAgentCommission'));
                $('.clientAgentPercentageCalculatorContainer').append(getPercentageCalculationForm(
                  (response.data[0].salesDirectorAgentName+ " (SDH) %"),'salesDirectorAgentCommission'));

                  $('.directAgentCommission').val(10);
                  $('.salesDirectorAgentCommission').val(3);
                  $('.salesManagerAgentCommission').val(2);
              
              }else if(pos == 4){

                $('.clientAgentPercentageCalculatorContainer').append(getPercentageCalculationForm(
                  (response.data[0].unitManagerAgentName  + " (UM) %"),'unitManagerAgentCommission'));
                $('.clientAgentPercentageCalculatorContainer').append(getPercentageCalculationForm(
                  (response.data[0].salesManagerAgentName+ " (SD) %"),'salesManagerAgentCommission'));
                $('.clientAgentPercentageCalculatorContainer').append(getPercentageCalculationForm(
                  (response.data[0].salesDirectorAgentName+ " (SDH) %"),'salesDirectorAgentCommission'));

                  $('.directAgentCommission').val(5);
                  $('.salesDirectorAgentCommission').val(3);
                  $('.salesManagerAgentCommission').val(2);
                  $('.unitManagerAgentCommission').val(5);

              }else if(pos == 5){

                $('.clientAgentPercentageCalculatorContainer').append(getPercentageCalculationForm(
                  (response.data[0].managerName  + " (M) %"),'managerAgentCommission'));
                $('.clientAgentPercentageCalculatorContainer').append(getPercentageCalculationForm(
                  (response.data[0].unitManagerAgentName  + " (UM) %"),'unitManagerAgentCommission'));
                $('.clientAgentPercentageCalculatorContainer').append(getPercentageCalculationForm(
                  (response.data[0].salesManagerAgentName+ " (SD) %"),'salesManagerAgentCommission'));
                $('.clientAgentPercentageCalculatorContainer').append(getPercentageCalculationForm(
                  (response.data[0].salesDirectorAgentName+ " (SDH) %"),'salesDirectorAgentCommission'));

                  $('.directAgentCommission').val(5);
                  $('.managerAgentCommission').val(3);
                  $('.salesDirectorAgentCommission').val(3);
                  $('.salesManagerAgentCommission').val(2);
                  $('.unitManagerAgentCommission').val(2);

              }else{
                $('.directAgentCommission').val(15);
              }

              $('.directAgentCommission, .salesDirectorAgentCommission ,.salesManagerAgentCommission ,.unitManagerAgentCommission ,.managerAgentCommission').focus();

              calculatePercentageIsEqualTo10Validation(false);
            }

            else{
              $('.addClient_agent_name').val('').focus();
              $('.addClient_agent_contactnumber').val('').focus();
              $('.addClient_agent_address').val('').focus();
              $('.addClient_agent_email').val('').focus();

              swal("Warning!", response.message, "warning");
            }
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }


    function getPercentageCalculationForm(clientName,inputClassName){
      return (
        '<div class="col-md-3 uplineClientAgentContainers">'+
          '<div class="form-group form-md-line-input form-md-floating-label ">'+
              '<input type="text"  class="form-control '+inputClassName+'" name="'+inputClassName+'" id="form_control_1" value="0">'+
              '<label for="form_control_1" >'+clientName+'</label>'+
              '<span class="help-block"></span>'+
          '</div> '+
      '</div>'
        );
    }


    function clientPropertyListValueChange()
    {
      showLoading();
      var id = $(this).val();

      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/property/getInfo",
          data : {id : id},
          cache: false,
          async: true,
          success: function(response) { 
            
            hideLoading();
            if(response.success ){
              $('.addClientprop_address').val(response.datas[0].update_propertyAddress).focus();
              $('.addClientprop_monthlyamortization').val(response.datas[0].update_propertyMonthlyAmoritzation).focus();
              $('.addClientprop_lotarea').val("Block "+response.datas[0].update_blockAreaLocation+" Lot "+response.datas[0].update_lotAreaLocation).focus();
              $('.addClientprop_sqm').val(response.datas[0].update_sqmAreaLocation).focus();
              $('.addClientprop_originalprice').val(response.datas[0].update_propertyPrice).focus();
              $('.addClientprop_model').val(response.datas[0].update_propertyModel).focus();
            }
            else{
              $('.addClientprop_address').val('').focus();
              $('.addClientprop_monthlyamortization').val('').focus();
              $('.addClientprop_lotarea').val('').focus();
              $('.addClientprop_sqm').val('').focus();
              $('.addClientprop_originalprice').val('').focus();
              $('.addClientprop_model').val('').focus();
              swal("Warning!", response.message, "warning");
            }
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }

    

    function printContent(){

      // var printPage = document.body.innerHTML;
      // var printPageContent = document.getElementById('printReceipt').innerHTML;
      // document.body.innerHTML = printPageContent;
      // window.print();
      // document.body.innerHTML = printPage;
      // location.reload(true);

      var html="<html>";
      // html+="<head><style>@page {size: auto;margin: 0cm; padding: 0; width: 100%; height: 100%; background-color: white;}@media print {#officialReceipt { page-break-after: always; width: 100%; margin: 0; padding: 0; background-color: white;} body , body > *{width: 100%; height:100%; margin: 0; padding: 0; background-color: white;}}</style></head>";
      html+= document.getElementById('printReceipt').innerHTML;
      html+="</html>";
      var printWin = window.open();
      printWin.document.write(html);
      printWin.document.close();
      printWin.focus();
      printWin.print();
      printWin.close();

  }




    function initalizeAddClientSummary(){
      
      var additionalChargesList = $('.additionalChargesDetails');
      var grandTotal = 0;
      var addtionalCharges = 0;



      var addClientAdditionalChargesSummaryContainerTemp =  "";
      var addClientAdditionalChargesSummaryContainer2Temp =  "";
      


      $.each(additionalChargesList,function(){
        var additionalCharges = $(this);
        var chargeValue = additionalCharges.find('.chargeValue').val();
        var chargeDescription = additionalCharges.find('.chargeDescription').val();

        addtionalCharges += parseFloat(chargeValue);

        addClientAdditionalChargesSummaryContainerTemp += (  
            '<div>'+
                '<div class="form-group padding-left-23">'+
                   '<label class="from-label sbold uppercase">Value: <span class="text-normal addClient_summary_propertytotalAdditionalChargeValue">&#8369; '+accounting.format(chargeValue,2)+'</span></label>'+
                '</div>'+
                '<div class="form-group padding-left-23">'+
                   '<label class="from-label sbold uppercase">Description : <span class="text-normal addClient_summary_propertyAdditionalChargeDescription">'+chargeDescription+'</span></label>'+
                '</div>'+
            '</div>'
            );


        addClientAdditionalChargesSummaryContainer2Temp += (
          '<div>'+
              '<div class="">'+
                  '<label class="form-label cms-bold txt-margin-left">Value : <span style="font-weight: normal;" class="addClient_summary_propertytotalAdditionalChargeValue">&#8369; '+accounting.format(chargeValue,2)+'</span></label>'+
              '</div>'+
              '<div class="">'+
                  '<label class="form-label cms-bold    txt-margin-left">Description : <span style="font-weight: normal;" class="addClient_summary_propertyAdditionalChargeDescription">'+chargeDescription+'</span></label>'+
              '</div>'+
          '</div>'
        );

      });



      var addClient_summary_propertyList = $('.addClient_summary_propertyList');
      var addClient_summary_propertyList2 = $('.addClient_summary_propertyList2');
      addClient_summary_propertyList2.empty();
      addClient_summary_propertyList.empty();


      var prpertiesSelected = $('.selectecPropertiesContainer .selectedProperty');

      

      var propertyName = $('.addClientprop_property option:selected').text();
      var addClient_summary_cleintName = $('').val();
      var addClient_summary_date;

      $.each(prpertiesSelected,function(i,n){

        var propertySelected = $(this);

        var propertyTypeID = propertySelected.attr('data-propertytypeid');
        var model = propertySelected.attr('data-model');

        var sqm = propertySelected.attr('data-sqm');
        var pricePerm2 = propertySelected.find('.addClientprop_pricePerm2').val();
        var planTerms = propertySelected.find('.addClientprop_planterms').val();

        
        var downpayment = propertySelected.find('.addClientprop_downpayment').val().trim();  
        


        var propertyContPrice = (parseFloat(sqm) * parseFloat(pricePerm2));
        propertyContPrice += addtionalCharges;
        grandTotal += propertyContPrice;

        if(downpayment != ""){
          if(validateNumberAndDot(downpayment)){
            downpayment = parseFloat(downpayment);
          }
          else
            downpayment = 0;
        }
        else
          downpayment = 0;


        grandTotal -= downpayment;




        var prop = $(getPropertySummary1(i));

        prop.find('.addClient_summary_propertyname').html(propertySelected.find('.addClientprop_propertyName').val());
        prop.find('.addClient_summary_propertyAddress').html(propertySelected.find('.addClientprop_address').val());
        prop.find('.addClient_summary_propertyLocation').html(propertySelected.find('.addClientprop_lotarea').val());  // phase 1 block 1 lot 1

        if(propertySelected.attr('data-propertytypeid') == 1){
          prop.find('.addClient_summary_propertyModel').html('NONE');
          prop.find('.addClient_summary_propertyType').html('Raw Lot');
        }
        else{
          prop.find('.addClient_summary_propertyType').html('House and Lot');
          prop.find('.addClient_summary_propertyModel').html(propertySelected.find('.addClientprop_model').val());
        }



        prop.find('.addClient_summary_propertyArea').html(sqm +' SQM<sup>2</sup>');
        prop.find('.addClient_summary_propertyPricePerm2').html(propertySelected.find('.addClientprop_pricePerm2').val());
        prop.find('.addClient_summary_propertyplanTerms').html(propertySelected.find('.addClientprop_planterms').val()+' <i class="fa fa-calendar" aria-hidden="true"></i>');

        prop.find('.addClient_summary_propertypTotalCharges').html(accounting.format(addtionalCharges,2));
        prop.find('.addClientAdditionalChargesSummaryContainer').html(addClientAdditionalChargesSummaryContainerTemp);    

        prop.find('.addClient_summary_propertyTotalCtractPrice').html(accounting.format(propertyContPrice,2)); 
        prop.find('.addClient_summary_propertyMonthlyAmortization').html(accounting.format(((propertyContPrice - downpayment)/parseInt(planTerms)),2)); 

        prop.find('.addClient_summary_propertyDownpayment').html(accounting.format(downpayment,2)); 
        prop.find('.addClient_summary_propertyBalance').html(accounting.format((propertyContPrice - downpayment),2)); 

        addClient_summary_propertyList.append(prop);



        var prop2 = $(getPropertySummary2(i));

        prop2.find('.addClient_summary_propertyname').html(propertySelected.find('.addClientprop_propertyName').val());
        prop2.find('.addClient_summary_propertyAddress').html(propertySelected.find('.addClientprop_address').val());
        prop2.find('.addClient_summary_propertyLocation').html(propertySelected.find('.addClientprop_lotarea').val());  // phase 1 block 1 lot 1

        if(propertySelected.attr('data-propertytypeid') == 1){
          prop2.find('.addClient_summary_propertyModel').html('NONE');
          prop2.find('.addClient_summary_propertyType').html('Raw Lot');
        }
        else{
          prop2.find('.addClient_summary_propertyType').html('House and Lot');
          prop2.find('.addClient_summary_propertyModel').html(propertySelected.find('.addClientprop_model').val());
        }

        prop2.find('.addClient_summary_propertyArea').html(sqm +' SQM<sup>2</sup>');
        prop2.find('.addClient_summary_propertyPricePerm2').html(propertySelected.find('.addClientprop_pricePerm2').val());
        prop2.find('.addClient_summary_propertyplanTerms').html(propertySelected.find('.addClientprop_planterms').val()+' <i class="fa fa-calendar" aria-hidden="true"></i>');

        prop2.find('.addClient_summary_propertypTotalCharges').html(accounting.format(addtionalCharges,2));
        prop2.find('.addClientAdditionalChargesSummaryContainer').html(addClientAdditionalChargesSummaryContainerTemp);    

        prop2.find('.addClient_summary_propertyTotalCtractPrice').html(accounting.format(propertyContPrice,2)); 
        prop2.find('.addClient_summary_propertyMonthlyAmortization').html(accounting.format(((propertyContPrice - downpayment) / parseInt(planTerms)),2)); 

        prop2.find('.addClient_summary_propertyDownpayment').html(accounting.format(downpayment,2)); 
        prop2.find('.addClient_summary_propertyBalance').html(accounting.format((propertyContPrice - downpayment),2)); 

        prop2.find('.addClientAdditionalChargesSummaryContainer2').html(addClientAdditionalChargesSummaryContainer2Temp); 

        addClient_summary_propertyList2.append(prop2);

      });

      $('.addClient_summary_propertyOverAllTotal').html(accounting.format(grandTotal,2));
      $('.addClient_summary_encoderName').html($('.activeUserFullname').html()); 
      $('.addClient_summary_agentName').html($('.addClientagent option:selected').text()); 

      $('.addClient_summary_cleintName').html($('.addMoreClientPropertyClientName').html()); 
      // $('.addClient_summary_date').html(new Date('yyyy-mm-dd')); 


    }






    function getPropertySummary1(i){
      return (
        '<div class="row">'+
          '<div class="cms-container">'+
              '<h4 class="h4-margin cms-left-pddng" style="margin-top:49px;">Property '+(i+1)+'</h4>'+
              '<div class="col-md-8 col-sm-12">'+
                  '<h3>Property Details</h3>'+
                  '<div class="form-group padding-left-23">'+
                     '<label class="from-label sbold uppercase">Name : <span class="text-normal addClient_summary_propertyname"> Villa Monte Marea</span></label>'+
                  '</div>'+
                  '<div class="form-group padding-left-23">'+
                     '<label class="from-label sbold uppercase">Address :<span class="text-normal addClient_summary_propertyAddress"> Matina Aplaya, Davao City  </span></label>'+
                  '</div>'+
                  '<div class="form-group padding-left-23">'+
                     '<label class="from-label sbold uppercase">Location : <span class="text-normal addClient_summary_propertyLocation"> Blk 1 Lt 1 / Blk 1 Lt 2</span></label>'+
                  '</div>'+
                  '<div class="form-group padding-left-23">'+
                     '<label class="from-label sbold uppercase">Type : <span class="text-normal addClient_summary_propertyType">Raw Lot</span></label>'+
                  '</div>'+
                  '<div class="form-group padding-left-23">'+
                     '<label class="from-label sbold uppercase">Model : <span class="text-normal addClient_summary_propertyModel">Payag</span></label>'+
                  '</div>'+
                  '<div class="form-group padding-left-23">'+
                     '<label class="from-label sbold uppercase">Area : <span class="text-normal addClient_summary_propertyArea">200 SQM<sup>2</sup></span></label>'+
                  '</div>'+
                  '<div class="form-group padding-left-23">'+
                     '<label class="from-label sbold uppercase">Price per SQM<sup>2</sup>: <span class="text-normal addClient_summary_propertyPricePerm2"> 1500.00</span></label>'+
                  '</div>'+
                 
                  '<h3>Additional Charges <label class="addClient_summary_propertypTotalCharges">0.00</label></h3>'+
                  '<div class="addClientAdditionalChargesSummaryContainer">'+
                  '</div>'+
              '</div>'+
              '<div class="col-md-4 col-sm-12 text-right">'+

                   '<h3>Plan Terms</h3>'+
                  '<div class="form-group padding-right-23">'+
                     '<label class="from-label sbold addClient_summary_propertyplanTerms">28 Month</label>'+
                  '</div>'+

                  '<h3>Contract Price</h3>'+
                  '<div class="form-group padding-right-23">'+
                     '<label class="from-label sbold uppercase addClient_summary_propertyTotalCtractPrice">P400,000.00</label>'+
                  '</div>  '+
                  '<h3>Monthly Amortization</h3>'+
                  '<div class="form-group padding-right-23">'+
                     '<label class="from-label sbold addClient_summary_propertyMonthlyAmortization">P25,000.00</label>'+
                  '</div>'+
                  '<h3>Downpayment</h3>'+
                  '<div class="form-group padding-right-23 ">'+
                     '<label class="from-label sbold uppercase addClient_summary_propertyDownpayment">P300,000.00</label>'+
                  '</div> '+
                  '<h3>Balance</h3>'+
                  '<div class="form-group padding-right-23">'+
                     '<label class="from-label sbold uppercase addClient_summary_propertyBalance">P300,000.00</label>'+
                  '</div>'+
              '</div>'+
              '<div class="col-md-12">'+
                  '<hr>'+
              '</div>'+
          '</div>'+
        '</div>'
      );
    }


    function getPropertySummary2(i){
      return (
        '<div class="property-container">'+

          '<div class="cms-col-md-12">'+
            '<h4 class="h4-margin cms-left-pddng">Property'+(i+1)+'</h4>'+
          '<div class="cms-col-md-6">'+
              '<div class="">'+
                  '<label class="form-label cms-bold txt-margin-left">Name: <span style="font-weight: normal;" class="addClient_summary_propertyname">Villa Monte Marea</span></label>'+
              '</div>'+
              '<div class="">'+
                  '<label class="form-label cms-bold txt-margin-left"> Address: <span style="font-weight: normal;" class="addClient_summary_propertyAddress">Matina Aplaya, Davao City</span></label>'+
              '</div>'+
              '<div class="">'+
                  '<label class="form-label cms-bold txt-margin-left"> Location: <span style="font-weight: normal;" class="addClient_summary_propertyLocation">Blk 1 Lt 1 / Blk 1 Lt 2</span></label>'+
              '</div>'+
              '<div class="">'+
                  '<label class="form-label cms-bold txt-margin-left"> Type: <span style="font-weight: normal;" class="addClient_summary_propertyType">Raw Lot</span></label>'+
              '</div>'+
              '<div class="">'+
                  '<label class="form-label cms-bold txt-margin-left"> Model: <span style="font-weight: normal;" class="addClient_summary_propertyModel">Payag</span></label>'+
              '</div>'+
              '<div class="">'+
                  '<label class="form-label cms-bold txt-margin-left"> Area: <span style="font-weight: normal;" class="addClient_summary_propertyArea">1255</span></label>'+
              '</div>'+
              '<div class="">'+
                  '<label class="form-label cms-bold txt-margin-left"> Price per SQM<sup>2</sup>: <span style="font-weight: normal;" class="addClient_summary_propertyPricePerm2">15000</span></label>'+
              '</div>'+
             
              '<h4 class="padding-left-25">Additional Charges <label class="addClient_summary_propertypTotalCharges">0.00</label></h4>'+
              '<div class="addClientAdditionalChargesSummaryContainer2">'+
              '</div>'+
          '</div>'+
          '<div class="cms-col-md-6 cms-text-right ">'+

              '<h4 class="cms-hr-margin-top h4-margin">Plan Terms</h4>'+
              '<div class="">'+
                  '<label class="form-label cms-bold txt-margin-left addClient_summary_propertyplanTerms"> 28 Months</label>'+
              '</div>'+

              '<div class="">'+
                  '<label class="form-label cms-bold txt-margin-left">Contract Price : <span style="font-weight: normal;" class="addClient_summary_propertyTotalCtractPrice">P400,000.00</span></label>'+
              '</div>'+
              '<h4 class="h4-margin">Monthly Amortization</h4>'+
              '<div class="">'+
                  '<label class="form-label cms-bold txt-margin-left"><span style="font-weight: normal;" class="addClient_summary_propertyMonthlyAmortization getMonthlyAmortization"></span></label>'+
              '</div>'+
          '</div>'+
          '<div class="cms-col-md-6 cms-text-right cms-downpayment-margin-top">'+
              '<label class="form-label cms-bold ">Downpayment : <span style="font-weight: normal;" class="addClient_summary_propertyDownpayment">P100,000.00</span></label>'+
          '</div>'+
          '<div class="cms-col-md-6 cms-text-right ">'+
              '<label class="form-label cms-bold    txt-margin-left">Balance : <span style="font-weight: normal;" class="addClient_summary_propertyBalance">P300,000.00</span></label>'+
          '</div>'+
          '<div class="cms-col-md-12">'+
              '<hr>'+
          '</div>'+
      '</div>'
      );
    }




    function postThisClient(){

      var addClient_fname                   = $('.addClient_fname').val().trim();
      var addClient_lname                   = $('.addClient_lname').val().trim();
      var addClient_mname                   = $('.addClient_mname').val().trim();
      var addClient_address                 = $('.addClient_address').val().trim();
      var addClient_contactnumber           = $('.addClient_contactnumber').val().trim();
      var addClient_email                   = $('.addClient_email').val().trim();
      var setChargeDate                     = $('.addClient_email').val().trim();

      var propertySelected = [];


      $.each($('.selectecPropertiesContainer .selectedProperty'),function(){
        var parentContainer = $(this);
        var parentID = parentContainer.attr('data-parentID');
        var block = parentContainer.attr('data-block');
        var lot = parentContainer.attr('data-lot');
        var phaseNumber = parentContainer.attr('data-phaseNumber');

        var ornumber                             = parentContainer.find('.addClientprop_ornumber').val().trim();
        var modeOfpayment                        = parentContainer.find('.clientmodeOfPayment').val();
        var clientremitanceCentername            = parentContainer.find('.clientremitanceCentername').val();

        var addClientprop_pricePerm2             = parentContainer.find('.addClientprop_pricePerm2').val();
        var addClientprop_property               = parentContainer.find('.addClientprop_property').val();
        var addClientprop_downpayment            = accounting.unformat(parentContainer.find('.addClientprop_downpayment').val().trim());
        var addClientDueDate                     = parentContainer.find('.addClientDueDate').val();
        var addClientprop_dateapplied            = parentContainer.find('.addClientprop_dateapplied').val().trim();
        var addClientprop_planterms              = accounting.unformat(parentContainer.find('.addClientprop_planterms').val().trim());

        var commissionRelease                    
        if(parentContainer.find('.commissonReleasedSched').val().trim() == 0){
              commissionRelease     = parentContainer.find('.commissonReleasedSchedInput').val().trim();
        }else{
              commissionRelease     = parentContainer.find('.commissonReleasedSched').val().trim();
        }
        var addCom                  = parentContainer.find('.addComission').val().trim();
        if (addCom === '') {
          addCom = 0; 
        }
// ew

        propertySelected.push({
          parentID                            : parentID,
          ornumber                            : ornumber,
          phaseNumber                         : phaseNumber,
          block                               : block,
          lot                                 : lot,
          modeOfpayment                       : modeOfpayment,
          clientremitanceCentername           : clientremitanceCentername,
          addClientprop_property              : addClientprop_property,
          addClientprop_pricePerm2            : addClientprop_pricePerm2,
          addClientprop_downpayment           : addClientprop_downpayment,
          addClientDueDate                    : addClientDueDate,
          addClientprop_dateapplied           : addClientprop_dateapplied,
          addClientprop_planterms             : addClientprop_planterms,
          comission_release                   : commissionRelease,
          add_commission                      : addCom
        });

      });


      // const spouseList = $('.spouse_details');

      // $.each(spouse_details, function (i, n) {
      //   console.log($(this));
      // })

      var directCom          = $('.directAgentCommission').val();
      var UnitManagerCom     = $('.unitManagerAgentCommission').val();
      var SalesManagerCom    = $('.salesManagerAgentCommission').val();
      var SalesDirectorCom   = $('.salesDirectorAgentCommission').val();
      var managerCom         = $('.managerAgentCommission').val();
      // var CommissisonRelease = $('.commissonReleasedSched').val();
      // var addCom             = $('.addComission').val();


      //spouse
      const spouse = getSpouseList();;
      // beneficiary
      const beneficiary = getBeneficiaries();
      

      var addClientagent                    = $('.addClientagent').val().trim();

      var data = {
          addClient_fname                     : addClient_fname,
          addClient_lname                     : addClient_lname,
          addClient_mname                     : addClient_mname,
          addClient_address                   : addClient_address,
          addClient_contactnumber             : addClient_contactnumber,
          addClient_email                     : addClient_email,

          propertySelected                    :propertySelected,
          chargesDetails                      :chargesDetails,
          addClientagent                      :addClientagent,
          commissionPercentage                :{
                                                  directCom         : directCom,
                                                  UnitManagerCom    : UnitManagerCom,
                                                  SalesManagerCom   : SalesManagerCom,
                                                  SalesDirectorCom  : SalesDirectorCom,  
                                                  ManagerCom        : managerCom  
                                              },
          spouse                              :spouse,                                
          benif                               :beneficiary                                
        };



        showLoading();
        $.ajax({
          type: 'POST',
          url: apiUrl+"/dashboard/client/addThis",
          data : data,
          cache: false,
          async: true,
          success: function(response) { 
              // console.log(response);

              if(response.success){

                $('.addClient_summary_date').html(response.dateNow);
                var content = document.getElementById('printReceipt').innerHTML;

                bootbox.dialog({
                  message: content,
                  title: "Property Summary",
                  // onEscape: function() {  },
                  // backdrop: true,
                  buttons: {
                    success: {
                      label: "Print",
                      className: "green btn-outline",
                      callback: function() {
                        var html="<html>";
                        // html+="<head><style>@page {size: auto;margin: 0cm; padding: 0; width: 100%; height: 100%; background-color: white;}@media print {#officialReceipt { page-break-after: always; width: 100%; margin: 0; padding: 0; background-color: white;} body , body > *{width: 100%; height:100%; margin: 0; padding: 0; background-color: white;}}</style></head>";
                        html+= content;
                        html+="</html>";
                        var printWin = window.open();
                        printWin.document.write(html);
                        printWin.document.close();
                        printWin.focus();
                        printWin.print();
                        printWin.close();
                      }
                    }
                  }
                });

                openProfile(response.id);
                // openClientListContainer();
                window.clearTimeout(loadInBackground);
              }
              else{
                hideLoading();
                swal("Warning!", response.message, "warning");
                $.each(response.variables,function(i,n){
                  $('.'+n.key).parent().addClass('has-error');
                  $('.'+n.key).parent().find('.help-block').html(n.errorMessage)
                });
                window.clearTimeout(loadInBackground);
              }
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
            window.clearTimeout(loadInBackground);
          }
        });
    }

    function openClientProfileOfthis(){
      var id = $(this).attr('data-id');
      set_clientID = id;

      getTemplate(baseTemplateUrl, 'clientProfile.html', function(render) {
        $( ".dashboardPage" ).empty();
        var renderedhtml = render({data: ""});
        $(".clientProfileContainer").html(renderedhtml);
        $('.clientProfileContainer').removeClass('hidden');
      });
       
      openProfile(id);
      tableclientPropertyPaymentHistoryTable();
      $('.date-picker').datepicker();
      $('.chargeValue').datepicker({
        format: 'yyyy-mm-dd'
      })

    }


    function openProfile(id){
      if(id == null || id == "" || id == '0'){
        swal("Warning!", 'Invalid ID ', "warning");
        return false;
      }

      showLoading();
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/client/getInfo",
          data : {id : id},
          cache: false,
          async: true,
          success: function(response) { 
              
              var id = 0;
              var imageName = "";
              var clientFName = "",clientLName = "";
              const role = response.role;
              if (role == "Agent") {
                $('.tab-to-remove').empty();
              }

              if(response.success){            
                    hideLoading();
                    $.each(response.datas[0], function(i,n){
                      if(i == 'clientID')
                        id = n;
                      else if(i == 'update_clientFname'){
                        clientFName = n;
                      }
                      else if(i == 'update_clientLname'){
                        clientLName = n; 
                      }
                      else if(i == 'imagePath')
                        imageName = n;

                      $('.'+i).val(n);
                      $('.'+i).html(n);
                      $('.'+i).focus();
                    });

                    if (JSON.parse(response.datas[0].spouseList) !== '') {
                      setSpouseList(JSON.parse(response.datas[0].spouseList), role);
                      set_spouseList = JSON.parse(response.datas[0].spouseList);                      
                    }
                      
                    if (JSON.parse(response.datas[0].benifList) !== '') {
                    
                        setBeneficiaryList(JSON.parse(response.datas[0].benifList), role);
                        set_beneficiaries = JSON.parse(response.datas[0].benifList);
                       
                    }
                    

                    $('.clientPropertiesList').empty();
                    $.each(response.cpList,function(i,n){
                      $('.clientPropertiesList').append('<option value="'+n.clientPropertyID+'">'+n.propertyName+'</option>');

                      if (n.propStat == 0) {
                         $('.propertyStatus').html('Property Forfeited');
                         $('.btnComRelSched').attr("Disabled","btnComRelSched");
                         $('.btnComRelSched').removeClass('btnComRelSched');
                         $('.btnAddCom').attr("Disabled","btnAddCom");
                         $('.btnAddCom').removeClass('btnAddCom');
                         $('.btnaddCharges').attr("Disabled","btnaddCharges");
                         $('.btnaddCharges').removeClass('btnaddCharges');
                         $('.btnForfietThisProperty').attr("Disabled","btnForfietThisProperty");
                         $('.btnForfietThisProperty').removeClass('btnForfietThisProperty');

                      }
                       
                    });

                    if(imageName == 'no_image.jpg'){
                      $('.clientImagePic').attr('src','images/no_image.jpg');  
                    }
                    else
                      $('.clientImagePic').attr('src','api/csm_v1/public/uploads/client/'+id+'/'+imageName);


                    $('.imageClientID').val(id);

                    $('.btnAddMoreProperty').attr('data-clientID',id);
                    $('.btnAddMoreProperty').attr('data-clientFullName', (clientFName+" "+clientLName));


                    $('.btnUpdateClient').attr('data-id',response.datas[0].clientID);
                    $('.update_clientFullname').html(response.datas[0].update_clientFname+' '+response.datas[0].update_clientMname+' '+response.datas[0].update_clientLname);
              }
              else{
                hideLoading();
                swal("Warning!", response.message, "warning");
              }
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });

    }

    function setSpouseList(data, role) {
      $('#spouse_list').empty();
      let spouse = ``;
      $.each(data, function (i, n) {
          spouse = ` <div class="alert alert-block alert-info fade in" data-id="`+ i +`">
                        <!-- <button type="button" class="close" data-dismiss="alert"></button> -->
                        <h4 class="alert-heading"> 
                            <strong> 
                              <span class="fa fa-user"> </span>
                              <a href="javascript:;" class="spouce_file">
                                 <span class="spouse_name"> 
                                      `+ n.fname +` `+ n.mname + ` `+  n.lname +` 
                                 </span> 
                              </a>
                            </strong> 
                        </h4>
                        <p> 
                            <strong>
                                <span class="fa fa-home"></strong>
                            </span> 
                            <i class="spouse_address">
                                  `+ n.address +`
                            </i>
                        </p>
                        <div class="row">
                            <div class="col-md-4 text-center">
                                <p><span class="fa fa-phone"></span> 
                                    <i class="spouse_num"> 
                                        `+ n. cnum +` 
                                    </i>
                                </p>
                            </div>
                            <div class="col-md-4 text-center">
                                <p><span class="fa fa-calendar"></span>
                                    <i class="spouse_bdate"> 
                                        `+ formatDate(n.bdate) +` 
                                    </i>
                                </p>
                            </div>
                            <div class="col-md-4 text-center">
                                <p><span class="fa fa-life-ring"></span>
                                    <i class="spouse_civilStat">
                                        `+ n.civilstat +`
                                    </i>
                                </p>
                            </div>
                        </div>
                    </div>`;

          $('#spouse_list').append(spouse);
      });
    }

    function setBeneficiaryList(data, role) {
      $('#benif_list_details').empty();
      const benif = "";
      let beneficiary = "";
      $.each(data, function (i,n) {
        if (role == "Agent") {
          beneficiary = `<li class="mt-list-item benif_name">
                            <div class="list-item-content benif_content">
                                <h3 class="uppercase">
                                    <a href="javascript:;">`+ n +`</a>
                                </h3>
                            </div>
                        </li>`;          
        }else{
           beneficiary = `<li class="mt-list-item benif_name">
                            <div class="list-item-content benif_content">
                                <h3 class="uppercase">
                                    <a href="javascript:;" class="benif_a" data-val="`+ n +`" data-id="`+i+`">`+ n +`</a>
                                </h3>
                            </div>
                        </li>`;      
        }

        $('#benif_list_details').append(beneficiary);
      });
    }




    function openClientListContainer(){
      showLoading();
      var navParent = $(this);
    
      getTemplate(baseTemplateUrl, 'clientList.html', function(render) {
          $( ".dashboardPage" ).empty();
          var renderedhtml = render({data: ""});
          $(".clientListContainer").html(renderedhtml);
          $('.clientListContainer').removeClass('hidden');

          $('.page-sidebar-menu').find('.nav-item').removeClass('active open');

          if(navParent.parent().hasClass('nav-item')){
            navParent.parent().addClass('active open');
          }

          if(navParent.parent().parent().parent().hasClass('nav-item')){
            navParent.parent().parent().parent().addClass('active open');
          }

          tableclientListTable();
          getAllClients();
          getPropertyList();
          hideLoading();

          $('#slt_propertyParentList, #slt_propertyList').select2();
      });
    }


    function getPropertyList(argument) {
       $.ajax({ 
          type: 'GET',
          url: apiUrl+"dashboard/client/getAllProperParentList",
          cache: false,
          async: true,
          success: function(response){
              if (response.success==true) {
                  if (response.role == "Agent") {
                      $('#prop_selection').addClass('hidden');
                  }else{
                    $('#slt_propertyParentList').select2({
                      data: response.data
                    });
                  }

              }else{
                  swal("Warning", response.message, 'warning');
              }
                 
          },
          error: function(err){
              hideLoading();
              swal("Error!", err.statusText, "error");
          }
      })
    }


    function openAddClientContainer(){
      showLoading();
      var navParent = $(this);
      
    
      getTemplate(baseTemplateUrl, 'addclient.html', function(render) {
          $( ".dashboardPage" ).empty();
          var renderedhtml = render({data: ""});
          $(".addClientContainer").html(renderedhtml);
          $('.addClientContainer').removeClass('hidden');
          $('select').select2();
          $('.date-picker').datepicker();

          $('.page-sidebar-menu').find('.nav-item').removeClass('active open');

          if(navParent.parent().hasClass('nav-item')){
            navParent.parent().addClass('active open');
          }

          if(navParent.parent().parent().parent().hasClass('nav-item')){
            navParent.parent().parent().parent().addClass('active open');
          }
          // getOrNumber();
          clientWizardValidator();
          getAllPropertiesAndAgents();
      });

    }

// new
    function getOrNumber(){
        $.ajax({ 
            type: 'GET',
            url: apiUrl+"dashboard/client/getAllNames",
            cache: false,
            async: true,
            success: function(response){
                if (response.success==true) {
                    $.each(response.num,function(i,n){
                        $(".addClientprop_ornumber").attr("placeholder", n.ornumber);                 
                    });
                }
                else
                    hideLoading("");
            },
            error: function(err){
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        })
    }

    function getAllClients(){
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/client/getAll",
          cache: false,
          async: false,
          success: function(response) { 
            
            if(response.success){
              
              var table = $('.clientListTable').DataTable();

              table.clear().draw();
              table.rows.add(response.clients).draw();
            }
            else{
              swal("Warning!", response.message, "warning");
            }
          },
          error: function(err){
            swal("Error!", err.statusText, "error");
          }
      });
    }



   function getAllPropertiesAndAgents(){
      showLoading();
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/client/getAllParentProAndAgents",
          cache: false,
          async: false, 
          success: function(response) { 
            
              if(response.success){

                var propertyList = $('.addClientprop_property');
                propertyList.empty();
                $.each(response.properties,function(i,n){
                  propertyList.append("<option value='"+n.propertyID+"'>"+n.propertyName+"</option>");
                });

                var agent = $('.addClientagent');
                agent.empty();
                agent.append('<option value="">Select here...</option>');
                $.each(response.agents,function(i,n){
                  agent.append("<option value='"+n.agentID+"'>"+n.agentName+"</option>");
                });

                hideLoading();
              }
              else{
                hideLoading();
                swal("Warning!", response.message, "warning");
              }
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }
    
    // new code
    function additionalCharges(){
      $('.addtional-charges').append(
          '<div class="row additionalChargesDetails">'+
              '<div  class="form-group" style="padding-left: 15px">'+
                  '<div class="col-md-3">'+
                      '<div class="form-group form-md-line-input">'+
                          '<input type="text" class="form-control chargeValue" name="chargeValue">'+
                          '<label for="form_control_1">Value</label>'+
                          '<span class="help-block"></span>'+
                      '</div>'+
                  '</div>'+
                  '<div class="col-md-3">'+
                      '<div class="form-group form-md-line-input">'+
                          '<input type="text" class="form-control chargeDate" name="chargeDate">'+
                          '<label for="form_control_1">Date </label>'+
                          '<span class="help-block"></span>'+
                      '</div>'+
                  '</div>'+
                  '<div class="col-md-7">'+
                      '<div class="form-group form-md-line-input">'+
                          '<input type="text" class="form-control chargeDescription" name="chargeDescription">'+
                          '<label for="form_control_1">Description</label>'+
                          '<span class="help-block"></span>'+
                      '</div>'+
                  '</div>'+
                   '<div class="col-md-2 text-left">'+
                      '<a href="javascript:;" class="btn btn-outline red cms-redo-charges"><span class="icon-close"></span></a>'+
                  '</div>'+
              '</div>'+
          '</div>'
        );

      $('.chargeDate').datepicker({
          format: 'yyyy-mm-dd'
      });
    }


    function loadAgentCom(argument) {
      var propertyID = $('.clientPropertiesList').val();
      var clientID = $('.imageClientID').val();

      // alert(propertyID + " " + clientID);
      showLoading();
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/client/getAgentComforClientProp",
          data: {propertyID: propertyID, clientID : clientID},
          cache: false,
          async: false, 
          success: function(response) {    
              if(response.success){
                // swal("Success!", response.message, "success");

                $('.AgentComList').empty();
                $.each(response.agentList, function(i,n){
                  $('.AgentComList').append(
                    `<div class="col-xs-4">
                        <div class="form-group form-md-line-input ">
                            <input type="text" class="form-control updateAgentCom" agent_id="` + n.agentID + `" value="` + n.agentCom + `" placeholder="" name="updateAddCom">
                            <label class="control-label agentName" for="form_control_1"> [`+ n.pos +`] `+ n.agentName +` (%) </label>
                            <div class="form-control-focus"> </div>
                            <span class="help-block"></span>
                        </div>    
                    </div>`
                  );
                })
               
                hideLoading();
              }
              else{
                hideLoading();
                swal("Warning!", response.message, "warning");
              }
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }

    function updateAgentCom(argument) {
        
       var cp_Id = $('.clientPropertiesList ').val();

      var totalCom = 0;
      var AgentUpdate;
      // check if total is 10
      $('.updateAgentCom').each(function(i,n) {
          totalCom += parseInt($(this).val());
      });

      if (totalCom != 15) {
          swal("Warning!", 'Total com must be equal to 15', "warning");
      }else{
        confirmWithSwal(function(){
           submitComUpdate(cp_Id);
        },
        'Are you sure to submit this update?',
        'warning');
      }

      totalCom = 0;  
    }

    function submitComUpdate(cp_Id) {
      var clientID = $('.imageClientID').val();

      var agentCom;
      var agentID;

      var errorCheck = 0;
      var message="";
      
      showLoading();
      $('.updateAgentCom').each(function(i,n) {
         agentCom =  $(this).val(),
         agentID = $(this).attr('agent_id')

          $.ajax({
              type: 'POST',
              url: apiUrl+"/dashboard/client/udpateAgentCom",
              data: {propertyID: cp_Id, agentCom: agentCom, agentID: agentID},
              cache: false,
              async: false, 
              success: function(response) {    
                  if(response.success){
                    message = response.message;
                  }
                  else{
                    errorCheck = 1;
                    hideLoading();
                    swal("Warning!", response.message, "warning");
                  }
              },
              error: function(err){
                errorCheck = 1;
                hideLoading();
                swal("Error!", err.statusText, "error");
              }
          });
          
      });

      loadAgentCom();
      if (errorCheck != 1) {
        swal("Success!", message, "success");
        hideLoading();
      }     
    }

    function getDownpayment(argument) {
     var propertyID = $('.clientPropertiesList').val();
     var clientID = $('.imageClientID').val();

      showLoading();
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/client/getThisDownpayment",
          data: {propertyID: propertyID, clientID : clientID},
          cache: false,
          async: false, 
          success: function(response) {    
              if(response.success){
                // swal("Success!", response.message, "success");
                $('.txt_updateDownpayment').val(response.downpayment);
                hideLoading();
              }
              else{
                hideLoading();
                swal("Warning!", response.message, "warning");
              }
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });

    }


    function updateDownpayment(argument) {
      var propertyID     = $('.clientPropertiesList').val();
      var newDownPayment = $('.txt_updateDownpayment').val();

      showLoading();
      $.ajax({
          type: 'POST',
          url: apiUrl+"/dashboard/client/updateThisDownpayment",
          data: {propertyID: propertyID, newDown: newDownPayment},
          cache: false,
          async: false, 
          success: function(response) {
              if(response.success){
                swal("Success!", response.message, "success");
                thisClientPropertiesTabIsClicked();
                hideLoading();
              }
              else{
                hideLoading();
                swal("Warning!", response.message, "warning");
              }
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }

    function changeProperty(argument) {
      alert('comming soon');

      // var clientPropID = $('.clientPropertiesList').val();
      // confirmWithSwal(function(){
      //   chageClientProperty(clientPropID, set_clientID);
      // },
      // 'Change Property?',
      // 'warning');
    }

    function chageClientProperty(PropID, ClientID) {
      bootbox.confirm({
          className: "dialogWide",
          title: "<i class='fa fa-exchange'> </i>  Chane Property", 
          message: model_ChangeProperty(),
          size: 'large',
          buttons: {
              confirm: {
                  label: 'Change',
                  className: 'btn-success'
              },
              cancel: {
                  label: 'Cancel',
                  className: 'btn-danger'
              }
          },
          callback: function (result) {
              console.log('This was logged in the callback: ' + result);
          }
      });
    }

    function model_ChangeProperty() {
      return  `<div class="form" role="form">
                  <div class="form-body" id="propertyForm">
                      <div class="row">
                          <div class="form-group">
                              <div class="col-sm-6">
                                  <div class="form-group form-md-line-input ">
                                      <select class="form-control edited select2 addClientprop_property"><option value="1">Hacienda Alfonzo</option><option value="2">Hacienda Margarita</option><option value="3">Hacienda Del Maria</option></select>
                                      <label for="form_control_1">Property Name</label>
                                  </div>
                              </div>

                              <div class="col-sm-2">
                                  <a href="javascript:;" class="btn btn-outline blue sbold cms-btn-padding-top btn_viewLots"><span class="icon-paper-plane"> </span> View</a> 
                              </div>    
                          </div>
                      </div>   
                      <div class="row">
                          <div class="col-sm-12 col-md-12">
                              <ul class="slectedPorpertyList hidden "></ul>

                              <div class="selectecPropertiesContainer" style="padding-top: 10px;"></div>
                          </div>
                      </div>
                  </div>
              </div>`;
            }



function printPropInfo(argument) {
  var content = document.getElementById('printReceipt').innerHTML;

  bootbox.dialog({
    message: content,
    title: "Property Summary",
    buttons: {
      success: {
        label: "Print",
        className: "green btn-outline",
        callback: function() {
          var html="<html>";
          html+= content;
          html+="</html>";
          var printWin = window.open();
          printWin.document.write(html);
          printWin.document.close();
          printWin.focus();
          printWin.print();
          printWin.close();
        }
      }
    }
  });
}

function getViewLotDetails(argument) {
  
}


function addSpouse(argument) {
  $('#spouseInfo').append(`<div class="row spouse_details fadeInDown_spouse"> `+ spouseForm() +`
                            <a href="javascript:;" class="btn btn-outline red" id="spouse_removeThisSpouse"><span class="fa fa-remove"> </span></a>
                        </div>`);

        callBackFunctions();
      }

      function AddBeneficiary(argument) {
        $('#benif_info').append(`
              <div class="col-md-3 benif_details" style="display: -webkit-inline-box;">  
                  <div class="form-group form-md-line-input">
                      <input type="text" class="form-control txt_benef_name" placeholder="" name="name_benef_name">
                      <label class="control-label" for="form_benef_name"> Full name
                      </label>
                      <div class="form-control-focus"> </div>
                      <span class="help-block"></span>
                  </div>
                  <a href="javascript:;" class="btn btn-outline red" id="btn_RemoveBenef"> <span class="fa fa-remove"></span></a>
              </div>
          `);
      }

      function RemoveBeneficiary(argument) {
        $(this).parent().remove();
      }


      function getSpouseList(argument) {
        const spouseList = $('.spouse_details');
        const spouseDetails = [];
        let fname = "";
        let lname = "";
        let mname = "";
        let civil = "";
        let addre = "";
        let bdate = "";
        let cnum = "";

        $.each(spouseList, function (i, n) {
            fname =  $(this).find('.spouse_fname');
            lname =  $(this).find('.spouse_lname');
            mname =  $(this).find('.spouse_mname');
            civil =  $(this).find('.spouse_civelStat');
            addre =  $(this).find('.spouse_address');
            bdate =  $(this).find('.spouse_bdate');
            cnum  =  $(this).find('.spouse_Cnumber');
            spouseDetails.push({fname: fname.val(), lname: lname.val(), mname: mname.val(), civilstat: civil.val(), address: addre.val(), bdate: bdate.val(), cnum: cnum.val()})
        });

        return spouseDetails;
        // console.log(spouseDetails);
      }

      function getBeneficiaries(argument) {
        const benif = $('.benif_details');
        const benifList = [];
        let name = "";

        $.each(benif, function (i, n) {
          lname =  $(this).find('.txt_benef_name');
          benifList.push(lname.val());
        })

        return benifList;
        // console.log(benifList);
      }

      function callBackFunctions(argument) {
         $('.spouse_civelStat').select2();
         $('.spouse_bdate').datepicker({
            format: 'yyyy-mm-dd'
         });
      }


      function spouseForm(argument) {
        return `<div class="form-group">
                    <div class="col-md-4">
                        <div class="form-group form-md-line-input ">
                            <input type="text" class="form-control spouse_fname" placeholder="" name="spouse_fname">
                            <label class="control-label" for="form_spouseFname">First Name
                                <span class="required">*</span>
                            </label>
                            <div class="form-control-focus"> </div>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-4">   
                        <div class="form-group form-md-line-input ">
                            <input type="text" class="form-control spouse_lname" placeholder="" name="spouse_lname"> 
                            <label class="control-label" for="form_spouseLname">Last Name
                                <span class="required">*</span>
                            </label>
                            <div class="form-control-focus"> </div>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-4">  
                        <div class="form-group form-md-line-input ">
                            <input type="text" class="form-control spouse_mname" placeholder="" name="spouse_mname">
                            <label class="control-label" for="form_spouseMnane">Middle Name
                            </label>
                            <div class="form-control-focus"> </div>
                            <span class="help-block"></span>
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="single" class="control-label"> Civil Status </label>
                            <select id="single" class="form-control select2 spouse_civelStat" >
                                <option></option>
                                <option value="Single"> Single </option>
                                <option value="Married"> Married </option>
                                <option value="Widowed"> Widowed </option>
                                <option value="Divorced"> Divorced </option>
                                <option value="Separated"> Separated </option>
                                <option value="Partnership"> Partnership </option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-8">   
                        <div class="form-group form-md-line-input ">
                            <input type="text" class="form-control spouse_address" placeholder="" name="spouse_address">
                            <label class="control-label" for="form_spouseAddress"> Home Address
                                <span class="required">*</span>
                            </label>
                            <div class="form-control-focus"> </div>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="col-md-4">  
                        <div class="form-group form-md-line-input ">
                            <input type="text" class="form-control spouse_bdate" placeholder="" name="spouse_bdate">
                            <label class="control-label" for="form_spouseBdate"> Birthdate
                            </label>
                            <div class="form-control-focus"> </div>
                            <span class="help-block"></span>
                        </div>
                    </div> 

                    <div class="col-md-4">  
                        <div class="form-group form-md-line-input ">
                            <input type="number" class="form-control spouse_Cnumber" placeholder="" name="spouse_Cnumber">
                            <label class="control-label" for="form_spouseCnumber"> Contact Number
                            </label>
                            <div class="form-control-focus"> </div>
                            <span class="help-block"></span>
                        </div>
                    </div>`;
      }


      function AddNewSpouse(argument) {
          bootbox.confirm({
            title: "<span class='fa fa-user-plus'> New Spouse </span>",
            message: "<div class='row'>" + spouseForm() + "</div>",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {

                const fname = $('.spouse_fname').val();  
                const lname = $('.spouse_lname').val();  
                const mname = $('.spouse_mname').val();  
                const address = $(this).find('.spouse_address').val();  
                const civilStat = $('.spouse_civelStat').val();  
                const dateBirth = $(this).find('.spouse_bdate').val();  
                const contact = $('.spouse_Cnumber').val();  

                let spouseDetails = [];
                if (result) {
                  spouseDetails = {fname: fname, lname: lname, mname: mname, civilstat: civilStat, address: address, bdate: dateBirth, cnum: contact};
                  add_newSpouse(spouseDetails);
                }  

                // console.log('This was logged in the callback: ' + result);
            }
        });

        callBackFunctions();
      }

      function add_newSpouse(data) {
        const clID = $('.imageClientID').val();
        if (clID === null) {
          console.log(undefined + 'client ID');
        }else{

          const newSpouse = [];
      
          if (set_spouseList ===  null) {
            newSpouse.push(data);
          }else{
            newSpouse = set_spouseList;
            newSpouse.push(data);
          }


          showLoading();
          $.ajax({
              type: 'PUT',
              url: apiUrl+"/dashboard/client/newSpouse",
              data: {clientID: clID, spouse: newSpouse},
              cache: false,
              async: false, 
              success: function(response) {
                  hideLoading();
                  if(response.success){
                    console.log(response.message);
                    openProfile(clID);
                  }else{ 
                    swal("Warning!", response.message, "warning");
                  }
              },
              error: function(err){
                hideLoading();
                swal("Error!", err.statusText, "error");
              }
          });
        }
      }

      function spouseUpdateOption(argument) {
        return `
             <div class="row"> 
                 <div class="col-md-12">
                    <div class="form-group spouse_upOption">
                        <label class="control-label"> <strong> Update Option </strong> </label>
                        <div class="form-group">
                            <div class="icheck-list">
                                <label class="radio-inline spouse_lblUpdate" style="display:inline">
                                    <input type="radio" name="optradio" class="rbtn_sp_update" data-id="0"> Remove
                                </label>
                                <label class="radio-inline" style="display:inline" >
                                    <input type="radio" name="optradio" class="rbtn_sp_update" checked data-id="1"> Update
                                </label>
                            </div>
                        </div>
                    </div>
                 </div>
             </div>
    
        `;
      }


      function update_thisSpouse(argument) {
        set_btnVal = 1;

        /*remove remove selected form the list*/
        const formID = $(this).parent().parent().parent().attr('data-id');
      
        // console.log(set_spouseList[formID]); remove selected index from array
        // set_spouseList.splice(0, 1);

        /*------------------------------------*/
          bootbox.confirm({
            title: "<span class='fa fa-pencil'> Update Spouse </span>",
            message: "<div class='row'>" + spouseForm() + " " + spouseUpdateOption() + "</div>",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Submit'
                }
            },
            callback: function (result) {
                const fname =  $(this).find('.spouse_fname').val();
                const lname =  $(this).find('.spouse_lname').val();
                const mname =  $(this).find('.spouse_mname').val();
                const addre =  $(this).find('.spouse_address').val();
                const civil =  $(this).find('.spouse_civelStat').val();
                const bdate =  $(this).find('.spouse_bdate').val();
                const cnumb =  $(this).find('.spouse_Cnumber').val();

                if (result) {
                  if (set_btnVal !== 1) {
                    set_spouseList.splice(formID, 1);
                  }else{
                    set_spouseList[formID].fname  = fname
                    set_spouseList[formID].lname = lname
                    set_spouseList[formID].mname = mname
                    set_spouseList[formID].address = addre
                    set_spouseList[formID].civilstat = civil
                    set_spouseList[formID].bdate = bdate
                    set_spouseList[formID].cnum = cnumb
                  }

                  updateThisSpouse();
                }     
               
            }
        });

         fillSpuseForm(formID);
         callBackFunctions();  

      }
      function fillSpuseForm(formID) {
         $('.spouse_fname').val(set_spouseList[formID].fname); 
         $('.spouse_lname').val(set_spouseList[formID].lname);  
         $('.spouse_mname').val(set_spouseList[formID].mname);  
         $('.spouse_address').val(set_spouseList[formID].address);  
         $('.spouse_civelStat').val(set_spouseList[formID].civilstat);  
         $('.spouse_bdate').val(set_spouseList[formID].bdate);  
         $('.spouse_Cnumber').val(set_spouseList[formID].cnum);
      }

      function updateThisSpouse(argument) {
          const clID = $('.imageClientID').val();

          $.ajax({
              type: 'PUT',
              url: apiUrl+"/dashboard/client/updateSpouse",
              data: {clientID: clID, spouse: set_spouseList},
              cache: false,
              async: false, 
              success: function(response) {
                  hideLoading();
                  if(response.success){
                    // console.log(response.message);
                    openProfile(clID);
                  }else{ 
                    swal("Warning!", response.message, "warning");
                  }
              },
              error: function(err){
                hideLoading();
                swal("Error!", err.statusText, "error");
              }
          });
      }

      function new_beneficiary(argument) {
          bootbox.prompt({
              title: "New Beneficiary",
              inputType: 'text',
              size: 'small',
              callback: function (result) {
                 if (result) {
                    addnewBenef(result);
                    // console.log(result);
                 }
              }
          });
      }

      function addnewBenef(result) {
        const clID = $('.imageClientID').val();
        let newBenif = [];

      
        if (set_beneficiaries === null) {
           newBenif.push(result);
           set_beneficiaries = newBenif;
        }else{ 
          set_beneficiaries.push(result);
        }

        $.ajax({
              type: 'PUT',
              url: apiUrl+"/dashboard/client/newBeneficiary",
              data: {clientID: clID, benef: set_beneficiaries},
              cache: false,
              async: false, 
              success: function(response) {
                  hideLoading();
                  if(response.success){
                    // console.log(response.message);
                    openProfile(clID);
                  }else{ 
                    swal("Warning!", response.message, "warning");
                  }
              },
              error: function(err){
                hideLoading();
                swal("Error!", err.statusText, "error");
              }
          });
      }

      function benefForm(val) {
        return `           
               <div class="col-md-12">
                  <input type="text" class="form-control" id="txt_newbenif" value="`+ val +`"/> 
               </div>`;
      }

      function update_benef(argument) {
        set_btnVal = 1;
        const val = $(this).attr('data-val');
        const dataID = $(this).attr('data-id');

        bootbox.confirm({
            title: "<span class='fa fa-pencil'> Update Beneficiary </span>",
            message: "<div class='row'>" + benefForm(val) + " " + spouseUpdateOption() + "</div>",
            size: 'small',
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Submit'
                }
            },
            callback: function (result) {
                const val = $('#txt_newbenif').val();

                if (result) {
                  if (set_btnVal !== 1) {
                    set_beneficiaries.splice(dataID, 1);
                  }else{
                    set_beneficiaries[dataID] = val
                  }

                  updateBeneficiary();

                }     
            }
        });
      }


      function updateBeneficiary() {
         const clID = $('.imageClientID').val();
         $.ajax({
            type: 'PUT',
            url: apiUrl+"/dashboard/client/updateBeneficiary",
            data: {clientID: clID, benef: set_beneficiaries},
            cache: false,
            async: false, 
            success: function(response) {
                hideLoading();
                if(response.success){
                  // console.log(response.message);
                  openProfile(clID);
                }else{ 
                  swal("Warning!", response.message, "warning");
                }
            },
            error: function(err){
              hideLoading();
              swal("Error!", err.statusText, "error");
            }
        });
      }

      function get_ClientOfTheSelectedProperty(argument) {
         const propID = $(this).val();
        
          if(propID === '0'){
            getAllClients();
          }else{
            geT_clientList(propID);
          }

      }

      function geT_clientList(propID) {
          showLoading();
           $.ajax({
            type: 'GET',
            url: apiUrl+"/dashboard/client/getClietsfromselectedProperTy",
            data: {properTyID:propID},
            cache: false,
            async: false, 
            success: function(response) {
                hideLoading();
                if(response.success){

                    $('#slt_propertyList').select2({
                        data: response.data[0][0],
                        allowClear: true,
                        minimumResultsForSearch: -1
                    });

                    var table = $('.clientListTable').DataTable();
                    table.clear().draw();
                    table.rows.add(response.data[0][1]).draw();

                }else{ 
                  swal("Warning!", response.message, "warning");
                }
            },
            error: function(err){
              hideLoading();
              swal("Error!", err.statusText, "error");
            }
        });
      }

      function get_ClientByFromTheSelectedBl(argument) {
          const parentID = $('#slt_propertyParentList').val(); 
          const properTD = $(this).val(); 

          if (parentID !== "0") {
            if (properTD == "0") {
                geT_clientList(parentID);
            }else{
                get_clientListByBlock(properTD);
            }
          }
      }

      function get_clientListByBlock(properTD) {
        showLoading();
           $.ajax({
            type: 'GET',
            url: apiUrl+"/dashboard/client/getThisClientListfromBockSearch",
            data: {listID : properTD},
            cache: false,
            async: false, 
            success: function(response) {
                hideLoading();
                if(response.success){
                  
                    var table = $('.clientListTable').DataTable();                    
                    if (response.data !== undefined) {
                        table.rows.add(response.data).draw();
                    }else{
                      table.clear().draw();
                    }

                }else{ 
                  swal("Warning!", response.message, "warning");
                }
            },
            error: function(err){
              hideLoading();
              swal("Error!", err.statusText, "error");
            }
        });
      }


}(); 
// validateUser();
jQuery(document).ready(function() {    
  Client.init();
});