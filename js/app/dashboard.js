  var Dashboard = function() {
      var templates = [],
          baseTemplateUrl = 'template/dashboard/';
    var userLoggedIn=0;
      var loadCount = 0;


      function getTemplate(baseTemplateUrl, templateName, callback) {
          if (!templates[templateName]) {
              $.get(baseTemplateUrl + templateName, function(resp) {
                  compiled = _.template(resp);
                  templates[templateName] = compiled;
                  if (_.isFunction(callback)) {
                      callback(compiled);
                  }
              }, 'html');
          } else {
              callback(templates[templateName]);
          }
      }



 


      return {
          init: init
      };

      // Init
      function init() {
        
          // loadInitialization();
         
          addEventHandlers();
          // $('.openDashboardContainer').click();
          // alert("handlers");
          openDashboardPage();
          $.ajax({
            type:'GET',
            url:apiUrl+'/dashboard/user/checkIfDSR',
            cache:false,
            async:true,
            success:function(response){
                userLoggedIn = response.userID;
            },
            error:function(err){

            }
        });

      }

      function addEventHandlers() {
          // $(document).on('click', '.openDashboardContainer', openDashboardPage);
         
          // $(document).on('click', '.toHome', function() { $('.openDashboardContainer').click() });
          $(document).on('click', '.headerGoToUserProfile', getInfoForThisEmployee);
          $(document).on('click', '.btn_percentage', UpdatePercentage);
          $(document).on('click','.btnSendNotice', function(){
            // alert($(this).attr('data-id'));
            sendNotice($('#slt_propertyParentList').val());
        });
          $(document).on('change', '#slt_propertyParentList', get_ClientByFromTheSelectedBl);
          // $(document).on('click', '.btn_sendNotice', sendlateNotice);
      }

      function getInfoForThisEmployee() {
          showLoading();

          var id = $(this).attr('data-id');

          if (id == '' || id == undefined) {
              swal('Warning!', "Can't find this employee.", 'error');
              return false;
          }

          $.ajax({
              type: 'GET',
              url: apiUrl + "/dashboard/user/getAllInfoForThisUser",
              data: { ID: id },
              cache: false,
              async: false,
              success: function(response) {
                  if (!response.has_login)
                      logoutAccount();

                  if (response.success) {
                      getUserProfilePage(response.user, response.userRole);
                  } else {
                      hideLoading();
                      swal("Warning!", response.message, "warning");
                  }
              },
              error: function(err) {
                  hideLoading();
                  swal("Error!", err.statusText, "error");
              }
          });
      }

      function getUserProfilePage(data, userRole) {
          showLoading();

          $(".dashboardPage").addClass('hidden');
          getTemplate('template/users/', 'userProfile.html', function(render) {
              var renderedhtml = render({ user: data });
              $(".updateUserOrEmployeeContainer").html(renderedhtml);
              $('.updateUserOrEmployeeContainer').removeClass('hidden');

              var id = "";
              $('#txt_username_user').val(data[0]['user_username']);
              $.each(data[0], function(i, n) {
                  if (i == 'ID') {
                      id = n;
                      $('.imageEmployeeID').val(n);
                  }

                  if (i == 'user_imageFilename') {


                      if (n == 'no_image.jpg') {
                          $('.userProfileImage').attr('src', 'images/' + n);
                      } else
                          $('.userProfileImage').attr('src', 'api/csm_v1/public/uploads/users/' + id + '/' + n);
                  }

                  $(('.' + i)).val(n);
              });

              if (userRole == "Encoder") {
                  // $('.showHide').addClass('hidden');
              }else if(userRole != "Admin"){
                $('#position_div').remove();
              }

              $(".userProfilePage").click();
              gf_userRole = userRole;

              hideLoading();
          });
      }

      function openDashboardPage() {
        loadCount++;

        if (loadCount === 1) {
            showLoading();
            var vavParent = $(this);

            $(".dashboardPage").addClass('hidden');

            getTemplate(baseTemplateUrl, 'dashboard.html', function(render) {
                var renderedhtml = render({ data: "" });
                $(".dashboardContainer").html(renderedhtml);
                $('.dashboardContainer').removeClass('hidden');
            
                $('.page-sidebar-menu').find('.nav-item').removeClass('active open');
                
                // LoadClientswithlatePayments();
                if (vavParent.parent().hasClass('nav-item')) {
                    vavParent.parent().addClass('active open');
                }

                if (vavParent.parent().parent().parent().hasClass('nav-item')) {
                    vavParent.parent().parent().parent().addClass('active open');
                }

                // $('.counter').counterUp();
           
                // $('#slt_propertyParentList').select2();
                getPropertyList();
                // getDataDashboard();

                $('.counter').counterUp({
                    delay: 10,
                    time: 1000,
                    offset: 70,
                    beginAt: 100,
                    formatter: function(n) {
                        return n.replace(/,/g, '.');
                    }
                });

                hideLoading();
                
               
            });

            loadCount = 1;
                 tableclientwithlatePayments();
                LoadClientswithlatePayments();
        }

      }
    function getPropertyList(argument) {
       $.ajax({ 
          type: 'GET',
          url: apiUrl+"dashboard/client/getAllProperParentList",
          cache: false,
          async: true,
          success: function(response){
              if (response.success==true) {
                 
                    $('#slt_propertyParentList').select2({
                      data: response.data
                    });
                  

              }else{
                  swal("Warning", response.message, 'warning');
              }
                 
          },
          error: function(err){
              hideLoading();
              swal("Error!", err.statusText, "error");
          }
      })
    }
function get_ClientByFromTheSelectedBl() {
          var parentID = $('#slt_propertyParentList').val(); 

          
            if (parentID != 0) {
                tableclientwithlatePayments();
                tableclientwithlatePaymentsWithID(parentID);
              
            }else{
              tableclientwithlatePayments();
                LoadClientswithlatePayments();

            }
          
      }
       function tableclientwithlatePaymentsWithID(propID) {
          showLoading();
           $.ajax({
            type: 'GET',
            url: apiUrl+"/dashboard/client/getclientLatepaymentswithID",
            data: {properTyID:propID},
            cache: false,
            async: false, 
            success: function(response) {
                hideLoading();
                if(response.success){

                    // $('#slt_propertyList').select2({
                    //     data: response.data[0][0],
                    //     allowClear: true,
                    //     minimumResultsForSearch: -1
                    // });

                    var table = $('.clientwithlatePayments').DataTable();
                    table.clear().draw();
                    // table.rows.add(response.latepayments[0][1]).draw();
                    table.rows.add(response.latepayments).draw();
              // $('.clientwithlatePayments').empty();
              $.each(response.latepayments,function(i,n){

                $('.clientwithlatePayments').append(
                  '<tr>'+
                    '<td>'+n[0]+'</td>'+
                    '<td>'+n[1]+'</td>'+
                    '<td>'+n[2]+'</td>'+
                    '<td>'+n[3]+'</td>'+
                    '<td>'+n[4]+'</td>'+
                    '<td>'+n[5]+'</td>'+
                    // '<td>'+("&#x20B1; "+accounting.format(n[6],2))+'</td>'+
                  '</tr>'
                  );
              });

                }else{ 
                  swal("Warning!", response.message, "warning");
                }
            },
            error: function(err){
              hideLoading();
              swal("Error!", err.statusText, "error");
            }
        });
      }

      // generate random color
      function getRandomColor() {

          var letters = '0123456789ABCDEF';
          var color = '#';
          for (var i = 0; i < 6; i++) {
              color += letters[Math.floor(Math.random() * 16)];
          }
          return color;
      }

      function propertyList(propDetails) {
          let proplist = '';


          var propDEv = propDetails.map(n => {
              const total = (n.sum === null ? 0 : n.sum);
              const valA = n.A === null ? 0 : n.A
              const valB = n.B === null ? 0 : n.B
              const valC = n.C === null ? 0 : n.C
              const percentageA = (total * (valA / 100));
              const percentageB = (total * (valB / 100));
              const percentageC = (total * (valC / 100));

              const data = `<div class="col-md-4">
                        <div class="mt-widget-2">
                            <div class="mt-body propList">
                                <h3 class="mt-body-title propTitle"> <span class="propName"> ${n.prop} </span> </h3>
                                <div class="mt-body-actions">
                                    <div class="btn-group btn-group btn-group-justified" data-id="${n.propID}">
                                        <a href="javascript:;" class="btn btn_percentage" data-dev="1">
                                            <h4> <span class="total_netA"> ${m(percentageA,2)} </span></h4>
                                            <i class="" style="font-size:12px;">Office</i> <br>(${valA} %) </a>
                                        </a>
                                        <a href="javascript:;" class="btn btn_percentage" data-dev="2">
                                            <h4> <span class="total_netB"> ${m(percentageB,2)} </span></h4>
                                            <i class="" style="font-size:12px;">Partner 1</i> <br> (${valB} %)</a>

                                        <a href="javascript:;" class="btn btn_percentage" data-dev="3">
                                        <h4> <span class="total_netC" style="color:red;"> ${m(percentageC,2)} </span></h4>
                                        <i class="" style="font-size:12px;">Partner 2</i> <br> (${valC} %) </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`
              proplist += data;
          });

          return proplist
      }


      function getDateToday(argument) {
          var today = new Date();
          var dd = today.getDate();
          var mm = today.getMonth()+1; //January is 0!
          var yyyy = today.getFullYear();

          if(dd<10) {
              dd = '0'+dd
          } 

          if(mm<10) {
              mm = '0'+mm
          } 

          today = yyyy + '-' + mm + '-' + dd;     

          return today;
      }


      function setTotalChargesToday(charges){
        //    let totalCharges = 0;
        //    charges.map(x =>{ 
              
        //       if (x.chargeDate !== undefined) {
        //           if (x.chargeDate === getDateToday()) {
        //               totalCharges += (parseInt(x.chargeValue));
        //           }
        //       }
        //   });
          $('#todayCharges').html(charges);
          $('#todayCharges').attr('data-value', charges);
        //   $('#todayCharges').html(totalCharges);
        //   $('#todayCharges').attr('data-value', totalCharges);
      }


      function setTotalChargesThisWeek(charges) {
        
        // var curr = new Date; // get current date
        // var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
        // var last = first + 6; // last day is the first day + 6

        // let firstday = new Date(curr.setDate(first));
        // let lastday = new Date(curr.setDate(last));


        // let totalCharges = 0;
        //    charges.map(x =>{ 
        //       if (x.chargeDate !== undefined) {
        //           if (x.chargeDate >= setDateFormat(firstday) && x.chargeDate <= setDateFormat(lastday)) {
        //               totalCharges += (parseInt(x.chargeValue));
        //           }
        //       }
        //   });

          $('#weekCharges').html(charges);
          $('#weekCharges').attr('data-value', charges);
        //   $('#weekCharges').html(totalCharges);
        //   $('#weekCharges').attr('data-value', totalCharges);
      }

      function setThisMonthTotalCharges(charges) {

        // const date = new Date(), y = date.getFullYear(), m = date.getMonth();
        // const firstday = new Date(y, m, 1);
        // const lastday = new Date(y, m + 1, 0);

        // let totalCharges = 0;
        //  charges.map(x =>{ 
            
        //     if (x.chargeDate !== undefined) {
        //         if (x.chargeDate >= setDateFormat(firstday) && x.chargeDate <= setDateFormat(lastday)) {
        //               totalCharges += (parseInt(x.chargeValue));
        //         }
        //     }
        // });

        $('#monthCharges').html(charges);
        $('#monthCharges').attr('data-value', charges); 
        // $('#monthCharges').html(totalCharges);
        // $('#monthCharges').attr('data-value', totalCharges); 
      }


      function setDateFormat(thisDate){
        let newDate = "";

        let dd = thisDate.getDate();
        let mm = thisDate.getMonth()+1;
        let yy = thisDate.getFullYear();

        if(dd<10) {
            dd = '0'+dd
        } 

        if(mm<10) {
            mm = '0'+mm
        } 

        newDate = yy + '-' + mm + '-' + dd;
        
        return  newDate;

      }

      function getDataDashboard() {
        // loadCount++;
        // if (loadCount === 3) {

            var id = 0;
            $.ajax({
                type: 'GET',
                url: apiUrl + "/dashboard/user/getAllDataDashboard",
                cache: false,
                async: false,
                success: function(response) {
                    // hideLoading();

                    if (response.success) {

                        // getWeekDateRange();
                        let { totalGross: gross, clientCount, AgentCount, Sales, prop, propSub, tax, com, rel, chargesDetais, position} = response.data[0];
                        const dataProvider = [];
                        let totalGross = 0;
                        let largest = 0;
                        
                        if (response.data[0]['position'] == "Admin") {
                            const Ttax = tax[1]['tax'] === null ? 0 : tax[1]['tax'] ;
                            const Wtax = tax[2]['tax'] === null ? 0 : tax[2]['tax'] ;
                            const Mtax = tax[3]['tax'] === null ? 0 : tax[3]['tax'] ;
                          
                            if (gross !== null) {
                                totalGross = gross;
                            }

                            setForm1(totalGross, clientCount, AgentCount, Sales, Ttax, Wtax, Mtax, tax, com, chargesDetais);
                            setForm2(prop, rel, propSub, dataProvider, largest);

                            $('#form_statistics3').empty();
                            // const dataComRelease = [];
                        }else if(response.data[0]['position'] == "Agent"){


                          let {clientCount, totalCom, commissionable, position} = response.data[0];

                          if (commissionable == null) {
                            commissionable = 0;
                          }

                          $('#lbl_clientList').html(clientCount);
                          $('#lbl_clientList').attr('data-value', clientCount);

                          $('#lbl_totalCommission').html(totalCom);
                          $('#lbl_totalCommission').attr('data-value', totalCom);

                          $('#lbl_commissionable').html(commissionable);
                          $('#lbl_commissionable').attr('data-value', commissionable);

                          $('#form_statistics1').empty();
                          $('#form_statistics2').empty();

                        }else if(response.data[0]['position'] == "Encoder"){

                          // setForm2(prop, rel, propSub, dataProvider, largest);

                          $('#form_statistics1').empty();
                          $('#form_statistics2').empty();
                          $('#form_statistics3').empty();
                        
                        }else if(response.data[0]['position'] == "Business Partner"){
                          const Ttax = tax[1]['tax'] === null ? 0 : tax[1]['tax'] ;
                          const Wtax = tax[2]['tax'] === null ? 0 : tax[2]['tax'] ;
                          const Mtax = tax[3]['tax'] === null ? 0 : tax[3]['tax'] ;
                        
                          if (gross !== null) {
                              totalGross = gross;
                          }

                          setForm1(totalGross, clientCount, AgentCount, Sales, Ttax, Wtax, Mtax, tax, com, chargesDetais);
                          setForm2(prop, rel, propSub, dataProvider, largest);

                          $('#today_Tax_div').remove();
                          $('#weekly_Tax_div').remove();
                          $('#monthly_Tax_div').remove();
                          $('#client_payments_div').remove();
                          $('#form_statistics3').remove();
                          $('#commissions_div').remove();
                          $('#com_release_div').remove();

                          $('.btn_percentage').removeClass('btn_percentage');

                        }

                        $('#li_propName').empty();

                    } else {

                        hideLoading();
                        swal("Warning!", response.message, "warning");
                    }

                },
                error: function(err) {
                    hideLoading();
                    swal("Error!", err.statusText, "error");
                }
            });

            $('.counterup').counterUp({
                delay: 3,
                time: 1000
            });
        
            



        //    loadCount = 0;
        // }
      }
    var clientwithlatePayments;
    function tableclientwithlatePayments(){

        var table = $('.clientwithlatePayments');
        clientwithlatePayments =  table.dataTable({
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No Data Yet",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            // bootstrap_full_number
            "pagingType": "bootstrap_extended", 
            "order": [
                [5, 'desc']
            ],
            "lengthMenu": [
                [50,100,300,500,800,1500],
                [50,100,300,500,800,1500] // change per page values here
            ],
            // set the initial value
            "pageLength" : 50,
            destroy : true
        });
          // var table = $('.clientwithlatePayments').DataTable();
          
       
 
    }

    //   function openClientProfileOfthis(){
    //   var id = $(this).attr('data-id');
    //   set_clientID = id;

    //   getTemplate(baseTemplateUrl, 'dashboard.html', function(render) {
    //     $( ".dashboardPage" ).empty();
    //     var renderedhtml = render({data: ""});
    //     $(".clientProfileContainer").html(renderedhtml);
    //     $('.clientProfileContainer').removeClass('hidden');
    //   });
       
    //   openProfile(id);
    //   tableclientwithlatePayments();
    //   $('.date-picker').datepicker();
    //   $('.chargeValue').datepicker({
    //     format: 'yyyy-mm-dd'
    //   })

    // }

 function LoadClientswithlatePayments(){

      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/client/getAllclientwithLatepayment",
          cache: false,
          async: true,
          success: function(response) { 
            if(response.success){      

              var table = $('.clientwithlatePayments').DataTable();
              table.clear().draw();
              table.rows.add(response.latepayments).draw();
              
              $.each(response.latepayments,function(i,n){

                $('.clientwithlatePayments').append(
                  '<tr>'+
                    '<td>'+n[0]+'</td>'+
                    '<td>'+n[1]+'</td>'+
                    '<td>'+n[2]+'</td>'+
                    '<td>'+n[3]+'</td>'+
                    '<td>'+n[4]+'</td>'+
                    '<td>'+n[5]+'</td>'+
                    // '<td>'+("&#x20B1; "+accounting.format(n[6],2))+'</td>'+
                  '</tr>'
                  );

              });

              // $('.clientwithlatePayments').prepend(` <tr>
              //                   <th>#</th>
              //                   <th >CLIENT NAME #</th>
              //                   <th>PROPERTY</th>
              //                   <th>BLOCK</th>
              //                   <th>LOT</th>
              //                   <th>LATE PAYMENTS</th>

              //               </tr>`);
            }
            else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }

            $('.slt_propertyParentList').select2({
              placeholder: "Select a project",
              allowClear: true
            }); 
         
            hideLoading();
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });

    }
    function getPropertyListforDashboard(argument) {
       $.ajax({ 
          type: 'GET',
          url: apiUrl+"dashboard/client/getAllProperParentList",
          cache: false,
          async: true,
          success: function(response){
              if (response.success==true) {
                  if (response.role == "Agent") {
                      $('#prop_selection').addClass('hidden');
                  }else{
                    $('#slt_propertyParentList').select2({
                      data: response.data
                    });
                  }

              }else{
                  swal("Warning", response.message, 'warning');
              }
                 
          },
          error: function(err){
              hideLoading();
              swal("Error!", err.statusText, "error");
          }
      })
    }
 function sendNotice(thisId) {
    // alert(thisId);
        bootbox.prompt({
          className: 'AdminPassword',
          title: "Administrator Password Required!",
          inputType: 'password',
          callback: function(result){
            if (result === null) {
                // alert('cancel');
            } else {
                checkAdminPass(result, thisId);
                // alert(thisId);
            }
          }
        });
    }
        function checkAdminPass(password,thisId) {
         $.ajax({
            url: apiUrl+"dashboard/payment/checkAdminPass",
            type: 'GET',
            data : {pass: password},
            cache: false,
            async: true,
            success: function(response){
                hideLoading("");
                if (response.success) {
                 sendNoticetoClients(thisId);

                }else{
                    alert('Invalid Password');
                }

            },

            error: function(err){
                hideLoading("");
                setTimeout(function(){swal("Error!", err.statusText, "error");},0);
            }
        });
    }
    function sendNoticetoClients(thisId) {
          showLoading();
           $.ajax({
            type: 'GET',
            url: apiUrl+"/dashboard/client/getSMSnotification",
            data: {properTyID:thisId},
            cache: false,
            async: false, 
            success: function(response) {
                hideLoading();
                if(response.success){
                   swal("Success!", "Message Sent", "success");
                  

                }else{ 
                  swal("Warning!", response.message, "warning");
                }
            },
            error: function(err){
              hideLoading();
              swal("Error!", err.statusText, "error");
            }
        });
      }
    function getSum(total, num) {
       return total + num;
    }
      function setForm1(totalGross, clientCount, AgentCount, Sales, Ttax, Wtax, Mtax, tax, com, chargesDetais) {

        if (true) {}


        $('#totalGross').html(ReplaceNumberWithCommas(totalGross));
        $('#totalGross').attr('data-value', ReplaceNumberWithCommas(totalGross));

        $('#counterupClients').html(clientCount);
        $('#counterupClients').attr('data-value', clientCount);

        $('#agentCount').html(AgentCount);
        $('#agentCount').attr('data-value', AgentCount);

        $('#totalSales').html(ReplaceNumberWithCommas(Sales[0]['Sales']));
        $('#totalSales').attr('data-value', ReplaceNumberWithCommas(Sales[0]['Sales']));

        $('#weekSales').html(ReplaceNumberWithCommas(Sales[1]['Sales']));
        $('#weekSales').attr('data-value', ReplaceNumberWithCommas(Sales[1]['Sales']));
        
        $('#monthSales').html(ReplaceNumberWithCommas(Sales[2]['Sales']));
        $('#monthSales').attr('data-value', ReplaceNumberWithCommas(Sales[2]['Sales']));

        $('#taxCol').html(ReplaceNumberWithCommas(Ttax.toFixed(2)));
        $('#taxCol').attr('data-value', ReplaceNumberWithCommas(Ttax.toFixed(2)));
        
        $('#weeTaxCol').html(ReplaceNumberWithCommas(Wtax.toFixed(2)));
        $('#weeTaxCol').attr('data-value', ReplaceNumberWithCommas(Wtax.toFixed(2)));
        
        $('#monthTaxCol').html(ReplaceNumberWithCommas(Mtax.toFixed(2)));
        $('#monthTaxCol').attr('data-value', ReplaceNumberWithCommas(Mtax.toFixed(2)));
        
        // $('#lbl_totalTax').html(ReplaceNumberWithCommas(tax[0]['tax'].toFixed(2)));
        $('#lbl_totalCom').html(ReplaceNumberWithCommas(com));

        setTotalChargesToday(chargesDetais[0]['c']);
        setTotalChargesThisWeek(chargesDetais[1]['c']);
        setThisMonthTotalCharges(chargesDetais[2]['c']);
      }

      function setForm2(prop, rel, propSub, dataProvider, largest){
          
        $.each(prop, function(i, n) {
            const propList = `
               <li>
                  <a href="javascript:;"> ` + n.prop + ` </a>
              </li>`;
            const sum = (n.sum === null ? 0.00 : n.sum);
            const data = {
                "name": n.prop,
                "points": sum,
                "color": getRandomColor(),
                "bullet": "images/prop.png"
            };
            dataProvider.push(data);

            if (largest < sum) {
                largest = Number(sum)
            }

            $('#li_propName').append(propList);
        });


        // const dataComRelease = rel.map(n => { 
        //     var rObj = {}
        //     rObj['date'] = n.date;
        //     rObj['value'] = n.val;
           
        //    return rObj;
        // });

        const dataComRelease = rel;

      $('#devPropList').empty();
      $('#devPropList').append(propertyList(propSub));
        var chart = AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "theme": "none",
            "dataProvider": dataProvider,
            "valueAxes": [{
                "maximum": (Number(largest) + Number(50000)),
                "minimum": 0,
                "axisAlpha": 0,
                "dashLength": 4,
                "position": "left"
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]]</b></span>",
                "bulletOffset": 10,
                "bulletSize": 52,
                "colorField": "color",
                "cornerRadiusTop": 8,
                "customBulletField": "bullet",
                "fillAlphas": 0.8,
                "lineAlpha": 0,
                "type": "column",
                "valueField": "points"
            }],
            "marginTop": 0,
            "marginRight": 0,
            "marginLeft": 0,
            "marginBottom": 0,
            "autoMargins": false,
            "categoryField": "name",
            "categoryAxis": {
                "axisAlpha": 0,
                "gridAlpha": 0,
                "inside": true,
                "tickLength": 0
            },
            "export": {
                "enabled": true
            }
        });

        revenueData(dataComRelease);
      }

      function revenueData(data) {
          var _chart_2 = AmCharts.makeChart( "site_activities", {
              "type": "serial",
              "theme": "light",
              "marginRight": 40,
              "marginLeft": 70,
              "autoMarginOffset": 20,
              "dataDateFormat": "YYYY-MM-DD",
              "valueAxes": [ {
                "id": "v1",
                "axisAlpha": 0,
                "position": "left",
                "ignoreAxisWidth": true
              } ],
              "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0
              },
              "graphs": [ {
                "id": "g1",
                "balloon": {
                  "drop": true,
                  "adjustBorderColor": false,
                  "color": "#ffffff",
                  "type": "smoothedLine"
                },
                "fillAlphas": 0.2,
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "bulletSize": 5,
                "hideBulletsCount": 50,
                "lineThickness": 2,
                "title": "red line",
                "useLineColorForBulletBorder": true,
                "valueField": "value",
                "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
              } ],
              "chartCursor": {
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true,
                "cursorAlpha": 0,
                "zoomable": false,
                "valueZoomable": true,
                "valueLineAlpha": 0.5
              },
              "valueScrollbar": {
                "autoGridCount": true,
                "color": "#000000",
                "scrollbarHeight": 50
              },
              "categoryField": "date",
              "categoryAxis": {
                "parseDates": true,
                "dashLength": 1,
                "minorGridEnabled": true
              },
              "export": {
                "enabled": true
              },
              "dataProvider": data
            } );
      }


      function UpdatePercentage() {
          loadCount++;
          if (loadCount === 2) {

              const id = $(this).parent().attr('data-id');
              const dev = $(this).attr('data-dev');
          
            if(userLoggedIn == 7){
                bootbox.prompt({
                    title: "Input Percentage value",
                    inputType: 'number',
                    size: 'small',
                    callback: function(result) {
                        const percentage = result;
                        if (result) {
                            $.ajax({
                                type: 'PUT',
                                url: apiUrl + "/dashboard/user/UpdatePErcentage",
                                data: { val: percentage, propID: id, dev: dev },
                                cache: false,
                                async: true,
                                success: function(response) {
                                    if (response.success) {
                                        // getDataDashboard();
                                        swal("Success!", response.message, "success");
                                    } else {
                                        swal("Warning!", response.message, "warning");
                                    }
                                },
                                error: function(err) {
                                    hideLoading();
                                    swal("Error!", err.statusText, "error");
                                }
                            });
                        }
                    }
                });
    
               
            }else{
                swal('Warning!','Please Contact Don Sakagawa','warning');
            }
            loadCount = 0;
         
            
          }
      }


      function m(number, decPlaces) {
          // 2 decimal places => 100, 3 => 1000, etc
          decPlaces = Math.pow(10, decPlaces);

          // Enumerate number abbreviations
          var abbrev = ["k", "m", "b", "t"];

          // Go through the array backwards, so we do the largest first
          for (var i = abbrev.length - 1; i >= 0; i--) {

              // Convert array index to "1000", "1000000", etc
              var size = Math.pow(10, (i + 1) * 3);

              // If the number is bigger or equal do the abbreviation
              if (size <= number) {
                  // Here, we multiply by decPlaces, round, and then divide by decPlaces.
                  // This gives us nice rounding to a particular decimal place.
                  number = Math.round(number * decPlaces / size) / decPlaces;

                  // Handle special case where we round up to the next abbreviation
                  if ((number == 1000) && (i < abbrev.length - 1)) {
                      number = 1;
                      i++;
                  }

                  // Add the letter for the abbreviation
                  number += abbrev[i];

                  // We are done... stop
                  break;
              }
          }

          return number;
      }

  }();

  validateUser();

  jQuery(document).ready(function() {
      Dashboard.init();
       isLogin();
  });