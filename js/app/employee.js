var Employee = function () {
    var templates = [],
            baseTemplateUrl_reports = 'template/reports/';


    function getTemplate(baseTemplateUrl, templateName, callback) {
        if (!templates[templateName]) {
            $.get(baseTemplateUrl + templateName, function(resp) {
                compiled = _.template(resp);
                templates[templateName] = compiled;
                if (_.isFunction(callback)) {
                    callback(compiled);
                }
            }, 'html');
        } else {
            callback(templates[templateName]);
        }
    }
 

    var hmsEmployeeListTable;
    function tableEmployeeList(){
        var table = $('.hmsEmployeeListTable');

        hmsEmployeeListTable =  table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No employees added yet",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            // bootstrap_full_number
            "pagingType": "bootstrap_extended", 
            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, 50,100],
                [5, 10, 15, 20, 50,100] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            destroy : true
        });
    }


    return {
        init: init
    };

    // Init
    function init() {
        addEventHandlers();
        loadInitialization();
        getAllEmployees();
        setTimeout(function() {
            $('.hmsLoading').addClass('hidden');
        }, 1000);
    }

    function loadInitialization(){
        tableEmployeeList();
    }





    function addEventHandlers(){
        $(document).on('click', '.hmsOpenAddEmployeePage', loadEmployeePage );
        $(document).on('click', '.addEmployeeBtn', showAddEmployeeForm );
        $(document).on('click', '.hmsRemoveThisEmployee', removeThisEmployee );
        $(document).on('click', '.hmsUpdateThisEmployee', requestUpdateThisEmployee );
         
    }
    function updateThisEmployee(btn){
      var Fname       = $('.empFormEmpFName').val().toUpperCase();
      var Lname       = $('.empFormEmpLName').val().toUpperCase();
      var Mname       = $('.empFormEmpMname').val().toUpperCase();
      var Position    = $('#empFormEmpPosition').val().toUpperCase();
      var Department  = $('#empFormEmpDepartment').val().toUpperCase();

      getisp(function(isp){
        var data = {
          'ID'            : btn.attr('data-empID'),
          'Fname'         : Fname,
          'Lname'         : Lname,
          'Mname'         : Mname,
          'Position'      : Position,
          'isp'           : isp,
          'Department'    : Department
        };

        $.ajax({
            type: 'POST',
            url: apiUrl+"/dashboard/reports/employee/updateThisEmployee",
            data : data,
            cache: false,
            async: false,
            success: function(response) {
                requestInvalid(response);

                if(response.success){
                  var table = $('.hmsEmployeeListTable').DataTable();

                  var currentTR = btn.closest("tr");
                  var d = table.row( currentTR ).data();
                  d[0] = (data.Fname + ' '+ data.Lname);
                  d[1] = data.Position ;
                  d[2] = data.Department ;
                  d[3] = ('<button type="button" data-empID="'+data.ID+'" data-empFname="'+data.Fname +'" data-empLname="'+data.Lname+'" data-empMname="'+data.Mname+'" data-empPosition="'+data.Position+'" data-empDepartment="'+data.Department+'" class="btn btn-outline green hmsUpdateThisEmployee" ><i class="icon-pencil"></i> Update</button>'+
                         '<button type="button" data-empID="'+data.ID+'" class="btn btn-outline red hmsRemoveThisEmployee"><i class="icon-close"></i> Remove</button>')  ;

                  table.row(currentTR).data(d); 

                  swal("Success!", response.message, "success");
                }
                else{
                  swal("Warning!", response.message, "warning");
                }
            },
              error: function(err){
                  setTimeout(function(){
                      $('.hmsLoading2').addClass('hidden');
                      swal("Error!", err.statusText, "error");
                  },500);
              }
        });
      
      });
    }

    function requestUpdateThisEmployee(){
      var btn = $(this).parent().find('.hmsUpdateThisEmployee');
      bootbox.dialog({
            message: getEmployeeForm(),
            title: '<div><h3><strong><i class="icon-users" aria-hidden="true"></i> Update Employee</strong></h3></div>',
            buttons: {
              success: {
                label: "Update",
                className: "green",
                callback: function() {
                  if(validateEmployeeForm()){  // has error
                    return false;
                  }

                  updateThisEmployee(btn);

                }
              },
              main: {
                label: "Cancel",
                className: "blue",
                callback: function() {
                    
                }
              }
            }
        });   


      $('.empFormEmpFName').val(btn.attr('data-empFname'));
      $('.empFormEmpLName').val(btn.attr('data-empLname'));
      $('.empFormEmpMname').val(btn.attr('data-empMname'));
      $('#empFormEmpPosition').val(btn.attr('data-empPosition'));
      $('#empFormEmpDepartment').val(btn.attr('data-empDepartment'));

      // bootbox.find('.modal-content')
      //   .css({'background-color': 'red',
      //         'font-weight' : 'bold', 
      //         color: '#F00', 
      //         'font-size': '2em', 
      //         'font-weight' : 'bold'} );   
      initializedAutoSuggestValues();
      
    }

    function removeThisEmployee(){
      var btn = $(this).parent().find('.hmsUpdateThisEmployee');
       swal({
          title: 'Are you sure to remove '+btn.attr('data-empFname')+' '+btn.attr('data-empLname')+'?',
          type: 'warning',
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonClass: 'btn-info',
          cancelButtonClass: 'btn-danger',
          closeOnConfirm: true,
          closeOnCancel: true,
          confirmButtonText: 'Yes',
          cancelButtonText: 'No',
        },
        function(isConfirm){
            if (isConfirm){
                var timeoutID ;
                var fn = function(){
                  getisp(function(isp){
                    var data = {
                        'ID'          : btn.attr('data-empID'),
                        'isp'         : isp
                      };
                    $.ajax({
                        type: 'POST',
                        url: apiUrl+"/dashboard/reports/employee/removeThisEmployee",
                        data : data,
                        cache: false,
                        async: false,
                        success: function(response) {
                            requestInvalid(response);

                            if(response.success){
                                var table = $('.hmsEmployeeListTable').DataTable();
                                table
                                  .row( btn.parent().parent() )
                                  .remove()
                                  .draw();

                                swal("Success!", response.message, "success");
                            }
                            else
                                swal("Warning!", response.message, "warning");

                            window.clearTimeout(timeoutID);
                        },
                        error: function(err){
                            setTimeout(function(){
                                $('.hmsLoading2').addClass('hidden');
                                swal("Error!", err.statusText, "error");
                            },500);
                        }
                    });
                
                  });
                };


                timeoutID = window.setTimeout(fn, 500);
            }
        });
    }


    function getEmployeeForm(){
      var form = ('<div class="addEmployeeForm ">'+
           ' <div class="row">'+
                '<div class="form  col-sm-6" style="width: 100%">'+
                    '<div class="form-body">'+
                        '<div class="row">'+
                            '<h3 style="margin-top: 0px"><strong>Personal Information</strong></h3>'+
                            '<div class=" col-sm-4">'+
                                '<label>First Name</label>'+
                                '<input type="text" class="form-control empForm empFormEmpFName" placeholder="">' +
                            '</div>'+
                            '<div class="  col-sm-4">'+
                               ' <label>Last Name</label>'+
                                '<input type="text" class="form-control empForm empFormEmpLName" placeholder=""> '+
                            '</div>'+
                            '<div class="col-sm-4">'+
                                '<label>Middle Name</label>'+
                                '<input type="text" class="form-control empForm empFormEmpMname" placeholder="">' +
                            '</div>'+
                        '</div>'+
                        '<div class="row">'+
                            '<h3><strong>Work Description</strong></h3>'+
                            '<div class="col-sm-6">'+
                                '<label>Position</label>'+
                                '<div class="">'+
                                    '<div class="">'+
                                        '<input type="text" id="empFormEmpPosition" name="empFormEmpPosition" class="form-control empForm empFormEmpPosName" /> '+
                                   ' </div>'+
                               ' </div>'+
                            '</div>'+
                            '<div class="col-sm-6">'+
                               ' <label>Department</label>'+
                                '<div class="">'+
                                    '<div class="">'+
                                        '<input type="text" id="empFormEmpDepartment" name="empFormEmpDepartment" class="form-control empForm empFormEmpDepName" /> '+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>');
      return form;
    }


    function validateEmployeeForm(){
      var hasError = false;

      $.each($('.addEmployeeForm').find('.empForm'), function(){
        $(this).parent().find('span').empty();
        if($(this).val().length == 0){
          $(this).parent().addClass('has-error');
          $(this).parent().append('<span class="help-block">This is required</span>');
          if(!$(this).hasClass('tt-hint'))
            hasError = true;
        }
        else{
          $(this).parent().removeClass('has-error');
          $(this).parent().find('span').empty();
        }
        
      });



      return hasError;

    }


    function postThisEmployee(){
      var Fname       = $('.empFormEmpFName').val().toUpperCase();
      var Lname       = $('.empFormEmpLName').val().toUpperCase();
      var Mname       = $('.empFormEmpMname').val().toUpperCase();
      var Position    = $('#empFormEmpPosition').val().toUpperCase();
      var Department  = $('#empFormEmpDepartment').val().toUpperCase();

      getisp(function(isp){
        var data = {
          'Fname'         : Fname,
          'Lname'         : Lname,
          'Mname'         : Mname,
          'Position'      : Position,
          'isp'           : isp,
          'Department'    : Department
        };

        $.ajax({
            type: 'POST',
            url: apiUrl+"/dashboard/reports/employee/postThisEmployee",
            data : data,
            cache: false,
            async: false,
            success: function(response) {
                requestInvalid(response);

                if(response.success){
                  var table = $('.hmsEmployeeListTable').DataTable();
                  var row = table.row.add([
                        (data.Fname +" "+ data.Lname),
                        data.Position,
                        data.Department,
                        ('<button type="button" data-empID="'+response.newID+'" data-empFname="'+data.Fname +'" data-empLname="'+data.Lname+'" data-empMname="'+data.Mname+'" data-empPosition="'+data.Position+'" data-empDepartment="'+data.Department+'" class="btn btn-outline green hmsUpdateThisEmployee" ><i class="icon-pencil"></i> Update</button>'+
                         '<button type="button" data-empID="'+response.newID+'" class="btn btn-outline red hmsRemoveThisEmployee"><i class="icon-close"></i> Remove</button>') 
                      ]).draw().node();

                  $( row ).animate( { color: '#95A5A6' } );

                  swal("Success!", response.message, "success");
                }
                else{
                  swal("Warning!", response.message, "warning");
                }
            },
              error: function(err){
                  setTimeout(function(){
                      $('.hmsLoading2').addClass('hidden');
                      swal("Error!", err.statusText, "error");
                  },500);
              }
        });
      });
    }

    function showAddEmployeeForm(){
     
      bootbox.dialog({
            message: getEmployeeForm(),
            title: '<div><h3><strong><i class="icon-users" aria-hidden="true"></i> Add Employee</strong></h3></div>',
            buttons: {
              success: {
                label: "Submit",
                className: "green",
                callback: function() {
                  if(validateEmployeeForm()){  // has error
                    return false;
                  }

                  postThisEmployee();

                }
              },
              main: {
                label: "Cancel",
                className: "blue",
                callback: function() {
                    
                }
              }
            }
        });      
      initializedAutoSuggestValues();
      
    }

    function initializedAutoSuggestValues(){
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/reports/employee/getAllPositionsAndDepartments",
          // data : data,
          cache: false,
          async: false,
          success: function(response) {
              if(!response.has_login)
                logoutAccount();

              ComponentsTypeahead.init(response.data.positions, response.data.departments);
          },
            error: function(err){
                setTimeout(function(){
                    $('.hmsLoading2').addClass('hidden');
                    swal("Error!", err.statusText, "error");
                },500);
            }
      });
    }



    function loadEmployeePage(){
      $('.page-sidebar-menu').find('.nav-item').removeClass('active open');

      if($(this).parent().hasClass('nav-item')){
        $(this).parent().addClass('active open');
      }

      if($(this).parent().parent().hasClass('nav-item')){
        $(this).parent().parent().addClass('active open');
      }
      
      
      $('.dashboard-page').addClass('hidden').attr('hidden','');
      $('#hmsEmployeeContainer').removeClass('hidden').removeAttr('hidden');
      setTimeout(function(){
        getAllEmployees();
      },0);
    }



    function getAllEmployees(){
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/reports/employee/getAllEmployees",
          cache: false,
          async: false,
          success: function(response) {
              if(!response.has_login)
                logoutAccount();

              if(response.success){
                $('.hmsEmployeeListTable > tbody').find('tr').empty();
                var table = $('.hmsEmployeeListTable').DataTable();
                table.clear().draw();
                $.each(response.data.employees, function(i, n){
                  table.row.add([
                      (n.empFname +" "+ n.empLname),
                      n.position,
                      n.department,
                      ('<button type="button" data-empID="'+n.empID+'" data-empFname="'+n.empFname +'" data-empLname="'+n.empLname+'" data-empMname="'+n.empMname+'" data-empPosition="'+n.position+'" data-empDepartment="'+n.department+'" class="btn btn-outline green hmsUpdateThisEmployee" ><i class="icon-pencil"></i> Update</button>'+
                       '<button type="button" data-empID="'+n.empID+'" class="btn btn-outline red hmsRemoveThisEmployee"><i class="icon-close"></i> Remove</button>')
                    ]).draw(false)
                  
                });
              }
              else
                  swal("Warning!", response.message, "warning");
          },
            error: function(err){
                setTimeout(function(){
                    $('.hmsLoading2').addClass('hidden');
                    swal("Error!", err.statusText, "error");
                },500);
            }
      });
    }


}(); 



var ComponentsTypeahead = function () {

  var handleTwitterTypeahead = function(positions, departments) {

      // local: [
       //    { num: 'Nursing' },
       //    { num: 'Doctor' },
       //    { num: 'Secretary' },
       //    { num: 'Cashier' },
       //    { num: 'Accountant'}
       //  ]
      var numbers = new Bloodhound({
        datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.num); },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: positions
      });
       
      // initialize the bloodhound suggestion engine
      numbers.initialize();
       
      // instantiate the typeahead UI
      if (App.isRTL()) {
        $('#empFormEmpPosition').attr("dir", "rtl");  
      }
      $('#empFormEmpPosition').typeahead(null, {
        displayKey: 'num',
        hint: (App.isRTL() ? false : true),
        source: numbers.ttAdapter()
      });


      var numbers1 = new Bloodhound({
        datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.num); },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: departments
      });
       
      // initialize the bloodhound suggestion engine
      numbers1.initialize();

      if (App.isRTL()) {
        $('#empFormEmpDepartment').attr("dir", "rtl");  
      }
      $('#empFormEmpDepartment').typeahead(null, {
        displayKey: 'num',
        hint: (App.isRTL() ? false : true),
        source: numbers1.ttAdapter()
      });

      

  }


  return {
      //main function to initiate the module
      init: function (positions, departments) {
          handleTwitterTypeahead(positions, departments);
      }
  };

}();




jQuery(document).ready(function() {    
  Employee.init();
});