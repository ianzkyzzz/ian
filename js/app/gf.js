function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validateTextWithNumber(field) { // with whitespace
    var re = /^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/;
    return re.test(field);
}

function validateInteger(field) {
    var re = /^\d+$/;
    return re.test(field);
}

function validateTextOnly(field) {
    var re = /^[a-zA-Z]*$/;
    return re.test(field);
} 

function validateAlphanumeric(field) {
    var re = /^[a-z0-9]+$/i;
    return re.test(field);
}


function validateNumberAndDot(field) {
    var re = /^[0-9]*\.?[0-9]*$/;
    return re.test(field);
}

function toWeirdDate(s) {
   var months = 'Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec'.split(' ');
   var bits = s.split('-');
   return months[--bits[1]] + ' ' + bits[2] + ' ' + bits[0];
}


var gf_userRole = "";

function validatePasswordMatch(field1, field2) {
    if($.trim(field1) == $.trim(field2)) {
        return true;
    } 
 
    return false;
}

function ReplaceNumberWithCommas(yourNumber) {
    //Seperates the components of the number
    var n= yourNumber.toString().split(".");
    //Comma-fies the first part
    n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    //Combines the two sections
    return n.join(".");
}



function ValidateDate2(dtValue) //   mm/dd/yyyy or mm-dd-yyyy
{
    var dtRegex = new RegExp(/\b\d{1,2}[\/-]\d{1,2}[\/-]\d{4}\b/);
    return dtRegex.test(dtValue);
}


function ValidateDate(dtValue) //   yyyy/mm/dd or yyyy-mm-dd
{
    var dtRegex = new RegExp(/\b\d{4}[\/-]\d{1,2}[\/-]\d{1,2}\b/);
    return dtRegex.test(dtValue);
}



// amountToWord(accounting.unformat(accounting.format(amount,2)))
function amountToWord(amount){

  const arr = x => Array.from(x);
  const num = x => Number(x) || 0;
  const str = x => String(x);
  const isEmpty = xs => xs.length === 0;
  const take = n => xs => xs.slice(0,n);
  const drop = n => xs => xs.slice(n);
  const reverse = xs => xs.slice(0).reverse();
  const comp = f => g => x => f (g (x));
  const not = x => !x;
  const chunk = n => xs =>
    isEmpty(xs) ? [] : [take(n)(xs), ...chunk (n) (drop (n) (xs))];


  // numToWords :: (Number a, String a) => a -> String
  let numToWords = n => {
    let a = [
      '', 'one', 'two', 'three', 'four',
      'five', 'six', 'seven', 'eight', 'nine',
      'ten', 'eleven', 'twelve', 'thirteen', 'fourteen',
      'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
    ];
    
    let b = [
      '', '', 'twenty', 'thirty', 'forty',
      'fifty', 'sixty', 'seventy', 'eighty', 'ninety'
    ];
    
    let g = [
      '', 'thousand,', 'million,', 'billion,', 'trillion,', 'quadrillion,',
      'quintillion,', 'sextillion,', 'septillion,', 'octillion,', 'nonillion,'
    ];
    
    // this part is really nasty still
    // it might edit this again later to show how Monoids could fix this up
    let makeGroup = ([ones,tens,huns]) => {
      return [
        num(huns) === 0 ? '' : a[huns] + ' hundred ',
        num(ones) === 0 ? b[tens] : b[tens] && b[tens] + '-' || '',
        a[tens+ones] || a[ones]
      ].join('');
    };
    
    let thousand = (group,i) => group === '' ? group : `${group} ${g[i]}`;
    
    if (typeof n === 'number')
      return numToWords(String(n));
    else if (n === '0')
      return 'zero';
    else
      return comp (chunk(3)) (reverse) (arr(n))
        .map(makeGroup)
        .map(thousand)
        .filter(comp(not)(isEmpty))
        .reverse()
        .join(' ');
  };

  amount = (''+amount+"");

  amount = amount.split('.');

  var firstPart  = numToWords(amount[0]) + ' peso ';

  var decimal = "";
  if(amount[1] != undefined)
    decimal  = ' and '+numToWords(amount[1]) + ' cents';

  return firstPart + decimal;
}




// mm/dd/yyyy
function isDate(txtDate)
{
  var currVal = txtDate;
  if(currVal == '')
    return false;

   
  //Declare Regex 
  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
  var dtArray = currVal.match(rxDatePattern); // is format OK?

  if (!rxDatePattern.test(currVal))
     return false;




  //Checks for mm/dd/yyyy format.
  dtMonth = dtArray[1];
  dtDay= dtArray[3];
  dtYear = dtArray[5];
 
  if (dtMonth < 1 || dtMonth > 12){
      return false;
  }
  else if (dtDay < 1 || dtDay> 31){
      return false;
  }
  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31){
      return false;
  }
  else if (dtMonth == 2)
  {

     var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
     if (dtDay> 29 || (dtDay ==29 && !isleap)){
          return false;
     }
  }

  
  return true;
}




var apiUrl = 'api/csm_v1/public/';

var getThisUserFn;
function gotToMyProfile(id){
    // getThisUserFn(id);
}

function setThis(fn){
    // getThisUserFn = fn
}

function logoutAccount(evt){
    showLoading();
    
    $.ajax({
        type: 'GET',
        url: apiUrl+"logout",
        cache: false,
        async: true,
        success: function(response) {
            if (response.success==true) {
                setTimeout(function() {
                    window.location.href = "/";
                }, 1000);
            }
            else{
                hideLoading();
            }
        },
        error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
        }
    });

}


function isLogin(){
    $.ajax({
        type: 'GET',
        url: apiUrl+"isLogin",
        cache: false,
        async: false,
        success: function(response) {
            if (response.login == false) {
                logoutAccount();
            }
            else{
               $('.hmsLoading').addClass('hidden');
            }
        },
        error: function(err){
            logoutAccount();
            // swal("Error!", err.statusText, "error")
             console.log(err.statusText);
        }
    });
}


function validateUser(){
    // setTimeout(function(){
    //     $.ajax({
    //         type: 'GET',
    //         url: apiUrl+"isLogin",
    //         cache: false,
    //         async: true,
    //         success: function(response) {
    //             if (response.login == false) {
    //                 logoutAccount();
    //             }
    //             else{
    //                 validateUser();
    //             }
    //         },
    //         error: function(err){
    //             validateUser();
    //             // swal("Error!", err.statusText, "error");
    //             console.log(err.statusText);
    //         }
    //     });
    // },10000);
}







// window.clearTimeout(loadInBackground);
var loadInBackground;
function confirmWithSwal(funct , title, type){
	swal({
	  title: title,  // title
	  type: type,           //'warning'
	  showConfirmButton: true,
	  showCancelButton: true,
	  confirmButtonClass: 'btn-info',
	  cancelButtonClass: 'btn-danger',
	  closeOnConfirm: true,
	  closeOnCancel: true,
	  confirmButtonText: 'Yes',
	  cancelButtonText: 'No',
	},
	function(isConfirm){
		if (isConfirm){
			loadInBackground = window.setTimeout(funct, 100);
		}
	});
}



function showLoading(msg){
    if(msg == undefined){
        msg = "";
    }
    $('.hmsLoading2').removeClass('hidden');
    $('.loadingMessage').html(msg);
    setTimeout(function(){},0);
}


function hideLoading(msg){
    if(msg == undefined){
        msg = "";
    }
    setTimeout(function(){
        $('.hmsLoading2').addClass('hidden');
        $('.loadingMessage').html(msg);
    },100);
}


function formatDate(date) {
  const d = new Date(date);
  var monthNames = [
    "Jan", "Feb", "Mar",
    "Apr", "May", "Jun", "Jul",
    "Aug", "Sep", "Oct",
    "Nov", "Dec"
  ];

  var day = d.getDate();
  var monthIndex = d.getMonth();
  var year = d.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
  console.log(date);

}


function GetTodayDate() {
   var tdate = new Date();
   var dd = tdate.getDate(); //yields day
   var MM = tdate.getMonth(); //yields month
   var yyyy = tdate.getFullYear(); //yields year
   var currentDate = yyyy + "-" +( MM+1) + "-" + dd;

   return currentDate;
}
 





