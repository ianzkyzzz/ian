var Login = function () {

	var handleLogin = function() {
		$('.btn_login').on('click',submitForm);


        $('.login-form input').keypress(function (e) {
            if (e.which == 13) {
                $('.login-notification').addClass('display-hide');
	            submitForm();
                return false;
            }
        });



        function inputValidation(){
        	var error = false;
        	

        	$.each($('.login-form input'), function(i,n){
        		if($(this).val().trim()==''){
        			error = true;
        			$(this).parent().parent().addClass('has-error');
        		}
        		else{
        			$(this).parent().parent().removeClass('has-error');
        		}
        	});
        }


        function submitForm(){
        	if(inputValidation()){
        		return false;
        	}
        	
        	var uname = $('.input_username').val();
			var pword = $('.input_password').val();
            var data = {
                'username'  : uname,
                'password'  : pword
            };
        	$.ajax({
                type    : 'POST',
                url     : apiUrl + '/', 
                data    : data,
                cache   : false,
                async   : false,
                success : function(response){
                    if( response.success == true ){
                        setTimeout(function() {
                            window.location.href = "/";
                        }, 1000);
                    }else{
                        setTimeout(function() {
                            $('.login-notification').removeClass('display-hide');
                            $('.login-msg').html('Invalid Email/Password!');
                            $('#login-pword').val('');
                        }, 1000);
                        
                    } 
                },
	            error: function(err){
	                console.log(err.statusText);
	            }
            });
        }
	}



    
    return {
        init: function () {
            handleLogin();
        	loadLoginForm();
        }
    };



    function loadLoginForm(){
    	$.ajax({
			type: 'GET',
            url: apiUrl,
            cache: false,
            async: false,
            success: function(response) {
            	setTimeout(function() {
                	$('.hmsLoading').addClass('hidden');    
                }, 1000);

            },
            error: function(err){
                console.log(err.statusText);
            }

		});
    }
 
}();

jQuery(document).ready(function() {
    Login.init();
});