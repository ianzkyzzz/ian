var clientPayments = (function($) {
    var templates = [],
        baseTemplateUrl = 'template/payments/';
    var payamentPropAddress;
    var paymentPropLocationAddress;

    var buyerName;
    var buyerID;
    var buyerOR;
    var buyerrProperty;
    var buyerPaymentDate;
    var buyerPaymentAmount;
    var notes;
    var orexist = 0;
    var chargesData = [];
    var clientWithChargeID = 0;
    var fullPaymentRemaining = 0;
    var selected=1;
    var fullPaymentTCP = 0;
    var orincrement = 0;

    return {
        init: init
    };

    // Init
    function init() {
        addEventHandlers();
    }

    function getTemplate(baseTemplateUrl, templateName, callback) {
        if (!templates[templateName]) {
            $.get(baseTemplateUrl + templateName, function(resp) {
                compiled = _.template(resp);
                templates[templateName] = compiled;
                if (_.isFunction(callback)) {
                    callback(compiled);
                }
            }, 'html');
        } else {
            callback(templates[templateName]);
        }
    }

    function clienPaymentFormValidation(){
        var form1 = $('.paymentForm');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input
            messages: {
                payment: {
                    maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                    minlength: jQuery.validator.format("At least {0} items must be selected")
                },
                'checkboxes1[]': {
                    required: 'Please check some options',
                    minlength: jQuery.validator.format("At least {0} items must be selected"),
                },
                'checkboxes2[]': {
                    required: 'Please check some options',
                    minlength: jQuery.validator.format("At least {0} items must be selected"),
                }
            },

            rules: {
                addPaymentClientList: {
                    required: true
                },
                addPaymentPropertyList: {
                    required: true
                },
                paymentCredit: {
                    required: true
                },
                paymentornumber: {
                    required: true
                },
                paymentDate: {
                    date: true,
                    required: true
                }
            },

            invalidHandler: function(event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },

            errorPlacement: function(error, element) {
                if (element.is(':checkbox')) {
                    error.insertAfter(element.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline"));
                } else if (element.is(':radio')) {
                    error.insertAfter(element.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },

            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function(form) {
                success1.show();
                error1.hide();


                checkornumber();
                return false;
            }
        });
    }

    var clientPaymentHistoryTable;
    function tableclientPaymentHistoryTable(){
        var table = $('.transactionPaymentHistoryTable');
        clientPaymentHistoryTable =  table.dataTable({
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No client payment added yet",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            // bootstrap_full_number
            "pagingType": "bootstrap_extended",
            "order": [
                [0, 'desc']
            ],
            responsive: true,
            "lengthMenu": [
                [5, 10, 15, 20, 50,100],
                [5, 10, 15, 20, 50,100] // change per page values here
            ],
            buttons: [
                {
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    message: 'Client property payment history'
                    // exportOptions: {
                    //     columns: [ 0, 1]
                    // }
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline'
                },
                {
                    extend: 'excel',
                    className: 'btn purple btn-outline ' ,
                    extension: '.xls'
                    // exportOptions: {
                    //     columns: [ 0, 1]
                    // }
                },
                {
                    text: '<i class="fa fa-lg fa-copy"></i> COPY',
                    extend: 'copy',
                    className: 'btn btn-outline red  p-5 m-0 width-35 assets-export-btn export-xls ttip'
                    // exportOptions: {
                    //     columns: [ 0, 1]
                    // }
                }
            ],
            // set the initial value
            "pageLength": 10,
            destroy : true
        });


        var table = $('.transactionPaymentHistoryTable').DataTable();
        table.on( 'order.dt search.dt', function () {
            table.column(5, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = accounting.format(cell.innerHTML,2);
            } );
        } ).draw();

        $('.paymentHistoryPaymentExportTools').empty();
        table.buttons( 0, null).container().appendTo( '.paymentHistoryPaymentExportTools' );

    }

    // Event Handlers
    function addEventHandlers() {
        $(document).on('click','.openPaymentPage', loadClientPaymentPage );
        $(document).on('change','.addPaymentClientList',addPaymentClientListChange);
        $(document).on('change','.addPaymentPropertyList',addPaymentPropertyListChange);
        $(document).on('click','.btnPaymentSubmit', checkornumber);

        $(document).on('change','.paymentCredit',paymentCreditIsChange);
        $(document).on('click','.btn_updatePayment',updatePaymentTable);
        $(document).on('click','#btn_adminSet',show_administratorOption);
        $(document).on('click','#btn_addCharges_p',add_newAddtionalCharges);
        $(document).on('click','#btn_newCharges_p',add_newAddtionalChargesList);
        $(document).on('click','.btn_printThisPayment', print_thisPayment);
        $(document).on('click','.btnSubmitFullPayment',submitThisFullPayment);
        $(document).on('keyup','.fullPaymentDiscount',changeBalanceWithDiscount);
        $(document).on('click','#tab_paymentTab', function (argument) {
            updateTransactionPaymentHistory();
        });

        // $(document).on('change','#changeDate', function(){
        //     bootbox.prompt({
        //         className: 'AdminPassword',
        //         title: "Administrator Password Required!",
        //         inputType: 'password',
        //         callback: function(result){
        //           if (result === null) {
        //           } else {
        //             checkPasswordDateChange(result);
        //           }
        //         }
        //       });
        // });
        
        $(document).on('click','.fullPaymentTab',populateDataForFullPayment);
        $(document).on('click','#remove_thisForm',function () {
            $(this).parent().parent().empty();    
        });

        $(document).on('change','.addPaymentPropertyList2', function(){
            udpateClientPaymenthistory();
        });
        $(document).on('change','.modeOfPayment2', function(){
            showSelectOption();
        });

        $(document).on('click','.btn-refresh', function(){
            loadClientPaymentPage();
        });

        $(document).on('click','.btn_DeletePayment', function(){
            // alert($(this).attr('data-id'));
            deleteThisPayment($(this).attr('data-id'));
        });
        $(document).on('change','#transType',()=>{
            const type = $('#transType').val();
            if(type == 0){
                    $('.banksz').addClass('hidden');
            }else{
                $('.banksz').removeClass('hidden');

            }
        });
        $(document).on('change','.paymentoptions',paymentOptions);
    }

    function paymentOptions(){
        selected = $('.paymentoptions').val();
        addPaymentPropertyListChange();

       if(selected == 1){
           $('.multiple').addClass('hidden');
           $('.single').removeClass('hidden');

       }else{
           $('.single').addClass('hidden');
           $('.multiple').removeClass('hidden');
           

       }
   }

   function checkPasswordDateChange(password) {
    $.ajax({
       url: apiUrl+"dashboard/payment/checkAdminPass",
       type: 'GET',
       data : {pass: password},
       cache: false,
       async: true,
       success: function(response){
           hideLoading("");
           if (response.success) {
            // $('.paymentmadeDate').val(new Date().toJSON().slice(0,10).replace(/-/g,'-'));           
           }else{
               alert('Invalid Password');
               $('.paymentDate').val(new Date().toJSON().slice(0,10).replace(/-/g,'-'));
               // $('.paymentmadeDate').val(new Date().toJSON().slice(0,10).replace(/-/g,'-'));

           }
       },

       error: function(err){
           hideLoading("");
           setTimeout(function(){swal("Error!", err.statusText, "error");},0);
       }
   });
}

    function submitThisFullPayment(){
        var data = {
            clientID                    : $('.fullPaymentClient').val(),
            addPaymentPropertyList      : $('.fullPaymentPropertyList').val(),
            modeOfPayment               : $('.modeOfFullPayment').val(),
            amount                      : $('.fullPaymentBalance').val(),
            paymentornumber             : $('.fullpaymentOrnumber').val(),
            // paymentmadeDate             : $('.paymentmadeDate').val(),
            paymentDate                 : $('.fullPaymentDate').val(),
            remitanceCenterName         : $('.fullpaymentRemitanceCenter').val(),
            paymentAgentCommision       : 0.00,
            paymentStatus               : 1,
            cp_id                       : $('.fullPaymentPropertyList').val(),
            notes                       : $('.paymentNote').val(),
            discount                    : $('.fullPaymentDiscount').val()
        };
        initializeDataFullPayment();
        submitPayment(data);
    }

    function initializeDataFullPayment(){
        let data ={
            AR_dateRelease      : $('.fullPaymentDate').val(),
            AR_clientName       : $('.fullPaymentClient option:selected').text(),
            AR_amountInText     : amountToWord($('.fullPaymentBalance').val()).toUpperCase(),
            AR_amount           : accounting.format($('.fullPaymentBalance').val(),2),
            AR_address          : payamentPropAddress,
            AR_ornumber         : $('.fullpaymentOrnumber').val(),
            AR_lotArea          : $('.fullPaymentPropertyList option:selected').text()
        }
        const printContainer = $('.print-previews');
        printContainer.empty();
        printContainer.append(printForm(data));
    }


    function populateDataForFullPayment(){
        $.ajax({
            type:'GET',
            url:apiUrl+'/dashboard/client/getAllNames',
            cache:false,
            async:true,
            success: function(response){
                if(response.success){
                     $('.fullPaymentClient').empty();
                    $.each(response.clients,function(i,n){
                        $('.fullPaymentClient ').append('<option value="'+n.clientID+ '">' + n.clientName+ '</option>');
                    });
                    populatePropertyNameFullPayment($('.fullPaymentClient').val());
                 
        
                }
            },
            error:function(err){

            }
        });
    }

    function populatePropertyNameFullPayment(id){
        showLoading();
        $.ajax({
            type:'GET',
            url:apiUrl+'/dashboard/client/getAllProperties',
            data:{id:id},
            cache:false,
            async:true,
            success:function(response){
                hideLoading();
                if(response.success){
                    $('.fullPaymentPropertyList').empty();
                    $.each(response.clientsProperties,function(i,n){
                        $('.fullPaymentPropertyList ').append('<option value="'+n.clientPropertyID+ '">' + n.propertyName+ '</option>');
                    });
                    getBalanceForFullPayment($('.fullPaymentPropertyList').val(),id);
                }
            },
            error:function(err){
                hideLoading();
            }
        });
    }

    function getBalanceForFullPayment(id,cpid){
        
       
        showLoading();
        $.ajax({
            type:'GET',
            url:apiUrl+'/dashboard/client/paymentHistoryForThisProperty',
            data:{id:id,cpID:cpid},
            cache:false,
            async:true,
            success:function(response){
                hideLoading();
                    if(response.success){

                        $('.fullPaymentBalance').val(accounting.format(response.balance,2));
                        fullPaymentTCP = response.contract_price;
                        fullPaymentRemaining= response.balance;
                    }
            },
            error:function(err){
                hideLoading();
            }
        });
    }


    function checkPaymentOption(){
      var bankRemitanceCenter="";

        if($('.modeOfPayment2').val() == 3 || $('.modeOfPayment2').val() == 2){
            bankRemitanceCenter = $('.remitanceCenterName2').val();
        }else{
            bankRemitanceCenter = "Cash";
        }

      return bankRemitanceCenter;
    }

    function checkOrChanges(){
        var orVal ="";
        if($('.paymentornumber2').val() == buyerOR ){
            orVal = buyerOR;
        }else{
            orVal = $('.paymentornumber2').val();
        }
        return orVal;
    }

    // fucntions
    function updteTransaction(paymentDesc, payMentDate){
        var paymentAmount   = $('.paymentCredit2').val();        

        console.log(paymentDesc);

        $.ajax({
            url: apiUrl+"dashboard/payment/updateClientPaymentTransaction",
            type: 'POST',
            data : {orNumber: checkOrChanges(), paymentOption: checkPaymentOption(), paymentDate :payMentDate, orNumberOld: buyerOR, payment: paymentAmount, payment_id: buyerID, notes: paymentDesc},
            cache: false,
            async: true,
            success: function(response){
                hideLoading("");
                if (response.success==true) {
                  // alert(response.message);
                    setTimeout(function(){swal("Success!", response.message, "success");},0);
                    updateTransactionPaymentHistory();
                }
                else{
                    setTimeout(function(){swal("Warning!", response.message, "warning");},0);
                }

            },

            error: function(err){
                hideLoading("");
                setTimeout(function(){swal("Error!", err.statusText, "error");},0);
            }
        });

    }



    function showSelectOption(){
        if($('.modeOfPayment2').val() == 1){

           $('.remitanceCenter2').addClass('hidden');

        }else{

           $('.remitanceCenter2').removeClass('hidden');

           if($('.modeOfPayment2').val() == 2){
                $('#paymentType').text('Bank Name');
           }else{
                $('#paymentType').text('Remitance Center');
           }

        }
    }


    function setPropertyListDetails(property){

    }

    function udpateClientPaymenthistory(){
        var propertId = $('.addPaymentPropertyList2').val();

        $.ajax({
            url: apiUrl+"dashboard/payment/getClientPaymentHistoryDetails",
            type: 'GET',
            data : {orNumber: buyerID, propertyId: propertId},
            cache: false,
            async: true,
            success: function(response){
                hideLoading("");
                if (response.success==true) {
                  alert(response.message);
                }
                else{
                    setTimeout(function(){swal("Warning!", response.message, "warning");},0);
                    loadClientPaymentPage();
                }

            },

            error: function(err){
                hideLoading("");
                setTimeout(function(){swal("Error!", err.statusText, "error");},0);
            }
        });
    }

    function checkAdminPass(password) {
         $.ajax({
            url: apiUrl+"dashboard/payment/checkAdminPass",
            type: 'GET',
            data : {pass: password},
            cache: false,
            async: true,
            success: function(response){
                hideLoading("");
                if (response.success) {
                    ModalupdateTable();
                }else{
                    alert('Invalid Password');
                }

            },

            error: function(err){
                hideLoading("");
                setTimeout(function(){swal("Error!", err.statusText, "error");},0);
            }
        });
    }
    

    function getPaymentDesc(buyerID) {
        $.ajax({
            url: apiUrl+"dashboard/payment/getPaymentDescription",
            type: 'GET',
            data : {id: buyerID},
            cache: false,
            async: true,
            success: function(response){

                if (response.success==true) {
                    notes  = response.paymentDes;
                    buyerPaymentDate  = response.dateadded;
                }else{
                    setTimeout(function(){swal("Warning!", response.message, "warning");},0);
                }
            },

            error: function(err){
                hideLoading("");
                setTimeout(function(){swal("Error!", err.statusText, "error");},0);
            }
        });
    }


    function updatePaymentTable(){

        bootbox.prompt({
          className: 'AdminPassword',
          title: "Administrator Password Required!",
          inputType: 'password',
          callback: function(result){
            if (result === null) {
            } else {
                checkAdminPass(result);
            }
          }
        });

        buyerID =  $(this).attr("data-id");

        getPaymentDesc(buyerID);

        $('.transactionPaymentHistoryTable tr').each(function() {
            if (buyerID == $(this).find("td").eq(0).html()) {
                 buyerName          = $(this).find("td").eq(2).html();
                 buyerOR            = $(this).find("td").eq(1).html();
                 buyerPaymentAmount = $(this).find("td").eq(5).html();
                 buyerProperty      = $(this).find("td").eq(3).html();
                 transactionType    = $(this).find("td").eq(6).html();
            }
        });


        // get buyer properties
    }

    function ModalupdateTable(){
        showLoading('preparing client...');
        $.ajax({
            url: apiUrl+"dashboard/payment/updateTransactionPaymentHistoryTable",
            type: 'GET',
            data : {orNumber: buyerID},
            cache: false,
            async: true,
            success: function(response){
                hideLoading("");
                if (response.success==true) {

                    $('.addPaymentPropertyList2').empty();
                    $.each(response.clientPaymentTransactionHistory,function(i,n){
                        $('.addPaymentPropertyList2 ').append('<option value="'+n.property_id+ '">' + n.propertyName + ' Phase ' + n.phaseNumber + ' Block ' + n.block +  ' Lot ' + n.lot + '</option>');

                    });

                    // console.log(response.clientPaymentTransactionHistory);
                    // updateTransactionPaymentHistory();
                }else{
                    setTimeout(function(){swal("Warning!", response.message, "warning");},0);
                }

            },

            error: function(err){
                hideLoading("");
                setTimeout(function(){swal("Error!", err.statusText, "error");},0);
            }
        });

        var amountPayment = buyerPaymentAmount.replace(/\,/g,'');
        let transactionTypeForm =  "hidden";
        if (transactionType != 'Cash') {
            transactionTypeForm = '';

        }

        bootbox.confirm({
            size: 'large',
            title: "Payment Record",
            message: ' <div class="form-body">'+
                            '<div class="form-group">'+
                                '<div class="col-md-4">'+
                                    '<div class="form-group form-md-line-input">'+
                                       ' <input type="text" readonly class="form-control textBuyerName" value="' + buyerName +'" name="">'+
                                        '<label for="form_control_1">Client Name</label>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                    '<div class="form-group form-md-line-input">'+
                                        // '<select class="form-control edited  addPaymentPropertyList2" name="addPaymentPropertyList2">'+

                                        // '</select>'+
                                        '<input type="text" class="form-control paymentCredit" readonly value="'+ buyerProperty +'" id="form_control_1" name="paymentCredit">'+
                                       // ' <input type="text" readonly class="form-control textBuyerProperty" value="' + buyerProperty +'" name="">'+
                                        '<label for="form_control_1">Property Name</label>'+
                                    '</div>'+
                                '</div>'+
                            
                            '</div>'+
                            '<div class="form-group">'+
                                '<div class="col-md-3">'+
                                    '<div class="form-group form-md-line-input">'+
                                        '<select class="form-control edited modeOfPayment2" id="modeOfPayment2">'+
                                            '<option value="1">Cash</option>'+
                                            '<option value="2">Bank</option>'+
                                            '<option value="3">Remittance Center</option>'+
                                        '</select>'+
                                        '<label for="form_control_1">Mode of Payment</label>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                    '<div class="form-group form-md-line-input">'+
                                        '<input type="number" class="form-control paymentCredit2" value="'+ amountPayment +'" id="form_control_1" name="paymentCredit">'+
                                        '<label for="form_control_1">Amount</label>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="form-group">    '+
                                    '<div class="col-md-4">'+
                                        '<div class="form-group form-md-line-input">'+
                                            '<input type="text" class="form-control date-picker paymentDate2" value="'+ buyerPaymentDate +'" required id="form_control_1" name="paymentDate">'+
                                            '<label for="form_control_1">Date</label>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                
                                // '<div class="col-md-3">'+
                                //     '<div class="form-group form-md-line-input">'+
                                //         '<input type="text" readonly class="form-control paymentAgentCommision" required id="form_control_1" name="paymentAgentCommision">'+
                                //         '<label for="form_control_1">Agent Commision</label>'+
                                //     '</div>'+
                                // '</div>'+

                                '<div class="col-md-3 ">'+
                                    '<div class="form-group form-md-line-input ">'+
                                        '<input type="text" class="form-control paymentornumber2" value="' + buyerOR + '" oldorID="'+ buyerOR +'" id="form_control_1" name="paymentornumber">'+
                                        '<label for="form_control_1">OR Number</label>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                    '<div class="form-group form-md-line-input remitanceCenter2 '+ transactionTypeForm +'">'+
                                        '<input type="text" class="form-control remitanceCenterName2"  id="form_control_1"  value ="'+ transactionType +'"name="remitanceCenter2">'+
                                        '<label for="form_control_1" id="paymentType"></label>'+
                                    '</div>'+
                                '</div>'+
                           '</div>'+
                           '<div class="form-group">'+
                                '<div class="row">'+
                                    '<div class="col-md-6">'+
                                        '<div class="form-group form-md-line-input ">'+
                                            ' <textarea class="form-control paymentNote" name="paymentNote" id="txt_notes" cols="30" rows="2" placeholder=" Add payment description" value="'+ notes +'"> '+notes+ '</textarea>'+
                                            '<label for="form-note">Note :</label>'+
                                        '</div>'+
                                    '</div>    '+
                                '</div>'+
                           '</div>'+
                        '</div>',
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label    : '<i class="fa fa-check"></i> Update',
                    className: 'btn-upate-transaction btn btn-primary'
                }
            },
            callback: function (result) {
                if (result) {
                    const notes = $('#txt_notes').val();
                    const datePayment   = $('.paymentDate2').val();

                    updteTransaction(notes, datePayment);

                }
            }
        });
    
        if (transactionType != 'Cash') {
            $('.modeOfPayment2').val(3);

        }

    
        $('.date-picker').datepicker({
          format: "yyyy-mm-dd",
          todayHighlight: true
        });

    }


    function paymentCreditIsChange(){
        // var value = $('.paymentCredit').val().trim();
        // if(value != ""){
        //     if(validateNumberAndDot(value)){
        //         // $('.paymentAgentCommision').val(accounting.format((parseFloat(value) * 0.10),2));
        //         $('.paymentAgentCommision').val(accounting.format((parseFloat(value) * 0.10),8));
        //         $('.paymentAgentCommision').parent().removeClass('has-error');
        //     }
        //     else
        //         $('.paymentAgentCommision').parent().addClass('has-error');
        // }
        // else
        //     $('.paymentAgentCommision').parent().addClass('has-error');
    }


    function validatePaymentValues(){
        var hasError = false;
        var modeOfPayment = 1;
        $.each($('.paymentForm select, .paymentForm input, .paymentForm textarea'),function(){
            var e = $(this);

            if(e.hasClass('paymentCredit') || e.hasClass('paymentornumber') || e.hasClass('addPaymentClientList')  || e.hasClass('addPaymentPropertyList')
                     || e.hasClass('paymentAgentCommision') || e.hasClass('remitanceCenterName') || e.hasClass('paymentDate') || e.hasClass('modeOfPayment') || e.hasClass('paymentNote')){

                if(e.hasClass('paymentDate')){
                    if(!ValidateDate(e.val())){
                        hasError = true;
                        e.parent().addClass('has-error');
                    }
                    else
                        e.parent().removeClass('has-error');
                }
                else if(e.hasClass('paymentCredit')){
                    if(e.val().trim() == ""){
                        hasError = true;
                        e.parent().addClass('has-error');
                    }
                    else if(!validateNumberAndDot(e.val())){
                        hasError = true;
                        e.parent().addClass('has-error');
                    }
                    else if(parseFloat(e.val()) >= parseFloat($('.btnPaymentSubmit').attr('data-balance'))+1){
                        hasError = true;
                        e.parent().addClass('has-error');
                    }
                    else
                        e.parent().removeClass('has-error');

                }
                else if(e.hasClass('paymentornumber')){
                    if(e.val().trim() == ""){
                        hasError = true;
                        e.parent().addClass('has-error');
                    }
                    else
                        e.parent().removeClass('has-error');
                }
                else if(e.hasClass('modeOfPayment')){
                    if(!(e.val() == 1 || e.val() == 2 || e.val() == 3)){
                        hasError = true;
                        e.parent().addClass('has-error');
                    }
                    else if( e.val() == 2 || e.val() == 3){
                        modeOfPayment = e.val() ;
                    }
                    else
                        e.parent().removeClass('has-error');
                }
                else if(e.hasClass('remitanceCenterName')){
                    if(( modeOfPayment == 2 || modeOfPayment == 3)){
                        if(e.val() == ""){
                            hasError = true;
                            e.parent().addClass('has-error');
                        }
                        else
                            e.parent().removeClass('has-error');
                    }
                    else
                        e.parent().removeClass('has-error');
                // }
                // // else if(e.hasClass('addPaymentClientList') || e.hasClass('addPaymentPropertyList')){
                // //     if(e.val() == "" || e.val() == null){
                // //         hasError = true;
                // //         e.parent().addClass('has-error');
                // //     }
                // //     else
                // //         e.parent().removeClass('has-error');
                }else if(e.hasClass('paymentNote')){
                    if(e.val() == "" || e.val() == null){
                        hasError = true;
                        e.parent().addClass('has-error');
                    }else if(e.val().trim().length < 12){
                        e.parent().addClass('has-error');
                        hasError = true;
                    }else
                        e.parent().removeClass('has-error');
                }

            }
        });
        return hasError;
    }


    function postPayment(){

     
        var paymentStat ="";
        var ballance    = $('.paymentBalance').val();
        var cp_id       = $('.addPaymentPropertyList').val();
        var payMentDate = $('.paymentDate').val();
        var option = $('.paymentoptions').val();
        if(Number(ballance.replace(",", "")) == Number($('.paymentCredit').val())){
            paymentStat = 1;
        }else{
            paymentStat = 0;
        }

      
            var balance = $('.btnPaymentSubmit').attr('data-balance');

            var data = {
                    clientID                    : $('.addPaymentClientList').val(),
                    addPaymentPropertyList      : $('.addPaymentPropertyList').val(),
                    modeOfPayment               : $('.modeOfPayment').val(),
                    amount                      : $('.paymentCredit').val(),
                    paymentornumber             : $('.paymentornumber').val(),
                    paymentDate                 : $('.paymentDate').val(),
                    // paymentmadeDate             : $('.paymentmadeDate').val(),
                    remitanceCenterName         : $('.remitanceCenterName').val(),
                    paymentAgentCommision       : $('.paymentAgentCommision').val(),
                    paymentStatus               : paymentStat,
                    cp_id                       : cp_id,
                    notes                       : $('.paymentNote').val()
                };
    
            initializeData();
            submitPayment(data);
        
           
        

      

    }

    function changeBalanceWithDiscount(){
        console.log(parseFloat(fullPaymentRemaining).toFixed(2)+' '+accounting.format(fullPaymentTCP));
        if($('.fullPaymentDiscount').val() == 0){
            var discount            = 0;
            var discountedAmount    = (fullPaymentRemaining - (fullPaymentTCP * discount));
            $('.fullPaymentBalance').val(parseFloat(discountedAmount).toFixed(2));
        }else{
            var discount            = (parseInt($('.fullPaymentDiscount').val())/100);
            var discountedAmount    = (fullPaymentRemaining - (fullPaymentTCP * discount));
            $('.fullPaymentBalance').val(parseFloat(discountedAmount).toFixed(2));
        }

    }

    function submitPayment(data) {
        var amount = $('.paymentBalance').val();
        var amountcredit = data[3];
        var option = $('.paymentoptions').val();
        var repl = accounting.format(amountcredit);
        if(option == 2 && amount !=repl){
            swal('Warning!','Enter full payment!', 'warning');
        }else{
            var content = document.getElementById('acknowledgementReceipt').innerHTML;
            var printView =  document.getElementById('printPreview').innerHTML;
            showLoading('submitting payment...');
                $.ajax({
                    url: apiUrl+"dashboard/payment/postThisPayment",
                    type: 'POST',
                    data : data,
                    cache: false,
                    async: true,
                    success: function(response){
                        hideLoading("");
    
                        if (response.success==true) {
                            $('.paymentNote').val("");
                            $('.paymentornumber').val("");
                            // loadClientPaymentPage();
                            updateTransactionPaymentHistory();
                            addPaymentPropertyListChange();
                            
                            
                          orincrement +=1;
                            console.log(orincrement);
                           
                           
                            // $(".paymentornumber").attr("placeholder", data['paymentornumber']+1);
                            console.log(clientWithChargeID+'  '+ data.clientID);
                                if(clientWithChargeID == data.clientID){
                                    updateChargesDetails(data.clientID,data.addPaymentPropertyList,chargesData);
                                    console.log('same');
                                }else{
                                    console.log('not ');
    
                                }
                                
                            
                         
                            bootbox.dialog({
                                message: printView,
                                title: "Acknowledgement Receipt",
                                // onEscape: function() {  },
                                // backdrop: true,
                                buttons: {
                                  success: {
                                        label: "Print",
                                        className: "green btn-outline rmma-btn-payment-print",
                                        callback: function() {
                                            var html="<html>";
                                            // html+="<head><style>@page {size: auto;margin: 0cm; padding: 0; width: 100%; height: 100%; background-color: white;}@media print {#officialReceipt { page-break-after: always; width: 100%; margin: 0; padding: 0; background-color: white;} body , body > *{width: 100%; height:100%; margin: 0; padding: 0; background-color: white;}}</style></head>";
                                            html+= content;
                                            html+="</html>";
                                            var printWin = window.open();
                                            printWin.document.write(html);
                                            printWin.document.close();
                                            printWin.focus();
                                            printWin.print();
                                            printWin.close();
                                        }
                                    }
                                },
                            }).init(function(){
                                hideLoading();
                            });
    
                            // setTimeout(function(){swal("Success!", response.message, "success");},0);
                        }
                        else{
                            setTimeout(function(){swal("Warning!", response.message, "warning");},0);
                        }
                    },
                    error: function(err){
                        hideLoading("");
                        setTimeout(function(){swal("Error!", err.statusText, "error");},0);
                    }
                });

        }

       
    }


    function checkornumber(){
       
        if(validatePaymentValues()){
            return false;
        }else{
            var setOrnumber = $('.paymentornumber').val();
            $.ajax({
                type: 'GET',
                url: apiUrl+"dashboard/payment/checkorNumber",
                data: {ornumber : setOrnumber},
                cache: false,
                async: true,
                success: function(response){
                    hideLoading("");
                    if (response.success==true) {
                        confirmWithSwal(function(){
                            postPayment();
                        } , 'Are you sure?', 'warning');
    
                    }else{
                      
                      
                        confirmWithSwal(function(){
                            postPayment();
                            orexist = 1;
                        } , 'OR Number already exist! continue?', 'warning');
                    }
                },
    
                error: function(err){
                    hideLoading("");
                    setTimeout(function(){swal("Error!", err.statusText, "error");},0);
                }
            });
        }
       
    }

    function printForm(data) {
        const { AR_dateRelease, AR_clientName,  AR_amountInText, AR_amount, AR_address ,AR_ornumber, AR_lotArea } = data;

        return `
            <div class="cms-container cms-text-center">
              <img src="images/logo-invert.jpg" class="rmma-logo">
            </div>

            <div class="rmma-receipt-header">
                   <h3 class="rmma-header">R AND SONS PROPERTIES Co.</h3>
                    <p class="rmma-location">2nd floor S10 at The Paddock, J.Camus St.</p>
                    <p class="rmma-location"> Corner General Luna St. , Brgy. 4-A Poblacion District,</p>
                    <p class="rmma-location">Davao City, Davao del Sur, Philippines 8000</p>
                    <p class="rmma-contact"> Tel no. (082) 322-6819</p>
            </div>

            <div class="rmma-container">

                <div class="rmma-receipt-container">
                    <h3 class="rmma-receipt-item">PROVISIONAL RECEIPT</h3>
                    <label class="rmma-receipt-item rmma-text-right">Date:<u class="AR_dateRelease"> ${AR_dateRelease} </u></label>
                </div>

                <div class="rmma-content">
                  
                    <p class="rmma-big"><span class="rmma-receive"> RECEIVED </span> from M  <u class="AR_clientName" stye="padding-left: 5px;"> ${AR_clientName} </u>  with TIN  <u class="AR_TIN">______________</u> and address at <u class="AR_address">${AR_address}</u> business style of <u>__________________________</u>
                        the sum of Pesos  <u class="AR_amountInText"> ${AR_amountInText} </u> (P <u class="AR_amount">${AR_amount}</u>) in partial / full payment for <u class="AR_lotArea">${AR_lotArea}</u>
                    </p>

                </div>

                <div class="rmma-or-number">
                    <label class="cms-float-right  cms-no-padding">OR No. <span class="AR_ornumber"> ${AR_ornumber} </span></label>
                </div>

                <div class="rmma-issued-by">
                    <label>Issued By:</label><br>
                    <label><u class="rmmaIssuedBy">______________________________</u></label><br>
                    <label><i>Cashier/ Authorized Signature</i></label>
                </div>
            </div> 
        `;
    }

    function initializeData(){
        let data = {
            AR_dateRelease  : $('.paymentDate').val(),
            AR_clientName   : $('.addPaymentClientList option:selected').text(),
            AR_amountInText : amountToWord($('.paymentCredit').val()).toUpperCase(),
            AR_amount       : accounting.format($('.paymentCredit').val(),2),
            AR_address      : payamentPropAddress,
            AR_ornumber     : $('.paymentornumber').val(),
            AR_lotArea      : paymentPropLocationAddress
        }

        const printContainer = $('.print-previews');
        printContainer.empty();
        printContainer.append(printForm(data));

    }



    function updateTransactionPaymentHistory(){
        showLoading('Getting clients payments history...');
        $.ajax({
            type: 'GET',
            url: apiUrl+"dashboard/payment/transactionPaymentHistory",
            cache: false,
            async: true,
            success: function(response){
                hideLoading("");
                if (response.success==true) {

                    $('#cms_actionTools').empty();
                    if (response.role == "Admin") {
                        $('#cms_actionTools').append(`<button class="btn purple btn-refresh"><span class="fa fa-refresh"></span> Refresh</button>
                        <button class="btn green" id="btn_adminSet"><span class="fa fa-cogs"></span>  </button>`);
                    }else{
                        $('#cms_actionTools').append(`<button class="btn purple btn-refresh"><span class="fa fa-refresh"></span> Refresh</button>`);
                    }

                    var table = $('.transactionPaymentHistoryTable').DataTable();
                    table.clear().draw();

                    if(response.clientPaymentHistory.length > 0){
                        table.rows.add(response.clientPaymentHistory).draw();
                    }


                }
                else{
                    setTimeout(function(){swal("Warning!", response.message, "warning");},0);
                }

            },

            error: function(err){
                hideLoading("");
                setTimeout(function(){swal("Error!", err.statusText, "error");},0);
            }
        });
    }



    function addPaymentPropertyListChange(){
        var val = $('.addPaymentPropertyList').val();
        var val_2 = $('.addPaymentClientList').val();
  

        // console.log(val, val_2);

        var paymentoptions = $('.paymentoptions').val();
       
        if(paymentoptions ==  2){
            var valmultiple = [];
            $('.addPaymentPropertyList > option:selected').each(function(){
                valmultiple.push($(this).val());
                });
                valmultiple.shift();
        }  
        console.log(selected);

        if(val == null || val == "" || val == "0"){
            $('.paymentHistoryTab').addClass('hidden');
        }else{
            $('.paymentHistoryTab').removeClass('hidden');
            showLoading('Getting clients payments history...');
            $.ajax({
                type: 'GET',
                url: apiUrl+"dashboard/client/paymentHistoryForThisProperty",
                data : {id : val, cpID : val_2,select:selected,paymentopt:paymentoptions,multiple:valmultiple},
                cache: false,
                async: true,
                success: function(response){
                    hideLoading("");
                    // alert(response.message);
                    if (response.success==true) {

                         if (response.paid == 1) {
                            $('#paidStat').removeClass('hidden');
                         }else{
                            $('#paidStat').addClass('hidden');
                         }

                        $('.paymentBalance').val(accounting.format(response.balance,2));
                        $('.paymentCredit').val(accounting.unformat(accounting.format(response.monthlyAmortization,2)));
                        $('.btnPaymentSubmit').attr('data-balance',response.balance);

                        $(".paymentornumber").attr("placeholder",orincrement);
                        $(".fullpaymentOrnumber").attr("placeholder",orincrement);

                        paymentCreditIsChange();
                        let totalbalance = 0
                        $.each(response.clientsPropertyDetails,function(i,n){
                           payamentPropAddress = n.prop_address;
                           paymentPropLocationAddress = " P " + n.prop_phaseNo + " Blk " + n.prop_blkNo + " Lt " + n.prop_ltNo;
                       });
                       if(response.option == 2){
                       
                        $.each(response.result, function(i,n){
                             totalbalance += n.Totalbalance
                             
                        });
                        
                        $('.paymentBalance').val(accounting.format(totalbalance,2));
                        $('.paymentCredit').val(accounting.unformat(accounting.format(response.monthlyAmortization,2)));
                        $('.btnPaymentSubmit').attr('data-balance',totalbalance);
                        multiselectproperty = response.multiple;
                        
                    }

                    }
                    else{
                        setTimeout(function(){swal("Warning!", response.message, "warning");},0);
                    }

                },
                error: function(err){
                    hideLoading("");
                    setTimeout(function(){swal("Error!", err.statusText, "error");},0);
                }
            });
        }
    }



    function addPaymentClientListChange(){
        var val = $('.addPaymentClientList').val();

        if(val == null || val == "" || val == "0"){
            $('.paymentHistoryTab').addClass('hidden');
        }
        else{
            $('.paymentHistoryTab').removeClass('hidden');
            showLoading('Getting clients properties..');
            $.ajax({
                type: 'GET',
                url: apiUrl+"dashboard/client/getAllProperties",
                data : {id : val},
                cache: false,
                async: true,
                success: function(response){

                    if (response.success==true) {
                        $('.addPaymentPropertyList').empty();
                        $.each(response.clientsProperties,function(i,n){
                            $('.addPaymentPropertyList').append('<option value="'+n.clientPropertyID+'">'+n.propertyName+'</option>');
                        });

                        $.each(response.clientsPropertyDetails,function(i,n){
                            payamentPropAddress = n.prop_address;
                            paymentPropLocationAddress = " P " + n.prop_phaseNo + " Blk " + n.prop_blkNo + " Lt " + n.prop_ltNo;
                        });

                        addPaymentPropertyListChange();
                    }
                    hideLoading("");
                },
                error: function(err){
                    hideLoading("");
                    swal("Error!", err.statusText, "error");
                }
            });
        }
    }


    //Functions
    function loadClientPaymentPage(){
        showLoading();
        var navParent = $(this);

        $( ".dashboardPage" ).addClass('hidden');

        getTemplate(baseTemplateUrl, 'payments.html', function(render) {
            var renderedhtml = render({
                position : '',
                // department: responsivese.department
            });

            $('.page-sidebar-menu').find('.nav-item').removeClass('active open');

            if(navParent.parent().hasClass('nav-item')){
                navParent.parent().addClass('active open');
            }

            if(navParent.parent().parent().parent().hasClass('nav-item')){
                navParent.parent().parent().parent().addClass('active open');
            }


            $(".addPaymentContainer").html(renderedhtml);
            $('.addPaymentContainer').removeClass('hidden');
            // clienPaymentFormValidation();
            getAllClient();

            // $('.date-picker').datepicker({
            //       format: "yyyy-mm-dd",
            //       todayHighlight: true,
            //       autoclose: true
            //   });
            $('.paymentDate').val(new Date().toJSON().slice(0,10).replace(/-/g,'-'));

            
            $(".select2").select2({
              // placeholder: "Select a state",
              // allowClear: true
            });
            // tableclientPaymentHistoryTable();
            // updateTransactionPaymentHistory();
        });

    }



    function getAllClient(){
        showLoading('Getting all clients list...');
        $.ajax({
            type: 'GET',
            url: apiUrl+"dashboard/client/getAllNames",
            cache: false,
            async: true,
            success: function(response){

                if (response.success==true) {
                    $('.addPaymentClientList').empty();
                    $.each(response.clients,function(i,n){
                        $('.addPaymentClientList').append('<option '+(i==0? 'selected':'')+' value="'+n.clientID+'">'+n.clientName+'</option>');
                    });

                    $.each(response.num,function(i,n){
                     
                        orincrement = parseInt(n.ornumber)+1; 
                        
                        $(".paymentornumber").attr("placeholder",orincrement);
                        $(".fullpaymentOrnumber").attr("placeholder",orincrement);
                    });

                    addPaymentClientListChange();
                }
                else
                    hideLoading("");


            },
            error: function(err){
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }


     function remove_verifyAdminPass(password, thisId) {
         $.ajax({
            url: apiUrl+"dashboard/payment/checkAdminPass",
            type: 'GET',
            data : {pass: password},
            cache: false,
            async: true,
            success: function(response){
                hideLoading("");
                if (response.success) {
                     confirmWithSwal(function(){
                        $.ajax({
                            type: 'GET',
                            url: apiUrl+"dashboard/payment/deleteThisPayment",
                            data: {payID: thisId},
                            cache: false,
                            async: true,
                            success: function(response){
                                hideLoading();
                                if (response.success) {
                                    $('.addPaymentClientList').empty();
                                    $.each(response.clients,function(i,n){
                                        $('.addPaymentClientList').append('<option '+(i==0? 'selected':'')+' value="'+n.clientID+'">'+n.clientName+'</option>');
                                    });

                                    $.each(response.num,function(i,n){
                                        $(".paymentornumber").attr("placeholder", n.ornumber);
                                    });

                                    addPaymentClientListChange();

                                }
                                else{
                                    updateTransactionPaymentHistory();
                                }


                            },
                            error: function(err){
                                hideLoading();
                                swal("Error!", err.statusText, "error");
                            }
                        });

                    } , 'Delete this Payment?', 'warning');
                }else{
                    alert('Invalid Password');
                }

            },

            error: function(err){
                hideLoading("");
                setTimeout(function(){swal("Error!", err.statusText, "error");},0);
            }
        });
    }

    function deleteThisPayment(thisId) {

        bootbox.prompt({
          className: 'AdminPassword',
          title: "Administrator Password Required!",
          inputType: 'password',
          callback: function(result){
            if (result === null) {
                // alert('cancel');
            } else {
                remove_verifyAdminPass(result, thisId);
            }
          }
        });
    }

    function AdminModal_HTML(argument) {
        return  `<div class="row">
                    <div class="col-md-12">
                        <label for=""> Change Password </label>
                        <input type="password" class="form-control" id="newPass"/>
                        <label for=""> Retype Password </label>
                        <input type="password" class="form-control" id="retype_pass" style="margin-bottom: 15px;"/>
                        <a href="javascript:;" id="csm_restorePass"> restore default </a>
                    </div>    
                </div>`;
    }

    function show_administratorOption(argument) {
        bootbox.confirm({
            title: "Administrator Settings",
            message: AdminModal_HTML(),
            size: 'small',
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                if (result) {
                    const pass = $('#newPass').val();
                    const type = $('#retype_pass').val();

                    if (pass !== type) {
                         alert('Password do not match');
                    }else{
                        setnewAdminPass(pass);
                    }
                }

            }
        });
    }

    function setnewAdminPass(newPass) {
        $.ajax({
            type: 'PUT',
            url: apiUrl+"dashboard/payment/newAdminPass",
            data: {pass: newPass},
            cache: false,
            async: true,
            success: function(response){
                if (response.success) {
                    swal("Success", response.message, "success");
                }
                else{
                    swal("Warning", response.message, "warning");
                } 
            },
            error: function(err){
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
                
        });
    }

    function charges_form(argument) {

        return  `<div  class="form-group ch_forms" style="padding-left: 15px">
                      <div class="col-md-5">
                          <div class="form-group form-md-line-input">
                              <input type="text" class="form-control chargeValue_p" name="chargeValue">
                              <label for="form_control_1">Value</label>
                              <span class="help-block"></span>
                          </div>
                      </div>
                      <div class="col-md-5">
                          <div class="form-group form-md-line-input">
                              <input type="text" class="form-control chargeDate_p" name="chargeDate">
                              <label for="form_control_1">Date Added</label>
                              <span class="help-block"></span>
                        </div>
                      </div>
                      <div class="col-md-10">
                           <div class="form-group form-md-line-input">
                              <input type="text" class="form-control chargeDescription_p" name="chargeDescription">
                              <label for="form_control_1">Description</label>
                              <span class="help-block"></span>
                          </div>
                      </div>
                        <div class="col-md-6">
                              
                                <div class="form-group form-md-line-input">
                                <select class="form-control" id="transType">
                                <option value="0">Cash</option>
                                <option value="1">Bank</option>
                                <option value="2">Remittance Center</option>
                            </select> 
                            </div>
                        </div>
                        <div class="col-md-10 hidden banksz">
                                <div class="form-group form-md-line-input">
                                <input type="text" class="form-control remittanceName" name="remittanceName">
                                <label for="form_control_1">Bank/Remittance Name</label>
                                <span class="help-block"></span>
                                </div>                        
                        </div>
                      <div class="col-md-2 text-left">
                          <a href="javascript:;" class="btn btn-outline red" id="remove_thisForm"><span class="icon-close"></span></a>
                      </div>
                </div>`;
    }


    function addChargesForm(argument) {
        return `<div class="row"> 
                    <div class="col-md-offset-9 col-md-3 text-right">
                        <a href="javascript:;" class="btn btn-outline blue" id="btn_newCharges_p"><span class="fa fa-plus"></span></a>
                    </div> 
                </div>
                <div class="row chargesList">
                `+ charges_form() +`
                </div>`;
    }


    function add_newAddtionalCharges(argument) {
        const cp_id       = $('.addPaymentPropertyList').val();
        const clientID    = $('.addPaymentClientList').val();

        getChargesDetails(cp_id, clientID);

        bootbox.confirm({
            title: "<strong> <span class='fa fa-plus'>  </span> </strong> Add Charges",
            message: addChargesForm(),
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                const ch = $('.ch_forms');

                $.each(ch, function (n,i) {
                      chargesData.push({
                        index : n,
                        chargeValue : $(this).find('.chargeValue_p').val(),
                        chargeDescription : $(this).find('.chargeDescription_p').val(),
                        chargeDate : $(this).find('.chargeDate_p').val(),
                        transactionType : $(this).find('#transType').val(),
                        remittanceName : ($(this).find('#transType').val() == 0) ? 'Cash' : $(this).find('.remittanceName').val()
                      });
                });

                if (result) {
                    // updateChargesDetails(cp_id, clientID, chargesData)
                    clientWithChargeID = cp_id;
                }
            }
        });

        $('.chargeDate_p').datepicker({
            format: 'yyyy-mm-dd'
        });

         // swal('Info', 'Comming soon', "info");



    }

    function add_newAddtionalChargesList(argument) {
        const newForm = charges_form();
        $('.chargesList').append(newForm);
    }


    function getChargesDetails(clientID, propID) {
        chargesData = [];
         $.ajax({
            type: 'GET',
            url: apiUrl+"dashboard/payment/getCharges",
            data: {clientID: clientID, propID: propID},
            cache: false,
            async: true,
            success: function(response){
                if (response.success) {
                    chargesData = response.data;
                }else{
                    swal("Warning", response.message, "warning");
                } 
            },
            error: function(err){
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
                
        });
    }

    function updateChargesDetails(clientID, propID, charges) {
        $.ajax({
            type: 'PUT',
            url: apiUrl+"dashboard/payment/addnewCharges",
            data: {clientID: clientID, propID: propID, charges:charges},
            cache: false,
            async: true,
            success: function(response){
                if (response.success) {
                    // swal("Success", response.message, "success");
                    chargesData = [];
                }
                else{
                    swal("Warning", response.message, "warning");
                } 
            },
            error: function(err){
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
                
        });
    }
 


    function print_thisPayment(argument) {

        const anotherContent = document.getElementById('acknowledgementReceipt').innerHTML;
        const printView =  document.getElementById('printPreview').innerHTML;
      
        let buyerName = "",  
            buyerOR = "",  
            buyerPaymentAmount = "",  
            buyerPaymentDate = "",  
            buyerProperty = "",  
            buyerAddress = "";  

        buyerID      =  $(this).attr("data-id");
        buyerAddress =  $(this).attr("data-address");

        $('.transactionPaymentHistoryTable tr').each(function() {
 
            if (buyerID == $(this).find("td").eq(0).html()) {
                buyerName          = $(this).find("td").eq(2).html();
                buyerOR            = $(this).find("td").eq(1).html();
                buyerPaymentAmount = $(this).find("td").eq(5).html();
                buyerPaymentDate   = $(this).find("td").eq(4).html();
                buyerProperty      = $(this).find("td").eq(3).html();
            }
        });
        let data = {
            AR_dateRelease  : buyerPaymentDate,
            AR_clientName   : buyerName,
            AR_amountInText : amountToWord(buyerPaymentAmount).toUpperCase(),
            AR_amount       : accounting.format(buyerPaymentAmount,2),
            AR_address      : buyerAddress,
            AR_ornumber     : buyerOR,
            AR_lotArea      : paymentPropLocationAddress
        }

        const printContainer = $('.print-previews');
        printContainer.empty();
        printContainer.append(printForm(data));
        $('.date_now').html(toWeirdDate(GetTodayDate()));
        
        var mywindow = window.open('', 'PRINT', 'height=600,width=1200');

        mywindow.document.write('<html><head><title>' + document.title  + '</title>');
        mywindow.document.write('</head><body >');
        mywindow.document.write('<h1>' + document.title  + '</h1>');
        mywindow.document.write(document.getElementById('acknowledgementReceipt').innerHTML);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10*/

        mywindow.print();
        mywindow.close();

    }


})($);

$(clientPayments.init);
