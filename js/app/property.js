var Property = function () {
    var templates = [],
            baseTemplateUrl = 'template/property/';

    var assumeVal = 0;
    var gl_cpId = "";

    function getTemplate(baseTemplateUrl, templateName, callback) {
        if (!templates[templateName]) {
            $.get(baseTemplateUrl + templateName, function(resp) {
                compiled = _.template(resp);
                templates[templateName] = compiled;
                if (_.isFunction(callback)) {
                    callback(compiled);
                }
            }, 'html');
        } else {
            callback(templates[templateName]);
        }
    }
    


    var properytClientPaymentHistoryTable;
    function tableproperytClientPaymentHistoryTable(){
        var table = $('.properytClientPaymentHistoryTable');
        properytClientPaymentHistoryTable =  table.dataTable({
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            responsive: true,

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},
            
            dom: 'Bfrtip',
            

            // setup buttons extentension: http://datatables.net/extensions/buttons/
            buttons: [
                { 
                    extend: 'print', 
                    className: 'btn dark btn-outline',
                    message: 'Client property payment history'
                    // exportOptions: {
                    //     columns: [ 0, 1]
                    // }
                },
                { 
                    extend: 'pdf', 
                    className: 'btn green btn-outline' 
                },
                { 
                    extend: 'excel', 
                    className: 'btn purple btn-outline ' ,
                    extension: '.xls'
                    // exportOptions: {
                    //     columns: [ 0, 1]
                    // }
                },
                {
                    text: '<i class="fa fa-lg fa-copy"></i> COPY',
                    extend: 'copy',
                    className: 'btn btn-outline red  p-5 m-0 width-35 assets-export-btn export-xls ttip'
                    // exportOptions: {
                    //     columns: [ 0, 1]
                    // }
                }
            ],

            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: {
                details: {
                    // type: 'column',
                    // target: 'tr'
                }
            },
            columnDefs: [ {
                // className: 'control',
                // orderable: false,
                // targets:   0
            } ],


            "pagingType": "bootstrap_extended", 
            "order": [
                [1, 'ASC']
            ],
            
            
            "lengthMenu": [
                [5, 10, 15, 20, 50, 100],
                [5, 10, 15, 20, 50, 100] // change per page values here
            ],
            // set the initial value

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            "pageLength": 20,
            destroy : true

        });




        var table = $('.properytClientPaymentHistoryTable').DataTable();
        table.on( 'order.dt search.dt', function () {
            table.column(3, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = accounting.format(cell.innerHTML,2);
            } );
            table.column(4, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = accounting.format(cell.innerHTML,2);
            } );
            table.column(5, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = accounting.format(cell.innerHTML,2);
            } );
            // table.column(6, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            //     cell.innerHTML = accounting.format(cell.innerHTML,2);
            // } );
        } ).draw();

        // table.on( 'order.dt search.dt', function () {
        //     table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        //         cell.innerHTML = i+1;
        //     } );
        // } ).draw();

        $('.paymentHistoryModalTableContainer .tools').empty();
        table.buttons( 0, null).container().appendTo( '.paymentHistoryModalTableContainer .tools' );

    }
 

    var propertyListTable;
    function propertyListTableFunction(){
        var table = $('.propertyListTable');
        propertyListTable =  table.dataTable({
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No property added yet",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            // bootstrap_full_number 
            "pagingType": "bootstrap_extended", 
            "order": [
                [0, 'asc']
            ],
            responsive: true,
            "lengthMenu": [
                [5, 10, 15, 20, 50,100],
                [5, 10, 15, 20, 50,100] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            destroy : true
        });


        var table = $('.propertyListTable').DataTable();
        table.on( 'order.dt search.dt', function () {
            
            table.column(3, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = accounting.format(cell.innerHTML,2);
            } );
            table.column(4, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = accounting.format(cell.innerHTML,2);
            } );
            table.column(6, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = accounting.format(cell.innerHTML,2);
            } );
            table.column(8, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = accounting.format(cell.innerHTML,2);
            } );
        } ).draw();

    }


    return {
        init: init
    };

    // Init
    function init() {
        addEventHandlers();
        loadInitialization();
        
    }

    function loadInitialization(){
      

    }





    function addEventHandlers(){
        
        $(document).on('click', '.openPorpertyListContainer', openPorpertyListContainer );
        $(document).on('click', '.typeOfProperty', typeOfPropertyChange );

        
        $(document).on('click', '.addnew-bloks', btnaddnewbloksIsClicked );
        $(document).on('click', '.openAddPorpertyContainer', openAddPorpertyContainer );
        $(document).on('click', '.btnAddNewProperty1Update', btnAddNewProperty1UpdateIsClicked );
        $(document).on('click', '.btnUpdateThisBlock', btnUpdateThisBlockIsClick );
        $(document).on('click', '.viewall-bloks', getParentDetails );
        $(document).on('change', '.select-prop', getParentDetails );
        $(document).on('change', '.select-block', getParentDetailsSpecificBlock);
        $(document).on('click', '.btnAddNewProperty1', btnAddNewProperty1IsClick );
        $(document).on('click', '.btnAddNewProperty1Cancel', btnAddNewProperty1CancelIsClicked );
        
        $(document).on('click', '.btnupdateNewProperty1Submit', updateThisProperty );        
        $(document).on('click', '.btnAddNewProperty1Submit', postThisProperty );
        $(document).on('click', '.btn_AssumeTo', assumePropertyToContainer );
        

        $(document).on('click', '.btnAddBlockAddLot', btnAddBlockAddLotIsClicked );
        $(document).on('change', '.addBlockProptertyTypeList', propertyTypeIsChange );
        
        $(document).on('click', '.btnlotRemove', btnlotRemoveIsClicked );
        $(document).on('click', '.btnlotUpdate', btnlotUpdateIsClicked );
        $(document).on('click', '.btnAddBlockUpdateSlectedLot', btnAddBlockUpdateSlectedLotIsClicked );

        $(document).on('click', '.btnAddBlockCancel', btnAddBlockCancelIsClicked );
        $(document).on('click', '.btnAddBlockSubmit', btnAddBlockSubmitIsClicked );
        
        $(document).on('click', '.btnBlockUpdate', btnBlockUpdateIsClicked );

        $(document).on('click', '.ckeckboxNewPhase', ckeckboxNewPhaseIsClicked );
      
        $(document).on('change', '.selectPhaseList', selectPhaseListIsChange );

        $(document).on('keyup', '.addBlockPropertiesAddlotDetailsForm .addBlockProptertyLotSQM,.addBlockPropertiesAddlotDetailsForm  .addBlockProptertyLotPriceperSQM,.addBlockPropertiesAddlotDetailsForm  .addBlockProptertyLotPlanTerms', generateContactPriceAndMonthlyAmortization );      


        $(document).on('click','.btnViewClientProperyPaymentHistory', btnViewClientProperyPaymentHistoryIsClicked );  
    
        $(document).on('click', '.btnUpdatelotRemove', btnUpadateRemoveCLicked );
        $(document).on('click', '#assumeBtn', function(argument) {
            assumeVal = '1';
        } );
        $(document).on('click', '#assumeBtnTo', function(argument) {
            assumeVal = '2';
        } );
    } 


    function btnViewClientProperyPaymentHistoryIsClicked(){
        var clientPropertyID = $(this).attr('data-clientpropertyID');
        var clientid = $(this).attr('clientid');

        // alert(clientPropertyID);

        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl+"dashboard/client/viewPaymentHistoryForThisProperty",
            data : {id : clientPropertyID, clientid: clientid},
            cache: false,
            async: true,
            buttons: {
              success: {
                label: "OK",
                className: "red btn-outline",
                callback: function() {

                }
              },
            },
            success: function(response) { 
                
                if(response.success){
                  bootbox.dialog({
                  message: getTableViewForPaymentHistory(),
                  title: "Property Payment History",
                  onEscape: function() {  },
                  backdrop: true,
                  }).init(function(){
                      tableproperytClientPaymentHistoryTable();
                      
                      var table = $('.properytClientPaymentHistoryTable').DataTable();
                      table.clear().draw();
                      table.rows.add(response.clientsPropertyPaymentHistory).draw();  

                      var table2 = $('.properytClientPaymentHistoryTable2').DataTable();
                      table2.clear().draw();
                      table2.rows.add(response.clientsPropertyPaymentHistory).draw();




                      hideLoading();  
                  }).find("div.modal-dialog").addClass("largeWidth");;
                }
                else{
                  hideLoading();
                  swal("Warning!", response.message, "warning");
                }
                
            },
            error: function(err){
              hideLoading();
              swal("Error!", err.statusText, "error");
            }
        });

    }







    function getTableViewForPaymentHistory(){ 
      return (
        '<style>'+
          '.largeWidth {'+
              'margin: 0 auto;'+
              'width: 90%;'+
          '}'+
        '</style><div class="portlet paymentHistoryModalTableContainer">'+
            '<div class="portlet-title">'+
                '<div class="tools pull-right"> </div>'+
            '</div>'+
            '<div class="portlet-body">'+
                '<table class="table table-striped table-bordered table-hover table-sm dt-responsive dataTable no-footer dtr-column collapsed properytClientPaymentHistoryTable2">'+
                    '<thead>'+
                        '<tr>'+
                            '<th> Payment No </th>'+
                            '<th> Particulars </th>'+
                            '<th> Dates </th>'+
                            '<th> Debit </th>'+
                            '<th> Credit </th>'+
                            '<th> Balance </th>'+
                            // '<th> Agent Commision </th>'+
                            // '<th> Date Claimed </th>'+
                        '</tr>'+
                    '</thead>'+
                    '<tbody>'+
                    '</tbody>'+
                '</table>'+
            '</div>'+
        '</div>'
        );
    }





    function selectPhaseListIsChange(){

      var phaseNumber = $('.selectPhaseList').val();
      if(phaseNumber == null){
        $('.newBlock-no').html(1);
        return false;
      }

      showLoading();
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/property/getLastBlockForThisPhase",
          data : {parentID : $('.select-prop').val(),
                  phaseNumber:phaseNumber},
          cache: false,
          async: true,
          success: function(response) { 
              
              hideLoading();
              if(response.success){
                //deboner
                if(response.blockData.length != 0)
                  $.each(response.blockData,function(i,n){
                    $('.newBlock-no').html(parseInt(n.blockNumber)+1);
                  });
                else
                  $('.newBlock-no').html(1);

              }
              else{
                swal("Warning!", response.message, "warning");
              }
              
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }




    function ckeckboxNewPhaseIsClicked(){
      if($('.selectPhaseList option').length != 0){
        if($('.ckeckboxNewPhase').find('i').hasClass('fa-check')){
          $('.ckeckboxNewPhase').find('i').removeClass('fa-check');
          $('.ckeckboxNewPhase').find('i').addClass('fa-close');
          $('.selectPhaseList').removeAttr('disabled');
          selectPhaseListIsChange();
        }
        else{
          $('.newBlock-no').html(1);
          $('.ckeckboxNewPhase').find('i').removeClass('fa-close');
          $('.ckeckboxNewPhase').find('i').addClass('fa-check');
          $('.selectPhaseList').attr('disabled','');
        }
      }
      else{
        $('.newBlock-no').html(1);
        $('.selectPhaseList').attr('disabled','');
        $('.ckeckboxNewPhase').find('i').removeClass('fa-close');
        $('.ckeckboxNewPhase').find('i').addClass('fa-check');
      }


    }


    function sqmOrPriceOrPlantesmHasError(){
      var hasError = false;
      var input = [$('.addBlockPropertiesAddlotDetailsForm .addBlockProptertyLotSQM'),$('.addBlockPropertiesAddlotDetailsForm .addBlockProptertyLotPriceperSQM'),$('.addBlockPropertiesAddlotDetailsForm .addBlockProptertyLotPlanTerms')];
      $.each(input,function(i,n){
        var e = $(this);
        
        if(e.val() == ""){
          hasError = true;
          $(e.parent().addClass('has-error'));
        }
        else if(e.hasClass('addBlockProptertyLotSQM') || e.hasClass('addBlockProptertyLotPriceperSQM')){
          if( !validateNumberAndDot(e.val()) ||  !validateNumberAndDot(e.val())){
            
            hasError = true;
            e.parent().addClass('has-error');
          }
          else{
            e.parent().removeClass('has-error');
          }
        }
        else{
          if( !validateInteger(e.val()) ){
            
            hasError = true;
            e.parent().addClass('has-error');
          }
          else{
            e.parent().removeClass('has-error');
          }
        }
      });

      return hasError;
    }


    function generateContactPriceAndMonthlyAmortization(){
      var sqm = $('.addBlockPropertiesAddlotDetailsForm .addBlockProptertyLotSQM').val();
      var price = $('.addBlockPropertiesAddlotDetailsForm .addBlockProptertyLotPriceperSQM').val();
      var terms = $('.addBlockPropertiesAddlotDetailsForm .addBlockProptertyLotPlanTerms').val();

      if(sqmOrPriceOrPlantesmHasError()){
        return false;
      }

      var total = (parseFloat(sqm) * parseFloat(price));

      $('.addBlockPropertiesAddlotDetailsForm .addBlockProptertyLotContractPrice').val(accounting.format(total,2));
      $('.addBlockPropertiesAddlotDetailsForm .addBlockProptertyLotMonthlyAmortization').val(accounting.format((total / parseInt(terms)),2));
    }






    function getThisBlockData(){
      var hasError = false;
      var data = [];
      $.each($('.AddBlocklot-container .addNewBlockDetails'), function(i,n){
        var propID = $(this).attr('data-id');
        if(propID == undefined){
          propID = 'new'
        }


        var lotDetails = {
            propertyID                      : propID,
            propertyTypeID                  : $(this).attr('data-propertytypeid'),
            lotnumber                       : $(this).attr('data-lotnumber'),
            status                          : $(this).attr('data-status'),
            sqm                             : $(this).attr('data-sqm'),
            priceperm2                      : $(this).attr('data-priceperm2'),
            planterms                       : $(this).attr('data-planterms'),
            model                           : $(this).attr('data-model'),
            contactprice                    : $(this).attr('data-contactprice'),
            monthlyamortization             : $(this).attr('data-monthlyamortization'),
            removeStat                      : $(this).attr('data-removeStat')
        };



        if(!validateInteger($(this).attr('data-propertytypeid')) || !validateInteger($(this).attr('data-lotnumber')) || !validateInteger($(this).attr('data-status'))
          || !validateNumberAndDot($(this).attr('data-priceperm2')) || !validateInteger($(this).attr('data-planterms')) || !validateNumberAndDot($(this).attr('data-sqm'))  || !validateNumberAndDot($(this).attr('data-contactprice'))  || !validateNumberAndDot($(this).attr('data-monthlyamortization'))){
          errorMessageForAddingBlock = 'Invalid data for Lot # '+ $(this).attr('data-lotnumber') ;
          hasError = true;
        }
        data.push(lotDetails);
      });


      if(hasError)
        return true;
      else
        return data;
    }







    function getAllBlockData(){
      var hasError = false;
      var data = [];

      $.each($('.AddBlocklot-container .addNewBlockDetails'), function(i,n){
        var lotDetails = {
            propertyTypeID                  : $(this).attr('data-propertytypeid'),
            lotnumber                       : $(this).attr('data-lotnumber'),
            status                          : $(this).attr('data-status'),
            sqm                             : $(this).attr('data-sqm'),
            priceperm2                      : $(this).attr('data-priceperm2'),
            planterms                       : $(this).attr('data-planterms'),
            model                           : $(this).attr('data-model'),
            contactprice                    : $(this).attr('data-contactprice'),
            monthlyamortization             : $(this).attr('data-monthlyamortization'),
            removeStat                      : $(this).attr('data-removeStat')
                               
        };
        if(!validateInteger($(this).attr('data-propertytypeid')) || !validateInteger($(this).attr('data-lotnumber')) || !validateInteger($(this).attr('data-status'))
         || !validateInteger($(this).attr('data-planterms')) || !validateNumberAndDot($(this).attr('data-priceperm2')) || !validateNumberAndDot($(this).attr('data-sqm'))  || !validateNumberAndDot($(this).attr('data-contactprice'))  || !validateNumberAndDot($(this).attr('data-monthlyamortization'))){
          errorMessageForAddingBlock = 'Invalid data for Lot # '+ $(this).attr('data-lotnumber') ;
          hasError = true;
        }
        data.push(lotDetails);
      });

      if($('.AddBlocklot-container .addNewBlockDetails').length == 0){
        hasError = true;
        errorMessageForAddingBlock = 'Create block content first.' ;
      }


      if(hasError)
        return true;
      else
        return data;
    }



    var errorMessageForAddingBlock = "";
    function btnAddBlockSubmitIsClicked(evt){
      confirmWithSwal(function(){
      
          var lots = $('.AddBlocklot-container .addNewBlockDetails');
          if(lots.length == 0){
            swal('Warning!','Please create a lot for this block first.','warning');
            return false;
          }

          var blockData = getAllBlockData();
          if(blockData == true){ 
            setTimeout(function(){
              swal('Error!' , errorMessageForAddingBlock, 'error');
            },0);
            return false;
          }


          var data = {
                blockData : blockData,
                parentID  : $('.select-prop').val(),
                phaseNumber : $('.selectPhaseList').val(),
                newPhase  : $('.ckeckboxNewPhase').find('i').hasClass('fa-check')
              };


          $.ajax({
              type: 'POST',
              url: apiUrl+"/dashboard/property/addThisBlock",
              data : data,
              cache: false,
              async: true,
              success: function(response) { 
                  
                  if(response.success){
                    
                    getParentDetails();
                    
                    setTimeout(function(){swal("Success!", response.message, "success");},0);
                  }
                  else{
                    setTimeout(function(){swal("Warning!", response.message, "warning");},0);
                  }
                  hideLoading();
              },
              error: function(err){
                hideLoading();
                setTimeout(function(){
                  swal("Error!", err.statusText, "error");
                },0);
                
              }
          });
        },
        'Are you sure to submit and create this block?',
        'warning');

      evt.preventDefault();
    }



    
    



    // function btnBlockUpdateIsClicked(){
    //   var lots = $('.AddBlocklot-container .addNewBlockDetails');
      
    //   if(lots.length == 0){
    //     swal('Warning!','Please create a lot for this block first.','warning');
    //     return false;
    //   }

    //   var parentID    = $(this).attr('data-parentid');
    //   var blockid     = $(this).attr('data-blockid');
    //   var phasenumber = $(this).attr('data-phasenumber');


    //   // alert(phasenumber + ', '+  parentID + ', ' + parentID);

    //   confirmWithSwal(function (){
    //       var blockData = getThisBlockData();
    //       if(blockData == true){ 
    //         swal('Error!' , errorMessageForAddingBlock, 'error');
    //         return false;
    //       }
    //       var data = {
    //             blockData   : blockData,
    //             blockid     : blockid,
    //             parentID    : parentID,
    //             phasenumber : phasenumber
    //           };


    //       $.ajax({
    //           type: 'POST',
    //           url: apiUrl+"/dashboard/property/updateThisBlock",
    //           data : data,
    //           cache: false,
    //           async: true,
    //           success: function(response) { 
                  
    //               if(response.success){
                    
    //                 getParentDetails();
    //                 swal("Success!", response.message, "success");
    //               }
    //               else{
    //                 swal("Warning!", response.message, "warning");
    //               }
    //               hideLoading();
    //           },
    //           error: function(err){
    //             hideLoading();
    //             swal("Error!", err.statusText, "error");
    //           }
    //       });
    //     },
    //     'Are you sure to submit this block updates?',
    //     'warning');
    // }


    function btnBlockUpdateIsClicked(){
      var lots = $('.AddBlocklot-container .addNewBlockDetails');
      
      if(lots.length == 0){
        swal('Warning!','Please create a lot for this block first.','warning');
        return false;
      }

      var parentID    = $(this).attr('data-parentid');
      var blockid     = $(this).attr('data-blockid');
      var phasenumber = $(this).attr('data-phasenumber');

      confirmWithSwal(function (){
            var blockData = getThisBlockData();
            if(blockData == true){ 
                swal('Error!' , errorMessageForAddingBlock, 'error');
                return false;
            }

            var data = {
                    blockData   : blockData,
                    blockid     : blockid,
                    parentID    : parentID,
                    phasenumber : phasenumber
                };


            $.ajax({
                type: 'POST',
                url: apiUrl+"/dashboard/property/updateThisBlock",
                data : data,
                cache: false,
                async: true,
                success: function(response) { 
                    if(response.success){
                    
                        getParentDetails();
                        swal("Success!", response.message, "success");
                    }
                    else{
                        swal("Warning!", response.message, "warning");
                    }
                    hideLoading();
                },
                error: function(err){
                    hideLoading();
                    swal("Error!", err.statusText, "error");
                }
            });
        },
        'Are you sure to submit this block updates?',
        'warning');
    }


    




    function btnAddBlockUpdateSlectedLotIsClicked(){
      var parent              = $(".lot-check:checked").parent().parent().parent().parent();
      if(parent.length == 0){
        swal("Warning!",'Please select a lot to be updated first.','warning');
        return false;
      }

      bootbox.dialog({
        message: getLotDetailsForm(),
        title: "<h4>Add Lot Details</h4>",
        onEscape: function() {  },
        backdrop: true,
        buttons: {
          confirm: {
              label: '<i class="icon-plus"></i> Save',
              className: "btn-success btn-outline addBlockAddLotDetailsModal",
              callback: function() {
                if(validateThisForm_addBlockPropertiesAddlotDetailsForm())
                  return false;

                generateContactPriceAndMonthlyAmortization();

                var lastNumber = getAddBlockLargestLotNumber();
                lastNumber = parseInt(lastNumber);

                var status              = $('.addBlockProptertyStatusList').val();
                var statuslabel         = $('.addBlockProptertyStatusList  option:selected').text();
                var propertyTypeID      = $('.addBlockProptertyTypeList').val();
                var propertyTypeName    = $('.addBlockProptertyTypeList  option:selected').text();
                var sqm                 = $('.addBlockProptertyLotSQM').val();
                var model               = $('.addBlockProptertyLotModel').val();
                var contractPrice       = $('.addBlockProptertyLotContractPrice').val();
                var monthlyAmortization = $('.addBlockProptertyLotMonthlyAmortization').val();
                var m2price             = $('.addBlockProptertyLotPriceperSQM').val();
                var planterms           = $('.addBlockProptertyLotPlanTerms').val();

                var statusColor = "";
                if(status == '2')
                  parent.find('.bordered').addClass('default');
                else
                  parent.find('.bordered').removeClass('default')

                

                parent.attr('data-propertytypeid',propertyTypeID);
                parent.attr('data-status',status);

                parent.find('.lotPropertytype').html(propertyTypeName);
                parent.find('.lotsqm').html(sqm);
                parent.find('.lotPlantTerms').html(planterms);
                parent.find('.lotPricePerm2').html(m2price);
                parent.find('.lotmodel').html(model);
                parent.find('.lotContractPrice').html(contractPrice);
                parent.find('.lotmonthlyAmortization').html(monthlyAmortization);
                parent.find('.lotstatuslabel').html(statuslabel);

                parent.attr('data-priceperm2',m2price);
                parent.attr('data-planTerms',planterms);
                parent.attr('data-sqm',sqm);
                parent.attr('data-model',model);
                parent.attr('data-contactprice',contractPrice);
                parent.attr('data-monthlyamortization',monthlyAmortization);
                parent.attr('data-contactprice',accounting.unformat(contractPrice));
                parent.attr('data-monthlyamortization',accounting.unformat(monthlyAmortization));

                $(".lot-check").prop('checked', false);
              }
          }
        }
      }).init(function(){
          propertyTypeIsChange();
          generateContactPriceAndMonthlyAmortization();
          
      });
    }




    function btnlotUpdateIsClicked(){
      var parent              = $(this).parent().parent().parent().parent();

      bootbox.dialog({
        message: getLotDetailsForm(),
        title: "<h4>Add Lot Details</h4>",
        onEscape: function() {  },
        backdrop: true,
        buttons: {
          confirm: {
              label: '<i class="icon-plus"></i> Save',
              className: "btn-success btn-outline addBlockAddLotDetailsModal",
              callback: function() {
                if(validateThisForm_addBlockPropertiesAddlotDetailsForm())
                  return false;

                generateContactPriceAndMonthlyAmortization();

                var lastNumber = getAddBlockLargestLotNumber();
                lastNumber = parseInt(lastNumber);

                var status              = $('.addBlockProptertyStatusList').val();
                var statuslabel         = $('.addBlockProptertyStatusList  option:selected').text();
                var propertyTypeID      = $('.addBlockProptertyTypeList').val();
                var propertyTypeName    = $('.addBlockProptertyTypeList  option:selected').text();
                var sqm                 = $('.addBlockProptertyLotSQM').val();
                var model               = $('.addBlockProptertyLotModel').val();
                var m2price             = $('.addBlockProptertyLotPriceperSQM').val();
                var planterms           = $('.addBlockProptertyLotPlanTerms').val();
                var contractPrice       = $('.addBlockProptertyLotContractPrice').val();
                var monthlyAmortization = $('.addBlockProptertyLotMonthlyAmortization').val();

                if(status == '2')
                  parent.find('.bordered').addClass('default');
                else
                  parent.find('.bordered').removeClass('default');

                parent.attr('data-propertytypeid',propertyTypeID);
                parent.attr('data-status',status);

                parent.find('.lotPropertytype').html(propertyTypeName);
                parent.find('.lotsqm').html(accounting.format(sqm,2));
                parent.find('.lotPricePerm2').html(accounting.format(m2price,2));
                parent.find('.lotPlantTerms').html(planterms);
                parent.find('.lotmodel').html(model);
                parent.find('.lotContractPrice').html(contractPrice);
                parent.find('.lotmonthlyAmortization').html(monthlyAmortization);
                parent.find('.lotstatuslabel').html(statuslabel);



                parent.attr('data-priceperm2',m2price);
                parent.attr('data-planTerms',planterms);
                parent.attr('data-sqm',sqm);
                parent.attr('data-model',model);
                parent.attr('data-contactprice',accounting.unformat(contractPrice));
                parent.attr('data-monthlyamortization',accounting.unformat(monthlyAmortization));

              }
          }
        }
      }).init(function(){
          propertyTypeIsChange();
          var propertytypeid      = parent.attr('data-propertytypeid');
          var status              = parent.attr('data-status');
          var sqm                 = parent.attr('data-sqm');
          var priceperm2          = parent.attr('data-priceperm2');
          var plantTerms          = parent.attr('data-planterms');
          var model               = parent.attr('data-model');
          var contractPrice       = parent.attr('data-contactprice');
          var monthlyAmortization = parent.attr('data-monthlyamortization');


          $('.addBlockProptertyTypeList').val(propertytypeid);
          $('.addBlockProptertyStatusList').val(status);
          $('.addBlockProptertyLotSQM').val(sqm);
          $('.addBlockProptertyLotPlanTerms').val(plantTerms);
          $('.addBlockProptertyLotPriceperSQM').val(priceperm2);
          $('.addBlockProptertyLotModel').val(model);
          $('.addBlockProptertyLotContractPrice').val(contractPrice);
          $('.addBlockProptertyLotMonthlyAmortization').val(monthlyAmortization);

          $('.addBlockPropertiesAddlotDetailsForm input').parent().removeClass('form-md-floating-label');
          generateContactPriceAndMonthlyAmortization();
          
      });
    }

    function btnUpadateRemoveCLicked(){
       // alert($(this).parent().parent().parent().parent().attr('data-removeStat').value = 1);
      
       var stat = $(this).parent().parent().parent().parent().attr('click-stat');
       if (stat == 0){
          $(this).parent().parent().parent().parent().attr('data-removeStat', '1');
          $(this).parent().parent().parent().parent().attr('click-stat', '1');
          $(this).removeClass('default');
          $(this).addClass('red');

       }else{
          $(this).parent().parent().parent().parent().attr('data-removeStat', '0');
          $(this).parent().parent().parent().parent().attr('click-stat', '0');
          $(this).removeClass('red');
          $(this).addClass('default');
       }

     
    }

  
    function btnlotRemoveIsClicked(){
      
      var parent = $(this).parent().parent().parent().parent();

      parent.toggle(500,function(){
        parent.remove();
        updateLotNumberFromThisRemoveButton();
      });
      
      
    }


    function updateLotNumberFromThisRemoveButton(){
      var x = 1;
      $.each($('.AddBlocklot-container .addNewBlockDetails'),function(i,n){
        $(this).attr('data-lotNumber',x);
        $(this).find('.lot-no').html(x);
        x++;
      }); 
    }



    function btnAddBlockCancelIsClicked(){
      $('.viewall-bloks').click();
      $('.addnew-bloks').removeClass('hidden');
    }



    function propertyTypeIsChange(){
      var val = $('.addBlockProptertyTypeList').val();
      if(val == 1){
        $('.addBlockProptertyLotModel').attr('readonly','');
        $('.addBlockProptertyLotModel').val("NONE");
      }
      else{
        $('.addBlockProptertyLotModel').removeAttr('readonly');
        $('.addBlockProptertyLotModel').val("");
      }

    }


    function validateThisForm_addBlockPropertiesAddlotDetailsForm(){
      var hasError = false;
      var propType = $('.addBlockProptertyTypeList').val();
      $.each($('.addBlockPropertiesAddlotDetailsForm input'),function(i,n){
        if($(this).val().trim() == ""){
          $(this).parent().addClass('has-error');
          hasError = true;
        }
        else if( $(this).hasClass('addBlockProptertyLotPriceperSQM')  || $(this).hasClass('addBlockProptertyLotSQM')){
          if(!validateNumberAndDot($(this).val().trim())){
            $(this).parent().addClass('has-error');
            hasError = true;
          }
          else
            $(this).parent().removeClass('has-error');
        }
        else if($(this).hasClass('addBlockProptertyLotPlanTerms')){
          if(!validateInteger($(this).val().trim())){
            $(this).parent().addClass('has-error');
            hasError = true;
          }
          else
            $(this).parent().removeClass('has-error');
        }
        else{
          $(this).parent().removeClass('has-error');
        }

      });



      return hasError;
    }

    
    function btnAddBlockAddLotIsClicked(){
      // $('.AddBlocklot-container')
      var numberOfLots = $('.addBlockNumberOfLotInput').val().trim();
      if(!validateInteger(numberOfLots)){
        swal('Warning!', 'Number of lot is required.','warning');
        return false;
      }

      numberOfLots = parseInt(numberOfLots);

      bootbox.dialog({
            message: getLotDetailsForm(),
            title: "<h4>Add Lot Details</h4>",
            onEscape: function() {  },
            backdrop: true,
            buttons: {
                confirm: {
                    label: '<i class="icon-plus"></i> Initialize',
                    className: "btn-success btn-outline addBlockAddLotDetailsModal",
                    callback: function() {
                      if(validateThisForm_addBlockPropertiesAddlotDetailsForm())
                        return false;


                      generateContactPriceAndMonthlyAmortization();

                      var lastNumber = getAddBlockLargestLotNumber();
                      
                      // console.log(lastNumber);
                      // if (lastNumber === 0) {
                      //   lastNumber = (parseInt(lastNumber));
                      // }else{
                      //   lastNumber = (parseInt(lastNumber)+1);
                      // }

                      var propertyTypeID = $('.addBlockProptertyTypeList').val();
                      var propertyTypeName = $('.addBlockProptertyTypeList  option:selected').text();
                      var propertyStatusText = $('.addBlockProptertyStatusList  option:selected').text();
                      var propertyStatus = $('.addBlockProptertyStatusList').val();
                      var sqm               = $('.addBlockProptertyLotSQM').val();
                      var model = $('.addBlockProptertyLotModel').val();
                      var contractPrice = $('.addBlockProptertyLotContractPrice').val();
                      var monthlyAmortization = $('.addBlockProptertyLotMonthlyAmortization').val();
                      var priceperm2 = $('.addBlockProptertyLotPriceperSQM').val();
                      var planTerms = $('.addBlockProptertyLotPlanTerms').val();
                      var statusColor = "";
                      if(propertyStatus == '2')
                        statusColor = "default";

                      for (var i = 1; i <= numberOfLots; i++) {
                        lastNumber++;
                        var newElement = $(getLotForAddBlock(lastNumber,propertyTypeID,propertyTypeName,sqm,model,contractPrice,
                          monthlyAmortization,propertyStatus,propertyStatusText,statusColor,priceperm2,planTerms)).hide();
                        $('.AddBlocklot-container').append(newElement);
                        newElement.slideDown(1000);
                      }

                    }
                }
            }
        }).init(function(){
            propertyTypeIsChange();
            generateContactPriceAndMonthlyAmortization();
        });
    }


    // AddBlocklot-container addNewBlockDetails attr('data-lotNumber')
    function getAddBlockLargestLotNumber(){
      var lastNumber = 0;
      // $.each($('.AddBlocklot-container .addNewBlockDetails'),function(i,n){
      //   if(lastNumber <= $(this).attr('data-lotNumber')){
      //     lastNumber = $(this).attr('data-lotNumber');
      //   }
      // });

      return $('.AddBlocklot-container .addNewBlockDetails').length;
    }



    function getLotDetailsForm(){
      return (
        '<div class="addBlockPropertiesAddlotDetailsForm">'+
          '<div class="form-group form-md-line-input  ">'+
              '<select  class="form-control edited select2 addBlockProptertyTypeList">'+
                  propertyTypesOptions+
              '</select>'+
              '<label for="form_control_1">Property Type</label>'+
          '</div>'+
          '<div class="form-group form-md-line-input  ">'+
              '<select  class="form-control edited select2 addBlockProptertyStatusList">'+
                  '<option value="0">Avaliable</option>'+
                  '<option value="2">Disabled</option>'+
              '</select>'+
              '<label for="form_control_1">Property Status</label>'+
          '</div>'+
          '<div class="row">'+
            '<div class="col-xs-3">'+
              '<div class="form-group form-md-line-input ">'+
                  '<input type="text" class="form-control addBlockProptertyLotSQM" id="form_control_1" value="100" >'+
                  '<label for="form_control_1">SQ.M</label>'+
              '</div>'+
            '</div>'+
            '<div class="col-xs-3">'+
              '<div class="form-group form-md-line-input ">'+
                  '<input type="text" class="form-control addBlockProptertyLotPlanTerms" id="form_control_1" value="24">'+
                  '<label for="form_control_1">Plan Terms</label>'+
              '</div>'+
            '</div>'+
            '<div class="col-xs-3">'+
              '<div class="form-group form-md-line-input ">'+
                  '<input type="text" class="form-control addBlockProptertyLotPriceperSQM" id="form_control_1" value="1000" >'+
                  '<label for="form_control_1">Price per m<sup>2</sup></label>'+
              '</div>'+
            '</div>'+
            '<div class="col-xs-3">'+
              '<div class="form-group form-md-line-input ">'+
                  '<input type="text" class="form-control addBlockProptertyLotModel" id="form_control_1" >'+
                  '<label for="form_control_1">Model</label>'+
              '</div>'+
            '</div>'+
          '</div>'+
          '<div class="row">'+
            '<div class="col-xs-6">'+
              '<div class="form-group form-md-line-input ">'+
                  '<input type="text" readonly class="form-control addBlockProptertyLotContractPrice" id="form_control_1" >'+
                  '<label for="form_control_1">Contract Price</label>'+
              '</div>'+
            '</div>'+
            '<div class="col-xs-6">'+
              '<div class="form-group form-md-line-input ">'+
                  '<input type="text" readonly class="form-control addBlockProptertyLotMonthlyAmortization" id="form_control_1" >'+
                  '<label for="form_control_1">Monthly Amortization</label>'+
              '</div> '+
            '</div>'+
          '</div>'+
        '</div>'
        );
    }







    function btnAddNewProperty1IsClick(){
      $('.addPropertyListContainer , .btnAddNewProperty1 , .blockListContainer , .updatePropertyFormContainer').addClass('hidden');
      $('.addPropertyFormContainer').removeClass('hidden');
    }


    function btnAddNewProperty1UpdateIsClicked(){
      $('.addPropertyListContainer , .btnAddNewProperty1 , .blockListContainer').addClass('hidden');
      $('.updatePropertyFormContainer').removeClass('hidden');

      $('.btnupdateNewProperty1Submit').attr('data-id',$('.select-prop').val());
    }



    function typeOfPropertyChange(){
      loadAllProperties($('.typeOfProperty:checked').val());
    }


    function btnReturnToPropertyList(){
      $('.openPorpertyListContainer').click();
    }


    function propertyTypeHasChange(){
      var type = $(this).val();
      if(type == 1){
        $('.propertyModel').attr('disabled','');
      }
      else{
        $('.propertyModel').removeAttr('disabled');
      }
    }






    function openUpdateUserProfileInfoform(){
      var pos = $('.updateuser_position').attr('data-value');
      $('.updateuserForm  input').focus();
      $.each($('.updateuser_position > option'),function(i,n){
          if($(this).val() == pos)
              $(this).attr('selected','');
      });
    }





    function validatePropertyFormInputs(){
      var hasError = false;

      $.each($('.addPropertyFormContainer  input'),function(i,n){
        if($(this).val() == '' ){
            $(this).parent().addClass('has-error');
            $(this).parent().find('span').html('This is required..');
            hasError = true;
        }
        else{
          $(this).parent().removeClass('has-error');
          $(this).parent().find('span').html('');
        }
      });
      return hasError;
    }


    function validateupdatePropertyFormContainer(){
      var hasError = false;

      $.each($('.updatePropertyForm  input'),function(i,n){
        if($(this).val() == '' ){
            $(this).parent().addClass('has-error');
            $(this).parent().find('span').html('This is required..');
            hasError = true;
        }
        else{
          $(this).parent().removeClass('has-error');
          $(this).parent().find('span').html('');
        }
      });
      return hasError;
    }


    function btnAddNewProperty1CancelIsClicked(){
      $('.addPropertyListContainer input').val('').focus();
      $('.addPropertyListContainer , .btnAddNewProperty1 , .blockListContainer').removeClass('hidden');
      $('.addPropertyFormContainer, .updatePropertyFormContainer').addClass('hidden'); 
      $('.select-prop').removeAttr('disabled');
    }







    function postThisProperty(){
      if(validatePropertyFormInputs()){
        return false;
      }

      var add_propertyFormName                      = $('.add_propertyFormName').val().toLowerCase().trim();
      var add_propertyFormAddress                   = $('.add_propertyFormAddress').val().toLowerCase().trim();
      var add_propertyWithPhases                    = $('.add_propertyWithPhases:checked');

      if($('.add_propertyWithPhases').is(':checked')){
        add_propertyWithPhases = 1;
      }else{
        add_propertyWithPhases = 0;
      }

      // console.log(add_propertyWithPhases);

      var data = {
          add_propertyFormName                      : add_propertyFormName,
          add_propertyFormAddress                   : add_propertyFormAddress,
          add_propertyWithPhases                    : add_propertyWithPhases
        };


      showLoading();
      $.ajax({
          type: 'POST',
          url: apiUrl+"/dashboard/property/addThisPorperty",
          data : data,
          cache: false,
          async: true,
          success: function(response) { 
              if(response.success){
                $('.addPropertyListContainer input').val('').focus();
                $('.addPropertyListContainer , .btnAddNewProperty1 , .blockListContainer').removeClass('hidden');
                $('.addPropertyFormContainer').addClass('hidden'); 
                swal("Success!", response.message, "success");
                $('.openAddPorpertyContainer').click();
              }
              else{
                swal("Warning!", response.message, "warning");
              }

              hideLoading();
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }




    function removeThisProperty(){
      showLoading();

      var propID                        = $(this).attr('data-id');
      var propertyType                  = $(this).attr('data-type');

      var data = {
          propID                        : propID,
          propertyType                  : propertyType
        };


      $.ajax({
          type: 'POST',
          url: apiUrl+"/dashboard/property/removeThis",
          data : data,
          cache: false,
          async: true,
          success: function(response) { 
              if(response.success){
                swal("Success!", response.message, "success");
                $('.updatePropertyForm input').val('').focus();
                var table = $('.propertyListTable').DataTable();
                table.clear().draw();
                table.rows.add(response.properties).draw();

                hideLoading();
              }
              else{
                hideLoading();
                swal("Warning!", response.message, "warning");
              }
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });

    }


    function updateThisProperty(){
      if(validateupdatePropertyFormContainer()){
        return false;
      }
 
      showLoading();

      var propID                                       = $(this).attr('data-id');
      var update_propertyFormName                      = $('.update_propertyFormName').val().toLowerCase().trim();
      var update_propertyFormAddress                   = $('.update_propertyFormAddress').val().toLowerCase().trim();


      var data = {
          propID                                        : propID,
          update_propertyFormName                       : update_propertyFormName,
          update_propertyFormAddress                    : update_propertyFormAddress
        };


      $.ajax({
          type: 'POST',
          url: apiUrl+"/dashboard/property/updateThis",
          data : data,
          cache: false,
          async: true,
          success: function(response) { 
              if(response.success){
                $('.addPropertyListContainer input').val('').focus();
                $('.addPropertyListContainer , .btnAddNewProperty1 , .blockListContainer').removeClass('hidden');
                $('.addPropertyFormContainer').addClass('hidden'); 

                swal("Success!", response.message, "success");
                $('.openAddPorpertyContainer').click();
              }
              else{
                swal("Warning!", response.message, "warning");
              }

              hideLoading();
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }


    function loadAllProperties(type){
      showLoading();
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/property/getAll",
          cache: false,
          data: {type: type},
          async: true,
          success: function(response) { 
            if(response.success){
              var table = $('.propertyListTable').DataTable();
              table.clear().draw();
              table.rows.add(response.properties).draw();

              hideLoading();
            }
            else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }
            
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }


    function openChangePorpertyContainer(data){
      showLoading();

      $( ".dashboardPage" ).addClass('hidden');
      getTemplate(baseTemplateUrl, 'updateProperty.html', function(render) {
          var renderedhtml = render({datas: data});
          $(".updatePropertyContainer").html(renderedhtml);
          $('.updatePropertyContainer').removeClass('hidden');

          $.each(data[0],function(i,n){
              $(('.'+i)).val(n);
          });

          $('.updatePropertyForm input').focus();

          hideLoading();
      });

    }






    
    function openPorpertyListContainer(){
      showLoading();
      var navParent = $(this);

      getTemplate(baseTemplateUrl, 'propertyList.html', function(render) {
          var renderedhtml = render({data: ""});

          // $( ".dashboardPage" ).addClass('hidden');
          $( ".dashboardPage" ).empty();


          $('.page-sidebar-menu').find('.nav-item').removeClass('active open');

          if(navParent.parent().hasClass('nav-item')){
            navParent.parent().addClass('active open');
          }

          if(navParent.parent().parent().parent().hasClass('nav-item')){
            navParent.parent().parent().parent().addClass('active open');
          }

          $(".propertyListContainer").html(renderedhtml);
          $('.propertyListContainer').removeClass('hidden');

          propertyListTableFunction();
          loadAllProperties($('.typeOfProperty:checked').val());
      });

    }







    function btnaddnewbloksIsClicked(){
      var parentID = $('.select-prop').val();
      if(parentID == null || parentID == "" || parentID == '0'){
        hideLoading();
        // swal("Warning!", 'Inalid property ID.', "warning");
        // console.log("Inalid property ID");
        return false;
      }

      showLoading();
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/property/getlastBlock", 
          data : {parentID : parentID},
          cache: false, 
          async: true,
          success: function(response) { 
            
            if(response.success){
              getTemplate(baseTemplateUrl, 'addBlock.html', function(render) {



                var renderedhtml = render({data: ''});
                $(".block-container").html(renderedhtml);

                if(response.blockData.length != 0)
                  $.each(response.blockData,function(i,n){
                    $('.newBlock-no').html(parseInt(n.blockNumber)+1);
                  });
                else
                  $('.newBlock-no').html(1);


                $('.selectPhaseList').empty();
                if($('.add_propertyAddress').attr('data-phasable') == 1){
                  $('.phaseControlContainer').removeClass('hidden');
                  $.each(response.phasesData,function(i,n){
                    $('.selectPhaseList').append('<option value="'+n.phaseNumber+'">'+n.phaseNumber2+'</option>');
                  });  

                  

                  // selectPhaseListIsChange();
                  ckeckboxNewPhaseIsClicked();
                }
                else
                  $('.phaseControlContainer').addClass('hidden');
                
                
                $('.addnew-bloks, .btnAddNewProperty1Update, .btnAddNewProperty1').addClass('hidden');

                hideLoading();
              });
            }
            else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }


    function getParentDetailsSpecificBlock(){
      var blockNum = $('.select-block').val();
      var parentID = $('.select-prop').val();
      if(parentID == null || parentID == "" || parentID == '0'){
        hideLoading();
        // swal("Warning!", 'Inalid property ID.', "warning");
        // console.log('Inalid property ID.');
        return false;
      }

      showLoading();
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/property/getPropertyParentDetailsSpecificBlock",
          data : {parentID : parentID,blockNo:blockNum},
          cache: false,
          async: true,
          success: function(response) { 
            // console.log(response);
            if(response.success){
              $('.block-container').empty();
              var parent = response.parent;
              $('.viewall-bloks , .phaseControlContainer').addClass('hidden');
              $('.select-prop').removeAttr('disabled');
              $('.btnAddNewProperty1 , .btnAddNewProperty1Update ,.addnew-bloks').removeClass('hidden');

              $('.add_propertyAddress').val(parent[0].properyAddress);
              $('.update_propertyFormAddress').val(parent[0].properyAddress);
              $('.update_propertyFormName').val(parent[0].propertyName);
              $('.add_propertyAddress').attr('data-phasable',parent[0].phasable);
              var phasable = parent[0].phasable;
              var collapsableContainer = "";



              var blocksContainer = $('.block-container');
              var preveBlockNumber = "";
              var block = "";

              var prevePhaseNumber = "";
              var phase = "";
              if(response.blocks.length > 0){



                // phaseNumber
                if(phasable == 1){
                  collapsableContainer = getCollapsableContainerStart();

                  collapsableContainer += getCollapsableContainerEnd();
                  collapsableContainer = $(collapsableContainer);


                  blocksContainer.append(collapsableContainer);
                }
                else{
                  
                }



                $.each(response.blocks,function(i,n){

                  if(phasable == 0){
                                                                              // none Phasable property
                    if(preveBlockNumber == ""){
                      preveBlockNumber = n.block;
                      block = getBlockStart(n.parentID,n.block,0);
                    }

                    if(n.clientName == null)
                      n.clientName = 'NONE';

                    if(preveBlockNumber != n.block){
                      block += getBlockEnd();
                      preveBlockNumber = n.block;


                      if(phasable == 0){
                        var newElement = $(block).hide();
                        blocksContainer.append(newElement);
                        newElement.fadeIn(500); 
                      }

                      block = getBlockStart(n.parentID,n.block,0);
                    }

                    block += getLotForView(n.lot, n.propertySQM, n.propertyModel,n.propertyOriginalPrice, n.propertyMonthlyAmortization
                        ,n.propertyStatus,n.propertyTypeID,n.propertyTypeName,n.parentID,n.propertyPricePerSQM,n.propertyPlanTerms,n.clientName);
                  

                  }else{


                                                                               //  Phasable property
                    if(prevePhaseNumber == ""){
                      prevePhaseNumber = n.phaseNumber;
                      
                      phase = getCollapsablePaneStart(prevePhaseNumber,'Phase '+prevePhaseNumber);

                      preveBlockNumber = n.block;
                      block = getBlockStart(n.parentID,n.block,prevePhaseNumber);

                    }


                    if(prevePhaseNumber != n.phaseNumber){
                      block += getBlockEnd();
                      preveBlockNumber = n.block;

                      phase += block;
                      phase += getCollapsablePaneEnd();

                      var newElement = $(phase).hide();   ///   append new collapse phase

                      collapsableContainer.append(newElement);
                      newElement.fadeIn(500); 
                      

                      prevePhaseNumber = n.phaseNumber;

                      
                      phase = getCollapsablePaneStart(prevePhaseNumber,'Phase '+prevePhaseNumber);

                      block = getBlockStart(n.parentID,n.block,prevePhaseNumber);
                    }
                    

                    if(preveBlockNumber != n.block){      //   adding all block child of this phase
                      block += getBlockEnd();
                      preveBlockNumber = n.block;

                      phase += block;
                      
                      block = getBlockStart(n.parentID,n.block,prevePhaseNumber);
                    }


                    if(n.clientName == null)
                      n.clientName = 'NONE';

                    block += getLotForView(n.lot, n.propertySQM, n.propertyModel,n.propertyOriginalPrice, n.propertyMonthlyAmortization
                        ,n.propertyStatus,n.propertyTypeID,n.propertyTypeName,n.parentID,n.propertyPricePerSQM,n.propertyPlanTerms,n.clientName);

                  }

                  
                });

                if(phasable == 0){
                  block += getBlockEnd();
                  var newElement = $(block).hide();
                  blocksContainer.append(newElement);
                  newElement.slideDown(2000);  
                }
                else{


                  block += getBlockEnd();

                  phase += block;
                  phase += getCollapsablePaneEnd();

                  var newElement = $(phase).hide();   ///   append new collapse phase
                  collapsableContainer.append(newElement);
                  newElement.fadeIn(500); 


                  
        
                }
                
              }
              else{

                var newElement = $('<h4 style="color: red"><span class="icon-close"></span> This property has no block details yet.</h4>').hide();
                blocksContainer.append(newElement);
                newElement.slideDown(2000);

                blocksContainer.append(
                    newElement
                  );
              }
              



              hideLoading();
            }
            else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }
            
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }




    function getParentDetails(){
      var parentID = $('.select-prop').val();
      if(parentID == null || parentID == "" || parentID == '0'){
        hideLoading();
        // swal("Warning!", 'Inalid property ID.', "warning");
        // console.log('Inalid property ID.');
        return false;
      }

      showLoading();
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/property/getPropertyParentsDetails",
          data : {parentID : parentID},
          cache: false,
          async: true,
          success: function(response) { 
            // console.log(response);
            if(response.success){
              $('.block-container').empty();
              var parent = response.parent;
              $('.viewall-bloks , .phaseControlContainer').addClass('hidden');
              $('.select-prop').removeAttr('disabled');
              $('.btnAddNewProperty1 , .btnAddNewProperty1Update ,.addnew-bloks').removeClass('hidden');

              $('.add_propertyAddress').val(parent[0].properyAddress);
              $('.update_propertyFormAddress').val(parent[0].properyAddress);
              $('.update_propertyFormName').val(parent[0].propertyName);
              $('.add_propertyAddress').attr('data-phasable',parent[0].phasable);
              var phasable = parent[0].phasable;
              var collapsableContainer = "";



              var blocksContainer = $('.block-container');
              var preveBlockNumber = "";
              var block = "";

              var prevePhaseNumber = "";
              var phase = "";
              if(response.blocks.length > 0){



                // phaseNumber
                if(phasable == 1){
                  collapsableContainer = getCollapsableContainerStart();

                  collapsableContainer += getCollapsableContainerEnd();
                  collapsableContainer = $(collapsableContainer);


                  blocksContainer.append(collapsableContainer);
                }
                else{
                  
                }



                $.each(response.blocks,function(i,n){

                  if(phasable == 0){
                                                                              // none Phasable property
                    if(preveBlockNumber == ""){
                      preveBlockNumber = n.block;
                      block = getBlockStart(n.parentID,n.block,0);
                    }

                    if(n.clientName == null)
                      n.clientName = 'NONE';

                    if(preveBlockNumber != n.block){
                      block += getBlockEnd();
                      preveBlockNumber = n.block;


                      if(phasable == 0){
                        var newElement = $(block).hide();
                        blocksContainer.append(newElement);
                        newElement.fadeIn(500); 
                      }

                      block = getBlockStart(n.parentID,n.block,0);
                    }

                    block += getLotForView(n.lot, n.propertySQM, n.propertyModel,n.propertyOriginalPrice, n.propertyMonthlyAmortization
                        ,n.propertyStatus,n.propertyTypeID,n.propertyTypeName,n.parentID,n.propertyPricePerSQM,n.propertyPlanTerms,n.clientName);
                  

                  }else{


                                                                               //  Phasable property
                    if(prevePhaseNumber == ""){
                      prevePhaseNumber = n.phaseNumber;
                      
                      phase = getCollapsablePaneStart(prevePhaseNumber,'Phase '+prevePhaseNumber);

                      preveBlockNumber = n.block;
                      block = getBlockStart(n.parentID,n.block,prevePhaseNumber);

                    }


                    if(prevePhaseNumber != n.phaseNumber){
                      block += getBlockEnd();
                      preveBlockNumber = n.block;

                      phase += block;
                      phase += getCollapsablePaneEnd();

                      var newElement = $(phase).hide();   ///   append new collapse phase

                      collapsableContainer.append(newElement);
                      newElement.fadeIn(500); 
                      

                      prevePhaseNumber = n.phaseNumber;

                      
                      phase = getCollapsablePaneStart(prevePhaseNumber,'Phase '+prevePhaseNumber);

                      block = getBlockStart(n.parentID,n.block,prevePhaseNumber);
                    }
                    

                    if(preveBlockNumber != n.block){      //   adding all block child of this phase
                      block += getBlockEnd();
                      preveBlockNumber = n.block;

                      phase += block;
                      
                      block = getBlockStart(n.parentID,n.block,prevePhaseNumber);
                    }


                    if(n.clientName == null)
                      n.clientName = 'NONE';

                    block += getLotForView(n.lot, n.propertySQM, n.propertyModel,n.propertyOriginalPrice, n.propertyMonthlyAmortization
                        ,n.propertyStatus,n.propertyTypeID,n.propertyTypeName,n.parentID,n.propertyPricePerSQM,n.propertyPlanTerms,n.clientName);

                  }

                  
                });

                if(phasable == 0){
                  block += getBlockEnd();
                  var newElement = $(block).hide();
                  blocksContainer.append(newElement);
                  newElement.slideDown(2000);  
                }
                else{


                  block += getBlockEnd();

                  phase += block;
                  phase += getCollapsablePaneEnd();

                  var newElement = $(phase).hide();   ///   append new collapse phase
                  collapsableContainer.append(newElement);
                  newElement.fadeIn(500); 


                  
        
                }
                
              }
              else{

                var newElement = $('<h4 style="color: red"><span class="icon-close"></span> This property has no block details yet.</h4>').hide();
                blocksContainer.append(newElement);
                newElement.slideDown(2000);

                blocksContainer.append(
                    newElement
                  );
              }
              



              hideLoading();

            
              
              var blockList = $('.select-block');
              blockList.empty();

              $.each(response.blockCounter,(i,n)=>{
                blockList.append(
                  '<option value="'+n.block+'">Block '+n.block+'</option>'
                );
              });

              //wew
            }
            else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }
            
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }



    function getCollapsableContainerStart(){
      return '<div class="panel-group accordion" id="accordion1">';
    }
    function getCollapsableContainerEnd(){
      return '</div >';
    }


    function getCollapsablePaneStart(id,title){
      return (
        '<div class="panel panel-default">'+
          '<div class="panel-heading">'+
              '<h4 class="panel-title">'+
                  '<a class="accordion-toggle collapsed in" data-toggle="collapse" data-parent="#accordion1" href="#collapse_'+id+'"> '+title+' </a>'+
              '</h4>'+
          '</div>'+
          '<div id="collapse_'+id+'" class="panel-collapse collapse" >'+
              '<div class="panel-body">'
        );
    }

    function getCollapsablePaneEnd(){
      return (
              '</div>'+
            '</div>'+
        '</div>'
        );
    }




    function btnUpdateThisBlockIsClick(){
      var parentID = $(this).attr('data-parentid');


      var phaseNumber = $(this).attr('data-prevephasenumber');// current phase number
      var block1 = $(this).attr('data-block');
      if(parentID == null || parentID == "" || parentID == 0  || block1 == null || block1 == "" || block1 == 0) {
        hideLoading();
        swal("Warning!", 'Inalid IDs.', "warning");
        return false;
      }

      showLoading();
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/property/getThisBlock",
          data  : {
                  parentID    : parentID,
                  block       : block1,
                  phaseNumber : phaseNumber
                },
          cache: false,
          async: true,
          success: function(response) { 
            
            if(response.success){
              $('.viewall-bloks').removeClass('hidden');
              $('.btnAddNewProperty1 , .btnAddNewProperty1Update ,.addnew-bloks').addClass('hidden');
              
              $('.select-prop').attr('disabled','');



              var blocksContainer = $('.block-container');
              blocksContainer.empty();

              var preveBlockNumber = "";
              var block = "";
              var blockNumber, parentID ;
              $.each(response.blocks,function(i,n){

                if(preveBlockNumber == ""){
                  preveBlockNumber = n.block;
                  blockNumber = n.block;
                  parentID = n.parentID;
                  block = getBlockStartForUpdate(n.parentID,n.block);
                }

                if(preveBlockNumber != n.block){
                  block += getBlockEnd();
                  
                  blocksContainer.append(block);

                  block = getBlockStartForUpdate(n.parentID,n.block);
                }

                block += getLot(n.lot, n.propertySQM, n.propertyModel,n.propertyOriginalPrice, n.propertyMonthlyAmortization
                    ,n.propertyStatus,n.propertyTypeID,n.propertyTypeName,n.parentID,n.block,n.ID,n.propertyPricePerSQM,n.propertyPlanTerms);
                
              });

              block += getBlockEndForUpdate(parentID,blockNumber,phaseNumber);
              blocksContainer.append(block);


              hideLoading();
            }
            else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }




    function getBlockStartForUpdate(parentID,block){
      return (
        '<div>'+
          '<div class="portlet light bordered">'+
              '<div class="portlet-title">'+
                  '<div class="caption">'+
                      '<span class="caption-subject bold font-green uppercase">Block <span style="color: red" class="block-no" Nvalue="sample">'+block+'</span></pan>'+
                  '</div>'+
                  '<div class="form-group" style="float: right; padding-right: 5px; padding-left: 5px;">'+
                      '<a data-toggle="modal" href="#basic" class="btn btn-outline yellow btnAddBlockUpdateSlectedLot  "><span class="icon-pencil"></span></a>'+
                  '</div>'+
                  '<div class="form-group" style="float: right; padding-left: 5px;">'+
                      '<a href="javascript:;" class="btn btn-outline blue btnAddBlockAddLot  " value=""><span class="icon-plus"></span></a>'+
                  '</div>'+
                   '<div class="form-group form-md-line-input" style="float: right; margin-left: 25px;padding-top: 0px; margin-bottom: 0px">'+
                      '<input type="number" class="form-control text-center addBlockNumberOfLotInput  " placeholder="Enter no of lots" id="lotNo">'+
                      '<label for="form_control_1"></label>'+
                  '</div>'+
              '</div>'+
              '<div class="portlet-body">'+
                  '<div class="row">'+
                      '<div class="lot-container AddBlocklot-container" >'
        );
    }


    function getBlockStart(parentID,block,prevePhaseNumber){
      return (
        '<form>'+
          '<div class="portlet light bordered">'+
              '<div class="portlet-title">'+
                  '<div class="caption">'+
                      '<span class="caption-subject bold font-green uppercase">Block <span style="color: red" class="block-no" Nvalue="sample">'+block+'</span></pan>'+
                  '</div>'+
                  '<div class="caption pull-right" >'+
                      '<a href="javascript:;" data-prevePhaseNumber="'+prevePhaseNumber+'" data-parentID="'+parentID+'" data-block="'+block+'" class="btn btn-outline  blue btn-sm btnUpdateThisBlock"><span class="icon-pencil"></span> Update</a>'+
                  '</div>'+
                  '<div class="form-group" style="float: right">'+
                      '<a href="javascript:;" class="btn btn-outline red btnBlockRemove hidden"><span class="icon-close"></span></a>'+
                  '</div>'+
                  '<div class="form-group" style="float: right; padding-right: 5px; padding-left: 5px;">'+
                      '<a data-toggle="modal" href="#basic" class="btn btn-outline blue lot-update hidden"><span class="icon-pencil"></span></a>'+
                  '</div>'+
                  '<div class="form-group" style="float: right; padding-left: 5px;">'+
                      '<a href="javascript:;" class="btn btn-outline blue add-lots hidden" value=""><span class="icon-plus"></span></a>'+
                  '</div>'+
                   '<div class="form-group form-md-line-input" style="float: right; margin-left: 25px;padding-top: 0px; margin-bottom: 0px">'+
                      '<input type="number" class="form-control text-center txt-lotNum hidden" placeholder="Enter no of lots" id="lotNo">'+
                      '<label for="form_control_1"></label>'+
                  '</div>'+
              '</div>'+
              '<div class="portlet-body">'+
                  '<div class="row">'+
                      '<div class="lot-container" >'
        );
    }


    function getBlockEnd(id){
      return (
                          '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</form>'
        );
    }


    function getBlockEndForUpdate(parentID,block,phaseNumber){
      return (
                          '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="form-actions  btn-lot-sub">'+
                    '<button type="" data-phaseNumber="'+phaseNumber+'" data-parentid="'+parentID+'" data-blockID = "'+block+'" class="btn btn-outline green btnBlockUpdate"> Submit</button>'+
                '</div> '+
            '</div>'+
        '</div>'
        );
    }




    function getLotForAddBlock(lotNumber,propertyTypeID,propertyTypeName,sqm,model,contractPrice,
      monthlyAmortization,propertyStatusID,status,statusColor,priceperm2,planTerms){

      return (
        '<div class="col-sm-4 addNewBlockDetails" data-removeStat="0"  data-propertyTypeID="'+propertyTypeID+'" data-lotNumber="'+lotNumber+'" '+
        ' data-status="'+propertyStatusID+'" data-priceperm2="'+priceperm2+'" data-planTerms="'+planTerms+'" data-sqm="'+accounting.unformat(sqm)+'" data-model="'+model+'" data-contactPrice="'+accounting.unformat(contractPrice)+'" data-monthlyAmortization="'+accounting.unformat(monthlyAmortization)+'"  >'+
          '<div class="portlet light bordered '+statusColor+'" style="box-shadow : 0px 0px 15px #3598dc;border-radius : 40px;">'+
              '<div class="portlet-title">'+
                  '<div class="caption">'+
                      '<span class="caption-subject bold font-green uppercase">Lot <span style="color: red;" class="lot-no">'+lotNumber+'</span></span>'+
                  '</div>'+
                  '<div class="form-group" style="float: right">'+
                      '<a href="javascript:;" class="btn btn-outline red btnlotRemove" ><span class="icon-close"></span></a>'+
                  '</div>'+
                  '<div class="form-group" style="float: right; padding-left: 5px; padding-right: 5px">'+
                      '<a  data-toggle="modal" href="#basic" class="btn btn-outline blue btnlotUpdate" " ><span class="icon-pencil"></span></a>'+
                  '</div>'+
                  '<div class="form-group" style="float: right; padding-top: 8px;">'+
                      '<input type="checkbox" class="lot-check  "  >  </label>'+
                  '</div>'+
              '</div>'+
              '<div class="portlet-body ">'+
                  '<div class="portlet sale-summary pd0">'+
                      '<div class="portlet-body">'+
                          '<ul class="list-unstyled">'+
                              '<li class="pd0">'+
                                  '<span class="sale-info  small-font">Property Type:'+
                                      '<i class="fa fa-img-up"></i>'+
                                  '</span>'+
                                  '<span class="sale-num pull-right  small-font lotPropertytype"> '+propertyTypeName+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info  small-font">SQ.M :'+
                                      '<i class="fa fa-img-up"></i>'+
                                  '</span>'+
                                  '<span class="sale-num  pull-right small-font lotsqm"> '+sqm+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info  small-font">Price per m<sup>2</sup> :'+
                                      '<i class="fa fa-img-up"></i>'+
                                  '</span>'+
                                  '<span class="sale-num  pull-right small-font lotPricePerm2"> '+priceperm2+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info  small-font">Plan Terms :'+
                                      '<i class="fa fa-img-up"></i>'+
                                  '</span>'+
                                  '<span class="sale-num  pull-right small-font lotPlantTerms"> '+planTerms+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info small-font "> Model :'+
                                      '<i class="fa fa-img-down"></i>'+
                                  '</span>'+
                                  '<span class="sale-num   pull-right small-font lotmodel"> '+model+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info small-font">Contract Price : </span>'+
                                  '<span class="sale-num  pull-right small-font lotContractPrice"> '+contractPrice+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info small-font">Monthly Amortization : </span>'+
                                  '<span class="sale-num  pull-right small-font lotmonthlyAmortization">  '+monthlyAmortization+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info small-font">Status : </span>'+
                                  '<span class="sale-num  pull-right small-font lotstatuslabel">  '+status+' </span>'+
                              '</li>'+
                          '</ul>'+
                      '</div>'+
                  '</div>'+
              '</div>'+
          '</div>'+
      '</div>'
        );
    }



    function getLotForView(lotNumber, sqm, model,contractPrice, monthlyAmortization,propertyStatus,propertyTypeID,
      propertyTypeName,parentID,propertyPricePerSQM,propertyPlanTerms,clientName){
      var status = "";
      var style = '';

      if(propertyStatus  == 1){
        status = "Occupied";
        style = 'red-pink';
      }
      else if(propertyStatus  == 2){
        status = "Disabled";
        style = 'default';
      }
      else
        status = "Unoccupied";


      return (
        '<div class="col-sm-4">'+
          '<div class="portlet light bordered  '+style+'">'+
              '<div class="portlet-title">'+
                  '<div class="caption">'+
                      '<span class="caption-subject bold font-green uppercase">Lot <span style="color: red;" class="lot-no">'+lotNumber+'</span></span>'+
                  '</div>'+
              '</div>'+
              '<div class="portlet-body ">'+
                  '<div class="portlet sale-summary pd0">'+
                      '<div class="portlet-body">'+
                          '<ul class="list-unstyled">'+
                              '<li class="pd0">'+
                                  '<span class="sale-info  small-font">Property Type:'+
                                      '<i class="fa fa-img-up"></i>'+
                                  '</span>'+
                                  '<span class="sale-num pull-right  small-font"> '+propertyTypeName+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info  small-font">SQ.M :'+
                                      '<i class="fa fa-img-up"></i>'+
                                  '</span>'+
                                  '<span class="sale-num  pull-right small-font"> '+accounting.format(sqm,2)+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info  small-font">Price per m<sup>2</sup> :'+
                                      '<i class="fa fa-img-up"></i>'+
                                  '</span>'+
                                  '<span class="sale-num  pull-right small-font"> '+accounting.format(propertyPricePerSQM,2)+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info  small-font">Plan Terms:'+
                                      '<i class="fa fa-img-up"></i>'+
                                  '</span>'+
                                  '<span class="sale-num  pull-right small-font"> '+propertyPlanTerms+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info small-font "> Model :'+
                                      '<i class="fa fa-img-down"></i>'+
                                  '</span>'+
                                  '<span class="sale-num   pull-right small-font"> '+model+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info small-font">Contract Price : </span>'+
                                  '<span class="sale-num  pull-right small-font"> '+accounting.format(contractPrice,2)+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info small-font">Monthly Amortization : </span>'+
                                  '<span class="sale-num  pull-right small-font">  '+accounting.format(monthlyAmortization,2)+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info small-font">Status : </span>'+
                                  '<span class="sale-num  pull-right small-font">  '+status+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info small-font">Occupied By: </span>'+
                                  '<span class="sale-num  pull-right small-font">  '+clientName+' </span>'+
                              '</li>'+
                          '</ul>'+
                      '</div>'+
                  '</div>'+
              '</div>'+
          '</div>'+
      '</div>'
        );
    }


    function getLot(lotNumber, sqm, model,contractPrice, monthlyAmortization,propertyStatus,
      propertyTypeID,propertyTypeName,parentID,blockNumber,ID,priceperm2,planTerms){

      var controls  = "";
      var status = "";
      var style = "";
      var css = "";
      var css2 ="";
      if(propertyStatus  == 1){
        status = "Occupied";
        style = 'red-pink';
        css= "margin-bottom: 5px";
      }
      else if(propertyStatus  == 2){
        status = "Disabled";
        style = 'default';
        controls = (
              '<div class="form-group" style="float: right; padding-left: 5px; padding-right: 5px">'+
                  '<a  data-toggle="modal" href="#basic" class="btn btn-outline yellow btnlotUpdate" " ><span class="icon-pencil"></span></a>'+
              '</div>'+
              '<div class="form-group" style="float: right; padding-top: 8px;">'+
                  '<input type="checkbox" class="lot-check  "  >  </label>'+
              '</div>'
          );
      }
      else{
        css= "margin-bottom: 3px";
        controls = (
               '<div class="form-group" style="float: right">'+
                  '<a href="javascript:;" class="btn default btnUpdatelotRemove" ><span class="icon-close"></span></a>'+
              '</div>'+
              '<div class="form-group" style="float: right; padding-left: 5px; padding-right: 5px">'+
                  '<a  data-toggle="modal" href="#basic" class="btn btn-outline yellow btnlotUpdate" " ><span class="icon-pencil"></span></a>'+
              '</div>'+
              '<div class="form-group" style="float: right; padding-top: 8px;">'+
                  '<input type="checkbox" class="lot-check  "  >  </label>'+
              '</div>'
              
          );
        status = "UnOccupied";
      }


      return (
        '<div class="col-sm-4 addNewBlockDetails sample" click-stat="0" data-removeStat="0" data-id="'+ID+'" data-propertyTypeID="'+ propertyTypeID +'" data-lotNumber="'+lotNumber+'" data-blockNumber="'+blockNumber+'" '+
        ' data-status="'+propertyStatus+'" data-priceperm2="'+priceperm2+'" data-planTerms="'+planTerms+'" data-sqm="'+sqm+'" data-model="'+model+'" data-contactPrice="'+contractPrice+'" data-monthlyAmortization="'+monthlyAmortization+'" data-parentID="'+parentID+'"  style="'+ css +'">'+
          '<div class="portlet light bordered '+style+'">'+
              '<div class="portlet-title">'+
                  '<div class="caption">'+
                      '<span class="caption-subject bold font-green uppercase">Lot <span style="color: red;" class="lot-no">'+lotNumber+'</span></span>'+
                  '</div>'+
                  controls
                  +
              '</div>'+
              '<div class="portlet-body">'+
                  '<div class="portlet sale-summary pd0">'+
                      '<div class="portlet-body">'+
                          '<ul class="list-unstyled">'+
                              '<li class="pd0">'+
                                  '<span class="sale-info  small-font">Property Type:'+
                                      '<i class="fa fa-img-up"></i>'+
                                  '</span>'+
                                  '<span class="sale-num pull-right  small-font lotPropertytype"> '+propertyTypeName+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info  small-font">SQ.M :'+
                                      '<i class="fa fa-img-up"></i>'+
                                  '</span>'+
                                  '<span class="sale-num  pull-right small-font lotsqm"> '+accounting.format(sqm,2)+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info  small-font">Price per m<sup>2</sup>:'+
                                      '<i class="fa fa-img-up"></i>'+
                                  '</span>'+
                                  '<span class="sale-num  pull-right small-font lotPricePerm2"> '+accounting.format(priceperm2,2)+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info  small-font">Plan Terms :'+
                                      '<i class="fa fa-img-up"></i>'+
                                  '</span>'+
                                  '<span class="sale-num  pull-right small-font lotPlantTerms"> '+planTerms+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info small-font "> Model :'+
                                      '<i class="fa fa-img-down"></i>'+
                                  '</span>'+
                                  '<span class="sale-num   pull-right small-font lotmodel"> '+model+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info small-font">Contract Price : </span>'+
                                  '<span class="sale-num  pull-right small-font lotContractPrice"> '+accounting.format(contractPrice,2)+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info small-font">Monthly Amortization : </span>'+
                                  '<span class="sale-num  pull-right small-font lotmonthlyAmortization">  '+accounting.format(monthlyAmortization,2)+' </span>'+
                              '</li>'+
                              '<li class="pd0">'+
                                  '<span class="sale-info small-font">Status : </span>'+
                                  '<span class="sale-num  pull-right small-font lotstatuslabel">  '+status+' </span>'+
                              '</li>'+
                          '</ul>'+
                      '</div>'+
                  '</div>'+
              '</div>'+
          '</div>'+
      '</div>'
        );
    }

    var propertyTypesOptions = "";

    function openAddPorpertyContainer(){
      var navParent = $(this);
      showLoading();
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/property/getAllPropertyParents",
          cache: false,
          async: true,
          success: function(response) { 
            if(response.success){
              propertyTypesOptions = response.propertyTypes;
              $('.page-sidebar-menu').find('.nav-item').removeClass('active open');

              if(navParent.parent().hasClass('nav-item')){
                navParent.parent().addClass('active open');
              }

              if(navParent.parent().parent().parent().hasClass('nav-item')){
                navParent.parent().parent().parent().addClass('active open');
              }

              $( ".dashboardPage" ).empty();

              getTemplate(baseTemplateUrl, 'addProperty.html', function(render) {
                  var renderedhtml = render({data: ""});
                  $(".addPropertyContainer").html(renderedhtml);
                  $('.addPropertyContainer').removeClass('hidden');

                  var propList = $('.select-prop');
                  propList.empty();

                  $.each(response.parents,function(i,n){
                    propList.append(
                        '<option value="'+n.propertyID+'">'+n.propertyName+'</option>'
                      );
                  });

                  $('.select-prop , .selectPhaseList').select2();

                  // var blockList = $('.select-block');
                  // blockList.empty();

                  // $.each(response.blockCount,function(i,x){
                  //   blockList.append(
                  //       '<option value="'+x.block+'">Block '+x.block+'</option>'
                  //     );
                  // });

                  // $('.select-block , .selectPhaseList').select2();

                  getParentDetails();

              });


              
            }
            else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }
            
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });

    }

    function modalContent(argument) {
        return ` <div class="portlet-body">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#eClient" data-toggle="tab" id="assumeBtn"> From Client List </a>
                            </li>
                            <li>
                                <a href="#noneClient" data-toggle="tab" id="assumeBtnTo"> New Client </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="eClient">
                               <div class="form-group">
                                    <label for="single" class="control-label">Select Client Name</label>
                                    <select class="form-control select2" id="slt_selectClient">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            <div class="tab-pane" id="noneClient">
                               <div class="portlet light">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-microphone font-purple-plum"></i>
                                            <span class="caption-subject bold font-purple-plum uppercase"> Client Information </span>
                                            <span class="caption-helper">more samples...</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="scroller" style="height:260px">
                                             <div class="form-group">
                                                <div class="col-md-4">
                                                    <div class="form-group form-md-line-input ">
                                                        <input type="text" class="form-control" placeholder="" id="txt_fname" name="addClient_fname">
                                                        <label class="control-label" for="form_control_1">First Name
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">   
                                                    <div class="form-group form-md-line-input ">
                                                        <input type="text" class="form-control" id="txt_lname"  name="addClient_lname">
                                                        <label class="control-label" for="form_control_1">Last Name
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">  
                                                    <div class="form-group form-md-line-input ">
                                                        <input type="text" class="form-control" id="txt_mname" placeholder="" name="addClient_mname">
                                                        <label class="control-label" for="form_control_1">Middle Name
                                                        </label>
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>   
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-8">
                                                    <div class="form-group form-md-line-input ">
                                                        <input type="text" class="form-control"id="txt_address" placeholder="" name="addClient_address">
                                                        <label class="control-label" for="form_control_1">Address
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group form-md-line-input ">
                                                        <input type="number" class="form-control" id="txt_contactnumber" placeholder="" name="addClient_contactnumber" required>
                                                        <label class="control-label" for="form_control_1">Contact Number
                                                            <span class="required">*</span>
                                                        </label>
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>   
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-4">   
                                                    <div class="form-group form-md-line-input ">
                                                        <input type="text" class="form-control" id="txt_email" placeholder="" name="addClient_email">
                                                        <label class="control-label" for="form_control_1">Email
                                                        </label>
                                                        <div class="form-control-focus"> </div>
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`;
    }

    function getCleintList(argument) {
      showLoading();

      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/property/getCleintList",
          cache: false,
          async: true,
          success: function(response) { 
            if(response.success){
                hideLoading();
                $.each(response.val, function(i,n) {
                    $('#slt_selectClient').append(
                        `<option value="`+ n['id'] +`"> `+ n['name'] +`</option>`
                    );
                })
            }else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }
            
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }

    function assumePropertyToContainer(argument) {
        gl_cpId = $(this).attr('data-clientpropertyid');

        bootbox.confirm({
            title: '<i class="fa fa-edit"> </i>  Assume property to' ,
            message: modalContent,
            size: "large",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                if (result) {
                   if (assumeVal <= 1) {
                        var data = $('#slt_selectClient').val();
                        submitAssumeTo(data, 0)
                   }else if (assumeVal = 2){
                        // if ($("#txt_fname").empty() || $("#txt_fname").empty() || $("#txt_contactnumber").empty()) {
                        //     alert("Some fields are empty!");
                        // }else{
                            var data =[
                                $("#txt_fname").val(),
                                $("#txt_lname").val(),
                                $("#txt_mname").val(),
                                $("#txt_address").val(),
                                $("#txt_contactnumber").val(),
                                $("#txt_email").val()
                            ];

                            submitAssumeTo(data, 1);
                        // }
                    }                   
                }
            }
        });

        getCleintList();
        $('#slt_selectClient').select2();
    }

    function submitAssumeTo(data, type) {
    
        showLoading();
          $.ajax({
              type: 'POST',
              url: apiUrl+"/dashboard/property/assumeTO",
              data: {data: data, type: type, cpID: gl_cpId},
              cache: false,
              async: true,
              success: function(response){ 
                if(response.success){
                    hideLoading();
                    if (type == 0) {
                         swal("Success!", response.message, "success");
                         Change();
                    }else{
                        getCleintList();
                        // swal("Success!", response.message, "success");
                    }
                }
                else{
                  hideLoading();
                  swal("Warning!", response.message, "warning");
                }    
            },
              error: function(err){
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }

}(); 


  
jQuery(document).ready(function() {    
  Property.init();
});