var Report = function() {
    var templates = [],
        baseTemplateUrl_report = 'template/reports/';

    var todayChargeList = [];
    var weeklyChargeList = [];
    var monthlyChargeList = [];
    var chargeDetails = [];
    var USER_TYPE = "";
    var paymentType = 0;
    var selectedProperty = 0;


    function getTemplate(baseTemplateUrl, templateName, callback) {
        if (!templates[templateName]) {
            $.get(baseTemplateUrl + templateName, function(resp) {
                compiled = _.template(resp);
                templates[templateName] = compiled;
                if (_.isFunction(callback)) {
                    callback(compiled);
                }
            }, 'html');
        } else {
            callback(templates[templateName]);
        }
    }

    var employeeListTable;

    function tableemployeeListTable() {
        var table = $('.employeeListTable');
        employeeListTable = table.dataTable({
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No employees added yet",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            // bootstrap_full_number
            "pagingType": "bootstrap_extended",
            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 50, 100],
                [5, 10, 15, 20, 50, 100] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            destroy: true
        });

    }


    return {
        init: init
    };

    // Init
    function init() {
        addEventHandlers();
        loadInitialization();

    }

    function loadInitialization() {


    }

    function addEventHandlers() {
        $(document).on('click', '.openReportPage', openReportHtml);
        $(document).on('click', '.rintReportSales', printContent);
        $(document).on('click', '.btn-searchDate', searchReleaseDate);
        $(document).on('click', '.btn-searchDateSum', searchReleaseDateSumamry);
        $(document).on('click','#btn_overall',createTableOverallReport);
        $(document).on('click', '.btn_searchForDate', searchByDate);
        $(document).on('click', '#btn_incomState', getIncomeStatement);
        $(document).on('click', '.btn_searchForSelectedDate', get_thisSelectedSearchDate);
        $(document).on('click', '#btn_additionalCharges', get_additionalChargesList);
        $(document).on('click', '.btn_additionalChargesSelDate', setDataForSelectedDate);

        $(document).on('click', '#btn_advanceDateSearch', advanceDateSearching)
        $(document).on('click', '#btn_advanceDateSearchCharges', advanceDateSearchingCharges)
        // alert('comming);

        // onclick change
        $(document).on('change', '.selectAgents', selectAgentFunction);
        $(document).on('change', '.selAgentPos ', selectAgentPositions);
        $(document).on('change', '.selAgenPropDetails', SelPropertyDetails);
        $(document).on('change', '#slt_selDate', showDateSellection);
        $(document).on('change', '#slt_selDate', showDateSellection);

        $(document).on('click', '.icheck_reports', function (argument) {
            paymentType = $(this).attr('data-id');
        });
        
    }



    function getDateToday(argument) {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        today = yyyy + '-' + mm + '-' + dd;

        return today;
    }

    function setTableTotalChargesToday(charges) {
        todayChargeList = [];
        let list = [];
        chargeDetails.map(x => {
            if (x[0] !== undefined) {
                if (x[0] === getDateToday()) {
                    if (Number(selectedProperty) === 0) {
                        list = {
                            0: x[0],
                            1: x[1],
                            2: x[2],
                            3: x[3],
                            4: x[4],
                            5: x[5],
                            6: x[6],
                            7: x[7],
                            8: x[10],
                            9: x[11],
                            10: x[8],
                            11:x[9]
                        };    
                    }else{
                        if (Number(x[9]) ===  Number(selectedProperty)) {
                            list = {
                                0: x[0],
                                1: x[1],
                                2: x[2],
                                3: x[3],
                                4: x[4],
                                5: x[5],
                                6: x[6],
                                7: x[7],
                                8: x[10],
                                9: x[11],
                                10: x[8],
                                11:x[9]
                            };    
                        }
                    }

                    todayChargeList.push(list);
                }
            }
        });

        tableChargesData(todayChargeList);
    }

    function tableChargesData(todayChargeList) {
        let totalAmount, totalOffice, totalPartner,totalPartner2=0;

        $("#tbl_additionalCharges").dataTable().fnDestroy();
        table = $('#tbl_additionalCharges').DataTable({
            buttons: [{
                extend: 'excel',
                className: 'btn purple btn-outline ',
                extension: '.xls',
                title: $(".slt_additionalCharges option:selected").text()
            }, ],
            "order": [
                [0, 'asc']
            ],
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                };

                // Total over this page
                totalAmount = api
                    .column(4)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                totalOffice= api
                    .column(5)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                totalPartner = api
                    .column(7)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                    totalPartner2 = api
                    .column(9)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
            }
            
        });

        table.clear().draw();
        table.rows.add(todayChargeList).draw();
        table.row.add( [
                    'End',
                    '',
                    '',
                    '',
                    'Total: ' + totalAmount.toFixed(2),
                    'Total: ' + totalOffice.toFixed(2),
                    '',
                    'Total: ' + totalPartner.toFixed(2),
                    '',
                    'Total: ' + totalPartner2.toFixed(2) ,
                    '',
                    ''
                ] ).draw( false );

        table.buttons().container()
            .appendTo($('.col-sm-6:eq(0)', table.table().container()));
    }


    function setTableWeeklyCharges() {

        var curr = new Date; // get current date
        var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
        var last = first + 6; // last day is the first day + 6

        let firstday = new Date(curr.setDate(first));
        let lastday = new Date(curr.setDate(last));


        todayChargeList = [];
        let  = [];
        chargeDetails.map(x => {
            if (x[0] !== 'none') {
                if (x[0] >= setDateFormat(firstday) && x[0] <= setDateFormat(lastday)) {
                    if (Number(selectedProperty) === 0) {
                        list = {
                            0: x[0],
                            1: x[1],
                            2: x[2],
                            3: x[3],
                            4: x[4],
                            5: x[5],
                            6: x[6],
                            7: x[7],
                            8: x[10],
                            9: x[11],
                            10: x[8],
                            11: x[9]
                        };    
                    }else{
                        if (Number(x[9]) === Number(selectedProperty)) {
                            list = {
                                0: x[0],
                                1: x[1],
                                2: x[2],
                                3: x[3],
                                4: x[4],
                                5: x[5],
                                6: x[6],
                                7: x[7],
                                8: x[10],
                                9: x[11],
                                10: x[8],
                                11: x[9]
                            };    
                        }
                    }

                    todayChargeList.push(list);
                }
            }
        });

        tableChargesData(todayChargeList);

    }

    function setTableMonnthlyCharges() {
        const date = new Date(),
            y = date.getFullYear(),
            m = date.getMonth();
        const firstday = new Date(y, m, 1);
        const lastday = new Date(y, m + 1, 0);

        todayChargeList = [];
        let list = [];
        chargeDetails.map(x => {
            if (x[0] !== 'none') {
                if (x[0] >= setDateFormat(firstday) && x[0] <= setDateFormat(lastday)) {
                    if (Number(selectedProperty) === 0) {
                        list = {
                            0: x[0],
                            1: x[1],
                            2: x[2],
                            3: x[3],
                            4: x[4],
                            5: x[5],
                            6: x[6],
                            7: x[7],
                            8: x[10],
                            9: x[11],
                            10: x[8],
                            11: x[9]
                        };    
                    }else{
                        if (Number(x[9]) === Number(selectedProperty)) {
                            list = {
                                0: x[0],
                                1: x[1],
                                2: x[2],
                                3: x[3],
                                4: x[4],
                                5: x[5],
                                6: x[6],
                                7: x[7],
                                8: x[10],
                                9: x[11],
                                10: x[8],
                                11: x[9]
                            };    
                        }
                    }

                    todayChargeList.push(list);
                }
            }
        });

        tableChargesData(todayChargeList);

    }


    function setDateFormat(thisDate) {
        let newDate = "";

        let dd = thisDate.getDate();
        let mm = thisDate.getMonth() + 1;
        let yy = thisDate.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        newDate = yy + '-' + mm + '-' + dd;

        return newDate;

    }




    function setDataForSelectedDate(argument) {
        const dateType = $('.slt_additionalCharges').val();
        selectedProperty = $('#slt_addtioanChargesPropList').val();

        
        if (dateType === '0') {
            get_additionalChargesList();
        } else if (dateType === '1') {
            setTableTotalChargesToday();
        } else if (dateType === '2') {
            setTableWeeklyCharges();
        } else {
            setTableMonnthlyCharges();
        }
    }


    function searchReleaseDate() {
        showLoading();
        var dateFrom = $('.dateFrom').val();
        var dateTo = $('.dateTo').val();

        // alert(dateFrom + ' ' + dateTo);    
        var searchFor = $('.selectAgentSumRef').val();

        $.ajax({
            type: 'GET',
            url: apiUrl + "dashboard/report/searchReleaseReport",
            data: { dateFrom: dateFrom, dateTo: dateTo, searchID: searchFor },
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {


                    var table;

                    table = $('#comReleaseTable').DataTable();
                    table.clear().draw();
                    table.rows.add(response.comRelease).draw();

                    hideLoading();
                } else {

                    hideLoading();
                    swal("Warning!", response.message, "warning");

                }
            },

            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });

    }


    function selectAgentFunction() {

        $selectedAgent = $('.selectAgents').val();

        switch ($selectedAgent) {
            case '1':
                $('.commissionSD').removeClass('hidden');
                $('.commissionAgent1').addClass('hidden');
                $('.commissionAgent2').addClass('hidden');
                $('.commissionAgent3').addClass('hidden');
                $('.commissionAgent4').addClass('hidden');
                $('.commissionAll').addClass('hidden');

                getAllClientPaymentHistory();

                break;
            case '2':
                $('.commissionSD').addClass('hidden');
                $('.commissionAgent1').removeClass('hidden');
                $('.commissionAgent2').addClass('hidden');
                $('.commissionAgent3').addClass('hidden');
                $('.commissionAgent4').addClass('hidden');
                $('.commissionAll').addClass('hidden');

                getAllClientPaymentHistory();

                break;
            case '3':
                $('.commissionSD').addClass('hidden');
                $('.commissionAgent1').addClass('hidden');
                $('.commissionAgent2').removeClass('hidden');
                $('.commissionAgent3').addClass('hidden');
                $('.commissionAgent4').addClass('hidden');
                $('.commissionAll').addClass('hidden');

                getAllClientPaymentHistory();

                break;
            case '4':
                $('.commissionSD').addClass('hidden');
                $('.commissionAgent1').addClass('hidden');
                $('.commissionAgent2').addClass('hidden');
                $('.commissionAgent3').removeClass('hidden');
                $('.commissionAgent4').addClass('hidden');
                $('.commissionAll').addClass('hidden');

                getAllClientPaymentHistory();

                break;

            case '5':
                $('.commissionSD').addClass('hidden');
                $('.commissionAgent1').addClass('hidden');
                $('.commissionAgent2').addClass('hidden');
                $('.commissionAgent3').addClass('hidden');
                $('.commissionAgent4').removeClass('hidden');
                $('.commissionAll').addClass('hidden');

                getAllClientPaymentHistory();

                break;
            default:
                $('.commissionSD').addClass('hidden');
                $('.commissionAgent1').addClass('hidden');
                $('.commissionAgent2').addClass('hidden');
                $('.commissionAgent3').addClass('hidden');
                $('.commissionAgent4').addClass('hidden');
                $('.commissionAll').removeClass('hidden');

                getAllClientPaymentHistory();
        }
    }


    function openReportHtml() {

        var navParent = $(this);

        $(".dashboardPage").addClass('hidden');

        getTemplate(baseTemplateUrl_report, 'reports.html', function(render) {
            var renderedhtml = render({ data: "" });
            $(".addReporstContainer").html(renderedhtml);
            $('.addReporstContainer').removeClass('hidden');
            $('.page-sidebar-menu').find('.nav-item').removeClass('active open');

            if (navParent.parent().hasClass('nav-item')) {
                navParent.parent().addClass('active open');
            }

            if (navParent.parent().parent().parent().hasClass('nav-item')) {
                navParent.parent().parent().parent().addClass('active open');
            }

            $('.select2').select2();
            $('.date-picker').datepicker({
                format: 'yyyy-mm-dd'
            });
            // all

            var table = $('#example').DataTable({
                buttons: [{
                        extend: 'print',
                        className: 'btn dark btn-outline',
                        message: 'Client property payment history'
                    },
                    {
                        extend: 'pdf',
                        className: 'btn green btn-outline'
                    },
                    {
                        extend: 'excel',
                        className: 'btn purple btn-outline ',
                        extension: '.xls'
                    },
                ],

                scrollX: true,
                scrollCollapse: true,

                columnDefs: [
                    { orderable: false, targets: 0 },
                    { orderable: false, targets: -1 }
                ],
                ordering: [
                    [1, 'asc']
                ],
                colReorder: {
                    fixedColumnsLeft: 1,
                    fixedColumnsRight: 1
                }
            });

            new $.fn.dataTable.FixedColumns(table, {
                leftColumns: 1,
                rightColumns: 1
            });

            table.buttons().container()
                .appendTo($('.col-sm-6:eq(0)', table.table().container()));

            // SD
            var table = $('#example_2').DataTable({
                buttons: [{
                        extend: 'print',
                        className: 'btn dark btn-outline',
                        message: 'Client property payment history'
                    },
                    {
                        extend: 'pdf',
                        className: 'btn green btn-outline'
                    },
                    {
                        extend: 'excel',
                        className: 'btn purple btn-outline ',
                        extension: '.xls'
                    },

                ],
            });


            var tableSummary = $('#Summary').DataTable({
                buttons: [{
                        extend: 'print',
                        className: 'btn dark btn-outline',
                        message: 'Client property payment history'
                    },
                    {
                        extend: 'pdf',
                        className: 'btn green btn-outline'
                    },
                    {
                        extend: 'excel',
                        className: 'btn purple btn-outline ',
                        extension: '.xls'
                    },

                ],
            });

            // new  $.fn.dataTable.FixedColumns( tableSummary, {
            //     leftColumns: 1,
            //     rightColumns: 1
            // } );

            tableSummary.buttons().container()
                .appendTo($('.col-sm-6:eq(0)', tableSummary.table().container()));


            // agent_1
            var tableAgent1 = $('#example_3').DataTable({
                buttons: [{
                        extend: 'print',
                        className: 'btn dark btn-outline',
                        message: 'Client property payment history'
                    },
                    {
                        extend: 'pdf',
                        className: 'btn green btn-outline'
                    },
                    {
                        extend: 'excel',
                        className: 'btn purple btn-outline ',
                        extension: '.xls'
                    },

                ],

            });

            // agent_2
            var tableAgent2 = $('#example_4').DataTable({
                buttons: [{
                        extend: 'print',
                        className: 'btn dark btn-outline',
                        message: 'Client property payment history'
                    },
                    {
                        extend: 'pdf',
                        className: 'btn green btn-outline'
                    },
                    {
                        extend: 'excel',
                        className: 'btn purple btn-outline ',
                        extension: '.xls'
                    },

                ],

            });

            // agent_5
            var tableAgent3 = $('#example_5').DataTable({
                buttons: [{
                        extend: 'print',
                        className: 'btn dark btn-outline',
                        message: 'Client property payment history'
                    },
                    {
                        extend: 'pdf',
                        className: 'btn green btn-outline'
                    },
                    {
                        extend: 'excel',
                        className: 'btn purple btn-outline ',
                        extension: '.xls'
                    },

                ],

            });

            //comrelease
            var releaseTable = $('#comReleaseTable').DataTable({
                columnDefs: [
                    { "visible": true, "targets": 5 }
                ],
                order: [
                    [5, 'asc']
                ],
                displayLength: 25,
                drawCallback: function(settings) {
                    var api = this.api();
                    var rows = api.rows({ page: 'current' }).nodes();
                    var last = null;

                    api.column(5, { page: 'current' }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="6" style="text-align: right;">' + group + '</td></tr>'
                            );

                            last = group;
                        }
                    });
                },
                buttons: [{
                        extend: 'print',
                        className: 'btn dark btn-outline sample',
                        message: 'Client property payment history',
                        customize: function(win) {
                            //
                            var last = null;
                            var colToGroup = 5;
                            $.each($(win.document.body).find('table > tbody > tr'), function(i, n) {
                                var tdLenght = $(this).find('td').length;
                                var groupValue = $(this).find('td').eq(colToGroup).html();

                                if (last !== groupValue) {
                                    $(this).before(
                                        '<tr class="group" style="-webkit-print-color-adjust: exact; background-color: #e7ebee !important;"><td colspan="' + tdLenght + '"><strong >' + groupValue + '</strong></td></tr>'
                                    );
                                    last = groupValue;
                                }
                            });

                            $(win.document.body).find('table').addClass('display').css('font-size', '9px');
                            $(win.document.body).find('tr:nth-child(odd) td').each(function(index) {
                                $(this).css('background-color', '#D0D0D0');
                            });
                            $(win.document.body).find('h1').css('text-align', 'center');

                        }

                    },
                    {
                        extend: 'pdf',
                        className: 'btn green btn-outline'
                    },
                    {
                        extend: 'excel',
                        className: 'btn purple btn-outline ',
                        extension: '.xls'

                    },

                ],

                footerCallback: function(row, data, start, end, display) {
                    var api = this.api(),
                        data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function(i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column(4)
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Total over this page
                    pageTotal = api
                        .column(4, { page: 'current' })
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Update footer
                    $(api.column(4).footer()).html(
                        '₱ ' + pageTotal + ' ( ₱ ' + total + ' Total)'
                    );
                }

            });

            releaseTable.buttons().container()
                .appendTo($('.col-sm-6:eq(0)', releaseTable.table().container()));

            // comisision release in details
            $("#ComRelDetails").dataTable().fnDestroy();
            var comRelDetails = $('#ComRelDetails').DataTable({
                // footerCallback: function ( row, data, start, end, display ) {
                //     var api = this.api(), data;

                //     // Remove the formatting to get integer data for summation
                //     var intVal = function ( i ) {
                //         return typeof i === 'string' ?
                //             i.replace(/[\$,]/g, '')*1 :
                //             typeof i === 'number' ?
                //                 i : 0;
                //     };

                //     // Total over all pages
                //     total = api
                //         .column( 4 )
                //         .data()
                //         .reduce( function (a, b) {
                //             return intVal(a) + intVal(b);
                //         }, 0 );

                //     // Total over this page
                //     pageTotal = api
                //         .column( 4, { page: 'current'} )
                //         .data()
                //         .reduce( function (a, b) {
                //             return intVal(a) + intVal(b);
                //         }, 0 );

                //     // Update footer
                //     $( api.column( 11 ).footer() ).html(
                //         '$'+pageTotal +' ( $'+ total +' total)'
                //     );
                // },

                columnDefs: [
                    { "visible": false, "targets": 13 }
                ],
                order: [
                    [13, 'asc']
                ],
                displayLength: 25,
                drawCallback: function(settings) {
                    var api = this.api();
                    var rows = api.rows({ page: 'current' }).nodes();
                    var last = null;

                    api.column(13, { page: 'current' }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="13" style="text-align: right; padding-right: 165px;">' + group + '</td></tr>'
                            );

                            last = group;
                        }
                    });
                },

                buttons: [{
                        extend: 'print',
                        className: 'btn dark btn-outline sample',
                        message: 'Commission Release Details',
                        customize: function(win) {

                            var last = null;
                            var colToGroup = 13;
                            $.each($(win.document.body).find('table > tbody > tr'), function(i, n) {
                                var tdLenght = $(this).find('td').length;
                                var groupValue = $(this).find('td').eq(colToGroup).html();

                                if (last !== groupValue) {
                                    $(this).before(
                                        '<tr class="group" style="-webkit-print-color-adjust: exact; background-color: #e7ebee !important; text-align: right; padding-right: 165px;"><td colspan="' + tdLenght + '"><strong >' + groupValue + '</strong></td></tr>'
                                    );
                                    last = groupValue;
                                }
                            });

                            $(win.document.body).find('table').addClass('display').css('font-size', '9px');
                            $(win.document.body).find('tr:nth-child(odd) td').each(function(index) {
                                $(this).css('background-color', '#D0D0D0');
                            });
                            $(win.document.body).find('h1').css('text-align', 'center');

                        }
                    },

                    {
                        extend: 'pdf',
                        className: 'btn green btn-outline'
                    },

                    {
                        extend: 'excel',
                        className: 'btn purple btn-outline ',
                        extension: '.xls'
                    },

                ]
            });

            comRelDetails.buttons().container()
                .appendTo($('.col-sm-6:eq(0)', comRelDetails.table().container()));
            
            checkUserAcount();

        });

        displayPropertyNames();
        getIncomeStatement();
    }


    function displayPropertyNames(){
        $.ajax({
                type:'GET',
                url:apiUrl+'dashboard/report/getAllPropertyName',
                cache:false,
                async:true,
                success: function(response){

                    $('.slt_proplist').empty();

                    $('.slt_proplist').append(
                        '<option value=0> All </option>'
                    );


                    $.each(response.result,function(i,n){
                        $('.slt_proplist').append(
                            '<option value="' + n.id + '"> ' + n.propertyName + ' </option>'
                        )
                    });

                },
                error: function(err){
                    hideLoading();
                    swal("Error!", err.statusText, "error");
                }
        });
    }

    function checkUserAcount(argument) {
        $.ajax({
            type: 'GET',
            url: apiUrl + "dashboard/report/checkUserAccount",
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {
                    USER_TYPE = response.role;
                    if (response.role != "Business Partner") {
                        getAllClientPaymentHistory();
                    }else{
                        getPropertyListByUserAccount();

                        $('#comReport_div').remove();
                        $('#ComReport').remove();
                        $('#comSummary_div').remove();
                        $('#ComSummary').remove();
                        $('#comRelease_div').remove();
                        $('#ComDetails').remove();
                        $('#incomeState').addClass('active');
                    }

                } else {

                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }
            },

            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }


    function getPropertyListByUserAccount(){
         $.ajax({
            type: 'GET',
            url: apiUrl + "dashboard/report/getPropertyByUserAccount",
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {

                    $('.selAgenPropDetails').empty();
                    $('.slt_proplist').empty();
                    $.each(response.prop, function(i, n) {
                        $('.selAgenPropDetails').append(
                            '<option value="' + n.propID + '"> ' + n.propName + ' </option>'
                        );
                        $('.slt_proplist').append(
                            '<option value="' + n.propID + '"> ' + n.propName + ' </option>'
                        );
                    })

                    // $('.selAgenPropDetails').append(
                    //     '<option value="' + 0 + '"> All </option>'
                    // );
                    // $('.slt_proplist').prepend(
                    //     '<option value="' + 0 + '" selected> All </option>'
                    // );

                    setTimeout(function() {
                        get_thisSelectedSearchDate();
                    }, 1000);

                } else {

                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }
            },

            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });


    }

    function getAllClientPaymentHistory() {
        //remove for a while
        // var selectedAgentValue = $('.selectAgents').val();

        // showLoading('Getting all client Payments...');
        // $.ajax({ 
        //     type: 'GET',
        //     url: apiUrl + "dashboard/report/getClientPayment",
        //     data: { id: selectedAgentValue },
        //     cache: false,
        //     async: true,
        //     success: function(response) {

        //         // alert(response.ClientsPayHis);
        //         if (response.success) {
        //             var table = "";

        //             table = $('#comReleaseTable').DataTable();
        //             table.clear().draw();
        //             table.rows.add(response.comRelease).draw();

        //             table = $('#Summary').DataTable();
        //             table.clear().draw();
        //             table.rows.add(response.summary).draw();

        //             table = $('#ComRelDetails').DataTable();
        //             table.clear().draw();
        //             table.rows.add(response.ComSummary).draw();

        //             $('.selAgenPropDetails').empty();
        //             $('.slt_proplist').empty();
        //             $.each(response.prop, function(i, n) {
        //                 $('.selAgenPropDetails').append(
        //                     '<option value="' + n.propID + '"> ' + n.propName + ' </option>'
        //                 );
        //                 $('.slt_proplist').append(
        //                     '<option value="' + n.propID + '"> ' + n.propName + ' </option>'
        //                 );
        //             })

        //             $('.selAgenPropDetails').append(
        //                 '<option value="' + 0 + '"> All </option>'
        //             );
        //             $('.slt_proplist').prepend(
        //                 '<option value="' + 0 + '" selected> All </option>'
        //             );

        //             if (selectedAgentValue == 0) {
        //                 table = $('#example').DataTable();
        //                 table.clear().draw();
        //                 table.rows.add(response.ClientsPayHis).draw();

        //             } else if (selectedAgentValue == 1) {

        //                 table = $('#example_2').DataTable();
        //                 table.clear().draw();
        //                 table.rows.add(response.ClientsPayHis).draw();

        //             } else if (selectedAgentValue == 2) {

        //                 table = $('#example_3').DataTable();
        //                 table.clear().draw();
        //                 table.rows.add(response.ClientsPayHis).draw();

        //             } else if (selectedAgentValue == 3) {

        //                 table = $('#example_4').DataTable();
        //                 table.clear().draw();
        //                 table.rows.add(response.ClientsPayHis).draw();

        //             } else if (selectedAgentValue == 4) {

        //                 table = $('#example_5').DataTable();
        //                 table.clear().draw();
        //                 table.rows.add(response.ClientsPayHis).draw();

        //             } else if (selectedAgentValue == 5) {

        //                 table = $('#example_6').DataTable();
        //                 table.clear().draw();
        //                 table.rows.add(response.ClientsPayHis).draw();

        //             }

        //             hideLoading();

        //         } else {

        //             hideLoading();
        //             // swal("Warning!", response.message, "warning");
        //             // console.log(response.message);

        //         }
        //     },

        //     error: function(err) {
        //         hideLoading();
        //         swal("Error!", err.statusText, "error");
        //     }
        // });
    }


    var clientPaymentHistoryTable;

    function tableclientPaymentHistoryTable() {
        var table = $('.commissionReportTable');
        clientPaymentHistoryTable = table.dataTable({
            "buttons": [{
                    extend: 'print',
                    className: 'btn dark btn-outline',
                    message: 'Client property payment history'
                        // exportOptions: {
                        //     columns: [ 0, 1]
                        // }
                },
                {
                    extend: 'pdf',
                    className: 'btn green btn-outline'
                },
                {
                    extend: 'excel',
                    className: 'btn purple btn-outline ',
                    extension: '.xls'
                        // exportOptions: {
                        //     columns: [ 0, 1]
                        // }
                },
                // 'print', 'excel', 'pdf'
            ],
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No client payment added yet",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            // bootstrap_full_number
            "pagingType": "bootstrap_extended",
            "order": [
                [0, 'asc']
            ],
            responsive: true,
            "lengthMenu": [
                [5, 10, 15, 20, 50, 100],
                [5, 10, 15, 20, 50, 100] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            destroy: true
        });

        var total = 0;
        var table = $('.exportControllContainer').DataTable();
        table.on('order.dt search.dt', function() {

            table.column(5, { search: 'applied', order: 'applied' }).nodes().each(function(cell, i) {
                cell.innerHTML = accounting.format(cell.innerHTML, 2);
                total = Number(cell.innerHTML);
            });
            table.column(6, { search: 'applied', order: 'applied' }).nodes().each(function(cell, i) {
                total = Number(cell.innerHTML);
                cell.innerHTML = accounting.format(cell.innerHTML, 2);
            });

            total = table.column(6).data().sum();

        }).draw();

        //new code 

        $('#totalVAl').text('Total of Debit and Credit: ' + document.getElementById("sALesReport").rows[3].cells[4].innerText);
        $('.exportControllContainer').empty();
        table.buttons(0, null).container().appendTo('.commissionReportTable');
    }


    function printContent() {
        w = window.open();
        w.document.write($('PrintResult').html());
        w.print();
        w.close();
    }

    function searchReleaseDateSumamry() {
        var date1 = $('.dateFromSum').val();
        var date2 = $('.dateToSum').val();

        $.ajax({
            type: 'GET',
            url: apiUrl + "dashboard/report/getCommissionSummary",
            data: { date1: date1, date2: date2 },
            cache: false,
            async: true,
            success: function(response) {
                // alert(response.ClientsPayHis);
                if (response.success) {
                    var table = "";

                    table = $('#Summary').DataTable();
                    table.clear().draw();
                    table.rows.add(response.summary).draw();
                    hideLoading();

                } else {

                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }
            },

            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });

    }


    function selectAgentPositions(argument) {
        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "dashboard/report/getSelectedAgentPosition",
            data: { selVal: $(this).val() },
            cache: false,
            async: true,
            success: function(response) {
                // alert(response.ClientsPayHis);
                if (response.success) {

                    $("#ComRelDetails").dataTable().fnDestroy();
                    var table = "";

                    table = $('#ComRelDetails').DataTable();
                    table.clear().draw();
                    table.rows.add(response.ComSummary).draw();

                    hideLoading();

                } else {

                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }
            },

            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });

    }

    function SelPropertyDetails() {

        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "dashboard/report/getThisPropDetails",
            data: { propID: $(this).val(), selVal: $('.selAgentPos').val() },
            cache: false,
            async: true,
            success: function(response) {
                // alert(response.ClientsPayHis);
                if (response.success) {

                    $("#ComRelDetails").dataTable().fnDestroy();
                    var table = "";

                    // switch($(this).val()) {
                    //     case 0:

                    //         break;
                    //     case 2:

                    //         break;
                    //     case 3:

                    //         break;
                    //     default:

                    // }

                    table = $('#ComRelDetails').DataTable();
                    table.clear().draw();
                    table.rows.add(response.ComSummary).draw();

                    hideLoading();
                } else {

                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }
            },

            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }

    function searchByDate() {
        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "dashboard/report/getThisPropDetailsByDate",
            data: { propID: $('.selAgenPropDetails').val(), selVal: $('.selAgentPos').val(), date1: $('.sel_dateFrom').val(), date2: $('.sel_dateTo').val() },
            cache: false,
            async: true,
            success: function(response) {
                // alert(response.ClientsPayHis);
                if (response.success) {
                    $("#ComRelDetails").dataTable().fnDestroy();
                    var table = "";

                    table = $('#ComRelDetails').DataTable();
                    table.clear().draw();
                    table.rows.add(response.ComSummary).draw();

                    hideLoading();
                } else {

                    hideLoading();
                    swal("Warning!", response.message, "warning");
                }
            },

            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }

    function getIncomeStatement(argument) {
        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "dashboard/report/getIncomStatement",
            cache: false,
            async: true,
            success: function(response) {      
                if (response.success) {
                    if (USER_TYPE != "Business Partner") {

                        incomeStatementTable(response.data,0);
                    }else{
                        get_thisSelectedSearchDate();
                    }
                } else {
                   
                    $('#icomeStement').DataTable();
                }
                hideLoading();

            },

            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
        
    }

    function createTableOverallReport(){
        $("#overalltable").dataTable().fnDestroy();
        $("#overalltable").remove();
        var container=$('.overallReportTable');
        var table=document.createElement("table");
        table.id += "overalltable";
        var tr=document.createElement("tr");
        var thead=document.createElement("thead");
        var thTotal = document.createElement("th");
        thTotal.appendChild(document.createTextNode("Total"));
    
        var thEmpty = document.createElement("th");
        thEmpty.appendChild(document.createTextNode('\u00A0\u00A0'));
        thEmpty.className += "empty-th";
        thEmpty.style.width='117px';
        tr.appendChild(thEmpty); 
        
        var rowDetails=["Cash Income 100%", "Bank Income 100%","2%","3%"];
        var rowDetails2=["Penalty"];
        $.ajax({
            type:'GET',
            url:apiUrl + "dashboard/report/getCashIncome",
            cache:false,
            async:true,
            success: function(response){

                var tableHeader = response.property;
                Object.keys(tableHeader).map((key,index)=>{
                    var th  = document.createElement("th");
                    th.appendChild(document.createTextNode(tableHeader[key]['propertyName']));
                    tr.appendChild(th);
                });
               
               tr.append(thTotal);
               thead.appendChild(tr);
               table.append(thead);
               container.append(table);
             
              var t=$('#overalltable').DataTable({buttons: [{
                    extend: 'excel', 
                    className: 'btn purple btn-outline ',
                    extension: '.xls',
                    title: $(".slt_proplist option:selected").text()
                }, ],"order":[]});
                

                for (var z=0; z < rowDetails.length ;z++){
                    var totalHorizontal=0,index=[];
                    index.push(rowDetails[z]);
                        for (var y=0;y<=response.overallIncome[z].length;y++){
                            index.push(response.overallIncome[y][z]);
                            totalHorizontal = (parseFloat(totalHorizontal) + parseFloat( response.overallIncome[y][z])).toFixed(2); 
                        }

                    index.push(totalHorizontal);
                    t.row.add(index).draw(false);
                }   
                var chargeObject = response.totalCharges[0];
                for (var x=0;x<rowDetails2.length;x++){
                    var totalHorizontal =  0;
                    var index           = [];
                    index.push(rowDetails2[x]);
                    for (var y=0;y < Object.keys(chargeObject).length;y++){
                        index.push(response.totalCharges[x][tableHeader[y]['propertyName']]);
                        totalHorizontal = (parseFloat(totalHorizontal) + parseFloat( response.totalCharges[x][tableHeader[y]['propertyName']])).toFixed(2); 
                    }
                    index.push(totalHorizontal);
                    t.row.add(index).draw(false);
                }


            
                t.buttons().container()
                .appendTo($('.col-sm-6:eq(0)', t.table().container()));
            },
            error:function(err){
            }
        });
    }

    function incomeStatementTable(dataVal,selectedProperty) {
        let ten = 0, second = 0, three = 0, sales = 0,  tax = 0, hundred = 0, totalTaxAndOffice=0;
        $("#icomeStement").dataTable().fnDestroy();
        var table = "";
        if (selectedProperty == 1){
            $('#two-percent').html('Don Sakagawa');
            $('#three-percent').html('Alex');
        }else if (selectedProperty == 2){
            $('#two-percent').html('Paul');
            $('#three-percent').html('Don Sakagawa');
        }else if (selectedProperty == 3){
            $('#two-percent').html('Beverly Son');
            $('#three-percent').html('Don Sakagawa');
        }else if (selectedProperty == 4){
            $('#two-percent').html('Beverly Son');
            $('#three-percent').html('Alex');
        }else if (selectedProperty == 5){
            $('#two-percent').html('Beverly Son');
            $('#three-percent').html('Alex');
        }else if (selectedProperty == 0){
            $('#two-percent').html('2%');
            $('#three-percent').html('3%');
        }
        table = $('#icomeStement').DataTable({
            buttons: [{
                extend: 'excel', 
                className: 'btn purple btn-outline ',
                extension: '.xls',
                title: $(".slt_proplist option:selected").text()
            }, ],
            "order": [
                [0, 'asc']
            ],
             "footerCallback": function (row, data, start, end, display) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                };

                // Total over this page
                sales = api
                    .column(11)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                ten = api
                    .column(12)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                second = api
                    .column(13)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                three = api
                    .column(14)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
                tax = api
                    .column(15)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
               
                hundred = api
                    .column(19)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);
            }

        });

        table.clear().draw();
        table.rows.add(dataVal).draw();
        table.rows().every(function(rowIdx, tableLoop, rowLoop) {
            var data = this.data();
            let newVal = data[3] % 10;
            let k  = data[3] % 100;
            if (newVal == 1 && k != 11) {
                data[3] += 'st'
            } else if (newVal == 2 && k != 12) {
                data[3] += 'nd'
            } else if (newVal == 3 && k != 13) {
                data[3] += 'rd'
            } else {
                data[3] += 'th'
            }
            this.data(data)
        });
        
        const totalOffce = dataVal.reduce(function (accumulator, data) {
          return accumulator + Number(data[21]);
        }, 0);

        const totalPartner1  = dataVal.reduce(function (accumulator, data) {
          return accumulator + Number(data[22]);
        }, 0);

        const totalPartner2  = dataVal.reduce(function (accumulator, data) {
            return accumulator + Number(data[23]);
          }, 0);
        totalTaxAndOffice= (parseFloat(tax.toFixed(2))+ parseFloat(totalOffce.toFixed(2)));
        table.row.add( [
            'End',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'Total: ' + sales.toFixed(2),
            'Total: ' + ten.toFixed(2),
            'Total: ' + second.toFixed(2),
            'Total: ' + three.toFixed(2),
            'Total: ' + tax.toFixed(2),
            'Total: ' + totalOffce.toFixed(2),
            'Total: ' + totalPartner1.toFixed(2),
            'Total: '+ totalPartner2.toFixed(2),
            'Total: ' + hundred.toFixed(2),
            '',
            ''

        ] ).draw( false );

        table.row.add( [
            'Total Tax And Office:',
            ''+totalTaxAndOffice.toFixed(2),
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''

        ] ).draw( false );

    

        table.buttons().container()
            .appendTo($('.col-sm-6:eq(0)', table.table().container()));
    }


    function get_thisSelectedSearchDate(argument) {
        const sel_property = $('.slt_proplist').val();
        const sel_date = $('.slt_dateSelection').val();


        const type = $('input[name="rd_Options"]:checked').val();
        
        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "dashboard/report/getPropertyDetailsByDate",
            data: { selectedroperTy: sel_property, dateToSearch: sel_date , paymentType: type},
            cache: false,
            async: true,
            success: function(response) {

                if (response.success) {
                    incomeStatementTable(response.data,sel_property);
                } else {
                    var table = "";
                    table = $('#icomeStement').DataTable();
                    table.clear().draw();
                    swal("Warning!", response.message, "warning");
                }

                hideLoading();
            },

            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }

    function get_additionalChargesList(argument) {
        const selectedProperty =  $('#slt_addtioanChargesPropList').val();
        const type = ($("input[name='rd_Options']:checked").attr('data-id') == undefined) ? 'All' : $("input[name='rd_Options']:checked").attr('data-id');
          
        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "dashboard/report/getAdditionalChargest",
            data:{transactionType : type},
            cache: false,
            async: false,
            success: function(response) {
                if (response.success) {
                    chargeDetails = response.data;
                    todayChargeList = [];
                    chargeDetails.map(x => {    


                     
                        if (x[0] !== 'none') {
                            if (Number(selectedProperty) == 0) {
                                const list = {
                                    0: x[0],
                                    1: x[1],
                                    2: x[2],
                                    3: x[3],
                                    4: x[4],
                                    5: x[5],
                                    6: x[6],
                                    7: x[7],
                                    8: x[10],
                                    9: x[11],
                                    10: x[8],
                                    11: x[9]
                                };   
                             
                                
                               returList(list);

                            }else{
                                if (Number(x[10]) == Number(selectedProperty)) {
                                    
                                    const list = {
                                        0: x[0],
                                        1: x[1],
                                        2: x[2],
                                        3: x[3],
                                        4: x[4],
                                        5: x[5],
                                        6: x[6],
                                        7: x[7],
                                        8: x[10],
                                        9: x[11],
                                        10: x[8],
                                        11: x[9]

                                    };    
                                   
                                    returList(list);
                                    
                                }
                            }
                        }
                    });

                    tableChargesData(todayChargeList);

                } else {
                    var table = "";
                    table = $('#tbl_additionalCharges').DataTable();
                    table.clear().draw();

                    swal("Warning!", response.message, "warning");
                }
                hideLoading();
            },

            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        });
    }


    function returList(list) {
        return todayChargeList.push(list);
    }

   "use strict";    

    function advanceDateContaner(){
        let selectTOptions = String("<option value=''> <option>");
        const year = (new Date()).getFullYear();
        selectTOption = "";
        let arr = [];
        for (var i = 1905; i <= year; i++) {
            arr.push("<option value='"+i+"'> "+i+" </option>");
        }
        arr.reverse();
        selectTOption = arr.join("");

        return `
            <div class="alert alert-success">
               <strong>Info!</strong> Desplayed property details will be based on the selected property in the select option before advanced date search. 
            </div> 
            <div id="advance_searchPropForm"></div>      
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group" style="width: 100%;">
                        <label for="">Search Date:</label>
                        <select name="" class="form-control select2" id="slt_selDate">
                           <option value="0"> Year </option>                           
                           <option value="1"> Month </option>                           
                           <option value="2"> Custom </option>                           
                       </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" style="width: 100%;">
                        <label for="">Year:</label>
                        <select name="year" class="form-control select2" id="year_selection" >
                            `+ selectTOption +`
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="dateSelection"> </div>
                </div>
            </div>`;
    }

    function advanceDateSearching(argument) {
        // alert('comming soon');
        bootbox.confirm({
            title: "<strong> <span class='fa fa-calendar'></span> Advance date search</strong>",
            message: advanceDateContaner(),
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                // console.log('This was logged in the callback: ' + result);
                const DATE_SELECTED = document.getElementById('slt_selDate').value;
                let data = [];
                let value = [];

                if(result){
                    switch (Number(DATE_SELECTED)) {
                        case 0:
                            value = {
                                option: 0,
                                data : {
                                    year: document.getElementById('year_selection').value
                                }
                            }

                            break;
                        case 1:
                            value = {
                                option: 1,
                                data : {
                                    year: document.getElementById('year_selection').value,
                                    month: document.getElementById('slt_monthList').value
                                }
                            }
                            
                            break;
                        default:
                            value = {
                                option: 2,
                                data : {
                                    from: document.getElementById('date_from_report').value,
                                    to: document.getElementById('date_to_report').value
                                }
                            }
                            break;
                    }

                    data.push(value);
                    searchPropertiesAdvanceSearch(data);
                }
            }
        });

        $('#slt_selDate, #year_selection').select2();
    }

    function advanceDateSearchingCharges(argument) {
        // alert('comming soon');
        bootbox.confirm({
            title: "<strong> <span class='fa fa-calendar'></span> Advance date search</strong>",
            message: advanceDateContaner(),
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                // console.log('This was logged in the callback: ' + result);
                const DATE_SELECTED = document.getElementById('slt_selDate').value;
                selectedProperty = $('#slt_addtioanChargesPropList').val();

                let year = '';
                let month = '';
                let from = '';
                let to = '';
                todayChargeList = [];
                let list = [];

                if(result){    

                    switch (Number(DATE_SELECTED)) {
                        case 0:

                              year = document.getElementById('year_selection').value;
                              list = {
                                    0: '',
                                    1: '',
                                    2: '',
                                    3: '',
                                    4: '',
                                    5: '',
                                    6: '',
                                    7: '',
                                    8: '',
                                    9:'',
                                    10:'',
                                    11:''
                                };

                                chargeDetails.map(x => {
                                    if (x[0] !== 'none') {
                                        
                                        const d = new Date(x[0])
                                        if (Number(d.getFullYear()) === Number(year)) {
                                            if (Number(selectedProperty) === 0) {
                                                const list = {
                                                    0: x[0],
                                                    1: x[1],
                                                    2: x[2],
                                                    3: x[3],
                                                    4: x[4],
                                                    5: x[5],
                                                    6: x[6],
                                                    7: x[7],
                                                    8: x[10],
                                                    9:x[11],
                                                    10:x[8],
                                                    11:x[9]
                                                };    

                                                todayChargeList.push(list);

                                            }else{

                                                if (Number(x[9]) === Number(selectedProperty)) {
                                                    const list = {
                                                        0: x[0],
                                                        1: x[1],
                                                        2: x[2],
                                                        3: x[3],
                                                        4: x[4],
                                                        5: x[5],
                                                        6: x[6],
                                                        7: x[7],
                                                        8: x[10],
                                                        9:x[11],
                                                        10:x[8],
                                                        11:x[9]
                                                    };    
                                                   
                                                    todayChargeList.push(list);
                                                }
                                            }

                                            
                                        }
                                    }
                                });

                                tableChargesData(todayChargeList);


                            break;
                        case 1:

                                year  =  document.getElementById('year_selection').value;
                                month =  document.getElementById('slt_monthList').value;

                                list = {
                                    0: '',
                                    1: '',
                                    2: '',
                                    3: '',
                                    4: '',
                                    5: '',
                                    6: '',
                                    7: '',
                                    8: '',
                                    9:'',
                                    10:'',
                                    11:''
                                };

                                chargeDetails.map(x => {
                                    if (x[0] !== 'none') {

                                        const d = new Date(x[0])

                                        if (Number(d.getFullYear()) === Number(year)  && Number(d.getMonth() + 1) === Number(month)) {
                                            if (Number(selectedProperty) === 0) {

                                                list = {
                                                    0: x[0],
                                                    1: x[1],
                                                    2: x[2],
                                                    3: x[3],
                                                    4: x[4],
                                                    5: x[5],
                                                    6: x[6],
                                                    7: x[7],
                                                    8: x[10],
                                                    9:x[11],
                                                    10:x[8],
                                                    11:x[9]
                                                };

                                            }else{

                                                if (Number(x[9]) === Number(selectedProperty)) {
                                                    list = {
                                                        0: x[0],
                                                        1: x[1],
                                                        2: x[2],
                                                        3: x[3],
                                                        4: x[4],
                                                        5: x[5],
                                                        6: x[6],
                                                        7: x[7],
                                                        8: x[10],
                                                        9:x[11],
                                                        10:x[8],
                                                        11:x[9]
                                                    };    
                                                }
                                            }

                                            todayChargeList.push(list);
                                        }
                                    }
                                });

                                tableChargesData(todayChargeList);
                         
                            
                            break;
                        default:

                                from = document.getElementById('date_from_report').value;
                                to   = document.getElementById('date_to_report').value;
                                todayChargeList.pop();  
                                list = {
                                    0: '',
                                    1: '',
                                    2: '',
                                    3: '',
                                    4: '',
                                    5: '',
                                    6: '',
                                    7: '',
                                    8: '',
                                    9:'',
                                    10:'',
                                    11:''
                                };  

                                 

                                chargeDetails.map(x => {
                                    if (x[0] !== 'none') {
                                        const d = new Date(x[0]);

                                        if (x[0] >= from && x[0] <= to) {
                                            if (Number(selectedProperty) === Number(0)) {
                                                list = {
                                                    0: x[0],
                                                    1: x[1],
                                                    2: x[2],
                                                    3: x[3],
                                                    4: x[4],
                                                    5: x[5],
                                                    6: x[6],
                                                    7: x[7],
                                                    8: x[10],
                                                    9:x[11],
                                                    10:x[8],
                                                    11:x[9]
                                                };    
                                                 todayChargeList.push(list);

                                            }else{

                                                if (Number(x[9]) === Number(selectedProperty)) {
                                                    list = {
                                                        0: x[0],
                                                        1: x[1],
                                                        2: x[2],
                                                        3: x[3],
                                                        4: x[4],
                                                        5: x[5],
                                                        6: x[6],
                                                        7: x[7],
                                                        8: x[10],
                                                        9: x[11],
                                                        10: x[8],
                                                        11:x[9]
                                                    };    
                                                     todayChargeList.push(list);

                                                }

                                            }


                                        }
                                    }
                                });

                                tableChargesData(todayChargeList);


                                // chargeDetails.map(x => {

                                //     if (x[0] >= from && x[0] <= to){
                                //        list = {
                                //                 0: x[0],
                                //                 1: x[1],
                                //                 2: x[2],
                                //                 3: x[3],
                                //                 4: x[4],
                                //                 5: x[5],
                                //                 6: x[6],
                                //                 7: x[7],
                                //                 8: x[8]
                                //             };    
                                //            todayChargeList.push(list);
                                //     } 


                                // }); 
                               
                                // tableChargesData(todayChargeList);
                                
                            
                            break;
                    }
                }
            }
        });

        $('#slt_selDate, #year_selection').select2();
        $('.alert-success').addClass('hidden');

    }

    function showDateSellection(argument) {
        const DATE_SELECTED = document.getElementById('slt_selDate').value;
        let FORM_SHOW = "";
        switch(Number(DATE_SELECTED)){
            case 1:
                FORM_SHOW = `
                        <div class="form-group" style="width: 100%;">
                            <label for="">Search Date:</label>
                            <select name="" class="form-control select2" id="slt_monthList">
                               <option value="1"> Jan </option>                           
                               <option value="2"> Feb </option>                           
                               <option value="3"> Mar </option>                           
                               <option value="4"> Apr </option>                           
                               <option value="5"> May </option>                           
                               <option value="6"> Jun </option>                           
                               <option value="7"> Jul </option>                           
                               <option value="8"> Aug </option>                           
                               <option value="9"> Sep </option>                           
                               <option value="10"> Oct </option>                           
                               <option value="11"> Nov </option>                           
                               <option value="12"> Dec </option>                           
                           </select>
                        </div>`;
                break;
            case 2:
                 FORM_SHOW = `
                    <div class="form-group" style="width: 100%;">
                        <label for="">From:</label>
                        <input type="text" class="form-control" id="date_from_report">
                    </div>
                 
                    <div class="form-group" style="width: 100%;">
                        <label for="">To:</label>
                        <input type="text" class="form-control" id="date_to_report">
                    </div>`;
                break;
            default:
                FORM_SHOW = "";
                break;
        }
        $('#dateSelection').empty();
        $('#dateSelection').append(FORM_SHOW);    
        $('#slt_monthList').select2();
        $('#date_from_report, #date_to_report').datepicker({
            format: "yyyy-mm-dd"
        });    
    }

    function searchPropertiesAdvanceSearch(data) {
        const propID = $('.slt_proplist').val();
        const type = $('input[name="rd_Options"]:checked').val();

        showLoading();
        $.ajax({
            type: 'GET',
            url: apiUrl + "dashboard/report/getPropertiesAdvanceSearh",
            data : {propID: propID, data: data, paymentType: type},
            cache: false,
            async: true,
            success: function(response) {
                if (response.success) {
                   incomeStatementTable(response.data,propID);
                } else {
                    var table = "";
                    table = $('#icomeStement').DataTable();
                    table.clear().draw();

                    swal("Warning!", response.message, "warning");
                }
                hideLoading();
            },

            error: function(err) {
                hideLoading();
                swal("Error!", err.statusText, "error");
            }
        })
    }


}();




jQuery(document).ready(function() {
    Report.init();
});