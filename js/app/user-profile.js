var User = function () {
    var templates = [],
            baseTemplateUrl_user = 'template/users/';


    function getTemplate(baseTemplateUrl, templateName, callback) {
        if (!templates[templateName]) {
            $.get(baseTemplateUrl + templateName, function(resp) {
                compiled = _.template(resp);
                templates[templateName] = compiled;
                if (_.isFunction(callback)) {
                    callback(compiled);
                }
            }, 'html');
        } else {
            callback(templates[templateName]);
        }
    }
    
 

  

    return {
        init: init
    };

    // Init
    function init() {
        addEventHandlers();
        loadInitialization();
        
    }

    function loadInitialization(){
      

    }





    function addEventHandlers(){
        $(document).on('click', '.employeeListTable td:not(:nth-child(5))', openUserProfilePage );
        $(document).on('click', '.btn_updateThisEmployee', openUserProfilePage2 );
        
        $(document).on('click', '.userProfileHTML  #btnfile', updateProfilePicClick ); 
        $(document).on('change', '.userProfileHTML  #file_uploads', updateProfilePicChange ); 
        $(document).on('submit', '#employeeProfilePictureForm', uploadThisImage ); 
        
        $(document).on('click', '.updateUserProfileInfoform', openUpdateUserProfileInfoform );
        
        $(document).on('click', '.updateuser_btnPasswordUpdatesubmit', updateThisUserPassword ); 
        $(document).on('click', '.updateuser_btnInformationUpdatesubmit', updateThisUserInfo ); 
        
        $(document).on('click', '.overView', ifoverView ); 
        $(document).on('click', '.updateInfo', ifupdateInfo ); 
        $(document).on('click', '.changePass', ifchangePass ); 
    }


    function uploadThisImage(event){
      var formdata = new FormData(this);

      confirmWithSwal(function(){
        showLoading();

        $.ajax({
          url: apiUrl+"/dashboard/user/uploadThisImage",
          type: 'POST',
          data: formdata,
          async: true,
          cache: false,
          contentType: false,
          // enctype: 'multipart/form-data',
          processData: false,
          success: function (response) {      
            
            hideLoading();
            if(response.success){
              if(response.userID == $('.headerGoToUserProfile').attr('data-id')){
                $('.headerGoToUserProfile').parent().parent().parent().find('.activeUserIcon').attr('src',$('.userProfileImage').attr('src'));
              }

              setTimeout(function(){swal('Success!',response.message,'success');},0);
            }
            else{
              setTimeout(function(){swal('Warning!',response.message,'warning');},0);
            }

          },
          error: function(err){
            hideLoading();
            setTimeout(function(){swal("Error!", err.statusText, "error");},0);
          }
        });

      },'Are you sure to change image?','warning');

      event.stopPropagation();
      event.preventDefault(); 
    }


    function  ifoverView(){
        $(".overView").css({"color": "#169ef4", "text-decoration": "none", "background": "#ecf5fb", "border-left": "solid 2px #169ef4"});
        $(".updateInfo").css({"color": "#557386", "text-decoration": "none", "background": "#f0f6fa", "border-left": "solid 2px #c4d5df"});
        $(".changePass").css({"color": "#557386", "text-decoration": "none", "background": "#f0f6fa", "border-left": "solid 2px #c4d5df"});
        $(".img-update").addClass("hidden");
        $(".profile-edit").addClass("hidden");
    }
    
    function  ifupdateInfo(){
        $(".updateInfo").css({"color": "#169ef4", "text-decoration": "none", "background": "#ecf5fb", "border-left": "solid 2px #169ef4"});
        $(".overView").css({"color": "#557386", "text-decoration": "none", "background": "#f0f6fa", "border-left": "solid 2px #c4d5df"});
        $(".changePass").css({"color": "#557386", "text-decoration": "none", "background": "#f0f6fa", "border-left": "solid 2px #c4d5df"});
        $(".img-update").removeClass("hidden");
        $(".profile-edit").removeClass("hidden");
    }


    function  ifchangePass(){
      $(".changePass").css({"color": "#169ef4", "text-decoration": "none", "background": "#ecf5fb", "border-left": "solid 2px #169ef4"});
      $(".updateInfo").css({"color": "#557386", "text-decoration": "none", "background": "#f0f6fa", "border-left": "solid 2px #c4d5df"});
      $(".overView").css({"color": "#557386", "text-decoration": "none", "background": "#f0f6fa", "border-left": "solid 2px #c4d5df"});
      $(".img-update").addClass("hidden");
      $(".profile-edit").addClass("hidden");
    }

    


    function updateProfilePicClick(){
      $(".userProfileHTML #file_uploads").click();
    }

    


    function updateProfilePicChange(){
      var input = this;
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('.userProfileHTML  #btnfile').attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]);
      }
      
      // var x = document.getElementById(".userProfileHTML  file_uploads").value;
              
      // var path = x;
      // var pos =path.lastIndexOf( path.charAt( path.indexOf(":")+1) );
      // filename = path.substring( pos+1); 
    }



    function openUpdateUserProfileInfoform(){
      var pos = $('.updateuser_position').attr('data-value');
      $('.updateuserForm  input').focus();
      $.each($('.updateuser_position > option'),function(i,n){
          if($(this).val() == pos)
              $(this).attr('selected','');
      });
    }


    function openUserProfilePage(){
      var id = $(this).parent().find('.btn_updateThisEmployee').attr('data-id');
      
      getThisUser(id);
    }

    function openUserProfilePage2(){
      var id = $(this).attr('data-id');

      getThisUser(id);
    }


    function getThisUser(id){
      showLoading();

      if(id == undefined || id == 0 || id == "")
        swal('Warning!',"Can't find that user/employee.",'error');

      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/user/getAllInfoForThisUser",
          data : {ID : id},
          cache: false,
          async: false, 
          success: function(response) {
            if(!response.has_login)
              logoutAccount(); 

            if(response.success){
              getUserProfilePage(response.user);
            }
            else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }
            
          },
            error: function(err){
                hideLoading();
                swal("Error!", err.statusText, "error");
            } 
      });


    }



   


    function validateUpdateLoginCredentialInput(){
      var hasError = false;

      $.each($('.updateUserLoginCredentialContainer input'), function(i,n){
        var parent = $(this).parent();
        parent.removeClass('has-error');
        parent.find('span').html('');

        if($(this).val().trim() == ''){
          parent.addClass('has-error');
          parent.find('span').html('This is required.');
          hasError = true;
        }
        else if($(this).hasClass('updateuser_password') || $(this).hasClass('updateuser_confirmpassword')){
          if($('.updateuser_password').val().trim() != $('.updateuser_confirmpassword').val().trim()){
            $('.updateuser_password , .updateuser_confirmpassword').parent().addClass('has-error');
            $('.updateuser_password , .updateuser_confirmpassword').parent().find('span').html('Password does not match.');
            hasError = true;
          }
        }
      });

      return hasError;
    }


    function validateUpdateUserInfoInput(){
      var hasError = false;

      $.each($('.updateUserInfoContainer input'), function(i,n){
        var parent = $(this).parent();
        parent.removeClass('has-error');
        parent.find('span').html('');

        if($(this).val().trim() == '' && !$(this).hasClass('updateuser_email')){
          parent.addClass('has-error');
          parent.find('span').html('This is required.');
          hasError = true;
        }
        else if($(this).hasClass('updateuser_email') && $('.updateuser_email').val().trim() != ''){
          if(!validateEmail($('.updateuser_email').val().trim())){
            $('.updateuser_email').parent().addClass('has-error');
            $('.updateuser_email').parent().find('span').html('Invalid email.');
            hasError = true;
          }
        }
      });

      return hasError;
    }


    


    function updateThisUserInfo(){
      if(validateUpdateUserInfoInput()){
        return false;
      }
      
      var ID            = $(this).attr('data-id');


      confirmWithSwal(function(){
        showLoading();
        var Fname         = $('.updateuser_firstname').val().toLowerCase().trim();
        var Lname         = $('.updateuser_lastname').val().toLowerCase().trim();
        var Mname         = $('.updateuser_middlename').val().toLowerCase().trim();
        var Address       = $('.updateuser_address').val().toLowerCase().trim();
        var ContactNumber = $('.updateuser_contactnumber').val().trim();
        var Email         = $('.updateuser_email').val().trim();
        var Position      = $('.updateuser_position').val().toLowerCase().trim();


        var data = {
            'ID'            : ID,
            'Fname'         : Fname,
            'Lname'         : Lname,
            'Mname'         : Mname,
            'Address'       : Address,
            'ContactNumber' : ContactNumber,
            'Email'         : Email,
            'Position'      : Position
          };



        $.ajax({
            type: 'POST',
            url: apiUrl+"/dashboard/user/updateUserInfo",
            data : data,
            cache: false,
            async: false,
            success: function(response) { 
              if(response.success){
                // clearAllInputs();
                if(ID == $('.headerGoToUserProfile').attr('data-id')){
                  $('.activeUserFullname').html(response.activeUserFullname);
                }

                getThisUser(ID);
                
                setTimeout(function(){swal("Success!", response.message, "success");},0);
              }
              else{
                hideLoading();
                
                setTimeout(function(){swal("Warning!", response.message, "warning");},0);
              }
              
              
            },
            error: function(err){
              hideLoading();
              setTimeout(function(){swal("Error!", err.statusText, "error")},0);
            }
        });
      },
      'Are you sure to submit this updates?',
      'warning');
    }





    function updateThisUserPassword(){
      if(validateUpdateLoginCredentialInput()){
        return false;
      }

      var id                      = $(this).attr('data-id');
      var Password                = $('.updateuser_password').val();

      
      confirmWithSwal(function (){
        showLoading();

        var data = {
            'ID'            : id,
            'Password'      : Password
          };


        $.ajax({
            type: 'POST',
            url: apiUrl+"/dashboard/user/updateUserLoginCredential",
            data : data,
            cache: false,
            async: false,
            success: function(response) { 
              if(response.success){
                $('.updateUserLoginCredentialContainer input').val("").focus();
                setTimeout(function(){swal("Success!", response.message, "success");},0);
              }
              else{
                setTimeout(function(){swal("Warning!", response.message, "warning");},0);
              }
              
              hideLoading();
            },
            error: function(err){
              hideLoading();
              setTimeout(function(){swal("Error!", err.statusText, "error");},0);
            }
        });
      },
        'Are you sure to update password?',
        'warning');
    }


    function getUserProfilePage(data){
      showLoading();

      $( ".dashboardPage" ).addClass('hidden');
      getTemplate(baseTemplateUrl_user, 'userProfile.html', function(render) {
          var renderedhtml = render({user: data});
          $(".updateUserOrEmployeeContainer").html(renderedhtml);
          $('.updateUserOrEmployeeContainer').removeClass('hidden');

        
          var id = "";
          $.each(data[0],function(i,n){
              if(i == 'ID'){
                id = n;
                $('.imageEmployeeID').val(n);
              }

              if(i == 'user_imageFilename'){
                

                if(n == 'no_image.jpg'){
                  $('.userProfileImage').attr('src','images/'+n);  
                }
                else
                  $('.userProfileImage').attr('src','api/csm_v1/public/uploads/users/'+id+'/'+n);
              }

              $(('.'+i)).val(n);
          });



          $(".userProfilePage").click();

          hideLoading();
      });



      
    }

    function getInfoForThisEmployee(id){
      showLoading();

      var id  = $(this).attr('data-id');

      if(id == '' || id == undefined){
        swal('Warning!' ,"Can't find this employee.",'error');
        return false;
      }


      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/user/getAllInfoForThisUser",
          data : {ID : id},
          cache: false,
          async: false, 
          success: function(response) {
            if(!response.has_login)
              logoutAccount();

            if(response.success){
              getUserProfilePage(response.user);
            }
            else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }
          },
            error: function(err){
                hideLoading();
                swal("Error!", err.statusText, "error");
            } 
      });
    }
}(); 



jQuery(document).ready(function() {    
  User.init();
});