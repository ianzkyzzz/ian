var User = function () {
    var templates = [],
            baseTemplateUrl_user = 'template/users/';
    var gl_userID = 0;
    var gl_state = false;
    var gl_userRole = "";

    function getTemplate(baseTemplateUrl, templateName, callback) {
        if (!templates[templateName]) {
            $.get(baseTemplateUrl + templateName, function(resp) {
                compiled = _.template(resp);
                templates[templateName] = compiled;
                if (_.isFunction(callback)) {
                    callback(compiled);
                }
            }, 'html');
        } else {
            callback(templates[templateName]);
        }
    }
    
 

    var employeeListTable;
    function tableemployeeListTable(){
        var table = $('.employeeListTable');
        employeeListTable =  table.dataTable({
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No employees added yet",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            // bootstrap_full_number
            "pagingType": "bootstrap_extended", 
            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, 50,100],
                [5, 10, 15, 20, 50,100] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            destroy : true
        });

    }


    return {
        init: init
    };

    // Init
    function init() {
        addEventHandlers();
        loadInitialization();
        
    }

    function loadInitialization(){
      

    }





    function addEventHandlers(){
        $(document).on('click', '.openAddUserPage', openAddUserPage );
        $(document).on('click', '.openUserListPage', openUserListPage );
        $(document).on('click','#backtoUserList',openUserListPage);
        $(document).on('click', '.employeeListTable td:not(:nth-child(5))', openUserProfilePage );
        $(document).on('click', '.btn_updateThisEmployee', openUserProfilePage2 );
        
        $(document).on('click', '.userProfileHTML  #btnfile', updateProfilePicClick ); 
        $(document).on('change', '.userProfileHTML  #file_uploads', updateProfilePicChange ); 
        $(document).on('submit', '#employeeProfilePictureForm', uploadThisImage ); 
        
        $(document).on('click', '.updateUserProfileInfoform', openUpdateUserProfileInfoform );
        
        $(document).on('click', '.adduser_btnsubmit', postThisUser ); 
        $(document).on('click', '.adduser_btnclear', clearAllInputs ); 
        $(document).on('click', '.updateuser_btnPasswordUpdatesubmit', updateThisUserPassword ); 
        $(document).on('click', '.updateuser_btnInformationUpdatesubmit', updateThisUserInfo ); 
        $(document).on('click', '.updateuser_btnUpdateReturn', returnToAllUsersList ); 

        $(document).on('click', '.btn_removeThisEmployee', removeThisUser ); 
        $(document).on('click', ' #btn_setUsername_user', UpdateUserUsername ); 
        
        $(document).on('click', '.overView', ifoverView ); 
        $(document).on('click', '.updateInfo', ifupdateInfo ); 
        $(document).on('click', '.changePass', ifchangePass ); 
    }


    function uploadThisImage(event){
      var formdata = new FormData(this);

      confirmWithSwal(function(){
        showLoading();

        $.ajax({
          url: apiUrl+"/dashboard/user/uploadThisImage",
          type: 'POST',
          data: formdata,
          async: true,
          cache: false,
          contentType: false,
          // enctype: 'multipart/form-data',
          processData: false,
          success: function (response) {      
            
            hideLoading();
            if(response.success){
              if(response.userID == $('.headerGoToUserProfile').attr('data-id')){
                $('.headerGoToUserProfile').parent().parent().parent().find('.activeUserIcon').attr('src',$('.userProfileImage').attr('src'));
              }

              setTimeout(function(){swal('Success!',response.message,'success');},0);
            }
            else{
              setTimeout(function(){swal('Warning!',response.message,'warning');},0);
            }

          },
          error: function(err){
            hideLoading();
            setTimeout(function(){swal("Error!", err.statusText, "error");},0);
          }
        });

      },'Are you sure to change image?','warning');

      event.stopPropagation();
      event.preventDefault(); 
    }


    function  ifoverView(){
        $(".overView").css({"color": "#169ef4", "text-decoration": "none", "background": "#ecf5fb", "border-left": "solid 2px #169ef4"});
        $(".updateInfo").css({"color": "#557386", "text-decoration": "none", "background": "#f0f6fa", "border-left": "solid 2px #c4d5df"});
        $(".changePass").css({"color": "#557386", "text-decoration": "none", "background": "#f0f6fa", "border-left": "solid 2px #c4d5df"});
        $(".img-update").addClass("hidden");
        $(".profile-edit").addClass("hidden");
    }
    
    function  ifupdateInfo(){
        $(".updateInfo").css({"color": "#169ef4", "text-decoration": "none", "background": "#ecf5fb", "border-left": "solid 2px #169ef4"});
        $(".overView").css({"color": "#557386", "text-decoration": "none", "background": "#f0f6fa", "border-left": "solid 2px #c4d5df"});
        $(".changePass").css({"color": "#557386", "text-decoration": "none", "background": "#f0f6fa", "border-left": "solid 2px #c4d5df"});
        $(".img-update").removeClass("hidden");
        $(".profile-edit").removeClass("hidden");
    }


    function  ifchangePass(){
      $(".changePass").css({"color": "#169ef4", "text-decoration": "none", "background": "#ecf5fb", "border-left": "solid 2px #169ef4"});
      $(".updateInfo").css({"color": "#557386", "text-decoration": "none", "background": "#f0f6fa", "border-left": "solid 2px #c4d5df"});
      $(".overView").css({"color": "#557386", "text-decoration": "none", "background": "#f0f6fa", "border-left": "solid 2px #c4d5df"});
      $(".img-update").addClass("hidden");
      $(".profile-edit").addClass("hidden");
    }

    


    function updateProfilePicClick(){
      $(".userProfileHTML #file_uploads").click();
    }

    


    function updateProfilePicChange(){
      var input = this;
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('.userProfileHTML  #btnfile').attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]);
      }
      
      // var x = document.getElementById(".userProfileHTML  file_uploads").value;
              
      // var path = x;
      // var pos =path.lastIndexOf( path.charAt( path.indexOf(":")+1) );
      // filename = path.substring( pos+1); 
    }



    function openUpdateUserProfileInfoform(){
      var pos = $('.updateuser_position').attr('data-value');
      $('.updateuserForm  input').focus();
      $.each($('.updateuser_position > option'),function(i,n){
          if($(this).val() == pos)
              $(this).attr('selected','');
      });
    }


    function openUserProfilePage(){
      var id = $(this).parent().find('.btn_updateThisEmployee').attr('data-id');
      
      getThisUser(id);
    }

    function openUserProfilePage2(){
      var id = $(this).attr('data-id');

      getThisUser(id);
    }


    function getThisUser(id){
      showLoading();

      if(id == undefined || id == 0 || id == "")
        swal('Warning!',"Can't find that user/employee.",'error');

      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/user/getAllInfoForThisUser",
          data : {ID : id},
          cache: false,
          async: false, 
          success: function(response) {
            if(!response.has_login)
              logoutAccount(); 

            if(response.success){
              getUserProfilePage(response.user, response.userRole);
            }
            else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }
            
          },
            error: function(err){
                hideLoading();
                swal("Error!", err.statusText, "error");
            } 
      });


    }


    function clearAllInputs(){
      $('.addUserOrEmployeeContainer input, .updateUserOrEmployeeContainer input, .listOfUserOrEmployeeContainer input').val('').focus();
    }




    function returnToAllUsersList(){
      clearAllInputs();
      openUserListPage();
    }


    function validateUserFormInputs(){
      var hasError = false;
      
      if($('.addUserForm').is(":visible")){
        if($('.adduser_password').val().trim() != $('.adduser_confirmpassword').val().trim()){
          $('.adduser_password , .adduser_confirmpassword').parent().addClass('has-error');
          $('.adduser_password , .adduser_confirmpassword').parent().find('span').html('Password does not match.');
          hasError = true;
        }

        $.each($('.addUserForm').find('input'),function(i,n){
          var parent = $(this).parent();
          parent.removeClass('has-error');
          parent.find('span').html('');

          if($(this).val().trim() == '' && !$(this).hasClass('adduser_email')){
            parent.addClass('has-error');
            parent.find('span').html('This is required.');
            hasError = true;
          }
          else if($(this).hasClass('adduser_password') || $(this).hasClass('adduser_confirmpassword')){
            if($('.adduser_password').val().trim() != $('.adduser_confirmpassword').val().trim()){
              $('.adduser_password , .adduser_confirmpassword').parent().addClass('has-error');
              $('.adduser_password , .adduser_confirmpassword').parent().find('span').html('Password does not match.');
              hasError = true;
            }
          }
          else if($(this).hasClass('adduser_email') && $('.adduser_email').val().trim() != ''){
            if(!validateEmail($('.adduser_email').val().trim())){
              $('.adduser_email').parent().addClass('has-error');
              $('.adduser_email').parent().find('span').html('Invalid email.');
              hasError = true;
            }
          }
        });


      }


      return hasError;
    }


    function validateUpdateLoginCredentialInput(){
      var hasError = false;

      $.each($('.updateUserLoginCredentialContainer input'), function(i,n){
        var parent = $(this).parent();
        parent.removeClass('has-error');
        parent.find('span').html('');

        if($(this).val().trim() == ''){
          parent.addClass('has-error');
          parent.find('span').html('This is required.');
          hasError = true;
        }
        else if($(this).hasClass('updateuser_password') || $(this).hasClass('updateuser_confirmpassword')){
          if($('.updateuser_password').val().trim() != $('.updateuser_confirmpassword').val().trim()){
            $('.updateuser_password , .updateuser_confirmpassword').parent().addClass('has-error');
            $('.updateuser_password , .updateuser_confirmpassword').parent().find('span').html('Password does not match.');
            hasError = true;
          }
        }
      });

      return hasError;
    }


    function validateUpdateUserInfoInput(){
      var hasError = false;

      $.each($('.updateUserInfoContainer input'), function(i,n){
        var parent = $(this).parent();
        parent.removeClass('has-error');
        parent.find('span').html('');

        if($(this).val().trim() == '' && !$(this).hasClass('updateuser_email')){
          parent.addClass('has-error');
          parent.find('span').html('This is required.');
          hasError = true;
        }
        else if($(this).hasClass('updateuser_email') && $('.updateuser_email').val().trim() != ''){
          if(!validateEmail($('.updateuser_email').val().trim())){
            $('.updateuser_email').parent().addClass('has-error');
            $('.updateuser_email').parent().find('span').html('Invalid email.');
            hasError = true;
          }
        }
      });

      return hasError;
    }


    

    function updateThisUserInfo(){

      if(validateUpdateUserInfoInput()){
        return false;
      }
      
      var ID            = $(this).attr('data-id');


      confirmWithSwal(function(){
        showLoading();
        var Fname         = $('.updateuser_firstname').val().toLowerCase().trim();
        var Lname         = $('.updateuser_lastname').val().toLowerCase().trim();
        var Mname         = $('.updateuser_middlename').val().toLowerCase().trim();
        var Address       = $('.updateuser_address').val().toLowerCase().trim();
        var ContactNumber = $('.updateuser_contactnumber').val().trim();
        var Email         = $('.updateuser_email').val().trim();
        var Position      = "";

        if (gf_userRole != "Business Partner" && gf_userRole != "Agent")  {
             Position = $('.updateuser_position').val().toLowerCase().trim();
        } 

        var data = {
            'ID'            : ID,
            'Fname'         : Fname,
            'Lname'         : Lname,
            'Mname'         : Mname,
            'Address'       : Address,
            'ContactNumber' : ContactNumber,
            'Email'         : Email,
            'Position'      : Position
          };


        $.ajax({
            type: 'POST',
            url: apiUrl+"/dashboard/user/updateUserInfo",
            data : data,
            cache: false,
            async: false,
            success: function(response) { 
              if(response.success){
                // clearAllInputs();
                if(ID == $('.headerGoToUserProfile').attr('data-id')){
                  $('.activeUserFullname').html(response.activeUserFullname);
                }

                getThisUser(ID);
                
                setTimeout(function(){swal("Success!", response.message, "success");},0);
              }
              else{
                hideLoading();
                
                setTimeout(function(){swal("Warning!", response.message, "warning");},0);
              }
              
              
            },
            error: function(err){
              hideLoading();
              setTimeout(function(){swal("Error!", err.statusText, "error")},0);
            }
        });
      },
      'Are you sure to submit this updates?',
      'warning');
    }





    function updateThisUserPassword(){
      if(validateUpdateLoginCredentialInput()){
        return false;
      }

      var id                      = $('.imageEmployeeID').val();
      var Password                = $('.updateuser_password').val();
      
      console.log(checkCurrentPassword());

      if (checkCurrentPassword()) {
          confirmWithSwal(function (){
            showLoading();
            var data = {
                'ID'            : id,
                'Password'      : Password
              };
            $.ajax({
                type: 'POST',
                url: apiUrl+"/dashboard/user/updateUserLoginCredential",
                data : data,
                cache: false,
                async: false,
                success: function(response) { 
                  if(response.success){
                    $('.updateUserLoginCredentialContainer input').val("").focus();
                    setTimeout(function(){swal("Success!", response.message, "success");},0);
                  }
                  else{
                    setTimeout(function(){swal("Warning!", response.message, "warning");},0);
                  }
                  
                  hideLoading();
                },
                error: function(err){
                  hideLoading();
                  setTimeout(function(){swal("Error!", err.statusText, "error");},0);
                }
            });
          },
            'Are you sure to update password?',
            'warning');
        }else{
          swal('Warning', 'Invalid current password, please try again', 'warning');
          $('#txt_current_pass').val('');
        }
    }


    function postThisUser(){
      if(validateUserFormInputs()){
        return false;
      }

      confirmWithSwal(function(){
        showLoading();

        var Fname         = $('.adduser_firstname').val().toLowerCase().trim();
        var Lname         = $('.adduser_lastname').val().toLowerCase().trim();
        var Mname         = $('.adduser_middlename').val().toLowerCase().trim();
        var Address       = $('.adduser_address').val().toLowerCase().trim();
        var ContactNumber = $('.adduser_contactnumber').val().trim();
        var Email         = $('.adduser_email').val().trim();
        var Position      = $('.adduser_position').val().toLowerCase().trim();
        var Username      = $('.adduser_username').val().trim();
        var Password      = $('.adduser_password').val();



        var data = {
            'Fname'         : Fname,
            'Lname'         : Lname,
            'Mname'         : Mname,
            'Address'       : Address,
            'ContactNumber' : ContactNumber,
            'Email'         : Email,
            'Position'      : Position,
            'Username'      : Username,
            'Password'      : Password
          };



        $.ajax({
            type: 'POST',
            url: apiUrl+"/dashboard/user/addUser",
            data : data,
            cache: false,
            async: false,
            success: function(response) { 
                if(response.success){
                  clearAllInputs();

                  hideLoading();
                  swal("Success!", response.message, "success");
                }
                else{
                  hideLoading();
                  swal("Warning!", response.message, "warning");
                }

                
            },
            error: function(err){
              hideLoading();
              swal("Error!", err.statusText, "error");
            }
        });
      },
        'Are you sure to submit this user?',
        'warning');

    }




    function openAddUserPage(){
      showLoading();
      var vavParent = $(this);
      

      $( ".dashboardPage" ).addClass('hidden').hide('fast');
      getTemplate(baseTemplateUrl_user, 'adduser.html', function(render) {
          var renderedhtml = render({users: ""});
          $(".addUserOrEmployeeContainer").html(renderedhtml);
          $('.addUserOrEmployeeContainer').removeClass('hidden');


          $('.page-sidebar-menu').find('.nav-item').removeClass('active open');

          if(vavParent.parent().hasClass('nav-item')){
            vavParent.parent().addClass('active open');
          }

          if(vavParent.parent().parent().parent().hasClass('nav-item')){
            vavParent.parent().parent().parent().addClass('active open');
          }
          
          hideLoading();
      });
    }



    function getUserProfilePage(data, userRole){
      const {user_position} = data[0];
      gf_userRole = user_position;

      showLoading();

      $( ".dashboardPage" ).addClass('hidden');
      getTemplate(baseTemplateUrl_user, 'userProfile.html', function(render) {
          var renderedhtml = render({user: data});
          $(".updateUserOrEmployeeContainer").html(renderedhtml);
          $('.updateUserOrEmployeeContainer').removeClass('hidden');

        
          var id = "";
          $.each(data[0],function(i,n){
              if(i == 'ID'){
                id = n;
                $('.imageEmployeeID').val(n);
                gl_userID = n;

              }

              if(i == 'user_imageFilename'){
                

                if(n == 'no_image.jpg'){
                  $('.userProfileImage').attr('src','images/'+n);  
                }
                else
                  $('.userProfileImage').attr('src','api/csm_v1/public/uploads/users/'+id+'/'+n);
              }

              $(('.'+i)).val(n);
          });

          if (userRole != 'Admin') {
              $('#position_div').remove();
          }


          $(".userProfilePage").click();

          hideLoading();
      });
 
    }


    function openUserListPage(){
      showLoading();
      var vavParent = $(this);
      

      $( ".dashboardPage" ).addClass('hidden');
      getTemplate(baseTemplateUrl_user, 'userslist.html', function(render) {
          var renderedhtml = render({users: ""});
          $(".listOfUserOrEmployeeContainer").html(renderedhtml);
          $('.listOfUserOrEmployeeContainer').removeClass('hidden');


          $('.page-sidebar-menu').find('.nav-item').removeClass('active open');

          if(vavParent.parent().hasClass('nav-item')){
            vavParent.parent().addClass('active open');
          }

          if(vavParent.parent().parent().parent().hasClass('nav-item')){
            vavParent.parent().parent().parent().addClass('active open');
          }
          
          tableemployeeListTable();

          getAllEmployees();

      });
      

    }


    function removeThisUser(){
      var id        = $(this).attr('data-id');
      var position  = $(this).attr('data-position');

      confirmWithSwal(function (){
        showLoading();

        if(id == '' || id == undefined){
          swal('Warning!' ,'Please provide id to be remove.','danger');
          return false;
        }

        $.ajax({
            type: 'POST',
            url: apiUrl+"/dashboard/user/removeUser",
            data : {ID : id,
                    Position : position
                  },
            cache: false,
            async: false, 
            success: function(response) {
              if(!response.has_login)
                logoutAccount();

              if(response.success){
                getAllEmployees();
                swal("Warning!", response.message, "success");
              }
              else
                  swal("Warning!", response.message, "warning");
              
              setTimeout(function(){$('.hmsLoading2').addClass('hidden')},100);
            },
              error: function(err){
                  setTimeout(function(){$('.hmsLoading2').addClass('hidden')},100);
                  swal("Error!", err.statusText, "error");
              } 
        });
      },
      'Are you sure to remove this user?',
      'warning');
      
    }



    

    function getInfoForThisEmployee(id){
      showLoading();
      var id  = $(this).attr('data-id');

      if(id == '' || id == undefined){
        swal('Warning!' ,"Can't find this employee.",'error');
        return false;
      }


      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/user/getAllInfoForThisUser",
          data : {ID : id},
          cache: false,
          async: false, 
          success: function(response) {
            if(!response.has_login)
              logoutAccount();

            if(response.success){
              getUserProfilePage(response.user);
            }
            else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }
          },
            error: function(err){
                hideLoading();
                swal("Error!", err.statusText, "error");
            } 
      });
    }


    function getAllEmployees(){
      showLoading();
      $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/user/getAllUser",
          cache: false,
          async: false, 
          success: function(response) {
            if(!response.has_login)
              logoutAccount();

            if(response.success){
              var table = $('.employeeListTable').DataTable();
              table.clear().draw();
              table.rows.add(response.users).draw();

              hideLoading();
            }
            else{
              hideLoading();
              swal("Warning!", response.message, "warning");
            }
          },
          error: function(err){
            hideLoading();
            swal("Error!", err.statusText, "error");
          }
      });
    }

    function UpdateUserUsername(argument) {
      const username = document.getElementById('txt_username_user').value;
      const userID = $('.imageEmployeeID').val();
      if (username === '') {
        alert('Unable to proceed username is empty!');
      }else{
        showLoading();
        $.ajax({
            type: 'PUT',
            url: apiUrl+"/dashboard/user/update_thisUserUsername",
            data: {username: username, ID: userID},
            cache: false,
            async: false, 
            success: function(response) {
              if(response.success){
                swal('success', response.message, "success");
              }
              else{
                swal("Warning!", response.message, "warning");
              }
                hideLoading();
            },
            error: function(err){
              hideLoading();
              swal("Error!", err.statusText, "error");
            }
        });
      } 
    }

    function checkCurrentPassword(){
      gl_state = false;
      const userID = $('.imageEmployeeID').val();
      const currnetPass = document.getElementById('txt_current_pass').value;
        $.ajax({
          type: 'GET',
          url: apiUrl+"/dashboard/user/checkThisCurrentPassword",
          data: {pass: currnetPass, ID: userID},
          cache: false,
          async: false, 
          success: function(response) {
              gl_state = response.success;
          },
          error: function(err){
            swal("Error!", err.statusText, "error");
          }
        });

      return gl_state
    } 
    
}(); 



jQuery(document).ready(function() {    
  User.init();
});