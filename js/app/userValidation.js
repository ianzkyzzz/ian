var UserValidation = function () {


    return {
        init: init
    };

    // Init
    function init() {
        loadUserValidation();
    }

    function loadUserValidation(){
        if(window.location.pathname != '/balance_inquiry_page')
            $.ajax({
                type: 'GET',
                url: apiUrl+"/dashboard/reports",
                cache: false,
                async: false,
                success: function(response) {
                    if (response.success==false || response.has_login==false) {
                        setTimeout(function() {
                            window.location.href = "/";
                        }, 1000);
                    }
                }
            });
 
        // $('.dashboard-page').addClass('hidden').attr('hidden','');
        // $('#hmsTransactionContainer').removeClass('hidden').removeAttr('hidden');
    }

}();




jQuery(document).ready(function() {    
  UserValidation.init();
});