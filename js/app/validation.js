function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validateTextWithNumber(field) { // with whitespace
    var re = /^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/;
    return re.test(field);
}

function validateInteger(field) {
    var re = /^\d+$/;
    return re.test(field);
}

function validateTextOnly(field) {
    var re = /^[a-zA-Z]*$/;
    return re.test(field);
} 

function validateAlphanumeric(field) {
    var re = /^[a-z0-9]+$/i;
    return re.test(field);
}

function validateNumberAndDot(s) {
    var rgx = /^[0-9]*\.?[0-9]*$/;
    return s.match(rgx);
}

function validatePasswordMatch(field1, field2) {
    if($.trim(field1) == $.trim(field2)) {
        return true;
    } 
 
    return false;
}

function ReplaceNumberWithCommas(yourNumber) {
    //Seperates the components of the number
    var n= yourNumber.toString().split(".");
    //Comma-fies the first part
    n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    //Combines the two sections
    return n.join(".");
}





// API Constant

var apiUrl = 'api/v1/public/';