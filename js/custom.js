$ = jQuery;
$(document).ready(function(){
	$('.contact-list>li').click(function(){
		$('.contact-list>li').removeClass('active');
		$(this).addClass('active');
		var id = $(this).children('input.contact-id').val();
		$('.btn-tool').attr('href','/conversation/'+id);
	});

	$('.chat-list').scrollTop($('.chat-list').prop("scrollHeight"));

	$('.hmsAdmissionFormFictitious').click(function(){
        var atLeastOneIsChecked = $('input[name="fictitious"]:checked').length > 0;
        if(atLeastOneIsChecked == false){
            $('.hmsAdmissionFormDob').fadeIn();
            $('#hmsAdmissionFormDobInput').attr("required", true);
        }
        else {
            $('#hmsAdmissionFormDobInput').removeAttr("required");
            $('.hmsAdmissionFormDob').fadeOut();
        }
    });
    
    $(document).on('change', '.hmsLaboratorySelect', function (){
        var value = $(this).val();
    
        if(value == "ongoing"){
            $(this).parent('td').siblings('td').children('button').removeClass('disabled');
        }
        else {
            $(this).parent('td').siblings('td').children('button').addClass('disabled');
        }

    });
});